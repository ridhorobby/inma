-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2021 at 01:38 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inma`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_pengajuan`
--

CREATE TABLE `detail_pengajuan` (
  `id` int(11) NOT NULL,
  `id_pengajuan_harga` int(11) NOT NULL,
  `id_harga` int(11) NOT NULL,
  `harga_pokok_granit` int(11) NOT NULL,
  `harga_pokok_keramik` int(11) NOT NULL,
  `harga_jual_granit` int(11) NOT NULL,
  `harga_jual_keramik` int(11) NOT NULL,
  `jumlah` decimal(10,1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `is_deleted` varchar(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pengajuan`
--

INSERT INTO `detail_pengajuan` (`id`, `id_pengajuan_harga`, `id_harga`, `harga_pokok_granit`, `harga_pokok_keramik`, `harga_jual_granit`, `harga_jual_keramik`, `jumlah`, `created_at`, `updated_at`, `updated_by`, `is_deleted`) VALUES
(1, 1, 5, 500000, 500000, 600000, 600000, '1.0', '2020-07-12 19:59:54', '2020-07-12 19:59:54', 0, '0'),
(2, 1, 6, 260000, 260000, 440000, 440000, '1.0', '2020-07-12 19:59:54', '2020-07-12 19:59:54', 0, '0'),
(3, 1, 7, 100000, 100000, 130000, 130000, '1.0', '2020-07-12 19:59:54', '2020-07-12 19:59:54', 0, '0'),
(4, 1, 8, 1880000, 1650000, 3000000, 2300000, '1.0', '2020-07-12 19:59:54', '2020-07-12 19:59:54', 0, '0'),
(5, 1, 25, 160000, 160000, 200000, 200000, '1.0', '2020-07-12 19:59:54', '2020-07-12 19:59:54', 0, '0'),
(6, 1, 22, 300000, 300000, 465000, 465000, '1.0', '2020-07-12 19:59:54', '2020-07-12 19:59:54', 0, '0'),
(7, 1, 30, 200000, 200000, 400000, 400000, '1.0', '2020-07-12 19:59:54', '2020-07-12 19:59:54', 0, '0'),
(8, 1, 32, 0, 0, 30000, 30000, '1.0', '2020-07-12 19:59:54', '2020-07-12 19:59:54', 0, '0'),
(9, 1, 37, 5000, 10000, 10000, 20000, '1.0', '2020-07-12 19:59:54', '2020-07-12 19:59:54', 0, '0'),
(10, 2, 4, 450000, 450000, 550000, 550000, '1.0', '2020-07-13 17:36:43', '2020-07-13 17:36:43', 16, '0'),
(11, 2, 5, 500000, 500000, 600000, 600000, '1.0', '2020-07-13 17:36:43', '2020-07-13 17:36:43', 16, '0'),
(12, 2, 6, 260000, 260000, 440000, 440000, '1.0', '2020-07-13 17:36:43', '2020-07-13 17:36:43', 16, '0'),
(13, 2, 7, 100000, 100000, 130000, 130000, '1.0', '2020-07-13 17:36:43', '2020-07-13 17:36:43', 16, '0'),
(14, 2, 8, 1880000, 1650000, 3000000, 2300000, '1.0', '2020-07-13 17:36:43', '2020-07-13 17:36:43', 16, '0'),
(15, 2, 25, 160000, 160000, 200000, 200000, '1.0', '2020-07-13 17:36:43', '2020-07-13 17:36:43', 16, '0'),
(16, 3, 4, 450000, 450000, 550000, 550000, '1.0', '2020-07-13 17:38:15', '2020-07-13 17:38:15', 16, '0'),
(17, 3, 5, 500000, 500000, 600000, 600000, '1.0', '2020-07-13 17:38:15', '2020-07-13 17:38:15', 16, '0'),
(18, 3, 6, 260000, 260000, 440000, 440000, '1.0', '2020-07-13 17:38:15', '2020-07-13 17:38:15', 16, '0'),
(19, 3, 7, 100000, 100000, 130000, 130000, '1.0', '2020-07-13 17:38:15', '2020-07-13 17:38:15', 16, '0'),
(20, 3, 8, 1880000, 1650000, 3000000, 3000000, '1.0', '2020-07-13 17:38:15', '2020-07-13 17:38:15', 16, '0'),
(21, 3, 25, 160000, 160000, 200000, 200000, '1.0', '2020-07-13 17:38:15', '2020-07-13 17:38:15', 16, '0'),
(22, 3, 32, 0, 0, 300000, 300000, '1.0', '2020-07-13 17:38:15', '2020-07-13 17:38:15', 16, '0'),
(23, 3, 37, 5000, 10000, 10000, 20000, '1.0', '2020-07-13 17:38:15', '2020-07-13 17:38:15', 16, '0'),
(28, 5, 2, 880000, 640000, 0, 1100000, '1.0', '2020-07-14 21:14:49', '2020-07-14 21:14:49', 16, '0'),
(29, 5, 9, 1800000, 1440000, 0, 2200000, '1.0', '2020-07-14 21:14:49', '2020-07-14 21:14:49', 16, '0'),
(30, 5, 19, 880000, 880000, 0, 1500000, '1.0', '2020-07-14 21:14:49', '2020-07-14 21:14:49', 16, '0'),
(31, 5, 22, 300000, 300000, 0, 465000, '1.0', '2020-07-14 21:14:49', '2020-07-14 21:14:49', 16, '0'),
(40, 6, 9, 1800000, 1440000, 2700000, 2200000, '1.0', '2020-07-15 19:48:12', '2020-07-15 19:48:12', 16, '0'),
(41, 6, 10, 0, 0, 0, 0, '1.0', '2020-07-15 19:48:12', '2020-07-15 19:48:12', 16, '0'),
(42, 6, 2, 880000, 640000, 1400000, 1100000, '1.0', '2020-07-15 19:48:12', '2020-07-15 19:48:12', 16, '0'),
(43, 6, 3, 0, 0, 0, 0, '1.0', '2020-07-15 19:48:12', '2020-07-15 19:48:12', 16, '0'),
(44, 6, 19, 880000, 880000, 1500000, 1500000, '1.0', '2020-07-15 19:48:12', '2020-07-15 19:48:12', 16, '0'),
(45, 6, 20, 0, 0, 0, 0, '1.0', '2020-07-15 19:48:12', '2020-07-15 19:48:12', 16, '0'),
(46, 6, 22, 300000, 300000, 465000, 465000, '1.0', '2020-07-15 19:48:12', '2020-07-15 19:48:12', 16, '0'),
(47, 6, 32, 0, 0, 10000, 10000, '1.0', '2020-07-15 19:48:12', '2020-07-15 19:48:12', 16, '0'),
(150, 7, 9, 1800000, 1440000, 2700000, 2200000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(151, 7, 10, 1880000, 1120000, 3000000, 1900000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(152, 7, 11, 200000, 160000, 300000, 250000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(153, 7, 12, 120000, 120000, 275000, 275000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(154, 7, 2, 880000, 640000, 1400000, 1100000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(155, 7, 3, 320000, 320000, 550000, 550000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(156, 7, 4, 450000, 450000, 550000, 550000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(157, 7, 5, 500000, 500000, 600000, 600000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(158, 7, 7, 100000, 100000, 130000, 130000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(159, 7, 25, 160000, 160000, 200000, 200000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(160, 7, 19, 880000, 880000, 1500000, 1500000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(161, 7, 22, 300000, 300000, 465000, 465000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(162, 7, 23, 0, 0, 300000, 300000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(163, 7, 30, 200000, 200000, 400000, 400000, '1.0', '2020-08-05 20:01:19', '2020-08-05 20:01:19', 16, '0'),
(213, 4, 9, 0, 0, 0, 0, '1.0', '2020-08-08 21:44:16', '2020-08-08 21:44:16', 27, '0'),
(214, 4, 2, 0, 0, 0, 0, '1.0', '2020-08-08 21:44:16', '2020-08-08 21:44:16', 27, '0'),
(215, 4, 19, 0, 0, 0, 0, '1.0', '2020-08-08 21:44:16', '2020-08-08 21:44:16', 27, '0'),
(216, 4, 22, 0, 0, 0, 0, '1.0', '2020-08-08 21:44:16', '2020-08-08 21:44:16', 27, '0'),
(243, 10, 2, 880000, 640000, 1400000, 1100000, '3.0', '2020-08-14 11:08:19', '2020-08-14 11:08:19', 16, '0'),
(244, 10, 3, 320000, 320000, 550000, 550000, '1.0', '2020-08-14 11:08:19', '2020-08-14 11:08:19', 16, '0'),
(245, 10, 4, 450000, 450000, 550000, 550000, '1.0', '2020-08-14 11:08:19', '2020-08-14 11:08:19', 16, '0'),
(246, 10, 5, 500000, 500000, 600000, 600000, '1.0', '2020-08-14 11:08:19', '2020-08-14 11:08:19', 16, '0'),
(247, 10, 6, 260000, 260000, 440000, 440000, '1.0', '2020-08-14 11:08:19', '2020-08-14 11:08:19', 16, '0'),
(298, 8, 2, 880000, 640000, 1400000, 1400000, '4.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(299, 8, 4, 450000, 450000, 550000, 550000, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(300, 8, 7, 100000, 100000, 130000, 130000, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(301, 8, 25, 160000, 160000, 200000, 200000, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(302, 8, 9, 1800000, 1440000, 2700000, 2200000, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(303, 8, 10, 1880000, 1120000, 3000000, 1900000, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(304, 8, 11, 200000, 160000, 300000, 250000, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(305, 8, 13, 250000, 350000, 410000, 410000, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(306, 8, 19, 880000, 880000, 1500000, 1500000, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(307, 8, 20, 880000, 880000, 1500000, 1500000, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(308, 8, 23, 0, 0, 300000, 300000, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(309, 8, 26, 0, 0, 150000, 150000, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(310, 8, 31, 250000, 250000, 400000, 400000, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(311, 8, 32, 0, 0, 0, 0, '1.0', '2020-08-14 16:10:39', '2020-08-14 16:10:39', 16, '0'),
(322, 11, 2, 880000, 640000, 1400000, 1100000, '1.0', '2020-08-14 16:11:13', '2020-08-14 16:11:13', 16, '0'),
(323, 11, 3, 320000, 320000, 550000, 550000, '1.0', '2020-08-14 16:11:13', '2020-08-14 16:11:13', 16, '0'),
(324, 11, 4, 450000, 450000, 550000, 550000, '3.0', '2020-08-14 16:11:13', '2020-08-14 16:11:13', 16, '0'),
(325, 11, 5, 500000, 500000, 600000, 600000, '1.0', '2020-08-14 16:11:13', '2020-08-14 16:11:13', 16, '0'),
(326, 11, 6, 260000, 260000, 440000, 440000, '2.0', '2020-08-14 16:11:13', '2020-08-14 16:11:13', 16, '0'),
(333, 13, 2, 880000, 640000, 1400000, 1100000, '1.0', '2020-08-24 10:29:10', '2020-08-24 10:29:10', 16, '0'),
(334, 13, 3, 320000, 320000, 550000, 550000, '1.0', '2020-08-24 10:29:10', '2020-08-24 10:29:10', 16, '0'),
(335, 12, 2, 880000, 640000, 1400000, 1100000, '1.0', '2020-09-02 19:22:20', '2020-09-02 19:22:20', 16, '0'),
(336, 12, 3, 320000, 320000, 550000, 550000, '3.0', '2020-09-02 19:22:20', '2020-09-02 19:22:20', 16, '0'),
(337, 12, 5, 500000, 500000, 600000, 600000, '1.0', '2020-09-02 19:22:20', '2020-09-02 19:22:20', 16, '0'),
(338, 12, 6, 260000, 260000, 440000, 440000, '1.0', '2020-09-02 19:22:20', '2020-09-02 19:22:20', 16, '0'),
(339, 12, 7, 100000, 100000, 130000, 130000, '1.0', '2020-09-02 19:22:20', '2020-09-02 19:22:20', 16, '0'),
(340, 12, 23, 0, 0, 300000, 300000, '1.0', '2020-09-02 19:22:20', '2020-09-02 19:22:20', 16, '0'),
(341, 15, 2, 880000, 640000, 1400000, 1100000, '1.0', '2021-01-13 21:04:28', '2021-01-13 21:04:28', 35, '0'),
(342, 9, 2, 880000, 650000, 1400000, 1400000, '1.0', '2021-02-26 17:39:27', '2021-02-26 17:39:27', 30, '0'),
(343, 9, 3, 320000, 320000, 550000, 550000, '1.0', '2021-02-26 17:39:27', '2021-02-26 17:39:27', 30, '0'),
(344, 9, 5, 500000, 500000, 600000, 600000, '1.0', '2021-02-26 17:39:27', '2021-02-26 17:39:27', 30, '0'),
(345, 9, 6, 260000, 260000, 440000, 440000, '1.0', '2021-02-26 17:39:27', '2021-02-26 17:39:27', 30, '0'),
(346, 9, 25, 160000, 160000, 200000, 200000, '1.0', '2021-02-26 17:39:27', '2021-02-26 17:39:27', 30, '0'),
(347, 9, 27, 2500000, 2250000, 3700000, 3700000, '1.0', '2021-02-26 17:39:27', '2021-02-26 17:39:27', 30, '0'),
(348, 9, 9, 1800000, 1440000, 2700000, 2700000, '1.0', '2021-02-26 17:39:27', '2021-02-26 17:39:27', 30, '0'),
(356, 17, 2, 880000, 640000, 1400000, 1100000, '1.0', '2021-04-27 21:23:04', '2021-04-27 21:23:04', 30, '0'),
(357, 17, 3, 320000, 320000, 550000, 550000, '2.0', '2021-04-27 21:23:04', '2021-04-27 21:23:04', 30, '0'),
(358, 17, 4, 450000, 450000, 550000, 550000, '3.0', '2021-04-27 21:23:04', '2021-04-27 21:23:04', 30, '0'),
(359, 17, 5, 500000, 500000, 600000, 600000, '4.0', '2021-04-27 21:23:04', '2021-04-27 21:23:04', 30, '0'),
(360, 17, 6, 260000, 260000, 440000, 440000, '1.0', '2021-04-27 21:23:04', '2021-04-27 21:23:04', 30, '0'),
(361, 17, 7, 100000, 100000, 130000, 130000, '1.0', '2021-04-27 21:23:04', '2021-04-27 21:23:04', 30, '0'),
(362, 17, 8, 1880000, 1650000, 3000000, 2300000, '1.0', '2021-04-27 21:23:04', '2021-04-27 21:23:04', 30, '0'),
(363, 17, 25, 160000, 160000, 200000, 200000, '2.0', '2021-04-27 21:23:04', '2021-04-27 21:23:04', 30, '0'),
(364, 18, 2, 880000, 640000, 1400000, 1100000, '1.0', '2021-04-27 22:11:56', '2021-04-27 22:11:56', 35, '0'),
(365, 19, 2, 880000, 640000, 1400000, 1100000, '2.0', '2021-06-01 15:39:21', '2021-06-01 15:39:21', 35, '0'),
(366, 19, 3, 320000, 320000, 550000, 550000, '3.0', '2021-06-01 15:39:21', '2021-06-01 15:39:21', 35, '0'),
(367, 20, 2, 880000, 640000, 1400000, 1100000, '2.0', '2021-06-01 16:41:57', '2021-06-01 16:41:57', 35, '0'),
(368, 20, 3, 320000, 320000, 550000, 550000, '2.0', '2021-06-01 16:41:57', '2021-06-01 16:41:57', 35, '0');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pengajuan_komplain`
--

CREATE TABLE `detail_pengajuan_komplain` (
  `id` int(11) NOT NULL,
  `id_pengajuan_harga_komplain` int(11) NOT NULL,
  `id_harga_maintenance` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` decimal(10,0) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `is_deleted` varchar(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pengajuan_komplain`
--

INSERT INTO `detail_pengajuan_komplain` (`id`, `id_pengajuan_harga_komplain`, `id_harga_maintenance`, `harga`, `jumlah`, `created_at`, `updated_at`, `updated_by`, `is_deleted`) VALUES
(1, 2, 1, 100000, '11', '2021-04-27 21:14:09', '2021-04-27 21:14:09', 33, '0'),
(2, 2, 2, 50000, '2', '2021-04-27 21:14:09', '2021-04-27 21:14:09', 33, '0'),
(3, 2, 3, 50000, '1', '2021-04-27 21:14:09', '2021-04-27 21:14:09', 33, '0'),
(4, 2, 4, 30000, '1', '2021-04-27 21:14:09', '2021-04-27 21:14:09', 33, '0'),
(5, 2, 5, 350000, '1', '2021-04-27 21:14:09', '2021-04-27 21:14:09', 33, '0'),
(6, 2, 6, 150000, '2', '2021-04-27 21:14:09', '2021-04-27 21:14:09', 33, '0'),
(7, 2, 7, 400000, '1', '2021-04-27 21:14:09', '2021-04-27 21:14:09', 33, '0'),
(8, 3, 1, 100000, '2', '2021-04-27 22:22:08', '2021-04-27 22:22:08', 33, '0'),
(9, 4, 1, 100000, '11', '2021-04-28 08:46:16', '2021-04-28 08:46:16', 33, '0'),
(10, 4, 2, 50000, '2', '2021-04-28 08:46:16', '2021-04-28 08:46:16', 33, '0'),
(11, 4, 3, 50000, '2', '2021-04-28 08:46:16', '2021-04-28 08:46:16', 33, '0'),
(12, 4, 4, 30000, '3', '2021-04-28 08:46:16', '2021-04-28 08:46:16', 33, '0'),
(13, 4, 5, 350000, '4', '2021-04-28 08:46:16', '2021-04-28 08:46:16', 33, '0'),
(14, 4, 6, 150000, '5', '2021-04-28 08:46:16', '2021-04-28 08:46:16', 33, '0'),
(15, 4, 7, 400000, '6', '2021-04-28 08:46:16', '2021-04-28 08:46:16', 33, '0'),
(16, 4, 8, 300000, '7', '2021-04-28 08:46:16', '2021-04-28 08:46:16', 33, '0');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `secure_id` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `manufacture` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  `product` varchar(100) NOT NULL,
  `serial` varchar(100) NOT NULL,
  `hardware` varchar(100) NOT NULL,
  `version` varchar(100) NOT NULL,
  `reg_id` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `secure_id`, `name`, `brand`, `manufacture`, `model`, `product`, `serial`, `hardware`, `version`, `reg_id`, `created_at`, `updated_at`) VALUES
(1, '9a622e4d0b48b59c', 'RMX1851', 'Realme', 'Realme', 'RMX1851', 'RMX1851', 'unknown', 'qcom', '28', 'cq2O7rH_S8CM4FuktN85k0:APA91bEUVG7fu52iH4Ut34x85FySaiOlENL2oTZACRPkIoJ6W2P_DIdLG2NxLr5i9LTO-yC-UJityuy4jDpEKtiP634pmdazm1fXvIXUWXEcHORYXvzDi2V6C0tV-hAw81wynsXO-OqY', '2020-08-15 07:33:28', '2020-01-29 13:07:01'),
(2, '0ee41f2e26215ebe', 'generic_x86', 'google', 'Google', 'Android SDK built for x86', 'sdk_gphone_x86', 'unknown', 'ranchu', '28', 'regIdCoba2', '2020-01-30 11:31:10', '2020-01-30 11:31:10'),
(3, '7b54085ba74f3bb1', 'generic_x86', 'google', 'Google', 'Android SDK built for x86', 'sdk_gphone_x86', 'unknown', 'ranchu', '29', 'ckbVMcaelRQ:APA91bFsWN5RbEZjdRMNHLoDm7kv6BKYGAKCVOzf9FLsuE2LgWEP-GUaC9Vy8d5eJTtop2CJsOX5LKqTa8Pd5DhATT8DGo-mtnfMqIuNOgxRl5wlh8OnSd3v2688ZP1GZxi1hNURyeor', '2020-03-27 11:26:18', '2020-02-06 11:19:17'),
(4, 'd4afbe257b4b72f2', 'RMX1851', 'Realme', 'Realme', 'RMX1851', 'RMX1851', 'unknown', 'qcom', '28', 'c94NGW5lSuWSuOIHd4P3tY:APA91bFgGWU9piq_zIyTY8luo4dfeOY6mlJjm8wAQWFYmx4rxguTNW3A8xz4PBwi35SOWnPmqBcmM3xEAqcyApLyRQKDYiI-aMLdDbJjE1YkSrQsXyO56q2YOimeRabwxNb3tyv_3LnN', '2021-02-01 14:25:36', '2020-02-09 01:53:38'),
(5, '86a606065729ebee', 'oxygen', 'Xiaomi', 'Xiaomi', 'MI MAX 2', 'oxygen', 'e2706a61', 'qcom', '25', 'e0ygQhUVRmGlMivWFpcm3i:APA91bGfqEkXY8C9l0MIPVIC0vLoLFyIduiwriMovpyvVmzvt8QaBXZ62jc7rvj7DN7duFbIa0of5kdtY97tTPKprwJJAYgOdUpqtOjQunaEbvgKyoBMiM_T-0Nr8s82jGp17hceiDVY', '2020-05-08 23:37:00', '2020-02-21 11:24:43'),
(6, '614ece438f4d547b', 'RMX1851', 'Realme', 'Realme', 'RMX1851', 'RMX1851', 'unknown', 'qcom', '29', 'duHjjXJ_SMOb6tDCzeb4Nt:APA91bGkt6uKsN3XfMkojBhjkHTmC9pXa-sFMfiBChDms8KQVMPo-wCsGxiCR5O5IxNNlRPnawOkWTdqnzJE9OAAsD6HnikeD_5yCjCvRQOTbyHHwPh21YjUCA3ma0nYZrrGau_49RXT', '2020-07-24 09:59:17', '2020-02-26 07:25:34'),
(7, 'e221d0da46267434', 'whyred', 'xiaomi', 'Xiaomi', 'Redmi Note 5', 'whyred', '1912c6ae', 'qcom', '27', 'fruxfruZREe2NQTr45EXnG:APA91bF34DW_9T2T09krg0qnlq2S0n9UURzx3tHT3YY9LEC37hHxi4MwhzHNTPcK0k5aRnuqvCGgUYRFHAdmAPlNDsrTlst_4IlanCDVP0vYN1ppKdch-xVyArzRRfWlLkjon2Jn1rmA', '2020-08-24 08:17:30', '2020-02-29 04:39:41'),
(8, '26c5509ee89154df', 'ysl', 'xiaomi', 'Xiaomi', 'Redmi S2', 'ysl', 'unknown', 'qcom', '28', 'fbdHx407RZCgqCqiEMGEXQ:APA91bHxGomoc0DPzZRgR2-HwXyk0mB4kstoLT1pARYNvs7C5Cz38LeqdhQr29RfFqIUm7WtmrTg_nECcTlxQWj-Z9fc0kyrJQXtT9N5lrTMMByz_w6wJ0kiUfGUgqfDBniulIwb_FeX', '2020-07-15 04:42:53', '2020-02-29 05:18:26'),
(9, 'edoganteng', 'RMX1851', 'Realme', 'Realme', 'RMX1851', 'RMX1851', 'unknown', 'qcom', '28', '', '2020-03-09 10:40:25', '2020-03-04 03:03:48'),
(10, '19190163602457e5', '1804', 'vivo', 'vivo', 'vivo 1804', '1804', 'unknown', 'qcom', '28', 'elw2GiqWSiydkJYqClTOTW:APA91bFh7xeGMIfLpDdlUXM_ZumbnpgG3wFp53bhdo9o-GCactHgNl0mTbuuIHUHaHGz-JtwdgSNgzl4JC2bVddcs_glcWL6PfpaJfphqs99RRxNOtTiLKkt0FkG2tQ81BTwo3tJEOwe', '2020-07-03 02:28:10', '2020-03-06 01:40:18'),
(11, '83a6f80ebcdbd095', 'generic_x86', 'google', 'Google', 'Android SDK built for x86', 'sdk_gphone_x86', 'EMULATOR29X3X4X0', 'ranchu', '27', 'caHHgZR-5Dw:APA91bFmUMj1YHoDp1Chvjbo_AvBaGZnImCHl05TPGa-BcYW6lYJtzQKDtTgfDd_b4X7dabDw0MzXProGX_5TUriXpKFqPJ8cl3OZr1NVj9zsW3odXQAzw65e5CkDMe2h242iZqUs2wK', '2020-04-01 09:41:03', '2020-04-01 09:41:03'),
(12, '7fac1f1dd3370859', 'generic_x86', 'google', 'Google', 'Android SDK built for x86', 'sdk_gphone_x86', 'EMULATOR29X3X4X0', 'ranchu', '27', 'eDn89Pnm5JQ:APA91bHgwKpjl9Rbmw9uRXqtEMWWCEsXyEVhTLS5DhsQ0hYnp8puCquC40ljeaTAdknqDkwW7mQph0bZK-bOfUpzpDXZCAK99c8Mrdg5my7LtnO3DUPgGPljAzyJH1xP4V4HzjarVUYv', '2020-05-06 10:36:06', '2020-04-16 01:35:13'),
(13, '548da8a5a57bf0ca', 'ginkgo', 'xiaomi', 'Xiaomi', 'Redmi Note 8', 'ginkgo', 'unknown', 'qcom', '28', 'eFRIf2aATgerMHqUXfKutv:APA91bEx4PIBo_j1pMW7RRSXwZxC7uUJ8t7gYFmStuIihEJP_cEOPeWHhpC0kQlOf54Djf-Oin2QJkCh9uYaeW7naz4-h4AnGkOpOnxuclxtbsEc1cXSYOQddrZXwuqNYr7r94vliBI6', '2020-09-01 08:35:07', '2020-04-24 12:17:41'),
(14, '58a55f7e9fdb827', 'sanders_n', 'motorola', 'motorola', 'Moto G (5S) Plus', 'sanders_n', 'ZY322GSF6B', 'qcom', '25', 'eeEeKJ6hQYWhYWBz8Ap1Ux:APA91bFGtlu5qrvhljtiZh7rGmty2U49oBG2qXyEHTjEXn3Ku5RPYsya0rUh26UgJuQZyPK90uhpRzhB7p_MECxQOOd6z2yajgpXVUzURnSlfZS4Zgc3vRdOf6ixDR8ANjqWILtY537Y', '2020-05-28 09:11:09', '2020-05-18 03:23:14'),
(15, '05285ce9c1faa66c', 'generic_x86', 'google', 'Google', 'Android SDK built for x86', 'sdk_gphone_x86', 'EMULATOR29X3X4X0', 'ranchu', '27', 'fuxjU8t7TgO-UBku73mzgR:APA91bGUzvBkpWDg5luOIfs3BVhCVi5oqY0IXctEHn2orAVDPDxKaDcEOiccnGkNhTqsJi7p1W_AKkPD4ml0rae6xOPj1xpKiBq2zCLaSU02tTXA_tk4eaunHnD4sRqTMtwAJLPZbqtv', '2020-06-22 09:56:35', '2020-06-22 09:56:35'),
(16, '43d7ae5809d665f9', 'generic_x86', 'google', 'Google', 'Android SDK built for x86', 'sdk_gphone_x86', 'EMULATOR29X3X4X0', 'ranchu', '27', 'cpoFMRAkTvKbMbXDCA8ilt:APA91bFAwtzQPndE2EievrXub1bNmjRmFRGTN2feqvMiOAMDRULgxTSRG1A__b9VwTbqba6E63odlH1KpOYzIjfIN1WDA_-ARPUyureBI53ZcA8PDD4DT1FLS5Spc9NYKiHDJl-ZmVVR', '2020-08-31 12:44:42', '2020-06-30 11:48:08'),
(17, '1c1b0d9f1a6c5351', 'greatlte', 'samsung', 'samsung', 'SM-N950F', 'greatltexx', 'd9f1a6c53511c1b0', 'samsungexynos8895', '19', 'dBZW1_v6R4i3pyebhm7ncU:APA91bGeuRfOAaVx6LyqTAYBUYnwwWr4cHhsqReCt5_mIdpf_uR43jC-bS9bX2vDAuJwnQfn7ov3eyXkku0e5D9-oMzoUxKj9Dc0J0FZkgS5GUBPNxkKGqZuC8vEQGIT2KqvzCDEjTXf', '2020-07-28 11:24:16', '2020-07-05 01:48:42'),
(18, '182bb50673695dc1', 'onc', 'xiaomi', 'Xiaomi', 'Redmi 7', 'onc', 'unknown', 'qcom', '28', 'ewM0fAMgQEil7N3a-9JQLH:APA91bG6eghEyLEvMVEXjKU3rxb_x1SK_FUKkvMrn7PfXhupGYfJoG74bQONlXRTg5_sf02DPawdP9MDGpYZ614IBTQhaiN83morYZTocXDnUIWtKajd9PhC7LUUv1zyKMRDCj0JDgRw', '2020-09-01 15:03:42', '2020-09-01 15:03:42'),
(19, 'a4e52ccd879bbd20', 'kenzo', 'Xiaomi', 'Xiaomi', 'Redmi Note 3', 'kenzo', '52df4758', 'qcom', '23', 'd9U9LN51SuKqoAwXiJL-JG:APA91bGDruY6nbemUgMvnoYfoykMpImKqDutP9rrhH0iqvSoYUGeRX5Z6a2_AcLdveihdHfdAv2WcqXa3460lfy2ltVMHV41C6j-DjHa7K6Q9jxHNTGgs6DS4Da_eMgB7LIoe0jKQuLH', '2021-01-11 12:12:08', '2020-09-01 15:05:20'),
(20, 'ss', 'hwbgo', 'HUAWEI', 'HUAWEI', 'BGO-DL09', 'BGO', 'T6RFG16B16000762', 'sc8830', '23', 'gggg', '2021-01-05 10:50:00', '2021-01-05 10:50:00'),
(21, 'ss', 'hwbgo', 'HUAWEI', 'HUAWEI', 'BGO-DL09', 'BGO', 'T6RFG16B16000762', 'sc8830', '23', 'gggg', '2021-01-05 18:36:26', '2021-01-05 18:36:26'),
(22, 'ss', 'hwbgo', 'HUAWEI', 'HUAWEI', 'BGO-DL09', 'BGO', 'T6RFG16B16000762', 'sc8830', '23', 'gggg', '2021-01-05 19:06:13', '2021-01-05 19:06:13'),
(23, 'e9f779c1a4c3ddfe', 'ginkgo', 'xiaomi', 'Xiaomi', 'Redmi Note 8', 'ginkgo', 'unknown', 'qcom', '29', 'eTsnpoI6Rgic7ibJ8Yz8cT:APA91bEQqznrmAGNPC_dfSdx2SQkI2KWOGfA47Y83Xjr3uCq66Sn2PQ-OB7ZlFBD2esV687ibqHYC-AshNIehdeAfrcRnbYJNUxXSkC-9UVi0UMVe746p08JjW9vQ1x4UtCNg_qHhYqT', '2021-02-05 17:47:59', '2021-01-05 19:43:33'),
(24, 'ff9f14f6d7e99fc4', 'onc', 'xiaomi', 'Xiaomi', 'Redmi 7', 'onc', 'unknown', 'qcom', '29', 'c-QWQuyQQ6OQzhBvXOzk8-:APA91bEnyJCJkXEZxHi3RMumgfsVnLxZvFXBOtD21dnd5qMVgOgrxgnUgPip9LRXs4KFN2gElNZEy20YsbDVaOdPADOfbkUjaxzXh1f0E4rHXoHtGt-Br0AkXrUNkxnskgBcc4Zvji1y', '2021-01-05 20:11:48', '2021-01-05 20:11:48'),
(25, 'ed4c3a90fdd39070', 'joyeuse', 'Redmi', 'Xiaomi', 'Redmi Note 9 Pro', 'joyeuse_id', 'unknown', 'qcom', '29', 'd7EpizvvQ5awaAyrE9qgd1:APA91bHsv3wSsSjBe8jireBqnaLXQMYLsMbUmCoz_HV7iRfiVzTCAWqET3MV2lqlx4zBBbQnp8-8nWitHSX2472la1aIcbLp1oXvlFS-OkqGcOT0dNvQ9X2ddbOBEXPL2wQnJerkg1sQ', '2021-02-07 12:54:59', '2021-02-07 12:54:59'),
(26, '761e6ebd23ef8dff', 'hwbgo', 'HUAWEI', 'HUAWEI', 'BGO-DL09', 'BGO', 'T6RFG16B16000762', 'sc8830', '23', 'fZ0BMeTQEu4:APA91bF13iUq7L4osGW7aqgJ9X55MIrefQ-R8bqJqwFlY8Y4eiZ7H5W3Zbc2aK2Eqirc1WCttgLFm5l7OqOPkPWk3RAWR8M-yU9_ABXUWesIgiA5egTn87Hp5H-lbyeJ1G2XE1SlwuLR', '2021-05-09 04:52:22', '2021-05-09 04:52:22'),
(27, '761e6ebd23ef8dff', 'hwbgo', 'HUAWEI', 'HUAWEI', 'BGO-DL09', 'BGO', 'T6RFG16B16000762', 'sc8830', '23', 'fZ0BMeTQEu4:APA91bF13iUq7L4osGW7aqgJ9X55MIrefQ-R8bqJqwFlY8Y4eiZ7H5W3Zbc2aK2Eqirc1WCttgLFm5l7OqOPkPWk3RAWR8M-yU9_ABXUWesIgiA5egTn87Hp5H-lbyeJ1G2XE1SlwuLR', '2021-05-10 15:09:41', '2021-05-10 15:09:41'),
(28, '761e6ebd23ef8dff', 'hwbgo', 'HUAWEI', 'HUAWEI', 'BGO-DL09', 'BGO', 'T6RFG16B16000762', 'sc8830', '23', 'fZ0BMeTQEu4:APA91bF13iUq7L4osGW7aqgJ9X55MIrefQ-R8bqJqwFlY8Y4eiZ7H5W3Zbc2aK2Eqirc1WCttgLFm5l7OqOPkPWk3RAWR8M-yU9_ABXUWesIgiA5egTn87Hp5H-lbyeJ1G2XE1SlwuLR', '2021-06-01 08:48:57', '2021-06-01 08:48:57');

-- --------------------------------------------------------

--
-- Table structure for table `fee_desainer`
--

CREATE TABLE `fee_desainer` (
  `id` int(11) NOT NULL,
  `desainer_design_id` int(11) DEFAULT NULL,
  `desainer_eksekusi_id` int(11) DEFAULT NULL,
  `pesanan_id` int(11) NOT NULL,
  `fee_design` double DEFAULT NULL,
  `persentase_eksekusi` double DEFAULT NULL,
  `fee_eksekusi` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_desainer`
--

INSERT INTO `fee_desainer` (`id`, `desainer_design_id`, `desainer_eksekusi_id`, `pesanan_id`, `fee_design`, `persentase_eksekusi`, `fee_eksekusi`, `created_at`, `updated_at`) VALUES
(9, 38, 28, 7, 50000, 1, 0, '2021-05-20 11:41:59', '2021-05-20 11:45:47'),
(10, 28, 38, 10, 50000, 2, 0, '2021-05-20 11:43:04', '2021-05-20 11:43:33');

-- --------------------------------------------------------

--
-- Table structure for table `gaji_desainer`
--

CREATE TABLE `gaji_desainer` (
  `id` int(11) NOT NULL,
  `desainer_id` int(11) NOT NULL,
  `gaji_pokok` double NOT NULL,
  `gaji_tunjangan` double NOT NULL,
  `bulan` varchar(20) NOT NULL,
  `tahun` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gaji_desainer`
--

INSERT INTO `gaji_desainer` (`id`, `desainer_id`, `gaji_pokok`, `gaji_tunjangan`, `bulan`, `tahun`, `created_at`, `updated_at`) VALUES
(1, 38, 50000, 60000, '05', '2021', '2021-05-03 16:43:00', '2021-05-03 16:43:00'),
(2, 28, 2000000, 20000, '05', '2021', '2021-05-20 11:58:56', '2021-05-20 11:58:56'),
(3, 28, 5000000, 60000, '06', '2021', '2021-06-01 08:53:57', '2021-06-01 08:53:57');

-- --------------------------------------------------------

--
-- Table structure for table `harga`
--

CREATE TABLE `harga` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kategori` varchar(1) NOT NULL,
  `harga_pokok_granit` int(11) NOT NULL,
  `harga_pokok_keramik` int(11) NOT NULL,
  `harga_jual_granit` int(11) NOT NULL,
  `harga_jual_keramik` int(11) NOT NULL,
  `note_granit` varchar(100) NOT NULL,
  `note_keramik` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga`
--

INSERT INTO `harga` (`id`, `nama`, `kategori`, `harga_pokok_granit`, `harga_pokok_keramik`, `harga_jual_granit`, `harga_jual_keramik`, `note_granit`, `note_keramik`, `created_at`, `updated_at`) VALUES
(2, 'Pintu Bawah', '1', 880000, 640000, 1400000, 1100000, 'Ukuran 60x60', 'Ukuran 50x50', '2020-06-26 14:11:13', '2020-06-26 21:11:13'),
(3, 'Sekat Vertikal', '1', 320000, 320000, 550000, 550000, '+ Ambalan', '+ Ambalan Granit', '2020-06-26 14:13:16', '2020-06-26 21:13:16'),
(4, 'Rak Piring 2 Susun + Nampan', '1', 450000, 450000, 550000, 550000, '', '', '2020-06-26 14:13:16', '2020-06-26 21:13:16'),
(5, 'Rak Piring 3 Susun + Nampan', '1', 500000, 500000, 600000, 600000, '', '', '2020-06-26 14:15:38', '2020-06-26 21:15:38'),
(6, 'Lacik Tarik Drawer (Fiber Semen)', '1', 260000, 260000, 440000, 440000, '', '', '2020-06-26 14:15:38', '2020-06-26 21:15:38'),
(7, 'Nampan Sendok', '1', 100000, 100000, 130000, 130000, '', '', '2020-06-26 14:18:16', '2020-06-26 21:18:16'),
(8, 'Kabinet Bawah (60x80)', '1', 1880000, 1650000, 3000000, 2300000, 'Full Granit (60x80)', 'Granit Keramik (60x80)', '2020-06-26 14:18:16', '2020-06-26 21:18:16'),
(9, 'Kabinet Atas', '2', 1800000, 1440000, 2700000, 2200000, '(30x70)', '(40x80)', '2020-06-26 14:25:03', '2020-06-26 21:25:03'),
(10, 'Kabinet Atas', '2', 1880000, 1120000, 3000000, 1900000, '(40x70)', '(40x60)', '2020-06-26 14:25:03', '2020-06-26 21:25:03'),
(11, 'Sekat Vertikal Lemari Atas', '2', 200000, 160000, 300000, 250000, '', '', '2020-06-26 14:27:27', '2020-06-26 21:27:27'),
(12, 'Rak Piring Gantung Melamin', '2', 120000, 120000, 275000, 275000, '', '', '2020-06-26 14:27:27', '2020-06-26 21:27:27'),
(13, 'Rak Piring Gelas Gantung Melamin + Nampan', '2', 250000, 350000, 400000, 400000, '', '', '2020-06-26 14:30:34', '2020-06-26 21:30:34'),
(14, 'Rak Piring Gantung Stainless', '2', 250000, 350000, 550000, 550000, '', '', '2020-06-26 14:30:34', '2020-06-26 21:30:34'),
(16, 'Lampu Downlight', '2', 160000, 160000, 330000, 330000, '', '', '2020-06-26 14:37:10', '2020-06-26 21:37:10'),
(17, 'Lampu LED Strip Bawah Kabinet', '2', 200000, 200000, 300000, 330000, '', '', '2020-06-26 14:37:10', '2020-06-26 21:37:10'),
(18, 'Rak Terbuka', '2', 320000, 300000, 550000, 450000, 'Granit (Lebar 30 cm)', 'Keramik (Lebar 40 cm)', '2020-06-26 14:37:10', '2020-06-26 21:37:10'),
(19, 'Meja Island (Lebar 60 cm)', '3', 880000, 880000, 1500000, 1500000, '', '', '2020-06-26 14:41:43', '2020-06-26 21:41:43'),
(20, 'Top Table Kompor Tanam', '3', 880000, 880000, 1500000, 1500000, '', '', '2020-06-26 14:41:43', '2020-06-26 21:41:43'),
(21, 'Minibar', '3', 4500000, 5500000, 7000000, 5500000, 'Full Granit', 'Top Table Granit + Full Keramik', '2020-06-26 14:41:43', '2020-06-26 21:41:43'),
(22, 'Pintu Kaca List Aluminium + Rel Hidrolis', '4', 300000, 300000, 465000, 465000, 'Full Granit', 'Top Table Granit + Full Keramik', '2020-06-26 14:45:36', '2020-06-26 21:45:36'),
(23, 'Desain 3D', '4', 0, 0, 300000, 300000, 'Full Granit', 'Top Table Granit + Full Keramik', '2020-06-26 14:45:36', '2020-06-26 21:45:36'),
(24, 'Ongkos Pasang Hood', '2', 75000, 75000, 150000, 150000, 'Full Granit', 'Top Table Granit + Full Keramik', '2020-06-26 14:45:36', '2020-06-26 21:45:36'),
(25, 'Ongkos Pasang Kompor Tanam', '1', 160000, 160000, 200000, 200000, '', '', '2020-06-26 14:45:36', '2020-06-26 21:45:36'),
(26, 'Ongkos Kirim', '4', 0, 0, 150000, 150000, 'Full Granit', 'Top Table Granit + Full Keramik', '2020-06-26 14:45:36', '2020-06-26 21:45:36'),
(27, 'Kabinet Bawah (60x80) + Badukan', '1', 2500000, 2250000, 3700000, 3000000, '', '', '2020-06-29 12:10:01', '2020-06-29 19:10:01'),
(28, 'Lubang Ventilasi Aluminium', '1', 50000, 50000, 75000, 75000, '', '', '2020-06-29 12:10:01', '2020-06-29 19:10:01'),
(29, 'Minibar Half Version (Kaki Bisa Masuk Bawah Meja)', '3', 3500000, 3000000, 6000000, 4500000, '', '', '2020-06-29 12:23:13', '2020-06-29 19:23:13'),
(30, 'Pintu Kaca List', '4', 200000, 200000, 400000, 400000, 'Granit', 'Keramik', '2020-06-29 12:36:27', '2020-06-29 19:36:27'),
(31, 'Keramik Dinding (Antara meja dan kabinet )\r\n', '4', 250000, 250000, 400000, 400000, '', '', '2020-06-29 12:41:11', '2020-06-29 19:41:11'),
(32, 'Ongkos Bongkar Kitchenset Lama\r\n', '4', 0, 0, 0, 0, '', '', '2020-06-29 12:42:02', '2020-06-29 19:42:02'),
(33, 'Testing harga Baru', '3', 1000000, 1500000, 1200000, 1600000, '-', '-', '2021-01-11 20:45:31', '2021-01-11 20:45:31');

-- --------------------------------------------------------

--
-- Table structure for table `harga_maintenance`
--

CREATE TABLE `harga_maintenance` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga_maintenance`
--

INSERT INTO `harga_maintenance` (`id`, `nama`, `harga`, `created_at`, `updated_at`) VALUES
(1, 'Lem Plat Pintu', 100000, '2021-04-26 07:22:54', '0000-00-00 00:00:00'),
(2, 'Ganti Magnet', 50000, '2021-04-26 07:22:54', '0000-00-00 00:00:00'),
(3, 'Ganti Engsel', 50000, '2021-04-26 07:22:54', '0000-00-00 00:00:00'),
(4, 'Cat Ulang Pintu', 30000, '2021-04-26 07:22:54', '0000-00-00 00:00:00'),
(5, 'Ganti Rangka Laci', 350000, '2021-04-26 07:22:54', '0000-00-00 00:00:00'),
(6, 'Ganti Rel Laci', 150000, '2021-04-26 07:22:54', '0000-00-00 00:00:00'),
(7, 'Ganti Pintu Granit', 400000, '2021-04-26 07:22:54', '0000-00-00 00:00:00'),
(8, 'Ganti Pintu Keramik', 300000, '2021-04-26 07:22:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Stand-in structure for view `harga_view`
-- (See below for the actual view)
--
CREATE TABLE `harga_view` (
`id` int(11)
,`nama_harga` varchar(100)
,`id_kategori` varchar(1)
,`kategori` varchar(100)
,`harga_pokok_granit` int(11)
,`harga_pokok_keramik` int(11)
,`harga_jual_granit` int(11)
,`harga_jual_keramik` int(11)
,`note_granit` varchar(100)
,`note_keramik` varchar(100)
,`created_at` timestamp
,`updated_at` datetime
);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pembayaran`
--

CREATE TABLE `jenis_pembayaran` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `tipe` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pembayaran`
--

INSERT INTO `jenis_pembayaran` (`id`, `nama`, `tipe`, `created_at`) VALUES
(1, 'GAJI', 2, '2020-08-21 07:33:18'),
(2, 'DOWNPAYMENT', 1, '2020-07-28 00:54:09'),
(3, 'PELUNASAN', 1, '2020-07-28 00:54:09'),
(4, 'TANDA JADI', 1, '2020-07-28 00:54:09'),
(5, 'SYSTEM', 2, '2020-08-21 07:33:22'),
(6, 'PRODUKSI', 2, '2020-08-21 07:33:24');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_produk`
--

CREATE TABLE `kategori_produk` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_produk`
--

INSERT INTO `kategori_produk` (`id`, `nama`, `created_at`) VALUES
(1, 'AREA BAWAH', '2020-06-26 14:21:37'),
(2, 'AREA ATAS', '2020-06-26 14:21:37'),
(3, 'MEJA TAMBAHAN', '2020-06-26 14:21:37'),
(4, 'PEKERJAAN TAMBAHAN', '2020-06-26 14:21:37');

-- --------------------------------------------------------

--
-- Table structure for table `komplain`
--

CREATE TABLE `komplain` (
  `id` int(11) NOT NULL,
  `tgl_komplain` date NOT NULL,
  `tgl_jatuh_tempo` date NOT NULL,
  `tgl_acc_konsumen` date DEFAULT NULL,
  `description` text NOT NULL,
  `pesanan_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` char(5) NOT NULL,
  `flow_id` int(11) NOT NULL,
  `mitra_pemasang_kusen` int(11) DEFAULT NULL,
  `mitra_pemasang_finish` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `catatan` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komplain`
--

INSERT INTO `komplain` (`id`, `tgl_komplain`, `tgl_jatuh_tempo`, `tgl_acc_konsumen`, `description`, `pesanan_id`, `user_id`, `status`, `flow_id`, `mitra_pemasang_kusen`, `mitra_pemasang_finish`, `created_at`, `updated_at`, `is_deleted`, `catatan`) VALUES
(1, '2021-04-29', '2021-05-29', NULL, 'TEs', 2, 35, 'P', 4, 33, NULL, '2021-04-23 11:58:18', '2021-04-23 11:58:18', 0, 'hhhhh'),
(2, '2021-04-30', '2021-05-30', NULL, 'tes', 5, 35, 'P', 4, 33, NULL, '2021-04-23 12:04:31', '2021-04-23 12:04:31', 0, 'ff');

-- --------------------------------------------------------

--
-- Table structure for table `komplain_files`
--

CREATE TABLE `komplain_files` (
  `id` int(11) NOT NULL,
  `komplain_id` int(11) NOT NULL,
  `tipe` char(5) NOT NULL,
  `nama_file` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komplain_files`
--

INSERT INTO `komplain_files` (`id`, `komplain_id`, `tipe`, `nama_file`, `created_at`, `updated_at`) VALUES
(1, 1, 'OM', '20210318_170018_0000-607efd6fd7cba.png', '2021-04-20 16:12:31', '2021-04-20 16:12:31'),
(2, 2, 'OM', '20210413_132148_0000-6082b6bc88724.png', '2021-04-23 11:59:56', '2021-04-23 11:59:56');

-- --------------------------------------------------------

--
-- Table structure for table `komplain_log`
--

CREATE TABLE `komplain_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `komplain_id` int(11) NOT NULL,
  `aktivitas` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komplain_log`
--

INSERT INTO `komplain_log` (`id`, `user_id`, `komplain_id`, `aktivitas`, `created_at`) VALUES
(1, 27, 6, 'Komplain dihapus', '2020-06-23 07:17:41'),
(2, 27, 7, 'Komplain dihapus', '2020-06-24 09:00:43'),
(3, 27, 9, 'Komplain dihapus', '2020-06-24 09:02:16'),
(4, 27, 10, 'menginputkan data komplain \"engsel lepas\" dari kode order \"COMPLAIN 10 JULI\"', '2020-07-10 01:19:34'),
(5, 27, 11, 'menginputkan data komplain \"pintu lepas, udah dr maret bel...\" dari kode order \"LAWANG\"', '2020-07-14 07:33:22'),
(6, 27, 12, 'menginputkan data komplain \"lem2an plat besinlepas dr pint...\" dari kode order \"PKMD145\"', '2020-07-14 12:16:47'),
(7, 27, 13, 'menginputkan data komplain \"handle pintu lepas bautnya, ti...\" dari kode order \"PGMD208\"', '2020-07-15 00:30:38'),
(8, 27, 14, 'menginputkan data komplain \"lem pintu lepas udah 5 bulan b...\" dari kode order \"PKMD104\"', '2020-07-17 02:12:17'),
(9, 37, 12, 'mengajukan jadwal pasang kusen pada komplain \"lem2an plat besinlepas dr pint...\" ', '2020-07-23 02:19:02'),
(10, 37, 13, 'mengajukan jadwal pasang kusen pada komplain \"handle pintu lepas bautnya, ti...\" ', '2020-07-23 03:04:10'),
(11, 28, 15, 'menginputkan data komplain \"Pintu lepas dari rangka dan pe...\" dari kode order \"KSGK32\"', '2020-08-12 02:04:17'),
(12, 29, 14, 'mengajukan jadwal penanganan pada komplain \"lem pintu lepas udah 5 bulan b...\" ', '2020-08-15 07:26:53'),
(13, 29, 15, 'mengajukan jadwal penanganan pada komplain \"Pintu lepas dari rangka dan pe...\" ', '2020-08-15 07:27:15'),
(14, 27, 14, 'menerima pengajuan jadwal penanganan pada komplain \"lem pintu lepas udah 5 bulan belum diperbaiki\" ', '2020-08-16 02:09:35'),
(15, 27, 16, 'menginputkan data komplain \"sinc lepas dr meja, sblmnya sd...\" dari kode order \"KSGK REVI\"', '2020-08-16 05:47:37'),
(16, 27, 13, 'Komplain dihapus', '2020-08-16 11:44:08'),
(17, 27, 15, 'menolak jadwal penanganan pada komplain \"Pintu lepas dari rangka dan pecah\" ', '2020-08-22 12:21:04'),
(18, 29, 15, 'menerima perubahan jadwal penanganan pada komplain \"Pintu lepas dari rangka dan pecah\" ', '2020-08-22 12:22:23'),
(19, 35, 17, 'menginputkan data komplain \"tes komplaim\" dari kode order \"TESTINGNEW\"', '2021-01-13 16:28:32'),
(20, 35, 18, 'menginputkan data komplain \"Testing Komplain\" dari kode order \"KOMPLAINTANPAKONSUME\"', '2021-02-14 11:32:16'),
(21, 35, 19, 'menginputkan data komplain \"dddd\" dari kode order \"HAHAHAHA\"', '2021-02-14 11:35:36'),
(22, 35, 20, 'menginputkan data komplain \"asdassa\" dari kode order \"TESTINGKOMPLAIN\"', '2021-02-14 11:36:53'),
(23, 35, 22, 'menginputkan data komplain \"asdsadsad\" dari kode order \"TTTTTT\"', '2021-02-14 11:40:28'),
(24, 35, 23, 'menginputkan data komplain \"Work for me\" dari kode order \"PKMD176\"', '2021-02-14 11:45:25'),
(25, 30, 17, 'mengajukan jadwal penanganan pada komplain \"tes komplaim\" ', '2021-02-16 14:18:51'),
(26, 35, 17, 'menolak jadwal penanganan pada komplain \"tes komplaim\" ', '2021-02-18 00:13:15'),
(27, 30, 17, 'menolak perubahan jadwal penanganan pada komplain \"tes komplaim\" ', '2021-02-18 00:15:00'),
(28, 35, 17, 'menolak jadwal penanganan pada komplain \"tes komplaim\" ', '2021-02-18 00:15:31'),
(29, 30, 17, 'menerima perubahan jadwal penanganan pada komplain \"tes komplaim\" ', '2021-02-18 00:19:41'),
(30, 35, 1, 'melakukan reschedule jadwal penanganan pada komplain \"\" ', '2021-02-18 00:45:58'),
(31, 35, 24, 'menginputkan data komplain \"Tes\" dari kode order \"TESTKOMPLAIN\"', '2021-02-18 00:47:47'),
(32, 30, 24, 'mengajukan jadwal penanganan pada komplain \"Tes\" ', '2021-02-18 00:48:15'),
(33, 35, 24, 'menerima pengajuan jadwal penanganan pada komplain \"Tes\" ', '2021-02-18 00:48:55'),
(34, 35, 1, 'melakukan reschedule jadwal penanganan pada komplain \"\" ', '2021-02-18 00:50:34'),
(35, 30, 24, 'menerima perubahan jadwal penanganan pada komplain \"Tes\" ', '2021-02-18 00:51:04'),
(36, 30, 17, 'menolak perubahan jadwal penanganan pada komplain \"tes komplaim\" ', '2021-02-19 10:26:41'),
(37, 35, 25, 'menginputkan data komplain \"TEs\" dari kode order \"TESTEDO0\"', '2021-02-19 10:36:11'),
(38, 30, 25, 'mengajukan jadwal penanganan pada komplain \"TEs\" ', '2021-02-19 10:39:03'),
(39, 35, 25, 'menerima pengajuan jadwal penanganan pada komplain \"TEs\" ', '2021-02-19 10:46:45'),
(40, 35, 26, 'menginputkan data komplain \"ggg\" dari kode order \"TESTEDO1\"', '2021-02-19 10:49:38'),
(41, 30, 26, 'mengajukan jadwal penanganan pada komplain \"ggg\" ', '2021-02-19 11:01:55'),
(42, 35, 26, 'menerima pengajuan jadwal penanganan pada komplain \"ggg\" ', '2021-02-19 11:04:28'),
(43, 35, 27, 'menginputkan data komplain \"25\" dari kode order \"TESORDER25\"', '2021-02-19 11:22:26'),
(44, 30, 27, 'mengajukan jadwal penanganan pada komplain \"25\" ', '2021-02-19 11:29:36'),
(45, 35, 27, 'menerima pengajuan jadwal penanganan pada komplain \"25\" ', '2021-02-19 11:32:38'),
(46, 35, 1, 'melakukan reschedule jadwal penanganan pada komplain \"\" ', '2021-02-19 11:56:09'),
(47, 30, 27, 'menolak perubahan jadwal penanganan pada komplain \"25\" ', '2021-02-19 11:56:57'),
(48, 35, 1, 'menginputkan data komplain \"Tes\" dari kode order \"TES1\"', '2021-02-19 12:07:13'),
(49, 30, 1, 'mengajukan jadwal penanganan pada komplain \"Tes\" ', '2021-02-19 12:07:49'),
(50, 35, 1, 'menerima pengajuan jadwal penanganan pada komplain \"Tes\" ', '2021-02-19 12:08:26'),
(51, 30, 1, 'mengajukan jadwal kunjungan lanjutan pada \"Tes\" ', '2021-02-19 12:11:05'),
(52, 35, 2, 'menginputkan data komplain \"Coba Dari pesanan\" dari kode order \"TESORDERBARU\"', '2021-02-19 12:13:16'),
(53, 30, 2, 'mengajukan jadwal penanganan pada komplain \"Coba Dari pesanan\" ', '2021-02-19 12:14:02'),
(54, 35, 2, 'menolak jadwal penanganan pada komplain \"Coba Dari pesanan\" ', '2021-02-19 12:18:29'),
(55, 30, 2, 'menolak perubahan jadwal penanganan pada komplain \"Coba Dari pesanan\" ', '2021-02-19 12:19:08'),
(56, 35, 2, 'menerima pengajuan jadwal penanganan pada komplain \"Coba Dari pesanan\" ', '2021-02-19 12:19:55'),
(57, 35, 1, 'menginputkan data komplain \"A\" dari kode order \"A\"', '2021-02-19 12:24:20'),
(58, 30, 1, 'mengajukan jadwal penanganan pada komplain \"A\" ', '2021-02-19 12:24:39'),
(59, 35, 1, 'menerima pengajuan jadwal penanganan pada komplain \"A\" ', '2021-02-19 12:25:18'),
(60, 35, 1, 'melakukan reschedule jadwal penanganan pada komplain \"A\" ', '2021-02-19 12:25:58'),
(61, 30, 1, 'menerima perubahan jadwal penanganan pada komplain \"A\" ', '2021-02-19 12:27:00'),
(62, 35, 1, 'menginputkan data komplain \"A\" dari kode order \"A\"', '2021-02-19 12:36:19'),
(63, 30, 1, 'mengajukan jadwal penanganan pada komplain \"A\" ', '2021-02-19 12:36:48'),
(64, 35, 1, 'menerima pengajuan jadwal penanganan pada komplain \"A\" ', '2021-02-19 12:37:26'),
(65, 35, 1, 'melakukan reschedule jadwal penanganan pada komplain \"A\" ', '2021-02-19 12:38:24'),
(66, 30, 1, 'menerima perubahan jadwal penanganan pada komplain \"A\" ', '2021-02-19 12:39:05'),
(67, 30, 1, 'mengajukan jadwal kunjungan lanjutan pada \"A\" ', '2021-02-19 13:44:01'),
(68, 35, 1, 'menolak jadwal pemasangan pada komplain \"A\" ', '2021-02-19 13:44:45'),
(69, 35, 2, 'menginputkan data komplain \"tes komplain\" dari kode order \"TESTINGKOMPLAINAWAL\"', '2021-02-20 08:33:11'),
(70, 30, 2, 'mengajukan jadwal penanganan pada komplain \"tes komplain\" ', '2021-02-20 08:35:35'),
(71, 35, 2, 'menolak jadwal penanganan pada komplain \"tes komplain\" ', '2021-02-20 08:37:42'),
(72, 30, 2, 'menolak perubahan jadwal penanganan pada komplain \"tes komplain\" ', '2021-02-20 08:37:59'),
(73, 35, 2, 'menerima pengajuan jadwal penanganan pada komplain \"tes komplain\" ', '2021-02-20 08:38:10'),
(74, 35, 1, 'melakukan reschedule jadwal penanganan pada komplain \"A\" ', '2021-02-20 08:38:27'),
(75, 30, 2, 'menerima perubahan jadwal penanganan pada komplain \"tes komplain\" ', '2021-02-20 08:38:44'),
(76, 35, 3, 'menginputkan data komplain \"Komplain\" dari kode order \"TESKOMPLAINDARIAWAL2\"', '2021-02-20 09:12:25'),
(77, 30, 3, 'mengajukan jadwal penanganan pada komplain \"Komplain\" ', '2021-02-20 09:13:44'),
(78, 35, 3, 'menerima pengajuan jadwal penanganan pada komplain \"Komplain\" ', '2021-02-20 09:14:46'),
(79, 35, 4, 'menginputkan data komplain \"kkkk\" dari kode order \"TESTKOMPLAIN3\"', '2021-02-20 10:19:47'),
(80, 30, 4, 'mengajukan jadwal penanganan pada komplain \"kkkk\" ', '2021-02-20 10:20:13'),
(81, 999, 4, 'menolak jadwal penanganan pada komplain \"kkkk\" ', '2021-02-20 11:06:08'),
(82, 30, 4, 'menolak perubahan jadwal penanganan pada komplain \"kkkk\" ', '2021-02-20 11:13:04'),
(83, 999, 4, 'menolak jadwal penanganan pada komplain \"kkkk\" ', '2021-02-20 11:17:42'),
(84, 30, 4, 'menolak perubahan jadwal penanganan pada komplain \"kkkk\" ', '2021-02-20 11:21:04'),
(85, 999, 4, 'menolak jadwal penanganan pada komplain \"kkkk\" ', '2021-02-20 11:21:39'),
(86, 35, 5, 'menginputkan data komplain \"TEs\" dari kode order \"TESBARU\"', '2021-03-31 10:18:35'),
(87, 30, 5, 'mengajukan jadwal penanganan pada komplain \"TEs\" ', '2021-03-31 10:19:11'),
(88, 999, 5, 'menerima pengajuan jadwal penanganan pada komplain \"TEs\" ', '2021-03-31 10:21:24'),
(89, 999, 5, 'menerima pengajuan jadwal penanganan pada komplain \"TEs\" ', '2021-03-31 10:26:09'),
(90, 35, 6, 'menginputkan data komplain \"tes\" dari kode order \"TESTKOMPLAIN22222\"', '2021-04-13 03:28:42'),
(91, 30, 6, 'mengajukan jadwal penanganan pada komplain \"tes\" ', '2021-04-13 03:30:06'),
(92, 35, 6, 'menerima pengajuan jadwal penanganan pada komplain \"tes\" ', '2021-04-13 03:31:50'),
(93, 35, 7, 'menginputkan data komplain \"TEs\" dari kode order \"TESTINGKOMPLAINDARIP\"', '2021-04-15 11:06:45'),
(94, 30, 7, 'mengajukan jadwal penanganan pada komplain \"TEs\" ', '2021-04-15 11:07:21'),
(95, 30, 7, 'mengajukan jadwal penanganan pada komplain \"TEs\" ', '2021-04-15 11:12:00'),
(96, 35, 7, 'menolak jadwal penanganan pada komplain \"TEs\" ', '2021-04-15 11:21:32'),
(97, 33, 7, 'menolak perubahan jadwal penanganan pada komplain \"TEs\" ', '2021-04-15 11:31:26'),
(98, 35, 7, 'menerima pengajuan jadwal penanganan pada komplain \"TEs\" ', '2021-04-15 11:32:07'),
(99, 35, 8, 'menginputkan data komplain \"TEs\" dari kode order \"TESTCATATANKOMPLAIN\"', '2021-04-19 13:55:28'),
(100, 35, 1, 'menginputkan data komplain \"TEs\" dari kode order \"TESKOMPLAIN\"', '2021-04-20 16:12:31'),
(101, 30, 1, 'mengajukan jadwal penanganan pada komplain \"TEs\" ', '2021-04-23 11:57:36'),
(102, 999, 1, 'menerima pengajuan jadwal penanganan pada komplain \"TEs\" ', '2021-04-23 11:58:18'),
(103, 35, 2, 'menginputkan data komplain \"tes\" dari kode order \"TESKOMPLAIN2\"', '2021-04-23 11:59:56'),
(104, 30, 2, 'mengajukan jadwal penanganan pada komplain \"tes\" ', '2021-04-23 12:00:44'),
(105, 999, 2, 'menerima pengajuan jadwal penanganan pada komplain \"tes\" ', '2021-04-23 12:04:31');

-- --------------------------------------------------------

--
-- Table structure for table `komplain_usulan`
--

CREATE TABLE `komplain_usulan` (
  `id` int(11) NOT NULL,
  `jadwal_usulan` date DEFAULT NULL,
  `jam_usulan` time DEFAULT NULL,
  `status` char(2) NOT NULL,
  `rencana_kerja_id` int(11) NOT NULL,
  `flow_id` int(11) NOT NULL,
  `komplain_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `rescheduler_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komplain_usulan`
--

INSERT INTO `komplain_usulan` (`id`, `jadwal_usulan`, `jam_usulan`, `status`, `rencana_kerja_id`, `flow_id`, `komplain_id`, `created_at`, `updated_at`, `user_id`, `approver_id`, `rescheduler_id`) VALUES
(1, '2021-04-24', NULL, 'P', 1, 2, 1, '2021-04-23 11:57:35', '2021-04-23 11:57:35', 30, NULL, NULL),
(2, '2021-04-30', NULL, 'P', 1, 2, 2, '2021-04-23 12:00:44', '2021-04-23 12:00:44', 30, NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `komplain_view`
-- (See below for the actual view)
--
CREATE TABLE `komplain_view` (
`id` int(11)
,`tgl_komplain` date
,`tgl_jatuh_tempo` date
,`tgl_acc_konsumen` date
,`description` text
,`pesanan_id` int(11)
,`user_id` int(11)
,`status` char(5)
,`flow_id` int(11)
,`mitra_pemasang_kusen` int(11)
,`mitra_pemasang_finish` int(11)
,`created_at` timestamp
,`updated_at` timestamp
,`is_deleted` int(11)
,`kode_order` varchar(20)
,`tgl_order_masuk` date
,`tanggal_pasang_kusen_pesanan` date
,`tanggal_pasang_finish_pesanan` date
,`tgl_acc_konsumen_pesanan` date
,`nama_pemasang_kusen` varchar(100)
,`nama_pemasang_finish` varchar(100)
,`jam_pasang_kusen` time
,`tanggal_pasang_kusen` date
,`jam_pasang_finish` date
,`tanggal_pasang_finish` date
,`flow_name` varchar(100)
,`rencana_kerja_id` int(11)
,`rencana_kerja_nama` varchar(50)
,`konsumen_id` int(11)
,`nama_konsumen` varchar(100)
,`alamat_konsumen` text
,`no_hp_konsumen` varchar(15)
,`catatan` text
);

-- --------------------------------------------------------

--
-- Table structure for table `konsumen`
--

CREATE TABLE `konsumen` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konsumen`
--

INSERT INTO `konsumen` (`id`, `nama`, `alamat`, `no_hp`, `created_at`, `updated_at`) VALUES
(1, 'Edo', ' aaaa', '545646', '2021-04-20 15:58:20', '2021-04-20 15:58:20'),
(2, 'dsfdsfdsfdsfds', 'fdfddsfdsfdsfds ', '65456456', '2021-04-20 16:12:31', '2021-04-20 16:12:31'),
(3, 'Edo', ' rrrr', '4335435', '2021-04-23 11:46:47', '2021-04-23 11:46:47'),
(4, 'Edo', ' sdfdsfds', '9324324324', '2021-04-23 11:51:00', '2021-04-23 11:51:00'),
(5, 'eDO', ' fsff', '435435435', '2021-04-23 11:59:56', '2021-04-23 11:59:56'),
(6, 'Test', 'Test', '5555', '2021-04-23 12:11:41', '2021-04-23 12:11:41'),
(7, 'Edo', 'fdssdfds ', '00990898', '2021-04-30 01:15:37', '2021-04-30 01:15:37'),
(8, 'Test', 'Test', '5555', '2021-05-10 11:43:37', '2021-05-10 11:43:38'),
(9, 'Test', 'Test', '5555', '2021-05-10 11:44:26', '2021-05-10 11:44:26'),
(10, 'Test', 'Test', '5555', '2021-05-10 11:45:17', '2021-05-10 11:45:17'),
(11, 'Test', 'Test', '5555', '2021-05-10 11:46:56', '2021-05-10 11:46:56'),
(12, 'Test', 'Test', '5555', '2021-05-10 11:48:51', '2021-05-10 11:48:51'),
(13, 'Test', 'Test', '5555', '2021-05-10 11:49:40', '2021-05-10 11:49:40'),
(14, 'Tutul', 'tttt ', '33333', '2021-05-24 08:52:17', '2021-05-24 08:52:17'),
(15, 'Edo', ' sdfdf', '33333', '2021-05-24 11:08:08', '2021-05-24 11:08:08'),
(16, 'Tesitgn Pengajuan', 'aaa', '0999777', '2021-06-01 08:40:12', '2021-06-01 08:40:12');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `reference_type` varchar(20) NOT NULL,
  `action` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `reference_id`, `message`, `reference_type`, `action`, `created_at`, `updated_at`) VALUES
(1, 1, 'menginputkan data komplain \"TEs\" dari kode order \"TESKOMPLAIN\"', 'KPPP', 'KC', '2021-04-20 16:12:31', '2021-04-20 16:12:31'),
(2, 1, 'mengajukan jadwal pasang kusen untuk kode order \"TES\" ', 'UPK', 'AJK', '2021-04-21 10:44:31', '2021-04-21 10:44:31'),
(3, 1, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TES\" ', 'CJK', 'CJK', '2021-04-23 11:36:16', '2021-04-23 11:36:16'),
(4, 3, 'menginputkan data konsumen dengan kode order \"TES2\"', 'PPP', 'PC', '2021-04-23 11:46:47', '2021-04-23 11:46:47'),
(5, 3, 'mengajukan jadwal pasang kusen untuk kode order \"TES2\" ', 'UPK', 'AJK', '2021-04-23 11:48:59', '2021-04-23 11:48:59'),
(6, 3, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TES2\" ', 'CJK', 'CJK', '2021-04-23 11:49:33', '2021-04-23 11:49:33'),
(7, 4, 'menginputkan data konsumen dengan kode order \"TES3\"', 'PPP', 'PC', '2021-04-23 11:51:00', '2021-04-23 11:51:00'),
(8, 4, 'mengajukan jadwal pasang kusen untuk kode order \"TES3\" ', 'UPK', 'AJK', '2021-04-23 11:51:29', '2021-04-23 11:51:29'),
(9, 4, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TES3\" ', 'CJK', 'CJK', '2021-04-23 11:53:51', '2021-04-23 11:53:51'),
(10, 1, 'mengajukan jadwal penanganan pada komplain \"TEs\" ', 'KUPK', 'AJK', '2021-04-23 11:57:36', '2021-04-23 11:57:36'),
(11, 1, 'menerima pengajuan jadwal penanganan pada komplain \"TEs\" ', 'KCJK', 'CJK', '2021-04-23 11:58:18', '2021-04-23 11:58:18'),
(12, 2, 'menginputkan data komplain \"tes\" dari kode order \"TESKOMPLAIN2\"', 'KPPP', 'KC', '2021-04-23 11:59:56', '2021-04-23 11:59:56'),
(13, 2, 'mengajukan jadwal penanganan pada komplain \"tes\" ', 'KUPK', 'AJK', '2021-04-23 12:00:44', '2021-04-23 12:00:44'),
(14, 2, 'menerima pengajuan jadwal penanganan pada komplain \"tes\" ', 'KCJK', 'CJK', '2021-04-23 12:04:31', '2021-04-23 12:04:31'),
(15, 6, 'menginputkan data konsumen dengan kode order \"TEST\"', 'PPP', 'PC', '2021-04-23 12:11:41', '2021-04-23 12:11:41'),
(16, 7, 'menginputkan data konsumen dengan kode order \"TESDESAINER\"', 'PPP', 'PC', '2021-04-30 01:15:37', '2021-04-30 01:15:37'),
(17, 7, 'mengajukan jadwal pasang kusen untuk kode order \"TESDESAINER\" ', 'UPK', 'AJK', '2021-05-04 02:24:51', '2021-05-04 02:24:51'),
(18, 7, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESDESAINER\" ', 'CJK', 'CJK', '2021-05-04 02:28:59', '2021-05-04 02:28:59'),
(19, 7, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESDESAINER\" ', 'CJK', 'CJK', '2021-05-04 02:44:01', '2021-05-04 02:44:01'),
(20, 10, 'menginputkan data konsumen dengan kode order \"TTTT\"', 'PPP', 'PC', '2021-05-10 11:49:40', '2021-05-10 11:49:40'),
(21, 11, 'menginputkan data konsumen dengan kode order \"TESFOTO\"', 'PPP', 'PC', '2021-05-24 08:52:18', '2021-05-24 08:52:18'),
(22, 12, 'menginputkan data konsumen dengan kode order \"TESFOTO2\"', 'PPP', 'PC', '2021-05-24 11:08:08', '2021-05-24 11:08:08'),
(23, 13, 'menginputkan data konsumen dengan kode order \"TEESTDOBLE\"', 'PPP', 'PC', '2021-06-01 08:40:12', '2021-06-01 08:40:12');

-- --------------------------------------------------------

--
-- Table structure for table `notification_senders`
--

CREATE TABLE `notification_senders` (
  `id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_senders`
--

INSERT INTO `notification_senders` (`id`, `notification_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 35, '2021-04-20 16:12:31', '2021-04-20 16:12:31'),
(2, 2, 30, '2021-04-21 10:44:31', '2021-04-21 10:44:31'),
(3, 3, 999, '2021-04-23 11:36:16', '2021-04-23 11:36:16'),
(4, 4, 35, '2021-04-23 11:46:47', '2021-04-23 11:46:47'),
(5, 5, 30, '2021-04-23 11:48:59', '2021-04-23 11:48:59'),
(6, 6, 999, '2021-04-23 11:49:33', '2021-04-23 11:49:33'),
(7, 7, 35, '2021-04-23 11:51:00', '2021-04-23 11:51:00'),
(8, 8, 30, '2021-04-23 11:51:29', '2021-04-23 11:51:29'),
(9, 9, 999, '2021-04-23 11:53:51', '2021-04-23 11:53:51'),
(10, 10, 30, '2021-04-23 11:57:36', '2021-04-23 11:57:36'),
(11, 11, 999, '2021-04-23 11:58:18', '2021-04-23 11:58:18'),
(12, 12, 35, '2021-04-23 11:59:56', '2021-04-23 11:59:56'),
(13, 13, 30, '2021-04-23 12:00:44', '2021-04-23 12:00:44'),
(14, 14, 999, '2021-04-23 12:04:31', '2021-04-23 12:04:31'),
(15, 15, 35, '2021-04-23 12:11:41', '2021-04-23 12:11:41'),
(16, 16, 35, '2021-04-30 01:15:37', '2021-04-30 01:15:37'),
(17, 17, 30, '2021-05-04 02:24:51', '2021-05-04 02:24:51'),
(18, 18, 999, '2021-05-04 02:28:59', '2021-05-04 02:28:59'),
(19, 19, 999, '2021-05-04 02:44:01', '2021-05-04 02:44:01'),
(20, 20, 1000, '2021-05-10 11:49:40', '2021-05-10 11:49:40'),
(21, 21, 35, '2021-05-24 08:52:18', '2021-05-24 08:52:18'),
(22, 22, 35, '2021-05-24 11:08:08', '2021-05-24 11:08:08'),
(23, 23, 16, '2021-06-01 08:40:12', '2021-06-01 08:40:12');

-- --------------------------------------------------------

--
-- Table structure for table `notification_users`
--

CREATE TABLE `notification_users` (
  `id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `status` char(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_users`
--

INSERT INTO `notification_users` (`id`, `notification_id`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'N', 27, '2021-04-20 16:12:31', '2021-04-20 16:12:31'),
(2, 1, 'N', 28, '2021-04-20 16:12:31', '2021-04-20 16:12:31'),
(3, 1, 'N', 29, '2021-04-20 16:12:31', '2021-04-20 16:12:31'),
(4, 1, 'N', 30, '2021-04-20 16:12:31', '2021-04-20 16:12:31'),
(5, 1, 'N', 34, '2021-04-20 16:12:31', '2021-04-20 16:12:31'),
(6, 1, 'N', 37, '2021-04-20 16:12:31', '2021-04-20 16:12:31'),
(7, 1, 'N', 38, '2021-04-20 16:12:31', '2021-04-20 16:12:31'),
(8, 2, 'N', 27, '2021-04-21 10:44:31', '2021-04-21 10:44:31'),
(9, 2, 'N', 28, '2021-04-21 10:44:31', '2021-04-21 10:44:31'),
(10, 2, 'N', 29, '2021-04-21 10:44:31', '2021-04-21 10:44:31'),
(11, 2, 'N', 34, '2021-04-21 10:44:31', '2021-04-21 10:44:31'),
(12, 2, 'N', 35, '2021-04-21 10:44:31', '2021-04-21 10:44:31'),
(13, 2, 'N', 37, '2021-04-21 10:44:31', '2021-04-21 10:44:31'),
(14, 2, 'N', 38, '2021-04-21 10:44:31', '2021-04-21 10:44:31'),
(15, 3, 'N', 27, '2021-04-23 11:36:16', '2021-04-23 11:36:16'),
(16, 3, 'N', 28, '2021-04-23 11:36:16', '2021-04-23 11:36:16'),
(17, 3, 'N', 29, '2021-04-23 11:36:16', '2021-04-23 11:36:16'),
(18, 3, 'N', 30, '2021-04-23 11:36:16', '2021-04-23 11:36:16'),
(19, 3, 'N', 34, '2021-04-23 11:36:16', '2021-04-23 11:36:16'),
(20, 3, 'N', 35, '2021-04-23 11:36:16', '2021-04-23 11:36:16'),
(21, 3, 'N', 37, '2021-04-23 11:36:16', '2021-04-23 11:36:16'),
(22, 3, 'N', 38, '2021-04-23 11:36:16', '2021-04-23 11:36:16'),
(23, 3, 'N', 33, '2021-04-23 11:36:16', '2021-04-23 11:36:16'),
(24, 4, 'N', 27, '2021-04-23 11:46:47', '2021-04-23 11:46:47'),
(25, 4, 'N', 28, '2021-04-23 11:46:47', '2021-04-23 11:46:47'),
(26, 4, 'N', 29, '2021-04-23 11:46:47', '2021-04-23 11:46:47'),
(27, 4, 'N', 30, '2021-04-23 11:46:47', '2021-04-23 11:46:47'),
(28, 4, 'N', 34, '2021-04-23 11:46:47', '2021-04-23 11:46:47'),
(29, 4, 'N', 37, '2021-04-23 11:46:47', '2021-04-23 11:46:47'),
(30, 4, 'N', 38, '2021-04-23 11:46:47', '2021-04-23 11:46:47'),
(31, 5, 'N', 27, '2021-04-23 11:48:59', '2021-04-23 11:48:59'),
(32, 5, 'N', 28, '2021-04-23 11:48:59', '2021-04-23 11:48:59'),
(33, 5, 'N', 29, '2021-04-23 11:48:59', '2021-04-23 11:48:59'),
(34, 5, 'N', 34, '2021-04-23 11:48:59', '2021-04-23 11:48:59'),
(35, 5, 'N', 35, '2021-04-23 11:48:59', '2021-04-23 11:48:59'),
(36, 5, 'N', 37, '2021-04-23 11:48:59', '2021-04-23 11:48:59'),
(37, 5, 'N', 38, '2021-04-23 11:48:59', '2021-04-23 11:48:59'),
(38, 6, 'N', 27, '2021-04-23 11:49:33', '2021-04-23 11:49:33'),
(39, 6, 'N', 28, '2021-04-23 11:49:33', '2021-04-23 11:49:33'),
(40, 6, 'N', 29, '2021-04-23 11:49:33', '2021-04-23 11:49:33'),
(41, 6, 'N', 30, '2021-04-23 11:49:33', '2021-04-23 11:49:33'),
(42, 6, 'N', 34, '2021-04-23 11:49:33', '2021-04-23 11:49:33'),
(43, 6, 'N', 35, '2021-04-23 11:49:33', '2021-04-23 11:49:33'),
(44, 6, 'N', 37, '2021-04-23 11:49:33', '2021-04-23 11:49:33'),
(45, 6, 'N', 38, '2021-04-23 11:49:33', '2021-04-23 11:49:33'),
(46, 6, 'N', 33, '2021-04-23 11:49:33', '2021-04-23 11:49:33'),
(47, 7, 'N', 27, '2021-04-23 11:51:00', '2021-04-23 11:51:00'),
(48, 7, 'N', 28, '2021-04-23 11:51:00', '2021-04-23 11:51:00'),
(49, 7, 'N', 29, '2021-04-23 11:51:00', '2021-04-23 11:51:00'),
(50, 7, 'N', 30, '2021-04-23 11:51:00', '2021-04-23 11:51:00'),
(51, 7, 'N', 34, '2021-04-23 11:51:00', '2021-04-23 11:51:00'),
(52, 7, 'N', 37, '2021-04-23 11:51:00', '2021-04-23 11:51:00'),
(53, 7, 'N', 38, '2021-04-23 11:51:00', '2021-04-23 11:51:00'),
(54, 8, 'N', 27, '2021-04-23 11:51:29', '2021-04-23 11:51:29'),
(55, 8, 'N', 28, '2021-04-23 11:51:29', '2021-04-23 11:51:29'),
(56, 8, 'N', 29, '2021-04-23 11:51:29', '2021-04-23 11:51:29'),
(57, 8, 'N', 34, '2021-04-23 11:51:29', '2021-04-23 11:51:29'),
(58, 8, 'N', 35, '2021-04-23 11:51:29', '2021-04-23 11:51:29'),
(59, 8, 'N', 37, '2021-04-23 11:51:29', '2021-04-23 11:51:29'),
(60, 8, 'N', 38, '2021-04-23 11:51:29', '2021-04-23 11:51:29'),
(61, 9, 'N', 27, '2021-04-23 11:53:51', '2021-04-23 11:53:51'),
(62, 9, 'N', 28, '2021-04-23 11:53:51', '2021-04-23 11:53:51'),
(63, 9, 'N', 29, '2021-04-23 11:53:51', '2021-04-23 11:53:51'),
(64, 9, 'N', 30, '2021-04-23 11:53:51', '2021-04-23 11:53:51'),
(65, 9, 'N', 34, '2021-04-23 11:53:51', '2021-04-23 11:53:51'),
(66, 9, 'N', 35, '2021-04-23 11:53:51', '2021-04-23 11:53:51'),
(67, 9, 'N', 37, '2021-04-23 11:53:51', '2021-04-23 11:53:51'),
(68, 9, 'N', 38, '2021-04-23 11:53:51', '2021-04-23 11:53:51'),
(69, 9, 'N', 33, '2021-04-23 11:53:51', '2021-04-23 11:53:51'),
(70, 10, 'N', 27, '2021-04-23 11:57:36', '2021-04-23 11:57:36'),
(71, 10, 'N', 28, '2021-04-23 11:57:36', '2021-04-23 11:57:36'),
(72, 10, 'N', 29, '2021-04-23 11:57:36', '2021-04-23 11:57:36'),
(73, 10, 'N', 34, '2021-04-23 11:57:36', '2021-04-23 11:57:36'),
(74, 10, 'N', 35, '2021-04-23 11:57:36', '2021-04-23 11:57:36'),
(75, 10, 'N', 37, '2021-04-23 11:57:36', '2021-04-23 11:57:36'),
(76, 10, 'N', 38, '2021-04-23 11:57:36', '2021-04-23 11:57:36'),
(77, 11, 'N', 27, '2021-04-23 11:58:18', '2021-04-23 11:58:18'),
(78, 11, 'N', 28, '2021-04-23 11:58:18', '2021-04-23 11:58:18'),
(79, 11, 'N', 29, '2021-04-23 11:58:18', '2021-04-23 11:58:18'),
(80, 11, 'N', 30, '2021-04-23 11:58:18', '2021-04-23 11:58:18'),
(81, 11, 'N', 34, '2021-04-23 11:58:18', '2021-04-23 11:58:18'),
(82, 11, 'N', 35, '2021-04-23 11:58:18', '2021-04-23 11:58:18'),
(83, 11, 'N', 37, '2021-04-23 11:58:18', '2021-04-23 11:58:18'),
(84, 11, 'N', 38, '2021-04-23 11:58:18', '2021-04-23 11:58:18'),
(85, 11, 'N', 33, '2021-04-23 11:58:18', '2021-04-23 11:58:18'),
(86, 12, 'N', 27, '2021-04-23 11:59:56', '2021-04-23 11:59:56'),
(87, 12, 'N', 28, '2021-04-23 11:59:56', '2021-04-23 11:59:56'),
(88, 12, 'N', 29, '2021-04-23 11:59:56', '2021-04-23 11:59:56'),
(89, 12, 'N', 30, '2021-04-23 11:59:56', '2021-04-23 11:59:56'),
(90, 12, 'N', 34, '2021-04-23 11:59:56', '2021-04-23 11:59:56'),
(91, 12, 'N', 37, '2021-04-23 11:59:56', '2021-04-23 11:59:56'),
(92, 12, 'N', 38, '2021-04-23 11:59:56', '2021-04-23 11:59:56'),
(93, 13, 'N', 27, '2021-04-23 12:00:44', '2021-04-23 12:00:44'),
(94, 13, 'N', 28, '2021-04-23 12:00:44', '2021-04-23 12:00:44'),
(95, 13, 'N', 29, '2021-04-23 12:00:44', '2021-04-23 12:00:44'),
(96, 13, 'N', 34, '2021-04-23 12:00:44', '2021-04-23 12:00:44'),
(97, 13, 'N', 35, '2021-04-23 12:00:44', '2021-04-23 12:00:44'),
(98, 13, 'N', 37, '2021-04-23 12:00:44', '2021-04-23 12:00:44'),
(99, 13, 'N', 38, '2021-04-23 12:00:44', '2021-04-23 12:00:44'),
(100, 14, 'N', 27, '2021-04-23 12:04:31', '2021-04-23 12:04:31'),
(101, 14, 'N', 28, '2021-04-23 12:04:31', '2021-04-23 12:04:31'),
(102, 14, 'N', 29, '2021-04-23 12:04:31', '2021-04-23 12:04:31'),
(103, 14, 'N', 30, '2021-04-23 12:04:31', '2021-04-23 12:04:31'),
(104, 14, 'N', 34, '2021-04-23 12:04:31', '2021-04-23 12:04:31'),
(105, 14, 'N', 35, '2021-04-23 12:04:31', '2021-04-23 12:04:31'),
(106, 14, 'N', 37, '2021-04-23 12:04:31', '2021-04-23 12:04:31'),
(107, 14, 'N', 38, '2021-04-23 12:04:31', '2021-04-23 12:04:31'),
(108, 14, 'N', 33, '2021-04-23 12:04:31', '2021-04-23 12:04:31'),
(109, 15, 'N', 27, '2021-04-23 12:11:41', '2021-04-23 12:11:41'),
(110, 15, 'N', 28, '2021-04-23 12:11:41', '2021-04-23 12:11:41'),
(111, 15, 'N', 29, '2021-04-23 12:11:41', '2021-04-23 12:11:41'),
(112, 15, 'N', 30, '2021-04-23 12:11:41', '2021-04-23 12:11:41'),
(113, 15, 'N', 34, '2021-04-23 12:11:41', '2021-04-23 12:11:41'),
(114, 15, 'N', 37, '2021-04-23 12:11:41', '2021-04-23 12:11:41'),
(115, 15, 'N', 38, '2021-04-23 12:11:41', '2021-04-23 12:11:41'),
(116, 16, 'N', 27, '2021-04-30 01:15:37', '2021-04-30 01:15:37'),
(117, 16, 'N', 28, '2021-04-30 01:15:37', '2021-04-30 01:15:37'),
(118, 16, 'N', 29, '2021-04-30 01:15:37', '2021-04-30 01:15:37'),
(119, 16, 'N', 30, '2021-04-30 01:15:37', '2021-04-30 01:15:37'),
(120, 16, 'N', 34, '2021-04-30 01:15:37', '2021-04-30 01:15:37'),
(121, 16, 'N', 37, '2021-04-30 01:15:37', '2021-04-30 01:15:37'),
(122, 16, 'N', 38, '2021-04-30 01:15:37', '2021-04-30 01:15:37'),
(123, 17, 'N', 27, '2021-05-04 02:24:51', '2021-05-04 02:24:51'),
(124, 17, 'N', 28, '2021-05-04 02:24:51', '2021-05-04 02:24:51'),
(125, 17, 'N', 29, '2021-05-04 02:24:51', '2021-05-04 02:24:51'),
(126, 17, 'N', 34, '2021-05-04 02:24:51', '2021-05-04 02:24:51'),
(127, 17, 'N', 35, '2021-05-04 02:24:51', '2021-05-04 02:24:51'),
(128, 17, 'N', 37, '2021-05-04 02:24:51', '2021-05-04 02:24:51'),
(129, 17, 'N', 38, '2021-05-04 02:24:51', '2021-05-04 02:24:51'),
(130, 18, 'N', 27, '2021-05-04 02:28:59', '2021-05-04 02:28:59'),
(131, 18, 'N', 28, '2021-05-04 02:28:59', '2021-05-04 02:28:59'),
(132, 18, 'N', 29, '2021-05-04 02:28:59', '2021-05-04 02:28:59'),
(133, 18, 'N', 30, '2021-05-04 02:28:59', '2021-05-04 02:28:59'),
(134, 18, 'N', 34, '2021-05-04 02:28:59', '2021-05-04 02:28:59'),
(135, 18, 'N', 35, '2021-05-04 02:28:59', '2021-05-04 02:28:59'),
(136, 18, 'N', 37, '2021-05-04 02:28:59', '2021-05-04 02:28:59'),
(137, 18, 'N', 38, '2021-05-04 02:28:59', '2021-05-04 02:28:59'),
(138, 18, 'N', 33, '2021-05-04 02:28:59', '2021-05-04 02:28:59'),
(139, 19, 'N', 27, '2021-05-04 02:44:01', '2021-05-04 02:44:01'),
(140, 19, 'N', 28, '2021-05-04 02:44:01', '2021-05-04 02:44:01'),
(141, 19, 'N', 29, '2021-05-04 02:44:01', '2021-05-04 02:44:01'),
(142, 19, 'N', 30, '2021-05-04 02:44:01', '2021-05-04 02:44:01'),
(143, 19, 'N', 34, '2021-05-04 02:44:01', '2021-05-04 02:44:01'),
(144, 19, 'N', 35, '2021-05-04 02:44:01', '2021-05-04 02:44:01'),
(145, 19, 'N', 37, '2021-05-04 02:44:01', '2021-05-04 02:44:01'),
(146, 19, 'N', 38, '2021-05-04 02:44:01', '2021-05-04 02:44:01'),
(147, 19, 'N', 33, '2021-05-04 02:44:01', '2021-05-04 02:44:01'),
(148, 20, 'N', 27, '2021-05-10 11:49:40', '2021-05-10 11:49:40'),
(149, 20, 'N', 28, '2021-05-10 11:49:40', '2021-05-10 11:49:40'),
(150, 20, 'N', 29, '2021-05-10 11:49:40', '2021-05-10 11:49:40'),
(151, 20, 'N', 30, '2021-05-10 11:49:40', '2021-05-10 11:49:40'),
(152, 20, 'N', 34, '2021-05-10 11:49:40', '2021-05-10 11:49:40'),
(153, 20, 'N', 35, '2021-05-10 11:49:40', '2021-05-10 11:49:40'),
(154, 20, 'N', 37, '2021-05-10 11:49:40', '2021-05-10 11:49:40'),
(155, 20, 'N', 38, '2021-05-10 11:49:40', '2021-05-10 11:49:40'),
(156, 21, 'N', 27, '2021-05-24 08:52:18', '2021-05-24 08:52:18'),
(157, 21, 'N', 28, '2021-05-24 08:52:18', '2021-05-24 08:52:18'),
(158, 21, 'N', 29, '2021-05-24 08:52:18', '2021-05-24 08:52:18'),
(159, 21, 'N', 30, '2021-05-24 08:52:18', '2021-05-24 08:52:18'),
(160, 21, 'N', 34, '2021-05-24 08:52:18', '2021-05-24 08:52:18'),
(161, 21, 'N', 37, '2021-05-24 08:52:18', '2021-05-24 08:52:18'),
(162, 21, 'N', 38, '2021-05-24 08:52:18', '2021-05-24 08:52:18'),
(163, 22, 'N', 27, '2021-05-24 11:08:08', '2021-05-24 11:08:08'),
(164, 22, 'N', 28, '2021-05-24 11:08:08', '2021-05-24 11:08:08'),
(165, 22, 'N', 29, '2021-05-24 11:08:08', '2021-05-24 11:08:08'),
(166, 22, 'N', 30, '2021-05-24 11:08:08', '2021-05-24 11:08:08'),
(167, 22, 'N', 34, '2021-05-24 11:08:08', '2021-05-24 11:08:08'),
(168, 22, 'N', 37, '2021-05-24 11:08:08', '2021-05-24 11:08:08'),
(169, 22, 'N', 38, '2021-05-24 11:08:08', '2021-05-24 11:08:08'),
(170, 23, 'N', 27, '2021-06-01 08:40:12', '2021-06-01 08:40:12'),
(171, 23, 'N', 28, '2021-06-01 08:40:12', '2021-06-01 08:40:12'),
(172, 23, 'N', 29, '2021-06-01 08:40:12', '2021-06-01 08:40:12'),
(173, 23, 'N', 30, '2021-06-01 08:40:12', '2021-06-01 08:40:12'),
(174, 23, 'N', 34, '2021-06-01 08:40:12', '2021-06-01 08:40:12'),
(175, 23, 'N', 35, '2021-06-01 08:40:12', '2021-06-01 08:40:12'),
(176, 23, 'N', 37, '2021-06-01 08:40:12', '2021-06-01 08:40:12'),
(177, 23, 'N', 38, '2021-06-01 08:40:12', '2021-06-01 08:40:12');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_log`
--

CREATE TABLE `pembayaran_log` (
  `id` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `tipe` int(11) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `id_konsumen` int(11) NOT NULL DEFAULT '0',
  `kode_order` varchar(20) DEFAULT NULL,
  `tgl_input` date NOT NULL,
  `keterangan` varchar(120) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` varchar(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran_log`
--

INSERT INTO `pembayaran_log` (`id`, `nominal`, `tipe`, `jenis`, `id_konsumen`, `kode_order`, `tgl_input`, `keterangan`, `updated_at`, `created_by`, `updated_by`, `created_at`, `is_deleted`) VALUES
(1, 220000, 2, '2', 0, '', '2020-07-28', 'Uji Coba', '2020-07-31 02:00:12', 16, 0, '2020-07-28 12:46:38', '0'),
(2, 310000, 1, '1', 0, '', '2020-07-31', '', '2020-07-31 02:15:45', 16, 0, '2020-07-31 00:39:24', '0'),
(3, 1000000, 2, '4', 0, '', '2020-07-01', '', '2020-08-11 16:37:38', 16, 0, '2020-07-31 03:41:44', '0'),
(4, 300000, 2, '3', 0, '', '2020-07-03', '', '2020-08-11 16:48:16', 16, 16, '2020-07-31 03:42:08', '0'),
(5, 600000, 2, '3', 0, 'g', '2020-07-12', 'kekekekek', '2020-07-31 03:42:28', 16, 0, '2020-07-31 03:42:28', '0'),
(6, 5000000, 2, '6', 75, 'PKMD159', '2020-08-21', '', '2020-08-21 03:15:41', 16, 0, '2020-08-21 03:15:41', '0'),
(7, 2500000, 1, '2', 75, 'PKMD159', '2020-08-21', '', '2020-08-21 03:39:53', 16, 0, '2020-08-21 03:39:53', '0'),
(8, 6000000, 1, '2', 134, 'PKMD170', '2020-08-21', '', '2020-08-21 05:21:30', 16, 0, '2020-08-21 05:21:30', '0'),
(9, 6000000, 2, '6', 134, 'PKMD170', '2020-08-21', '', '2020-08-21 05:21:55', 16, 0, '2020-08-21 05:21:55', '0'),
(10, 2000000, 1, '2', 155, 'PGMW10', '2020-07-26', '', '2020-08-21 06:09:16', 16, 0, '2020-08-21 06:09:16', '0'),
(11, 3000000, 1, '3', 134, 'PKMD170', '2020-08-23', '', '2020-08-23 06:07:14', 16, 0, '2020-08-23 06:07:14', '0'),
(12, 5000000, 1, '4', 134, 'PKMD170', '2020-08-23', '', '2020-08-23 06:12:41', 16, 0, '2020-08-23 06:12:41', '0'),
(13, 3000000, 2, '6', 0, '', '2020-09-01', 'Transfer', '2020-09-01 12:07:10', 16, 0, '2020-09-01 12:07:10', '0'),
(14, 100000, 2, '5', 155, 'PGMW10', '2021-03-18', 'TEsting', '2021-03-18 03:37:38', 34, 0, '2021-03-18 03:37:38', '0'),
(15, 100000, 1, '3', 0, NULL, '2021-03-18', 'TEsting', '2021-03-18 03:45:29', 34, 0, '2021-03-18 03:45:29', '0'),
(16, 200, 1, '4', 16, 'TEESTDOBLE', '2021-06-01', 'tes', '2021-06-01 09:33:31', 35, 0, '2021-06-01 09:33:31', '0');

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan_harga`
--

CREATE TABLE `pengajuan_harga` (
  `id` int(11) NOT NULL,
  `tgl_pengajuan` date NOT NULL,
  `tgl_disetujui` date DEFAULT '0000-00-00',
  `id_konsumen` int(11) NOT NULL DEFAULT '0',
  `nama_konsumen` varchar(50) NOT NULL,
  `alamat_konsumen` varchar(100) NOT NULL,
  `no_hp_konsumen` varchar(20) NOT NULL,
  `harga_setuju_pokok_granit` int(11) NOT NULL DEFAULT '0',
  `harga_setuju_pokok_keramik` int(11) NOT NULL DEFAULT '0',
  `harga_setuju_granit` int(11) NOT NULL DEFAULT '0',
  `harga_setuju_keramik` int(11) NOT NULL DEFAULT '0',
  `diskon` int(11) NOT NULL DEFAULT '0',
  `uang_muka` int(11) DEFAULT '0',
  `tanda_jadi` int(11) NOT NULL DEFAULT '0',
  `pelunasan` int(11) NOT NULL DEFAULT '0',
  `jenis` int(11) NOT NULL DEFAULT '0' COMMENT '1 Untuk granit, 2 untuk keramik',
  `status` varchar(50) NOT NULL,
  `revisi_hpp` varchar(1) DEFAULT '0',
  `keterangan` varchar(200) NOT NULL,
  `is_deleted` varchar(1) NOT NULL DEFAULT '0',
  `user_id_pengaju` int(11) NOT NULL,
  `user_id_approve` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajuan_harga`
--

INSERT INTO `pengajuan_harga` (`id`, `tgl_pengajuan`, `tgl_disetujui`, `id_konsumen`, `nama_konsumen`, `alamat_konsumen`, `no_hp_konsumen`, `harga_setuju_pokok_granit`, `harga_setuju_pokok_keramik`, `harga_setuju_granit`, `harga_setuju_keramik`, `diskon`, `uang_muka`, `tanda_jadi`, `pelunasan`, `jenis`, `status`, `revisi_hpp`, `keterangan`, `is_deleted`, `user_id_pengaju`, `user_id_approve`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, '2020-07-12', '0000-00-00', 0, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1', '0', 'Uji Coba', '0', 16, 0, 16, 0, '2020-07-12 12:59:54', '2020-07-12 19:59:54'),
(2, '2020-07-12', '0000-00-00', 0, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1', '0', 'Uji Coba', '0', 16, 0, 16, 0, '2020-07-13 10:36:43', '2020-07-13 17:36:43'),
(3, '2020-07-13', '0000-00-00', 0, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1', '0', 'Uji Coba', '0', 16, 0, 16, 0, '2020-07-13 10:38:15', '2020-07-13 17:38:15'),
(4, '2020-07-14', '0000-00-00', 0, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '2', '0', 'Uji Coba', '0', 16, 0, 16, 0, '2020-07-14 14:11:31', '2020-08-08 21:44:16'),
(5, '2020-07-14', '0000-00-00', 0, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1', '0', 'Uji Coba', '0', 16, 0, 16, 0, '2020-07-14 14:14:49', '2020-07-14 21:14:49'),
(6, '2020-07-15', '2020-07-22', 134, '', '', '', 6075000, 5275000, 6075000, 5275000, 0, 0, 0, 0, 1, '4', '0', 'Uji Coba', '0', 16, 16, 16, 0, '2020-07-15 12:30:37', '2020-08-21 12:17:05'),
(7, '2020-08-05', '2020-08-11', 155, '', 'Jl. Terusan Kayan A135', '', 7790000, 6390000, 12370000, 10420000, 0, 200000, 0, 0, 1, '4', '0', '', '0', 16, 27, 37, 16, '2020-08-05 12:11:43', '2020-08-10 21:01:20'),
(8, '2020-08-07', '0000-00-00', 0, '', '', '', 0, 0, 0, 0, 0, 200000, 0, 0, 0, '3', '0', '', '0', 37, 0, 37, 0, '2020-08-07 16:01:09', '2020-08-07 23:18:10'),
(9, '2020-08-09', '2020-08-26', 75, '', 'Griya Asri Purwosari, Pasuruan', '', 0, 0, 0, 0, 0, 0, 0, 0, 2, '5', '1', '', '0', 16, 30, 16, 16, '2020-08-09 03:21:19', '2021-02-26 17:39:27'),
(10, '2020-08-12', '0000-00-00', 0, 'Grady 2', 'Malang 2', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1', '0', '', '0', 16, 0, 16, 0, '2020-08-12 15:46:30', '2020-08-12 22:46:30'),
(11, '2020-08-14', '0000-00-00', 0, 'Andi', 'Malang', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1', '0', '', '0', 16, 0, 16, 0, '2020-08-12 15:51:47', '2020-08-12 22:51:47'),
(12, '2020-08-23', '0000-00-00', 0, 'tita', 'sawojajar', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1', '0', '', '0', 27, 0, 27, 0, '2020-08-23 01:51:02', '2020-08-23 08:51:02'),
(13, '2020-08-24', '2020-09-02', 6, 'Test', 'Test', '', 1950000, 1650000, 1950000, 1650000, 0, 0, 0, 0, 1, '4', '0', '', '0', 16, 16, 16, 16, '2020-08-24 03:29:10', '2020-09-02 19:03:19'),
(15, '2021-01-11', '0000-00-00', 0, 'Edo', 'Bondowoso', '088888', 0, 0, 0, 0, 0, NULL, 0, 0, 0, '1', '0', 'test', '0', 35, 0, 35, 0, '2021-01-13 21:04:28', '2021-01-13 21:04:28'),
(16, '2021-03-22', '0000-00-00', 0, 'Bu Priatna', 'Jl. Terusan Kayan A135', '+62 816-517-559', 0, 0, 0, 0, 0, NULL, 0, 0, 0, '1', '0', 'tes', '0', 35, 0, 35, 0, '2021-03-22 09:37:52', '2021-03-22 16:37:52'),
(17, '2021-04-29', '2021-04-27', 13, 'Test', 'Test', '5555', 7430000, 6960000, 10520000, 9520000, 0, 0, 0, 0, 0, '4', '1', 'tes', '0', 35, 34, 35, 0, '2021-04-27 14:15:03', '2021-04-27 21:23:38'),
(18, '2021-04-27', '0000-00-00', 0, 'Test', 'Test', '5555', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1', '0', 'tes', '0', 35, 0, 35, 0, '2021-04-27 15:11:56', '2021-04-27 22:11:56'),
(19, '2021-06-17', '2021-06-01', 16, 'Tesitgn Pengajuan', 'aaa', '0999777', 2720000, 2240000, 4450000, 3850000, 0, 0, 0, 0, 1, '4', '0', 'TEsting', '0', 35, 34, 35, 0, '2021-06-01 08:39:21', '2021-06-01 15:39:44'),
(20, '2021-06-01', '0000-00-00', 0, 'Tesitgn Pengajuan', 'aaa', '0999777', 0, 0, 0, 0, 0, 0, 0, 0, 1, '1', '0', '', '0', 35, 0, 35, 0, '2021-06-01 09:41:57', '2021-06-01 16:41:57');

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan_harga_komplain`
--

CREATE TABLE `pengajuan_harga_komplain` (
  `id` int(11) NOT NULL,
  `tgl_pengajuan` date NOT NULL,
  `tgl_disetujui` date DEFAULT '0000-00-00',
  `id_konsumen` int(11) NOT NULL DEFAULT '0',
  `nama_konsumen` varchar(50) NOT NULL,
  `alamat_konsumen` varchar(100) NOT NULL,
  `no_hp_konsumen` varchar(20) NOT NULL,
  `harga_setuju` int(11) NOT NULL DEFAULT '0',
  `diskon` int(11) NOT NULL DEFAULT '0',
  `uang_muka` int(11) DEFAULT '0',
  `tanda_jadi` int(11) NOT NULL DEFAULT '0',
  `pelunasan` int(11) NOT NULL DEFAULT '0',
  `jenis` int(11) NOT NULL DEFAULT '0' COMMENT '1 Untuk granit, 2 untuk keramik',
  `status` varchar(50) NOT NULL,
  `revisi_hpp` varchar(1) DEFAULT '0',
  `keterangan` varchar(200) NOT NULL,
  `is_deleted` varchar(1) NOT NULL DEFAULT '0',
  `user_id_pengaju` int(11) NOT NULL,
  `user_id_approve` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajuan_harga_komplain`
--

INSERT INTO `pengajuan_harga_komplain` (`id`, `tgl_pengajuan`, `tgl_disetujui`, `id_konsumen`, `nama_konsumen`, `alamat_konsumen`, `no_hp_konsumen`, `harga_setuju`, `diskon`, `uang_muka`, `tanda_jadi`, `pelunasan`, `jenis`, `status`, `revisi_hpp`, `keterangan`, `is_deleted`, `user_id_pengaju`, `user_id_approve`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, '2021-04-27', '2021-04-27', 2, 'dsfdsfdsfdsfds', 'fdfddsfdsfdsfds ', '65456456', 2330000, 0, 0, 0, 0, 0, '4', '0', 'Testing', '0', 33, 35, 33, 0, '2021-04-27 14:14:09', '2021-04-27 21:50:44'),
(3, '2021-04-27', '0000-00-00', 5, 'eDO', ' fsff', '435435435', 0, 0, 0, 0, 0, 0, '3', '0', 'tes', '0', 33, 0, 33, 0, '2021-04-27 15:22:08', '2021-04-27 22:23:25'),
(4, '2021-04-28', '0000-00-00', 2, 'dsfdsfdsfdsfds', 'fdfddsfdsfdsfds ', '65456456', 0, 0, 0, 0, 0, 0, '1', '0', '', '0', 33, 0, 33, 0, '2021-04-28 01:46:16', '2021-04-28 08:46:16');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan`
--

CREATE TABLE `pesanan` (
  `id` int(11) NOT NULL,
  `kode_order` varchar(20) NOT NULL,
  `tgl_order_masuk` date NOT NULL,
  `tgl_jatuh_tempo` date NOT NULL,
  `tgl_acc_konsumen` date DEFAULT NULL,
  `flow_id` int(11) NOT NULL,
  `status_order` char(5) NOT NULL,
  `user_id` int(11) NOT NULL,
  `konsumen_id` int(11) NOT NULL,
  `desainer_design` int(11) DEFAULT NULL,
  `desainer_eksekusi` int(11) DEFAULT NULL,
  `mitra_pemasang_kusen` int(11) DEFAULT NULL,
  `mitra_pemasang_finish` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `tipe` varchar(1) DEFAULT NULL,
  `status_produksi` varchar(1) NOT NULL DEFAULT '3' COMMENT ' 1: Bisa Cek Ruangan; 2: Bisa pasang rangka; 3: Menunggu Cor Siap',
  `link` varchar(200) DEFAULT NULL,
  `catatan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesanan`
--

INSERT INTO `pesanan` (`id`, `kode_order`, `tgl_order_masuk`, `tgl_jatuh_tempo`, `tgl_acc_konsumen`, `flow_id`, `status_order`, `user_id`, `konsumen_id`, `desainer_design`, `desainer_eksekusi`, `mitra_pemasang_kusen`, `mitra_pemasang_finish`, `created_at`, `updated_at`, `is_deleted`, `tipe`, `status_produksi`, `link`, `catatan`) VALUES
(1, 'TES', '2021-04-29', '2021-05-29', NULL, 4, 'P', 35, 1, NULL, NULL, 33, NULL, '2021-04-20 15:58:20', '2021-04-23 11:36:16', 0, 'P', '1', NULL, 'tesa'),
(2, 'TESKOMPLAIN', '2021-04-29', '2021-05-29', NULL, 8, 'S', 35, 2, NULL, NULL, NULL, NULL, '2021-04-20 16:12:31', '2021-04-20 16:12:31', 0, 'K', '3', NULL, 'catata'),
(3, 'TES2', '2021-04-29', '2021-05-29', NULL, 4, 'P', 35, 3, NULL, NULL, 33, NULL, '2021-04-23 11:46:47', '2021-04-23 11:49:33', 0, 'P', '1', NULL, NULL),
(4, 'TES3', '2021-04-30', '2021-05-30', NULL, 4, 'P', 35, 4, NULL, NULL, 33, NULL, '2021-04-23 11:51:00', '2021-04-23 11:53:51', 0, 'P', '1', NULL, NULL),
(5, 'TESKOMPLAIN2', '2021-04-30', '2021-05-30', NULL, 8, 'S', 35, 5, NULL, NULL, NULL, NULL, '2021-04-23 11:59:56', '2021-04-23 11:59:56', 0, 'K', '3', NULL, 'ff'),
(6, 'TEST', '2021-04-29', '2021-05-29', NULL, 2, 'P', 35, 6, 28, NULL, NULL, NULL, '2021-04-23 12:11:41', '2021-05-04 02:24:05', 0, 'P', '1', NULL, 'test'),
(7, 'TESDESAINER', '2021-05-01', '2021-05-31', NULL, 4, 'P', 35, 7, 38, 28, 33, NULL, '2021-04-30 01:15:37', '2021-05-20 07:30:26', 0, 'P', '1', NULL, 'tes'),
(10, 'TTTT', '2021-05-28', '2021-06-27', NULL, 2, 'P', 1000, 13, 28, 38, NULL, NULL, '2021-05-10 11:49:40', '2021-05-20 07:02:02', 0, 'P', '1', NULL, 't'),
(11, 'TESFOTO', '2021-05-21', '2021-06-20', NULL, 2, 'P', 35, 14, NULL, NULL, NULL, NULL, '2021-05-24 08:52:18', '2021-05-24 08:52:18', 0, 'P', '1', NULL, 'tes'),
(12, 'TESFOTO2', '2021-05-28', '2021-06-27', NULL, 2, 'P', 35, 15, NULL, NULL, NULL, NULL, '2021-05-24 11:08:08', '2021-05-24 11:08:08', 0, 'P', '1', NULL, NULL),
(13, 'TEESTDOBLE', '2021-06-18', '2021-07-18', NULL, 2, 'P', 16, 16, 28, 38, NULL, NULL, '2021-06-01 08:40:12', '2021-06-01 08:40:12', 0, 'P', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pesanan_files`
--

CREATE TABLE `pesanan_files` (
  `id` int(11) NOT NULL,
  `pesanan_id` int(11) NOT NULL,
  `tipe` char(5) NOT NULL,
  `nama_file` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesanan_files`
--

INSERT INTO `pesanan_files` (`id`, `pesanan_id`, `tipe`, `nama_file`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 11, 'OM', 'risol-sayur-frozen-foto-resep-utama-60ab6942186f9.jpg', 35, '2021-05-24 08:52:18', '2021-05-24 08:52:18'),
(2, 12, 'OM', '60ab886771a69.png', 35, '2021-05-24 11:08:08', '2021-05-24 11:08:08');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan_flow`
--

CREATE TABLE `pesanan_flow` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `rencana_kerja_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesanan_flow`
--

INSERT INTO `pesanan_flow` (`id`, `nama`, `rencana_kerja_id`) VALUES
(1, 'Order Masuk', 0),
(2, 'Pengajuan Pasang Kusen', 1),
(3, 'Konfirmasi Pasang Kusen', 1),
(4, 'Mitra Pemasang Kusen', 1),
(5, 'Pengajuan Pasang Unit', 2),
(6, 'Konfirmasi Pasang Unit', 2),
(7, 'Selesai Pemasangan Unit', 3),
(8, 'Selesai', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pesanan_log`
--

CREATE TABLE `pesanan_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pesanan_id` int(11) NOT NULL,
  `aktivitas` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesanan_log`
--

INSERT INTO `pesanan_log` (`id`, `user_id`, `pesanan_id`, `aktivitas`, `created_at`) VALUES
(8, 28, 56, 'menginputkan data konsumen dengan kode order \"PKMD146\"', '2020-03-01 03:53:29'),
(9, 29, 56, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD146\" ', '2020-03-02 10:31:59'),
(10, 27, 56, 'menolak jadwal pasang kusen untuk kode order \"PKMD146\" ', '2020-03-03 01:08:50'),
(11, 28, 57, 'menginputkan data konsumen dengan kode order \"PGMD123r\"', '2020-03-03 08:12:18'),
(12, 30, 57, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD123r\" ', '2020-03-04 06:39:15'),
(13, 28, 58, 'menginputkan data konsumen dengan kode order \"KSFG58\"', '2020-03-05 04:06:54'),
(14, 28, 59, 'menginputkan data konsumen dengan kode order \"PKMD148\"', '2020-03-05 04:21:26'),
(15, 28, 60, 'menginputkan data konsumen dengan kode order \"ESTI\"', '2020-03-05 04:46:50'),
(16, 28, 61, 'menginputkan data konsumen dengan kode order \"PGMD243\"', '2020-03-05 04:50:00'),
(17, 28, 62, 'menginputkan data konsumen dengan kode order \"PKMD147\"', '2020-03-05 04:55:27'),
(18, 28, 63, 'menginputkan data konsumen dengan kode order \"DIANA\"', '2020-03-05 06:46:05'),
(19, 28, 64, 'menginputkan data konsumen dengan kode order \"KSGK38\"', '2020-03-05 06:48:58'),
(20, 28, 65, 'menginputkan data konsumen dengan kode order \"PKMD149\"', '2020-03-05 06:51:51'),
(21, 30, 56, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD146\" ', '2020-03-06 01:43:56'),
(22, 28, 66, 'menginputkan data konsumen dengan kode order \"PKMD107\"', '2020-03-06 08:45:14'),
(23, 28, 67, 'menginputkan data konsumen dengan kode order \"PGMD244\"', '2020-03-09 05:20:56'),
(24, 28, 68, 'menginputkan data konsumen dengan kode order \"PGMD246\"', '2020-03-13 10:02:27'),
(25, 28, 69, 'menginputkan data konsumen dengan kode order \"PGMD245\"', '2020-03-18 09:48:42'),
(26, 27, 57, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD123r\" ', '2020-03-24 07:15:04'),
(29, 30, 58, 'mengajukan jadwal pasang kusen untuk kode order \"KSFG58\" ', '2020-03-26 13:20:24'),
(30, 27, 58, 'menerima pengajuan jadwal pasang kusen untuk kode order \"KSFG58\" ', '2020-03-26 13:20:40'),
(31, 30, 59, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD148\" ', '2020-03-26 13:22:50'),
(32, 27, 59, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD148\" ', '2020-03-26 13:23:12'),
(33, 36, 58, 'telah menyelesaikan pasang kusen untuk kode order \"KSFG58\" ', '2020-03-26 13:27:50'),
(34, 36, 59, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD148\" ', '2020-03-26 13:27:54'),
(35, 27, 64, 'Pesanan dihapus', '2020-03-26 13:48:24'),
(36, 33, 56, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD146\" ', '2020-03-26 13:49:47'),
(37, 28, 72, 'menginputkan data konsumen dengan kode order \"PGMD247\"', '2020-03-28 06:46:57'),
(38, 28, 73, 'menginputkan data konsumen dengan kode order \"PGMD249\"', '2020-03-28 06:51:49'),
(39, 28, 74, 'menginputkan data konsumen dengan kode order \"PKMD150\"', '2020-04-07 00:10:12'),
(40, 28, 75, 'menginputkan data konsumen dengan kode order \"ADG10\"', '2020-04-07 04:22:13'),
(41, 27, 60, 'Pesanan dihapus', '2020-04-11 02:36:03'),
(42, 27, 61, 'Pesanan dihapus', '2020-04-11 02:36:16'),
(43, 27, 63, 'Pesanan dihapus', '2020-04-11 02:36:32'),
(44, 28, 76, 'menginputkan data konsumen dengan kode order \"PKMD151\"', '2020-04-17 02:34:40'),
(45, 29, 73, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD249\" ', '2020-04-18 02:57:29'),
(46, 29, 68, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD246\" ', '2020-04-18 03:01:35'),
(47, 29, 69, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD245\" ', '2020-04-18 03:51:37'),
(48, 29, 74, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD150\" ', '2020-04-18 04:05:52'),
(49, 29, 75, 'mengajukan jadwal pasang kusen untuk kode order \"ADG10\" ', '2020-04-18 04:06:22'),
(50, 29, 76, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD151\" ', '2020-04-18 04:07:21'),
(51, 29, 67, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD244\" ', '2020-04-18 04:11:17'),
(52, 27, 67, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD244\" ', '2020-04-18 04:12:08'),
(53, 27, 76, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD151\" ', '2020-04-18 04:12:18'),
(54, 27, 75, 'menerima pengajuan jadwal pasang kusen untuk kode order \"ADG10\" ', '2020-04-18 04:12:22'),
(55, 27, 69, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD245\" ', '2020-04-18 04:12:31'),
(56, 27, 74, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD150\" ', '2020-04-18 04:12:36'),
(57, 27, 68, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD246\" ', '2020-04-18 04:12:52'),
(58, 27, 73, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD249\" ', '2020-04-18 04:12:59'),
(59, 29, 72, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD247\" ', '2020-04-18 04:14:50'),
(60, 29, 65, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD149\" ', '2020-04-18 04:16:13'),
(61, 29, 66, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD107\" ', '2020-04-18 04:16:48'),
(62, 29, 62, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD147\" ', '2020-04-18 04:19:53'),
(63, 28, 77, 'menginputkan data konsumen dengan kode order \"PKMD152\"', '2020-04-22 01:01:33'),
(64, 29, 77, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD152\" ', '2020-04-22 02:29:47'),
(65, 27, 62, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD147\" ', '2020-04-22 03:01:13'),
(66, 27, 66, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD107\" ', '2020-04-22 03:01:22'),
(67, 27, 65, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD149\" ', '2020-04-22 03:01:28'),
(68, 27, 72, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD247\" ', '2020-04-22 03:01:33'),
(69, 27, 77, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD152\" ', '2020-04-23 05:47:48'),
(70, 29, 68, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD246\" ', '2020-04-24 01:19:05'),
(71, 29, 57, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD123r\" ', '2020-04-24 01:20:16'),
(72, 29, 69, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD245\" ', '2020-04-24 01:20:50'),
(73, 29, 73, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD249\" ', '2020-04-24 01:21:47'),
(74, 29, 74, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD150\" ', '2020-04-24 01:22:34'),
(75, 29, 77, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD152\" ', '2020-04-24 01:22:54'),
(76, 29, 62, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD147\" ', '2020-04-24 01:23:09'),
(77, 29, 66, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD107\" ', '2020-04-24 01:25:58'),
(78, 29, 65, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD149\" ', '2020-04-24 01:27:41'),
(79, 29, 72, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD247\" ', '2020-04-24 01:28:10'),
(80, 29, 67, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD244\" ', '2020-04-24 01:28:27'),
(81, 29, 76, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD151\" ', '2020-04-24 01:28:55'),
(82, 29, 75, 'telah menyelesaikan pasang kusen untuk kode order \"ADG10\" ', '2020-04-24 01:30:44'),
(83, 28, 78, 'menginputkan data konsumen dengan kode order \"PGMD251\"', '2020-04-25 02:16:35'),
(84, 29, 78, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD251\" ', '2020-04-27 07:01:43'),
(85, 28, 79, 'menginputkan data konsumen dengan kode order \"PGMD250\"', '2020-04-27 08:00:29'),
(86, 27, 78, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD251\" ', '2020-04-30 22:50:18'),
(87, 29, 79, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD250\" ', '2020-05-01 11:46:03'),
(88, 28, 79, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD250\" ', '2020-05-04 00:58:00'),
(89, 28, 80, 'menginputkan data konsumen dengan kode order \"PGMD252\"', '2020-05-04 02:08:00'),
(90, 28, 81, 'menginputkan data konsumen dengan kode order \"PGMD248\"', '2020-05-04 02:53:26'),
(91, 29, 78, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD251\" ', '2020-05-04 02:58:09'),
(92, 29, 79, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD250\" ', '2020-05-04 03:11:37'),
(93, 29, 81, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD248\" ', '2020-05-04 11:56:37'),
(94, 29, 80, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD252\" ', '2020-05-04 11:57:31'),
(95, 28, 81, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD248\" ', '2020-05-05 01:33:53'),
(96, 27, 80, 'menolak jadwal pasang kusen untuk kode order \"PGMD252\" ', '2020-05-05 02:21:37'),
(97, 29, 81, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD248\" ', '2020-05-05 02:23:06'),
(98, 29, 80, 'menerima perubahan jadwal pasang kusen untuk kode order \"PGMD252\" ', '2020-05-05 02:23:58'),
(99, 28, 82, 'menginputkan data konsumen dengan kode order \"PKMD153\"', '2020-05-06 07:26:25'),
(100, 29, 65, 'menolak perubahan jadwal pasang finish untuk kode order \"PKMD149\" ', '2020-05-07 02:31:54'),
(101, 28, 83, 'menginputkan data konsumen dengan kode order \"PKMD154\"', '2020-05-07 05:07:17'),
(102, 29, 82, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD153\" ', '2020-05-08 02:47:20'),
(103, 28, 82, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD153\" ', '2020-05-08 03:15:32'),
(104, 28, 84, 'menginputkan data konsumen dengan kode order \"PKMD155\"', '2020-05-08 03:56:04'),
(105, 29, 84, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD155\" ', '2020-05-09 01:20:09'),
(106, 28, 89, 'menginputkan data konsumen dengan kode order \"PKBT01\"', '2020-05-09 02:47:46'),
(107, 29, 89, 'mengajukan jadwal pasang kusen untuk kode order \"PKBT01\" ', '2020-05-09 03:48:45'),
(108, 29, 80, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD252\" ', '2020-05-10 01:38:41'),
(109, 28, 89, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKBT01\" ', '2020-05-10 03:02:18'),
(110, 28, 84, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD155\" ', '2020-05-10 03:02:33'),
(111, 29, 83, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD154\" ', '2020-05-11 01:56:02'),
(112, 28, 90, 'menginputkan data konsumen dengan kode order \"PGMD253\"', '2020-05-11 08:01:12'),
(113, 29, 89, 'telah menyelesaikan pasang kusen untuk kode order \"PKBT01\" ', '2020-05-13 01:26:42'),
(114, 29, 90, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD253\" ', '2020-05-13 01:27:35'),
(115, 29, 82, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD153\" ', '2020-05-13 01:29:22'),
(116, 29, 84, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD155\" ', '2020-05-13 01:29:50'),
(117, 28, 90, 'menolak jadwal pasang kusen untuk kode order \"PGMD253\" ', '2020-05-13 02:05:42'),
(118, 28, 83, 'menolak jadwal pasang kusen untuk kode order \"PKMD154\" ', '2020-05-16 03:37:24'),
(119, 29, 83, 'menolak perubahan jadwal pasang kusen untuk kode order \"PKMD154\" ', '2020-05-16 03:56:53'),
(120, 28, 83, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD154\" ', '2020-05-16 04:23:19'),
(121, 29, 90, 'menolak perubahan jadwal pasang kusen untuk kode order \"PGMD253\" ', '2020-05-16 08:21:15'),
(122, 28, 90, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD253\" ', '2020-05-16 08:21:50'),
(123, 28, 91, 'menginputkan data konsumen dengan kode order \"PKMD157\"', '2020-05-18 08:25:21'),
(124, 29, 91, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD157\" ', '2020-05-18 13:45:47'),
(125, 28, 92, 'menginputkan data konsumen dengan kode order \"PGMD254\"', '2020-05-19 03:25:21'),
(126, 29, 92, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD254\" ', '2020-05-19 07:21:23'),
(127, 28, 93, 'menginputkan data konsumen dengan kode order \"PKMD158\"', '2020-05-20 02:32:10'),
(128, 27, 92, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD254\" ', '2020-05-20 08:21:31'),
(129, 27, 91, 'menolak jadwal pasang kusen untuk kode order \"PKMD157\" ', '2020-05-20 08:53:02'),
(130, 29, 83, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD154\" ', '2020-05-20 11:00:03'),
(131, 29, 92, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD254\" ', '2020-05-20 11:00:20'),
(132, 29, 93, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD158\" ', '2020-05-20 11:00:58'),
(133, 27, 93, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD158\" ', '2020-05-21 03:33:59'),
(134, 29, 91, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD157\" ', '2020-05-21 10:07:09'),
(135, 29, 91, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD157\" ', '2020-05-21 10:07:22'),
(136, 28, 94, 'menginputkan data konsumen dengan kode order \"PKMD159\"', '2020-05-22 23:41:44'),
(137, 27, 93, 'melakukan reschedule jadwal pasang kusen untuk kode order \"PKMD158\" ', '2020-05-28 09:17:47'),
(138, 38, 98, 'menginputkan data konsumen dengan kode order \"PGMD255\"', '2020-05-28 09:20:51'),
(139, 38, 99, 'menginputkan data konsumen dengan kode order \"PGMD257\"', '2020-05-28 09:24:47'),
(140, 38, 100, 'menginputkan data konsumen dengan kode order \"PKMD160\"', '2020-05-28 09:29:50'),
(141, 38, 101, 'menginputkan data konsumen dengan kode order \"PGMD256\"', '2020-05-28 09:37:56'),
(142, 29, 98, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD255\" ', '2020-05-28 14:49:53'),
(143, 29, 99, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD257\" ', '2020-05-28 14:50:49'),
(144, 29, 101, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD256\" ', '2020-05-28 14:51:11'),
(145, 29, 93, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD158\" ', '2020-05-28 15:08:55'),
(146, 29, 93, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD158\" ', '2020-05-28 15:09:09'),
(147, 28, 102, 'menginputkan data konsumen dengan kode order \"MK01\"', '2020-05-29 01:57:34'),
(148, 38, 101, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD256\" ', '2020-05-29 02:50:36'),
(149, 38, 99, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD257\" ', '2020-05-29 02:50:40'),
(150, 38, 98, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD255\" ', '2020-05-29 02:50:42'),
(151, 29, 102, 'mengajukan jadwal pasang kusen untuk kode order \"MK01\" ', '2020-05-29 12:34:25'),
(152, 27, 102, 'menerima pengajuan jadwal pasang kusen untuk kode order \"MK01\" ', '2020-05-29 15:58:13'),
(153, 29, 99, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD257\" ', '2020-06-02 04:25:43'),
(154, 29, 98, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD255\" ', '2020-06-02 04:25:54'),
(155, 29, 101, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD256\" ', '2020-06-02 04:26:01'),
(156, 27, 103, 'menginputkan data konsumen dengan kode order \"PKMD162\"', '2020-06-02 05:47:03'),
(157, 38, 104, 'menginputkan data konsumen dengan kode order \"PKMD161\"', '2020-06-02 06:54:25'),
(158, 28, 102, 'melakukan reschedule jadwal pasang kusen untuk kode order \"MK01\" ', '2020-06-04 01:57:29'),
(159, 38, 111, 'menginputkan data konsumen dengan kode order \"PGMD258\"', '2020-06-04 02:02:39'),
(160, 29, 102, 'menerima perubahan jadwal pasang kusen untuk kode order \"MK01\" ', '2020-06-04 02:38:23'),
(161, 29, 102, 'telah menyelesaikan pasang kusen untuk kode order \"MK01\" ', '2020-06-04 02:38:28'),
(162, 29, 94, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD159\" ', '2020-06-04 03:17:06'),
(163, 29, 104, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD161\" ', '2020-06-04 03:17:44'),
(164, 29, 103, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD162\" ', '2020-06-04 03:30:02'),
(165, 38, 112, 'menginputkan data konsumen dengan kode order \"KSGK39\"', '2020-06-04 09:27:41'),
(166, 27, 94, 'menolak jadwal pasang kusen untuk kode order \"PKMD159\" ', '2020-06-04 11:04:52'),
(167, 27, 104, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD161\" ', '2020-06-04 11:05:00'),
(168, 27, 103, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD162\" ', '2020-06-04 14:57:54'),
(169, 29, 111, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD258\" ', '2020-06-04 15:10:04'),
(170, 29, 94, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD159\" ', '2020-06-04 15:10:30'),
(171, 29, 94, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD159\" ', '2020-06-04 15:10:34'),
(172, 27, 111, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD258\" ', '2020-06-04 15:25:01'),
(173, 29, 112, 'mengajukan jadwal pasang kusen untuk kode order \"KSGK39\" ', '2020-06-05 06:22:09'),
(174, 27, 112, 'menerima pengajuan jadwal pasang kusen untuk kode order \"KSGK39\" ', '2020-06-05 06:44:47'),
(175, 38, 113, 'menginputkan data konsumen dengan kode order \"ADK05\"', '2020-06-06 01:22:11'),
(176, 38, 114, 'menginputkan data konsumen dengan kode order \"PGMD259\"', '2020-06-06 07:54:21'),
(177, 38, 115, 'menginputkan data konsumen dengan kode order \"PKMD128\"', '2020-06-06 08:05:51'),
(178, 29, 104, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD161\" ', '2020-06-06 10:45:52'),
(179, 29, 115, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD128\" ', '2020-06-06 10:47:53'),
(180, 27, 115, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD128\" ', '2020-06-06 11:16:00'),
(181, 29, 115, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD128\" ', '2020-06-06 22:44:20'),
(182, 29, 112, 'telah menyelesaikan pasang kusen untuk kode order \"KSGK39\" ', '2020-06-07 01:26:05'),
(183, 29, 113, 'mengajukan jadwal pasang kusen untuk kode order \"ADK05\" ', '2020-06-07 02:35:17'),
(184, 29, 114, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD259\" ', '2020-06-07 02:35:34'),
(185, 27, 114, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD259\" ', '2020-06-07 02:41:09'),
(186, 27, 113, 'menerima pengajuan jadwal pasang kusen untuk kode order \"ADK05\" ', '2020-06-07 02:41:14'),
(187, 38, 116, 'menginputkan data konsumen dengan kode order \"KSGK40\"', '2020-06-08 02:16:07'),
(188, 29, 116, 'mengajukan jadwal pasang kusen untuk kode order \"KSGK40\" ', '2020-06-08 03:02:25'),
(189, 29, 80, 'menolak perubahan jadwal pasang finish untuk kode order \"PGMD252\" ', '2020-06-08 03:02:48'),
(190, 28, 116, 'menerima pengajuan jadwal pasang kusen untuk kode order \"KSGK40\" ', '2020-06-08 03:27:59'),
(191, 29, 90, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD253\" ', '2020-06-08 11:28:54'),
(192, 29, 111, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD258\" ', '2020-06-08 11:29:05'),
(193, 29, 113, 'telah menyelesaikan pasang kusen untuk kode order \"ADK05\" ', '2020-06-08 11:29:13'),
(194, 29, 114, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD259\" ', '2020-06-08 11:29:21'),
(195, 29, 116, 'telah menyelesaikan pasang kusen untuk kode order \"KSGK40\" ', '2020-06-08 11:29:29'),
(196, 29, 103, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD162\" ', '2020-06-09 01:33:51'),
(197, 38, 117, 'menginputkan data konsumen dengan kode order \"KSGK26\"', '2020-06-09 02:24:43'),
(198, 38, 118, 'menginputkan data konsumen dengan kode order \"PKMD163\"', '2020-06-09 09:32:52'),
(199, 38, 119, 'menginputkan data konsumen dengan kode order \"PKMD164\"', '2020-06-10 06:14:17'),
(200, 29, 117, 'mengajukan jadwal pasang kusen untuk kode order \"KSGK26\" ', '2020-06-12 01:56:30'),
(201, 29, 118, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD163\" ', '2020-06-12 01:56:52'),
(202, 29, 119, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD164\" ', '2020-06-12 01:57:12'),
(203, 27, 118, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD163\" ', '2020-06-12 02:02:55'),
(204, 27, 117, 'menerima pengajuan jadwal pasang kusen untuk kode order \"KSGK26\" ', '2020-06-12 02:03:17'),
(205, 27, 119, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD164\" ', '2020-06-12 02:03:23'),
(206, 27, 120, 'menginputkan data konsumen dengan kode order \"PKMd150b\"', '2020-06-15 15:24:07'),
(207, 27, 121, 'menginputkan data konsumen dengan kode order \"PKMD156\"', '2020-06-15 15:27:27'),
(208, 29, 121, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD156\" ', '2020-06-16 02:51:52'),
(209, 29, 120, 'mengajukan jadwal pasang kusen untuk kode order \"PKMd150b\" ', '2020-06-16 02:52:40'),
(210, 27, 120, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMd150b\" ', '2020-06-16 03:08:52'),
(211, 38, 126, 'menginputkan data konsumen dengan kode order \"PKMD165\"', '2020-06-16 06:33:08'),
(212, 29, 117, 'telah menyelesaikan pasang kusen untuk kode order \"KSGK26\" ', '2020-06-18 06:30:16'),
(213, 29, 119, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD164\" ', '2020-06-18 06:30:22'),
(214, 29, 120, 'telah menyelesaikan pasang kusen untuk kode order \"PKMd150b\" ', '2020-06-18 06:30:43'),
(215, 29, 126, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD165\" ', '2020-06-18 07:25:34'),
(216, 29, 118, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD163\" ', '2020-06-18 07:28:47'),
(217, 38, 127, 'menginputkan data konsumen dengan kode order \"PGMD260\"', '2020-06-19 06:31:05'),
(218, 38, 128, 'menginputkan data konsumen dengan kode order \"KSGK41\"', '2020-06-19 06:33:36'),
(219, 27, 126, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD165\" ', '2020-06-20 01:01:29'),
(220, 38, 129, 'menginputkan data konsumen dengan kode order \"PKMD166\"', '2020-06-20 08:35:25'),
(221, 29, 128, 'mengajukan jadwal pasang kusen untuk kode order \"KSGK41\" ', '2020-06-21 06:33:17'),
(222, 27, 121, 'menolak jadwal pasang kusen untuk kode order \"PKMD156\" ', '2020-06-21 06:33:22'),
(223, 29, 129, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD166\" ', '2020-06-21 06:33:35'),
(224, 27, 129, 'menolak jadwal pasang kusen untuk kode order \"PKMD166\" ', '2020-06-21 06:35:14'),
(225, 29, 121, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD156\" ', '2020-06-21 06:58:24'),
(226, 29, 129, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD166\" ', '2020-06-21 06:58:29'),
(227, 27, 130, 'menginputkan data konsumen dengan kode order \"PKMD120\"', '2020-06-21 07:00:45'),
(228, 27, 131, 'menginputkan data konsumen dengan kode order \"PRIORITAS\"', '2020-06-22 03:08:34'),
(229, 27, 131, 'Pesanan dihapus', '2020-06-22 03:19:08'),
(230, 27, 130, 'Pesanan dihapus', '2020-06-22 03:19:14'),
(231, 27, 132, 'menginputkan data konsumen dengan kode order \"KSFG17\"', '2020-06-22 03:25:09'),
(232, 38, 129, 'melakukan reschedule jadwal pasang kusen untuk kode order \"PKMD166\" ', '2020-06-24 05:54:15'),
(233, 29, 129, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD166\" ', '2020-06-24 08:27:42'),
(234, 29, 129, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD166\" ', '2020-06-24 08:27:53'),
(235, 29, 115, 'menolak perubahan jadwal pasang finish untuk kode order \"PKMD128\" ', '2020-06-24 08:28:21'),
(236, 29, 127, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD260\" ', '2020-06-24 08:30:10'),
(237, 29, 132, 'mengajukan jadwal pasang kusen untuk kode order \"KSFG17\" ', '2020-06-24 08:30:37'),
(238, 38, 142, 'menginputkan data konsumen dengan kode order \"PGMD262\"', '2020-06-24 08:55:19'),
(239, 27, 127, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD260\" ', '2020-06-24 08:56:30'),
(240, 27, 132, 'menerima pengajuan jadwal pasang kusen untuk kode order \"KSFG17\" ', '2020-06-24 08:56:42'),
(241, 27, 141, 'Pesanan dihapus', '2020-06-24 09:06:08'),
(242, 38, 144, 'menginputkan data konsumen dengan kode order \"PKMD151B\"', '2020-06-24 10:24:17'),
(243, 29, 132, 'telah menyelesaikan pasang kusen untuk kode order \"KSFG17\" ', '2020-06-26 08:36:53'),
(244, 29, 121, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD156\" ', '2020-06-26 11:58:19'),
(245, 38, 145, 'menginputkan data konsumen dengan kode order \"PGMD261\"', '2020-06-26 15:30:46'),
(246, 29, 145, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD261\" ', '2020-06-27 02:15:42'),
(247, 38, 145, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD261\" ', '2020-06-27 02:24:06'),
(248, 29, 126, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD165\" ', '2020-06-27 06:40:32'),
(249, 29, 127, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD260\" ', '2020-06-28 09:24:41'),
(250, 29, 145, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD261\" ', '2020-06-28 09:24:54'),
(251, 29, 142, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD262\" ', '2020-06-30 11:52:44'),
(252, 38, 142, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD262\" ', '2020-07-01 04:31:29'),
(253, 38, 146, 'menginputkan data konsumen dengan kode order \"KSFG70\"', '2020-07-01 06:37:44'),
(255, 29, 121, 'menolak perubahan jadwal pasang finish untuk kode order \"PKMD156\" ', '2020-07-03 14:14:02'),
(256, 29, 126, 'menolak perubahan jadwal pasang finish untuk kode order \"PKMD165\" ', '2020-07-03 14:17:39'),
(257, 29, 146, 'mengajukan jadwal pasang kusen untuk kode order \"KSFG70\" ', '2020-07-03 14:18:23'),
(258, 28, 146, 'menerima pengajuan jadwal pasang kusen untuk kode order \"KSFG70\" ', '2020-07-03 14:39:18'),
(259, 29, 142, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD262\" ', '2020-07-04 01:24:24'),
(260, 29, 126, 'menolak perubahan jadwal pasang finish untuk kode order \"PKMD165\" ', '2020-07-04 01:25:44'),
(262, 29, 144, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD151B\" ', '2020-07-04 14:48:00'),
(264, 27, 144, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD151B\" ', '2020-07-05 04:35:33'),
(265, 38, 115, 'mengubah gambar kerja untuk kode order \"PKMD128\" ', '2020-07-05 04:53:48'),
(266, 29, 144, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD151B\" ', '2020-07-06 02:59:47'),
(267, 38, 116, 'mengubah gambar kerja untuk kode order \"KSGK40\" ', '2020-07-06 03:25:46'),
(268, 38, 116, 'mengubah gambar kerja untuk kode order \"KSGK40\" ', '2020-07-06 03:26:35'),
(269, 29, 146, 'telah menyelesaikan pasang kusen untuk kode order \"KSFG70\" ', '2020-07-06 14:12:23'),
(272, 38, 149, 'menginputkan data konsumen dengan kode order \"PKMD167\"', '2020-07-10 08:21:47'),
(273, 38, 150, 'menginputkan data konsumen dengan kode order \"PKMD168\"', '2020-07-10 08:46:53'),
(274, 38, 151, 'menginputkan data konsumen dengan kode order \"KSFG71\"', '2020-07-11 04:41:37'),
(275, 29, 151, 'mengajukan jadwal pasang kusen untuk kode order \"KSFG71\" ', '2020-07-11 04:45:44'),
(276, 29, 150, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD168\" ', '2020-07-11 04:47:11'),
(277, 27, 150, 'menolak jadwal pasang kusen untuk kode order \"PKMD168\" ', '2020-07-11 05:11:02'),
(278, 38, 151, 'menolak jadwal pasang kusen untuk kode order \"KSFG71\" ', '2020-07-11 05:24:00'),
(279, 29, 151, 'menerima perubahan jadwal pasang kusen untuk kode order \"KSFG71\" ', '2020-07-11 08:03:49'),
(280, 29, 150, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD168\" ', '2020-07-11 08:03:56'),
(281, 29, 126, 'menolak perubahan jadwal pasang finish untuk kode order \"PKMD165\" ', '2020-07-13 03:56:22'),
(282, 38, 152, 'menginputkan data konsumen dengan kode order \"PKMD170\"', '2020-07-13 07:22:49'),
(283, 29, 149, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD167\" ', '2020-07-14 05:45:07'),
(284, 38, 153, 'menginputkan data konsumen dengan kode order \"PKMD169\"', '2020-07-14 06:31:41'),
(285, 38, 150, 'melakukan reschedule jadwal pasang kusen untuk kode order \"PKMD168\" ', '2020-07-14 06:56:47'),
(286, 27, 100, 'mengubah gambar kerja untuk kode order \"PKMD160\" ', '2020-07-14 07:28:08'),
(287, 38, 94, 'mengubah gambar kerja untuk kode order \"PKMD159\" ', '2020-07-14 08:33:45'),
(288, 38, 94, 'mengubah gambar kerja untuk kode order \"PKMD159\" ', '2020-07-14 08:33:48'),
(289, 29, 150, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD168\" ', '2020-07-15 01:10:34'),
(290, 38, 153, 'mengubah gambar kerja untuk kode order \"PKMD169\" ', '2020-07-15 04:32:51'),
(291, 38, 152, 'mengubah gambar kerja untuk kode order \"PKMD170\" ', '2020-07-15 04:33:32'),
(292, 29, 152, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD170\" ', '2020-07-16 04:53:18'),
(293, 27, 152, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD170\" ', '2020-07-16 06:39:08'),
(294, 27, 149, 'menolak jadwal pasang kusen untuk kode order \"PKMD167\" ', '2020-07-16 06:39:26'),
(295, 38, 157, 'menginputkan data konsumen dengan kode order \"PKMD171\"', '2020-07-16 08:48:59'),
(296, 29, 94, 'menolak perubahan jadwal pasang finish untuk kode order \"PKMD159\" ', '2020-07-17 00:53:14'),
(297, 29, 149, 'menolak perubahan jadwal pasang kusen untuk kode order \"PKMD167\" ', '2020-07-17 00:55:01'),
(298, 38, 152, 'melakukan reschedule jadwal pasang kusen untuk kode order \"PKMD170\" ', '2020-07-17 10:33:20'),
(299, 29, 100, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD160\" ', '2020-07-20 05:38:10'),
(300, 29, 153, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD169\" ', '2020-07-20 05:38:44'),
(301, 29, 157, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD171\" ', '2020-07-20 05:43:38'),
(302, 38, 100, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD160\" ', '2020-07-20 06:49:10'),
(303, 29, 150, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD168\" ', '2020-07-20 07:03:17'),
(304, 29, 152, 'menolak perubahan jadwal pasang kusen untuk kode order \"PKMD170\" ', '2020-07-20 07:03:55'),
(305, 27, 152, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD170\" ', '2020-07-20 07:06:51'),
(306, 27, 149, 'menolak jadwal pasang kusen untuk kode order \"PKMD167\" ', '2020-07-20 14:04:42'),
(307, 28, 153, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD169\" ', '2020-07-20 14:09:33'),
(308, 29, 149, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD167\" ', '2020-07-21 00:29:18'),
(309, 29, 149, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD167\" ', '2020-07-21 00:29:23'),
(310, 38, 171, 'menginputkan data konsumen dengan kode order \"PKMD172\"', '2020-07-21 00:41:23'),
(311, 38, 172, 'menginputkan data konsumen dengan kode order \"PGMD263\"', '2020-07-21 01:30:26'),
(312, 29, 172, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD263\" ', '2020-07-21 01:46:13'),
(313, 27, 172, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD263\" ', '2020-07-21 06:30:30'),
(314, 27, 118, 'Pesanan dihapus', '2020-07-21 06:39:34'),
(315, 29, 171, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD172\" ', '2020-07-22 02:38:31'),
(316, 27, 171, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD172\" ', '2020-07-22 07:03:44'),
(317, 29, 172, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD263\" ', '2020-07-22 07:16:45'),
(318, 29, 151, 'telah menyelesaikan pasang kusen untuk kode order \"KSFG71\" ', '2020-07-22 07:17:05'),
(319, 38, 157, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD171\" ', '2020-07-22 08:16:37'),
(320, 38, 157, 'melakukan reschedule jadwal pasang kusen untuk kode order \"PKMD171\" ', '2020-07-22 08:17:20'),
(321, 29, 157, 'menolak perubahan jadwal pasang kusen untuk kode order \"PKMD171\" ', '2020-07-22 09:21:00'),
(322, 38, 100, 'mengubah gambar kerja untuk kode order \"PKMD160\" ', '2020-07-24 04:12:23'),
(323, 38, 172, 'mengubah gambar kerja untuk kode order \"PGMD263\" ', '2020-07-24 08:06:38'),
(324, 29, 100, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD160\" ', '2020-07-24 09:53:01'),
(325, 29, 153, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD169\" ', '2020-07-24 09:53:09'),
(326, 29, 171, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD172\" ', '2020-07-25 14:59:02'),
(327, 29, 152, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD170\" ', '2020-07-26 06:37:02'),
(328, 38, 157, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD171\" ', '2020-07-27 05:15:44'),
(329, 38, 157, 'melakukan reschedule jadwal pasang kusen untuk kode order \"PKMD171\" ', '2020-07-27 05:16:49'),
(330, 29, 157, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD171\" ', '2020-07-27 06:01:20'),
(331, 29, 157, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD171\" ', '2020-07-28 06:41:05'),
(332, 38, 173, 'menginputkan data konsumen dengan kode order \"PGMW10\"', '2020-08-04 08:59:56'),
(333, 38, 177, 'menginputkan data konsumen dengan kode order \"PKMD173\"', '2020-08-06 02:53:42'),
(334, 29, 177, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD173\" ', '2020-08-07 02:57:01'),
(335, 38, 177, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD173\" ', '2020-08-07 03:12:59'),
(336, 29, 177, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD173\" ', '2020-08-11 11:34:44'),
(337, 29, 173, 'mengajukan jadwal pasang kusen untuk kode order \"PGMW10\" ', '2020-08-12 04:50:04'),
(338, 38, 179, 'menginputkan data konsumen dengan kode order \"PGMD264\"', '2020-08-12 11:08:58'),
(339, 38, 173, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMW10\" ', '2020-08-13 02:32:53'),
(340, 38, 180, 'menginputkan data konsumen dengan kode order \"PKMD174\"', '2020-08-15 02:59:15'),
(341, 29, 180, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD174\" ', '2020-08-15 07:18:34'),
(342, 29, 179, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD264\" ', '2020-08-15 07:19:00'),
(343, 27, 179, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD264\" ', '2020-08-15 11:22:02'),
(344, 27, 180, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD174\" ', '2020-08-15 11:41:16'),
(345, 27, 181, 'menginputkan data konsumen dengan kode order \"KSGK BU REVI\"', '2020-08-16 05:43:22'),
(346, 27, 181, 'Pesanan dihapus', '2020-08-16 05:43:45'),
(347, 29, 180, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD174\" ', '2020-08-17 04:23:25'),
(348, 38, 179, 'melakukan reschedule jadwal pasang kusen untuk kode order \"PGMD264\" ', '2020-08-18 05:47:32'),
(349, 29, 179, 'menerima perubahan jadwal pasang kusen untuk kode order \"PGMD264\" ', '2020-08-18 05:53:04'),
(350, 38, 100, 'mengubah gambar kerja untuk kode order \"PKMD160\" ', '2020-08-18 11:47:34'),
(351, 38, 152, 'mengubah gambar kerja untuk kode order \"PKMD170\" ', '2020-08-18 11:48:02'),
(352, 38, 152, 'mengubah gambar kerja untuk kode order \"PKMD170\" ', '2020-08-18 11:48:18'),
(353, 29, 173, 'telah menyelesaikan pasang kusen untuk kode order \"PGMW10\" ', '2020-08-18 13:06:53'),
(354, 38, 183, 'menginputkan data konsumen dengan kode order \"PGMD265\"', '2020-08-21 08:34:50'),
(355, 38, 184, 'menginputkan data konsumen dengan kode order \"PGMD266\"', '2020-08-21 12:23:08'),
(356, 29, 184, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD266\" ', '2020-08-22 01:32:49'),
(357, 38, 152, 'mengubah gambar kerja untuk kode order \"PKMD170\" ', '2020-08-22 07:22:44'),
(358, 28, 185, 'menginputkan data konsumen dengan kode order \"PKMD175\"', '2020-08-22 10:54:27'),
(359, 27, 184, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD266\" ', '2020-08-22 11:05:34'),
(360, 29, 185, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD175\" ', '2020-08-22 12:35:30'),
(361, 29, 183, 'mengajukan jadwal pasang kusen untuk kode order \"PGMD265\" ', '2020-08-22 13:10:38'),
(362, 27, 185, 'menolak jadwal pasang kusen untuk kode order \"PKMD175\" ', '2020-08-22 13:23:05'),
(363, 29, 184, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD266\" ', '2020-08-22 13:26:17'),
(364, 29, 179, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD264\" ', '2020-08-22 13:26:28'),
(365, 27, 183, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PGMD265\" ', '2020-08-22 14:27:35'),
(366, 29, 185, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD175\" ', '2020-08-22 15:03:50'),
(367, 29, 185, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD175\" ', '2020-08-22 15:03:54'),
(368, 38, 185, 'mengubah gambar kerja untuk kode order \"PKMD175\" ', '2020-08-24 06:47:56'),
(369, 28, 183, 'mengubah gambar kerja untuk kode order \"PGMD265\" ', '2020-08-24 08:40:11'),
(370, 28, 183, 'mengubah gambar kerja untuk kode order \"PGMD265\" ', '2020-08-24 08:40:14'),
(371, 28, 183, 'mengubah gambar kerja untuk kode order \"PGMD265\" ', '2020-08-24 08:40:17'),
(372, 29, 185, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD175\" ', '2020-08-24 08:43:45'),
(373, 27, 185, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD175\" ', '2020-08-24 08:54:25'),
(374, 38, 183, 'mengubah gambar kerja untuk kode order \"PGMD265\" ', '2020-08-24 09:34:39'),
(375, 38, 128, 'menolak jadwal pasang kusen untuk kode order \"KSGK41\" ', '2020-08-24 09:42:06'),
(376, 38, 198, 'menginputkan data konsumen dengan kode order \"ADK08\"', '2020-08-25 04:34:45'),
(377, 29, 183, 'telah menyelesaikan pasang kusen untuk kode order \"PGMD265\" ', '2020-08-25 05:08:24'),
(378, 29, 128, 'menolak perubahan jadwal pasang kusen untuk kode order \"KSGK41\" ', '2020-08-25 10:12:56'),
(379, 29, 198, 'mengajukan jadwal pasang kusen untuk kode order \"ADK08\" ', '2020-08-25 10:15:27'),
(380, 38, 149, 'mengubah gambar kerja untuk kode order \"PKMD167\" ', '2020-08-25 10:33:16'),
(381, 38, 149, 'mengubah gambar kerja untuk kode order \"PKMD167\" ', '2020-08-25 10:33:18'),
(382, 38, 149, 'mengubah gambar kerja untuk kode order \"PKMD167\" ', '2020-08-25 10:33:38'),
(383, 38, 149, 'mengubah gambar kerja untuk kode order \"PKMD167\" ', '2020-08-25 10:33:57'),
(384, 27, 198, 'menerima pengajuan jadwal pasang kusen untuk kode order \"ADK08\" ', '2020-08-26 04:39:27'),
(385, 29, 149, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD167\" ', '2020-08-26 07:20:02'),
(386, 29, 198, 'telah menyelesaikan pasang kusen untuk kode order \"ADK08\" ', '2020-08-26 10:08:40'),
(387, 27, 128, 'menerima pengajuan jadwal pasang kusen untuk kode order \"KSGK41\" ', '2020-08-28 00:59:18'),
(388, 27, 149, 'menerima pengajuan jadwal pasang kusen untuk kode order \"PKMD167\" ', '2020-08-28 01:08:07'),
(389, 29, 185, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD175\" ', '2020-08-28 02:59:31'),
(390, 38, 149, 'melakukan reschedule jadwal pasang kusen untuk kode order \"PKMD167\" ', '2020-08-31 08:13:23'),
(391, 38, 199, 'menginputkan data konsumen dengan kode order \"PKMD176\"', '2020-08-31 08:15:21'),
(392, 29, 128, 'telah menyelesaikan pasang kusen untuk kode order \"KSGK41\" ', '2020-08-31 11:17:04'),
(393, 29, 149, 'menerima perubahan jadwal pasang kusen untuk kode order \"PKMD167\" ', '2020-08-31 11:17:16'),
(394, 16, 200, 'menginputkan data konsumen dengan kode order \"TESTINGEDO\"', '2021-01-11 20:35:14'),
(395, 16, 201, 'menginputkan data konsumen dengan kode order \"TESTINGNEW\"', '2021-01-11 20:39:55'),
(396, 30, 200, 'mengajukan jadwal pasang kusen untuk kode order \"TESTINGEDO\" ', '2021-02-01 19:19:13'),
(397, 35, 177, 'menolak jadwal pasang finish untuk kode order \"PKMD173\" ', '2021-02-06 19:52:26'),
(398, 30, 201, 'mengajukan jadwal pasang kusen untuk kode order \"TESTINGNEW\" ', '2021-02-07 14:06:47'),
(399, 35, 202, 'menginputkan data konsumen dengan kode order \"TESTINGORDER\"', '2021-02-07 14:37:58'),
(400, 30, 202, 'mengajukan jadwal pasang kusen untuk kode order \"TESTINGORDER\" ', '2021-02-07 15:18:05'),
(401, 35, 202, 'menolak jadwal pasang kusen untuk kode order \"TESTINGORDER\" ', '2021-02-07 15:55:25'),
(402, 30, 202, 'menolak perubahan jadwal pasang kusen untuk kode order \"TESTINGORDER\" ', '2021-02-07 16:13:36'),
(403, 35, 202, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESTINGORDER\" ', '2021-02-07 16:54:48'),
(404, 30, 149, 'telah menyelesaikan pasang kusen untuk kode order \"PKMD167\" ', '2021-02-09 20:09:18'),
(405, 30, 202, 'telah menyelesaikan pasang kusen untuk kode order \"TESTINGORDER\" ', '2021-02-12 15:42:33'),
(406, 35, 202, 'menerima pengajuan jadwal pasang finish untuk kode order \"TESTINGORDER\" ', '2021-02-12 15:50:01'),
(407, 999, 200, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESTINGEDO\" ', '2021-02-14 02:30:20'),
(408, 999, 200, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESTINGEDO\" ', '2021-02-14 02:31:16'),
(409, 999, 201, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESTINGNEW\" ', '2021-02-14 02:40:28'),
(410, 35, 203, 'menginputkan data konsumen dengan kode order \"TESTINGKONSUMEN\"', '2021-02-14 02:41:54'),
(411, 30, 203, 'mengajukan jadwal pasang kusen untuk kode order \"TESTINGKONSUMEN\" ', '2021-02-14 02:42:23'),
(412, 999, 203, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESTINGKONSUMEN\" ', '2021-02-14 02:46:01'),
(413, 35, 204, 'menginputkan data konsumen dengan kode order \"TESTINGKONSUMEN2\"', '2021-02-14 02:49:10'),
(414, 30, 204, 'mengajukan jadwal pasang kusen untuk kode order \"TESTINGKONSUMEN2\" ', '2021-02-14 02:50:06'),
(415, 999, 204, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESTINGKONSUMEN2\" ', '2021-02-14 02:51:07'),
(416, 30, 204, 'telah menyelesaikan pasang kusen untuk kode order \"TESTINGKONSUMEN2\" ', '2021-02-14 02:51:52'),
(417, 999, 204, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESTINGKONSUMEN2\" ', '2021-02-14 02:54:02'),
(418, 35, 203, 'melakukan reschedule jadwal pasang kusen untuk kode order \"TESTINGKONSUMEN\" ', '2021-02-14 02:58:33'),
(419, 30, 203, 'menolak perubahan jadwal pasang kusen untuk kode order \"TESTINGKONSUMEN\" ', '2021-02-14 02:59:47'),
(420, 999, 203, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESTINGKONSUMEN\" ', '2021-02-14 03:00:35'),
(421, 35, 204, 'melakukan reschedule jadwal pasang finish untuk kode order \"TESTINGKONSUMEN2\" ', '2021-02-14 03:28:22'),
(422, 35, 204, 'telah menyelesaikan pasang finish untuk kode order \"TESTINGKONSUMEN2\" ', '2021-02-14 03:28:22'),
(423, 30, 177, 'menolak perubahan jadwal pasang finish untuk kode order \"PKMD173\" ', '2021-02-14 03:29:35'),
(424, 30, 204, 'menolak perubahan jadwal pasang finish untuk kode order \"TESTINGKONSUMEN2\" ', '2021-02-14 03:30:49'),
(425, 999, 204, 'menolak jadwal pasang kusen untuk kode order \"TESTINGKONSUMEN2\" ', '2021-02-14 03:35:55'),
(426, 30, 204, 'telah menyelesaikan pasang finish untuk kode order \"TESTINGKONSUMEN2\" ', '2021-02-17 12:39:42'),
(427, 35, 223, 'menginputkan data konsumen dengan kode order \"TESORDERBARU\"', '2021-02-18 01:01:20'),
(428, 34, 225, 'menginputkan data konsumen dengan kode order \"TESTINGKONSUMENA\"', '2021-02-18 14:09:20'),
(429, 35, 229, 'menginputkan data konsumen dengan kode order \"cekordermasuk\"', '2021-02-19 11:49:37'),
(430, 30, 229, 'mengajukan jadwal pasang kusen untuk kode order \"cekordermasuk\" ', '2021-02-19 11:50:40'),
(431, 30, 203, 'telah menyelesaikan pasang kusen untuk kode order \"TESTINGKONSUMEN\" ', '2021-02-19 14:04:20'),
(432, 30, 202, 'telah menyelesaikan pasang finish untuk kode order \"TESTINGORDER\" ', '2021-02-19 15:43:55'),
(433, 35, 232, 'menginputkan data konsumen dengan kode order \"TESDARIAWAL\"', '2021-02-20 04:34:19'),
(434, 30, 232, 'mengubah gambar kerja untuk kode order \"TESDARIAWAL\" ', '2021-02-20 06:43:54'),
(435, 30, 232, 'mengubah gambar kerja untuk kode order \"TESDARIAWAL\" ', '2021-02-20 06:46:50'),
(436, 30, 232, 'mengubah gambar kerja untuk kode order \"TESDARIAWAL\" ', '2021-02-20 06:48:35'),
(437, 30, 232, 'mengubah gambar kerja untuk kode order \"TESDARIAWAL\" ', '2021-02-20 06:57:08'),
(438, 30, 232, 'mengubah gambar kerja untuk kode order \"TESDARIAWAL\" ', '2021-02-20 07:10:30'),
(439, 30, 232, 'mengubah gambar kerja untuk kode order \"TESDARIAWAL\" ', '2021-02-20 07:15:24'),
(440, 30, 232, 'mengajukan jadwal pasang kusen untuk kode order \"TESDARIAWAL\" ', '2021-02-20 07:20:12'),
(441, 35, 232, 'menolak jadwal pasang kusen untuk kode order \"TESDARIAWAL\" ', '2021-02-20 07:20:39'),
(442, 30, 232, 'menolak perubahan jadwal pasang kusen untuk kode order \"TESDARIAWAL\" ', '2021-02-20 07:21:03'),
(443, 35, 232, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESDARIAWAL\" ', '2021-02-20 07:21:19'),
(444, 36, 232, 'telah menyelesaikan pasang kusen untuk kode order \"TESDARIAWAL\" ', '2021-02-20 08:21:14'),
(445, 30, 232, 'mengajukan jadwal pasang finish untuk kode order \"TESDARIAWAL\" ', '2021-02-20 08:21:58'),
(446, 35, 232, 'menerima pengajuan jadwal pasang finish untuk kode order \"TESDARIAWAL\" ', '2021-02-20 08:22:17'),
(447, 36, 232, 'telah menyelesaikan pasang finish untuk kode order \"TESDARIAWAL\" ', '2021-02-20 08:26:16'),
(448, 35, 202, 'mengubah gambar kerja untuk kode order \"TESTINGORDER\" ', '2021-02-20 16:30:47'),
(449, 35, 202, 'mengubah gambar kerja untuk kode order \"TESTINGORDER\" ', '2021-02-20 16:31:57'),
(450, 35, 202, 'mengubah gambar kerja untuk kode order \"TESTINGORDER\" ', '2021-02-20 16:33:27'),
(451, 35, 202, 'mengubah gambar kerja untuk kode order \"TESTINGORDER\" ', '2021-02-20 16:44:44'),
(452, 35, 204, 'mengubah gambar kerja untuk kode order \"TESTINGKONSUMEN2\" ', '2021-02-20 17:44:24'),
(453, 35, 144, 'Pesanan dihapus', '2021-02-22 11:32:41'),
(454, 35, 144, 'Pesanan dihapus', '2021-02-22 11:34:03'),
(455, 35, 229, 'menerima pengajuan jadwal pasang kusen untuk kode order \"cekordermasuk\" ', '2021-02-23 05:19:57'),
(456, 30, 199, 'mengajukan jadwal pasang kusen untuk kode order \"PKMD176\" ', '2021-02-24 04:04:15'),
(457, 30, 223, 'mengajukan jadwal pasang kusen untuk kode order \"TESORDERBARU\" ', '2021-02-24 04:06:57'),
(458, 30, 225, 'mengajukan jadwal pasang kusen untuk kode order \"TESTINGKONSUMENA\" ', '2021-02-24 04:07:57'),
(459, 35, 199, 'menolak jadwal pasang kusen untuk kode order \"PKMD176\" ', '2021-02-26 10:23:45'),
(460, 30, 173, 'mengajukan jadwal pasang finish untuk kode order \"PGMW10\" ', '2021-04-04 09:52:12'),
(461, 30, 180, 'mengajukan jadwal pasang finish untuk kode order \"PKMD174\" ', '2021-04-12 12:30:40'),
(462, 35, 237, 'menginputkan data konsumen dengan kode order \"testinglagi\"', '2021-04-12 12:32:34'),
(463, 30, 237, 'mengajukan jadwal pasang kusen untuk kode order \"testinglagi\" ', '2021-04-12 12:37:29'),
(464, 33, 237, 'mengajukan jadwal pasang kusen untuk kode order \"testinglagi\" ', '2021-04-12 13:09:23'),
(465, 33, 237, 'mengajukan jadwal pasang kusen untuk kode order \"testinglagi\" ', '2021-04-12 13:16:03'),
(466, 35, 237, 'menolak jadwal pasang kusen untuk kode order \"testinglagi\" ', '2021-04-12 13:31:45'),
(467, 33, 237, 'menolak perubahan jadwal pasang kusen untuk kode order \"testinglagi\" ', '2021-04-12 13:43:16'),
(468, 35, 237, 'menolak jadwal pasang kusen untuk kode order \"testinglagi\" ', '2021-04-12 13:44:46'),
(469, 35, 253, 'menginputkan data konsumen dengan kode order \"TESTINGEDOFOTO5\"', '2021-04-13 11:41:41'),
(470, 33, 237, 'menerima perubahan jadwal pasang kusen untuk kode order \"testinglagi\" ', '2021-04-13 11:57:39'),
(471, 33, 115, 'telah menyelesaikan pasang finish untuk kode order \"PKMD128\" ', '2021-04-13 12:05:55'),
(472, 37, 254, 'menginputkan data konsumen dengan kode order \"testinglagi2\"', '2021-04-13 12:18:01'),
(473, 30, 254, 'mengajukan jadwal pasang kusen untuk kode order \"testinglagi2\" ', '2021-04-13 12:18:34'),
(474, 33, 254, 'mengajukan jadwal pasang kusen untuk kode order \"testinglagi2\" ', '2021-04-13 12:18:58'),
(475, 35, 254, 'menerima pengajuan jadwal pasang kusen untuk kode order \"testinglagi2\" ', '2021-04-13 12:19:21'),
(476, 33, 237, 'telah menyelesaikan pasang kusen untuk kode order \"testinglagi\" ', '2021-04-13 12:29:00'),
(477, 33, 254, 'telah menyelesaikan pasang kusen untuk kode order \"testinglagi2\" ', '2021-04-13 12:29:15'),
(478, 30, 237, 'mengajukan jadwal pasang finish untuk kode order \"testinglagi\" ', '2021-04-13 12:45:53'),
(479, 32, 237, 'mengajukan jadwal pasang finish untuk kode order \"testinglagi\" ', '2021-04-13 12:47:16'),
(480, 35, 237, 'menerima pengajuan jadwal pasang finish untuk kode order \"testinglagi\" ', '2021-04-13 12:47:45'),
(481, 35, 256, 'menginputkan data konsumen dengan kode order \"TESMITRA\"', '2021-04-18 03:20:53'),
(482, 30, 256, 'mengajukan jadwal pasang kusen untuk kode order \"TESMITRA\" ', '2021-04-19 11:33:43'),
(483, 35, 257, 'menginputkan data konsumen dengan kode order \"TESTINGCATATAN\"', '2021-04-19 13:41:29'),
(484, 35, 1, 'menginputkan data konsumen dengan kode order \"TES\"', '2021-04-20 15:58:20'),
(485, 30, 1, 'mengajukan jadwal pasang kusen untuk kode order \"TES\" ', '2021-04-21 10:44:31'),
(486, 999, 1, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TES\" ', '2021-04-23 11:36:16'),
(487, 35, 3, 'menginputkan data konsumen dengan kode order \"TES2\"', '2021-04-23 11:46:47'),
(488, 30, 3, 'mengajukan jadwal pasang kusen untuk kode order \"TES2\" ', '2021-04-23 11:48:59'),
(489, 999, 3, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TES2\" ', '2021-04-23 11:49:33'),
(490, 35, 4, 'menginputkan data konsumen dengan kode order \"TES3\"', '2021-04-23 11:51:00'),
(491, 30, 4, 'mengajukan jadwal pasang kusen untuk kode order \"TES3\" ', '2021-04-23 11:51:29'),
(492, 999, 4, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TES3\" ', '2021-04-23 11:53:51'),
(493, 35, 6, 'menginputkan data konsumen dengan kode order \"TEST\"', '2021-04-23 12:11:41'),
(494, 35, 7, 'menginputkan data konsumen dengan kode order \"TESDESAINER\"', '2021-04-30 01:15:37'),
(495, 30, 7, 'mengajukan jadwal pasang kusen untuk kode order \"TESDESAINER\" ', '2021-05-04 02:24:51'),
(496, 999, 7, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESDESAINER\" ', '2021-05-04 02:28:59'),
(497, 999, 7, 'menerima pengajuan jadwal pasang kusen untuk kode order \"TESDESAINER\" ', '2021-05-04 02:44:01'),
(498, 1000, 10, 'menginputkan data konsumen dengan kode order \"TTTT\"', '2021-05-10 11:49:40'),
(499, 35, 11, 'menginputkan data konsumen dengan kode order \"TESFOTO\"', '2021-05-24 08:52:18'),
(500, 35, 12, 'menginputkan data konsumen dengan kode order \"TESFOTO2\"', '2021-05-24 11:08:08'),
(501, 16, 13, 'menginputkan data konsumen dengan kode order \"TEESTDOBLE\"', '2021-06-01 08:40:12');

-- --------------------------------------------------------

--
-- Stand-in structure for view `pesanan_view`
-- (See below for the actual view)
--
CREATE TABLE `pesanan_view` (
`id` int(11)
,`kode_order` varchar(20)
,`tgl_order_masuk` date
,`tgl_jatuh_tempo` date
,`tgl_acc_konsumen` date
,`flow_id` int(11)
,`flow_name` varchar(100)
,`rencana_kerja_id` int(11)
,`rencana_kerja_nama` varchar(50)
,`user_id` int(11)
,`nama_user` varchar(100)
,`konsumen_id` int(11)
,`nama_konsumen` varchar(100)
,`alamat_konsumen` text
,`no_hp_konsumen` varchar(15)
,`desainer_design` int(11)
,`nama_desainer_design` varchar(100)
,`desainer_eksekusi` int(11)
,`nama_desainer_eksekusi` varchar(100)
,`mitra_pemasang_kusen` int(11)
,`nama_pemasang_kusen` varchar(100)
,`mitra_pemasang_finish` int(11)
,`nama_pemasang_finish` varchar(100)
,`jam_pasang_kusen` time
,`tanggal_pasang_kusen` date
,`jam_pasang_finish` time
,`tanggal_pasang_finish` date
,`is_deleted` int(11)
,`tipe` varchar(1)
,`status_produksi` varchar(1)
,`link_sosmed` varchar(200)
,`catatan` text
,`harga_deal` bigint(11)
,`tipe_pengajuan` varchar(7)
,`fee_design` double
,`persentase_eksekusi` double
,`fee_eksekusi` double
,`foto_after` bigint(21)
,`edt_katalog` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `rencana_kerja`
--

CREATE TABLE `rencana_kerja` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rencana_kerja`
--

INSERT INTO `rencana_kerja` (`id`, `kode`, `nama`) VALUES
(1, '', 'Pasang Kusen'),
(2, '', 'Pasang Unit'),
(3, '', 'Selesai Pasang Unit');

-- --------------------------------------------------------

--
-- Table structure for table `reset_password`
--

CREATE TABLE `reset_password` (
  `id` int(11) NOT NULL,
  `temp_password` varchar(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(100) NOT NULL,
  `password_salt` varchar(100) NOT NULL,
  `role` char(2) NOT NULL,
  `bank_rekening` varchar(200) DEFAULT NULL,
  `no_rekening` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `no_hp`, `is_active`, `password`, `password_salt`, `role`, `bank_rekening`, `no_rekening`, `created_at`, `updated_at`) VALUES
(16, 'adminganteng', 'Administrator', '', 1, '67ceb17dfe75f5602936e72d57e3e3cacaff2825', 'Ak8OXmZarIRnqj1aNvH25ufPRt8TjMlzxDw+DXSZqDnHZs9DXSkQaqmepZEm9c/+EH+xeSPXhqYs+zTvAIuutg==', 'A', NULL, NULL, '2020-01-29 12:50:09', '2020-03-26 11:44:46'),
(27, 'ariaryoto', 'Ari Aryoto', '087855994300', 1, 'b11e6f3c8d948a26ca3abd21ad9a43d92a83681f', 'Wqzeqhuruw78ZDiuL9oO8EX6Sqx3sU8cSh7SbkNQQUb6mtI8h+csfsHhxDByignRvaYngvZLStSFMmdcp4utmw==', 'R', NULL, NULL, '2020-02-29 03:02:41', '2020-04-23 05:46:27'),
(28, 'rezagulamz', 'reza gulam zulfikar', '+62 823-5943-4074', 1, 'a67e851a4b1a0d87d2701d517bb2bf2b664bc7f3', '/52j72wostheJ9zAlIOIyID2KTLrriCXEJdBfirjpac6T/dpHmxWHfQ4bUR4XM2ULwVcaF4ZIk+n8NdG9C5ozg==', 'D', NULL, NULL, '2020-02-29 03:07:27', '2020-02-29 03:07:27'),
(29, 'ning', 'ririn widyaningsih', '085100309920', 1, 'fe7b683ac551da4201cf54931e02a26463d9fc5a', 'BQq7b9V26TR43tmkCQcUVSmyWNQZ9bcwNSkSZp4kmC2eSsmMrsJ/8NehJzRiRYFSkL7VwlWR6J4370HpJDiQ/w==', 'P', NULL, NULL, '2020-02-29 03:09:23', '2020-03-24 07:25:50'),
(30, 'ekobp', 'Eko Budi', '+62 857-3111-4444', 1, '186a90314f65b0d648f80fe46f8642ac49be5e20', 'eAzSi5OXJofxON4JzTUNJI04vYDYf5kuArnaLPc9VINPOeR6Ic1E3JWIirkO1f6yz7v5qNNriXKHJHeWBIn4UA==', 'P', NULL, NULL, '2020-02-29 03:10:26', '2020-03-02 10:35:36'),
(31, 'samsulhadi', 'Samsul Hadi', '+62 822-4441-7445', 1, 'b610a8c8a2cf24692ae48f872d13edc8cbfba4ca', '2/7TdsFgdJmJq0riWE3CdRx+trDEFZ2GIwvtRpo6n/ydvrKh4s7M3iS9SwnpyfhCSacq/OsfT96gknK+rp86dw==', 'S', NULL, NULL, '2020-02-29 04:30:35', '2020-02-29 04:30:35'),
(32, 'aziz', 'muhamad aziz', '+62 856-0741-9836', 1, '08eeedce81310cbf3c2630134d90a9a283f0b855', '3N96ihZK8c7xG4+hVQRBLggh9d4R+TnDKv2WUw4Lpehr22qOq2E7axC9JDK1Ym8cv0D3FEHJGl1Hx84YKRJL9Q==', 'S', NULL, NULL, '2020-02-29 04:31:31', '2020-02-29 04:31:31'),
(33, 'wandi', 'heri iswandi', '+62 821-3232-5735', 1, '19ef56a190c6902daa98c73703680b4cc2a8ff08', 'w9tf6NkRTDBzIE64dBBLpp1E5U+XM/N+36gwGK4PCnEyXLP5dzTCG5N5C5lvKHBvCrxmo+GEFE5XTqhblGEy9Q==', 'S', NULL, NULL, '2020-02-29 04:32:18', '2020-02-29 04:32:18'),
(34, 'nasatechadmin', 'nst admin', '1234567890', 1, '32c96ff83d774d22214321e1e6b3d75057007843', '4kv2+VeS1krwELcWrHqR+1hvqKbL0WGEbyJmasTO+4BkXDbRfnm1GbzNM8g+RJwyTMo07otYPJlLoW2mYScYww==', 'R', NULL, NULL, '2020-03-02 13:43:37', '2020-03-02 13:43:37'),
(35, 'test_1', 'Tester 1', '', 1, '7a27285713bf13219467d878d35422cfb86b671b', 'C5r1KCBN7NF8NWg3qVr3rs7K55obqx4/lb3fbZmSe3mG7YS4lO5EFM/P7/c8ZNnjdLCbh47egvWpl0MFs0uV7w==', 'M', NULL, NULL, '2020-03-10 11:24:02', '2020-03-30 10:20:35'),
(36, 'test_mitra', 'Tester Mitra', '', 1, '5b23f58c499dfeecdce2312aad16c5295493040b', 'KGAV2igl2BILM7LCmRAuGq456hDyOKi+SL9iiSHvBuQLu3RLq4c9dnyOCM4LjI/Nb9ktZ7lYuIkUA+yu++H/Bg==', 'S', NULL, NULL, '2020-03-10 11:28:26', '2020-03-10 11:28:26'),
(37, 'test_2', 'Tester 2', '', 1, '001655a56d8f76d8845c18f5482dcacdd178237d', 'G/NrBcxF0cpcTk8t3aFfyzEnfNPlqYUfUif8nbnTh6xT2eKLg78RvTFEOI8aFbXtrV595aWUYMp3cwLWTsLZ+w==', 'M', NULL, NULL, '2020-03-11 08:08:59', '2021-02-03 19:16:37'),
(38, 'france', 'franc hidayat', '128455504', 1, 'be9047cbbccc10f1350331ebc9fba9592a13a7bb', '0QHXLAYNsdax+f32W+bDdHl8gckQFlGaS9aGGmeOdToD9yvTRCx59jTlCfw5kU/7siy6CLRznv2ayRTosG/j5Q==', 'D', 'BCA', '1203984', '2020-05-18 03:18:32', '2021-05-09 05:21:24'),
(999, 'konsumen', 'konsumen', '-', 1, '-', '-', 'K', NULL, NULL, '2021-02-14 02:17:47', '0000-00-00 00:00:00'),
(1000, 'user_mitra_marketing', 'Mitra Marketing', '0877777', 1, '986d50ed14ade22131d74733be5eb355a0cc2fc2', 'kceJlikl7zBr64Y/vUOkukEwYVLub3CauP1MG2UZGyNuwk2RcOjGA0KzXYIW3e+ivIPIYYFNyMgkV0W4FF/Mqw==', 'X', NULL, NULL, '2021-05-10 11:12:01', '2021-05-10 11:12:01');

-- --------------------------------------------------------

--
-- Table structure for table `user_tokens`
--

CREATE TABLE `user_tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `login_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_activity` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `token` varchar(255) NOT NULL,
  `token_expired_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `count_request` int(11) NOT NULL,
  `status` char(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_tokens`
--

INSERT INTO `user_tokens` (`id`, `user_id`, `device_id`, `ip_address`, `login_time`, `last_activity`, `token`, `token_expired_time`, `count_request`, `status`, `created_at`, `updated_at`) VALUES
(2, 16, 6, '118.137.118.248', '2020-04-16 08:51:49', '2020-04-23 05:42:17', '359a6bb7f89fd3f514fbcf62a58c533cc74d97b01eae257e44aa9d5bade97baf12fbf1356eefc6ce56bf0298ad494c24', '2020-04-28 05:42:17', 34, 'I', '2020-04-16 08:51:49', '2020-04-23 05:42:17'),
(3, 28, 7, '36.71.35.2', '2020-04-17 02:32:38', '2020-04-28 01:57:09', 'c2425ac176cbf3cb7afcffcca5b2b95533e75ff09dd601bbe69f351039152189771844f2f7e3a859d45b45110ce39fbf', '2020-05-03 01:57:09', 139, 'E', '2020-04-17 02:32:38', '2020-05-04 00:46:16'),
(4, 27, 5, '36.71.35.2', '2020-04-18 02:20:55', '2020-04-27 02:32:20', 'fe1354af1804d53cf7c6f7273911fefc02e74f10e0327ad868d138f2b4fdd6f02f15bb63f4be5ab9e0a840524680ec34', '2020-05-02 02:32:20', 151, 'I', '2020-04-18 02:20:55', '2020-04-27 02:32:20'),
(5, 29, 8, '36.82.79.13', '2020-04-18 02:55:22', '2020-07-05 13:34:16', '2592201c6582bfce3ace45b69ac8ea816ea9ab1baa0efb9e19094440c317e21b300880d19bf3a525cb4c4a107f492bd4', '2020-07-10 13:34:16', 3425, 'E', '2020-04-18 02:55:22', '2020-07-15 03:22:19'),
(6, 37, 6, '114.124.161.189', '2020-04-18 06:48:59', '2020-04-22 03:41:16', '61739e4338e88713f25b624de617076ba5bfc9e07964f8dddeb95fc584cd965df5fb68d37657cd6ee28e04b6c772ba64', '2020-04-27 03:41:16', 86, 'I', '2020-04-18 06:48:59', '2020-04-22 03:41:16'),
(7, 16, 6, '114.124.161.189', '2020-04-22 02:39:48', '2020-04-22 02:39:50', 'dd21da5870616e0ed62f287b35d89003c74d97b01eae257e44aa9d5bade97baf6150667bf6e3920dc9692d1656514dbb', '2020-04-27 02:39:50', 3, 'I', '2020-04-22 02:39:48', '2020-04-22 02:39:50'),
(8, 29, 6, '114.124.161.189', '2020-04-22 02:40:08', '2020-04-22 02:43:08', 'a1fdd88f295781d2d17798bd1b1b51ab6ea9ab1baa0efb9e19094440c317e21bbc39ff6de78e1769b7c704a463ffba3e', '2020-04-27 02:43:08', 20, 'I', '2020-04-22 02:40:08', '2020-04-22 02:43:08'),
(9, 29, 6, '118.137.118.248', '2020-04-22 03:48:58', '2020-04-22 04:48:21', '6e251d81f4a7af05366726820b91827b6ea9ab1baa0efb9e19094440c317e21b79e5a105a7b5c4d0cf5599128df857a2', '2020-04-27 04:48:21', 18, 'I', '2020-04-22 03:48:58', '2020-04-22 04:48:21'),
(10, 30, 6, '118.137.118.248', '2020-04-22 03:49:38', '2020-04-22 04:32:04', 'e015a4efc23d1260e082dc694bd70d6734173cb38f07f89ddbebc2ac9128303f0d0d17e435ad3c2600dd6ba76b194e4f', '2020-04-27 04:32:04', 9, 'I', '2020-04-22 03:49:38', '2020-04-22 04:32:04'),
(11, 36, 6, '118.137.118.248', '2020-04-22 04:32:24', '2020-04-22 04:32:29', '25cbd34c27e894126fb1c1d673eaaf2c19ca14e7ea6328a42e0eb13d585e4c22c7a8b100ef3024b5cb11c28b1969f893', '2020-04-27 04:32:29', 5, 'I', '2020-04-22 04:32:24', '2020-04-22 04:32:29'),
(12, 37, 6, '114.124.161.142', '2020-04-22 04:32:51', '2020-05-06 12:06:08', 'e105b1e8f93d9f9b3646e6043ee65b42a5bfc9e07964f8dddeb95fc584cd965df5b8965394ea9c4f08700634c122818e', '2020-05-11 12:06:08', 644, 'I', '2020-04-22 04:32:51', '2020-05-06 12:06:08'),
(13, 37, 12, '118.137.118.248', '2020-04-22 05:54:41', '2020-04-23 02:36:54', '6d087b75df756184c3b1f14d829032baa5bfc9e07964f8dddeb95fc584cd965d154b1fcc0890d52b2fe047545601af0f', '2020-04-28 02:36:54', 67, 'E', '2020-04-22 05:54:41', '2020-04-28 09:12:18'),
(14, 16, 12, '118.137.118.248', '2020-04-22 05:58:50', '2020-04-22 06:22:50', 'b4ff037142624e10006009fa0415c550c74d97b01eae257e44aa9d5bade97baf518af4ca72d3ce5aef32dd2aaebc578d', '2020-04-27 06:22:50', 19, 'E', '2020-04-22 05:58:50', '2020-05-06 10:35:34'),
(15, 36, 12, '118.137.118.248', '2020-04-22 06:05:41', '2020-04-22 06:10:47', 'fe290635665879b0c8e5a57a3f1a47de19ca14e7ea6328a42e0eb13d585e4c228ca3bafc8a991ab050760187c02f7f99', '2020-04-27 06:10:47', 4, 'I', '2020-04-22 06:05:41', '2020-04-22 06:10:47'),
(16, 32, 12, '118.137.118.248', '2020-04-22 06:22:26', '2020-04-22 06:22:31', 'bd9188f2b7e559b54cb5abe4f79cdcc56364d3f0f495b6ab9dcf8d3b5c6e0b0177d5a0c845d8b44042566fa1ffea7dd7', '2020-04-27 06:22:31', 5, 'I', '2020-04-22 06:22:26', '2020-04-22 06:22:31'),
(17, 33, 12, '118.137.118.248', '2020-04-22 06:23:12', '2020-04-22 06:23:14', 'b04f184ab2da34d85118fd82ca3e2db5182be0c5cdcd5072bb1864cdee4d3d6e7d1f5d7106ba88609695a2efa2481329', '2020-04-27 06:23:14', 4, 'I', '2020-04-22 06:23:12', '2020-04-22 06:23:14'),
(18, 16, 6, '114.124.212.110', '2020-04-23 01:28:24', '2020-04-23 01:28:30', '1dd6df8611bd982fd68c024fbd516700c74d97b01eae257e44aa9d5bade97baf535d229a735a0165d352015cbefd7289', '2020-04-28 01:28:30', 7, 'I', '2020-04-23 01:28:24', '2020-04-23 01:28:30'),
(19, 37, 6, '114.124.212.110', '2020-04-23 03:54:55', '2020-04-23 05:27:00', '51bda96e6bc0b7cdf8a318204d740592a5bfc9e07964f8dddeb95fc584cd965d5bdada5925450bc66f94276590f3a450', '2020-04-28 05:27:00', 30, 'I', '2020-04-23 03:54:55', '2020-04-23 05:27:00'),
(20, 37, 12, '118.137.118.248', '2020-04-23 04:12:10', '2020-04-24 01:48:20', '051717de019e970316e0fc6428ca7589a5bfc9e07964f8dddeb95fc584cd965d2d7697371a54de19798625ba7023d7ff', '2020-04-29 01:48:20', 33, 'E', '2020-04-23 04:12:10', '2020-05-04 09:55:01'),
(21, 37, 6, '114.124.229.78', '2020-04-23 05:05:05', '2020-04-23 05:05:08', '03ee977e819fd21e43176973888bc64da5bfc9e07964f8dddeb95fc584cd965dfe693513393c6296f3af9f6eb18910e2', '2020-04-28 05:05:08', 5, 'I', '2020-04-23 05:05:05', '2020-04-23 05:05:08'),
(23, 16, 13, '110.138.225.163', '2020-04-24 12:17:41', '2020-04-24 12:18:05', 'e6a51af031a29f278b7c978f1e468ba2c74d97b01eae257e44aa9d5bade97baf6bea22a0c806abdd703a866c8403bc5a', '2020-04-29 12:18:05', 8, 'I', '2020-04-24 12:17:41', '2020-04-24 12:18:05'),
(24, 37, 12, '118.137.118.248', '2020-04-28 09:12:18', '2020-04-28 09:12:44', 'ed6850d2322823d0315f9ead88e71439a5bfc9e07964f8dddeb95fc584cd965d9d00aca34f873fe05a326848923b554c', '2020-05-03 09:12:44', 7, 'E', '2020-04-28 09:12:18', '2020-05-04 09:50:52'),
(25, 27, 5, '36.71.75.127', '2020-04-30 22:49:19', '2020-05-07 11:03:24', 'b8e301c166c5d9fde53761769147689902e74f10e0327ad868d138f2b4fdd6f0f9d44dcdd41d0687440c2981807a2906', '2020-05-12 11:03:24', 235, 'I', '2020-04-30 22:49:19', '2020-05-07 11:03:24'),
(26, 28, 7, '140.213.57.126', '2020-05-04 00:46:16', '2020-05-04 00:46:16', 'e1126898a5428daf89d4d156f28f317633e75ff09dd601bbe69f35103915218977ef1d952df029a82d159682a0fc7f92', '2020-05-09 00:46:16', 1, 'I', '2020-05-04 00:46:16', '2020-05-04 00:46:16'),
(27, 28, 7, '36.71.75.127', '2020-05-04 00:57:34', '2020-05-08 03:56:04', '2f517959c4f5e51a76cfbeb9b4bba5c133e75ff09dd601bbe69f351039152189ba681f4143dcf91afd3b3b2ef08c8ba7', '2020-05-13 03:56:04', 273, 'I', '2020-05-04 00:57:34', '2020-05-08 03:56:04'),
(28, 37, 12, '118.137.118.248', '2020-05-04 09:50:52', '2020-05-07 12:00:14', '2069ca85edfe4e25d3878027813ca9b6a5bfc9e07964f8dddeb95fc584cd965d94a2039d5c99fdb0a6936541f06df63c', '2020-05-12 12:00:14', 68, 'I', '2020-05-04 09:50:52', '2020-05-07 12:00:14'),
(29, 37, 12, '118.137.118.248', '2020-05-04 09:55:01', '2020-05-06 10:30:13', '88e0a08ffe74765c79ce05416d195a7fa5bfc9e07964f8dddeb95fc584cd965de300dba58d637bed44ccea5786f3c42b', '2020-05-11 10:30:13', 221, 'I', '2020-05-04 09:55:01', '2020-05-06 10:30:13'),
(30, 16, 12, '118.137.118.248', '2020-05-06 10:35:34', '2020-05-06 10:35:37', 'e5d4725e7a3a6d3dbfa5bf16001c43d5c74d97b01eae257e44aa9d5bade97bafadc86f08945982ae8c6dda5bb5d0c758', '2020-05-11 10:35:37', 3, 'I', '2020-05-06 10:35:34', '2020-05-06 10:35:37'),
(31, 27, 12, '118.137.118.248', '2020-05-06 10:36:06', '2020-05-07 02:59:07', 'bf087f0cfb0bf00380153dd71f19151902e74f10e0327ad868d138f2b4fdd6f028198d08c0cb1d662614c9ec0c2dd41e', '2020-05-12 02:59:07', 72, 'I', '2020-05-06 10:36:06', '2020-05-07 02:59:07'),
(32, 27, 6, '118.137.118.248', '2020-05-06 12:25:40', '2020-05-07 02:16:18', '1360157ae27d23aaba37b954a2c2566302e74f10e0327ad868d138f2b4fdd6f0f7e876930bd73ffa3d615472baa09070', '2020-05-12 02:16:18', 30, 'I', '2020-05-06 12:25:40', '2020-05-07 02:16:18'),
(33, 37, 6, '114.124.161.207', '2020-05-07 02:16:38', '2020-05-16 02:28:58', '225f9e5f871ca96325c928b7a838ef49a5bfc9e07964f8dddeb95fc584cd965d9f9aaf0c930114ea6d8513529f5a4a47', '2020-05-21 02:28:58', 148, 'I', '2020-05-07 02:16:38', '2020-05-16 02:28:58'),
(34, 27, 5, '114.125.124.115', '2020-05-08 23:37:00', '2020-09-02 12:44:51', 'dbccacddd4622ebbc17cb6d51a7e706f02e74f10e0327ad868d138f2b4fdd6f0d47023801f08dfee7883132ceddd1bbe', '2020-09-07 12:44:51', 3032, 'I', '2020-05-08 23:37:00', '2020-09-02 12:44:51'),
(35, 28, 7, '125.160.154.85', '2020-05-09 02:17:59', '2020-06-10 09:56:04', '0184dbbbe43a74973fdb2ddaec39464e33e75ff09dd601bbe69f351039152189822ffe057cec67e09f287a6eafa3bc2c', '2020-06-15 09:56:04', 303, 'E', '2020-05-09 02:17:59', '2020-06-22 22:52:17'),
(36, 27, 6, '114.124.161.207', '2020-05-16 02:24:08', '2020-05-16 02:28:19', '662f98073020981d3905024dc88550ad02e74f10e0327ad868d138f2b4fdd6f0bf7ca223dba6f6163f9179159cf131b3', '2020-05-21 02:28:19', 15, 'I', '2020-05-16 02:24:08', '2020-05-16 02:28:19'),
(37, 37, 4, '114.124.161.207', '2020-05-16 02:29:30', '2020-05-16 06:12:39', 'ba172b2ffb481358d3cc1f66244026fea5bfc9e07964f8dddeb95fc584cd965dcbb749516b844070d8e574cdad777e3d', '2020-05-21 06:12:39', 34, 'I', '2020-05-16 02:29:30', '2020-05-16 06:12:39'),
(38, 27, 4, '114.124.161.207', '2020-05-16 02:33:30', '2020-05-16 06:11:51', '0aa4cc977b87304b60831701122d22c002e74f10e0327ad868d138f2b4fdd6f0f311c2063b662dadf5645eee5f5d78da', '2020-05-21 06:11:51', 61, 'I', '2020-05-16 02:33:30', '2020-05-16 06:11:51'),
(39, 38, 14, '114.125.81.99', '2020-05-18 03:23:14', '2020-05-21 12:23:50', '4fc53321468838b2db11c69acf1f6c51a5771bce93e200c36f7cd9dfd0e5deaa7042382cfc6c91d09f33d63e4dbe02e2', '2020-05-26 12:23:50', 103, 'E', '2020-05-18 03:23:14', '2020-05-28 09:10:32'),
(40, 38, 14, '125.166.4.146', '2020-05-28 09:10:32', '2020-09-02 09:21:46', 'f9f9c8ac9fd06fa3db194d92d85a00e1a5771bce93e200c36f7cd9dfd0e5deaa265f723b9d2c4b8d12e86392a7d7da50', '2020-09-07 09:21:46', 2327, 'I', '2020-05-28 09:10:32', '2020-09-02 09:21:46'),
(41, 37, 6, '114.124.212.138', '2020-05-28 10:01:11', '2020-06-02 06:55:16', '862035d479aa43d8afa049a70a56729ea5bfc9e07964f8dddeb95fc584cd965d2a999c74be9091129add3f31d95f3389', '2020-06-07 06:55:16', 88, 'I', '2020-05-28 10:01:11', '2020-06-02 06:55:16'),
(42, 16, 6, '114.124.161.134', '2020-06-02 06:55:28', '2020-06-04 03:20:49', 'ab4a94ada29f3538b5303c6c04e0c9fdc74d97b01eae257e44aa9d5bade97bafb0f24fbcc50096bc32044cfdd8fedae5', '2020-06-09 03:20:49', 38, 'I', '2020-06-02 06:55:28', '2020-06-04 03:20:49'),
(43, 37, 6, '114.124.161.134', '2020-06-04 03:21:06', '2020-06-04 03:28:01', 'ab7bab3cb4514868061e599f7bca752fa5bfc9e07964f8dddeb95fc584cd965d23e564386998142aa08536d6e099f3c8', '2020-06-09 03:28:01', 16, 'I', '2020-06-04 03:21:06', '2020-06-04 03:28:01'),
(44, 37, 6, '114.124.161.198', '2020-06-08 02:35:49', '2020-06-08 03:05:34', 'f3bd092d0d2d95a13fa388a680c28f23a5bfc9e07964f8dddeb95fc584cd965d74d7716a968e35d65bcaec8547a73e24', '2020-06-13 03:05:34', 38, 'E', '2020-06-08 02:35:49', '2020-06-22 03:10:49'),
(45, 37, 6, '114.124.161.148', '2020-06-22 03:10:49', '2020-07-02 12:20:56', 'faf50621653b49841e19f7123c086e72a5bfc9e07964f8dddeb95fc584cd965db0908bb6232dc0404832c72ca45c8e39', '2020-07-07 12:20:56', 333, 'I', '2020-06-22 03:10:49', '2020-07-02 12:20:56'),
(46, 37, 15, '139.195.93.228', '2020-06-22 09:56:35', '2020-06-24 10:13:40', '9f474679cdc0a01a8417b2eb076de091a5bfc9e07964f8dddeb95fc584cd965dd2f9d13a51621530a930d2c3bca8b068', '2020-06-29 10:13:40', 58, 'I', '2020-06-22 09:56:35', '2020-06-24 10:13:40'),
(47, 28, 7, '182.1.115.136', '2020-06-22 22:52:17', '2020-07-21 01:14:33', '6b0492edf7f6eca7ef03258c2c8bd4ea33e75ff09dd601bbe69f3510391521894d67693ce7e6778db0482fdde79cd33c', '2020-07-26 01:14:33', 164, 'E', '2020-06-22 22:52:17', '2020-07-26 03:58:11'),
(48, 27, 6, '139.195.93.228', '2020-06-27 15:06:09', '2020-06-28 01:35:36', 'd5fe63b6784de04cb91c3695d2ebe37302e74f10e0327ad868d138f2b4fdd6f0766dc18244edf55da64a29a4be6bc3d7', '2020-07-03 01:35:36', 28, 'I', '2020-06-27 15:06:09', '2020-06-28 01:35:36'),
(49, 27, 1, '139.195.93.228', '2020-06-28 02:00:38', '2020-06-28 02:12:59', '95da2ffdaf32a2d29910e496f23f526202e74f10e0327ad868d138f2b4fdd6f0fb96c27435ac8d69c7111c02aa13f0b6', '2020-07-03 02:12:59', 40, 'I', '2020-06-28 02:00:38', '2020-06-28 02:12:59'),
(50, 37, 1, '139.195.93.228', '2020-06-28 02:13:13', '2020-06-28 02:13:25', 'dd5d59f284f7b2485f810ea37f23a1efa5bfc9e07964f8dddeb95fc584cd965d098ead3e649e99d0816702fd6e488e4a', '2020-07-03 02:13:25', 13, 'I', '2020-06-28 02:13:13', '2020-06-28 02:13:25'),
(51, 37, 16, '139.195.93.228', '2020-06-30 11:48:08', '2020-07-09 04:11:26', 'ff1797777fe8ddc6a5703284c5f5a4c3a5bfc9e07964f8dddeb95fc584cd965d550a68b6b8dee37007825ea72c047df8', '2020-07-14 04:11:26', 322, 'I', '2020-06-30 11:48:08', '2020-07-09 04:11:26'),
(52, 37, 4, '139.195.93.228', '2020-07-02 12:23:05', '2020-07-11 13:20:50', 'c0f85b5efb363cd5ea5e139bc8ea5455a5bfc9e07964f8dddeb95fc584cd965d6a468027dc252c08f932c83b411ec3ac', '2020-07-16 13:20:50', 563, 'I', '2020-07-02 12:23:05', '2020-07-11 13:20:50'),
(53, 29, 10, '36.85.19.115', '2020-07-03 02:28:10', '2020-09-02 09:11:26', '7bb797b3ddd47aa7d1ddb527c57fb50f6ea9ab1baa0efb9e19094440c317e21b87be4c70145f1e765b449e9ddb5d564c', '2020-09-07 09:11:26', 2064, 'I', '2020-07-03 02:28:10', '2020-09-02 09:11:26'),
(54, 37, 17, '139.195.93.228', '2020-07-05 01:48:42', '2020-07-06 12:18:59', '8787c452235ff2068954aa306fe40e12a5bfc9e07964f8dddeb95fc584cd965de0dc09fcc26850e348efc35366530a35', '2020-07-11 12:18:59', 18, 'E', '2020-07-05 01:48:42', '2020-07-13 11:22:43'),
(55, 16, 17, '139.195.93.228', '2020-07-06 11:44:44', '2020-07-07 11:31:20', 'd82998d93b3b900ee392b972a60b69bac74d97b01eae257e44aa9d5bade97baf0ec56802cc4535ca6e2f7aa2accd445a', '2020-07-12 11:31:20', 16, 'E', '2020-07-06 11:44:44', '2020-07-13 11:32:52'),
(56, 16, 4, '114.124.161.136', '2020-07-06 11:52:36', '2020-07-16 04:46:45', '9e65e9e507a9a309c8bd6c88a19ca479c74d97b01eae257e44aa9d5bade97baf7756388482d0449b958f24cd34fd1766', '2020-07-21 04:46:45', 726, 'I', '2020-07-06 11:52:36', '2020-07-16 04:46:45'),
(57, 16, 16, '139.195.93.228', '2020-07-07 01:40:32', '2020-07-13 10:38:15', '905f82348720d1a0b7f4138f734a2aa5c74d97b01eae257e44aa9d5bade97bafce1b418cce90e8c9ad63a94864d55435', '2020-07-18 10:38:15', 251, 'E', '2020-07-07 01:40:32', '2020-07-21 02:11:41'),
(58, 37, 17, '139.195.93.228', '2020-07-13 11:22:43', '2020-07-13 11:22:43', '684e5cdc985d5da7fdee3e8f1684cad0a5bfc9e07964f8dddeb95fc584cd965df6e090c39a2024dc775f44e3f570685e', '2020-07-18 11:22:43', 1, 'I', '2020-07-13 11:22:43', '2020-07-13 11:22:43'),
(59, 16, 17, '140.0.27.15', '2020-07-13 11:32:52', '2020-07-15 12:35:12', '0fc2dc7d4a3656239dc5dfaeb1cda97ec74d97b01eae257e44aa9d5bade97baf14f51902b858ab9ec075920ca8d0699d', '2020-07-20 12:35:12', 18, 'E', '2020-07-13 11:32:52', '2020-07-28 11:21:23'),
(60, 29, 8, '36.81.169.94', '2020-07-15 03:22:19', '2020-07-15 04:44:08', 'c7e353dc4325d03fc7054d2133c1401c6ea9ab1baa0efb9e19094440c317e21b4498bb0ecdf7228d5131fc5b7008168e', '2020-07-20 04:44:08', 11, 'I', '2020-07-15 03:22:19', '2020-07-15 04:44:08'),
(61, 37, 4, '114.124.161.136', '2020-07-16 04:46:56', '2020-07-16 05:22:00', 'c3756c824e5f6fcf9d728710a0722f20a5bfc9e07964f8dddeb95fc584cd965d16acc1a04136162d1f8072d0a1c3769d', '2020-07-21 05:22:00', 22, 'I', '2020-07-16 04:46:56', '2020-07-16 05:22:00'),
(62, 37, 1, '114.124.161.136', '2020-07-16 05:25:12', '2020-07-16 05:25:16', '4328d43771ecad81ebcf6d64500c7848a5bfc9e07964f8dddeb95fc584cd965d9d0725dfe30ad53ef7b5f3123847c972', '2020-07-21 05:25:16', 6, 'I', '2020-07-16 05:25:12', '2020-07-16 05:25:16'),
(63, 16, 1, '114.124.161.136', '2020-07-16 05:26:22', '2020-07-16 06:21:49', 'ca5683897ea6b26bb54a6a45ee0b3414c74d97b01eae257e44aa9d5bade97baf136f0207adddff205b270529cce1a2e4', '2020-07-21 06:21:49', 18, 'I', '2020-07-16 05:26:22', '2020-07-16 06:21:49'),
(64, 29, 1, '114.124.161.136', '2020-07-16 05:28:00', '2020-07-16 06:21:33', 'd6c3a129ea3c47f95dd8e8bbbfef35756ea9ab1baa0efb9e19094440c317e21ba5483ff99b762c04218f33f8cf24c753', '2020-07-21 06:21:33', 37, 'I', '2020-07-16 05:28:00', '2020-07-16 06:21:33'),
(65, 16, 4, '140.0.27.15', '2020-07-20 11:59:42', '2020-07-22 12:26:01', 'af7d492c3d80af2b7a621e3fefde72adc74d97b01eae257e44aa9d5bade97bafa6ea78801d62bcafbc3295370f42eba2', '2020-07-27 12:26:01', 213, 'I', '2020-07-20 11:59:42', '2020-07-22 12:26:01'),
(66, 16, 16, '140.0.27.15', '2020-07-21 02:11:41', '2020-07-21 02:42:36', '52202c758d4c6c67b8c2199d4d774c60c74d97b01eae257e44aa9d5bade97baf1cd73445fe32387c65087c279154c2b9', '2020-07-26 02:42:36', 89, 'I', '2020-07-21 02:11:41', '2020-07-21 02:42:36'),
(67, 37, 16, '139.195.134.22', '2020-07-21 02:42:47', '2020-07-28 09:40:02', '25c0a617124984fbcdb4f9daabaea3f7a5bfc9e07964f8dddeb95fc584cd965d82cf5cb769a4b3be4a6dc19b46b34e70', '2020-08-02 09:40:02', 157, 'E', '2020-07-21 02:42:47', '2020-08-06 01:35:36'),
(68, 37, 4, '140.0.27.15', '2020-07-22 12:44:13', '2020-07-23 02:54:16', 'c6d8794f55b29bedcd22321d24b49b35a5bfc9e07964f8dddeb95fc584cd965d7cf70dfb1f77549aff4122fcf8640cfd', '2020-07-28 02:54:16', 44, 'I', '2020-07-22 12:44:13', '2020-07-23 02:54:16'),
(69, 16, 6, '114.124.161.194', '2020-07-24 09:59:17', '2020-07-24 09:59:26', '693cfb33ede8797a367298991c2c2cf6c74d97b01eae257e44aa9d5bade97baf9b4188068a1c8632ac25e2b6a6339ba8', '2020-07-29 09:59:26', 4, 'I', '2020-07-24 09:59:17', '2020-07-24 09:59:26'),
(70, 37, 1, '139.195.134.22', '2020-07-24 10:24:04', '2020-07-24 10:37:39', '9577c976776ac0712a16ff1ab166f472a5bfc9e07964f8dddeb95fc584cd965d65b1903e5713d35acedd698e812f5262', '2020-07-29 10:37:39', 39, 'I', '2020-07-24 10:24:04', '2020-07-24 10:37:39'),
(71, 37, 4, '139.195.134.22', '2020-07-24 13:00:00', '2020-07-24 13:00:05', 'f82573714b3f6a94195530686735abb0a5bfc9e07964f8dddeb95fc584cd965d572ee17320f83008b3d9cb02fa28caaa', '2020-07-29 13:00:05', 6, 'E', '2020-07-24 13:00:00', '2020-08-05 12:10:42'),
(72, 16, 4, '114.124.212.131', '2020-07-24 13:04:08', '2020-08-11 16:51:45', '3d04a064fda782b1a48978d9a2fb4061c74d97b01eae257e44aa9d5bade97baf50cdfac2d4ed569e5f5a6c9e7954eeb4', '2020-08-16 16:51:45', 1155, 'E', '2020-07-24 13:04:08', '2020-08-25 12:45:50'),
(73, 28, 7, '114.122.136.55', '2020-07-26 03:58:11', '2020-07-26 03:58:11', 'b95016a6914614617d7214462848b00d33e75ff09dd601bbe69f3510391521890762a330fa8cf791790466d21b679673', '2020-07-31 03:58:11', 1, 'I', '2020-07-26 03:58:11', '2020-07-26 03:58:11'),
(74, 28, 7, '180.242.223.62', '2020-07-26 03:58:36', '2020-07-28 10:25:52', '2b34a24b6c70763814a986288a99a03f33e75ff09dd601bbe69f351039152189249cc644b55686ae62e2c1feda95f3bc', '2020-08-02 10:25:52', 32, 'I', '2020-07-26 03:58:36', '2020-07-28 10:25:52'),
(75, 16, 17, '139.195.134.22', '2020-07-28 11:21:23', '2020-07-28 13:03:50', '225dc4b534054c81c32c01269754b607c74d97b01eae257e44aa9d5bade97bafb0348a71f68765eae9b2857f1d9e47ef', '2020-08-02 13:03:50', 13, 'I', '2020-07-28 11:21:23', '2020-07-28 13:03:50'),
(76, 37, 4, '114.124.204.116', '2020-08-05 12:10:42', '2020-08-07 16:17:46', 'e05f3e27d1ac7464d9aa195130e805f9a5bfc9e07964f8dddeb95fc584cd965d9d4df2bb96a27b6a80fd44ff2399ad9e', '2020-08-12 16:17:46', 222, 'I', '2020-08-05 12:10:42', '2020-08-07 16:17:46'),
(77, 37, 16, '139.195.134.22', '2020-08-06 01:35:36', '2020-08-06 11:27:05', '0aa3af09ed26c68673862cc4fe6e7db4a5bfc9e07964f8dddeb95fc584cd965da48a7cdb9d0dc4c3c9719e1dd9ab014e', '2020-08-11 11:27:05', 61, 'E', '2020-08-06 01:35:36', '2020-08-12 15:04:19'),
(78, 16, 4, '114.124.237.117', '2020-08-07 16:17:57', '2020-08-07 16:18:17', '8dc49caa0afb8756d5adf86002f4bd97c74d97b01eae257e44aa9d5bade97baf8cc9d4f36bb26bd8f17f4b449ec8969e', '2020-08-12 16:18:17', 13, 'I', '2020-08-07 16:17:57', '2020-08-07 16:18:17'),
(79, 37, 4, '114.124.199.220', '2020-08-07 16:18:30', '2020-08-08 01:03:46', 'fc4e374968c8f22f1c91f0f5b3f6cfc3a5bfc9e07964f8dddeb95fc584cd965d1dcde0f6a8dbad4885c80cff27bb5ee9', '2020-08-13 01:03:46', 51, 'I', '2020-08-07 16:18:30', '2020-08-08 01:03:46'),
(80, 16, 4, '114.124.215.124', '2020-08-08 01:03:55', '2020-08-08 02:58:58', '913d9ef545573a820386178e0f3e8df7c74d97b01eae257e44aa9d5bade97baf5e50e58a6eed8e84e529e76d80d749aa', '2020-08-13 02:58:58', 25, 'I', '2020-08-08 01:03:55', '2020-08-08 02:58:58'),
(81, 37, 4, '139.195.134.22', '2020-08-08 02:59:17', '2020-08-09 02:16:31', 'b47651f5f5633f669491552de1bfe137a5bfc9e07964f8dddeb95fc584cd965d6151fae0cd1980210567b7b67819c11b', '2020-08-14 02:16:31', 53, 'E', '2020-08-08 02:59:17', '2020-08-16 12:47:43'),
(82, 16, 16, '139.195.134.22', '2020-08-10 09:38:59', '2020-09-02 12:34:12', 'dcbc7da230acd203353e1aead77ce8e3c74d97b01eae257e44aa9d5bade97bafd2e557f10a21336ad5ae99391eb36a28', '2020-09-07 12:34:12', 948, 'I', '2020-08-10 09:38:59', '2020-09-02 12:34:12'),
(83, 37, 4, '114.124.231.92', '2020-08-10 13:13:45', '2020-08-10 13:13:46', 'c655e3b39c15e6ad42b31b2ef5c71e37a5bfc9e07964f8dddeb95fc584cd965d18bbc44bf01c88c45624e0b35082a090', '2020-08-15 13:13:46', 3, 'I', '2020-08-10 13:13:45', '2020-08-10 13:13:46'),
(84, 27, 4, '114.124.213.35', '2020-08-11 03:40:22', '2020-08-11 03:40:23', 'ca4d1fb224faa62bd2d5ff430423128a02e74f10e0327ad868d138f2b4fdd6f0866b62e0c04b9cc8eebb5183e897fdd5', '2020-08-16 03:40:23', 3, 'I', '2020-08-11 03:40:22', '2020-08-11 03:40:23'),
(85, 37, 4, '114.124.215.246', '2020-08-11 16:52:13', '2020-08-12 00:22:06', 'fa883cd6865146890d2e2b6f4c620853a5bfc9e07964f8dddeb95fc584cd965d73cec652421c799a1aae351f2eaadff9', '2020-08-17 00:22:06', 49, 'I', '2020-08-11 16:52:13', '2020-08-12 00:22:06'),
(86, 29, 4, '114.124.215.246', '2020-08-12 00:22:21', '2020-08-12 00:22:36', '7eeed1dac978b8155e55061650958eaf6ea9ab1baa0efb9e19094440c317e21b2e9003dd0129a5c2d8aecdbb0f44b371', '2020-08-17 00:22:36', 11, 'I', '2020-08-12 00:22:21', '2020-08-12 00:22:36'),
(87, 16, 4, '114.124.237.215', '2020-08-12 00:56:58', '2020-08-12 01:18:28', 'c064a4431b8968f2ac7dc5c26787881fc74d97b01eae257e44aa9d5bade97baf424f90c09c79e4b098f2cbd61f815cde', '2020-08-17 01:18:28', 47, 'I', '2020-08-12 00:56:58', '2020-08-12 01:18:28'),
(88, 28, 7, '114.125.100.200', '2020-08-12 01:03:59', '2020-08-15 09:24:47', '9b028af78858a34a78e3d054a30f743133e75ff09dd601bbe69f351039152189320b3cc5125007d0584094b066ca9837', '2020-08-20 09:24:47', 115, 'E', '2020-08-12 01:03:59', '2020-08-22 10:51:21'),
(89, 37, 4, '114.124.236.142', '2020-08-12 01:18:51', '2020-08-13 13:44:41', '4d10a8470ec31da4b9c8897ddf4249aba5bfc9e07964f8dddeb95fc584cd965dd9702c9375bde6728b05eeb34333efe5', '2020-08-18 13:44:41', 231, 'I', '2020-08-12 01:18:51', '2020-08-13 13:44:41'),
(90, 37, 16, '139.195.134.22', '2020-08-12 15:04:19', '2020-08-12 15:30:31', '969ab1fb222c973aafdae25d590f44a5a5bfc9e07964f8dddeb95fc584cd965df3481351ddb221654c5d83f63de119d5', '2020-08-17 15:30:31', 9, 'E', '2020-08-12 15:04:19', '2020-08-31 12:44:14'),
(91, 37, 1, '114.124.161.157', '2020-08-15 07:33:28', '2020-08-15 11:32:05', '1c55ff153bd0863ee97ed41a8dde3ecba5bfc9e07964f8dddeb95fc584cd965d411eafc5313cbb9f951b04677557fc0d', '2020-08-20 11:32:05', 37, 'I', '2020-08-15 07:33:28', '2020-08-15 11:32:05'),
(92, 37, 4, '114.124.161.193', '2020-08-15 12:39:44', '2020-08-22 03:14:23', '0e05d3cf7f3e128608c4b3fd4cc6f8d7a5bfc9e07964f8dddeb95fc584cd965d8456e9319024774302d49b257f6fd865', '2020-08-27 03:14:23', 147, 'I', '2020-08-15 12:39:44', '2020-08-22 03:14:23'),
(93, 27, 4, '139.195.134.22', '2020-08-16 06:24:26', '2020-08-16 12:47:28', '6a6c22a5a60dda695ac10fc1614b16a902e74f10e0327ad868d138f2b4fdd6f0be0df9e4484d9558217c1bdb0f2d4563', '2020-08-21 12:47:28', 260, 'I', '2020-08-16 06:24:26', '2020-08-16 12:47:28'),
(94, 37, 4, '114.124.214.81', '2020-08-16 12:47:43', '2020-08-29 10:00:45', '9fadc54f79c678f265b697d16a6191a2a5bfc9e07964f8dddeb95fc584cd965daa4ee241279ceb7917183ab58490dd7d', '2020-09-03 10:00:45', 620, 'I', '2020-08-16 12:47:43', '2020-08-29 10:00:45'),
(95, 16, 4, '139.195.134.22', '2020-08-20 13:09:33', '2020-08-28 03:01:07', '85c7c046d3f65d3407aafe9e5a1b73c8c74d97b01eae257e44aa9d5bade97baf3f8ece275bf34f7bde92756c5d830426', '2020-09-02 03:01:07', 844, 'I', '2020-08-20 13:09:33', '2020-08-28 03:01:07'),
(96, 16, 4, '139.195.134.22', '2020-08-22 03:14:40', '2020-08-22 13:13:17', 'f59f4956550028265a5de34ca8181a17c74d97b01eae257e44aa9d5bade97bafe037dc84c4fc95b0bc2b9fad79a37754', '2020-08-27 13:13:17', 75, 'I', '2020-08-22 03:14:40', '2020-08-22 13:13:17'),
(97, 28, 7, '110.136.218.164', '2020-08-22 10:51:21', '2020-08-25 10:23:35', 'a96c4ae9b7405a2acc8568ebd140442333e75ff09dd601bbe69f351039152189879d960d0ee8986e51f2c7abddb92b63', '2020-08-30 10:23:35', 86, 'I', '2020-08-22 10:51:21', '2020-08-25 10:23:35'),
(98, 16, 4, '139.195.134.22', '2020-08-25 12:45:50', '2020-08-25 12:45:50', 'de6f0623383a9cc38e6a73de0276026ac74d97b01eae257e44aa9d5bade97bafbd1166b32a13ba0e0e28fa9d061facb8', '2020-08-30 12:45:50', 1, 'I', '2020-08-25 12:45:50', '2020-08-25 12:45:50'),
(99, 16, 4, '139.195.134.22', '2020-08-29 10:00:56', '2020-09-02 11:39:12', '3b073e070b588fae2e39bc01b77f72a9c74d97b01eae257e44aa9d5bade97baf640335af561c1138a5668932793669e7', '2020-09-07 11:39:12', 138, 'I', '2020-08-29 10:00:56', '2020-09-02 11:39:12'),
(100, 37, 16, '139.195.134.22', '2020-08-31 12:44:14', '2020-08-31 12:44:30', '91153be5d45a32d6f0c3a85ab7333531a5bfc9e07964f8dddeb95fc584cd965d4a871d91f630c46ee176cde564229c07', '2020-09-05 12:44:30', 8, 'I', '2020-08-31 12:44:14', '2020-08-31 12:44:30'),
(101, 16, 13, '114.124.161.149', '2020-09-01 08:35:07', '2020-09-02 09:06:47', 'b4ce3f5e6ae2fd3312bf297cca287182c74d97b01eae257e44aa9d5bade97bafc21c0f6809cc8be250f5348184ab8734', '2020-09-07 09:06:47', 63, 'I', '2020-09-01 08:35:07', '2020-09-02 09:06:47'),
(102, 16, 18, '116.206.40.79', '2020-09-01 15:03:42', '2020-09-01 15:17:41', '4bfef6d66b6cabd7b3950f23a39849d5c74d97b01eae257e44aa9d5bade97baf661559dce936b4ee64f387eee850008a', '2020-09-06 15:17:41', 20, 'I', '2020-09-01 15:03:42', '2020-09-01 15:17:41'),
(103, 16, 19, '114.5.243.250', '2020-09-01 15:05:20', '2020-09-01 15:44:48', '10607fe0f86ca5b8cc37420207696321c74d97b01eae257e44aa9d5bade97baf68087ab60aa5d8e9ea3478b6ac66321e', '2020-09-06 15:44:48', 75, 'I', '2020-09-01 15:05:20', '2020-09-01 15:44:48'),
(104, 16, 20, '::1', '2021-01-05 10:50:00', '2021-01-05 11:00:06', '57c5dd5fcc42d5c55f52a8fac11721c9c74d97b01eae257e44aa9d5bade97baff2726c3a2964b59d0bea4142018a4f53', '2021-01-10 11:00:06', 2, 'I', '2021-01-05 10:50:00', '2021-01-05 11:00:06'),
(105, 16, 21, '125.166.118.48', '2021-01-05 18:36:26', '2021-01-05 19:05:02', '5b57d14af21418e435777a9174987665c74d97b01eae257e44aa9d5bade97bafd8ffe439544ab2cbd54736de81b5a40a', '2021-01-10 19:05:02', 2, 'I', '2021-01-05 18:36:26', '2021-01-05 19:05:02'),
(106, 37, 4, '114.124.130.59', '2021-01-05 18:58:32', '2021-01-05 19:52:45', '975c1bc40c5870a5549c2f8f64de8e9ea5bfc9e07964f8dddeb95fc584cd965dc136e0dfa7baa7e953e4f8633b246c44', '2021-01-10 19:52:45', 75, 'I', '2021-01-05 18:58:32', '2021-01-05 19:52:45'),
(107, 28, 22, '125.166.116.195', '2021-01-05 19:06:13', '2021-01-07 22:40:15', '7a0b98f4e8f730e01c9a2ebfd5373bd233e75ff09dd601bbe69f351039152189b1c4df7593fadaa801ef0da8dbc55d2c', '2021-01-12 22:40:15', 12, 'I', '2021-01-05 19:06:13', '2021-01-07 22:40:15'),
(108, 28, 20, '125.166.118.48', '2021-01-05 19:08:24', '2021-01-05 19:08:24', '121d8ef4fd7b2fbff71cebb102ca323233e75ff09dd601bbe69f351039152189120defa6d49aadb1b05a033b4a89e8b9', '2021-01-10 19:08:24', 1, 'I', '2021-01-05 19:08:24', '2021-01-05 19:08:24'),
(109, 16, 23, '125.166.118.22', '2021-01-05 19:43:33', '2021-01-09 20:18:11', '81bdeac4b69558c7e8e99e0f51a584e5c74d97b01eae257e44aa9d5bade97baf47833a7cd3bcd9b077d62798cb3fb25c', '2021-01-14 20:18:11', 169, 'I', '2021-01-05 19:43:33', '2021-01-09 20:18:11'),
(110, 16, 24, '202.67.40.31', '2021-01-05 20:11:48', '2021-01-06 18:11:02', '34ccd07e5547d8cf83cfb66bc1687899c74d97b01eae257e44aa9d5bade97baf0bf65f0b071b7c628d2e523e39ced999', '2021-01-11 18:11:02', 32, 'I', '2021-01-05 20:11:48', '2021-01-06 18:11:02'),
(111, 16, 24, '202.67.41.1', '2021-01-06 21:08:11', '2021-01-07 17:52:02', 'fe42c6619b3fee0131537f78d445272ec74d97b01eae257e44aa9d5bade97baf070d4221d4bff7da3fb434c41c033cd8', '2021-01-12 17:52:02', 29, 'I', '2021-01-06 21:08:11', '2021-01-07 17:52:02'),
(112, 16, 4, '139.228.249.94', '2021-01-07 22:06:36', '2021-01-09 09:04:05', '2e6e824eadf341ce2bae66b8c4d93d52c74d97b01eae257e44aa9d5bade97baf888db5923dc93943230a2868873d4633', '2021-01-14 09:04:05', 24, 'I', '2021-01-07 22:06:36', '2021-01-09 09:04:05'),
(113, 16, 24, '202.67.40.220', '2021-01-07 22:22:37', '2021-01-07 22:22:39', 'f6b48bb88051ba8395a5e64c0fcdeb1ec74d97b01eae257e44aa9d5bade97baf86c068f0da7ee62191cf00588e6c54e3', '2021-01-12 22:22:39', 3, 'I', '2021-01-07 22:22:37', '2021-01-07 22:22:39'),
(114, 32, 23, '125.166.116.195', '2021-01-07 23:32:31', '2021-01-07 23:32:41', '98f9f4c89db480d523c75cc7b81902316364d3f0f495b6ab9dcf8d3b5c6e0b01ca6fd6ccb1c21b65d5ca4ceee9e506ab', '2021-01-12 23:32:41', 5, 'I', '2021-01-07 23:32:31', '2021-01-07 23:32:41'),
(115, 28, 23, '125.166.116.195', '2021-01-07 23:33:21', '2021-01-08 00:01:27', '3334ea24643407475ac12bd1005b9ade33e75ff09dd601bbe69f351039152189e0d38f53dc58cd388a6a35d54dd3fb70', '2021-01-13 00:01:27', 9, 'I', '2021-01-07 23:33:21', '2021-01-08 00:01:27'),
(116, 35, 23, '125.166.116.195', '2021-01-07 23:34:09', '2021-01-08 00:08:19', 'aaba0317431703fbc677c212b74c6be61c383cd30b7c298ab50293adfecb7b184000153ae6d9da1e012b760c48befa35', '2021-01-13 00:08:19', 23, 'I', '2021-01-07 23:34:09', '2021-01-08 00:08:19'),
(117, 27, 23, '125.166.116.195', '2021-01-08 00:00:53', '2021-01-08 00:00:54', '4ec10bb986b470f66d4a8bb70278f36002e74f10e0327ad868d138f2b4fdd6f0fcc152d052f0ea4284e7a936e6b10020', '2021-01-13 00:00:54', 3, 'I', '2021-01-08 00:00:53', '2021-01-08 00:00:54'),
(118, 29, 23, '125.166.116.195', '2021-01-08 00:01:47', '2021-01-08 00:01:48', 'f90ac0d28e31b90b72dafaae351ee2f26ea9ab1baa0efb9e19094440c317e21b80f0b7823f3b9bc1db9276e121bd99db', '2021-01-13 00:01:48', 3, 'I', '2021-01-08 00:01:47', '2021-01-08 00:01:48'),
(119, 16, 24, '202.67.40.29', '2021-01-09 12:13:02', '2021-01-22 23:15:12', 'cd7d47e89f9123b281323fafc9cddc32c74d97b01eae257e44aa9d5bade97baf115e6c039c221564dc4a49e138a51c24', '2021-01-27 23:15:12', 427, 'I', '2021-01-09 12:13:02', '2021-01-22 23:15:12'),
(120, 37, 23, '125.166.118.22', '2021-01-09 20:31:25', '2021-01-09 20:32:04', '2afd998b06b0462420d0a9d18be22fdea5bfc9e07964f8dddeb95fc584cd965d61547d3fe78af951d61eaba4e7041908', '2021-01-14 20:32:04', 8, 'I', '2021-01-09 20:31:25', '2021-01-09 20:32:04'),
(121, 35, 23, '125.166.118.123', '2021-01-09 20:32:39', '2021-01-11 19:42:24', '1542dc30b0b3766bf04b78db0fcdfd8e1c383cd30b7c298ab50293adfecb7b180c0eba1e985fc3f5f1de03899c2ac706', '2021-01-16 19:42:24', 137, 'I', '2021-01-09 20:32:39', '2021-01-11 19:42:24'),
(122, 16, 19, '120.188.86.107', '2021-01-11 19:12:08', '2021-01-15 17:33:29', '046869c5f33e6fe64310abdda982046ec74d97b01eae257e44aa9d5bade97baf4f2887a6302eb419d398e3d729d012b9', '2021-01-20 17:33:29', 154, 'I', '2021-01-11 19:12:08', '2021-01-15 17:33:29'),
(123, 16, 23, '125.166.118.123', '2021-01-11 19:31:42', '2021-01-11 21:19:57', 'd7260c95402b3e4bbada2f9068841009c74d97b01eae257e44aa9d5bade97baffa68d6209b83e2a8fcc0ccb857fa90c2', '2021-01-16 21:19:57', 173, 'I', '2021-01-11 19:31:42', '2021-01-11 21:19:57'),
(124, 16, 20, '125.166.118.123', '2021-01-11 20:25:54', '2021-01-11 22:07:17', '8182fb318d0ecff9d9956efc7ca637d0c74d97b01eae257e44aa9d5bade97baf2c4d86ae9a69be8bbc8bcbb709d2c338', '2021-01-16 22:07:17', 17, 'I', '2021-01-11 20:25:54', '2021-01-11 22:07:17'),
(125, 16, 4, '114.124.139.230', '2021-01-11 20:56:56', '2021-01-16 08:41:50', '01fe33692b5f9e150f16d8f3eb6351c8c74d97b01eae257e44aa9d5bade97bafdaed0c86e167f6dac51e3c84b4b05536', '2021-01-21 08:41:50', 89, 'I', '2021-01-11 20:56:56', '2021-01-16 08:41:50'),
(126, 38, 23, '125.166.118.123', '2021-01-11 21:17:52', '2021-01-11 21:18:16', 'd36d893d55fbc9b8260e353c0ce5a615a5771bce93e200c36f7cd9dfd0e5deaa9e9adfa2c98ef370c2e35635e9ab46bd', '2021-01-16 21:18:16', 8, 'I', '2021-01-11 21:17:52', '2021-01-11 21:18:16'),
(127, 30, 23, '114.125.124.252', '2021-01-11 21:18:43', '2021-01-11 21:28:34', 'fbb5fbbce2bc552f46fc88836192a37834173cb38f07f89ddbebc2ac9128303f71b3acaa7b9ee9de3141da7cd7070934', '2021-01-16 21:28:34', 27, 'I', '2021-01-11 21:18:43', '2021-01-11 21:28:34'),
(128, 16, 23, '114.125.127.28', '2021-01-11 21:28:48', '2021-01-11 21:33:41', 'c0158b8c46f0ca71f5ae1f0502d258f0c74d97b01eae257e44aa9d5bade97bafd8caad5ba883a8b4ebe53158c55b3a67', '2021-01-16 21:33:41', 38, 'I', '2021-01-11 21:28:48', '2021-01-11 21:33:41'),
(129, 30, 23, '114.125.126.216', '2021-01-11 21:33:53', '2021-01-11 22:00:03', '9f966c303e633fb02e6b1980cd6f461134173cb38f07f89ddbebc2ac9128303f80d35789e93f339d682828bc78fde450', '2021-01-16 22:00:03', 20, 'I', '2021-01-11 21:33:53', '2021-01-11 22:00:03'),
(130, 16, 23, '125.166.118.123', '2021-01-11 22:00:19', '2021-01-11 22:22:54', '8af3ff23141c46a66b09912d9e5da538c74d97b01eae257e44aa9d5bade97bafb0249c2bfa5e295fb8ab9c2ba65a232c', '2021-01-16 22:22:54', 58, 'I', '2021-01-11 22:00:19', '2021-01-11 22:22:54'),
(131, 30, 23, '125.166.118.123', '2021-01-11 22:23:15', '2021-01-12 18:24:57', 'ff6737a1ab49257642a6584cc2ad8ef034173cb38f07f89ddbebc2ac9128303fa4ef0678a1435cac3b7c76727e28b47c', '2021-01-17 18:24:57', 38, 'I', '2021-01-11 22:23:15', '2021-01-12 18:24:57'),
(132, 35, 23, '114.125.78.255', '2021-01-12 20:11:24', '2021-01-15 17:31:12', 'd5e8b73d905de4de492a9911d5d069d91c383cd30b7c298ab50293adfecb7b18d3c258fc9775ab9ce3bc47bbced95dae', '2021-01-20 17:31:12', 45, 'I', '2021-01-12 20:11:24', '2021-01-15 17:31:12'),
(133, 35, 20, '125.166.118.7', '2021-01-12 21:31:35', '2021-01-13 21:04:28', '6371b3373ba8da9f449f2e29cc5181351c383cd30b7c298ab50293adfecb7b18c502bc93fc665981ec695c81a671263c', '2021-01-18 21:04:28', 10, 'I', '2021-01-12 21:31:35', '2021-01-13 21:04:28'),
(134, 16, 23, '182.1.118.172', '2021-01-15 17:32:12', '2021-01-20 19:56:58', 'b3853ac3822efc3dcfbfe4d7711d50a5c74d97b01eae257e44aa9d5bade97baf1806e210dfa50b99314df7ca631216c5', '2021-01-25 19:56:58', 33, 'I', '2021-01-15 17:32:12', '2021-01-20 19:56:58'),
(135, 35, 23, '125.166.119.173', '2021-01-20 19:59:44', '2021-01-20 19:59:47', '6b0c47f34b9e21093f1da4fb911d6c771c383cd30b7c298ab50293adfecb7b18a63bd5a342b0ba2eb2073b6dd3e6ae25', '2021-01-25 19:59:47', 4, 'I', '2021-01-20 19:59:44', '2021-01-20 19:59:47'),
(136, 16, 23, '125.166.119.173', '2021-01-20 20:07:44', '2021-01-20 20:08:02', '03f2f9ddadff20d8e609cda14ed196fac74d97b01eae257e44aa9d5bade97baf3ef52b337697b9be2c7a4ffceaf062a0', '2021-01-25 20:08:02', 4, 'I', '2021-01-20 20:07:44', '2021-01-20 20:08:02'),
(137, 35, 23, '182.1.120.125', '2021-01-20 20:24:27', '2021-01-20 20:25:01', 'c9f88fadf9536cbb2e36971ed6d9e3451c383cd30b7c298ab50293adfecb7b186bcfd748432d32d4f46ffc3315cadf8e', '2021-01-25 20:25:01', 5, 'I', '2021-01-20 20:24:27', '2021-01-20 20:25:01'),
(138, 30, 23, '182.1.115.121', '2021-01-20 21:16:11', '2021-01-20 21:16:12', '7d8c16a1f40e3ff34fdd825b711a6a4834173cb38f07f89ddbebc2ac9128303f84a330d47738c18540875f08233c350b', '2021-01-25 21:16:12', 3, 'I', '2021-01-20 21:16:11', '2021-01-20 21:16:12'),
(139, 28, 23, '182.1.110.101', '2021-01-20 21:19:38', '2021-01-20 21:19:39', '8c4138fe66215b410ae117eab9142e8733e75ff09dd601bbe69f351039152189c181a12d7b5f06d324569354323cb11e', '2021-01-25 21:19:39', 3, 'I', '2021-01-20 21:19:38', '2021-01-20 21:19:39'),
(140, 27, 23, '114.125.108.29', '2021-01-20 21:23:43', '2021-01-20 21:23:45', 'e0e8928359ba70841e2a0717cc70816002e74f10e0327ad868d138f2b4fdd6f0ea14ba5264486e95c301c9a202a4ccd5', '2021-01-25 21:23:45', 3, 'I', '2021-01-20 21:23:43', '2021-01-20 21:23:45'),
(141, 29, 23, '182.1.118.153', '2021-01-20 21:25:32', '2021-01-20 21:25:33', '420ddf4207e77617ad4b8b1d3a2394d96ea9ab1baa0efb9e19094440c317e21b0c4db520634fe92086ce0fedca5a4a9f', '2021-01-25 21:25:33', 3, 'I', '2021-01-20 21:25:32', '2021-01-20 21:25:33'),
(142, 31, 23, '114.125.108.29', '2021-01-20 21:26:50', '2021-01-20 21:26:52', 'c23495d54f5d07bd79931ef7cd8c13a2c16a5320fa475530d9583c34fd356ef522babc7f01b89098e060032dc913752c', '2021-01-25 21:26:52', 3, 'I', '2021-01-20 21:26:50', '2021-01-20 21:26:52'),
(143, 16, 23, '182.1.118.153', '2021-01-20 21:30:02', '2021-01-20 21:30:04', '06d814942c1b8cfeb4c93e56c8c390a7c74d97b01eae257e44aa9d5bade97baf3e5401b8161d8bc83db136fa0ebcf9d4', '2021-01-25 21:30:04', 3, 'I', '2021-01-20 21:30:02', '2021-01-20 21:30:04'),
(144, 27, 23, '182.1.123.113', '2021-01-20 21:31:45', '2021-01-20 21:31:47', '911c73ca169b481d585bbc09c313185302e74f10e0327ad868d138f2b4fdd6f02ffddb15134352349e772f61bb6efdda', '2021-01-25 21:31:47', 3, 'I', '2021-01-20 21:31:45', '2021-01-20 21:31:47'),
(145, 28, 23, '182.1.123.113', '2021-01-20 21:32:28', '2021-01-20 21:32:29', '5227974eeebe08779d882ddb73b24efd33e75ff09dd601bbe69f351039152189292e35a137d89d19bb2bab90766984a3', '2021-01-25 21:32:29', 3, 'I', '2021-01-20 21:32:28', '2021-01-20 21:32:29'),
(146, 29, 23, '182.1.123.113', '2021-01-20 21:33:08', '2021-01-20 21:33:09', '63e331481b0e6c07b3ac5378c838eea66ea9ab1baa0efb9e19094440c317e21b1cf465729fc07c490f13c13241a8513e', '2021-01-25 21:33:09', 3, 'I', '2021-01-20 21:33:08', '2021-01-20 21:33:09'),
(147, 31, 23, '182.1.123.113', '2021-01-20 21:33:42', '2021-01-20 21:33:43', 'fda665269f2fe83f0ca573d5c6342016c16a5320fa475530d9583c34fd356ef533537f3d8affa9637afc842cf3ca2f9e', '2021-01-25 21:33:43', 3, 'I', '2021-01-20 21:33:42', '2021-01-20 21:33:43'),
(148, 35, 23, '125.166.119.113', '2021-01-20 21:34:22', '2021-01-21 18:49:50', '3f5a48d8b00254bcb89ff82c1e0913d61c383cd30b7c298ab50293adfecb7b18988ebaa6ca3075f76110217ca469d277', '2021-01-26 18:49:50', 9, 'I', '2021-01-20 21:34:22', '2021-01-21 18:49:50'),
(149, 16, 23, '182.1.73.251', '2021-01-21 19:18:31', '2021-01-23 18:14:05', '4af49bd82d9cabded986822a6b085527c74d97b01eae257e44aa9d5bade97baff4b6778df94d3ac4aee6af3380fb3cd9', '2021-01-28 18:14:05', 22, 'I', '2021-01-21 19:18:31', '2021-01-23 18:14:05'),
(544, 16, 23, '125.166.116.94', '2021-01-30 11:09:01', '2021-02-03 17:01:44', 'e7bba7acf557aeb51063508880de7b52c74d97b01eae257e44aa9d5bade97baf2defee2a2ba18f0c0c7f147574075e9d', '2021-02-08 17:01:44', 55, 'I', '2021-01-30 11:09:01', '2021-02-03 17:01:44'),
(625, 35, 23, '125.166.116.216', '2021-02-01 19:08:21', '2021-02-01 19:21:09', '86d662c41cb63112f51dd1ed26f922ba1c383cd30b7c298ab50293adfecb7b184dd964ff9f57405b20074c16f6560e6d', '2021-02-06 19:21:09', 28, 'I', '2021-02-01 19:08:21', '2021-02-01 19:21:09'),
(626, 30, 23, '125.166.116.216', '2021-02-01 19:16:48', '2021-02-01 19:20:33', 'c735c4e8d1f7bc514afb21d5389a899e34173cb38f07f89ddbebc2ac9128303faddb991c32732dfef0eef785ced03550', '2021-02-06 19:20:33', 27, 'I', '2021-02-01 19:16:48', '2021-02-01 19:20:33'),
(641, 16, 23, '114.125.76.67', '2021-02-01 21:05:49', '2021-02-01 21:37:27', '3208a8b04b2aaca4773cc08ccc357ac8c74d97b01eae257e44aa9d5bade97bafc215566c1399320523eb27805bd1f98e', '2021-02-06 21:37:27', 43, 'I', '2021-02-01 21:05:49', '2021-02-01 21:37:27'),
(642, 16, 4, '139.228.121.207', '2021-02-01 21:25:36', '2021-02-03 18:43:00', 'f5fff10921d03bd3e5b705cf151510adc74d97b01eae257e44aa9d5bade97baf364d92a2a7d58c935dd666d618d46d39', '2021-02-08 18:43:00', 63, 'I', '2021-02-01 21:25:36', '2021-02-03 18:43:00'),
(683, 16, 23, '182.1.74.119', '2021-02-02 14:57:28', '2021-02-02 14:57:29', '8662bbab22eacd82f17ab737e5a2befcc74d97b01eae257e44aa9d5bade97baf9bec947da00317b81b0a48fb607def12', '2021-02-07 14:57:29', 3, 'I', '2021-02-02 14:57:28', '2021-02-02 14:57:29'),
(684, 30, 23, '114.125.68.141', '2021-02-02 14:57:44', '2021-02-06 00:47:46', 'b2b7821f3d1b648e0f25e88a895991c834173cb38f07f89ddbebc2ac9128303f3bbffb5363fc26663d068ec260ef6916', '2021-02-11 00:47:46', 25, 'I', '2021-02-02 14:57:44', '2021-02-06 00:47:46'),
(697, 30, 23, '182.1.97.98', '2021-02-03 17:03:53', '2021-02-03 17:51:14', '14541dbe5787e417ca0d369c2f4a416434173cb38f07f89ddbebc2ac9128303fdcc1579848ca740fef5b1c5683bfe9b4', '2021-02-08 17:51:14', 30, 'I', '2021-02-03 17:03:53', '2021-02-03 17:51:14'),
(698, 35, 23, '182.1.117.206', '2021-02-03 17:11:41', '2021-02-03 17:24:31', '047bdfea14d063e4ef8906d73aa6be3b1c383cd30b7c298ab50293adfecb7b18a3a55ed0f86ef9a2c2115387214818cc', '2021-02-08 17:24:31', 27, 'I', '2021-02-03 17:11:41', '2021-02-03 17:24:31'),
(704, 35, 24, '114.5.243.122', '2021-02-03 17:14:35', '2021-02-03 17:14:44', 'd2af6c7bef19130e7183490762d8a93e1c383cd30b7c298ab50293adfecb7b180df6cea7e75a1e804d73fb2a94f02bb9', '2021-02-08 17:14:44', 7, 'I', '2021-02-03 17:14:35', '2021-02-03 17:14:44'),
(705, 30, 23, '182.1.117.206', '2021-02-03 17:15:14', '2021-02-03 17:22:58', '42ccfa484be9d3052ed545ee5df0dbc334173cb38f07f89ddbebc2ac9128303fc657ffb9f1585f7d3da92c599be5db3e', '2021-02-08 17:22:58', 51, 'I', '2021-02-03 17:15:14', '2021-02-03 17:22:58'),
(706, 30, 24, '202.67.46.239', '2021-02-03 17:15:35', '2021-02-03 17:23:19', '53aa42c99c2b7c4aa0b87306092e034b34173cb38f07f89ddbebc2ac9128303f15e29b7c03ece8aeb5e3723842487448', '2021-02-08 17:23:19', 26, 'I', '2021-02-03 17:15:35', '2021-02-03 17:23:19'),
(707, 35, 24, '202.67.40.2', '2021-02-03 17:23:56', '2021-02-03 22:32:09', '8d9ad932cb4d17ae37b2f674ae36b5961c383cd30b7c298ab50293adfecb7b18a6e282ea7f4bd90143611869b47b2800', '2021-02-08 22:32:09', 239, 'I', '2021-02-03 17:23:56', '2021-02-03 22:32:09'),
(708, 37, 23, '182.1.123.126', '2021-02-03 17:25:07', '2021-02-03 17:25:28', 'b9ee8d9fe3f938e0b0cdf2a1d3d0d61aa5bfc9e07964f8dddeb95fc584cd965ded6ceac91678413d81ce7a3d2faa10fd', '2021-02-08 17:25:28', 9, 'I', '2021-02-03 17:25:07', '2021-02-03 17:25:28'),
(709, 36, 23, '182.1.127.106', '2021-02-03 17:25:41', '2021-02-03 17:25:48', 'db54d2fb56f1aa3a0eafec94675dfc8519ca14e7ea6328a42e0eb13d585e4c222e51546b45bba96e02f21444bfeb94ca', '2021-02-08 17:25:48', 5, 'I', '2021-02-03 17:25:41', '2021-02-03 17:25:48'),
(710, 31, 23, '114.125.125.14', '2021-02-03 17:26:25', '2021-02-03 17:29:53', '347e45ce05e5c1bb61b208c213ea8d13c16a5320fa475530d9583c34fd356ef5f7eff713507429ecae0761b9e8a60dd5', '2021-02-08 17:29:53', 5, 'I', '2021-02-03 17:26:25', '2021-02-03 17:29:53'),
(711, 35, 23, '182.1.102.98', '2021-02-03 17:33:34', '2021-02-03 17:42:42', 'd85ed558adc58ac8433e0a21e61b9c371c383cd30b7c298ab50293adfecb7b188b7cd8f8941ab565cf55dbe4530d139c', '2021-02-08 17:42:42', 24, 'I', '2021-02-03 17:33:34', '2021-02-03 17:42:42'),
(712, 16, 23, '182.1.127.106', '2021-02-03 17:43:01', '2021-02-03 17:49:32', '2f740d265e6518275bde2290e27cf732c74d97b01eae257e44aa9d5bade97baf814e416f0bd43ce6d90d8c9565eb68c3', '2021-02-08 17:49:32', 33, 'I', '2021-02-03 17:43:01', '2021-02-03 17:49:32'),
(713, 35, 23, '182.1.97.98', '2021-02-03 17:51:30', '2021-02-03 18:27:18', 'eb52d6e4a8c7d4bd1fcfc490ee6809ec1c383cd30b7c298ab50293adfecb7b182d2a09270f74f954cb8d198e3fad0056', '2021-02-08 18:27:18', 14, 'I', '2021-02-03 17:51:30', '2021-02-03 18:27:18'),
(714, 16, 23, '114.125.111.228', '2021-02-03 18:27:40', '2021-02-03 20:59:21', 'f2c9920683fb6586631e0db058671432c74d97b01eae257e44aa9d5bade97baf4e3cad4afdf29413b611c33194d915d4', '2021-02-08 20:59:21', 25, 'I', '2021-02-03 18:27:40', '2021-02-03 20:59:21'),
(715, 37, 4, '139.228.121.207', '2021-02-03 18:43:18', '2021-02-06 10:16:39', 'ccd7b4de200422a2abfddab6fd3ef8f3a5bfc9e07964f8dddeb95fc584cd965d8830b8ea793782f4b3d887420971f5f9', '2021-02-11 10:16:39', 51, 'I', '2021-02-03 18:43:18', '2021-02-06 10:16:39'),
(716, 30, 23, '182.1.101.188', '2021-02-03 20:59:35', '2021-02-03 21:01:45', '1cbae6e25d5362aaf0504c124ae5a01b34173cb38f07f89ddbebc2ac9128303f0add3bc1c7f3068f847bbd6e9df19073', '2021-02-08 21:01:45', 11, 'I', '2021-02-03 20:59:35', '2021-02-03 21:01:45'),
(717, 35, 23, '182.1.101.188', '2021-02-03 21:01:55', '2021-02-03 21:01:59', '2eae8859d15a1851779b62f208843ddb1c383cd30b7c298ab50293adfecb7b18ecd20ac319feb79c2c8b27c974d26a02', '2021-02-08 21:01:59', 6, 'I', '2021-02-03 21:01:55', '2021-02-03 21:01:59'),
(718, 30, 23, '114.125.111.228', '2021-02-03 21:04:05', '2021-02-03 21:14:23', '30ea17cbd13a00172d89c3edbf0c444934173cb38f07f89ddbebc2ac9128303fafa53ef31e6ee1f623a6b8dfcbe8c792', '2021-02-08 21:14:23', 25, 'I', '2021-02-03 21:04:05', '2021-02-03 21:14:23'),
(719, 35, 23, '182.1.116.215', '2021-02-03 21:14:33', '2021-02-04 16:16:15', '3347dad3c5b5d8d0e86a625bdb32fe9d1c383cd30b7c298ab50293adfecb7b18ceb1842e33bf16b338a64457debab7c3', '2021-02-09 16:16:15', 31, 'I', '2021-02-03 21:14:33', '2021-02-04 16:16:15'),
(720, 30, 24, '202.67.40.215', '2021-02-03 22:32:26', '2021-02-10 20:12:33', '28fbf8cc9b9b65afec7550f72477785234173cb38f07f89ddbebc2ac9128303fc8acac2908d25c8f4d2c74756b5374e4', '2021-02-15 20:12:33', 82, 'I', '2021-02-03 22:32:26', '2021-02-10 20:12:33'),
(721, 30, 23, '125.166.116.94', '2021-02-04 16:16:26', '2021-02-05 14:41:12', '12874bfa8c5f1b57d2510463395cb16934173cb38f07f89ddbebc2ac9128303f5f0d51cdb1d563ee06a04360a53c794e', '2021-02-10 14:41:12', 23, 'I', '2021-02-04 16:16:26', '2021-02-05 14:41:12'),
(722, 16, 23, '114.125.68.141', '2021-02-06 00:47:59', '2021-02-06 00:48:02', '25aee3315cbce85fcb9e9ebdb0420f46c74d97b01eae257e44aa9d5bade97baf0cee6e8930490ed17c279fceda8af89c', '2021-02-11 00:48:02', 4, 'I', '2021-02-06 00:47:59', '2021-02-06 00:48:02'),
(723, 32, 23, '114.125.88.69', '2021-02-06 00:48:31', '2021-02-06 00:49:32', 'f898e7cb901149ea87a06f90d00952ff6364d3f0f495b6ab9dcf8d3b5c6e0b01821e62286f8f8fe1cb452eab235c5564', '2021-02-11 00:49:32', 17, 'I', '2021-02-06 00:48:31', '2021-02-06 00:49:32'),
(724, 30, 23, '114.125.86.39', '2021-02-06 00:49:45', '2021-02-06 01:01:29', '4c7a6e5a85a46fcb51d37a4f98e9826a34173cb38f07f89ddbebc2ac9128303f6a629658831f4256a4c26ce6acbceade', '2021-02-11 01:01:29', 17, 'I', '2021-02-06 00:49:45', '2021-02-06 01:01:29'),
(725, 35, 24, '202.67.46.9', '2021-02-06 19:36:49', '2021-02-06 20:26:46', 'f1abc8a367e1687fcda4d06fadef588c1c383cd30b7c298ab50293adfecb7b1853628a84e1c5946f3a9a0566da94ecff', '2021-02-11 20:26:46', 30, 'I', '2021-02-06 19:36:49', '2021-02-06 20:26:46'),
(726, 16, 25, '114.125.79.211', '2021-02-07 12:54:59', '2021-02-07 12:55:28', 'd8de3eb380f7f51ac16cabcb657f7d03c74d97b01eae257e44aa9d5bade97baff0a7b9d0f6757865fe389ee4011cec3c', '2021-02-12 12:55:28', 9, 'I', '2021-02-07 12:54:59', '2021-02-07 12:55:28'),
(727, 30, 25, '125.166.119.116', '2021-02-07 12:55:52', '2021-02-07 14:33:40', '97cfaa1614ea72edef5216c1048a349434173cb38f07f89ddbebc2ac9128303f992f5a55936c7385c676a2117ea2815f', '2021-02-12 14:33:40', 15, 'I', '2021-02-07 12:55:52', '2021-02-07 14:33:40'),
(728, 35, 23, '114.125.79.211', '2021-02-07 13:14:05', '2021-02-07 13:14:37', 'ca4c8f43b47c8bbb1e588599db28328a1c383cd30b7c298ab50293adfecb7b181b7bdf48c8842940152b4557ecc53e07', '2021-02-12 13:14:37', 6, 'I', '2021-02-07 13:14:05', '2021-02-07 13:14:37'),
(729, 30, 23, '125.166.119.116', '2021-02-07 13:17:13', '2021-02-07 14:08:11', '3e6f103d0ecffdd4d27ea803fee8304c34173cb38f07f89ddbebc2ac9128303fd8973f2443606ebabafdefc8f194bef5', '2021-02-12 14:08:11', 45, 'I', '2021-02-07 13:17:13', '2021-02-07 14:08:11'),
(730, 35, 23, '125.166.119.116', '2021-02-07 14:12:13', '2021-02-07 14:12:14', '8e348188985ef8a6028f644c75b12a2e1c383cd30b7c298ab50293adfecb7b1824437620d4e544677b115559e049fff5', '2021-02-12 14:12:14', 3, 'I', '2021-02-07 14:12:13', '2021-02-07 14:12:14'),
(731, 35, 25, '182.1.83.188', '2021-02-07 14:34:07', '2021-02-07 14:55:50', '0431f1a9dfa41341990a77662ed184fc1c383cd30b7c298ab50293adfecb7b184d70246040033870c1ad46cddf60f45d', '2021-02-12 14:55:50', 36, 'I', '2021-02-07 14:34:07', '2021-02-07 14:55:50'),
(732, 30, 25, '182.1.84.112', '2021-02-07 14:56:12', '2021-02-07 17:39:26', 'e4b9249e4a6533da7c4fc278723392b034173cb38f07f89ddbebc2ac9128303f0b7360b83737dee23a42e5c9d3845ccc', '2021-02-12 17:39:26', 124, 'I', '2021-02-07 14:56:12', '2021-02-07 17:39:26'),
(733, 30, 25, '182.1.82.204', '2021-02-07 15:42:29', '2021-02-07 15:50:15', '4011740d37b6a9d28e6b1b5cb15d38f834173cb38f07f89ddbebc2ac9128303fcb17d238dba78cbc601be96269b11b47', '2021-02-12 15:50:15', 25, 'I', '2021-02-07 15:42:29', '2021-02-07 15:50:15'),
(734, 35, 25, '182.1.76.176', '2021-02-07 15:51:01', '2021-02-07 16:59:15', 'deccde814ae175084ef3528f1cd1eb8e1c383cd30b7c298ab50293adfecb7b1853bba3bfb3991405512996a663738838', '2021-02-12 16:59:15', 37, 'I', '2021-02-07 15:51:01', '2021-02-07 16:59:15'),
(735, 35, 25, '103.114.76.22', '2021-02-07 17:39:38', '2021-02-09 20:42:47', 'e5b76275bb3362a97ae469ea1c53755f1c383cd30b7c298ab50293adfecb7b18fe4391f9369cd5df6c633b17c2fbc295', '2021-02-14 20:42:47', 41, 'I', '2021-02-07 17:39:38', '2021-02-09 20:42:47'),
(736, 30, 25, '103.114.76.22', '2021-02-09 20:08:45', '2021-02-09 20:59:38', '0df754b03b13f579ea55db5d78ba1f6a34173cb38f07f89ddbebc2ac9128303f2d28b42f0337f82c6e3c9acc406213f4', '2021-02-14 20:59:38', 30, 'I', '2021-02-09 20:08:45', '2021-02-09 20:59:38'),
(737, 30, 23, '103.114.76.22', '2021-02-09 20:11:22', '2021-02-09 20:12:00', 'c5d1451736e4fafbf4aff40ba2bd1ebc34173cb38f07f89ddbebc2ac9128303f95c0632c394897e588b9838715770b26', '2021-02-14 20:12:00', 10, 'I', '2021-02-09 20:11:22', '2021-02-09 20:12:00'),
(738, 34, 26, '::1', '2021-05-09 04:52:22', '2021-05-10 14:57:21', '361edfd7de761920de07eb915fa561c5e369853df766fa44e1ed0ff613f563bdb0f0a83ff0be14b43983b7f652a1f707', '2021-05-15 14:57:21', 7, 'I', '2021-05-09 04:52:22', '2021-05-10 14:57:21'),
(739, 1000, 27, '::1', '2021-05-10 15:09:41', '2021-05-10 15:10:44', 'dd949652ef432bd5ce685d654f378f6da9b7ba70783b617e9998dc4dd82eb3c5d4cf51c219a7567c951322ad13dce425', '2021-05-15 15:10:44', 2, 'I', '2021-05-10 15:09:41', '2021-05-10 15:10:44'),
(740, 34, 28, '::1', '2021-06-01 08:48:57', '2021-06-01 08:58:56', '084c3d0fd80e5996f7fb1cd5bbcb6407e369853df766fa44e1ed0ff613f563bd8edd819c62f8543151f29d3514e4c08f', '2021-06-06 08:58:56', 9, 'I', '2021-06-01 08:48:57', '2021-06-01 08:58:56');

-- --------------------------------------------------------

--
-- Stand-in structure for view `user_view`
-- (See below for the actual view)
--
CREATE TABLE `user_view` (
`id` int(11)
,`username` varchar(100)
,`name` varchar(100)
,`no_hp` varchar(20)
,`is_active` tinyint(1)
,`password` varchar(100)
,`password_salt` varchar(100)
,`role` char(2)
,`no_rekening` varchar(200)
,`created_at` timestamp
,`updated_at` timestamp
,`reset_id` int(11)
,`temp_password` varchar(10)
);

-- --------------------------------------------------------

--
-- Table structure for table `usulan`
--

CREATE TABLE `usulan` (
  `id` int(11) NOT NULL,
  `jadwal_usulan` date DEFAULT NULL,
  `jam_usulan` time DEFAULT NULL,
  `status` char(2) NOT NULL,
  `rencana_kerja_id` int(11) NOT NULL,
  `flow_id` int(11) NOT NULL,
  `pesanan_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `rescheduler_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usulan`
--

INSERT INTO `usulan` (`id`, `jadwal_usulan`, `jam_usulan`, `status`, `rencana_kerja_id`, `flow_id`, `pesanan_id`, `created_at`, `updated_at`, `user_id`, `approver_id`, `rescheduler_id`) VALUES
(1, '2021-04-29', '14:00:00', 'P', 1, 2, 1, '2021-04-21 10:44:31', '2021-04-21 10:44:31', 30, NULL, NULL),
(2, '2021-04-28', '02:00:00', 'P', 1, 2, 3, '2021-04-23 11:48:59', '2021-04-23 11:48:59', 30, NULL, NULL),
(3, '2021-04-22', '14:05:00', 'P', 1, 2, 4, '2021-04-23 11:51:29', '2021-04-23 11:51:29', 30, NULL, NULL),
(4, '2021-05-29', '14:00:00', 'D', 1, 2, 7, '2021-05-04 02:24:51', '2021-05-04 02:44:01', 30, 999, NULL);

-- --------------------------------------------------------

--
-- Structure for view `harga_view`
--
DROP TABLE IF EXISTS `harga_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `harga_view`  AS  select `h`.`id` AS `id`,`h`.`nama` AS `nama_harga`,`h`.`kategori` AS `id_kategori`,`kp`.`nama` AS `kategori`,`h`.`harga_pokok_granit` AS `harga_pokok_granit`,`h`.`harga_pokok_keramik` AS `harga_pokok_keramik`,`h`.`harga_jual_granit` AS `harga_jual_granit`,`h`.`harga_jual_keramik` AS `harga_jual_keramik`,`h`.`note_granit` AS `note_granit`,`h`.`note_keramik` AS `note_keramik`,`h`.`created_at` AS `created_at`,`h`.`updated_at` AS `updated_at` from (`harga` `h` join `kategori_produk` `kp`) where (`h`.`kategori` = `kp`.`id`) ;

-- --------------------------------------------------------

--
-- Structure for view `komplain_view`
--
DROP TABLE IF EXISTS `komplain_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `komplain_view`  AS  select `c`.`id` AS `id`,`c`.`tgl_komplain` AS `tgl_komplain`,`c`.`tgl_jatuh_tempo` AS `tgl_jatuh_tempo`,`c`.`tgl_acc_konsumen` AS `tgl_acc_konsumen`,`c`.`description` AS `description`,`c`.`pesanan_id` AS `pesanan_id`,`c`.`user_id` AS `user_id`,`c`.`status` AS `status`,`c`.`flow_id` AS `flow_id`,`c`.`mitra_pemasang_kusen` AS `mitra_pemasang_kusen`,`c`.`mitra_pemasang_finish` AS `mitra_pemasang_finish`,`c`.`created_at` AS `created_at`,`c`.`updated_at` AS `updated_at`,`c`.`is_deleted` AS `is_deleted`,`p`.`kode_order` AS `kode_order`,`p`.`tgl_order_masuk` AS `tgl_order_masuk`,`p`.`tanggal_pasang_kusen` AS `tanggal_pasang_kusen_pesanan`,`p`.`tanggal_pasang_finish` AS `tanggal_pasang_finish_pesanan`,`p`.`tgl_acc_konsumen` AS `tgl_acc_konsumen_pesanan`,`mpk`.`name` AS `nama_pemasang_kusen`,`mpf`.`name` AS `nama_pemasang_finish`,`uk`.`jam_usulan` AS `jam_pasang_kusen`,`uk`.`jadwal_usulan` AS `tanggal_pasang_kusen`,`uf`.`jadwal_usulan` AS `jam_pasang_finish`,`uf`.`jadwal_usulan` AS `tanggal_pasang_finish`,`f`.`nama` AS `flow_name`,`f`.`rencana_kerja_id` AS `rencana_kerja_id`,`r`.`nama` AS `rencana_kerja_nama`,`p`.`konsumen_id` AS `konsumen_id`,`k`.`nama` AS `nama_konsumen`,`k`.`alamat` AS `alamat_konsumen`,`k`.`no_hp` AS `no_hp_konsumen`,`c`.`catatan` AS `catatan` from ((((((((`komplain` `c` left join `pesanan_flow` `f` on((`c`.`flow_id` = `f`.`id`))) left join `rencana_kerja` `r` on((`f`.`rencana_kerja_id` = `r`.`id`))) left join `pesanan_view` `p` on((`c`.`pesanan_id` = `p`.`id`))) left join `konsumen` `k` on((`p`.`konsumen_id` = `k`.`id`))) left join `users` `mpk` on((`c`.`mitra_pemasang_kusen` = `mpk`.`id`))) left join `users` `mpf` on((`c`.`mitra_pemasang_finish` = `mpf`.`id`))) left join `komplain_usulan` `uk` on(((`c`.`id` = `uk`.`komplain_id`) and (`uk`.`status` = 'D') and (`uk`.`rencana_kerja_id` = 1)))) left join `komplain_usulan` `uf` on(((`c`.`id` = `uf`.`komplain_id`) and (`uf`.`status` = 'D') and (`uf`.`rencana_kerja_id` = 2)))) ;

-- --------------------------------------------------------

--
-- Structure for view `pesanan_view`
--
DROP TABLE IF EXISTS `pesanan_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pesanan_view`  AS  select `p`.`id` AS `id`,`p`.`kode_order` AS `kode_order`,`p`.`tgl_order_masuk` AS `tgl_order_masuk`,`p`.`tgl_jatuh_tempo` AS `tgl_jatuh_tempo`,`p`.`tgl_acc_konsumen` AS `tgl_acc_konsumen`,`p`.`flow_id` AS `flow_id`,`f`.`nama` AS `flow_name`,`f`.`rencana_kerja_id` AS `rencana_kerja_id`,`r`.`nama` AS `rencana_kerja_nama`,`p`.`user_id` AS `user_id`,`u`.`name` AS `nama_user`,`p`.`konsumen_id` AS `konsumen_id`,`k`.`nama` AS `nama_konsumen`,`k`.`alamat` AS `alamat_konsumen`,`k`.`no_hp` AS `no_hp_konsumen`,`p`.`desainer_design` AS `desainer_design`,`dsn`.`name` AS `nama_desainer_design`,`p`.`desainer_eksekusi` AS `desainer_eksekusi`,`dsne`.`name` AS `nama_desainer_eksekusi`,`p`.`mitra_pemasang_kusen` AS `mitra_pemasang_kusen`,`mpk`.`name` AS `nama_pemasang_kusen`,`p`.`mitra_pemasang_finish` AS `mitra_pemasang_finish`,`mpf`.`name` AS `nama_pemasang_finish`,`uk`.`jam_usulan` AS `jam_pasang_kusen`,`uk`.`jadwal_usulan` AS `tanggal_pasang_kusen`,`uf`.`jam_usulan` AS `jam_pasang_finish`,`uf`.`jadwal_usulan` AS `tanggal_pasang_finish`,`p`.`is_deleted` AS `is_deleted`,`p`.`tipe` AS `tipe`,`p`.`status_produksi` AS `status_produksi`,`p`.`link` AS `link_sosmed`,`p`.`catatan` AS `catatan`,(case `ph`.`jenis` when 1 then `ph`.`harga_setuju_granit` when 2 then `ph`.`harga_setuju_keramik` end) AS `harga_deal`,(case `ph`.`jenis` when 1 then 'ranit' when 2 then 'keramik' end) AS `tipe_pengajuan`,`fd`.`fee_design` AS `fee_design`,`fd`.`persentase_eksekusi` AS `persentase_eksekusi`,`fd`.`fee_eksekusi` AS `fee_eksekusi`,(select count(0) from `pesanan_files` `pf` where ((`pf`.`pesanan_id` = `p`.`id`) and (`pf`.`tipe` = 'FA'))) AS `foto_after`,(select count(0) from `pesanan_files` `pf` where ((`pf`.`pesanan_id` = `p`.`id`) and (`pf`.`tipe` = 'EK'))) AS `edt_katalog` from ((((((((((((`pesanan` `p` left join `pesanan_flow` `f` on((`p`.`flow_id` = `f`.`id`))) left join `rencana_kerja` `r` on((`f`.`rencana_kerja_id` = `r`.`id`))) left join `users` `dsn` on((`p`.`desainer_design` = `dsn`.`id`))) left join `users` `dsne` on((`p`.`desainer_eksekusi` = `dsne`.`id`))) left join `users` `mpk` on((`p`.`mitra_pemasang_kusen` = `mpk`.`id`))) left join `users` `mpf` on((`p`.`mitra_pemasang_finish` = `mpf`.`id`))) left join `users` `u` on((`p`.`user_id` = `u`.`id`))) left join `konsumen` `k` on((`p`.`konsumen_id` = `k`.`id`))) left join `usulan` `uk` on(((`p`.`id` = `uk`.`pesanan_id`) and (`uk`.`status` = 'D') and (`uk`.`rencana_kerja_id` = 1)))) left join `usulan` `uf` on(((`p`.`id` = `uf`.`pesanan_id`) and (`uf`.`status` = 'D') and (`uf`.`rencana_kerja_id` = 2)))) left join `pengajuan_harga` `ph` on((`p`.`konsumen_id` = `ph`.`id_konsumen`))) left join `fee_desainer` `fd` on((`p`.`id` = `fd`.`pesanan_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `user_view`
--
DROP TABLE IF EXISTS `user_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user_view`  AS  select `u`.`id` AS `id`,`u`.`username` AS `username`,`u`.`name` AS `name`,`u`.`no_hp` AS `no_hp`,`u`.`is_active` AS `is_active`,`u`.`password` AS `password`,`u`.`password_salt` AS `password_salt`,`u`.`role` AS `role`,`u`.`no_rekening` AS `no_rekening`,`u`.`created_at` AS `created_at`,`u`.`updated_at` AS `updated_at`,`rp`.`id` AS `reset_id`,`rp`.`temp_password` AS `temp_password` from (`users` `u` left join `reset_password` `rp` on((`u`.`id` = `rp`.`user_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pengajuan`
--
ALTER TABLE `detail_pengajuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pengajuan_komplain`
--
ALTER TABLE `detail_pengajuan_komplain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_desainer`
--
ALTER TABLE `fee_desainer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gaji_desainer`
--
ALTER TABLE `gaji_desainer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harga_maintenance`
--
ALTER TABLE `harga_maintenance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komplain`
--
ALTER TABLE `komplain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komplain_files`
--
ALTER TABLE `komplain_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komplain_log`
--
ALTER TABLE `komplain_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komplain_usulan`
--
ALTER TABLE `komplain_usulan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `konsumen`
--
ALTER TABLE `konsumen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_senders`
--
ALTER TABLE `notification_senders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_users`
--
ALTER TABLE `notification_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran_log`
--
ALTER TABLE `pembayaran_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengajuan_harga`
--
ALTER TABLE `pengajuan_harga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengajuan_harga_komplain`
--
ALTER TABLE `pengajuan_harga_komplain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_order` (`kode_order`);

--
-- Indexes for table `pesanan_files`
--
ALTER TABLE `pesanan_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesanan_flow`
--
ALTER TABLE `pesanan_flow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesanan_log`
--
ALTER TABLE `pesanan_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rencana_kerja`
--
ALTER TABLE `rencana_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reset_password`
--
ALTER TABLE `reset_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_tokens`
--
ALTER TABLE `user_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usulan`
--
ALTER TABLE `usulan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pengajuan`
--
ALTER TABLE `detail_pengajuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=369;

--
-- AUTO_INCREMENT for table `detail_pengajuan_komplain`
--
ALTER TABLE `detail_pengajuan_komplain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `fee_desainer`
--
ALTER TABLE `fee_desainer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `gaji_desainer`
--
ALTER TABLE `gaji_desainer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `harga`
--
ALTER TABLE `harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `harga_maintenance`
--
ALTER TABLE `harga_maintenance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `komplain`
--
ALTER TABLE `komplain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `komplain_files`
--
ALTER TABLE `komplain_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `komplain_log`
--
ALTER TABLE `komplain_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `komplain_usulan`
--
ALTER TABLE `komplain_usulan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `konsumen`
--
ALTER TABLE `konsumen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `notification_senders`
--
ALTER TABLE `notification_senders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `notification_users`
--
ALTER TABLE `notification_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT for table `pembayaran_log`
--
ALTER TABLE `pembayaran_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pengajuan_harga`
--
ALTER TABLE `pengajuan_harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `pengajuan_harga_komplain`
--
ALTER TABLE `pengajuan_harga_komplain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pesanan_files`
--
ALTER TABLE `pesanan_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pesanan_flow`
--
ALTER TABLE `pesanan_flow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pesanan_log`
--
ALTER TABLE `pesanan_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;

--
-- AUTO_INCREMENT for table `rencana_kerja`
--
ALTER TABLE `rencana_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `reset_password`
--
ALTER TABLE `reset_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

--
-- AUTO_INCREMENT for table `user_tokens`
--
ALTER TABLE `user_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=741;

--
-- AUTO_INCREMENT for table `usulan`
--
ALTER TABLE `usulan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
