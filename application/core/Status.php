<?php

class Status {

    // const ACTIVE = 'A';
    // const INACTIVE = 'I';
    // const DRAFT = 'D';
    // const VOID = 'V';
    
    const STATUS_DILAPORKAN = 'L';
    const STATUS_DIALIHKAN = 'A';
    const STATUS_RESPON = 'R';
    const STATUS_PROSES = 'P';
    const STATUS_SELESAI = 'S';
    const STATUS_DIARSIPKAN = 'I';

    const STATUS_PRIVATE = "1";
    const STATUS_PUBLIC = "0";

    public static function getArray() {
        return array(
            self::STATUS_DILAPORKAN => 'Lapor',
            self::STATUS_DIALIHKAN => 'Dialihkan',
            self::STATUS_RESPON => 'Direspon',
            self::STATUS_PROSES => 'Proses',
            self::STATUS_SELESAI => 'Selesai',
            self::STATUS_DIARSIPKAN => 'Diarsipkan'
        );
    }

    public static function getArrayPrivate() {
        return array(
            self::STATUS_PRIVATE => 'Private',
            self::STATUS_PUBLIC => 'Public'
        );
    }

    public static function statusName($status = NULL) {
        $names = array(
            self::STATUS_DILAPORKAN => 'Lapor',
            self::STATUS_DIALIHKAN => 'Dialihkan',
            self::STATUS_RESPON => 'Direspon',
            self::STATUS_PROSES => 'Proses',
            self::STATUS_SELESAI => 'Selesai',
            self::STATUS_DIARSIPKAN => 'Diarsipkan'
        );
        return empty($status) ? '-' : $names[$status];
    }

    public static function privateName($status = NULL) {
        $names = array(
            self::STATUS_PRIVATE => 'Private',
            self::STATUS_PUBLIC => 'Public'
        );
        return $status==NULL ? '-' : $names[$status];
    }

    public static function statusColor($status = NULL) {
        $names = array(
            self::STATUS_DILAPORKAN => '#795548',
            self::STATUS_DIALIHKAN => '#9C27B0',
            self::STATUS_RESPON => '#00AEEF',
            self::STATUS_PROSES => '#FF9800',
            self::STATUS_SELESAI => '#4CAF50',
            self::STATUS_DIARSIPKAN => '#9C27B0'
        );
        return empty($status) ? 'white' : $names[$status];
    }

    public static function statusIcon($status = NULL) {
        $names = array(
            self::STATUS_DILAPORKAN => 'fa-book',
            self::STATUS_DIALIHKAN => 'fa-share',
            self::STATUS_RESPON => 'fa-bullhorn',
            self::STATUS_PROSES => 'fa-clock-o',
            self::STATUS_SELESAI => 'fa-check',
            self::STATUS_DIARSIPKAN => 'fa-archive'
        );
        return empty($status) ? 'white' : $names[$status];
    }

}
