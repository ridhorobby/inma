<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class WEB_Controller extends CI_Controller {
    const CTL_PATH = 'web/';
    const LIB_PATH = 'web/';
    const VIEW_PATH = 'web/';

    protected $class;
    protected $ctl;
    protected $data;
    protected $ispublic = 0;
    protected $method;
    protected $model;
    protected $postData;
    protected $template;
    protected $title = 'Nasatech INMA';
    protected $view;
    protected $page;

    protected $uri_segments = array();
    public $list_sql = array();
    /**
     * Constructor
     */
    public function __construct() {

        parent::__construct();

        // library
        $this->load->library(static::LIB_PATH . 'AuthManagerWeb');
        $this->load->library(static::LIB_PATH . 'FormatterWeb');
        $this->load->library(static::LIB_PATH . 'SessionManagerWeb');
        $this->load->library(static::LIB_PATH . 'TemplateManagerWeb');
        $this->page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        if ($this->class!='thumb'){
            // model
            $this->load->model('Notification_user_model');
            // $this->load->model('Group_member_model');
            // $this->load->model('User_model');
            // $this->load->model('Group_model');
            // $this->load->model('Post_model', 'Post');
            // $this->load->model('Absensi_model');

            // inisialisasi atribut
            $this->class = $this->router->class;
            $this->ctl = $this->getCTL();
            $this->method = ($this->router->class == $this->router->method) ? 'index' : $this->router->method;
            $this->template = new TemplateManagerWeb();
            $this->view = strtolower($this->class . (empty($this->method) ? '' : '_' . $this->method));

            // cek login
            if (empty($this->ispublic) and ! SessionManagerWeb::isAuthenticated()) {
                AuthManagerWeb::logout();
                SessionManagerWeb::setFlash(array('errmsg' => array('Anda harus login terlebih dahuluuu')));
                redirect($this->getCTL('site'));
            }

            // inisialisasi model
            $model = ucfirst($this->class . '_model');
            if (file_exists(APPPATH . 'models/' . $model . EXT)) {
                $this->load->model($model);
                $this->model = new $model();
            }

            // inisialisasi post
            $post = $this->input->post();
            if (!empty($post)) {
                foreach ($post as $k => $v) {
                    if (!is_array($v) and ! is_object($v) and strlen($v) == 0)
                        $post[$k] = NULL;
                }

                $this->postData = $post;
            } else
                $this->postData = array();
 
            // inisialisasi data
            $data = array();
            $data['path'] = static::CTL_PATH;
            $data['class'] = $this->class;
            $data['method'] = $this->method;
            $data['title'] = $this->title;
            // $data['arrmenu'] = $this->getMenu();
            $data['nopic'] = base_url('assets/web/img/nopic.png');
            $data['jmlnotif'] = (int) $this->Notification_user_model->getCountMe(SessionManagerWeb::getUserID());
            // $data['isdev'] = (SessionManagerWeb::isDeveloper() ? true : false);
            // $data['user_id'] = $this->User_model->setUserID(SessionManagerWeb::getUserID());
            // $data['me'] = $this->User_model->getUser(SessionManagerWeb::getUserID());
            // $data['me']['user_photo'] = $this->User_model->getUserPhoto($data['me']['id'],$data['me']['photo']);
            // $data['belum_rating'] = $this->isNotRatingYet();
            $this->data = $data;

            // ambil data flash
            foreach (SessionManagerWeb::getFlash() as $k => $v)
                $this->data[$k] = $v;

        
            $user_id = SessionManagerWeb::getUserID();
        }
        // echo $data['method'];die();
    
        
    }

    /**
     * Mendapatkan nama controller untuk redirect
     * @param string $ctl
     */
    protected function getCTL($ctl = null) {
        if (!isset($ctl))
            $ctl = $this->class;

        return $this::CTL_PATH . $ctl;
    }

    

    /**
     * Mendapatkan menu
     * @return array
     */
    protected function getMenu() {
        $menu = array();

        $menu[] = array('namamenu' => 'Portal', 'levelmenu' => 0, 'faicon' => 'desktop');
        // $menu[] = array('namamenu' => 'Publik', 'namafile' => $this->getCTL('post'), 'levelmenu' => 1, 'faicon' => 'home');
        // $menu[] = array('namamenu' => 'Pribadi', 'namafile' => $this->getCTL('post/me'), 'levelmenu' => 1, 'faicon' => 'user');
        // $menu[] = array('namamenu' => 'Grup', 'namafile' => $this->getCTL('group/list_me'), 'levelmenu' => 1, 'faicon' => 'comments-o');

        // $menu[] = array('namamenu' => 'Task', 'levelmenu' => 0, 'faicon' => 'line-chart');
        // $menu[] = array('namamenu' => 'To Do', 'namafile' => $this->getCTL('todo/for_new'), 'levelmenu' => 1, 'faicon' => 'calendar');

        // $menu[] = array('namamenu' => 'KM', 'namafile' => $this->getCTL('knowledge_management'), 'levelmenu' => 0, 'faicon' => 'home');

        // $menu[] = array('namamenu' => 'Edit Profil', 'namafile' => $this->getCTL('user/me'), 'levelmenu' => 1, 'faicon' => 'user');

        return $menu;
    }

    /**
     * Redirect, biasanya setelah tambah atau hapus data
     */
    protected function redirect() {
        $back = $this->postData['referer'];
        if (empty($back))
            redirect($this->ctl);
        else
            header('Location: ' . $back);
    }

    /**
     * Menampilkan query lalu exit
     */
    protected function dbExit() {
        echo '<pre>';
        print_r(BenchmarkManager::query());
        echo '</pre>';
        exit;
    }

    /**
     * Halaman detail data
     * @param int $id
     */
    public function detail($id) {
        // tombol
        $buttons = array();
        $buttons['back'] = array('label' => 'Kembali', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        // $buttons['edit'] = array('label' => 'Edit', 'type' => 'success', 'icon' => 'pencil', 'click' => 'goEdit()');
        $buttons['delete'] = array('label' => 'Hapus', 'type' => 'danger', 'icon' => 'trash-o', 'click' => 'goDelete()');

        $this->data['id'] = $id;
        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Detail ' . $this->data['title'];
    }

    /**
     * Halaman tambah data
     */
    public function add() {
        $this->edit();
    }

    /**
     * Halaman edit data
     * @param int $id
     */
    // public function edit($id = null) {
    //     // sementara disable edit, kecuali beberapa...
    //     if (isset($id) and ! in_array($this->class, array('group', 'issue', 'daily_report', 'user', 'knowledge_management')))
    //         redirect($this->getCTL() . '/detail/' . $id);

    //     $this->data['id'] = $id;

    //     // judul
    //     if (empty($this->data['title']))
    //         $this->data['title'] = (isset($id) ? 'Edit' : 'Tambah') . ' ' . $this->data['title'];

    //     // tombol
    //     if (empty($this->data['buttons'])) {
    //         $buttons = array();
    //         $buttons['back'] = array('label' => 'Kembali', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
    //         $buttons['save'] = array('label' => 'Simpan', 'type' => 'success', 'icon' => 'save', 'click' => 'goSave()');

    //         $this->data['buttons'] = $buttons;
    //     }

    //     // bila add
    //     if ($this->method == 'add')
    //         $this->view = $this->class . '_edit';
    // }

    protected function curl($url, $fields) {
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        $fields_string = rtrim($fields_string,'&');
        
        if (!function_exists('curl_exec')) {
            # no curl
            die('ERROR: no curl');
        }
        

        $url = str_replace('103.56.190.35', 'localhost', $url);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_BODY, true);
        //curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch,CURLOPT_POST,count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
        curl_exec($ch);
        curl_close($ch);
    }

    public function toNoData($message=null) {
        //$this->view_items['content']  = $default_template.'error';

        $this->template->viewDefault('_error', $this->data);

        echo $this->output->get_output();
        exit;
    }

    function imageform($folder='posts/temp')  {
        $folder_ori = $folder;

        if ($_FILES['gambar']['name']) {
            $images_arr = array();

            $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));

            $folder .= '/' . $id;

            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }

            foreach($_FILES['gambar']['name'] as $key=>$val){

                $name = $_FILES['gambar']['name'][$key];

                $_FILES['userfile']['name']= $_FILES['gambar']['name'][$key];
                $_FILES['userfile']['type']= $_FILES['gambar']['type'][$key];
                $_FILES['userfile']['tmp_name']= $_FILES['gambar']['tmp_name'][$key];
                $_FILES['userfile']['error']= $_FILES['gambar']['error'][$key];
                $_FILES['userfile']['size']= $_FILES['gambar']['size'][$key];    


                $file_name = Image::getFileName($id, Image::IMAGE_ORIGINAL, $name);
                unlink($path . $file_name);
                
                $file_mime = Image::getMime($folder.$file_name);
                $image_mime = explode('/',$file_mime);
                if ($image_mime[0]=='image') {
                    Image::upload('userfile', $id, $name, $folder);

                    $link = Image::generateLink($id, $name, $folder);
                    
                    $images_arr[] = $link['thumb'];
                    $_SESSION['sigap']['draft_post'] = 1;
                }
                
            }  

            $str = $this->_getdraftimages($folder_ori);
            die($str);
        }    
        exit;
    }

    public function sendImageWebCam($folder='posts/temp') {
        $folder_ori = $folder;

        if ($_FILES) {
            $images_arr = array();

            $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }
            $nama_file = time().'_'.SessionManagerWeb::getUserID().'.jpg';
            $target = $path.$nama_file;

            move_uploaded_file($_FILES['webcam']['tmp_name'], $target);
            // echo "<pre>";print_r('webcam');die();
            // $_FILES['webcam']['name'] = 'asd';
            // $name = $_FILES['webcam']['name'];
            // $nama_file = uniqid()().'.jpg';
            // $direktori = 'uploads/';
            // $target = $direktori.$nama_file;

            // move_uploaded_file($_FILES['webcam']['tmp_name'], $target);
            // $file_name = Image::getFileName($id, Image::IMAGE_ORIGINAL, $name);
            // echo "<pre>";print_r($file_name);die();
            // unlink($path . $file_name);
            
            $file_mime = Image::getMime($folder.$nama_file);
            $image_mime = explode('/',$file_mime);
            if ($image_mime[0]=='image') {
                Image::copy($path.'/'.$nama_file, $id, $nama_file, $folder);
                unlink($path.'/'.$nama_file);
                $link = Image::generateLink($id, $nama_file, $folder);
                $images_arr[] = $link['thumb'];
            }
            // kalau mau responsenya list dari 5 file image yang diconvert 
            // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Success"));
        } 
    }

    public function sendImage($folder='pesanan/temp') {
        $folder_ori = $folder;
        if($_GET['folder']){
            $folder_ori = $folder = $_GET['folder'].'/temp';
        }
        if ($_FILES) {
            $_FILES['gambar'] = $_FILES['file'];
            $images_arr = array();

            $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            // echo $path;
            if (!is_dir($path)) {
               mkdir($path);         
            }
            
            $name = uniqid().".".pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);

            $file_name = Image::getFileName($id, Image::IMAGE_ORIGINAL, $name);
            
            unlink($path . $file_name);
            $file_mime = Image::getMime($folder.$file_name);
            $image_mime = explode('/',$file_mime);
            if ($image_mime[0]=='image') {
                Image::upload('gambar', $id, $name, $folder);
                $link = Image::generateLink($id, $name, $folder);
                $images_arr[] = $link['thumb'];
            }

            $arr_name=explode(' ',$file_name);
            $name = implode('_',$arr_name);
            if(file_exists($path.$name)){
                echo "1";
            }else{
                echo "0";
            }
            // kalau mau responsenya list dari 5 file image yang diconvert 
            // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Success"));
            // return true;
        } 
    }

    public function checkImageWebCam($folder='posts/temp') {
        $folder_ori = $folder;

        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        if(is_dir($path)){
            $files = glob($path.'/*');
            if(!$files){
                return false;
            }
            return true;
        }
        return false;
    }

    function fileform($folder='posts/temp')  {
        $folder_ori = $folder;

        if ($_FILES['file_upload']['name']) {
            $files_arr = array();

            $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }
            foreach($_FILES['file_upload']['name'] as $key=>$val){

                $name = $_FILES['file_upload']['name'][$key];
                
                $_FILES['userfile']['name']= $_FILES['file_upload']['name'][$key];
                $_FILES['userfile']['type']= $_FILES['file_upload']['type'][$key];
                $_FILES['userfile']['tmp_name']= $_FILES['file_upload']['tmp_name'][$key];
                $_FILES['userfile']['error']= $_FILES['file_upload']['error'][$key];
                $_FILES['userfile']['size']= $_FILES['file_upload']['size'][$key];

                $file_name = File::getFileName($id, $name);
                unlink($path . $file_name);

                File::uploadFile('userfile', $id, $name, $folder);
                $link = File::generateLink($id, $name, $folder);
                $files_arr = $link;
                
                $_SESSION['sigap']['draft_post'] = 1;
            }  

            $str = $this->_getdraftfiles($folder_ori);
            die($str);
        }    
        exit;
    }    

    function resetkirim($folder='posts/temp') {
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        if (is_dir($path)) 
           $this->_deleteFiles($path);  

        unset($_SESSION['sigap']['draft_post']);
    }

    function getdraft($folder='posts/temp') {
        $str['images'] = $this->_getdraftimages($folder);
        $str['files'] = $this->_getdraftfiles($folder);
        die($str);
    }

    function _getdraftimages($folder='posts/temp') {

        $str = '';
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';

        if (is_dir($path)) {
           $files = glob($path.'thumb*'); 
           foreach ($files as $file) {
               $image_src = base_url() . $rel_path . basename($file);
               $str .= "<img src=\"$image_src\" style=\"max-height:100px\"> ";
           }
        }
        return $str;
    }

    function _getdraftfiles($folder='posts/temp') {

        $str = '';
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';
        if (is_dir($path)) {
           $files = glob($path.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $file_name = explode('-',$file_src);
               $arr_file_ext = explode('.',$file_name[count($file_name)-1]);
               $file_ext = $arr_file_ext[count($arr_file_ext)-1];
               if ($file_ext=="pdf" || $file_ext=="doc" || $file_ext=="docx" || $file_ext=="xls" || $file_ext=="xlsx" || $file_ext=="ppt" || $file_ext=="pptx"){
                    $index = 0;
                    $name = '';
                    foreach ($file_name as $fname){
                        if ($index>0) {
                            $name .=$fname;
                            if ($index<count($file_name)-1) {
                                $name .='-';
                            }
                        }
                        $index++;
                    }
                    $str .= $name . "<br>";
               }
           }
        }
        return $str;
    }



    function _deleteFiles($path){
        if (is_dir($path) === true) {
            $files = array_diff(scandir($path), array('.', '..'));
            foreach ($files as $file) {
                $this->_deleteFiles(realpath($path) . '/' . $file);
            }
            return rmdir($path);
        }

        else if (is_file($path) === true) {
            return unlink($path);
        }

        return false;
    }

    function _moveFileTemp($post_id, $tipe='OM', $folder='pesanan') {
        $id = md5(SessionManagerWeb::getUserID(). $this->config->item('encryption_key'));
        $folder_source = $folder.'/temp/'.$id;
        // echo $folder_source;die();
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $files = glob($path_source.'*'); 
            $arr_image = array();
            $arr_files = array();
            foreach ($files as $file) {
                list($mime,$ext) = explode('/',Image::getMime($file));
                $basename = basename($file);
                if ($mime=='image') {
                    $folder_dest = $folder.'/photos';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $image = Image::getName($basename);

                    // // $exp_image = explode('.', $image);
                    // $image_rename = $image.'-'.uniqid().'.'.$exp_image[1];

                    if (!in_array($image, $arr_image)) {
                        $arr_image[] = $image;
                        if($folder=='komplain'){
                            if (!$this->model->insertKomplainImage($post_id, $image, $file,$tipe, SessionManagerWeb::getUserID())) {
                                return false;
                            }
                        }else{
                            if (!$this->model->insertPesananImage($post_id, $image, $file,$tipe, SessionManagerWeb::getUserID())) {
                                return false;
                            }
                        }
                        
                    }
                }
                
                // $exp_basename = explode('-', $basename);
                
                if (!rename($file, $path_dest . '/' . $basename)){
                    return false;
                }
            }
        }
        else {
            return true;
        }
        return $this->_deleteFiles($path_source);
    }

    function removeTemp($folder_ori='pesanan',$notecho = false){
        $nama = $this->input->post('file');
        $arr_name=explode(' ',$file_name);
        $nama_file = implode('_',$arr_name);
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'].$folder_ori.'/temp'. $folder . '/';
        if($nama_file){
            $file_name_org = Image::getFileName($id, Image::IMAGE_ORIGINAL, $nama_file);
            $file_name_med = Image::getFileName($id, Image::IMAGE_MEDIUM, $nama_file);
            $file_name_thm = Image::getFileName($id, Image::IMAGE_THUMB, $nama_file);
            self::_deleteFiles($path.$file_name_org);
            self::_deleteFiles($path.$file_name_med);
            self::_deleteFiles($path.$file_name_thm);
            // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil hapus file"));
        }else{
            self::_deleteFiles($path);
            // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil hapus data"));
        }
        if($notecho==false){
            echo "1";
        }
        
    }

    function getimages($post_id) {
        $post_id = (int) $post_id;

        $sql = "select user_id from posts where id=$post_id";
        $user_id = dbGetOne($sql);

        $sql = "select * from post_image where post_id=$post_id";
        $rows = dbGetRows($sql);
        $str = '';
        $id_image = md5($user_id . $this->config->item('encryption_key'));
        foreach ($rows as $row) {
            $thumb = Image::createLink($id_image, Image::IMAGE_THUMB, $row['image'], 'posts/photos');
            $image_id = $row['id'];   
            $str .= "<div style=\"display:inline-block;\"><img src=\"{$thumb}\" style=\"max-height:100px\"/><span class=\"btn btn-group btn-xs\" style=\"position:relative;left:-24px;top:-38px;background-color:black\" onClick=\"deleteimage({$post_id},{$image_id})\"><i class=\"glyphicon glyphicon-remove\" style=\"color:white\"></i></span></div>";
        }
        echo $str;
        die();
    }

    function getgroupname($id) {
        $id = (int) $id;
        if ($id==0) 
            die('Helpdesk');
        if ($id==-1)
            die("Group");
        $sql = "select name from groups where id=$id";
        $name= dbGetOne($sql);
        die($name);
    }

    function deleteimage($post_id,$image_id) {
        $post_id = (int) $post_id;

        $sql = "delete from post_image where post_id=$post_id and id=$image_id";
        $query = dbQuery($sql);
        $this->getimages($post_id);
    }

    function getfiles($post_id) {
        $post_id = (int) $post_id;
        $sql = "select user_id from posts where id=$post_id";
        $user_id = dbGetOne($sql);

        $sql = "select * from post_file where post_id=$post_id";
        $rows = dbGetRows($sql);
        $str = '';
        $id_file = md5($user_id . $this->config->item('encryption_key'));

        $ciConfig = $this->config->item('utils');
        foreach ($rows as $row) {
            $link = File::createLink($id_file, $row['file'], 'posts/files');
            $file_id = $row['id'];
            $file_name = explode('-',basename($link));
            $index=0;
            $file="";
            foreach ($file_name as $fname){
                if ($index>0) {
                    $file .=$fname;
                    if ($index<count($file_name)-1) {
                        $file .='-';
                    }
                }
                $index++;
            }
            $str .= "<div style=\"background-color: #f5f5f5;border-style:solid;border-width:1px;border-color: black;margin-top:1px\"><i class=\"glyphicon glyphicon-file\" style=\"color:black\"></i> <strong>".$file ."</strong><span class=\"btn btn-group btn-xs\" style=\"position:relative;float:right\" onClick=\"deletefile({$post_id},{$file_id})\"><i class=\"glyphicon glyphicon-remove\" style=\"color:black\"></i></span></div>";
        }
        echo $str;
        die();
    }
    
    function deletefile($post_id,$file_id) {
        $post_id = (int) $post_id;

        $sql = "delete from post_file where post_id=$post_id and id=$file_id";
        $query = dbQuery($sql);
        $this->getfiles($post_id);
        
    }

    
}
