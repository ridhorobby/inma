<?php

class Role {

    const ADMINISTRATOR = 'A';
    const MARKETING = 'M';
    const DESAIN = 'D';
    const PRODUKSI = 'P';
    const PEMASANG = 'S';
    const MITRA_MARKETING = 'X';
    const DIREKTUR = 'R';
    const KONSUMEN = 'K';

    public static function name($role = NULL) {
        $names = array(
            self::ADMINISTRATOR => 'Administrator',
            self::MARKETING => 'Manager Marketing',
            self::DESAIN => 'Desain Staff',
            self::PRODUKSI => 'Manager Produksi',
            self::PEMASANG => 'Mitra Pemasang',
            self::MITRA_MARKETING => 'Mitra Marketing',
            self::DIREKTUR => 'Direktur'
        );
        return empty($role) ? '-' : $names[$role];
    }

    public static function getArray() {
        return array(
            self::ADMINISTRATOR => 'Administrator',
            self::MARKETING => 'Manager Marketing',
            self::DESAIN => 'Desain Staff',
            self::PRODUKSI => 'Manager Produksi',
            self::PEMASANG => 'Mitra Pemasang',
            self::MITRA_MARKETING => 'Mitra Marketing',
            self::DIREKTUR => 'Direktur'
        );
    }

    static function getStyle($role) {
    }

    public static function list_menu($role) {

        $akses = array(
            'jadwal'        => array(self::DIREKTUR, self::ADMINISTRATOR, self::PRODUKSI, self::MARKETING, self::DESAIN, self::MITRA_MARKETING, self::PEMASANG),
            'daftar_order'  => array(self::DIREKTUR, self::ADMINISTRATOR, self::PRODUKSI, self::MARKETING, self::DESAIN, self::PEMASANG, self::MITRA_MARKETING),
            'daftar_komplain' => array(self::DIREKTUR, self::ADMINISTRATOR, self::PRODUKSI, self::MARKETING,self::DESAIN,self::MITRA_MARKETING),
            'history_konsumen'=> array(self::DIREKTUR, self::ADMINISTRATOR, self::PRODUKSI, self::MARKETING,self::DESAIN,self::MITRA_MARKETING),
            'daftar_harga'  => array(self::DIREKTUR, self::ADMINISTRATOR),
            'daftar_harga_maintenance'  => array(self::DIREKTUR, self::ADMINISTRATOR),
            'daftar_pengajuan_harga'=> array(self::DIREKTUR, self::ADMINISTRATOR, self::PRODUKSI, self::MARKETING, self::DESAIN, self::MITRA_MARKETING),
            'daftar_pengajuan_harga_komplain'=> array(self::DIREKTUR, self::ADMINISTRATOR, self::MARKETING, self::PEMASANG,Role::PRODUKSI),
            'kas'   => array(self::DIREKTUR, self::ADMINISTRATOR, self::MARKETING),
            'hutang_produksi'   => array(self::DIREKTUR, self::ADMINISTRATOR, self::PRODUKSI),
            'piutang_konsumen'  => array(self::DIREKTUR, self::ADMINISTRATOR, self::MARKETING, self::MITRA_MARKETING),
            'laba_rugi' => array(self::DIREKTUR, self::ADMINISTRATOR),
            'fee_desainer'  => array(self::DIREKTUR, self::ADMINISTRATOR),
            'kpimarketing'  => array(self::DIREKTUR, self::ADMINISTRATOR, self::MARKETING),
            'daftar_user'   => array(self::DIREKTUR, self::ADMINISTRATOR)
        );
        $link = array(
            'jadwal'        => array('link' => '/pesanan/index', 'name' => 'Jadwal', 'icon' => 'nav-icon fas fa-calendar-alt'),
            'daftar_order'  => array('link' => '/pesanan/daftarorder', 'name' => 'Daftar Order', 'icon' => 'far fa-clipboard nav-icon'),
            
            'daftar_komplain' => array('link' => '/komplain/komplainbaru' , 'name'  => 'Daftar Komplain', 'icon' => 'fas fa-comments nav-icon'),
            'history_konsumen'=> array('link' => '/pesanan/history_konsumen/1', 'name' => 'History Konsumen', 'icon'  => 'fas fa-history nav-icon'),
            'daftar_harga'  => array('link' => '/harga/index', 'name' => 'Daftar Harga', 'icon' => 'fas fa-dollar-sign nav-icon'),
            'daftar_harga_maintenance'  => array('link' => '/harga_maintenance/index', 'name' => 'Daftar Harga Maintenance', 'icon' => 'fas fa-dollar-sign nav-icon'),
            'daftar_pengajuan_harga'=> array('link' => '/pengajuan_harga/index', 'name' => 'Daftar Pengajuan Harga', 'icon'  => 'fas fa-list-alt nav-icon'),
            'daftar_pengajuan_harga_komplain'=> array('link' => '/pengajuan_harga_komplain/index', 'name' => 'Pengajuan Harga Komplain', 'icon'  => 'fas fa-list-alt nav-icon'),
            'kas'   => array('link' => '/pembayaran_log/index', 'name' => 'Kas','icon' => 'fas fa-book nav-icon'),
            'hutang_produksi'   => array('link' => '/pembayaran_log/gethutangproduksiperbulan', 'name'=> 'Hutang Produksi', 'icon'  => 'fas fa-dollar-sign nav-icon'),
            'piutang_konsumen'  => array('link' => '/pembayaran_log/getListStatusPiutangKonsumen', 'name'   => 'Piutang Konsumen', 'icon'   => 'fas fa-dollar-sign nav-icon') ,
            'laba_rugi' => array('link' => '/pengajuan_harga/getLabaRugiBulanan', 'name' => 'Laba Rugi', 'icon' => 'fas fa-wallet nav-icon'),
            'fee_desainer'  => array('link' => '/fee_desainer', 'name' => 'Slip Gaji', 'icon' => 'far fa-file-alt nav-icon'),
            'kpimarketing'  => array('link' => '/pengajuan_harga/getkpimarketing', 'name' => 'KPI Marketing', 'icon' => 'far fa-chart-bar nav-icon'),
            'daftar_user'   => array('link' => '/user/index', 'name' => 'Daftar User', 'icon' => 'fas fa-user nav-icon'),
        );

        $data = array();
        foreach ($akses as $key => $value) {
            if(in_array($role, $value)){
                $data[$key] = $link[$key];

            }
        }

        return $data;
    }

    public static function menu_permission($role = NULL){
    }

}
