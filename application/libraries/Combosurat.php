<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Combosurat {
	
	function topik($name,$val='',$add='',$includekosong=true){
		$sql = "select kodetopik || ' - ' || nama as label, idsurattopik as val from surattopik order by nama";
		$rows = dbGetRows($sql);
		
		return $this->_genList($name, $rows, 0, $add, "-- Topik Surat --");
	}

	function jenis($name,$val='',$add='',$includekosong=true){
		$sql = "select nama as label, idsuratjenis as val from suratjenis order by nama";
		$rows = dbGetRows($sql);
		
		return $this->_genList($name, $rows, 0, $add, "-- Jenis Surat --");
	}

	function sifat($name,$val='',$add='',$includekosong=true){
		$sql = "select nama as label, idsuratsifat as val from suratsifat order by urutan";
		$rows = dbGetRows($sql);
		
		return $this->_genList($name, $rows, 0, $add, "-- Sifat Surat --");
	}

	function lokasi($name,$val='',$add='',$includekosong=true, $idunit=false){
		$ci = &get_instance();
		$idunits = implode(',', $ci->getIdUnitAdminUnit($idunit));
		
		if (!$idunits)
			return false;
	
		$sql = "select nama as label, idsuratlokasi as val from suratlokasi where idunit in ({$idunit}) order by nama";
		$rows = dbGetRows($sql);
		
		return $this->_genList($name, $rows, 0, $add, "-- Lokasi Surat --");
	}

	function templatesurat($name,$val='',$add='',$includekosong=true){
		if (!$_SESSION[G_SESSION]['is_admin']) {
			$sql = "select unitnomorsurat from unitkerja where idunit={$_SESSION[G_SESSION]['idunit']}";
			$unitnomorsurat = dbGetOne($sql);
			if (!$unitnomorsurat)
				$unitnomorsurat = $_SESSION[G_SESSION]['idunit'];
			
			$sql = "select nama as label, idsurattemplate as val from surattemplate
			where idunit = $unitnomorsurat order by nama";
		}
		else {
			$sql = "select distinct t.nama || ' - ' || u.nama as label, idsurattemplate as val
				from surattemplate t left join unitkerja u on t.idunit=u.idunit
				order by t.nama || ' - ' || u.nama, idsurattemplate";
		}
		
		$rows = dbGetRows($sql);
		
		return $this->_genList($name, $rows, 0, $add, "-- Template Surat --");
	}

	function templatesuratdocx($name,$val='',$add='',$includekosong=true){
		if (!$_SESSION[G_SESSION]['is_admin']) {
			$sql = "select nama, idsurattemplate from surattemplate
			where namafile ilike '%.docx' and idunit={$_SESSION[G_SESSION]['idunitlingkungan']} order by nama";
		}
		else {
			$sql = "select distinct t.nama || ' - ' || u.nama, idsurattemplate
				from surattemplate t left join unitkerja u on t.idunit=u.idunit
				where namafile ilike '%.docx'
				order by t.nama || ' - ' || u.nama, idsurattemplate";
		}
		
		$rows = dbGetRows($sql);
		
		return $this->_genList($name, $rows, 0, $add, "-- Template Surat --");
	}
	
	function _genList($name, $rows, $val, $add, $title) {
		$list = "<select name=\"$name\" id=\"$name\" $add>";
		if ($title)
			$list .= "<option value=\"\">$title</option>";
		foreach ($rows as $row) {
			$selected = '';
			if ($row['val'] == $val)
				$selected = 'selected';
			$list .= "<option value=\"{$row['val']}\" $selected>{$row['label']}</option>";
		}
		$list .= "</select>";
		
		return $list;
	}

}