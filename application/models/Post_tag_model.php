<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post_tag_model extends AppModel {

    protected $belongs_to = array(
        'Tag',
        'Post'
    );
    protected $label = array(
        'tag_id' => 'Tag',
        'post_id' => 'Postingan',
    );
    protected $validation = array(
        'tag_id' => 'required|integer',
        'post_id' => 'required|integer'
    );

}
