<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tracker_model extends AppModel {

    protected $belongs_to = array(
        'User',
        'Post'
    );
    protected $label = array(
        'user_id' => 'User',
        'post_id' => 'Postingan',
    );

    public function post($postId, $userId) {
        $this->insert(array('post_id' => $postId, 'user_id' => $userId));
    }

}
