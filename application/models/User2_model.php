<?php

class User2_model extends Base_Model {
	## EDIT HERE ##
    public $table = 'users';
    public $id = 'iduser';
	public $sort = 'nama asc';
	public $filter = '';

    function User2_model() {
        parent::__construct();
    }
	
	function getRoles($iduser) {
		$sql = "select ur.*, r.nama as namarole, j.nama as namajabatan, u.nama as namaunit
				from userrole ur 
				join role r on ur.idrole=r.idrole
				join jabatan j on ur.idjabatan=j.idjabatan
				join unitkerja u on j.idunit=u.idunit
				and ur.iduser=$iduser order by r.nama ";
		$rows = dbGetRows($sql);
		return $rows;
	}
	
	function addRole($record) {
        $ret = dbInsert('userrole', $record);
        if (!$ret)
            return false;
        
		return true;
	}

    function deleteRole($iduser, $idrole, $idjabatan) {
		$sql = "delete from userrole where iduser=$iduser and idrole=$idrole and idjabatan=$idjabatan";
        $ret = dbQuery($sql);
        if (!$ret)
            return false;
		return true;
    }
	
	function getListUserJabatan($limit, $offset=0) {
		$sql = "select u1.id as iduser, u1.name as nama, u1.email,j.idjabatan, j.nama as namajabatan ".
		$sql .= "from users u1 left join userrole u2 on u1.id=u2.iduser ";
		$sql .= "left join jabatan j on u2.idjabatan=j.idjabatan  ";
		$sql .= $this->filter;
		$sql .= " order by $this->sort ";
		$sql .= " limit $limit offset $offset ";
		return dbGetRows($sql);
	}

	function getCountListUserJabatan() {
		$sql = "select count(*) ".
		$sql .= "from users u1 left join userrole u2 on u1.id=u2.iduser ";
		$sql .= "left join jabatan j on u2.idjabatan=j.idjabatan ";
		$sql .= $this->filter;

        return dbGetOne($sql);
	}
	
	function getJabatan($idjabatan) {
		$sql = "select j.idunit, j.nama as namajabatan, k.nama as namaunit
			from jabatan j, unitkerja k where idjabatan=$idjabatan and j.idunit=k.idunit";
		return dbGetRow($sql);
	}
    
}
