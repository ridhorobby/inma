<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Harga_model extends AppModel {

    protected $_filter = "";
    protected $_order = "";
    protected $_table = "harga";
    protected $_view = "harga_view";
    protected $_column = "*";
    protected $_join = "";


    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function create($data, $user_id, $skip_validation = TRUE, $return = TRUE) {
        $create = parent::create($data, true, true);
        
        if ($create) {
            return $create;
        }else{
            return false;
        }
    }

    public function updateHarga($data){
        $dataUpdate = array(
            'nama'                  => $data['nama'],
            'kategori'              => $data['kategori'],
            'harga_pokok_granit'    => $data['harga_pokok_granit'],
            'harga_pokok_keramik'   => $data['harga_pokok_keramik'],
            'harga_jual_granit'     => $data['harga_jual_granit'],
            'harga_jual_keramik'    => $data['harga_jual_keramik'],
            'note_granit'           => $data['note_granit'],
            'note_keramik'          => $data['note_keramik'],
            'updated_at'           => date('Y-m-d G:i:s', time())
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }

    public function getHarga($page=0){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "select $this->_column from $this->_view p $this->_join $this->_filter ";
        return dbGetRows($sql);
    }

    public function getHargaById($id){
        return dbGetRow("select * from harga where id = $id");
    }

    public function deleteHarga($id) {
        $dataUpdate = array(
            'is_deleted' => "1"
        );
        $save = parent::save($id, $dataUpdate, true);
        return $save;
    }


}
