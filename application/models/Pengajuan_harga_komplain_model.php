<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class pengajuan_harga_komplain_model extends AppModel {

    protected $_filter = "";
    protected $_order = "";
    protected $_table = "pengajuan_harga_komplain";
    // protected $_view = "harga_view";
    protected $_column = "*";
    protected $_join = "";
    
    // Penjelasan Status
    // 1 : DRAFT                : User masih bebas update jumlah 
    // 2 : MENUNGGU APPROVAL    : Menunggu approval dari direktur
    // 3 : REVISI               : Pengajuan ditolak oleh direktur dan harus direvisi
    // 4 : DISETUJUI            : Pengajuan disetujui oleh direktur
    // 5 : REVISI HPP           : Revisi Hpp oleh Manager Produksi dan menunggu approval direktur


    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function create($data, $user_id, $skip_validation = TRUE, $return = TRUE) {
        $dataInsert = array(
            'tgl_pengajuan'     => $data['tgl_pengajuan'],
            // 'tgl_disetujui'     => $data['tgl_disetujui'],
            'id_konsumen'       => $data['id_konsumen'],
            'nama_konsumen'     => $data['nama_konsumen'],
            'alamat_konsumen'   => $data['alamat_konsumen'],
            'no_hp_konsumen'   => $data['no_hp_konsumen'],
            // 'hrg_setuju'        => $data['hrg_setuju'],
            // 'uang_muka'         => $data['uang_muka'],
            // 'tanda_jadi'        => $data['tanda_jadi'],
            'status'            => '1',
            'jenis'             => '0',
            // 'pelunasan'         => $data['pelunasan'],
            'keterangan'        => $data['keterangan'],
            // 'is_deleted'        => $data['is_deleted'],
            'user_id_pengaju'   => $user_id,
            'created_by'        => $user_id,
            // 'user_id_approve'   => $data['user_id_approve'],
            // 'created_at'        => date('Y-m-d G:i:s', time()),
            // 'updated_at'        => date('Y-m-d G:i:s', time())
        );
        // echo "<pre>";print_r($dataInsert);die();
        // dbInsert($this->_table, $dataInsert);die();
        $create = parent::create($dataInsert, true, true);
        if ($create) {
            return $create;
        }else{
            return false;
        }
    }

    public function updateHarga($data){
        $dataUpdate = array(
            'tgl_pengajuan'     => $data['tgl_pengajuan'],
            'hrg_setuju'        => $data['hrg_setuju'],
            'DP'                => $data['DP'],
            'tanda_jadi'        => $data['tanda_jadi'],
            'status'            => $data['status'],
            'pelunasan'         => $data['pelunasan'],
            'keterangan'        => $data['keterangan'],
            'updated_at'        => date('Y-m-d G:i:s', time()),
            'updated_by'        => $data['user_id']
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }

    public function getPengajuan($page=0, $status="0", $role="", $bulan="", $tahun="", $keyword=""){
        if ($status != "0") {
            $filter = " AND status = '".$status."'";
            
            if ($status == "4") {
                $filter = $filter . " AND pengajuan_harga_komplain.id_konsumen <> '0'";
            }
        } else {
            if($role == Role::PRODUKSI){
                $filter = " AND status in(4,5) ";
            }elseif($role == Role::MARKETING || $role == Role::DESAIN){
                $filter = " AND status != '5' ";
            }else{
                $filter = "";
            }
            
        }

        if($keyword!=""){
            $key = strtolower($keyword);
            $filter .= " and (lower(pengajuan_harga_komplain.nama_konsumen) like '%$key%' or lower(konsumen.nama) like '%$key%') ";
        }
        
        if ($bulan != "") {
            $filter = $filter . " AND MONTH(pengajuan_harga_komplain.updated_at) = '".$bulan."' AND YEAR(pengajuan_harga_komplain.updated_at) = '".$tahun."'";
        }
        
        $sql = "SELECT pengajuan_harga_komplain.*, konsumen.nama, konsumen.alamat, konsumen.no_hp, pesanan.kode_order, u1.name AS 'user_pengaju', u2.name AS 'user_approve'
                FROM pengajuan_harga_komplain
                LEFT JOIN konsumen ON pengajuan_harga_komplain.id_konsumen = konsumen.id
                LEFT JOIN pesanan ON konsumen.id = pesanan.konsumen_id
                LEFT JOIN users u1 ON pengajuan_harga_komplain.user_id_pengaju = u1.id
                LEFT JOIN users u2 ON pengajuan_harga_komplain.user_id_approve = u2.id
                WHERE 1 AND pengajuan_harga_komplain.is_deleted = '0' ".$filter."
                ORDER BY tgl_pengajuan DESC";
                
                
        // echo $sql;
        // die();
                
        return dbGetRows($sql);
    }
    
    public function getOnePengajuan($page=0, $id, $status="0"){
        if ($status != "0") {
            $filter = " AND status = '".$status."'";
        } else {
            $filter = "";
        }
        
        $sql = "SELECT pengajuan_harga_komplain.*, konsumen.nama, konsumen.alamat, konsumen.no_hp, pesanan.kode_order, u1.name AS 'user_pengaju', u2.name AS 'user_approve'
                FROM pengajuan_harga_komplain
                LEFT JOIN konsumen ON pengajuan_harga_komplain.id_konsumen = konsumen.id
                LEFT JOIN pesanan ON konsumen.id = pesanan.konsumen_id
                LEFT JOIN users u1 ON pengajuan_harga_komplain.user_id_pengaju = u1.id
                LEFT JOIN users u2 ON pengajuan_harga_komplain.user_id_approve = u2.id
                WHERE 1 AND pengajuan_harga_komplain.is_deleted = '0' AND pengajuan_harga_komplain.id = '".$id."'
                ORDER BY tgl_pengajuan DESC";
                
                
        // echo $sql;
        // die();
                
        return dbGetRow($sql);
    }

    public function getDetailPengajuan($id) {
        $sql = "SELECT dp.*, harga_maintenance.nama
                FROM detail_pengajuan_komplain dp
                LEFT JOIN harga_maintenance ON dp.id_harga_maintenance = harga_maintenance.id
                WHERE 1 AND dp.is_deleted = '0' AND dp.id_pengajuan_harga_komplain = ".$id;
        return dbGetRows($sql);
    }

    public function ajukanHarga($data){
        $dataUpdate = array(
            'status'                => '2',
            'updated_at'            => date('Y-m-d G:i:s', time())
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }

    public function approveHarga($data){
        $dataUpdate = array(
            'harga_setuju'                  => $data['harga_setuju'],
            // 'tanda_jadi'            => $data['tanda_jadi'],
            'user_id_approve'               => $data['user_id'],
            'status'                        => '4',
            'updated_at'                    => date('Y-m-d G:i:s', time()),
            'tgl_disetujui'                 => date('Y-m-d', time())
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }
public function tolakPengajuanHarga($data){
        $dataUpdate = array(
            'status'                => '3',
            'updated_at'            => date('Y-m-d G:i:s', time())
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }

}
?>