<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->model('Post_model');

class Knowledge_management_model extends Post_model {

    protected $_table = 'posts';

    protected function join_or_where($row) {
        $this->load->model('Category_model');
        $this->_database->select("posts.*, tr.seen");
        $this->_database->join("users", "users.id = posts.user_id");
        $this->_database->join("posts p2", "p2.parent_id = posts.id", "LEFT");
        $this->_database->join('post_tags', 'post_tags.post_id=posts.id', 'LEFT');
        $this->_database->join('tags', 'post_tags.tag_id=tags.id', 'LEFT');
        $this->_database->join('(select ds.post_id, count(ds.*) as seen from (select distinct user_id, post_id from trackers) ds group by ds.post_id) as tr', 'tr.post_id=posts.id', 'LEFT');
        $this->_database->where('posts.category_id', Category_model::KNOWLEDGE_MANAGEMENT);
        $this->_database->where('p2.id IS NULL');
        $this->_database->group_by('posts.id,tr.seen');
        return $row;
    }

    public function getAll($page = 0, $param = null) {
        $condition = array();
        if (!empty($param)) {
            $condition[] = "(users.name ilike '%" . $param . "%' or posts.description ilike '%" . $param . "%' or tags.name ilike '%" . $param . "%')";
        }
        $this->limit(10, ($page * 10));
        return $this->order_by("posts.id", 'DESC')->with(array('Category', 'User', 'Comment', 'Post_tag', 'Updated_by'))->get_many_by($condition);
    }

    public function getDetail($id) {
        return $this->order_by("posts.id", 'DESC')->with(array('Category', 'User', 'Comment' => 'User', 'Post_tag', 'Updated_by'))->get_by(array('posts.id' => $id));
    }

    public function getByTag($page = 0, $param = null) {
        $condition = array();
//        $this->_database->join("post_tags", "post_tags.post_id = posts.id");
//        $this->_database->join("tags", "tags.id = post_tags.tag_id");
        if (!empty($param)) {
            $condition['tags.name'] = $param;
        }
        $this->limit(10, ($page * 10));
        $this->_database->group_by('posts.id');
        return $this->order_by("posts.id", 'DESC')->with(array('Category', 'User', 'Comment', 'Post_tag', 'Updated_by'))->get_many_by($condition);
    }

    public function update($id, $data, $skip_validation = FALSE, $return = TRUE) {
        if (empty($data['description']) && empty($data['link']) && empty($data['image']) && empty($data['video']) && empty($data['file'])) {
            return 'Data yang dikirim tidak lengkap.';
        }
        $oldData = $this->with('Comment', 'Post_tag')->get_by('posts.id', $id);

        if (empty($oldData))
            return false;

        $this->_database->trans_begin();
        $data['type'] = self::TYPE_PUBLIC;
        $data['parentId'] = $oldData['id'];
        $data['userId'] = $oldData['userId'];
        $data['created_at'] = $oldData['createdAt'];
        $h = array();
        if (!empty($oldData['parentHierarchy']))
            $h[] = (int) $oldData['parentHierarchy'];

        $h[] = (int) $oldData['id'];
        $data['parentHierarchy'] = implode('/', $h);

        if (!empty($oldData['image'])) {
            $data['image'] = Image::getName($oldData['image'][strtolower(Image::IMAGE_ORIGINAL)]['name']);
        }
        if (!empty($oldData['video'])) {
            $upload = $oldData['video']['name'];
        }
        if (!empty($oldData['file'])) {
            $upload = $oldData['file']['name'];
        }
        $create = $this->insert($this->setFieldDB($data), $skip_validation, $return);
        if ($create) {
            if (isset($data['postTags'])) {
                $toTag = explode(',', $data['postTags']);
                $this->load->model('Tag_model');
                $this->load->model('Post_tag_model');
                $tags = $this->Tag_model->get_many_by(array('name' => $toTag));
                $tagNames = Util::toList($tags, 'name');
                foreach ($toTag as $tag) {
                    if (!in_array($tag, $tagNames)) {
                        $insert = $this->Tag_model->insert(array('name' => $tag), TRUE, FALSE);
                        if (!$insert) {
                            $this->_database->trans_rollback();
                            return FALSE;
                        }
                    }
                }
                $tags = $this->Tag_model->get_many_by(array('name' => $toTag));
                foreach ($tags as $tag) {
                    $dataToTag = array(
                        'post_id' => $create,
                        'tag_id' => $tag['id']
                    );
                    $insert = $this->Post_tag_model->insert($dataToTag, TRUE, FALSE);
                    if (!$insert) {
                        $this->_database->trans_rollback();
                        return FALSE;
                    }
                }
            }

            $this->load->model('Comment_model');
            foreach ($oldData['comments'] as $comment) {
                $copyComment = array(
                    'post_id' => $create,
                    'user_id' => $comment['userId'],
                    'text' => $comment['text'],
                    'created_at' => $comment['createdAt'],
                    'updated_at' => $comment['updatedAt'],
                    'candelete' => $comment['candelete']);
                $insert = $this->_database->insert('comments', $copyComment);
                if (!$insert) {
                    $this->_database->trans_rollback();
                    return FALSE;
                }
            }

            $upload = TRUE;
            if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                $upload = Image::upload('image', $create, $_FILES['image']['name'], 'posts/photos');
            } else if (!empty($oldData['image'])) {
                $name = Image::getName($oldData['image'][strtolower(Image::IMAGE_ORIGINAL)]['name']);
                $link = Image::getLocation($oldData['id'], Image::IMAGE_ORIGINAL, $name, 'posts/photos');
                $upload = Image::copy($link, $create, $name, 'posts/photos');
            }
            if (isset($_FILES['video']) && !empty($_FILES['video'])) {
                $upload = File::upload('video', $create, 'posts/videos');
            } else if (!empty($oldData['video'])) {
                $upload = File::copy(File::getLocation($oldData['id'], 'posts/videos'), $create, 'posts/videos');
            }
            if (isset($_FILES['file']) && !empty($_FILES['file'])) {
                $upload = File::upload('file', $create, 'posts/files');
            } else if (!empty($oldData['file'])) {
                $upload = File::upload(File::getLocation($oldData['id'], 'posts/files'), $create, 'posts/files');
            }
            if ($upload === TRUE) {
//                 $this->load->model('Notification_model');
//                 $this->Notification_model->generate(Notification_model::ACTION_POST_CREATE, $create);
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

}
