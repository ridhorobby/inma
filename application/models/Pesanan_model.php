<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan_model extends AppModel {

    public $limit = 10;

    // FLOW
    const FLOW_ORDERMASUK = 1;
    const FLOW_PENGAJUANKUSEN = 2;

    const PHOTO_ORDER_MASUK = 'OM';
    const PHOTO_PASANG_FINISH = 'PF';

    const POST_PESANAN = 'PPP';
    const UPDATE_PESANAN = 'UPP';
    
    const USULAN_JADWAL_KUSEN = 'UPK';
    const REJECT_REVISI_KUSEN = 'RRK';
    const CONFIRM_REVISI_KUSEN = 'CRK';
    const REJECT_JADWAL_KUSEN = 'RJK';
    const CONFIRM_JADWAL_KUSEN = 'CJK';
    const RESCHEDULE_JADWAL_KUSEN = 'SJK';
    const DONE_JADWAL_KUSEN = 'DJK';

    const AJUKAN_JADWAL_FINISH = 'AJF';
    const REJECT_REVISI_FINISH = 'RRF';
    const ACTION_CONFIRM_REVISI_FINISH = 'CRF';
    const ACTION_REJECT_JADWAL_FINISH = 'RJF';
    const ACTION_CONFIRM_JADWAL_FINISH = 'CJF';
    const ACTION_RESCHEDULE_JADWAL_FINISH = 'SJF';
    const ACTION_DONE_JADWAL_FINISH = 'DJF';
    const ACTION_REVISI_GAMBAR_KERJA = 'RGK';
    //STATUS
    const STATUS_PROSES = 'P';
    const STATUS_SELESAI = 'S';
    const STATUS_ARSIP = 'A';

    public $limit_quota = 3;   
    protected $_filter = "";
    protected $_order = "";
    protected $_table = "pesanan";
    protected $_view = "pesanan_view";
    protected $_column = "*";
    protected $_join = "";

    protected $has_many = array(
        'Comment',
    );
    protected $belongs_to = array(
        'User',
        'Group',
        'Category',
        'Updated_by' => array('primary_key' => 'userIdUpdated', 'model' => 'User_model')
    );
    protected $many_to_many = array(
        'Post_tag' => array('Post', 'Tag')
    );
    protected $label = array(
        'category_id' => 'Kategori',
        'image' => 'Gambar',
        'file' => 'Berkas',
        'video' => 'Video'
    );
    protected $validation = array(
        'link' => 'valid_url',
        'category_id' => 'required|integer'
    );

    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function resetVariables(){
        $this->_filter = '';
        $this->column = '*';
        $this->_join = '';
        $this->_show_sql = false;
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('Category_model');
    }


    function getCount() {
        $sql = "select count(*) from $this->table {$this->filter}";
        return dbGetOne($sql);
    }

    public function create($data, $user_id, $skip_validation = TRUE, $return = TRUE) {
        $dataInsert = array(
            'kode_order'        => $data['kode_order'],
            'tgl_order_masuk'   => $data['tgl_order_masuk'],
            // 'tgl_jatuh_tempo'   => date('Y-m-d G:i:s', strtotime('+30 days', strtotime($data['tgl_order_masuk']))),
            'flow_id'           => self::FLOW_PENGAJUANKUSEN,
            'status_order'      => self::STATUS_PROSES,
            'user_id'           => $user_id,
            'konsumen_id'       => $data['konsumen_id'],
            'created_at'        => date('Y-m-d G:i:s', time()),
            'updated_at'        => date('Y-m-d G:i:s', time()),
            'tipe'              => $data['tipe'],
            'status_produksi'   => $data['status_produksi'],
            'catatan'           => ($data['catatan']) ? $data['catatan'] : null,
            'desainer_design'          => ($data['desainer_design']) ? $data['desainer_design'] : null,
            'desainer_eksekusi'        => ($data['desainer_eksekusi']) ? $data['desainer_eksekusi'] : null
        );
        //echo "<pre>";print_r($dataInsert);die();
        // $this->_database->trans_begin();
        // $create = db
        //$create = dbInsert($this->_table, $dataInsert);
        $create = parent::create($dataInsert, true, true);
        if ($create) {
            return $create;

        }else{
            return false;
        }
    }

    public function edit($data, $pesanan_id, $skip_validation = TRUE) {
        $dataUpdate = $data;
        $dataUpdate['updated_at'] =date('Y-m-d G:i:s', time());
        // echo $pesanan_id."<br>";
        // echo "<pre>";print_r($dataUpdate);die();
        // $save =dbUpdate($this->_table, $dataUpdate, "id=$pesanan_id");
        // // die();
        // return true;
        $save = parent::save($pesanan_id,$dataUpdate, false);
        return $save;
        // echo "ddd";die();
        // if ($save) {
        //     return $save;

        // }else{
        //     return false;
        // }
    }

    public function setSelesai($pesanan_id){
        $dataUpdate = array(
            'flow_id'       => 8,
            'status_order'  => self::STATUS_SELESAI,
            'updated_at'=> date('Y-m-d G:i:s', time())
        );
        // $save = dbUpdate($this->_table, $dataUpdate, "id=$pesanan_id");
        $save = parent::save($pesanan_id, $dataUpdate, true);
        return $save;
    }

    public function getPesanans($page=0){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "
                select $this->_column from $this->_view p $this->_join $this->_filter ";
        
        if(!$page == 'unl'){
            $sql .=" and is_deleted=0 limit 30 offset $page";
        }
        else{
            $sql .=" and is_deleted=0";
        }
        if($this->_show_sql){
            die($sql);
        }
        // die($sql);
        return dbGetRows($sql);
    }

    public function getKalendar($page=0){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "
            select $this->_column from (
                select id, kode_order, rencana_kerja_id,user_id, rencana_kerja_nama, nama_konsumen, alamat_konsumen, no_hp_konsumen, 'pesanan' as \"type\", tgl_order_masuk as \"tgl_masuk\", tgl_jatuh_tempo, tanggal_pasang_kusen, jam_pasang_kusen, jatuh_tempo_kusen, tanggal_pasang_finish, jam_pasang_finish,jatuh_tempo_finish,mitra_pemasang_kusen, nama_pemasang_kusen, mitra_pemasang_finish, nama_pemasang_finish, tgl_acc_konsumen,flow_id, 'P' AS tipe, link_sosmed, foto_after, edt_katalog
                    from pesanan_view where is_deleted=0 AND kode_order NOT IN (SELECT kode_order FROM pesanan_view WHERE flow_id = '8' AND tgl_acc_konsumen IS NULL)
                UNION
                select id, kode_order, rencana_kerja_id,user_id, rencana_kerja_nama, nama_konsumen, alamat_konsumen, no_hp_konsumen, 'komplain' as \"type\", tgl_komplain as \"tgl_masuk\", tgl_jatuh_tempo, tanggal_pasang_kusen, jam_pasang_kusen, null as jatuh_tempo_kusen, tanggal_pasang_finish, jam_pasang_finish, null as jatuh_tempo_finish, mitra_pemasang_kusen , nama_pemasang_kusen, mitra_pemasang_finish, nama_pemasang_finish, tgl_acc_konsumen,flow_id, 'K' AS tipe, '' as link_sosmed ,'0' AS foto_after,'0' AS edt_katalog
                    from komplain_view where is_deleted=0
                ) p $this->_filter
         

        ";
        if($page == 'unl'){
            $sql .= " order by 
                p.tipe DESC, p.flow_id";
        }
        elseif($page=='count'){
            $sql .= " order by 
                p.tipe DESC, p.flow_id";
            return dbGetCount($sql);
        }
        else{
            
            $offset = $page * $this->limit;
            $sql .=" order by 
                p.tgl_masuk desc, p.tipe DESC, p.flow_id limit $this->limit offset $offset";
        }
        // die($sql);
        return dbGetRows($sql);
    }

    public function getDetailPesanan($pesanan_id){
        $sql = "
                select p.* , u.jadwal_usulan as jadwal_usulan, us.name as nama_pengusul, us.role as role_pengusul 
                from $this->_view p
                left join usulan u on p.id = u.pesanan_id and u.status='P'
                left join users us on u.user_id = us.id
                where p.id = $pesanan_id
            ";
        return dbGetRow($sql);
    }

    public function getDetailPesananForKonsumen($kode_order, $konsumen_id, $status='P'){
        $sql = "
                select p.* , u.jadwal_usulan as jadwal_usulan,jam_usulan as jam_usulan, us.name as nama_pengusul, us.role as role_pengusul
                from $this->_view p
                left join usulan u on p.id = u.pesanan_id and u.status='".$status."'
                left join users us on u.user_id = us.id
                where p.kode_order = '$kode_order' and p.konsumen_id=$konsumen_id
            ";
        return dbGetRow($sql);
    }

    public function getPesananBaru($page=0){
        $sql = "
                select p.* from $this->_view p 
                left join usulan u on p.id = u.pesanan_id
                where u.id is null $this->_filter
                and p.is_deleted = 0
                and p.flow_id <> '8'
                limit 10 offset $page
            ";
            // die($sql);
        return dbGetRows($sql);
    }
    
    // NOTE : FITUR UNTUK MENCARI PESANAN YG BELUM 'PASANG FINISH' TETAPI SUDAH MENDEKATI JATUH TEMPO
    public function getPesananJatuhTempo(){
        $sql = "
                select id, kode_order, rencana_kerja_nama, nama_konsumen, alamat_konsumen, no_hp_konsumen, tgl_order_masuk as tgl_masuk, tgl_jatuh_tempo, 
                        tanggal_pasang_kusen, tanggal_pasang_finish,tgl_acc_konsumen,flow_id, DATEDIFF(tgl_jatuh_tempo, CURRENT_DATE) as sisa_hari
                from pesanan_view where is_deleted=0 and DATEDIFF(tgl_jatuh_tempo, CURRENT_DATE) < 4 AND DATEDIFF(tgl_jatuh_tempo, CURRENT_DATE) >= 0 AND flow_id < 7
            ";
        return dbGetRows($sql);
    }


    public function getFlow(){
        $sql = "select * from pesanan_flow";
        return dbGetRows($sql);
    }
    
    public function updateKodeOrder($pesanan_id, $kode_order){
        $dataUpdate = array(
            'kode_order'   => $kode_order,
            'updated_at'=> date('Y-m-d G:i:s', time())
        );
        $save = parent::save($pesanan_id, $dataUpdate, true);
        return $save;
    }

    public function addFlow($pesanan_id, $flow_id, $add=1){
        $new_flow = $flow_id+$add;
        $dataUpdate = array(
            'flow_id'   => $new_flow,
            'updated_at'=> date('Y-m-d G:i:s', time())
        );
        $save = parent::save($pesanan_id, $dataUpdate, true);
        return $save;
    }

    public function minFlow($pesanan_id, $flow_id, $min=1){
        $new_flow = $flow_id-$min;
        $dataUpdate = array(
            'flow_id'   => $new_flow,
            'updated_at'=> date('Y-m-d G:i:s', time())
        );
        $save = parent::save($pesanan_id, $dataUpdate, true);
        return $save;
    }

    public function setMitraPemasang($id_mitra_pemasang, $renker_id, $pesanan_id){
        if($renker_id == 1){
            $data['mitra_pemasang_kusen'] = $id_mitra_pemasang;
        }else{
            $data['mitra_pemasang_finish'] = $id_mitra_pemasang;
        }
        $data['updated_at'] = date('Y-m-d G:i:s', time());
        $save = parent::save($pesanan_id, $data, true);
        return $save;

    }

    public function setTglAcc($pesanan_id){
        $data['tgl_acc_konsumen'] = date('Y-m-d G:i:s', time());
        $data['status_order'] = self::STATUS_SELESAI;
        $save = parent::save($pesanan_id, $data, true);
        return $save;
    }

    public function setJatuhTempo($pesanan_id){
        $tgl = date('Y-m-d G:i:s', time());
        $data['tgl_jatuh_tempo'] = date('Y-m-d G:i:s', strtotime('+30 days', strtotime($tgl)));
        $save = parent::save($pesanan_id, $data, true);
        return $save;
        
    }

    function insertPesananImage($pesanan_id, $nama_file, $file_ori, $tipe='OM', $user_id) {
        $data = array(
            'pesanan_id'    => $pesanan_id,
            'tipe'          => $tipe,
            'nama_file'     => $nama_file,
            'user_id'       => $user_id,
            'created_at'    =>  date('Y-m-d G:i:s', time()),
            'updated_at'    =>  date('Y-m-d G:i:s', time())

        );
        $update = dbInsert('pesanan_files',$data);
        if($update){
            return true;
        }else{
            return false;
        }

        
    }

    public function getPhoto($pesanan_id){
        $sql = "select * from pesanan_files where pesanan_id=".$pesanan_id;
        return dbGetRows($sql);
    }
    
    // Mendapatkan total pesanan seluruhnya, total pesanan selesai dan total pesanan yang selesai tepat waktu (sebelum jatuh tempo)
    public function getKPIPesanan() {
        $sql = "SELECT
                    COUNT(p.id) AS 'total_pesanan',
                    (
                    SELECT
                        COUNT(p1.id)
                    FROM
                        pesanan p1
                    WHERE
                        p1.flow_id = '8' AND p1.tipe = 'P'
                	) AS 'pesanan_selesai',
                    (
                    SELECT
                        COUNT(p1.id)
                    FROM
                        pesanan p1
                    WHERE
                        p1.flow_id = '8' AND p1.tipe = 'P' AND p1.tgl_acc_konsumen < p1.tgl_jatuh_tempo
                	) AS 'pesanan_selesai_tepat'
                FROM
                    pesanan p
                WHERE
                    p.is_deleted = '0' AND p.tipe = 'P'";
                    
        return dbGetRow($sql); 
    }
    
    public function getKPIBulanan($tahun) {
        $sql = "SELECT
                	MONTH(p.tgl_order_masuk) AS 'bulan',
                    COUNT(p.id) AS 'total_pesanan',
                    (
                    SELECT
                        COUNT(p1.id)
                    FROM
                        pesanan p1
                    WHERE
                        p1.flow_id = '8' AND p1.tipe = 'P'  AND p1.is_deleted = '0' AND MONTH(p1.tgl_order_masuk) = MONTH(p.tgl_order_masuk)
                ) AS 'pesanan_selesai',
                (
                    SELECT
                        COUNT(p1.id)
                    FROM
                        pesanan p1
                    WHERE
                        p1.flow_id = '8' AND p1.tipe = 'P' AND p1.tgl_acc_konsumen < p1.tgl_jatuh_tempo AND p1.is_deleted = '0' AND MONTH(p1.tgl_order_masuk) = MONTH(p.tgl_order_masuk)
                ) AS 'pesanan_selesai_tepat'
                FROM
                    pesanan p
                WHERE
                    p.is_deleted = '0' AND p.tipe = 'P' AND YEAR(p.tgl_order_masuk) = '".$tahun."'
                GROUP BY MONTH(p.tgl_order_masuk)";

        return dbGetRows($sql); 
    }
    
    // ADDITIONAL FEATURE : 30 JUNI 2020
    public function deletePesananImage($pesanan_id, $nama_file) {
        $sql = "delete from pesanan_files where pesanan_id=".$pesanan_id." AND nama_file = '".$nama_file."'";
        return $this->db->query($sql);
    }
    
    // ADDITIONAL FEATURE : 22 NOVEMBER 2020
    public function updateStatusProduksi($data){
        $dataUpdate = array(
            'status_produksi'       => $data['status_produksi'],
            'updated_at'            => date('Y-m-d G:i:s', time())
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }
    
    // ADDITIONAL FEATURE : 9 January 2021
    public function updateLinkPesanan($pesanan_id, $link){
        $dataUpdate = array(
            'link'       => $link,
            'updated_at' => date('Y-m-d G:i:s', time())
        );
        $save = parent::save($pesanan_id, $dataUpdate, true);
        return $save;
    }
    
    public function getGambarAfter($pesanan_id) {
        $sql = "SELECT * FROM pesanan_files WHERE tipe IN ('FA', 'EK') and pesanan_id = '".$pesanan_id."'";
        return dbGetRows($sql); 
    }
    
    // KEPERLUAN DEV
    // KEPERLUAN DEV
    // KEPERLUAN DEV
    // KEPERLUAN DEV
    // KEPERLUAN DEV
    // KEPERLUAN DEV
    // KEPERLUAN DEV
    public function getKalendarDev($page=0){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "
            select $this->_column from (
                select id, kode_order, rencana_kerja_nama, nama_konsumen, alamat_konsumen, no_hp_konsumen, 'pesanan' as \"type\", tgl_order_masuk as \"tgl_masuk\", tgl_jatuh_tempo, tanggal_pasang_kusen, tanggal_pasang_finish,tgl_acc_konsumen,flow_id, 'P' AS tipe
                    from pesanan_view where is_deleted=0 AND kode_order NOT IN (SELECT kode_order FROM pesanan_view WHERE flow_id = '8' AND tgl_acc_konsumen IS NULL)
                UNION
                select id, kode_order, rencana_kerja_nama, nama_konsumen, alamat_konsumen, no_hp_konsumen, 'komplain' as \"type\", tgl_komplain as \"tgl_masuk\", tgl_jatuh_tempo, tanggal_pasang_kusen, tanggal_pasang_finish,tgl_acc_konsumen,flow_id, 'K' AS tipe
                    from komplain_view where is_deleted=0
                ) p
            $this->_filter
            order by 
                p.tipe DESC, p.flow_id
        ";
        // $sql = "
        //     select $this->_column from (
        //         select id, kode_order, rencana_kerja_nama, nama_konsumen, alamat_konsumen, no_hp_konsumen, 'pesanan' as \"type\", tgl_order_masuk as \"tgl_masuk\", tgl_jatuh_tempo, tanggal_pasang_kusen, tanggal_pasang_finish,tgl_acc_konsumen,flow_id, tipe
        //             from pesanan_view where is_deleted=0 AND tipe = 'P'
        //         ) p
        //     $this->_filter
        //     order by p.tgl_masuk desc
        // ";
        if(!$page == 'unl'){
            $sql .=" limit 20 offset $page";
        }
        // die($sql);
        return dbGetRows($sql);
    }
    
    public function getPesananBaruDev($page=0){
        $sql = "
                select p.* from $this->_view p 
                left join usulan u on p.id = u.pesanan_id
                where u.id is null $this->_filter
                and p.is_deleted = 1
                and p.flow_id <> '8'
                limit 10 offset $page
            ";
            // die($sql);
        return dbGetRows($sql);
    }
    
    public function createDev($data, $user_id, $skip_validation = TRUE, $return = TRUE) {
        $dataInsert = array(
            'kode_order'        => $data['kode_order'],
            'tgl_order_masuk'   => $data['tgl_order_masuk'],
            'tgl_jatuh_tempo'   => date('Y-m-d G:i:s', strtotime('+30 days', strtotime($data['tgl_order_masuk']))),
            'flow_id'           => self::FLOW_PENGAJUANKUSEN,
            'status_order'      => self::STATUS_PROSES,
            'user_id'           => $user_id,
            'konsumen_id'       => $data['konsumen_id'],
            'created_at'        => date('Y-m-d G:i:s', time()),
            'updated_at'        => date('Y-m-d G:i:s', time()),
            'tipe'              => $data['tipe'],
            'status_produksi'   => $data['status_produksi']
        );
        // echo "<pre>";print_r($dataInsert);die();
        // $this->_database->trans_begin();
        // $create = db
        // $create = dbInsert($this->_table, $dataInsert);
        $create = parent::create($dataInsert, true, true);
        if ($create) {
            return $create;

        }else{
            return false;
        }
    }

    // function konfirmasiPesananKonsumen($confirm,$kode_order,$pesanan_id){
    //     $usulan_id = dbGetOne("select id from $this->_table where pesanan_id=".$pesanan_id." and status='".self::STATUS_PENGAJUAN."'");

    //     $dataUpdate = array(
    //         'status'        => self::STATUS_TERIMA,
    //         'approver_id'   => 0,
    //         'updated_at'    => date('Y-m-d G:i:s', time())
    //     );
    //     $updatePengajuan = parent::save($usulan_id, $dataUpdate, true);
    //     if($updatePengajuan){
    //         return true;
    //     }
    // }

    public function getIdPesananFromIdKonsumen($konsumen_id) {
        $sql = "select id from pesanan where konsumen_id = '".$konsumen_id."'";
        return dbGetRow($sql);
    }

    // public function getOrderDesainer($desainer_id,$bulan="", $tahun="", $keyword=""){
    //     $filter = "";        

    //     if ($bulan != "") {
    //         $filter = $filter . " AND MONTH(p.tgl_order_masuk) = '".$bulan."' AND YEAR(p.tgl_order_masuk) = '".$tahun."'";
    //     }
        

    //     $sql = "
    //             select p.* from $this->_view p 
    //             left join usulan u on p.id = u.pesanan_id
    //             where u.id is null $this->_filter
    //             and p.is_deleted = 0
    //             and p.flow_id <> '8'
    //             and p.desainer = $desainer_id
    //             ".$filter."
                
    //         ";        
                
    //     // echo $sql;
    //     // die();
                
    //     return dbGetRows($sql);
    // }

    public function getOrderDesainer($desainer_id,$bulan="", $tahun="", $keyword=""){
        $filter = "";        

        if ($bulan != "") {
            $filter = $filter . " AND MONTH(p.tgl_order_masuk) = '".$bulan."' AND YEAR(p.tgl_order_masuk) = '".$tahun."'";
        }
        

        $sql = "
                select DISTINCT p.* from $this->_view p 
                where  
                p.is_deleted = 0
                $this->_filter
                and p.flow_id <> '8'
                and (p.desainer_design = $desainer_id or p.desainer_eksekusi=$desainer_id)
                ".$filter."
                
            ";        
                
        // echo $sql;
        // die();
                
        return dbGetRows($sql);
    }

    public function getDesainerAssignment($bulan, $tahun){
        $filter = "";        
        
        if ($bulan != "") {
            $filter = $filter . " AND MONTH(p.tgl_order_masuk) = '".$bulan."' AND YEAR(p.tgl_order_masuk) = '".$tahun."'";
        }

        $sql = "select * from $this->_view p
                where desainer is not null
                ".$filter."
                group by desainer";

        return dbGetRows($sql);
    }

    public function getFeeFromPesanan($bulan, $tahun){
        $filter = "";        

        if ($bulan != "") {
            $filter = $filter . " AND MONTH(p.tgl_order_masuk) = '".$bulan."' AND YEAR(p.tgl_order_masuk) = '".$tahun."'";
        }
        

        $sql = "
                select p.* from $this->_view p
                left join fee_desainer fd on p.id = fd.pesanan_id
                where  
                p.is_deleted = 0
                $this->_filter
                and p.flow_id <> '8'
                ".$filter."
                
            ";        
                
        // echo $sql;
        // die();
                
        return dbGetRows($sql);
    }

}