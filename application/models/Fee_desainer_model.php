<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Fee_desainer_model extends AppModel {

    protected $_filter = "";
    protected $_order = "";
    protected $_table = "fee_desainer";
    protected $_view = "fee_desainer";
    protected $_column = "*";
    protected $_join = "";
    
    // Penjelasan Status
    // 1 : DRAFT                : User masih bebas update jumlah 
    // 2 : MENUNGGU APPROVAL    : Menunggu approval dari direktur
    // 3 : REVISI               : Pengajuan ditolak oleh direktur dan harus direvisi
    // 4 : DISETUJUI            : Pengajuan disetujui oleh direktur
    // 5 : REVISI HPP           : Revisi Hpp oleh Manager Produksi dan menunggu approval direktur


    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function getFee($desainer_id, $pesanan_id){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "select $this->_column from $this->_view gp where desainer_id=$desainer_id and pesanan_id=$pesanan_id $this->_filter";
        return dbGetRows($sql);
    }

    public function getFeeRow($pesanan_id,$desainer_id=null){
        if(!$this->_column){
            $this->_column = '*';        
        }
        if($desainer_id!=null){
            $sql = "select $this->_column from $this->_view gp where (desainer_design_id=$desainer_id or desainer_eksekusi_id = $desainer_id) and pesanan_id=$pesanan_id $this->_filter";
        }else{
            $sql = "select $this->_column from $this->_view gp where pesanan_id=$pesanan_id $this->_filter";
        }
        
        return dbGetRow($sql);
    }

    public function create($data, $skip_validation = TRUE, $return = TRUE) {
        $dataInsert = array(
            'desainer_design_id'        => $data['desainer_design_id'],
            'desainer_eksekusi_id'        => $data['desainer_eksekusi_id'],
            'pesanan_id'   => $data['pesanan_id'],
            'fee_design'        => ($data['fee_design']) ? $data['fee_design'] : 0,
            'persentase_eksekusi'           => ($data['persentase_eksekusi']) ? $data['persentase_eksekusi'] : 0,
            'fee_eksekusi'      =>($data['fee_eksekusi']) ? $data['fee_eksekusi'] : 0,
            'created_at'        => date('Y-m-d G:i:s', time()),
            'updated_at'        => date('Y-m-d G:i:s', time())
        );
        $create = parent::create($dataInsert, true, true);
        if ($create) {
            return $create;

        }else{
            return false;
        }
    }

    public function edit($data, $fee_desainer_id,$skip_validation = TRUE) {
        $dataUpdate = $data; // fee_design,persentase_eksekusi, fee_eksekusi
        $dataUpdate['updated_at'] =date('Y-m-d G:i:s', time());
        // echo $fee_desainer_id;die();
        // echo "<pre>";print_r($dataUpdate);die();
        // echo $pesanan_id."<br>";
        // echo "<pre>";print_r($dataUpdate);die();
        // $save =dbUpdate($this->_table, $dataUpdate, "id=$pesanan_id");
        // // die();
        // return true;
        $save = parent::save($fee_desainer_id,$dataUpdate, false);
        return $save;

    }

    

}
?>