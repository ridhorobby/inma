<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Komplain_model extends AppModel {

    public $limit = 20;

    // FLOW
    const FLOW_ORDERMASUK = 1;
    const FLOW_PENGAJUANKUSEN = 2;

     const PHOTO_ORDER_MASUK = 'OM';
    const PHOTO_PASANG_FINISH = 'PF';
    
    const POST_KOMPLAIN = 'KPPP';
    const UPDATE_KOMPLAIN = 'KUPP';
    
    const USULAN_JADWAL_KUSEN = 'KUPK';
    const REJECT_REVISI_KUSEN = 'KRRK';
    const CONFIRM_REVISI_KUSEN = 'KCRK';
    const REJECT_JADWAL_KUSEN = 'KRJK';
    const CONFIRM_JADWAL_KUSEN = 'KCJK';
    const RESCHEDULE_JADWAL_KUSEN = 'KSJK';
    const DONE_JADWAL_KUSEN = 'KDJK';

    const AJUKAN_JADWAL_FINISH = 'KAJF';
    const REJECT_REVISI_FINISH = 'KRRF';
    const ACTION_CONFIRM_REVISI_FINISH = 'KCRF';
    const ACTION_REJECT_JADWAL_FINISH = 'KRJF';
    const ACTION_CONFIRM_JADWAL_FINISH = 'KCJF';
    const ACTION_RESCHEDULE_JADWAL_FINISH = 'KSJF';
    const ACTION_DONE_JADWAL_FINISH = 'KDJF';

    //STATUS
    const STATUS_PROSES = 'P';
    const STATUS_SELESAI = 'S';
    const STATUS_ARSIP = 'A';
   
    protected $_filter = "";
    protected $_order = "";
    protected $_table = "komplain";
    protected $_view = "komplain_view";
    protected $_column = "*";
    protected $_join = "";


    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function resetVariables(){
        $this->_filter = '';
        $this->column = '*';
        $this->_join = '';
        $this->_show_sql = false;
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('Category_model');
    }


    function getCount() {
        $sql = "select count(*) from $this->table {$this->filter}";
        return dbGetOne($sql);
    }

    public function create($data, $user_id, $skip_validation = TRUE, $return = TRUE) {
        
        $dataInsert = array(
            'tgl_komplain'      => $data['tgl_komplain'],
            'tgl_jatuh_tempo'   => date('Y-m-d G:i:s', strtotime('+30 days', strtotime($data['tgl_order_masuk']))),
            'description'       => $data['description'],
            'pesanan_id'        => $data['pesanan_id'],
            'user_id'           => $user_id,
            'status'            => self::STATUS_PROSES,
            'flow_id'           => self::FLOW_PENGAJUANKUSEN,
            'created_at'        => date('Y-m-d G:i:s', time()),
            'updated_at'        => date('Y-m-d G:i:s', time()),
            'catatan'           => ($data['catatan']) ? $data['catatan'] : null
        );
        
        // echo "<pre>";print_r($create);die();
        $create = parent::create($dataInsert, true, true);
        
        
        if ($create) {
            return $create;
        }else{
            return false;
        }
    }

    public function edit($data, $pesanan_id, $skip_validation = TRUE) {
        $dataUpdate = $data;
        $dataUpdate['updated_at'] =date('Y-m-d G:i:s', time());
        // echo $pesanan_id."<br>";
        // echo "<pre>";print_r($dataUpdate);die();
        // $save =dbUpdate($this->_table, $dataUpdate, "id=$pesanan_id");
        // // die();
        // return true;
        $save = parent::save($pesanan_id,$dataUpdate, false);
        return $save;
        // echo "ddd";die();
        // if ($save) {
        //     return $save;

        // }else{
        //     return false;
        // }
    }

    public function getKomplains($page=0){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "
                select $this->_column from $this->_view k $this->_join $this->_filter ";
        
        if(!$page == 'unl'){
            $sql .=" and k.is_deleted=0 limit 20 offset $page";
        }else{
            $sql .=" and k.is_deleted=0";
        }
        // die($sql);
        return dbGetRows($sql);
    }

    public function getDetailKomplain($komplain_id){
        $sql = "
                select k.* , u.jadwal_usulan as jadwal_usulan, us.name as nama_pengusul, us.role as role_pengusul 
                from $this->_view k
                left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                left join users us on u.user_id = us.id
                where k.id = $komplain_id
            ";
        return dbGetRow($sql);
    }

    

    public function getDetailKomplainByPesanan($pesanan_id, $status='P'){
        $sql = "
                select k.* , u.jadwal_usulan as jadwal_usulan,jam_usulan as jam_usulan, us.name as nama_pengusul, us.role as role_pengusul 
                from $this->_view k
                left join komplain_usulan u on k.id = u.komplain_id and u.status='".$status."'
                left join users us on u.user_id = us.id
                where k.pesanan_id = $pesanan_id
            ";
        return dbGetRow($sql);
    }

    public function getKomplainBaru($page=0){
        $sql = "
                select k.* from $this->_view k 
                left join komplain_usulan u on k.id = u.komplain_id
                where u.id is null $this->_filter
                and k.is_deleted=0
                limit $this->limit offset $page
            ";
            // die($sql);
        return dbGetRows($sql);
    }

    public function getKomplainDeleted($page=0){
        $sql = "
                select k.* from $this->_view k 
                left join komplain_usulan u on k.id = u.komplain_id
                where u.id is null $this->_filter
                and k.is_deleted=1
                limit $this->limit offset $page
            ";
            // die($sql);
        return dbGetRows($sql);
    }

    public function getFlow(){
        $sql = "select * from pesanan_flow";
        return dbGetRows($sql);
    }

    public function addFlow($komplain_id, $flow_id, $add=1){
        $new_flow = $flow_id+$add;
        $dataUpdate = array(
            'flow_id'   => $new_flow,
            'updated_at'=> date('Y-m-d G:i:s', time())
        );
        $save = parent::save($komplain_id, $dataUpdate, true);
        return $save;
    }

    public function minFlow($komplain_id, $flow_id, $min=1){
        $new_flow = $flow_id-$min;
        $dataUpdate = array(
            'flow_id'   => $new_flow,
            'updated_at'=> date('Y-m-d G:i:s', time())
        );
        $save = parent::save($komplain_id, $dataUpdate, true);
        return $save;
    }

    public function setMitraPemasang($id_mitra_pemasang, $renker_id, $komplain_id){
        if($renker_id == 1){
            $data['mitra_pemasang_kusen'] = $id_mitra_pemasang;
        }else{
            $data['mitra_pemasang_finish'] = $id_mitra_pemasang;
        }
        $data['updated_at'] = date('Y-m-d G:i:s', time());
        $save = parent::save($komplain_id, $data, true);
        return $save;

    }

    public function setTglAcc($komplain_id){
        $data['tgl_acc_konsumen'] = date('Y-m-d G:i:s', time());
        $data['status'] = self::STATUS_SELESAI;
        $save = parent::save($komplain_id, $data, true);
        return $save;
    }

    public function setJatuhTempo($komplain_id){
        $tgl = date('Y-m-d G:i:s', time());
        $data['tgl_jatuh_tempo'] = date('Y-m-d G:i:s', strtotime('+30 days', strtotime($tgl)));
        $save = parent::save($komplain_id, $data, true);
        return $save;
        
    }

    function insertKomplainImage($komplain_id, $nama_file, $file_ori, $tipe='OM', $user_id) {
        $data = array(
            'komplain_id'    => $komplain_id,
            'tipe'          => $tipe,
            'nama_file'     => $nama_file,
            'created_at'    =>  date('Y-m-d G:i:s', time()),
            'updated_at'    =>  date('Y-m-d G:i:s', time())

        );
        $update = dbInsert('komplain_files',$data);
        if($update){
            return true;
        }else{
            return false;
        }

        
    }

    public function getPhoto($komplain_id){
        $sql = "select kf.*, k.user_id 
                from komplain_files kf
                left join komplain k on kf.komplain_id = k.id
                 where kf.komplain_id=".$komplain_id;
        return dbGetRows($sql);
    }
    
    // DEV
    public function getKomplainsDev($page=0){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "
                select $this->_column from $this->_view k $this->_join $this->_filter ";
        
        if(!$page == 'unl'){
            $sql .=" and k.is_deleted=1 limit 20 offset $page";
        }
        // die($sql);
        return dbGetRows($sql);
    }
    
    public function createDev($data, $user_id, $skip_validation = TRUE, $return = TRUE) {
        
        $dataInsert = array(
            'tgl_komplain'      => $data['tgl_komplain'],
            'tgl_jatuh_tempo'   => date('Y-m-d G:i:s', strtotime('+30 days', strtotime($data['tgl_order_masuk']))),
            'description'       => $data['description'],
            'pesanan_id'        => $data['pesanan_id'],
            'user_id'           => $user_id,
            'status'            => self::STATUS_PROSES,
            'flow_id'           => self::FLOW_PENGAJUANKUSEN,
            'created_at'        => date('Y-m-d G:i:s', time()),
            'updated_at'        => date('Y-m-d G:i:s', time())
        );
        echo "<pre>";print_r($dataInsert);die();
        // $create = parent::create($dataInsert, true, true);
        
        
        if ($create) {
            return $create;
        }else{
            return false;
        }
    }

}