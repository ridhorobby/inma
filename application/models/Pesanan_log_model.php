<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan_log_model extends AppModel {

    private $limit = 10;
    private $table = 'pesanan_log';
    private $_order = "order by";
    const PUBLIC_ALL = 0;
    const PUBLIC_DINAS = 1;
    const PUBLIC_MASYARAKAT = 2;

    const LOG_POST_PESANAN = 'LPP';
    const LOG_PRIVATE_CHANGED = 'LVC';
    const LOG_STATUS_CHANGED = 'LSC';
    const LOG_DINAS_CHANGED = 'LDC';
    const LOG_DINAS_CHANGED_USER = 'LDCU';
    const LOG_APPROVED_DINAS_CHANGED = 'LADC';
    const LOG_APPROVED_DINAS_CHANGED_USER = 'LADCU';
    const LOG_OPERATOR_CHANGED = 'LOC';
    const LOG_OPERATOR_CHANGED_USER = 'LOCU';
   	public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function column($col){
        $this->_columns = $col;
        return $this;
    }

    public function limit($limit){
        $this->_limit = $limit;
        return $this;
    }

    public function order($order='id', $asc="asc"){
        $this->_order = "order by ".$order." ".$asc;
        return $this;
    }

    public function group_by($group_by=''){
        $this->_group_by = $group_by;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getBy(){
        $sql = "select $this->_columns from $this->_table $this->_filter $this->_limit $this->_group_by $this->_order";
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $row = dbGetRow($sql);
        return $row;
    }

    public function getAll(){
        $sql = "select $this->_columns,
        		u.name as nama_user,
                case
                    when u.photo is null then  'users/photos/nopic.png'
                    else 'users/photos/thumb-'||MD5(CAST (u.id AS character varying)) ||'-'||u.photo
                end as foto_profil
        		from $this->table pl 
        		left join users u on pl.user_id = u.id
        		$this->_filter $this->_limit $this->_group_by $this->_order";
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $rows = dbGetRows($sql);
        if($rows){
        	return $rows;
        }else{
        	return array();
        }
        // echo "<pre>";print_r($rows);echo "</pre>";die();
        
    }
    
    public function getOne($col){
        $sql = "select $col from $this->_table $this->_filter" ;
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $column = dbGetOne($sql);
        return $column;
    }


    public function generate($pesanan_id ,$user_id, $description){
    	$data = array(
    		'pesanan_id'	=> $pesanan_id,
    		'user_id'	=> $user_id,
    		'aktivitas'=> $description,
            'created_at'  => date('Y-m-d G:i:s', time())
    	);
        // echo "<pre>";print_r($data);die();
        $create = dbInsert($this->table,$data);

    }

}
