<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Group_model extends AppModel {

    const PRIVACY_PUBLIC = 'C';
    const PRIVACY_PRIVATE = 'E';
    const TYPE_GROUP = 'G';
    const TYPE_DIVISION = 'D';
    const TYPE_PROJECT = 'P';
    const TYPE_KOPERTIS = 'K';
    const TYPE_HELPDESK = 'H';

    private $limit = 50;
    private $table = 'groups';

    function getList($page=1) {
        $page = $this->limit*($page-1);
        $sql = "select * from $this->table order by name limit $this->limit offset $page";
        return dbGetRows($sql);
    }

    function getRow($id) {
        $sql = "select * from $this->table where id=$id";
        return dbGetRow($sql);
    }
    function getListPerMember($user_id, $page=1) {
        if (!$user_id) return false;
        if ($this->User_model->isSuperAdministrator($user_id)) {
            $sql = "select * from groups where type in ('".Group_model::TYPE_GROUP."', '".Group_model::TYPE_KOPERTIS."') and is_delete=0";
            
            return dbGetRows($sql);
        }
        $sql = "select t.* from $this->table t join group_members b on t.id=b.group_id and b.user_id='$user_id' where t.type in ('".Group_model::TYPE_GROUP."', '".Group_model::TYPE_KOPERTIS."') and is_delete=0 order by t.id";
        return dbGetRows($sql);
    }
    
    protected $has_many = array(
        'Post',
        'Group_member',
    );
    protected $belongs_to = array(
        'Division' => array('primary_key' => 'parentId', 'model' => 'Group_model'),
    );
    protected $label = array(
        'name' => 'Nama',
        'privacy' => 'Privasi',
        'image' => 'Gambar',
    );
    protected $validation = array(
        'name' => 'required',
        'privacy' => 'required|in_list[C,E]'
    );

    public static function getType() {
        return array(
            self::TYPE_GROUP => 'GROUP',
            self::TYPE_DIVISION => 'DIVISION',
            self::TYPE_PROJECT => 'PROJECT',
            self::TYPE_KOPERTIS => 'KOPERTIS',
            self::TYPE_HELPDESK => 'HELPDESK'
        );
    }

    protected function property($row) {
        $row['image'] = Image::getImage($row['id'], $row['image'], 'groups/images');
        $row['cover'] = Image::getImage($row['id'], $row['cover'], 'groups/cover');
        return $row;
    }

    public function getMe($userId) {
        $this->_database->select('groups.*');
        $this->_database->join('group_members', 'group_members.group_id = groups.id', 'LEFT');
        $groups = $this->with(array('Group_member' => 'User', 'Division'))->order_by('groups.name', 'ASC')->get_many_by(
                "(group_members.user_id = $userId) OR (group_members.user_id IS NULL AND groups.privacy = '" . self::PRIVACY_PUBLIC . "')");
        foreach ($groups as $key => $group) {
            $groups[$key] = self::setOwner($group);
        }
        return $groups;
    }

    public function getAll() {
        $groups = $this->with(array('Group_member' => 'User', 'Division'))->order_by('name', 'ASC')->get_all();
        foreach ($groups as $key => $group) {
            $groups[$key] = self::setOwner($group);
        }
        return $groups;
    }

    function getDaftar() {
        $sql = "select g.*, m.jumlah from groups g left join (select group_id, count(*) as jumlah from group_members where is_admin=0 group by group_id) m 
            on g.id=m.group_id
            order by g.name asc";
        return dbGetRows($sql);
    }

    function getDaftarGroup() {
        $sql = "select g.*, m.jumlah from groups g left join (select group_id, count(*) as jumlah from group_members where is_admin=0 group by group_id) m 
            on g.id=m.group_id
            where type in ('".Group_model::TYPE_GROUP."', '".Group_model::TYPE_KOPERTIS."') and is_delete=0
            order by g.name asc";
        return dbGetRows($sql);
    }

    function getDaftarHelpdesk($param =NULL) {
        $sql = "select g.*, m.jumlah from groups g left join (select group_id, count(*) as jumlah from group_members  group by group_id) m 
            on g.id=m.group_id
            where type='H' and is_delete=0
            order by g.name asc";
        return dbGetRows($sql);
    }

    function getDaftarMyHelpdesk($user_id) {
        $sql = "select g.*, m.jumlah from groups g left join (select group_id,  count(*) as jumlah from group_members group by group_id) m 
            on g.id=m.group_id
            left join group_members gm on g.id=gm.group_id
            where type='H' and gm.user_id='$user_id' and is_delete=0
            order by g.name asc";
            // echo nl2br($sql);
            // die();
        return dbGetRows($sql);
    }

    function getMyHelpdesk($param =NULL) {
        if ($param!=NULL){
            $filter_id = " and id in ($param) ";
        }
        $sql = "select g.*, m.jumlah from groups g left join (select group_id, count(*) as jumlah from group_members where is_admin=0 group by group_id) m 
            on g.id=m.group_id
            where type='H' $filter_id
            order by g.name asc";
        return dbGetRow($sql);
    }

    //Pakai active record
    public function get($primary_value) {
        $this->with(array('Group_member' => 'User', 'division'));
        $group = parent::get($primary_value);
        //         echo '<pre>';
        // var_dump($group);
        // echo '</pre>';
        // die();
        return self::setOwner($group);
    }

    // public function get($primary_value){
    //     $sql = "select id, name, created_at as \"createdAt\", updated_at as \"updatedAt\", privacy, image, description, type, parent_id as \"parentId\", cover 
    //         from groups where id=$primary_value";
    //     $row = dbGetRow($sql);
    //     if ($row!=NULL){
    //         $group = $row;
    //         $group['image']=Image::getImage($primary_value, $row['image'], 'groups/images');
    //         $group['cover']=Image::getImage($primary_value,$row['cover'],'groups/cover');
    //         $sql = "select gm.group_id as \"groupId\", gm.user_id as \"userId\", gm.is_admin as \"isAdmin\" 
    //             from group_members gm 
    //             where gm.group_id=$primary_value";
    //         $group_members = dbGetRows($sql);
    //         foreach ($group_members as &$members) {
    //             $user_id = $members['userId'];
    //             $sql = "select id, name, username, photo, role from users where id='$user_id'";
    //             $members['user'] = dbGetRow($sql);
    //         }
    //         $group['groupMembers']=$group_members;
    //     }else {
    //         $group=NULL;
    //     }
        
    //     // $group['id'] = $primary_value;
    //     // $group['name'] = $row['name'];
    //     // $group['createdAt']= $row['created_at'];
    //     // $group['updatedAt'] = $row['updated_at'];
    //     // $group['privacy']=$row['privacy'];
    //     // $group['image']=Image::getImage($primary_value, $row['image'], 'groups/images');
    //     // $group['description']=$row['description'];
    //     // $group['type']=$row['type'];
    //     // $group['parentId']=$row['parent_id'];
    //     // $group['cover']=Image::getImage($primary_value,$row['cover'],'groups/cover');
    //     // echo '<pre>';
    //     // var_dump($group);
    //     // echo '</pre>';
    //     // die();
    //     return $group;
    // }

    public static function setOwner($group) {
        if (!empty($group['groupMembers'])) {
            foreach ($group['groupMembers'] as $member) {
                if ($member['groupMemberRole']['id'] == Group_member_model::ROLE_OUWNER) {
                    $group['owner'] = $member['user'];
                    break;
                }
            }
        }
        return $group;
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        $toMember = empty($data['members']) ? array() : explode(',', $data['members']);
        $toMember[] = $data['userId'];
        if (empty($toMember)) {
            return 'Data member tidak ada.';
        }
        $this->_database->trans_begin();
        $create = parent::create($data, $skip_validation, $return);

        if ($create) {
            $this->load->model('Group_member_model');
            foreach ($toMember as $member) {
                $dataToMember = array(
                    'group_id' => $create,
                    'user_id' => $member,
                    'role' => (($member == $data['userId']) ? Group_member_model::ROLE_OUWNER : Group_member_model::ROLE_MEMBER)
                );
                $insert = $this->Group_member_model->insert($dataToMember, TRUE, FALSE);
                if (!$insert) {
                    $this->_database->trans_rollback();
                    return FALSE;
                }
            }

            $upload = TRUE;
            if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                $upload = Image::upload('image', $create, $_FILES['image']['name'], 'groups/images');
            }
            if ($upload === TRUE) {
                $this->load->model('Notification_model');
                $this->Notification_model->generate(Notification_model::ACTION_GROUP_CREATE, $create,$this->User_model->getUserID());
                // $this->Notification_model->mobileNotification(Notification_model::ACTION_GROUP_CREATE, $create,$this->User_model->getUserID());
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function getForMe($userId) {
        $this->_database->select('groups.*');
        $this->_database->join('group_members', 'group_members.group_id = groups.id');
        $groups = $this->with(array('Group_member' => 'User'))->order_by('groups.name', 'ASC')->get_many_by(array('group_members.user_id' => $userId));
        foreach ($groups as $key => $group) {
            $groups[$key] = self::setOwner($group);
        }
        return $groups;
    }

    public function savephoto($primary_value, $data = array(), $skip_validation = FALSE) {
        $this->_database->trans_begin();
        $update = parent::save($primary_value, $data, $skip_validation);
        if ($update) {
            $upload = TRUE;
            if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                $upload = Image::upload('image', $primary_value, $_FILES['image']['name'], 'groups/images');
            }
            if ($upload === TRUE) {
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function savecover($primary_value, $data = array(), $skip_validation = FALSE) {
        $this->_database->trans_begin();
        $update = parent::save($primary_value, $data, $skip_validation);

        if ($update) {
            $upload = TRUE;
            if (isset($_FILES['cover']) && !empty($_FILES['cover'])) {
                $upload = Image::upload('cover', $primary_value, $_FILES['cover']['name'], 'groups/cover');
            }
            if ($upload === TRUE) {
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function save($primary_value, $data = array(), $skip_validation = FALSE) {
        $toMember = empty($data['members']) ? array() : explode(',', $data['members']);
        $toMember[] = $data['userId'];
        if (empty($toMember)) {
            return 'Data member tidak ada.';
        }
        $this->_database->trans_begin();
        $update = parent::save($primary_value, $data, $skip_validation);

        if ($update) {
            $this->load->model('Group_member_model');
            if (!empty($toMember)) {
                $this->Group_member_model->delete_by(array('group_id' => $primary_value, 'user_id NOT IN (' . implode(',', $toMember) . ')'));
            }
            $oldMember = Util::toMapObject($this->Group_member_model->get_many_by(array('group_id' => $primary_value, 'user_id' => $toMember)), 'userId');
            foreach ($toMember as $member) {
                $role = ($member == $data['userId']) ? Group_member_model::ROLE_OUWNER : Group_member_model::ROLE_MEMBER;
                $action = TRUE;
                if (!isset($oldMember[$member])) {
                    $dataToMember = array(
                        'group_id' => $primary_value,
                        'user_id' => $member,
                        'role' => $role
                    );
                    $action = $this->Group_member_model->insert($dataToMember, TRUE, FALSE);
                } elseif ($oldMember[$member]['groupMemberRole']['id'] != $role) {
                    $action = $this->Group_member_model->update($oldMember[$member]['id'], array('role' => $role));
                }
                if (!$action) {
                    $this->_database->trans_rollback();
                    return FALSE;
                }
            }

            $upload = TRUE;
            if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                $upload = Image::upload('image', $primary_value, $_FILES['image']['name'], 'groups/images');
            }
            if ($upload === TRUE) {
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function getDropdownByType($type, $list = array(), $groups = array()) {
        if (empty($groups)) {
            $this->_database->select('groups.*');
            $groups = $this->get_many_by('groups.type', $type);
        }

        foreach ($groups as $group) {
            if ($group['type'] == $type)
                $list[$group['id']] = $group['name'];
        }
        return $list;
    }

    public function getDropdownProject($userId, $list = array()) {
        $this->_database->join('group_members', 'group_members.group_id = groups.id');
        if (!empty($userId))
            $this->_database->where('group_members.user_id', $userId);
        return $this->getDropdownByType(self::TYPE_PROJECT, $list);
    }

    function add($data) {
        // $record = array();
        // $record['name'] = $name;
        return dbInsert('groups', $data);
    }

    function addhelpdesk($name) {
        $record = array();
        $record['name'] = $name;
        $record['type'] = 'H';
        $record['quota_limit'] = 0;
        return dbInsert('groups', $record);
    }

    function addUser($group_id, $user_id) {
        $record = array();
        $record['group_id'] = (int) $group_id;
        $record['user_id'] = $user_id;
        dbInsert('group_members', $record);
    }

    function deleteUser($group_id, $user_id) {
        $group_id = (int) $group_id;
        $sql = "delete from group_members where group_id=$group_id and user_id='$user_id'";
        $query = dbQuery($sql);
        return $query;
    }

    function setStaff($group_id, $user_id) {
        $group_id = (int) $group_id;
        $record = array();
        $record['is_admin'] = 1;
        dbUpdate('group_members', $record, "group_id=$group_id and user_id='$user_id'");
    }

    function removeStaff($group_id, $user_id) {
        $group_id = (int) $group_id;
        $record = array();
        $record['is_admin'] = 0;
        dbUpdate('group_members', $record, "group_id=$group_id and user_id='$user_id'");
    }

    public function getGroupDefault(){
        $group_id = 84;
        $sql = "select * from groups g where g.id=$group_id";
        $group = dbGetRow($sql);
        return $group;
    }

    public function getGroupBy($column,$param){
        if ($column=='name'){
            $param = "'".$param."'";
        }
        $sql = "select * from groups g where $column=$param";
        $group = dbGetRow($sql);
        $group['image'] = $this->Group_model->getGroupPhoto($group['id'],$group['image']);
        // $group['cover'] = $this->Group_model->getGroupCover($group['id'],$group['cover']);
        return $group;
    }

    public function getGroupPhoto($group_id, $photo){
        if ($photo!=NULL) {
            $file = Image::getLocation($group_id, Image::IMAGE_THUMB, $photo, 'groups/images');
            $group_photo =base_url('assets/uploads/groups/images/'.Image::getFileName($group_id, Image::IMAGE_THUMB, $photo));
            if (!file_exists($file) || is_dir($file)){
                $file = Image::getLocation($user_id, Image::IMAGE_ROUNDED, $photo, 'users/photos');
                $file .='.'.$ext;
                $group_photo = 'assets/uploads/groups/images/'.Image::getFileName($group_id, Image::IMAGE_ROUNDED, $photo);
                if (!file_exists($file) || is_dir($file)){
                    $group_photo = base_url('assets/web/img/logo.png');
                }
            }            
        } else {
            $group_photo = base_url('assets/web/img/logo.png');
        }
        return $group_photo;
    }

    public function getGroupCover($group_id, $cover){
        if ($cover!=NULL) {
            $file = Image::getLocation($group_id, Image::IMAGE_THUMB, $cover, 'groups/cover');
            $group_cover =base_url('assets/uploads/groups/cover/'.Image::getFileName($group_id, Image::IMAGE_THUMB, $cover));
            if (!file_exists($file) || is_dir($file)){
                $group_cover = NULL;
            }            
        } else {
            $group_cover = NULL;
        }
        return $group_cover;
    }

    public function updateInitial($data){
        $record = array();
        $id = (int)$data['id'];
        $record['initial'] = $data['initial'];
        return dbUpdate('groups',$record,"id=$id");
    }

    public function updateGroupType($data){
        $record = array();
        $id = (int)$data['id'];
        $record['type'] = $data['type'];
        return dbUpdate('groups',$record,"id=$id");
    }

    public function updateDescription($data){
        $record = array();
        $id = (int)$data['id'];
        $record['description'] = $data['description'];
        return dbUpdate('groups',$record,"id=$id");
    }

    public function updateQuotaLimit($data){
        $record = array();
        $id = (int)$data['id'];
        $record['quota_limit'] = $data['quota_limit'];
        return dbUpdate('groups',$record,"id=$id");
    }

    public function getHelpdeskInitial(){
        $sql = "select id as group_id,name, initial from groups g where g.type='H'";
        $initials = dbGetRows($sql);
        $data = array();
        foreach ($initials as $key => $initial) {
            if (empty($initial['initial'])) {
                $initials[$key]['initial'] = strtoupper(substr($initial['name'],0,3));
                // $data[$initial['group_id']] = strtoupper(substr($initial['name'],0,3));
                array_push($data,strtoupper(substr($initial['name'],0,3)));
            } else {
                // $data[$initial['group_id']] = $initial['initial'];
                array_push($data,$initial['initial']);
            }
        }
        return $data;
    }

    public function delete($id){
        $sql = "update $this->_table set is_delete=1 where id=$id";
        return dbQuery($sql);
    }
}
