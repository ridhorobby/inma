<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Harga_maintenance_model extends AppModel {

    protected $_filter = "";
    protected $_order = "";
    protected $_table = "harga_maintenance";
    protected $_view = "harga_maintenance_view";
    protected $_column = "*";
    protected $_join = "";


    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function create($data, $user_id, $skip_validation = TRUE, $return = TRUE) {
        $create = parent::create($data, true, true);
        
        if ($create) {
            return $create;
        }else{
            return false;
        }
    }

    public function updateHargaMaintenance($data){
        $dataUpdate = array(
            'nama'          => $data['nama'],
            'harga'         => $data['harga'],
            'updated_at'    => date('Y-m-d G:i:s', time())
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }

    public function getHargaMaintenance($page=0){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "select $this->_column from $this->_table p $this->_join $this->_filter ";
        return dbGetRows($sql);
    }

    public function getHargaMaintenanceById($id){
        return dbGetRow("select * from harga_maintenance where id = $id");
    }

    public function deleteHargaMaintenance($id) {
        $dataUpdate = array(
            'is_deleted' => "1"
        );
        $save = parent::save($id, $dataUpdate, true);
        return $save;
    }


}
