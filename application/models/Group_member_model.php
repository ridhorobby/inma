<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Group_member_model extends AppModel {

    const ROLE_OUWNER = 'O';
    const ROLE_ADMINISTRATOR = 'A';
    const ROLE_MEMBER = 'M';

    protected $after_get = array('mapper', 'property');
    protected $belongs_to = array(
        'Group',
        'User'
    );
    protected $label = array(
        'group_id' => 'Group',
        'user_id' => 'User',
        'role' => 'Role',
    );
    protected $validation = array(
        'user_id' => 'required|integer',
        'group_id' => 'required|integer',
        'role' => 'required|in_list[O,A,M]'
    );

    public static function getRole($role = NULL) {
        $roles = array(
            self::ROLE_OUWNER => 'Owner',
            self::ROLE_ADMINISTRATOR => 'Admnistrator',
            self::ROLE_MEMBER => 'Member'
        );
        return empty($role) ? $roles : $roles[$role];
    }

    protected function property($row) {
        if (!empty($row['role'])) {
            $row['groupMemberRole'] = array(
                'id' => $row['role'],
                'name' => self::getRole($row['role'])
            );
        }
        unset($row['role']);
        return $row;
    }

    public function getMembers($id,$page=0,$param=NULL, $limit=50){

        $CI = & get_instance();
        $filter ='';
        $offset = $page * $limit;
        if (!empty($param)) {
            $filter .= " and (u.name ilike '%" . $param . "%' or u.username ilike '%" . $param . "%')";
        }
        $sql = "select gm.group_id as groupId, gm.user_id as userId, u.name as name, u.username as username, u.photo as user_photo, u.role as role from group_members gm
            join users u on u.id=gm.user_id
            join groups g on g.id=gm.group_id
            where gm.group_id=$id $filter  
            order by u.name";

        $next_page_sql = $sql;
        $sql .=" limit $limit  offset $offset ";

        $rows = dbGetRows($sql);
        foreach ($rows as $key => $row) {
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['userid'],$row['user_photo']);
        }
        $offset=($page+1)*$limit;
        $next_page_sql .="
            limit $limit offset $offset";
        
        $data_next_page = dbGetRows($next_page_sql);
        if (empty($data_next_page)) {
            $rows['next_page_member']=0;
        } else {
            $rows['next_page_member']=1;
        }


        return $rows;
    }

    public function getStaffs($id,$page=0){
        $CI = & get_instance();
        $filter ='';
        $limit=50;
        $offset = $page * $limit;

        // if (!empty($param)) {
        //         $filter .= " and (u.name ilike '%" . $param . "%' or u.username ilike '%" . $param . "%')";
        // }

        $sql = "select gm.group_id as groupId, gm.user_id as userId, u.name as name, u.username as username, u.photo as user_photo, u.role as role from group_members gm
            join users u on u.id=gm.user_id
            join groups g on g.id=gm.group_id
            where gm.group_id=$id and gm.is_admin=1 $filter  
            order by u.name";
        $sql .=" limit $limit  offset $offset ";
        $rows = dbGetRows($sql);
        foreach ($rows as $key => $row) {
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['userid'],$row['user_photo']);
        }
        return $rows;
    }

    public function getGroupFromUserId($user_id){
        $sql = "select * from groups g left join group_members gm on gm.group_id=g.id where gm.user_id='$user_id'";
        $group = dbGetRow($sql);
        return $group;
    }

    public function getMyGroups($user_id){
        $sql = "select gm.group_id from group_members gm
            left join users u on u.id=gm.user_id
            where u.id='$user_id'";
        $groups = dbGetRows($sql);
        return $groups;
    }

    public function getMyHelpdeskGroup($user_id){
        if (!$this->User_model->isSuperAdministrator($user_id)){
            $filter_user = " and u.id='$user_id'";
        }
        $sql = "select g.* from group_members gm
            left join groups g on g.id=gm.group_id
            left join users u on u.id=gm.user_id
            where g.type='H' $filter_user";
        $group = dbGetRow($sql);

        return $group;
    }

    public function getMyHelpdeskGroupCommentAs($user_id){
        if (!$this->User_model->isSuperAdministrator($user_id)){
            $filter_user = " and u.id='$user_id'";
        }
        $sql = "select g.* from group_members gm
            left join groups g on g.id=gm.group_id
            left join users u on u.id=gm.user_id
            where g.type='H' $filter_user";
        $group = dbGetRows($sql);
        
        return $group;
    }

    function getAdminKopertis($user_id) {
        $sql = "select * from group_members gm 
            left join groups g on g.id=gm.group_id
            where g.name ilike 'Kopertis %' and gm.is_admin=1 and gm.user_id='$user_id'";
        return dbGetRow($sql);
    }

    function getAllAdminKopertis() {
        $sql = "select u.* from group_members gm 
            left join groups g on g.id=gm.group_id
            left join users u on u.id=gm.user_id
            where g.type='K' and gm.is_admin=1";
        return dbGetRows($sql);
    }

    public function isAdminKopertis($user_id){
        $sql = "select 1 from group_members gm 
            left join groups g on g.id=gm.group_id
            where g.name ilike 'Kopertis %' and gm.is_admin=1 and gm.user_id='$user_id'";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    function _isAdminGroup($user_id) {
        $sql = "select 1 from group_members gm left join users u on u.id='$user_id' and u.role='O' where user_id='$user_id' and is_admin=1 and u.id is not null";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    function isAdminThisGroup($group_id,$user_id){
        $sql = "select 1 from group_members gm 
                left join users u on u.id='$user_id' 
                where gm.group_id=$group_id and user_id='$user_id' and is_admin=1 and u.id is not null";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    function isAdminThisHelpdesk($group_id,$user_id){
        if ($this->User_model->isAdministrator($user_id)) {
            $sql = "select 1 from group_members gm 
                left join users u on u.id='$user_id' 
                where gm.group_id=$group_id and user_id='$user_id' and  u.id is not null";
            if (dbGetOne($sql))
                return true;
        }
        
        return false;
    }

    function getAllMembers($id){
        $sql = "select u.* from group_members gm left join users u on u.id=gm.user_id where gm.group_id=$id and u.id is not null order by u.name";
        $members = dbGetRows($sql);
        return $members;
    }

}
