<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_sender_model extends AppModel {

    protected $belongs_to = array(
        'User',
        'Notification'
    );

}
