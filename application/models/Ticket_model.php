<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket_model extends AppModel {

    protected $belongs_to = array(
        'Post'
    );
    protected $label = array(
        'id' => 'Kiriman',
    );
    protected $validation = array(
        'post_id' => 'required|integer'
    );

}
