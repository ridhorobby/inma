<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Comment_model extends AppModel {

    const DISKUSI = 'D';
    const KOMENTAR = 'K';

    protected $_table = "comments";
    protected $_filter = "";
    protected $show_sql = false;

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    protected $belongs_to = array(
        'User',
        'Post'
    );
    protected $label = array(
        'user_id' => 'User',
        'post_id' => 'Postingan',
        'text' => 'Komentar',
    );
    protected $validation = array(
        'text' => 'required'
    );


    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function create($data, $skip_validation = FALSE, $notification = TRUE) {
        $this->_database->trans_begin();
        $this->load->model('Post_model');
        // echo "<pre>";
        // var_dump($data);
        // die();
        $data['text'] = $this->checkURL($data['text']);
        $data['is_delete'] = 0;
        $create = parent::create($data, $skip_validation, true);
        // $create = dbInsert('comments', $data);
        if ($create) {
    	    // if($notification){
         //        $this->load->model('Notification_model');
         //        $this->load->model('User_model');
         //        $this->Notification_model->generate(Notification_model::ACTION_COMMENT_CREATE, $create,$this->User_model->getUserID(), $data['group_id']);
         //        // if (!isset($data['candelete'])){
         //        //     $this->Notification_model->mobileNotification(Notification_model::ACTION_COMMENT_CREATE, $create,$this->User_model->getUserID(), $data['group_id']);
         //        // }
                
         //    }
            return $create;
        } else {
            $this->_database->trans_rollback();
            return FALSE;
        }
    }

    function deletes($id) {
        $id = (int) $id;
        $sql = "update comments set is_delete=1 where id=$id";
        return dbQuery($sql);
    }
    
    public function deleteComment($id){
	$this->_database->trans_begin();
	$comment = $this->get($id);
	$this->load->model('Post_model');
        $post = $this->Post_model->with(array('Group' => 'Group_member', 'User', 'Post_user'))->get_by('posts.id', $comment['postId']);
        $delete = $this->delete($id);
        if($delete){
	  if(!empty($post['parentHierarchy'])){
	    $cond = array(
	      'post_id' => explode('/', $post['parentHierarchy']),
	      'text' => $comment['text'],
	      'user_id' => $comment['userId']
	    );
	    $deleteparent =  $this->delete_by($cond);
	    if(!$deleteparent){
	      $this->_database->trans_rollback();
	      return FALSE;
	    }
	  }
	  return $this->_database->trans_commit();
        }
        $this->_database->trans_rollback();
	return FALSE;
    }

    // function insertPostImage($comment_id, $image) {
    //     $record = array();
    //     $record['comment_id'] = (int) $comment_id;
    //     $record['image'] = $image;

    //     return dbInsert('comment_image', $record);
    // }

    function insertPostImage($comment_id, $nama_file, $file_ori) {
        $db2 = $this->load->database('upload', true);

        $record = array();
        $record['comment_id'] = $comment_id = (int) $comment_id;
        $record['image'] = $nama_file;
        if (dbInsert('comment_image', $record)) {
            $ukuran = strlen($file_ori);
            $isi_file = file_get_contents($file_ori);
            $isi_file = pg_escape_bytea($isi_file);
            
            $db2 = $this->load->database('upload', true);
            
            $sql = "insert into appfile (trans_id, trans_type, nama_file,ukuran,isi_file)
                values($comment_id, 'C', '$nama_file', $ukuran,'$isi_file') ";          
            dbQuery($sql, $db2);
        }
        return true;
    }

    function insertPostFile($comment_id, $nama_file, $file_ori) {
        $db2 = $this->load->database('upload', true);

        $record = array();
        $record['comment_id'] = $comment_id = (int) $comment_id;
        $record['file'] = $nama_file;

        if (dbInsert('comment_file', $record)) {
            $ukuran = strlen($file_ori);
            $isi_file = file_get_contents($file_ori);
            $isi_file = pg_escape_bytea($isi_file);
            
            $db2 = $this->load->database('upload', true);
            
            $sql = "insert into appfile (trans_id, trans_type, nama_file,ukuran,isi_file)
                values($comment_id, 'C', '$nama_file', $ukuran,'$isi_file') ";          
            dbQuery($sql, $db2);
        }

        return true;
    }

    function getOne($col){
        $sql = "select $col from $this->_table $this->_filter";
        return dbGetOne($sql);
    }

    function getAll($post_id, $type=self::DISKUSI, $mobile=false){
        $ciConfig = $this->config->item('utils');
        if($mobile){
            $ip = $ciConfig['full_upload_dir_ip'];
        }
        $sql = "select  c.*,u.name as nama_user, u.photo as foto_user,
                case
                    when u.photo is null then  '".$ip."users/photos/nopic.png'
                    else '".$ip."users/photos/thumb-'||MD5(CAST (u.id AS character varying)) ||'-'||u.photo
                end
                as foto_profil
          from $this->_table c 
                left join users u on c.user_id = u.id
         where post_id=".$post_id." and type='".$type."'";
        if($this->_show_sql){
            die($sql);
        }
        return dbGetRows($sql);
    }

}
