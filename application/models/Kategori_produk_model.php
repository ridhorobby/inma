<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_produk_model extends AppModel {

    protected $_filter = "";
    protected $_order = "";
    protected $_table = "kategori_produk";
    protected $_column = "*";
    protected $_join = "";


    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function create($data, $user_id, $skip_validation = TRUE, $return = TRUE) {
        $dataInsert = array(
            'nama'                  => $data['nama']
        );

        $create = parent::create($dataInsert, true, true);
        if ($create) {
            return $create;
        }else{
            return false;
        }
    }

    public function updateKategori($data){
        $dataUpdate = array(
            'nama'                  => $data['nama']
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }

    public function getKategori(){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "select $this->_column from $this->_table ";
        return dbGetRows($sql);
    }


}