<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// require '../third_party/PHPMailer/PHPMailerAutoload.php';

class Email_model extends AppModel {
  public $ispublic = true;
  private $_from = 'noreply-meole@pemkabposo.com';
  private $_name = 'Meole - Pemkab Poso';
  private $_subject = "";
  private $_message = "";
  private $_recipient = array();
  private $config = Array(
  'protocol' => 'smtp',
  'smtp_host' => 'ssl://smtp.gmail.com',
  'smtp_port' => 465,
  // 'smtp_host' => '10.15.3.12',
  // 'smtp_port' => 25,
  'smtp_user' => 'integra.email.testing@gmail.com', // change it to yours
  'smtp_pass' => 's3mbarang', // change it to yours
  'mailtype' => 'html',
  'charset' => 'iso-8859-1',
  'newline'   => "\r\n",
  'wordwrap' => TRUE
  );
  
  const NOTIFICATION = 'N';
  const SUMMARY = 'S';
  const REGISTER = 'R';

  public function __construct(){
    parent::__construct();
    $this->load->library('email', $this->config);
    $this->email;
  }

  public function send(){
    // SET EMAIL
    $this->load->library('email', $this->config);
    $this->email->set_crlf( "\r\n" );
    $this->email->set_newline("\r\n")
                ->from($this->_from, $this->_name)
                ->to(implode(",", $this->_recipient))
                ->subject($this->_subject)
                ->message($this->_message)
                ->attach($this->_attach);
    if($this->email->send()){
      return 'success';
    }else{
      return $this->email->print_debugger();
    }
  }

  public function reset(){
    $this->_subject = "";
    $this->_message = "";
    $this->_recipient = array();
  }

  public function to($email){
    if(is_array($email)){
      foreach ($email as $value) {
        if(filter_var($value, FILTER_VALIDATE_EMAIL))
          $this->_recipient[] = $value;
      }
    }else{
      if(filter_var($email, FILTER_VALIDATE_EMAIL))
        $this->_recipient[] = $email;
    }
    return $this;
  }

  /*
  * @msg = array('head',
  'body' => array('content', 
  'url'),
  'footer'
  )
  */
  public function message($msg = null){
    if(is_array($msg)){
      $html = "<html>
                <body style='background-color: #F5F5F5;'>
                  <div style='margin: 2% 20%;background-color: #fff;border: 1px solid #E0E0E0;background-repeat: no-repeat;background-attachment: fixed;width: 60%;height: 50%;border-radius: 5px;text-align: left;'>
                    <div style='padding: 3% 4% 3% 4%;'>
                      <div style='font-family:Muli, Montserrat, Geneva, sans-serif;color: #424242;margin: 0px;color: #757575;font-weight: 300;font-size: 16px;'>";
      switch ($msg['type']) {
        case self::REGISTER:
          $html .= " 
                    <h1>Meole Notification</h1>
                    <p>
                      ".$msg['body']['content']."<br><br>
                      Nama : ".$msg['name']."<br>
                      Nomor HP : ".$msg['no_hp']."
                    </p>
                    
                    <a href=\"".$msg['url']."/".$msg['activation_id']."\" style='background-color: #2ecc71;border: 0px solid;border-radius: 3px;color: #fff;padding: 8px 20px;font-weight: bold;font-family:Muli, Montserrat, Geneva, sans-serif;text-align: center;text-decoration: none;letter-spacing: 0.3px;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer;'>Klik disini untuk melakukan aktivasi</a>
                      
           
                    
                    ";
        break;
      }
      $html .= "    
                  </div>
                </div>
              </body>
            </html>";
      $this->_message = $html;
    }else{
      $this->_message = $msg;
    }
    return $this;
  }

  public function subject($subject){
  $this->_subject = $subject;
  return $this;
  }

  public function attach($attach){
  $this->_attach = $attach;
  return $this;
  }

}
