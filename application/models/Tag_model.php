<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tag_model extends AppModel {

    protected $validation = array(
        'name' => 'required'
    );

    public function getTags(){
        $sql = "select id, name as name from tags order by name";
        $rows = $this->_database->query($sql)->result_array();
        return $rows;
    }

    public function getListTags() {
        $rows = $this->getTags();
        unset($rows[""]);
        $data = array();
        foreach($rows as $row){
            if ($row['name']!="")
                $data[$row['name']] = $row['name'];
        }
        return $data;
    }

    public function getListTagsAndroid() {
        $rows = $this->getTags();
        $data = array();
        foreach($rows as $row){
            $data['tags'][] = $row['name'];
        }
        return json_encode($data);
    }

    public function getDropdownTags(){
        $rows = $this->getTags();
        $data = array();
        foreach($rows as $row){
            $data[$row['id']] = $row['name'];
        }
        return $data;
    }

}
?>