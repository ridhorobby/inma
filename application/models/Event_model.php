<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->model('Post_model');

class Event_model extends Post_model {

    protected $_table = 'posts';

    protected function property($row) {
        $row['isOverTime'] = ($row['isOverTime'] == 1) ? TRUE : FALSE;
        return parent::property($row);
    }

    protected function join_or_where($row) {
        $this->_database->join('events', 'posts.id = events.post_id');
        $this->_database->where('category_id', Category_model::EVENT);
        $this->with(array('Category', 'User', 'Comment' => 'User', 'Post_user', 'Group' => array('Group_member' => 'User')));
        $this->order_by('events.date, posts.created_at', 'DESC');
        return $row;
    }

    public function getMe($userId) {
        $condition = array(
            'posts.user_id' => $userId
        );
        return $this->get_many_by($condition);
    }

    public function getForMe($userId) {
        $condition = array(
            '(SELECT COUNT(*) 
                    FROM post_users 
                    WHERE post_users.post_id = posts.id AND post_users.user_id = ' . $userId . ' 
                    LIMIT 1) = 1'
        );
        return $this->get_many_by($condition);
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        if (empty($data['description']) && empty($data['link']) && empty($data['image']) && empty($data['video']) && empty($data['file'])) {
            return 'Data yang dikirim tidak lengkap.';
        }

        $this->_database->trans_begin();
        if (empty($data['postUsers'])) {
            return 'Tujuan laporan harian harus diisi.';
        }
        $data['type'] = self::TYPE_FRIEND;
        $data['categoryId'] = Category_model::DAILY_REPORT;
        $create = parent::defaultCreate($data, $skip_validation, $return);
        if ($create) {
            $data['postId'] = $create;
            $createDailyReport = $this->_database->insert('daily_reports', $this->setFieldDB($data, $this->_database->list_fields('daily_reports')));
            if (!$createDailyReport) {
                $this->_database->trans_rollback();
                return FALSE;
            }
            if (isset($data['postUsers'])) {
                $toUser = explode(',', $data['postUsers']);
                $this->load->model('Post_user_model');
                foreach ($toUser as $user) {
                    $dataToUser = array(
                        'post_id' => $create,
                        'user_id' => $user
                    );
                    $insert = $this->Post_user_model->insert($dataToUser, TRUE, FALSE);
                    if (!$insert) {
                        $this->_database->trans_rollback();
                        return FALSE;
                    }
                }
            }
            $upload = TRUE;
            if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                $upload = Image::upload('image', $create, $_FILES['image']['name'], 'posts/photos');
            }
            if (isset($_FILES['video']) && !empty($_FILES['video'])) {
                $upload = File::upload('video', $create, 'posts/videos');
            }
            if (isset($_FILES['file']) && !empty($_FILES['file'])) {
                $upload = File::upload('file', $create, 'posts/files');
            }
            if ($upload === TRUE) {
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

}
