<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usulan_model extends AppModel {

    public $limit = 10;



    //STATUS
    const STATUS_PENGAJUAN = 'P';
    const STATUS_TERIMA = 'D';
    const STATUS_TOLAK = 'R';
    const STATUS_RESCHEDULE = 'E';

    public $limit_quota = 3;   
    protected $_filter = "";
    protected $_order = "";
    protected $_table = "usulan";
    protected $_column = "*";
    protected $_join = "";

    protected $has_many = array(
        'Comment',
    );
    protected $belongs_to = array(
        'User',
        'Group',
        'Category',
        'Updated_by' => array('primary_key' => 'userIdUpdated', 'model' => 'User_model')
    );
    protected $many_to_many = array(
        'Post_tag' => array('Post', 'Tag')
    );
    protected $label = array(
        'category_id' => 'Kategori',
        'image' => 'Gambar',
        'file' => 'Berkas',
        'video' => 'Video'
    );
    protected $validation = array(
        'link' => 'valid_url',
        'category_id' => 'required|integer'
    );

    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function resetVariables(){
        $this->_filter = '';
        $this->column = '*';
        $this->_join = '';
        $this->_show_sql = false;
    }

    function getCount() {
        $sql = "select count(*) from $this->table {$this->filter}";
        return dbGetOne($sql);
    }

    public function create($jadwal_usulan, $jam_usulan, $rencana_kerja_id, $flow_id, $pesanan_id, $user_id) {
        $dataInsert = array(
            'jadwal_usulan'     => $jadwal_usulan,
            'jam_usulan'        => $jam_usulan,
            'status'            => self::STATUS_PENGAJUAN,
            'rencana_kerja_id'  => $rencana_kerja_id,
            'flow_id'           => $flow_id,
            'pesanan_id'        => $pesanan_id,
            'created_at'        => date('Y-m-d G:i:s', time()),
            'updated_at'        => date('Y-m-d G:i:s', time()),
            'user_id'           => $user_id
        );
        // echo "<pre>";print_r($dataInsert);die();
        // $this->_database->trans_begin();
        // $create = db
        // $create = dbInsert($this->_table, $dataInsert);
        $create = parent::create($dataInsert, true, true);
        if ($create) {
            return $create;

        }else{
            return false;
        }
    }

    public function konfirmasi($confirm=1,$jadwal_usulan=null,$jam_usulan=null, $rencana_kerja_id, $flow_id, $pesanan_id,$approver_id,$reschedule=false, $flow_tujuan=false){
        if(!$reschedule){
            if($confirm == 0){
                $dataUpdate = array(
                    'status'        => self::STATUS_TOLAK,
                    'approver_id'   => $approver_id,
                    'updated_at'    => date('Y-m-d G:i:s', time())
                );

                if(!$flow_tujuan){
                    if($flow_id==2 || $flow_id==5){
                        $flow_tujuan = $flow_id+1;
                    }else{
                        $flow_tujuan = $flow_id-1;
                    }
                }
                // echo "<pre>";print_r($dataUpdate);die();
                $usulan_id = dbGetOne("select id from $this->_table where pesanan_id=".$pesanan_id." and flow_id=".$flow_tujuan." and status='".self::STATUS_PENGAJUAN."'");
                // $updateProd = dbUpdate($this->_table, $dataUpdate, "id=".$usulan_id);die();
                $updateProd = parent::save($usulan_id, $dataUpdate, true);
                if($updateProd){
                    if($rencana_kerja_id == 1){
                        $flow_renker = 2;
                    }else{
                        $flow_renker = 5;
                    }

                    $tambahkan = self::create($jadwal_usulan,$jam_usulan, $rencana_kerja_id, $flow_renker, $pesanan_id, $approver_id);
                    if($tambahkan){
                        return true;
                    }
                }
            }else{
                $usulan_id = dbGetOne("select id from $this->_table where pesanan_id=".$pesanan_id." and status='".self::STATUS_PENGAJUAN."'");
                $dataUpdate = array(
                    'status'        => self::STATUS_TERIMA,
                    'approver_id'   => $approver_id,
                    'updated_at'    => date('Y-m-d G:i:s', time())
                );
                $updatePengajuan = parent::save($usulan_id, $dataUpdate, true);
                if($updatePengajuan){
                    return true;
                }
            }
        }else{
            if($rencana_kerja_id==3){
                $flow_mundur = 5;
                $rencana_kerja_id = 2;
            }else{
                $flow_mundur = 2;
            }
            $usulan_id = dbGetOne("select id from $this->_table where pesanan_id=".$pesanan_id." and status='".self::STATUS_TERIMA."' and rencana_kerja_id=".$rencana_kerja_id);
            $dataUpdate = array(
                    'status'        => self::STATUS_RESCHEDULE,
                    'rescheduler_id'   => $approver_id,
                    'jadwal_usulan' => $jadwal_usulan,
                    'jam_usulan'    => $jam_usulan,
                    'updated_at'    => date('Y-m-d G:i:s', time())
                );
            $updatePengajuan = parent::save($usulan_id, $dataUpdate, true);
            if($updatePengajuan){
                $tambahkan = self::create($jadwal_usulan,$jam_usulan, $rencana_kerja_id, $flow_mundur, $pesanan_id, $approver_id);
                return true;
            }
        }
        
    }


    public function updateUsulan($usulan_id,$data){
        $update = parent::save($usulan_id, $data, true);
        return $update;
    }

}