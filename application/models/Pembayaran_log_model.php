<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_log_model extends AppModel {

    const HARI = 1;
    const PEKAN = 2;
    const BULAN = 3;
    
    protected $_filter = "";
    protected $_order = "";
    protected $_table = "pembayaran_log";
    // protected $_view = "harga_view";
    protected $_column = "*";
    protected $_join = "";


    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function getPengeluaran($satuan, $start, $end){
        // echo $type."<br>";
        
        switch ($satuan) {
            case self::BULAN :
                $this->_column   = "year(tgl_input) as tahun,month(tgl_input) as bulan,
                                    DATE_FORMAT(tgl_input,'%Y-%m') as tahun_bulan,
                                    sum(nominal) as total_pengeluaran";
                $this->_group_by = "group by year(tgl_input),month(tgl_input)";
                $this->_order_by = "order by year(tgl_input),month(tgl_input)";
                $this->_filter = "and
                                    (tgl_input>='$start' and tgl_input<='$end') ";
            break;
            case self::PEKAN:
                $this->_column   = "CONCAT(year(tgl_input), '-', week(tgl_input,1)) as tahun_pekan,
                                    count(id) as jumlah";
                $this->_group_by = "group by concat(year(tgl_input), '-', week(tgl_input,1))";
                $this->_order_by = "order by concat(year(tgl_input), '-', week(tgl_input,1))";
                $this->_filter = "and
                                    (concat(year(tgl_input), '-', week(tgl_input))>='$start' and concat(year(tgl_input), '-', week(tgl_input))<='$end') ";
            case self::HARI:
                $this->_column   = "tgl_input,
                                    sum(nominal) as total_pengeluaran";
                $this->_group_by = "group by tgl_input";
                $this->_order_by = "order by tgl_input";
                $this->_filter = "and
                                    (tgl_input>='$start' and tgl_input<='$end') ";
            break;
            
            // default:
            //     # code...
            //     break;
        }

        $sql = "select $this->_column
                from $this->_table
                where jenis in(1,5) $this->_filter
                $this->_group_by
                $this->_order_by
                ";
        // if($type!='l'){
            // die($sql);    
        // }
        
        return dbGetRows($sql);

    }

    // Tipe : 1 DEBIT, 2 KREDIT
    // Jenis : Jenis_pembayaran_model
    public function create($data, $user_id, $skip_validation = TRUE, $return = TRUE) {

        $dataInsert = array(
            'nominal'               => $data['nominal'],
            'tipe'                  => $data['tipe'],
            'jenis'                 => $data['jenis'],
            'subjenis'              => $data['subjenis'] ? $data['subjenis'] : 0 ,
            'tgl_input'             => $data['tgl_input'],
            'id_konsumen'           => ($data['id_konsumen']==null || $data['id_konsumen'] == '') ? 0 : $data['id_konsumen'],
            'kode_order'            => $data['kode_order'],
            'keterangan'            => $data['keterangan'],
            'created_at'            => date('Y-m-d G:i:s', time()),
            'created_by'            => $user_id
        );
        
        // Jika pembayaran jenis DP, maka tambah tgl jatuh tempo 30 hari saat input DP
        if ($data['jenis'] == '2') {
            if ($data['id_konsumen'] != null || $data['id_konsumen'] != '') {
                $this->load->model('Pesanan_model');
                $pesanan_id = $this->Pesanan_model->getIdPesananFromIdKonsumen($data['id_konsumen']);
                $this->Pesanan_model->setJatuhTempo($pesanan_id['id']);
            }
        }
        
        // echo "<pre>";print_r($dataInsert);die();

        $create = parent::create($dataInsert, true, true);
        if ($create) {
            return $create;
        }else{
            return false;
        }
    }

    // Tipe : 1 DEBIT, 2 KREDIT
    // Jenis : Jenis_pembayaran_model
    public function updateLog($data){
        $dataUpdate = array(
            'nominal'               => $data['nominal'],
            'tipe'                  => $data['tipe'],
            'jenis'                 => $data['jenis'],
            'tgl_input'             => $data['tgl_input'],
            'id_konsumen'           => $data['id_konsumen'],
            'kode_order'            => $data['kode_order'],
            'keterangan'            => $data['keterangan'],
            'updated_at'            => date('Y-m-d G:i:s', time()),
            'updated_by'            => $data['user_id']
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }
    
    
    public function getLog($bulan, $tahun, $orderby='tgl_input', $by='desc', $filter){
        $sql = "SELECT pembayaran_log.*, jenis_pembayaran.nama AS 'nama_jenis', u1.name AS 'created_by_nama', u2.name AS 'update_by_nama'
                FROM pembayaran_log
                LEFT JOIN jenis_pembayaran ON pembayaran_log.jenis = jenis_pembayaran.id
                LEFT JOIN pesanan ON pembayaran_log.kode_order = pesanan.kode_order
                LEFT JOIN users u1 ON pembayaran_log.created_by = u1.id
                LEFT JOIN users u2 ON pembayaran_log.updated_by = u2.id
            WHERE 1 AND pembayaran_log.is_deleted = '0' and (YEAR(tgl_input) = $tahun AND MONTH(tgl_input) = $bulan) $filter
            order by $orderby $by ";

        // die($sql);
            
        return dbGetRows($sql);
    }

    public function getLogById($id){        
        return dbGetRow("select pl.*, u.name as created_by_nama, u2.name as updated_by_nama 
                        from $this->_table pl 
                        left join users u on u.id=pl.created_by
                        left join users u2 on u.id=pl.updated_by
                        where pl.id = $id");
    }
    
    // Menampilkan record total harga per id pengajuan
    public function getDetailHargaKonsumen($id) {
        $sql = "SELECT
                    ph.id,
                    ph.id_konsumen,
                    k.nama,
                    k.alamat,
                    k.no_hp,
                    p.kode_order,
                    SUM(
                        dp.jumlah * dp.harga_pokok_granit
                    ) AS 'harga_pokok_granit',
                    SUM(
                        dp.jumlah * dp.harga_pokok_keramik
                    ) AS 'harga_pokok_keramik',
                    SUM(
                        dp.jumlah * dp.harga_jual_granit
                    ) AS 'harga_jual_granit',
                    SUM(
                        dp.jumlah * dp.harga_jual_keramik
                    ) AS 'harga_jual_keramik'
                FROM
                    pengajuan_harga ph
                LEFT JOIN konsumen k ON
                    ph.id_konsumen = k.id
                LEFT JOIN detail_pengajuan dp ON
                    ph.id = dp.id_pengajuan_harga
                LEFT JOIN pesanan p ON
                    p.konsumen_id = ph.id_konsumen
                LEFT JOIN pembayaran_log pl ON
                	pl.id_konsumen = ph.id_konsumen
                WHERE
                    ph.is_deleted = '0' AND ph.status = '4' AND ph.id_konsumen <> '0' AND ph.id = '".$id."'
                GROUP BY
                    ph.id";
                    
        return dbGetRows($sql);
    }
    
    // Mendapatkan Hutang Produksi dan Piutang Konsumen per bulan pada param tahun
    // Penjumlahan sudah sesuai dengan jenis (granit/keramik) yang dipilih
    public function getBebanStatistikPerMonth($tahun) {
        $sql = "SELECT
                    MONTH(created_at) AS 'bulan',
                    SUM(IF(jenis='1', harga_setuju_pokok_granit, harga_setuju_pokok_keramik)) AS 'hutang_produksi',
                    SUM(IF(jenis='1', harga_setuju_granit, harga_setuju_keramik) - diskon) AS 'piutang_konsumen',
                    COUNT(id) AS 'jumlah_order'
                FROM
                    pengajuan_harga
                WHERE
                    created_at >= NOW() - INTERVAL 1 YEAR AND is_deleted = '0' AND
                    status = '4' AND YEAR(created_at) = '".$tahun."'
                GROUP BY
                    MONTH(created_at) DESC";
                
        return dbGetRows($sql);
    }
    
    // Mendapatkan PELUNASAN hutang produksi dan piutang konsumen per bulan pada param tahun
    // Penjumlahan sudah sesuai dengan jenis (debit/kredit)
    public function getLunasStatistikPerMonth($tahun) {
        $sql = "SELECT
                    ph.id_konsumen,
                    pl.kode_order,
                    SUM(IF(pl.tipe = '1', nominal, '0')) AS 'kredit',
                    SUM(IF(pl.tipe = '2', nominal, '0')) AS 'debit',
                    MONTH(pl.tgl_input) AS 'bulan',
                    tipe
                FROM
                    pengajuan_harga ph
                LEFT JOIN konsumen k ON
                    ph.id_konsumen = k.id
                LEFT JOIN pembayaran_log pl ON
                    ph.id_konsumen = pl.id_konsumen
                WHERE
                    ph.is_deleted = '0' AND ph.id_konsumen <> '0' AND YEAR(pl.tgl_input) = '".$tahun."'
                GROUP BY MONTH(pl.tgl_input) DESC";
                
        return dbGetRows($sql);
    }
    
    // Ambil detail pelunasan pembayaran dalam bulan tersebut
    public function getDetailLunasPembayaran($bulan, $tahun) {
        // $sql = "SELECT
        //             ph.id,
        //             pl.tgl_input,
        //             ph.id_konsumen,
        //             pl.kode_order,
        //             pl.keterangan,
        //             pl.nominal AS 'kredit'
        //         FROM
        //             pengajuan_harga ph
        //         LEFT JOIN konsumen k ON
        //             ph.id_konsumen = k.id
        //         LEFT JOIN pembayaran_log pl ON
        //             ph.id_konsumen = pl.id_konsumen AND pl.jenis = '6'
        //         WHERE
        //             ph.is_deleted = '0' AND ph.id_konsumen <> '0' AND MONTH(pl.tgl_input) = '".$bulan."' AND YEAR(pl.tgl_input) = '".$tahun."'";
        $sql = "SELECT
                    pl.id,
                    pl.tgl_input,
                    pl.id_konsumen,
                    pl.kode_order,
                    pl.keterangan,
                    pl.nominal AS 'kredit'
                FROM
                    pembayaran_log pl
                WHERE
                    pl.jenis = '6' AND pl.is_deleted = '0' AND MONTH(pl.tgl_input) = '".$bulan."' AND YEAR(pl.tgl_input) = '".$tahun."'";
                    
        return dbGetRows($sql);        
    }
    
    // Ambil detail beban pembayaran dalam bulan tersebut
    public function getDetailBebanPembayaran($bulan, $tahun) {
        $sql = "SELECT
                    ph.id AS 'id_pengajuan',
                    ph.tgl_disetujui,
                    k.id,
                    k.nama,
                    k.alamat,
                    k.no_hp,
                    p.kode_order,
                    p.flow_id,
                    CASE
                        WHEN p.flow_id = 8 THEN 
                            IF(
                                ph.jenis = '1',
                                ph.harga_setuju_pokok_granit,
                                ph.harga_setuju_pokok_keramik
                            )
                        WHEN p.flow_id < 5 THEN 0
                        WHEN p.flow_id IN ('5','6','7') AND pl.nominal > 0 THEN 
                        	IF(
                            	ph.jenis = '1',
                                CAST(
                                    ph.harga_setuju_pokok_granit / 2 AS signed
                                ),
                                CAST(
                                    ph.harga_setuju_pokok_keramik / 2 AS signed
                                )
                            )
                        ELSE 0
                    END AS 'hutang_produksi',
                    IF(
                        ph.jenis = '1',
                        harga_setuju_pokok_granit,
                        harga_setuju_pokok_keramik
                    ) AS 'omzet'
                FROM
                    pengajuan_harga ph
                LEFT JOIN pesanan p ON
                    ph.id_konsumen = p.konsumen_id
                LEFT JOIN pembayaran_log pl ON
                    pl.id_konsumen = ph.id_konsumen AND pl.jenis = '2' AND pl.tipe = '1' AND pl.is_deleted = '0'
                LEFT JOIN konsumen k ON
                    ph.id_konsumen = k.id
                WHERE
                    ph.is_deleted = '0'  AND ph.status = '4' AND ph.id_konsumen <> '0' AND MONTH(p.tgl_order_masuk) = '".$bulan."' AND YEAR(p.tgl_order_masuk) = '".$tahun."'";
        // echo "<pre>";var_dump($sql);
        // die();
        return dbGetRows($sql);  
    }
    
    // Menampilkan data pembayaran per order
    public function getListStatusPiutangKonsumen($bulan, $tahun, $role, $user_id) {
        // if ($bulan == '8') {
        //     $bulan = date('m');
        // }
        
        // if ($tahun == '') {
        //     $tahun = date('Y');
        // }

        if($role == Role::MITRA_MARKETING){
            $filter_role = " and ph.user_id_pengaju=".$user_id;
        }else{
            $filter_role = "";
        }
        
        $sql = "SELECT
                    ph.id AS 'id_pengajuan',
                    p.tgl_order_masuk,
                    p.kode_order,
                    k.nama,
                    k.alamat,
                    k.no_hp,
                    p.flow_id,
                    ph.jenis,
                    ph.tgl_disetujui,
                    (IF(
                        ph.jenis = '1',
                        ph.harga_setuju_granit,
                        ph.harga_setuju_keramik
                    )-ph.diskon) AS 'harga_jual',
                    COALESCE(SUM(pl1.nominal),
                    0) AS 'dp',
                    COALESCE(SUM(pl2.nominal),
                    0) AS 'pelunasan',
                    COALESCE(SUM(pl3.nominal),
                    0) AS 'tanda_jadi',
                    COALESCE(SUM(pl1.nominal),
                    0) + COALESCE(SUM(pl2.nominal),
                    0) + COALESCE(SUM(pl3.nominal),
                    0) AS 'total_pembayaran',
                    IF(
                        ph.jenis = '1',
                        ph.harga_setuju_granit,
                        ph.harga_setuju_keramik
                    ) -(
                        COALESCE(SUM(pl1.nominal),
                        0) + COALESCE(SUM(pl2.nominal),
                        0) + COALESCE(SUM(pl3.nominal),
                        0)
                    ) - ph.diskon AS 'sisa_pembayaran',
                    pl1.tgl_input AS 'tgl_dp',
                    pl2.tgl_input AS 'tgl_pelunasan',
                    pl3.tgl_input AS 'tgl_tanda_jadi'
                FROM
                    pengajuan_harga ph
                LEFT JOIN konsumen k ON
                	k.id = ph.id_konsumen
                LEFT JOIN pembayaran_log pl1 ON
                    pl1.id_konsumen = ph.id_konsumen AND pl1.jenis = '2' AND pl1.tipe = '1'
                LEFT JOIN pembayaran_log pl2 ON
                    pl2.id_konsumen = ph.id_konsumen AND pl2.jenis = '3' AND pl2.tipe = '1'
                LEFT JOIN pembayaran_log pl3 ON
                    pl3.id_konsumen = ph.id_konsumen AND pl3.jenis = '4' AND pl3.tipe = '1'
                LEFT JOIN pesanan p ON
                    p.konsumen_id = ph.id_konsumen
                WHERE
                    ph.is_deleted = '0' AND ph.id_konsumen <> '0' AND ph.status = '4'  AND MONTH(p.tgl_order_masuk) = '".$bulan."' AND YEAR(p.tgl_order_masuk) = '".$tahun."' ".$filter_role."
                GROUP BY
                    ph.id DESC
                LIMIT 100";
        // die(nl2br($sql));        
        return dbGetRows($sql); 
    }


    // Menampilkan data pembayaran tiap konsumen
    public function getPembayaranLogByKonsumen($konsumen_id) {
        // if ($bulan == '8') {
        //     $bulan = date('m');
        // }
        
        // if ($tahun == '') {
        //     $tahun = date('Y');
        // }
        
        $sql = "SELECT
                    ph.id AS 'id_pengajuan',
                    p.tgl_order_masuk,
                    p.kode_order,
                    k.nama,
                    k.alamat,
                    k.no_hp,
                    p.flow_id,
                    ph.jenis,
                    ph.tgl_disetujui,
                    (IF(
                        ph.jenis = '1',
                        ph.harga_setuju_granit,
                        ph.harga_setuju_keramik
                    )-ph.diskon) AS 'harga_jual',
                    COALESCE(SUM(pl1.nominal),
                    0) AS 'dp',
                    COALESCE(SUM(pl2.nominal),
                    0) AS 'pelunasan',
                    COALESCE(SUM(pl3.nominal),
                    0) AS 'tanda_jadi',
                    COALESCE(SUM(pl1.nominal),
                    0) + COALESCE(SUM(pl2.nominal),
                    0) + COALESCE(SUM(pl3.nominal),
                    0) AS 'total_pembayaran',
                    IF(
                        ph.jenis = '1',
                        ph.harga_setuju_granit,
                        ph.harga_setuju_keramik
                    ) -(
                        COALESCE(SUM(pl1.nominal),
                        0) + COALESCE(SUM(pl2.nominal),
                        0) + COALESCE(SUM(pl3.nominal),
                        0)
                    ) - ph.diskon AS 'sisa_pembayaran',
                    pl1.tgl_input AS 'tgl_dp',
                    pl2.tgl_input AS 'tgl_pelunasan',
                    pl3.tgl_input AS 'tgl_tanda_jadi'
                FROM
                    pengajuan_harga ph
                LEFT JOIN konsumen k ON
                    k.id = ph.id_konsumen
                LEFT JOIN pembayaran_log pl1 ON
                    pl1.id_konsumen = ph.id_konsumen AND pl1.jenis = '2' AND pl1.tipe = '1'
                LEFT JOIN pembayaran_log pl2 ON
                    pl2.id_konsumen = ph.id_konsumen AND pl2.jenis = '3' AND pl2.tipe = '1'
                LEFT JOIN pembayaran_log pl3 ON
                    pl3.id_konsumen = ph.id_konsumen AND pl3.jenis = '4' AND pl3.tipe = '1'
                LEFT JOIN pesanan p ON
                    p.konsumen_id = ph.id_konsumen
                WHERE
                    ph.is_deleted = '0' AND ph.id_konsumen <> '0' AND ph.status = '4' and ph.id_konsumen=$konsumen_id
                GROUP BY
                    ph.id DESC
                LIMIT 100";
        // die(nl2br($sql));        
        return dbGetRow($sql); 
    }
    
    public function getSumPiutangKonsumenPerBulan($tahun) {
        $sql = "SELECT
                    MONTH(p.tgl_order_masuk) AS 'bulan',
                    SUM(
                        (
                            IF(
                                ph.jenis = '1',
                                ph.harga_setuju_granit,
                                ph.harga_setuju_keramik
                            ) - ph.diskon
                        )
                    ) AS 'harga_jual',
                    SUM(
                        IF(
                            ph.jenis = '1',
                            ph.harga_setuju_granit,
                            ph.harga_setuju_keramik
                        )
                    ) -(
                        COALESCE(SUM(pl1.nominal),
                        0) + COALESCE(SUM(pl2.nominal),
                        0) + COALESCE(SUM(pl3.nominal),
                        0)
                    ) - SUM(ph.diskon) AS 'sisa_pembayaran'
                FROM
                    pengajuan_harga ph
                LEFT JOIN konsumen k ON
                    k.id = ph.id_konsumen
                LEFT JOIN pembayaran_log pl1 ON
                    pl1.id_konsumen = ph.id_konsumen AND pl1.jenis = '2' AND pl1.tipe = '1'
                LEFT JOIN pembayaran_log pl2 ON
                    pl2.id_konsumen = ph.id_konsumen AND pl2.jenis = '3' AND pl2.tipe = '1'
                LEFT JOIN pembayaran_log pl3 ON
                    pl3.id_konsumen = ph.id_konsumen AND pl3.jenis = '4' AND pl3.tipe = '1'
                LEFT JOIN pesanan p ON
                    p.konsumen_id = ph.id_konsumen
                WHERE
                    ph.is_deleted = '0' AND ph.id_konsumen <> '0' AND ph.status = '4' AND YEAR(p.tgl_order_masuk) = '".$tahun."'
                GROUP BY
                    MONTH(p.tgl_order_masuk)
                HAVING
                    SUM(
                        IF(
                            ph.jenis = '1',
                            ph.harga_setuju_granit,
                            ph.harga_setuju_keramik
                        )
                    ) -(
                        COALESCE(SUM(pl1.nominal),
                        0) + COALESCE(SUM(pl2.nominal),
                        0) + COALESCE(SUM(pl3.nominal),
                        0)
                    ) - SUM(ph.diskon) > 0";
        // die(nl2br($sql));         
        return dbGetRows($sql); 
    }
    
    // Mendapatkan rincian hutang produksi per Kode order
    public function getListHutangProduksi($bulan, $tahun) {
        $sql = "SELECT
                    p.kode_order,
                    p.flow_id,
                    ph.tgl_disetujui,
                    IF(
                        ph.jenis = '1',
                        ph.harga_setuju_pokok_granit,
                        ph.harga_setuju_pokok_keramik
                    ) AS 'harga_pokok',
                    COALESCE(SUM(pl1.nominal),0) AS 'pelunasan',
                    IF(
                        ph.jenis = '1',
                        ph.harga_setuju_pokok_granit,
                        ph.harga_setuju_pokok_keramik
                    ) - COALESCE(SUM(pl1.nominal),0) AS 'sisa_pembayaran'
                FROM
                    pengajuan_harga ph
                LEFT JOIN pembayaran_log pl1 ON
                    pl1.id_konsumen = ph.id_konsumen AND pl1.jenis = '6' AND pl1.tipe = '2'
                LEFT JOIN pesanan p ON
                	p.konsumen_id = ph.id_konsumen
                WHERE
                    ph.is_deleted = '0' AND ph.id_konsumen <> '0' AND ph.status = '4' AND MONTH(p.tgl_order_masuk) = '".$bulan."' AND YEAR(p.tgl_order_masuk) = '".$tahun."'
                GROUP BY
                    pl1.kode_order
                LIMIT 100";
                
        return dbGetRows($sql); 
    }

    // Mendapatkan hutang produksi per Bulan dan pembayaran hutang yg dilakukan sesuai dengan tgl input
    // Jika flow_id = 8 maka hutang produksi 1.0 x harga_setuju
    // Jika flow_id < 7 maka hutang produksi 0.5 x harga_setuju
    public function getHutangProduksiPerBulan($bulan, $tahun) {
        $sql = "SELECT
                    MONTH(p.tgl_order_masuk) AS 'bulan',
                    SUM(
                        IF(
                            ph.jenis = '1',
                            ph.harga_setuju_pokok_granit,
                            ph.harga_setuju_pokok_keramik
                        )
                    ) AS 'omzet',
                    SUM(
                        CASE WHEN p.flow_id = 8 THEN 
                        IF(
                            ph.jenis = '1',
                            ph.harga_setuju_pokok_granit,
                            ph.harga_setuju_pokok_keramik
                        ) 
                        WHEN p.flow_id < 5 THEN 0 
                        WHEN p.flow_id IN('5', '6', '7') AND pl.nominal > 0 THEN 
                            CAST(IF( 
                                ph.jenis = '1',
                                ph.harga_setuju_pokok_granit / 2,
                                ph.harga_setuju_pokok_keramik / 2
                            ) as signed)
                        ELSE 0
                    END ) AS 'hutang_produksi',
                ifnull((
                    SELECT
                        SUM(pl.nominal)
                    FROM
                        pembayaran_log pl
                    WHERE
                        pl.jenis = '6' AND MONTH(pl.tgl_input) = MONTH(p.tgl_order_masuk)
                ),0) AS 'pelunasan'
                FROM
                    pengajuan_harga ph
                LEFT JOIN pesanan p ON
                    ph.id_konsumen = p.konsumen_id
                LEFT JOIN pembayaran_log pl ON
                    pl.id_konsumen = ph.id_konsumen AND pl.jenis = '2' AND pl.tipe = '1' AND pl.is_deleted = '0'
                WHERE
                    ph.status = '4' AND ph.is_deleted = '0' AND YEAR(p.tgl_order_masuk) = '".$tahun."'
                GROUP BY
                    MONTH(p.tgl_order_masuk)";
        // die($sql);        
        return dbGetRows($sql); 
    }
    
    // 19 NOVEMBER 2020
    public function getOmzet($user, $bulan, $tahun) {
        $sql = "SELECT
                    MONTH(ph.tgl_disetujui) AS 'bulan',
                    SUM(
                        IF(
                            ph.jenis = '1',
                            ph.harga_setuju_granit,
                            ph.harga_setuju_keramik
                        )
                    ) AS 'omzet'
                FROM
                    pengajuan_harga ph
                WHERE
                    is_deleted = '0' AND jenis <> '0' AND
                STATUS
                    = '4' AND id_konsumen <> '0' AND MONTH(ph.tgl_disetujui) = '".$bulan."' AND YEAR(ph.tgl_disetujui) = '".$tahun."'
                GROUP BY
                    MONTH(ph.tgl_disetujui)";
                    
        $rows = dbGetRow($sql);
    	return $rows;
    }

    // DEV
    // DEV
    public function createDev($data, $user_id, $skip_validation = TRUE, $return = TRUE) {

        $dataInsert = array(
            'nominal'               => $data['nominal'],
            'tipe'                  => $data['tipe'],
            'jenis'                 => $data['jenis'],
            'tgl_input'             => $data['tgl_input'],
            'id_konsumen'           => ($data['id_konsumen']==null || $data['id_konsumen'] == '') ? 0 : $data['id_konsumen'],
            'kode_order'            => $data['kode_order'],
            'keterangan'            => $data['keterangan'],
            'created_at'            => date('Y-m-d G:i:s', time()),
            'created_by'            => $user_id
        );
        
        // Jika pembayaran jenis DP, maka tambah tgl jatuh tempo 30 hari saat input DP
        if ($data['jenis'] == '2') {
            if ($data['id_konsumen'] != null || $data['id_konsumen'] != '') {
                $this->load->model('Pesanan_model');
                $pesanan_id = $this->Pesanan_model->getIdPesananFromIdKonsumen($data['id_konsumen']);
                die($pesanan_id['id']);
                $this->Pesanan_model->setJatuhTempo($pesanan_id['id']);
            }
        }
        
        echo "<pre>";print_r($dataInsert);die();

        $create = parent::create($dataInsert, true, true);
        if ($create) {
            return $create;
        }else{
            return false;
        }
    }
}