<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tujuan_model extends AppModel {

    private $limit = 50;
    private $table = 'tujuan';

    protected $label = array(
        'id' => 'ID',
        'name' => 'Nama',
        'urut' => 'Urut',
    );
    protected $validation = array(
        'name' => 'required',
    );

    function getList($page=1) {
        $page = $this->limit*($page-1);
        $sql = "select * from $this->table order by urut limit $this->limit offset $page";
        return dbGetRows($sql);
    }

    function getRow($id) {
        $sql = "select * from $this->table where id=$id";
        return dbGetRow($sql);
    }

}
