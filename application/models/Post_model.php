<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post_model extends AppModel {

    public $limit = 10;

    //TYPE
    const TYPE_PRIVATE = 'V';
    const TYPE_PUBLIC = 'C';

    // const TYPE_FRIEND = 'F';
    // const TYPE_KNOWLEDGE = 'K';
    // const TYPE_DISCLAIMER = 'D';
    // const TYPE_INFORMATION = 'I';
    // const TYPE_GROUP = 'G';
    const POST_TICKET = 'T';
    const PRIVATE_CHANGED = 'PC';
    const DINAS_CHANGED = 'DC';
    const OPERATOR_CHANGED = 'OC';
    const STATUS_CHANGED = 'SC';
    const UPVOTE = 'U';

    const STATUS_DILAPORKAN = 'L';
    const STATUS_RESPON = 'R';
    const STATUS_OPEN = 'O';
    const STATUS_PROSES = 'P';
    const STATUS_SELESAI = 'S';

    public $limit_quota = 3;   
    protected $_filter = "";
    protected $_order = "";
    protected $_table = "posts";
    protected $_column = "*";
    protected $_join = "";

    protected $has_many = array(
        'Comment',
    );
    protected $belongs_to = array(
        'User',
        'Group',
        'Category',
        'Updated_by' => array('primary_key' => 'userIdUpdated', 'model' => 'User_model')
    );
    protected $many_to_many = array(
        'Post_tag' => array('Post', 'Tag')
    );
    protected $label = array(
        'category_id' => 'Kategori',
        'image' => 'Gambar',
        'file' => 'Berkas',
        'video' => 'Video'
    );
    protected $validation = array(
        'link' => 'valid_url',
        'category_id' => 'required|integer'
    );

    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function resetVariables(){
        $this->_filter = '';
        $this->column = '*';
        $this->_join = '';
        $this->_show_sql = false;
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('Category_model');
    }


    function getCount() {
        $sql = "select count(*) from $this->table {$this->filter}";
        return dbGetOne($sql);
    }
    
    public function getPosts($user_id,$page = 0, $mine=false) {
        $ciConfig = $this->config->item('utils');
        $sql = "select p.*, u.name as nama_dinas, '".$ciConfig['full_upload_dir_ip'].'posts/photos/'."medium-'||MD5(MD5(CAST (p.user_id AS character varying)||'".$this->config->item('encryption_key')."')) ||'-'||p.file as nama_file,p.file,
            us.name as nama_user, us.photo as foto_user,
            case
                when us.photo is null then  '".$ciConfig['full_upload_dir_ip']."users/photos/nopic.png'
                else '".$ciConfig['full_upload_dir_ip'].'posts/photos/'."thumb-'||MD5(CAST(p.user_id AS character varying)) ||'-'||p.file
            end as foto_profil,
            p.vote as vote,
            pv.id as vote_exist,
            count(case when c.type='K' then c.id end) as jumlah_komentar
            from posts p
            left join comments c on p.id = c.post_id
            left join unitkerja u on p.dinas_id = u.idunit
            left join users us on p.user_id = us.id
            left join post_vote pv on p.id = pv.post_id and pv.user_id=".$user_id."
            where p.user_id=$user_id";
        if($mine == false){
            $sql .=" or (is_private=0 and status!='".self::STATUS_DILAPORKAN."') ";
        }
        $page = $page*10;
        $sql .= "group by p.id,u.name,us.name,us.photo,pv.id order by id desc limit 10 offset $page";
        $post = dbGetRows($sql);
        return $post;

    }

    public function getPostBy($column, $value){
        if ($column=='user_id'){
            $value="'".$value."'";
        }
        $sql = "select * from posts p where p.$column=$value";
        $post = dbGetRow($sql);
        return $post;
    }

    public function notRatedPosts($user_id,$page = 0) {
        $CI = & get_instance();

        $sql='';
        $offset = $page * 10;
        $order = ' order by post_id desc ';
        
            
        $sql .= "select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, p.status as post_status, p.kode as kode_ticket, p.type as post_type,
            u.name as user_name, u.photo as user_photo, g.id as group_id,
            g.name as group_name, g.initial as group_initial,null as group_cover,
            c.comment_number, v.view_number,
            o.nm_lemb, p.is_sticky, pr.rating
            from posts p
            join users u on u.id=p.user_id
            left join groups g on g.id=p.group_id
            left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
            left join organisasi o on o.id_organisasi=p.id_organisasi
            left join post_rating pr on pr.post_id=p.id and pr.user_id=p.user_id
            where p.user_id='$user_id' and p.type='T' and p.status='S' and pr.rating is null ";

        $next_page_sql = $sql;
        $sql .="
            order by post_id desc
            limit 10 offset $offset";
        $rows = dbGetRows($sql);

        $offset=($page+1)*10;
        $next_page_sql .="
            order by post_id desc
            limit 10 offset $offset";

        $data_next_page = dbGetRows($next_page_sql);
        $ids = array();
        foreach ($rows as $key => $row) {
            $ids[] = $row['post_id'];
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
            if (empty($data_next_page)) {
                $rows[$key]['next_page_post']=0;
            } else {
                $rows[$key]['next_page_post']=1;
            }
        }

        $str_ids = implode(',', $ids);
        if ($str_ids) {
            $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $images = array();

            foreach ($temp as $d) {

                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                $images[$d['post_id']][$i]['thumb'] = Image::createLink($id, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['medium'] = Image::createLink($id, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['original'] = Image::createLink($id, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
                $i++;
            }

            $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $files = array();

            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                // $this->checkCreateFile($id, $d['post_id'], $d['file'], 'posts/files');
                $files[$d['post_id']][$i] = File::createLink($id, $d['file'], 'posts/files');
                $i++;
            }

            $sql = "select a.*, u.name from post_assign a join users u on a.user_id=u.id where post_id in ($str_ids)";
            $temp2 = dbGetRows($sql);
            $assigns = array();
            foreach ($temp2 as $d) {
                $d['user_photo'] = $this->User_model->getUserPhoto($d['user_id'],$d['user_photo']);
                $assigns[$d['post_id']][] = $d;
            }
        }
        
        foreach ($rows as &$row) {
            $row['images'] = $images[$row['post_id']];
            $row['files'] = $files[$row['post_id']];
            $row['assigns'] = $assigns[$row['post_id']];
        }
        return $rows;
    }

    public function getDoneTicket($user_id=NULL, $group_id=NULL, $page = 0, $start=NULL, $end=NULL) {
        $CI = & get_instance();
        $group_default = $this->Group_model->getGroupDefault();
        $group_default_id = $group_default['id'];
        $group_default_name = $group_default['name'];
        $group_default_initial = $group_default['initial'];
        $sql='';
        $offset = $page * 10;
        $order = ' order by post_id desc ';
        $filter_user='';
        if ($user_id!=NULL){
            $filter_user = " and p.user_id='$user_id' ";
            $join_assigns="left join (select a.* from post_assign a join users u on a.user_id=u.id where u.id='$user_id') pa on pa.post_id=p.id";
            $filter1 = " and ( p.id in (pa.post_id)) ";
        }
        if ($group_id!=NULL) {
            $filter_group = " and p.group_id=$group_id " ;
        }
        if (!empty($start) and !empty($end)){
            $filter_post = " and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
        }
        
        if ($group_default['id']==$group_id || $group_id==NULL) {
            $sql .= "select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, p.status as post_status, p.kode as kode_ticket, p.type as post_type,
            u.name as user_name, u.photo as user_photo, $group_default_id as group_id,
            '$group_default_name' as group_name, '$group_default_initial' as group_initial,null as group_cover,
            c.comment_number, v.view_number,
            o.nm_lemb, p.is_sticky, pr.rating
            from posts p
            join users u on u.id=p.user_id
            left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
            left join organisasi o on o.id_organisasi=p.id_organisasi
            left join post_rating pr on pr.post_id=p.id and pr.user_id=p.user_id
            $join_assigns
            where p.type='T' $filter1 and p.status='S' $filter_post and p.group_id is null
            union ";
        }
        $sql .= "
            select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, p.status as post_status, p.kode as kode_ticket, p.type as post_type,
            u.name as user_name, u.photo as user_photo, g.id as group_id,
            g.name as group_name, g.initial as group_initial, null as group_cover,
            c.comment_number, v.view_number,
            o.nm_lemb, p.is_sticky, pr.rating
            from posts p
            join users u on u.id=p.user_id
            join groups g on g.id=p.group_id
            left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
            left join organisasi o on o.id_organisasi=p.id_organisasi
            left join post_rating pr on pr.post_id=p.id and pr.user_id=p.user_id
            $join_assigns
            where p.type='T' $filter1 and p.status='S' $filter_post $filter_group ";

        $next_page_sql = $sql;
        $sql .="
            order by post_id desc
            limit 10 offset $offset";
        $rows = dbGetRows($sql);

        $offset=($page+1)*10;
        $next_page_sql .="
            order by post_id desc
            limit 10 offset $offset";
        // echo nl2br($sql);
        // die();
        $data_next_page = dbGetRows($next_page_sql);
        $ids = array();
        foreach ($rows as $key => $row) {
            $ids[] = $row['post_id'];
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
            if (empty($data_next_page)) {
                $rows[$key]['next_page_post']=0;
            } else {
                $rows[$key]['next_page_post']=1;
            }
        }

        $str_ids = implode(',', $ids);
        if ($str_ids) {
            $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $images = array();

            foreach ($temp as $d) {

                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                $images[$d['post_id']][$i]['thumb'] = Image::createLink($id, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['medium'] = Image::createLink($id, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['original'] = Image::createLink($id, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
                $i++;
            }

            $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $files = array();

            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                // $this->checkCreateFile($id, $d['post_id'], $d['file'], 'posts/files');
                $files[$d['post_id']][$i] = File::createLink($id, $d['file'], 'posts/files');
                $i++;
            }

            $sql = "select a.*, u.name, u.photo as user_photo from post_assign a join users u on a.user_id=u.id where post_id in ($str_ids)";
            $temp2 = dbGetRows($sql);
            $assigns = array();
            foreach ($temp2 as $d) {
                $d['user_photo'] = $this->User_model->getUserPhoto($d['user_id'],$d['user_photo']);
                $assigns[$d['post_id']][] = $d;
            }
        }
        
        foreach ($rows as &$row) {
            $row['images'] = $images[$row['post_id']];
            $row['files'] = $files[$row['post_id']];
            $row['assigns'] = $assigns[$row['post_id']];
        }

        // reset variable
        return $rows;
    }

    public function getKnowledges($user_id,$chosen_helpdesk=NULL,$page = 0, $param = null, $param_tag=NULL) {
        $CI = & get_instance();
        $offset = $page * 10;
        $filter_group = $filter1 = $filter2 = '';
        $group_default = $this->Group_model->getGroupDefault();
        $group_default_id = $group_default['id'];
        $group_default_name = $group_default['name'];
        $my_helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup($user_id);
        $my_helpdesk_group_id = $my_helpdesk_group['id'];
        if ($this->User_model->isOperator($user_id)) {
            $filter1 = " and p.user_id='$user_id' ";
        }
        if (!empty($param)) {
            $filter2 .= " and (u.name ilike '%" . $param . "%' or p.description ilike '%" . $param . "%')";
        }
        if ($param_tag!=NULL) {
            $tag_post_ids = $this->checkPostTag($param_tag);
            $post_ids = array();
            foreach ($tag_post_ids as $key => $value) {
                $post_ids[] = $value['post_id'];
            }
            $post_ids = implode(',',$post_ids);
            $filter_tag = " and p.id in ($post_ids) " ;
        }

        $sql='';
        //if ($this->User_model->isOperator($user_id) or $this->User_model->isSuperAdministrator($user_id)){
        if ($chosen_helpdesk==NULL) {
            $filter_unit = '';
        } else {
            $filter_unit = " g.name='$chosen_helpdesk' and ";
        }

        $subquery = "select * from (";
        $subquery_end = " 
            order by post_id desc
            limit 10 offset $offset  ) x ";
        $subquery_end_union = " 
            order by post_id desc
            limit 10 offset $offset ) un ";

        $sql.= "
        $subquery
        select * from (select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, p.group_id as group_id,g.name as group_name, g.initial as group_initial,
            p.type as post_type, v.view_number, c.comment_number,
            u.name as user_name, u.photo as user_photo, p.is_sticky, (case when (avg(coalesce(pr.rating,0))*10)%10>5 then ceil(avg(coalesce(pr.rating,0))) else floor(avg(coalesce(pr.rating,0))) end) as rating
            from posts p
            left join groups g on g.id=p.group_id
            join users u on u.id=p.user_id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
            left join (select post_id, count(*) as comment_number from comments group by post_id) c on c.post_id=p.id 
            left join post_rating pr on pr.post_id=p.id

            where $filter_unit p.type in ('K') and p.group_id is not null and (p.status != 'D' and (p.is_delete!=1 or p.is_delete is null)) 
            $filter2 $filter_tag
            group by p.id, p.description, p.title, p.user_id, p.created_at, p.type, v.view_number, c.comment_number, g.name,
            u.name, u.photo, p.is_sticky, g.initial
            $subquery_end
            ";
        if (1==0 and ($this->User_model->isOperator($user_id) || $group_default_id==$my_helpdesk_group['id'] || $this->User_model->isSuperAdministrator($user_id)) and ($chosen_helpdesk==NULL || $chosen_helpdesk==$group_default_name)) {
            $sql .= "union 
            $subquery
                select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, $group_default_id as group_id,'$group_default_name' as group_name, '$group_default_initial' as group_initial,
            p.type as post_type, v.view_number, c.comment_number,
            u.name as user_name, u.photo as user_photo, p.is_sticky, (case when (avg(coalesce(pr.rating,0))*10)%10>5 then ceil(avg(coalesce(pr.rating,0))) else floor(avg(coalesce(pr.rating,0))) end) as rating
            from posts p
            join users u on u.id=p.user_id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
            left join (select post_id, count(*) as comment_number from comments group by post_id) c on c.post_id=p.id 
            left join post_rating pr on pr.post_id=p.id
            where p.type in ('K') and p.group_id is null and (p.status != 'D' and (p.is_delete!=1 or p.is_delete is null)) 
            $filter2 $filter_tag
            group by p.id, p.description, p.title, p.user_id, p.created_at, p.type, v.view_number, c.comment_number,
            u.name, u.photo, p.is_sticky 
            $subquery_end_union
             ";
        }
        $sql .=") km 
            where km.group_id is not null
            ";
        $next_page_sql = $sql;
        $sql .="
            order by post_id desc
            limit 11 offset $offset";
        $rows = dbGetRows($sql);
        // $offset=($page+1)*10;
        // $next_page_sql .="
        //     order by post_id desc
        //     limit 10 offset $offset";
        
        // $data_next_page = dbGetRows($next_page_sql);
        // if ($_SESSION['sigap']['auth']['username']=='integra'){
        //     echo nl2br($sql);
        //     die();
        // }
        // echo nl2br($sql);
        // die();

        $ids = array();
        foreach ($rows as $key => $row) {
            $rows[$key]['hak_akses'] = $this->getHakAkses($row,$user_id);
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
            $ids[] = $row['post_id'];
            $rows[$key]['tag'] = $rows[$key]['tags'] = $this->getPostTag($row['post_id']);
            if (!isset($rows[10])) {
                $rows[$key]['next_page_knowledge']=0;
            } else {
                $rows[$key]['next_page_knowledge']=1;
            }

            if ($key==10){
                unset($rows[$key]);
            }
        }

        $str_ids = implode(',', $ids);
        if ($str_ids) {
            $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $images = array();
            $folder = 'posts/photos';
            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                // $this->checkCreateImage($id, $d['post_id'], $d['image'], $folder);
                $images[$d['post_id']][$i]['thumb'] = Image::createLink($id, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['medium'] = Image::createLink($id, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['original'] = Image::createLink($id, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
                $i++;
            }

            $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $files = array();

            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                // $this->checkCreateFile($id, $d['post_id'], $d['file'], 'posts/files');
                $files[$d['post_id']][$i] = File::createLink($id, $d['file'], 'posts/files');
                $i++;
            }

        }

        foreach ($rows as &$row) {
            $row['images'] = $images[$row['post_id']];
            $row['files'] = $files[$row['post_id']];
        }
        // echo '<pre>';
        // var_dump($rows);
        // die();
        return $rows;
    }

    public function getKnowledgesByTag($user_id,$page = 0, $param = null) {
        $CI = & get_instance();
        
        $offset = $page * 10;
        $filter_group = $filter1 = $filter2 = '';

        if ($this->User_model->isOperator($user_id)) {
            $filter1 = " and p.user_id='$user_id' ";
        }
        if (!empty($param)) {
            // $filter2 .= " and (u.name ilike '%" . $param . "%' or p.description ilike '%" . $param . "%')";
            $filter2 .= " and p.id in (select post_id from post_tags where tag_id=(select id from tags where name='".$param."'))";
        }

            
        $sql = "select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, 
            p.type as post_type, v.view_number,
            u.name as user_name, u.photo as user_photo, p.is_sticky, (case when (avg(coalesce(pr.rating,0))*10)%10>5 then ceil(avg(coalesce(pr.rating,0))) else floor(avg(coalesce(pr.rating,0))) end) as rating
            from posts p
            join users u on u.id=p.user_id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
            left join post_rating pr on pr.post_id=p.id
            where p.type in ('K') and (p.status != 'D' and (p.is_delete!=1 or p.is_delete is null)) $filter2 ";
        $next_page_sql = $sql;
        $sql .="
            group by p.id, p.description, p.title, p.user_id, p.created_at, p.type, v.view_number,
            u.name, u.photo, p.is_sticky
            order by post_id desc
            limit 10 offset $offset";
        // die($sql);
        $rows = dbGetRows($sql);

        $offset=($page+1)*10;
        $next_page_sql .="
            group by p.id, p.description, p.title, p.user_id, p.created_at, p.type, v.view_number,
            u.name, u.photo, p.is_sticky
            order by post_id desc
            limit 10 offset $offset";
        
        $data_next_page = dbGetRows($next_page_sql);
        if (empty($data_next_page)) {
            $rows['next_page_knowledge']=0;
        } else {
            $rows['next_page_knowledge']=1;
        }

        $ids = array();
        foreach ($rows as $key => $row){
            $rows[$key]['tag'] = $rows[$key]['tags'] = $this->getPostTag($row['id']);
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
            $ids[] = $row['post_id'];
        }

        $str_ids = implode(',', $ids);
        if ($str_ids) {
            $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $images = array();

            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                $images[$d['post_id']][$i]['thumb'] = Image::createLink($id, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['medium'] = Image::createLink($id, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['original'] = Image::createLink($id, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
                $i++;
            }

            $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $files = array();

            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                // $this->checkCreateFile($id, $d['post_id'], $d['file'], 'posts/files');
                $files[$d['post_id']][$i] = File::createLink($id, $d['file'], 'posts/files');
                $i++;
            }

        }

        foreach ($rows as &$row) {
            $row['images'] = $images[$row['post_id']];
            $row['files'] = $files[$row['post_id']];
        }

        return $rows;
    }

    public function getPostTag($postid){
        $sql="select t.name from post_tags pt
            join tags t on pt.tag_id=t.id 
            where pt.post_id=$postid";
        $rows = dbGetRows($sql);
        
        if(!$rows)
            $rows = array();
        
        return $rows;
    }

    public function checkPostTag($param){
        $sql = " select pt.post_id from post_tags pt
            join tags t on pt.tag_id=t.id 
            where t.name='$param'";
        $rows =dbGetRows($sql);
        return $rows;

    }

    function isGroupMember($group_name,$user_id) {
        $CI = & get_instance();
        $sql = "select 1 from group_members 
                    left join groups on groups.name=$group_name
                    where group_id=id and user_id='$user_id'" ;
        $row = dbGetRow($sql);        
        return $row;
    }

    function getPost($user_id,$id, $show_log=NULL) {        
        $CI = & get_instance();
        $group_default = $this->Group_model->getGroupDefault($user_id);
        $group_default_id = $group_default['id'];
        $group_default_name = $group_default['name'];
        $group_default_initial = $group_default['initial'];
        $sql = "select  p.id as post_id, 
                        p.description as post_description, 
                        p.title as post_title, 
                        p.user_id as post_user_id, 
                        p.created_at as post_created_at, 
                        p.status as post_status, 
                        p.kode as kode_ticket, 
                        p.type as post_type, 
                        COALESCE(g.id,$group_default_id) as group_id,
                        COALESCE(g.name,'$group_default_name') as group_name, 
                        CASE WHEN (g.id is not null) THEN g.initial ELSE 'PUS' END as group_initial,
                        u.name as user_name, 
                        u.role as user_role, 
                        u.photo as user_photo,
                        c.comment_number, 
                        v.view_number,
                        o.nm_lemb,
                        p.is_sticky, 
                        pr.rating
                from posts p 
                join users u on u.id=p.user_id
                left join groups g on g.id=p.group_id
                left join group_members b on g.id=b.group_id and b.user_id='$user_id'
                left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
                left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
                left join organisasi o on o.id_organisasi=p.id_organisasi
                left join post_rating pr on pr.post_id=p.id and pr.user_id=p.user_id
                where p.id=$id and p.type!='I' and p.type!='K' and (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) " ;
        $row = dbGetRow($sql);
        $row['youtube'] = $this->getYoutubeURL($row['post_description']);
        $row['hak_akses'] = $this->getHakAkses($row,$user_id);
        $row['tags'] = $row['tag'] = $this->getPostTag($row['post_id']);
        $row['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
        $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id=$id";
        $temp = dbGetRows($sql);

        $images = array();
        $folder = 'posts/photos';
        // $i=0;
        foreach ($temp as $d) {
            $id_image = md5($d['user_id'] . $CI->config->item('encryption_key'));
            // $this->checkCreateImage($id_image, $d['post_id'], $d['image'], $folder);
            $images[$d['post_id']][$i]['thumb'] = Image::createLink($id_image, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
            $images[$d['post_id']][$i]['medium'] = Image::createLink($id_image, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
            $images[$d['post_id']][$i]['original'] = Image::createLink($id_image, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
            $i++;
        }
        $row['images'] = $images[$id];
        // echo '<pre>';
        // var_dump($id_image);
        // die();
        $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id=$id";
        $temp = dbGetRows($sql);
        $files = array();

        foreach ($temp as $d) {
            $id_file = md5($d['user_id'] . $CI->config->item('encryption_key'));
            // $this->checkCreateFile($id_file, $d['post_id'], $d['file'], 'posts/files');
            $files[$d['post_id']][$i] = File::createLink($id_file, $d['file'], 'posts/files');
            $i++;
        }
        $row['files'] = $files[$id];
        if (empty($show_log)){
            $filter_comment = " and c.candelete=1";
        }

        /**
         * Get user yang ditugaskan
         */
        $sql = "select a.*, u.name, u.photo as user_photo from post_assign a join users u on a.user_id=u.id where post_id=$id";
        $assigns = dbGetRows($sql);

        foreach ($assigns as $k => $d) {
            $d['user_photo'] = $this->User_model->getUserPhoto($d['user_id'],$d['user_photo']);
            if($d['group_id']){
                $sql = "select *
                        from groups
                        where type in ('".Group_model::TYPE_HELPDESK."', '".Group_model::TYPE_KOPERTIS."')
                        and id = '".$d['group_id']."'";
                $d['groups'] = dbGetRow($sql);
                $d['groups']['group_photo'] = $this->Group_model->getGroupPhoto($d['groups']['id'], $d['groups']['image']);
            }

            $assigns[$k] = $d;
        }

        $row['assigns'] = $assigns;
        $assigned_ids = Util::toList($row['assigns'], 'user_id');

        /*
         * Get Comments
         */
        $sql = "select c.*, 
        u.name as user_name, u.role as user_role, u.photo as user_photo
        from comments c join users u on c.user_id=u.id where (c.is_delete!=1 or c.is_delete is null) and post_id=$id $filter_comment order by c.id";
        $comments = dbGetRows($sql);
        foreach ($comments as $k => $d) {
            $d['user_photo'] = $this->User_model->getUserPhoto($d['user_id'],$d['user_photo']);

            if($d['group_id']){
                $sql = "select *
                        from groups
                        where type in ('".Group_model::TYPE_HELPDESK."', '".Group_model::TYPE_KOPERTIS."')
                        and id = '".$d['group_id']."'";
                $d['groups'] = dbGetRow($sql);
                $d['groups']['group_photo'] = $this->Group_model->getGroupPhoto($d['groups']['id'], $d['groups']['image']);
            }
            $d['is_assigned'] = in_array($d['user_id'], $assigned_ids);
            $comments[$k] = $d;
        }
        
        $row['comments'] = $comments;

        $comment_ids = array();
        foreach ($row['comments'] as $d) {
            $comment_ids[] = $d['id'];
        }
        $images = $this->getImagesByCommentIDs($comment_ids);
        $files = $this->getFilesByCommentIDs($comment_ids);

        foreach ($row['comments'] as $k => $d) {
            if ($images[$d['id']]){
                $row['comments'][$k]['images'] = $images[$d['id']];
            }
            if ($files[$d['id']]){
                $row['comments'][$k]['files'] = $files[$d['id']];
            }
        }
        // $images = $this->getImagesByCommentIDs($comment_ids);
        // $files = $this->getFilesByCommentIDs($comment_ids);
        // foreach ($row['comments'] as $k => $d) {
        //     if ($images[$d['id']])
        //         $row['comments'][$k]['images'] = $images[$d['id']];
        //     if ($files[$d['id']])
        //         $row['comments'][$k]['files'] = $files[$d['id']];
        // }
        // echo '<pre>';
        // var_dump($row);
        // die();
        return $row;
    }

    public function getPostImage($post_id){
        $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id=$post_id";
        $post_images = dbGetRows($sql);
        return $post_images;
    }

    public function getPostFile($post_id){
        $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id=$post_id";
        $post_files = dbGetRows($sql);
        return $post_files;
    }

    public function getInformations($user_id,$chosen_helpdesk=NULL,$page = 0, $param = null) {
        $CI = & get_instance();
        $offset = $page * 10;
        $filter_group = $filter1 = $filter2 = '';
        $group_default = $this->Group_model->getGroupDefault();
        $group_default_id = $group_default['id'];
        $group_default_name = $group_default['name'];
        $group_default_initial = $group_initial['initial'];
        $my_helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup($user_id);
        $my_helpdesk_group_id = $my_helpdesk_group['id'];
        if ($this->User_model->isOperator($user_id)) {
            $filter1 = " and p.user_id='$user_id' ";
        }
        if (!empty($param)) {
            $filter2 .= " and (u.name ilike '%" . $param . "%' or p.description ilike '%" . $param . "%')";
        }

        $sql='';
        //if ($this->User_model->isOperator($user_id) or $this->User_model->isSuperAdministrator($user_id)){
        if ($chosen_helpdesk==NULL){
            $filter_unit = '';
        } else {
            $filter_unit = " g.name='$chosen_helpdesk' and ";
        }

        $subquery = "select * from (";
        $subquery_end = " 
            order by post_id desc
            limit 10 offset $offset  ) x ";
        $subquery_end_union = " 
            order by post_id desc
            limit 10 offset $offset ) un ";

        $sql.= "
        $subquery
        select * from (select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, p.group_id as group_id,g.name as group_name, g.initial as group_initial,
            p.type as post_type, v.view_number, c.comment_number,
            u.name as user_name, u.photo as user_photo, p.is_sticky
            from posts p
            left join groups g on g.id=p.group_id
            join users u on u.id=p.user_id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
            left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id 
            left join post_rating pr on pr.post_id=p.id
            where $filter_unit p.type in ('I') and p.group_id is not null and (p.status != 'D' and (p.is_delete!=1 or p.is_delete is null)) 
            $filter2
            group by p.id, p.description, p.title, p.user_id, p.created_at, p.type, v.view_number, c.comment_number, g.name,
            u.name, u.photo, p.is_sticky, g.initial
        $subquery_end
            ";
        if (1==0 and ($this->User_model->isOperator($user_id) || $group_default_id==$my_helpdesk_group['id'] || $this->User_model->isSuperAdministrator($user_id)) and ($chosen_helpdesk==NULL || $chosen_helpdesk==$group_default_name)) {
            $sql .= "union 
            $subquery
                select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, $group_default_id as group_id,'$group_default_name' as group_name, '$group_default_initial' as group_initial,
            p.type as post_type, v.view_number, c.comment_number,
            u.name as user_name, u.photo as user_photo, p.is_sticky
            from posts p
            left join groups g on g.id=p.group_id
            join users u on u.id=p.user_id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
            left join (select post_id, count(*) as comment_number from comments group by post_id) c on c.post_id=p.id 
            left join post_rating pr on pr.post_id=p.id
            where p.type in ('I') and p.group_id is null and (p.status != 'D' and (p.is_delete!=1 or p.is_delete is null)) 
            $filter2
            group by p.id, p.description, p.title, p.user_id, p.created_at, p.type, v.view_number, c.comment_number,
            u.name, u.photo, p.is_sticky 
            $subquery_end_union
             ";
        }
        $sql .=") if 
            where if.group_id is not null
            ";
        $next_page_sql = $sql;
        $sql .="
            order by post_id desc
            limit 11 offset $offset";
        $rows = dbGetRows($sql);
        // $offset=($page+1)*10;
        // $next_page_sql .="
        //     order by post_id desc
        //     limit 10 offset $offset";
        // $data_next_page = dbGetRows($next_page_sql);
        // echo nl2br($sql);
        // die();
        // $sql = "select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, p.status as post_status, p.kode as kode_ticket, p.type as post_type,
        //     u.name as user_name, u.photo as user_photo,
        //     c.comment_number, v.view_number,
        //     o.nm_lemb,
        //     p.is_sticky
        //     from posts p
        //     join users u on u.id=p.user_id
        //     left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
        //     left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
        //     left join organisasi o on o.id_organisasi=p.id_organisasi 
        //     where p.type='I' and (p.status!='D' and (p.is_delete!=1 or p.is_delete is null))
        //     $filter2 ";
        // $next_page_sql = $sql;
        // $sql .="
        //     order by post_id desc
        //     limit 10 offset $offset";
        // $rows = dbGetRows($sql);

        // $offset=($page+1)*10;
        // $next_page_sql .="
        //     order by post_id desc
        //     limit 10 offset $offset";
        // $data_next_page = dbGetRows($next_page_sql);

        $ids = array();
        foreach ($rows as $key => $row) {
            $rows[$key]['hak_akses'] = $this->getHakAkses($row,$user_id);
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
            $ids[] = $row['post_id'];
            if (!isset($rows[10])) {
                $rows[$key]['next_page_informasi']=0;
            } else {
                $rows[$key]['next_page_informasi']=1;
            }

            if ($key==10){
                unset($rows[$key]);
            }
        }

        $str_ids = implode(',', $ids);
        if ($str_ids) {
            $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $images = array();
            $folder = 'posts/photos';
            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                // $this->checkCreateImage($id, $d['post_id'], $d['image'], $folder);
                $images[$d['post_id']][$i]['thumb'] = Image::createLink($id, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['medium'] = Image::createLink($id, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['original'] = Image::createLink($id, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
                $i++;
            }

            $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $files = array();

            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                // $this->checkCreateFile($id, $d['post_id'], $d['file'], 'posts/files');
                $files[$d['post_id']][$i] = File::createLink($id, $d['file'], 'posts/files');
                $i++;
            }

            $sql = "select a.*, u.name from post_assign a join users u on a.user_id=u.id where post_id in ($str_ids)";
            $temp2 = dbGetRows($sql);
            $assigns = array();
            foreach ($temp2 as $d) {
                $assigns[$d['post_id']][] = $d;
            }
        }

        foreach ($rows as &$row) {
            $row['images'] = $images[$row['post_id']];
            $row['files'] = $files[$row['post_id']];
            $row['assigns'] = $assigns[$row['post_id']];
        }
        return $rows;
    }

    function getInformasi($user_id,$id) {
        
        $CI = & get_instance();

        $sql = "select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, p.status as post_status,  p.kode as kode_ticket, p.type as post_type,
            g.name as group_name, g.initial as group_initial,
            u.name as user_name, u.role as user_role, u.photo as user_photo,
            c.comment_number, v.view_number,
            o.nm_lemb,
            p.is_sticky
            from posts p 
            join users u on u.id=p.user_id
            left join groups g on g.id=p.group_id
            left join group_members b on g.id=b.group_id and b.user_id='$user_id'
            left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
            left join organisasi o on o.id_organisasi=p.id_organisasi
            where p.id=$id and p.type='I' and p.type!='D'";
        $row = dbGetRow($sql);
        $row['hak_akses'] = $this->getHakAkses($row,$user_id);
        $row['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
        $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id=$id";
        $temp = dbGetRows($sql);
        $images = array();
        $folder = 'posts/photos';
        foreach ($temp as $d) {
            $id_image = md5($d['user_id'] . $CI->config->item('encryption_key'));
            // $this->checkCreateImage($id_image, $d['post_id'], $d['image'], $folder);
            $images[$d['post_id']][$i]['thumb'] = Image::createLink($id_image, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
            $images[$d['post_id']][$i]['medium'] = Image::createLink($id_image, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
            $images[$d['post_id']][$i]['original'] = Image::createLink($id_image, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
            $i++;
        }
        $row['images'] = $images[$id];

        $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id=$id";
        $temp = dbGetRows($sql);
        $files = array();

        foreach ($temp as $d) {
            $id_file = md5($d['user_id'] . $CI->config->item('encryption_key'));
            // $this->checkCreateFile($id_file, $d['post_id'], $d['file'], 'posts/files');
            $files[$d['post_id']][$i] = File::createLink($id_file, $d['file'], 'posts/files');
            $i++;
        }
        $row['files'] = $files[$id];

        $sql = "select c.*, 
        u.name as user_name, u.role as user_role, u.photo as user_photo
        from comments c join users u on c.user_id=u.id where post_id=$id and (c.is_delete!=1 or is_delete is null) order by c.id";
        $comments = dbGetRows($sql);
        foreach ($comments as $key => $c) {
            $comments[$key]['user_photo'] = $this->User_model->getUserPhoto($c['user_id'],$c['user_photo']);
        }
        $row['comments'] = $comments;

        $comment_ids = array();
        foreach ($row['comments'] as $d) {
            $comment_ids[] = $d['id'];
        }
        $images = $this->getImagesByCommentIDs($comment_ids);
        $files = $this->getFilesByCommentIDs($comment_ids);

        foreach ($row['comments'] as $k => $d) {
            if ($images[$d['id']]){
                $row['comments'][$k]['images'] = $images[$d['id']];
            }
            if ($files[$d['id']]){
                $row['comments'][$k]['files'] = $files[$d['id']];
            }
        }
        
        $sql = "select a.*, u.name from post_assign a join users u on a.user_id=u.id where post_id=$id";
        $assigns = dbGetRows($sql);
        $row['assigns'] = $assigns;

        return $row;
    }

    function addFromHelpdesk($id, $user_id){
        $sql = "select  id as post_id, 
                        description as post_description, 
                        user_id as post_user_id, 
                        status as post_status,
                        kode as kode_ticket,
                        type as post_type
                from posts
                where id = $id
                and type = '".self::TYPE_TICKET."'";
        $data = dbGetRow($sql);

        if($data){
            $sql = "select  id as user_id,
                            photo as user_photo,
                            name as user_name
                    from users
                    where id = '$user_id'";
            $user = dbGetRow($sql);

            $data['user_photo'] = $this->User_model->getUserPhoto($user['user_id'],$user['user_photo']);
            $data['user_name'] = $user['user_name'];
            $data['post_title'] = '[QA]'.$data['post_title'];

            /********************************
             * Set description into format 
             * Q: description 
             * A: comments1
             * 
             * comments2
             ********************************/
            $sql = "select  c.*, 
                            u.name as user_name, 
                            u.role as user_role, 
                            u.photo as user_photo
                    from comments c 
                    join users u on c.user_id=u.id 
                    where post_id=$id
                    and candelete = 1
                    order by c.id";
            $data['comments'] = dbGetRows($sql);

            $data['post_description'] = 'Q : '.$data['post_description'].'&#13;&#10;&#13;&#10;';
            if($data['comments']){
                $data['post_description'] .= ' A : &#13;&#10;';
                foreach ($data['comments'] as $i => $comment) {
                    $data['post_description'] .= $comment['text'].'&#13;&#10;&#13;&#10;';
                }
            }

        }

        return $data;
    }

    function getKnowledge($id,$user_id) {
        $CI = & get_instance();

        $sql = "select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, p.status as post_status, p.kode as kode_ticket, p.type as post_type,
            g.name as group_name, g.initial as group_initial,
            u.name as user_name, u.role as user_role, u.photo as user_photo,
            c.comment_number, v.view_number,
            o.nm_lemb,
            p.is_sticky, (case when (avg(coalesce(pr.rating,0))*10)%10>5 then ceil(avg(coalesce(pr.rating,0))) else floor(avg(coalesce(pr.rating,0))) end) as rating
            from posts p 
            join users u on u.id=p.user_id
            left join groups g on g.id=p.group_id
            left join group_members b on g.id=b.group_id and b.user_id='$user_id'
            left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id
            left join post_rating pr on pr.post_id=p.id 
            left join organisasi o on o.id_organisasi=p.id_organisasi
            where p.id=$id and p.type='K' and (p.status != 'D' and (p.is_delete!=1 or p.is_delete is null))
            group by p.id, p.description, p.title, p.user_id, p.created_at, p.type, v.view_number,
            u.name, u.photo, p.is_sticky, g.name, u.role, c.comment_number, o.nm_lemb, g.initial";
        $row = dbGetRow($sql);
        $row['hak_akses'] = $this->getHakAkses($row,$user_id);
        $row['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
        $row['tag'] = $row['tags'] = $this->getPostTag($row['post_id']);
        $row['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
        $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id=$id";
        $temp = dbGetRows($sql);
        $images = array();
        $folder = 'posts/photos';
        foreach ($temp as $d) {
            $id_image = md5($d['user_id'] . $CI->config->item('encryption_key'));
            // $this->checkCreateImage($id_image, $d['post_id'], $d['image'], $folder);
            $images[$d['post_id']][$i]['thumb'] = Image::createLink($id_image, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
            $images[$d['post_id']][$i]['medium'] = Image::createLink($id_image, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
            $images[$d['post_id']][$i]['original'] = Image::createLink($id_image, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
            $i++;
        }
        $row['images'] = $images[$id];

        $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id=$id";
        $temp = dbGetRows($sql);
        $files = array();

        foreach ($temp as $d) {
            $id_file = md5($d['user_id'] . $CI->config->item('encryption_key'));
            // $this->checkCreateFile($id_file, $d['post_id'], $d['file'], 'posts/files');
            $files[$d['post_id']][$i] = File::createLink($id_file, $d['file'], 'posts/files');
            $i++;
        }
        $row['files'] = $files[$id];

        $sql = "select c.*, 
        u.name as user_name, u.role as user_role, u.photo as user_photo
        from comments c join users u on c.user_id=u.id where post_id=$id order by c.id";
        $comments = dbGetRows($sql);
        foreach ($comments as $k => $comment) {
            $comments[$k]['user_photo'] = $this->User_model->getUserPhoto($comment['user_id'],$comment['user_photo']);
        }
        $row['comments'] = $comments;

        $comment_ids = array();
        foreach ($row['comments'] as $d) {
            $comment_ids[] = $d['id'];
        }
        $images = $this->getImagesByCommentIDs($comment_ids);
        $files = $this->getFilesByCommentIDs($comment_ids);

        foreach ($row['comments'] as $k => $d) {
            if ($images[$d['id']]){
                $row['comments'][$k]['images'] = $images[$d['id']];
            }
            if ($files[$d['id']]){
                $row['comments'][$k]['files'] = $files[$d['id']];
            }
        }

        return $row;
    }


    function getPostStat($user_id = NULL) {

        $stat = array();
        $group_default = $this->Group_model->getGroupDefault($user_id);
        $group_default_id = $group_default['id'];
        $group_default_name = $group_default['name'];
        $my_helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup($user_id);
        $my_helpdesk_group_id = $my_helpdesk_group['id'];
        $filter_group = $filter1 = $filter2 = $filter_group_helpdesk = '';
        $filter="";
        // if (($this->Group_member_model->_isAdminGroup($user_id) || ($this->User_model->isStaff($user_id) and !$this->User_model->isSuperAdministrator($user_id))) and !$this->User_model->isSuperAdministrator($user_id)){
        if (((!$this->User_model->isAdministrator($user_id) and $this->User_model->isStaff($user_id)) or $this->Group_member_model->_isAdminGroup($user_id)) and (!isset($home_status['dummy']) || $this->Group_member_model->_isAdminGroup($user_id))) {
            $join_assigns="left join (select a.* from post_assign a join users u on a.user_id=u.id where u.id='$user_id') pa on pa.post_id=p.id";
            $filter = " and (p.user_id='$user_id' or p.id in (pa.post_id)) ";
        } else if ($this->User_model->isOperator($user_id)){
            $filter .="and user_id='$user_id' ";
        } 
        if ($my_helpdesk_group!=NULL and (!$this->User_model->isOperator($user_id) and !$this->User_model->isSuperAdministrator($user_id))){
            if ($my_helpdesk_group_id==$group_default_id){
                $filter_group = " and (p.group_id=$my_helpdesk_group_id or p.group_id is null or p.user_id='$user_id') ";
            } else {
                $filter_group = " and (p.group_id=$my_helpdesk_group_id or p.user_id='$user_id') ";
                // if ($this->User_model->isStaff($user_id)){
                //     $filter_group = " and (group_id=$my_helpdesk_group_id or p.user_id='$user_id') ";
                // }
            }
            
        }
        $sql = "select p.status, count(*) as num
            from posts p
            $join_assigns 
            where p.type='T' and (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) $filter $filter_group
            group by p.status";
            // echo nl2br($sql);
            // die();
        $rows = dbGetRows($sql);
        foreach ($rows as $row) {
            $stat[$row['status']] = $row['num'];
        }
        return $stat;
    }

    function getListInformasi($chosen_helpdesk=NULL) {
        $CI = & get_instance();
        if ($chosen_helpdesk==NULL) {
            $filter = "";
        } else {
            $filter = " and g.name='$chosen_helpdesk' ";
        }
        $sql = "select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, p.status as post_status,  p.kode as kode_ticket, p.type as post_type,
            g.name as group_name, g.initial as group_initial,
            u.name as user_name, u.role as user_role, u.photo as user_photo,
            c.comment_number, v.view_number,
            o.nm_lemb,
            p.is_sticky
            from posts p 
            join users u on u.id=p.user_id
            left join groups g on g.id=p.group_id
            left join group_members b on g.id=b.group_id and b.user_id='$user_id'
            left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id
            left join organisasi o on o.id_organisasi=p.id_organisasi
            where (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) and p.type='I' $filter order by p.id desc limit 2 ";
        $rows = dbGetRows($sql);
        // echo nl2br($sql);
        // die();
        $ids = array();
        foreach ($rows as $key => $row) {
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
            $ids[] = $row['post_id'];
        } 
            
        $str_ids = implode(',', $ids);
        if ($str_ids) {
            $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $images = array();
            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                $images[$d['post_id']][$i]['thumb'] = Image::createLink($id, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['medium'] = Image::createLink($id, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['original'] = Image::createLink($id, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
                $i++;
            }

            $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $files = array();

            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                // $this->checkCreateFile($id, $d['post_id'], $d['file'], 'posts/files');
                $files[$d['post_id']][$i] = File::createLink($id, $d['file'], 'posts/files');
                $i++;
            }
        }

        foreach ($rows as &$row) {
            if ($row['post_id']){
                $row['images'] = $images[$row['post_id']];
                $row['files'] = $files[$row['post_id']];
            }
        }
        // echo '<pre>';
        // var_dump($rows);
        // echo '</pre>';
        // die($rows);

        return $rows;
    }

    function getCountInformasi($chosen_helpdesk=NULL) {
        if ($chosen_helpdesk==NULL){
            $filter = '';
        } else {
            $filter = " and g.name='$chosen_helpdesk' ";
        }
        $sql = "select count(*) from posts p left join groups g on g.id=p.group_id where (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) and p.type='I' $filter";
        $rows = dbGetOne($sql);
        return $rows;
    }

    function getListKnowledge($chosen_helpdesk=NULL) {
        $CI = & get_instance();
        if ($chosen_helpdesk==NULL) {
            $filter='';
        } else {
            $filter = " and g.name='$chosen_helpdesk' ";
        }
        $sql = "select *, ((0.1*view_number)+(0.4*comment_number)+(0.5*rating)) as poin
                from (select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, 
                p.type as post_type, coalesce(v.view_number,0) as view_number,
                u.name as user_name, u.photo as user_photo, p.is_sticky, (case when c.comment_number is not null then c.comment_number else 0 end) as comment_number,
                (case when (avg(coalesce(pr.rating,0))*10)%10>5 then ceil(avg(coalesce(pr.rating,0))) else floor(avg(coalesce(pr.rating,0))) end) as rating
                from posts p
                left join groups g on g.id=p.group_id
                join users u on u.id=p.user_id
                left join (select post_id, count(*) as comment_number from comments group by post_id) c on c.post_id=p.id
                left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
                left join post_rating pr on pr.post_id=p.id
                where p.type in ('K') $filter and (p.status != 'D' and (p.is_delete!=1 or p.is_delete is null)) 
                group by p.id, p.description, p.title, p.user_id, p.created_at, p.type, v.view_number,
                u.name, u.photo, p.is_sticky, c.comment_number) x
                order by rating desc, comment_number desc, view_number desc, post_id desc
                limit 2 ";
            // echo nl2br($sql);
            // die();
        $rows = dbGetRows($sql);

        $ids = array();
        foreach ($rows as $key => $row) {
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
            $ids[] = $row['post_id'];
        } 

        //echo $sql;



        $str_ids = implode(',', $ids);
        if ($str_ids) {
            $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $images = array();
            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                $images[$d['post_id']][$i]['thumb'] = Image::createLink($id, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['medium'] = Image::createLink($id, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['original'] = Image::createLink($id, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
                $i++;
            }

            $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $files = array();

            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                // $this->checkCreateFile($id, $d['post_id'], $d['file'], 'posts/files');
                $files[$d['post_id']][$i] = File::createLink($id, $d['file'], 'posts/files');
                $i++;
            }
        }

        foreach ($rows as &$row) {
            if ($row['post_id']) {
                $row['images'] = $images[$row['post_id']];
                $row['files'] = $files[$row['post_id']];
            }
        }
        // echo '<pre>';
        // var_dump($rows);
        // echo '</pre>';
        // die($rows);

        return $rows;
    }

    function getCountKnowledge($chosen_helpdesk=NULL) {
        if ($chosen_helpdesk==NULL) {
            $filter= "";
        } else {
            $filter = " and g.name='$chosen_helpdesk' ";
        }
        $sql = "select count(*) from posts p left join groups g on g.id=p.group_id where (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) and p.type='K' $filter";
        $rows = dbGetOne($sql);
        return $rows;
    }

    function getDisclaimer() {
        $CI = & get_instance();

        $sql = "select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, p.status as post_status,  p.kode as kode_ticket, p.type as post_type,
            g.name as group_name, g.initial as group_initial,
            u.name as user_name, u.role as user_role, u.photo as user_photo,
            c.comment_number,
            o.nm_lemb,
            p.is_sticky
            from posts p 
            join users u on u.id=p.user_id
            left join groups g on g.id=p.group_id
            left join group_members b on g.id=b.group_id and b.user_id='$user_id'
            left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
            left join organisasi o on o.id_organisasi=p.id_organisasi
            where (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) and p.type='D' order by p.id desc limit 1 ";
        $rows = dbGetRows($sql);

        $ids = array();
        foreach ($rows as $row) 
            $ids[] = $row['post_id'];

        //echo $sql;



        $str_ids = implode(',', $ids);
        if ($str_ids) {
            $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $images = array();
            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                $images[$d['post_id']][$i]['thumb'] = Image::createLink($id, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['medium'] = Image::createLink($id, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['original'] = Image::createLink($id, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
                $i++;
            }
            $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $files = array();

            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                // $this->checkCreateFile($id, $d['post_id'], $d['file'], 'posts/files');
                $files[$d['post_id']][$i] = File::createLink($id, $d['file'], 'posts/files');
                $i++;
            }
        }

        foreach ($rows as &$row) {
            if ($row['post_id']) {
                $row['images'] = $images[$row['post_id']];
                $row['files'] = $files[$row['post_id']];
            }
        }
        // echo '<pre>';
        // var_dump($rows);
        // echo '</pre>';
        // die(count($rows));

        return $rows;
    }

    function getListSticky() {
        $CI = & get_instance();
        $sql = "select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, p.status as post_status,  p.kode as kode_ticket, p.type as post_type, g.id as group_id,
            g.name as group_name, g.initial as group_initial,
            u.name as user_name, u.role as user_role, u.photo as user_photo,
            c.comment_number, v.view_number,
            o.nm_lemb,
            p.is_sticky, pr.rating
            from posts p 
            join users u on u.id=p.user_id
            left join groups g on g.id=p.group_id
            left join group_members b on g.id=b.group_id and b.user_id='$user_id'
            left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id
            left join organisasi o on o.id_organisasi=p.id_organisasi
            left join post_rating pr on pr.post_id=p.id and pr.user_id=p.user_id
            where (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) and p.is_sticky=1 order by p.id desc ";
        //die($sql);
        // echo '<pre>';
        // var_dump($sql);
        // echo '</pre>';
        // die();
        $rows = dbGetRows($sql);

        $ids = array();
        foreach ($rows as $key => $row) {
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
            if ($row['group_id']=='1'){
                $user_id = $row['post_user_id'];
                $sql_helpdesk = " select g.name, g.initial from group_members gm left join groups g on g.id=gm.group_id where gm.user_id='$user_id' and g.type='H' ";

                $helpdesk_name = dbGetRow($sql_helpdesk);

                if ($helpdesk_name!=NUll and $helpdesk_name!='') {
                    $rows[$key]['helpdesk_name'] = $helpdesk_name['name'];
                    $rows[$key]['helpdesk_initial'] = $helpdesk_name['initial'];
                }
                else {
                    $rows[$key]['helpdesk_name'] = '';
                    $rows[$key]['helpdesk_initial'] = '';
                }
            } else {
                $rows[$key]['helpdesk_name'] = '';
                $rows[$key]['helpdesk_initial'] = '';
            }
            $ids[] = $row['post_id'];
        }

        //echo $sql;

        $str_ids = implode(',', $ids);
        if ($str_ids) {
            $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $images = array();
            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                $images[$d['post_id']][$i]['thumb'] = Image::createLink($id, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['medium'] = Image::createLink($id, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['original'] = Image::createLink($id, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
                $i++;
            }
            $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $files = array();

            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                // $this->checkCreateFile($id, $d['post_id'], $d['file'], 'posts/files');
                $files[$d['post_id']][$i] = File::createLink($id, $d['file'], 'posts/files');
                $i++;
            }
        }

        foreach ($rows as &$row) {
            if ($row['post_id']) {
                $row['images'] = $images[$row['post_id']];
                $row['files'] = $files[$row['post_id']];
            }
        }
        // echo '<pre>';
        // var_dump($rows);
        // echo '</pre>';
        // die($rows);

        return $rows;
    }

    function getStickyByType($user_id,$type) {
        $CI = & get_instance();
        $sql = "select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, p.status as post_status,  p.kode as kode_ticket, p.type as post_type, g.id as group_id,
            g.name as group_name, g.initial as group_initial,
            u.name as user_name, u.role as user_role, u.photo as user_photo,
            c.comment_number, v.view_number,
            o.nm_lemb,
            p.is_sticky, pr.rating
            from posts p 
            join users u on u.id=p.user_id
            left join groups g on g.id=p.group_id
            left join group_members b on g.id=b.group_id and b.user_id='$user_id'
            left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
            left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id
            left join organisasi o on o.id_organisasi=p.id_organisasi
            left join post_rating pr on pr.post_id=p.id and pr.user_id=p.user_id
            where (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) and p.is_sticky=1 and p.type='$type' order by p.id desc ";
        $rows = dbGetRows($sql);

        $ids = array();
        foreach ($rows as $key => $row) {
            $rows[$key]['hak_akses'] = $this->getHakAkses($row,$user_id);
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
            if ($row['group_id']=='1'){
                $user_id = $row['post_user_id'];
                $sql_helpdesk = " select g.name, g.initial from group_members gm left join groups g on g.id=gm.group_id where gm.user_id='$user_id' and g.type='H' ";

                $helpdesk_name = dbGetRow($sql_helpdesk);

                if ($helpdesk_name!=NUll and $helpdesk_name!='') {
                    $rows[$key]['helpdesk_name'] = $helpdesk_name['name'];
                    $rows[$key]['helpdesk_initial'] = $helpdesk_name['initial'];
                }
                else {
                    $rows[$key]['helpdesk_name'] = '';
                    $rows[$key]['helpdesk_initial'] = '';
                }
            } else {
                $rows[$key]['helpdesk_name'] = '';
                $rows[$key]['helpdesk_initial'] = '';
            }
            $ids[] = $row['post_id'];
        }

        $str_ids = implode(',', $ids);
        if ($str_ids) {
            $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $images = array();
            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                $images[$d['post_id']][$i]['thumb'] = Image::createLink($id, Image::IMAGE_THUMB, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['medium'] = Image::createLink($id, Image::IMAGE_MEDIUM, $d['image'], 'posts/photos');
                $images[$d['post_id']][$i]['original'] = Image::createLink($id, Image::IMAGE_ORIGINAL, $d['image'], 'posts/photos');
                $i++;
            }
            $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $files = array();

            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                $files[$d['post_id']][$i] = File::createLink($id, $d['file'], 'posts/files');
                $i++;
            }
        }

        foreach ($rows as &$row) {
            if ($row['post_id']) {
                $row['images'] = $images[$row['post_id']];
                $row['files'] = $files[$row['post_id']];
            }
        }
        return $rows;
    }

    public function getMe($userId, $page = 0, $param = null) {
        $condition = array(
            '(
                (posts.user_id = ' . $userId . ') 
                OR (SELECT COUNT(*) 
                    FROM post_users 
                    WHERE post_users.post_id = posts.id AND post_users.user_id = ' . $userId . ' 
                    LIMIT 1) = 1 
                OR (SELECT COUNT(*) 
                    FROM group_members 
                    WHERE group_members.group_id = posts.group_id 
                        AND group_members.user_id = ' . $userId . ' 
                    LIMIT 1) = 1
            )'
        );
        if (!empty($param)) {
            $condition[] = "(users.name ilike '%" . $param . "%' or posts.description ilike '%" . $param . "%')";
        }
        $this->limit(10, ($page * 10));
        return $this->with(array('Category', 'User', 'Comment' => 'User', 'Group' => array('Group_member' => 'User'), 'Post_user'))
                        ->order_by('posts.id', 'DESC')->get_many_by($condition);
    }

    public function getGroup($id, $page = 0, $param = null) {
        $condition = array(
            'posts.group_id' => $id,
            'posts.type' => self::TYPE_GROUP,
        );
        if (!empty($param)) {
            $condition[] = "(users.name ilike '%" . $param . "%' or posts.description ilike '%" . $param . "%')";
        }
        $this->limit(10, ($page * 10));
        $result = $this->order_by("posts.id", 'DESC')->with(array('Category', 'Group' => array('Group_member' => 'User'), 'Comment' => 'User'))->get_many_by($condition);
        return $result;
    }

    public function getTag($userId, $page = 0) {
        $condition = array(
            'post_users.user_id' => $userId,
            'post_users.post_id = posts.id',
            'type' => self::TYPE_FRIEND,
        );
        $this->_database->from('post_users');
        $this->limit(10, ($page * 10));
        $result = $this->order_by("posts.id", 'DESC')->with(array('Category', 'Post_user', 'Comment' => 'User'))->get_many_by($condition);
        return $result;
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        // if (empty($data['description']) && empty($data['link']) && empty($data['image']) && empty($data['video']) && empty($data['file'])) {
        //     return 'Data yang dikirim tidak lengkap.';
        // }
        $data['description'] = $this->checkURL($data['description']);

        $dataInsert = array(
            'description'       => strip_tags($data['description']),
            'file'              => $data['file'],
            'created_at'        => Util::timeNow(),
            'updated_at'        => Util::timeNow(),
            'user_id_updated'   => $data['user_id'],
            'status'            => self::STATUS_DILAPORKAN,
            'kode'              => self::getTicketNumber(),
            'type'              => self::POST_TICKET,
            'user_id'           => $data['user_id'],
            'dinas_id'          => $data['id_unitkerja'],
            'is_private'        => 1,
            'is_delete'         => 0,
            'latitude'         => $data['latitude'],
            'longitude'         => $data['longitude'],
            'alamat'            => $data['alamat'],
            'detail_alamat'     => $data['detail_alamat']

        );

        $this->_database->trans_begin();

        /* if ada kuota*/
        $create = parent::create($dataInsert, $skip_validation, $return);
        if ($create) {
            $this->rotateTicket($create, $data['id_unitkerja']);
            return $create;

        }else{
            return false;
        }
    }

    function addView($post_id, $user_id) {
        $sql = "select * from post_view where post_id=$post_id and user_id='$user_id' order by waktu_lihat desc";
        $rows = dbGetRows($sql);

        $str_date_now = date("Y-m-d H:i:s");
        $date_viewed=new DateTime("01-01-1000 00:00:00");
        if (!empty($rows[0]['waktu_lihat'])){
            $str_date_viewed = $rows[0]['waktu_lihat'];
            $date_viewed = new DateTime($str_date_viewed);
        }
        
        $date_now = new DateTime($str_date_now);
        
        $interval = $date_viewed->diff($date_now);
        $interval=(int)$interval->format('%a');
        if ($interval>0){
            $record = array();
            $record['post_id'] = (int) $post_id;
            $record['user_id'] = $user_id;
            $record['waktu_lihat'] = date("Y-m-d H:i:s");
            return dbInsert('post_view', $record);
        } else {
            return false;
        }
        
    }

    public function update($data, $id) {
        if (isset($data['postTags'])) {
            $toTag = $data['postTags'];
            $this->load->model('Tag_model');
            $this->load->model('Post_tag_model');
            $tags = $this->Tag_model->get_many_by(array('name' => $toTag));
            $tagNames = Util::toList($tags, 'name');
            foreach ($toTag as $tag) {
                if (!in_array($tag, $tagNames)) {
                    $insert = $this->Tag_model->insert(array('name' => $tag), TRUE, FALSE);
                    if (!$insert) {
                        $this->_database->trans_rollback();
                        return FALSE;
                    }
                }
            }
            $tags = $this->Tag_model->get_many_by(array('name' => $toTag));
            $delete = $this->Post_tag_model->delete_by(array('post_id' => $id));
            if(!$delete){
                $this->_database->trans_rollback();
                return FALSE;
            } 
            foreach ($tags as $tag) {
                $dataToTag = array(
                    'post_id' => $id,
                    'tag_id' => $tag['id']
                );
                $insert = $this->Post_tag_model->insert($dataToTag, TRUE, FALSE);
                if (!$insert) {
                    $this->_database->trans_rollback();
                    return FALSE;
                }
            }
            
        }
        unset($data['postTags']);
        // if (dbUpdate('posts', $data, "id=$id")) {
        //     return $id;
        // } else {
        //     return false;
        // }
        $data['description'] = $this->checkURL($data['description']);
        return dbUpdate('posts', $data, "id=$id");
    }

    public function isAccessible($postId, $userId) {
        $post = $this->get($postId);
        if ($post['userId'] == $userId || $post['type'] == self::TYPE_PUBLIC) {
            return $post;
        } elseif (!empty($post['groupId']) && $post['type'] == self::TYPE_GROUP) {
            $this->load->model('Group_member_model');
            $member = $this->Group_member_model->get_by(array('group_id' => $post['groupId'], 'user_id' => $userId));
            return !empty($member) ? $post : FALSE;
        } elseif ($post['type'] == self::TYPE_FRIEND) {
            $this->load->model('Post_user_model');
            $member = $this->Post_user_model->get_by(array('post_id' => $postId, 'user_id' => $userId));
            return !empty($member) ? $post : FALSE;
        }
    }

    public function delete($id) {
        $id = (int) $id;
        $sql = "update posts set is_delete=1 where id=$id";
        return dbQuery($sql);

/*        $this->_database->trans_begin();
        $delete = parent::delete($id);
        if ($delete) {
            $this->load->model('Notification_model');
            $delete = $this->Notification_model->delete_by(array('reference_id' => $id, 'reference_type' => Notification_model::TYPE_POST_DETAIL));
        }

        if ($delete) {
            return $this->_database->trans_commit();
        } else {
            $this->_database->trans_rollback();
            return FALSE;
        }
        */
    }

    function insertPostImage($post_id, $nama_file, $file_ori) {
        $update = dbInsert('posts',array('file'=> $nama_file), " id=".$post_id);
        if($update){
            return true;
        }else{
            return false;
        }
        // $db2 = $this->load->database('upload', true);

        // $record = array();
        // $record['post_id'] = $post_id = (int) $post_id;
        // $record['image'] = $nama_file;
        // if (dbInsert('post_image', $record)) {
        //     $ukuran = strlen($file_ori);
        //     $isi_file = file_get_contents($file_ori);
        //     $isi_file = pg_escape_bytea($isi_file);
            
        //     $db2 = $this->load->database('upload', true);
            
        //     $sql = "insert into appfile (trans_id, trans_type, nama_file,ukuran,isi_file)
        //         values($post_id, 'P', '$nama_file', $ukuran,'$isi_file') ";          
        //     dbQuery($sql, $db2);
        //     return true;
        // } else {
        //     return false;
        // }

        
    }

    function insertPindahPostImage($post_id, $nama_file) {

        $record = array();
        $record['post_id'] = $post_id = (int) $post_id;
        $record['image'] = $nama_file;
        return dbInsert('post_image', $record);
    }    

    function insertSalinPostImage($post_id, $nama_file) {

        $record = array();
        $record['post_id'] = $post_id = (int) $post_id;
        $record['image'] = $nama_file;
        return dbInsert('post_image', $record);
    }    

    function insertPostFile($post_id, $file, $file_ori) {
        $record = array();
        $record['post_id'] = (int) $post_id;
        $record['file'] = $file;

        if (dbInsert('post_file', $record)) {
            $ukuran = strlen($file_ori);
            $isi_file = file_get_contents($file_ori);
            $isi_file = pg_escape_bytea($isi_file);
            
            $db2 = $this->load->database('upload', true);
            
            $sql = "insert into appfile (trans_id, trans_type, nama_file,ukuran,isi_file)
                values($post_id, 'P', '$nama_file', $ukuran,'$isi_file') ";          
            dbQuery($sql, $db2);
        }

        return true;
    }    

    function insertSalinPostFile($post_id, $file) {
        $record = array();
        $record['post_id'] = (int) $post_id;
        $record['file'] = $file;

        return dbInsert('post_file', $record);
    }  

    function insertPindahPostFile($post_id, $file) {
        $record = array();
        $record['post_id'] = (int) $post_id;
        $record['file'] = $file;

        return dbInsert('post_file', $record);
    } 

    function getImagesByCommentIDs($comment_ids) {
        $CI = & get_instance();

        if (!$comment_ids)
            return '';

        $ids = implode(',', $comment_ids);

        $sql = "select i.*, c.user_id from comment_image i join comments c on i.comment_id=c.id 
            where comment_id in ($ids) order by id";
        $images = dbGetRows($sql);
        $arr_images = array();
        $i=1;
        $folder = 'posts/photos';
        foreach ($images as $image) {
            $id = md5($image['user_id'] . $CI->config->item('encryption_key'));
            // $this->checkCreateImage($id, $image['comment_id'], $image['image'], $folder,1);
            $arr_images[$image['comment_id']][$i]['thumb'] = Image::createLink($id, Image::IMAGE_THUMB, $image['image'], 'posts/photos');
            $arr_images[$image['comment_id']][$i]['medium'] = Image::createLink($id, Image::IMAGE_MEDIUM, $image['image'], 'posts/photos');
            $arr_images[$image['comment_id']][$i]['original'] = Image::createLink($id, Image::IMAGE_ORIGINAL, $image['image'], 'posts/photos');
            $i++;
        }

        return $arr_images;
    }

    function getFilesByCommentIDs($comment_ids) {
        $CI = & get_instance();

        if (!$comment_ids)
            return '';

        $ids = implode(',', $comment_ids);

        $sql = "select i.*, c.user_id from comment_file i join comments c on i.comment_id=c.id 
            where comment_id in ($ids) order by id";
        $files = dbGetRows($sql);
        $arr_files = array();
        $i=1;
        $folder = 'posts/files';
        foreach ($files as $file) {
            $id = md5($file['user_id'] . $CI->config->item('encryption_key'));
            // $this->checkCreateFile($id, $file['comment_id'], $file['file'], $folder,1);
            $arr_files[$file['comment_id']][$i] = File::createLink($id, $file['file'], 'posts/files');
            $i++;
        }

        return $arr_files;
    }

    function setSticky($id, $is_sticky, $type='G') {
        $id = (int) $id;
        $type = pg_escape_string($type);

        $sql = "update posts set is_sticky=0 where type='$type' and is_sticky=1";
        dbQuery($sql);

        $sql = "update posts set is_sticky=1 where type='$type' and id=$id";
        dbQuery($sql);
    }

    function setPinPost($id, $is_sticky){
        $id = (int) $id;
        $record = array();
        $record['is_sticky'] = $is_sticky;
        return dbUpdate('posts', $record, "id=$id");
    }

    function setStatus($id, $status) {
        $sql = "select id, user_id, group_id, status
                from posts
                where id = $id";
        $postData = dbGetRow($sql);

        $id = (int) $id;
        $record = array();
        $record['status'] = $status;
        $update = dbUpdate('posts', $record, "id=$id");

        return $update;
    }

    function setRating($id, $rating,$user_id) {
        $id = (int) $id;
        $record = array();
        $record['post_id'] = $id;
        $record['user_id'] = $user_id;
        $record['rating'] = $rating;
        $insert = dbInsert('post_rating', $record);
        
        if($insert){
            $sql = "select id, user_id, group_id, status
                    from posts
                    where id = $id";
            $postData = dbGetRow($sql);

            $this->load->model("Post_quota_model");
            $insert = $this->Post_quota_model->incQuota($postData['user_id'], $postData['group_id']);
        }
        return $insert;
    }

    function assignTicket($id, $id_operator, $is_delete=true) {
        $id = (int) $id;
        if ($is_delete){
            $sql = "delete from post_assign where post_id=$id";
            $ret = dbQuery($sql);
            if (!$ret)
                return false;
        }

        $record = array();
        $record['post_id'] = $id;
        $record['user_id'] = $id_operator;
        $record['created_at'] = date("Y-m-d H:i:s");
        $record['updated_at'] = date("Y-m-d H:i:s");
        dbInsert('post_assign', $record);

        return true;
    }

    function deleteAssign($id){
        $sql = " delete from post_assign where id=$id ";
        return dbQuery($sql);
    }

    private function setForDashboard($userId, $condition = array()) {
        $this->load->model('Group_model');
        $this->_database->join("post_users", 'post_users.post_id = posts.id', 'LEFT');
        $this->_database->join("groups", "groups.id = posts.group_id AND groups.type = '" . Group_model::TYPE_PROJECT . "' ", 'LEFT');
        $this->_database->join("group_members", 'group_members.group_id = groups.id', 'LEFT');
        if (!empty($userId)) {
            $condition[] = "(post_users.user_id = $userId OR group_members.user_id = $userId OR posts.user_id = $userId)";
        }
        $condition[] = '((issues.status <' . self::STATUS_CLOSE . ') or (issues.status = ' . self::STATUS_CLOSE . " and posts.updated_at < '" . date('Y-m-d', strtotime('-90 days')) . "'))";
        $this->_database->group_by('posts.id, issues.post_id');
        $this->order_by(array('issues.deadline' => 'ASC', 'issues.priority' => 'DESC', 'posts.id' => 'ASC'));
        return $condition;
    }

    public function getDashboard($user_id,$chosen_helpdesk=NULL,$page=1, $id_user=NULL, $is_created_by_me=0, $status=NULL, $as_admin_kopertis=FALSE, $sort_by=0, $date_filter=NULL) {
        $filter_user = "";
        $filter_chosen_helpdesk = '';
        $group_default = $this->Group_model->getGroupDefault();
        $group_default_id = $group_default['id'];
        $my_helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup($user_id);
        $my_helpdesk_group_id = $my_helpdesk_group['id'];
        $my_helpdesk_group_name = $my_helpdesk_group['name'];
        // if ($my_helpdesk_group!=NULL and !$this->User_model->isSuperAdministrator($user_id)) {
        if ($my_helpdesk_group!=NULL and !$this->User_model->isStaff($user_id)) {
            $chosen_helpdesk = $my_helpdesk_group['name'];
        }
        $and_or = "and";
        $limit_buka = 10;
        $limit_proses = 10;
        $limit_selesai = 10;

        if ($this->User_model->isStaff($user_id) || $this->User_model->isOperator($user_id) || $this->User_model->isSuperAdministrator($user_id) || $my_helpdesk_group['id']==$group_default['id']) {
            $select_group_id = $group_default['id'];
            $select_group_name = $group_default['name'];
            $select_group_initial = $group_default['initial'];
        } else {
            $select_group_id = 'NULL';
            $select_group_name = $group_default['name'];
            $select_group_initial = $group_default['initial'];
        }
        
        if($date_filter!=NULL){
            $filter_date = " and p.created_at between '".$date_filter['start_date']."' and '".$date_filter['end_date']."' ";
        }

        if ($chosen_helpdesk!=NULL and $chosen_helpdesk!="") {
            $filter_chosen_helpdesk = " where (x.group_name='$chosen_helpdesk' or x.post_user_id='$user_id') ";
            //if ($this->User_model->isSuperAdministrator($user_id)) {
            if ($this->User_model->isStaff($user_id)) {
                $filter_chosen_helpdesk = " where (x.group_name='$chosen_helpdesk') ";
            }
            $filter_helpdesk = " and (g.name='$chosen_helpdesk' or p.user_id='$user_id') ";
            $union_helpdesk = "";
        }
        if ($id_user!=NULL || $chosen_helpdesk!=NULL) {
            if ($id_user!=NULL){
                if ($this->User_model->isOperator($user_id)) {
                    $filter_user .= " and (p.user_id='$user_id' ";
                    $and_or = "or";
                }
                $filter_user .= " $and_or (p.id in (select post_id from post_assign where user_id = '$id_user') ";

                $users[] = "'".$id_user."'";
                if($as_admin_kopertis){
                    $members = $this->User_model->getListMemberKopertis($user_id);
                    foreach ($members as $key_member => $member) {
                        $users[] = "'".$member."'";
                    }
                }
                $filter_user .= "or p.user_id in (".implode(',', $users).")) ";
                // $filter_user .= "user_id = '$id_user') or p.user_id='$id_user') ";
                if ($this->User_model->isOperator($user_id)){
                    $filter_user.=")";
                }
            }
            else 
                $filter_user="";
        } else {
            // if ($this->User_model->isStaff($user_id) and !$this->User_model->isSuperAdministrator($user_id)){
            //     $filter_helpdesk = '';
            //     $filter_chosen_helpdesk = '';
            // }
        }
        if ($is_created_by_me==1) {
            $filter_created_by_me = " and x.post_user_id='$user_id' ";
            if ($filter_chosen_helpdesk=="" or $filter_chosen_helpdesk==NULL or !isset($filter_chosen_helpdesk)){
                $filter_created_by_me = " where x.post_user_id='$user_id' ";
            }
            
        }

        // backup
        // $union_user = "union
        //     select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, g.id as group_id, g.name as group_name, g.initial as group_initial,
        //         p.status as post_status, p.kode as kode_ticket, p.type,
        //         u.name as user_name, u.photo as user_photo, 
        //         c.comment_number,  pr.rating,
        //         o.nm_lemb
        //         from posts p
        //         join users u on u.id=p.user_id
        //         left join groups g on g.id=p.group_id and g.type='H'
        //         left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
        //         left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
        //         left join organisasi o on o.id_organisasi=p.id_organisasi
        //         left join post_rating pr on pr.post_id=p.id and pr.user_id=p.user_id
                
        //         where p.group_id is not null $filter_helpdesk and p.type!='K' and p.type!='I' and (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) and p.type!='D' and p.type='T' $filter_user";       
        
        // $sql = "select x.*, max(c.created_at) as last_comment from (
        //         select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, $select_group_id as group_id, CAST('$select_group_name' as text) as group_name, '$select_group_initial' as group_initial,
        //             p.status as post_status, p.kode as kode_ticket, p.type,
        //             u.name as user_name, u.photo as user_photo,
        //             c.comment_number, pr.rating,
        //             o.nm_lemb
        //             from posts p
        //             join users u on u.id=p.user_id
        //             left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
        //             left join organisasi o on o.id_organisasi=p.id_organisasi
        //             left join post_rating pr on pr.post_id=p.id and pr.user_id=p.user_id
        //             where p.group_id is null and p.type='T' and (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) $filter_user
        //             $union_user
        //             ) x 
        //             left join comments c on c.post_id=x.post_id
        //             $filter_chosen_helpdesk $filter_created_by_me 
        //             group by x.post_id, x.post_description, x.post_title, x.post_user_id, x.post_created_at, x.group_id, x.group_name, x.group_initial,
        //                 x.post_status, x.kode_ticket, x.type,
        //                 x.user_name, x.user_photo,
        //                 x.comment_number, x.rating,
        //                 x.nm_lemb
        //             order by 
        //             ";

        $union_user = "
            select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, g.id as group_id, g.name as group_name, g.initial as group_initial,
                p.status as post_status, p.kode as kode_ticket, p.type,
                u.name as user_name, u.photo as user_photo, 
                c.comment_number,  pr.rating,
                o.nm_lemb
                from posts p
                join users u on u.id=p.user_id
                left join groups g on g.id=p.group_id and g.type='H'
                left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
                left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
                left join organisasi o on o.id_organisasi=p.id_organisasi
                left join post_rating pr on pr.post_id=p.id and pr.user_id=p.user_id
                
                where p.group_id is not null $filter_helpdesk $filter_date and p.type!='K' and p.type!='I' and (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) and p.type!='D' and p.type='T' $filter_user $this->_filter"; 

        $sql = "select x.*, max(c.created_at) as last_comment from (
                    $union_user
                    ) x 
                    left join comments c on c.post_id=x.post_id
                    $filter_chosen_helpdesk $filter_created_by_me 
                    group by x.post_id, x.post_description, x.post_title, x.post_user_id, x.post_created_at, x.group_id, x.group_name, x.group_initial,
                        x.post_status, x.kode_ticket, x.type,
                        x.user_name, x.user_photo,
                        x.comment_number, x.rating,
                        x.nm_lemb
                    order by 
                    ";

        // add Sort
        if ($sort_by==0){
            $sql .= " x.post_id asc ";
        } else if ($sort_by==1){
            $sql .= " x.post_id desc ";
        } else if ($sort_by==2){
            $sql .= " last_comment desc nulls last";
        }

        // if ($_SESSION['sigap']['auth']['username']=='inirmala'){
        //     echo nl2br($sql);
        //     echo '<br>'.$chosen_helpdesk;
        //     die(); 
        // }
        // echo nl2br($sql);
        // die();
        $rows = dbGetRows($sql);

        $ids = array();
        $jml_post = array(
            self::STATUS_OPEN => 0,
            self::STATUS_PROSES => 0,
            self::STATUS_CLOSE => 0
        );

        $this->load->model('Comment_model');
        foreach ($rows as $key => $row) {
            $rows[$key]['hak_akses'] = $this->getHakAkses($row,$user_id);
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
            if ($row['post_status']=='B') {
                $jml_post[self::STATUS_OPEN]++;
            } elseif ($row['post_status']=='P') {
                $jml_post[self::STATUS_PROSES]++;
            } elseif ($row['post_status']=='S') {
                $jml_post[self::STATUS_CLOSE]++;
            }

            $ids[] = $row['post_id'];
        }

        $str_ids = implode(',', $ids);
        if ($str_ids) {
            $sql = "select a.*, u.name, u.photo as user_photo from post_assign a join users u on a.user_id=u.id where post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $assigns = array();
            foreach ($temp as $d) {
                $d['user_photo'] = $this->User_model->getUserPhoto($d['user_id'],$d['user_photo']);
                if($d['group_id']){
                    $sql = "select *
                            from groups
                            where type in ('".Group_model::TYPE_HELPDESK."', '".Group_model::TYPE_KOPERTIS."')
                            and id = '".$d['group_id']."'";
                    $d['groups'] = dbGetRow($sql);
                    $d['groups']['group_photo'] = $this->Group_model->getGroupPhoto($d['groups']['id'], $d['groups']['image']);
                }

                $assigns[$d['post_id']][] = $d;
            }
        }
        foreach ($rows as $k => $row) {
            $rows[$k]['assigns'] = $assigns[$row['post_id']];
        }

        $data = array(
            self::STATUS_OPEN => array(),
            self::STATUS_PROSES => array(),
            self::STATUS_CLOSE => array()
        );
        $counter_buka=1;
        $counter_proses=1;
        $counter_close =1;
        
        foreach ($rows as $row) {
            if (in_array($row['post_status'], array(self::STATUS_OPEN)) && $counter_buka<=$limit_buka) {
                $data[self::STATUS_OPEN][] = $row;
                $counter_buka++;
                if ($status=='B') {
                    $counter_buka--;
                }
            } elseif (in_array($row['post_status'], array(self::STATUS_PROSES)) && $counter_proses<=$limit_proses) {
                $data[self::STATUS_PROSES][] = $row;
                $counter_proses++;
                if ($status=='P') {
                    $counter_proses--;
                }
            } elseif (in_array($row['post_status'], array(self::STATUS_CLOSE)) && $counter_close<=$limit_selesai) {
                $data[self::STATUS_CLOSE][] = $row;
                $counter_close++;
                if ($status=='S') {
                    $counter_close--;
                }
            }
        }
        $data['jml_post']=$jml_post;
        ksort($data);        
        

        // Sort ticket done from newest
        // foreach ($data as $key => $value) {
        //     if ($key==self::STATUS_CLOSE) {
        //         $tmp = array();
        //         foreach ($value as $k => $v) {
        //             $tmp[$k][] = &$v['post_id'];
        //         }
        //         array_multisort($tmp, SORT_DESC, $value);
        //         // $data[$key] = $value;
        //         $counter=0;
        //         foreach ($value as $k => $v) {
        //             if ($counter<10 or $status=='S'){
        //                 $val[$k] = $v;
        //                 $counter++;
        //             } else 
        //                 break;
        //         }
        //         $data[$key] = $val;
        //     }
        //     $data['jml_post'][$key]=$jml_post[$key];
        // } 
        // echo '<pre>';
        // vaR_dump($data);
        // die();

        // RESET variables
        $this->_filter = '';

        return $data;
    }

    public function getDashboardForAPI($user_id,$chosen_helpdesk=NULL,$page=0, $status='B', $id_user=NULL,$is_created_by_me=0, $as_admin_kopertis=FALSE, $sort_by=0) {
        $limit = 10;
        $CI = & get_instance();   
        $offset = $page*$limit;
        $filter_user = "";
        $filter_chosen_helpdesk = '';
        $group_default = $this->Group_model->getGroupDefault();
        $group_default_id = $group_default['id'];
        $my_helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup($user_id);
        $my_helpdesk_group_id = $my_helpdesk_group['id'];
        $my_helpdesk_group_name = $my_helpdesk_group['name'];
        if ($my_helpdesk_group!=NULL and !$this->User_model->isSuperAdministrator($user_id)) {
            $chosen_helpdesk = $my_helpdesk_group['name'];
        }
        $filter_status = " where post_status='$status' ";
        $and_or = "and";
        $limit_buka = 10;
        $limit_proses = 10;
        $limit_selesai = 10;
        if ($this->User_model->isOperator($user_id) || $this->User_model->isSuperAdministrator($user_id) || $my_helpdesk_group['id']==$group_default['id']) {
            $select_group_id = $group_default['id'];
            $select_group_name = $group_default['name'];
            $select_group_initial = $group_default['initial'];
        } else {
            $select_group_id = 'NULL';
            $select_group_name = $group_default['name'];
            $select_group_initial = $group_default['initial'];
        }

        if ($chosen_helpdesk!=NULL and $chosen_helpdesk!="") {
            $filter_chosen_helpdesk = " and (group_name='$chosen_helpdesk' or post_user_id='$user_id') ";
            if ($this->User_model->isSuperAdministrator($user_id)) {
                $filter_chosen_helpdesk = " and (group_name='$chosen_helpdesk') ";
            }
            $filter_helpdesk = " and (g.name='$chosen_helpdesk' or p.user_id='$user_id') ";
            $union_helpdesk = "";
        }

        if ($id_user!=NULL || $chosen_helpdesk!=NULL) {
            if ($id_user!=NULL){
                if ($this->User_model->isOperator($user_id)) {
                    $filter_user .= " and (p.user_id='$user_id' ";
                    $and_or = "or";
                }
                $filter_user .= " $and_or (p.id in (select post_id from post_assign where user_id='$id_user') ";

                $users[] = "'".$id_user."'";
                if($as_admin_kopertis){
                    $members = $this->User_model->getListMemberKopertis($user_id);
                    foreach ($members as $key_member => $member) {
                        $users[] = "'".$member."'";
                    }
                }
                $filter_user .= "or p.user_id in (".implode(',', $users).")) ";

                if ($this->User_model->isOperator($user_id)){
                    $filter_user.=")";
                }
            }
            else 
                $filter_user="";
        } else {
            if ($this->User_model->isStaff($user_id) and !$this->User_model->isSuperAdministrator($user_id)){
                $filter_helpdesk = '';
                $filter_chosen_helpdesk = '';
            }
        }
        if ($is_created_by_me==1) {
            $filter_created_by_me = " and post_user_id='$user_id' ";
            if ($filter_chosen_helpdesk=="" or $filter_chosen_helpdesk==NULL or !isset($filter_chosen_helpdesk)){
                $filter_created_by_me = " and post_user_id='$user_id' ";
            }
            
        }

        // add Sort
        if ($sort_by==0){
            $order = " order by post_id asc ";
        } else if ($sort_by==1){
            $order = " order by post_id desc ";
        } else if ($sort_by==2){
            $order = " order by last_comment desc nulls last";
        }

        $subquery = "select * from (";
        $subquery_end_helpdesk = "$order
            limit 10 offset $offset ) helpdesk ";
        if ($sort_by==2){
            $subquery_end_helpdesk = "
                limit 10 offset $offset ) helpdesk ";
        }

        // $union_user = "
        //     union
        //     $subquery
        //     select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, g.id as group_id, g.name as group_name, g.initial as group_initial,
        //         p.status as post_status, p.kode as kode_ticket, p.type as post_type,
        //         u.name as user_name, u.photo as user_photo, 
        //         c.comment_number,  pr.rating,
        //         o.nm_lemb
        //         from posts p
        //         join users u on u.id=p.user_id
        //         left join groups g on g.id=p.group_id and g.type='H'
        //         left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
        //         left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
        //         left join organisasi o on o.id_organisasi=p.id_organisasi
        //         left join post_rating pr on pr.post_id=p.id and pr.user_id=p.user_id
                
        //         where p.group_id is not null $filter_helpdesk and p.type!='K' and p.type!='I' and (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) and p.type!='D' and p.type!='G' $filter_user
        //     $subquery_end_helpdesk
        //     ";       
        

        // $sql = "select x.*, max(c.created_at) as last_comment from (
        //         $subquery
        //         select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, $select_group_id as group_id, CAST('$select_group_name' as text) as group_name, '$select_group_initial' as group_initial,
        //             p.status as post_status, p.kode as kode_ticket, p.type as post_type,
        //             u.name as user_name, u.photo as user_photo,
        //             c.comment_number, pr.rating,
        //             o.nm_lemb
        //             from posts p
        //             join users u on u.id=p.user_id
        //             left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
        //             left join organisasi o on o.id_organisasi=p.id_organisasi
        //             left join post_rating pr on pr.post_id=p.id and pr.user_id=p.user_id
        //             where p.group_id is null and p.type='T' and (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) $filter_user
        //             $subquery_end_helpdesk
        //             $union_user
        //             ) x 
        //             left join comments c on c.post_id=x.post_id
        //             $filter_status $filter_chosen_helpdesk $filter_created_by_me 
        //             group by x.post_id, x.post_description, x.post_title, x.post_user_id, x.post_created_at, x.group_id, x.group_name, x.group_initial,
        //                 x.post_status, x.kode_ticket, x.post_type,
        //                 x.user_name, x.user_photo,
        //                 x.comment_number, x.rating,
        //                 x.nm_lemb
        //             $order
        //             ";

        $union_user = "
            $subquery
            select p.id as post_id, p.description as post_description, p.title as post_title, p.user_id as post_user_id, p.created_at as post_created_at, g.id as group_id, g.name as group_name, g.initial as group_initial,
                p.status as post_status, p.kode as kode_ticket, p.type as post_type,
                u.name as user_name, u.photo as user_photo, 
                c.comment_number,  pr.rating,
                o.nm_lemb
                from posts p
                join users u on u.id=p.user_id
                left join groups g on g.id=p.group_id and g.type='H'
                left join (select post_id, count(*) as comment_number from comments where candelete = 1 and (is_delete!=1 or is_delete is null) group by post_id) c on c.post_id=p.id
                left join (select post_id, count(*) as view_number from post_view group by post_id) v on v.post_id=p.id 
                left join organisasi o on o.id_organisasi=p.id_organisasi
                left join post_rating pr on pr.post_id=p.id and pr.user_id=p.user_id
                
                where p.group_id is not null $filter_helpdesk and p.type!='K' and p.type!='I' and (p.status!='D' and (p.is_delete!=1 or p.is_delete is null)) and p.type!='D' and p.type!='G' $filter_user and p.status='$status'
            $subquery_end_helpdesk
            "; 

        $sql = "select x.*, max(c.created_at) as last_comment from (
                
                    $union_user
                    ) x 
                    left join comments c on c.post_id=x.post_id
                    $filter_status $filter_chosen_helpdesk $filter_created_by_me 
                    group by x.post_id, x.post_description, x.post_title, x.post_user_id, x.post_created_at, x.group_id, x.group_name, x.group_initial,
                        x.post_status, x.kode_ticket, x.post_type,
                        x.user_name, x.user_photo,
                        x.comment_number, x.rating,
                        x.nm_lemb
                    $order
                    ";
        $rows = dbGetRows($sql);
        // $ids = array();
        // $jml_post = array(
        //     self::STATUS_OPEN => 0,
        //     self::STATUS_PROSES => 0,
        //     self::STATUS_CLOSE => 0
        // );

        foreach ($rows as $key => $row) {
            $rows[$key]['hak_akses'] = $this->getHakAkses($row,$user_id);
            $rows[$key]['user_photo'] = $this->User_model->getUserPhoto($row['post_user_id'],$row['user_photo']);
            // if ($row['post_status']=='B') {
            //     $jml_post[self::STATUS_OPEN]++;
            // } elseif ($row['post_status']=='P') {
            //     $jml_post[self::STATUS_PROSES]++;
            // } elseif ($row['post_status']=='S') {
            //     $jml_post[self::STATUS_CLOSE]++;
            // }
            $ids[] = $row['post_id'];
        }

        $str_ids = implode(',', $ids);
        if ($str_ids) {
            $sql = "select i.*, p.user_id from post_image i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $images = array();

            $folder = 'posts/photos';

            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));

                // check jika file tidak ada maka generate dari table
                // $this->checkCreateImage($id, $d['post_id'], $d['image'], $folder);

                $images[$d['post_id']][$i]['thumb'] = Image::createLink($id, Image::IMAGE_THUMB, $d['image'], $folder);
                // $images[$d['post_id']][$i]['medium'] = Image::createLink($id, Image::IMAGE_MEDIUM, $d['image'], $folder);
                $images[$d['post_id']][$i]['original'] = Image::createLink($id, Image::IMAGE_ORIGINAL, $d['image'], $folder);
                $i++;
            }
            // die();

            $sql = "select i.*, p.user_id from post_file i join posts p on i.post_id=p.id  where i.post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $files = array();
            foreach ($temp as $d) {
                $id = md5($d['user_id'] . $CI->config->item('encryption_key'));
                // $this->checkCreateFile($id, $d['post_id'], $d['file'], 'posts/files');
                $files[$d['post_id']][$i] = File::createLink($id, $d['file'], 'posts/files');
                $i++;
            }

            $sql = "select a.*, u.name, u.photo as user_photo from post_assign a join users u on a.user_id=u.id where post_id in ($str_ids)";
            $temp = dbGetRows($sql);
            $assigns = array();
            foreach ($temp as $d) {
                $d['user_photo'] = $this->User_model->getUserPhoto($d['user_id'],$d['user_photo']);
                if($d['group_id']){
                    $sql = "select *
                            from groups
                            where type in ('".Group_model::TYPE_HELPDESK."', '".Group_model::TYPE_KOPERTIS."')
                            and id = '".$d['group_id']."'";
                    $d['groups'] = dbGetRow($sql);
                    $d['groups']['group_photo'] = $this->Group_model->getGroupPhoto($d['groups']['id'], $d['groups']['image']);
                }
                $assigns[$d['post_id']][] = $d;
            }
        }
        foreach ($rows as $k => $row) {
            $rows[$k]['images'] = $images[$row['post_id']];
            $rows[$k]['files'] = $files[$row['post_id']];
            $rows[$k]['assigns'] = $assigns[$row['post_id']];
        }

        // $data = array(
        //     self::STATUS_OPEN => array(),
        //     self::STATUS_PROSES => array(),
        //     self::STATUS_CLOSE => array()
        // );
        // $counter_buka=1;
        // $counter_proses=1;
        
        // foreach ($rows as $row) {
        //     if (in_array($row['post_status'], array(self::STATUS_OPEN)) && $counter_buka<=$limit_buka) {
        //         $data[self::STATUS_OPEN][] = $row;
        //         $counter_buka++;
        //     } elseif (in_array($row['post_status'], array(self::STATUS_PROSES)) && $counter_proses<=$limit_proses) {
        //         $data[self::STATUS_PROSES][] = $row;
        //         $counter_proses++;
        //     } elseif (in_array($row['post_status'], array(self::STATUS_CLOSE))) {
        //         $data[self::STATUS_CLOSE][] = $row;
                
        //     }
        // }
        // ksort($data);
        // foreach ($data as $key => $value) {
        //     if ($key==self::STATUS_CLOSE) {
        //         $tmp = array();
        //         foreach ($value as $k => $v) {
        //             $tmp[$k][] = &$v['post_id'];
        //         }
        //         array_multisort($tmp, SORT_DESC, $value);
        //         // $data[$key] = $value;
        //         $counter=0;
        //         foreach ($value as $k => $v) {
        //             if ($counter<10){
        //                 $val[$k] = $v;
        //                 $counter++;
        //             } else 
        //                 break;
        //         }
        //         $data[$key] = $val;
        //         // echo '<pre>';
        //         // var_dump($data[self::STATUS_OPEN]);
        //         // die();
        //     }
        //     $data['jml_post'][$key]=$jml_post[$key];
        // } 
        $data = $rows;
        return $data;
    }

    public function getPublic($page = 0, $param = null) {

        $condition = array(
            'posts.type' => self::TYPE_PUBLIC,
        );
        if (!empty($param)) {
            $condition[] = "(users.name ilike '%" . $param . "%' or posts.description ilike '%" . $param . "%')";
        }
        $this->limit(10, ($page * 10));
        //$dta = $this->order_by("posts", 'DESC')->get_many_by($condition);

        return $this->get_many_by($condition);
    }

    public function getDashboardByStatus($status, $userId, $condition = array()) {
        $condition = $this->setForDashboard($userId, $condition);
        $condition['issues.status'] = $status;
        $this->with(array('Category', 'User', 'Post_user', 'Group', 'Comment' => 'User'));

        $data = $this->get_many_by($condition);
        return $data;
    }

    public function getChartDashboard($userId, $condition = array()) {
        $condition = $this->setForDashboard($userId, $condition);
        $this->with(array('Category'));

        $data = $this->get_many_by($condition);
        return $data;
    }

    public static function getStatus() {
        return array(
            self::STATUS_OPEN => 'TERBUKA' ,
            self::STATUS_START =>'START' ,
            self::STATUS_PROSES => 'DIPROSES' ,
            self::STATUS_DONE => 'DONE' ,
            self::STATUS_REOPEN => 'REOPEN' ,
            self::STATUS_CLOSE => 'SELESAI' ,
            self::STATUS_INVALID => 'INVALID' ,
            self::STATUS_DELETE => 'DIHAPUS'
        );
    }

    function getTicketNumber() {
        $sql = "select max(cast(kode as integer)) from posts where kode ilike '". date('y') . "%'";
        $num = dbGetOne($sql);
        $num = (int) substr($num, 2);
        $num++;   
        return date('y') . str_pad($num, 6, '0', STR_PAD_LEFT);
    }

    function rotateTicket($post_id, $dinas_id, $is_delete=true) {
        $sql ="select id,username,sum(
                    case when t.status='S' then 0.1
                         when t.status is null then 0
                         when t.status!='S' then 1
                     end
                ) as bobot
                from
                (select u.id, u.name, u.username, p.status, p.id as post_id
                from users u
                left join post_assign pa on u.id = pa.user_id
                left join posts p on pa.post_id = p.id
                where u.id_dinas=$dinas_id and u.role='O') t
                group by id,username
                order by bobot asc";
        $row = dbGetRow($sql);
        $list_staff = $row['id'];
        $this->assignTicket($post_id, $list_staff, $is_delete);
    }

    function checkCreateImage($id, $trans_id, $file_name, $folder, $is_comment=0) {
        $filename =  Image::getFileName($id, Image::IMAGE_ORIGINAL, $file_name);
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/' ;
        $file = $path . '/' . $filename;

        $CI->load->library('upload');
        $CI->load->library('image_lib');
        $config['file_name'] = $filename;
        $config['remove_spaces']=TRUE;
        $upload = new CI_Upload($config);

        if (!file_exists($file)) {
            // die($file);
            $db2 = $CI->load->database('upload', true);
            $sql = "select * from appfile where trans_id=$trans_id";
            $filter_type = " and trans_type='P'";
            if ($is_comment)
                $filter_type =" and trans_type='C'";
            $sql .=$filter_type;

            $files = dbGetRows($sql, $db2);

            foreach ($files as $file2) {
                $isi_file = pg_unescape_bytea($file2['isi_file']);
                $ukuran = $file2['ukuran'];
                $mime = get_mime_by_extension($file2['nama_file']);
                $tmpfile = tempnam(sys_get_temp_dir(), "helpdesk");
                file_put_contents($tmpfile, $isi_file);
                $imageConfig = array('source_image' => $tmpfile);

                list($width, $height) = Image::getProperty($id, Image::IMAGE_ORIGINAL, $file_name, $folder);
                foreach (array(Image::IMAGE_ORIGINAL,Image::IMAGE_LARGE, Image::IMAGE_MEDIUM, Image::IMAGE_SMALL, Image::IMAGE_THUMB) as $size) {
                    $file_name = $file2['nama_file'];
                    $imageConfig['new_image'] = $path . Image::getFileName($id, $size, $file_name);

                    list($imageConfig['width'], $imageConfig['height']) = explode('x', $ciConfig['image'][strtolower($size)]);
                    if ($width > $height) {
                        unset($imageConfig['width']);
                    } else {
                        unset($imageConfig['height']);
                    }

                    $image = new CI_Image_lib();
                    $image->initialize($imageConfig);
                    $image->resize();
                    $image->clear();          
                
                }

                $imagick = new Imagick();
                $file = $path . Image::getFileName($id, Image::IMAGE_MEDIUM, $file_name);

                $imagick->readImage($file);
                
                $iWidth = $imagick->getImageWidth();
                $iHeight = $imagick->getImageHeight();
                
                if($iWidth != $iHeight) {
                    $x = $y = 0;
                    if($iWidth > $iHeight) {
                        $x = round(($iWidth-$iHeight)/2);
                        $iWidth = $iHeight;
                    }
                    else {
                        $y = round(($iHeight-$iWidth)/2);
                        $iHeight = $iWidth;
                    }

                    $imagick->cropImage($iWidth, $iHeight, $x, $y);
                }
                
                $size = 150;
                if(!empty($size)) {
                    $iWidth = $iHeight = $size;
                    $imagick->scaleImage($iWidth, $iHeight);
                }
                
                $file = str_replace('medium', 'rounded', $file);
                file_put_contents($file, $imagick);                
            }

        }
    }


    public function getOne($col, $filter){
        $sql = "select $col from posts $filter";
        $col = dbGetOne($sql);
        return $col;
    }

    public function getAll($page=0, $user_id,$order_by='asc', $is_die=false){
        $sql = "select p.*, u.name as nama_dinas, 'posts/photos/medium-'||MD5(MD5(CAST (p.user_id AS character varying)||'".$this->config->item('encryption_key')."')) ||'-'||p.file as nama_file,p.file,
            us.name as nama_user, us.photo as foto_user,
            case
                when us.photo is null then  'users/photos/nopic.png'
                else 'users/photos/thumb-'||MD5(CAST (p.user_id AS character varying)) ||'-'||us.photo
            end as foto_profil,
            p.vote as vote,
            pv.id as vote_exist,
            (select name from users where id = pa.user_id) as petugas,
            count(case when c.type='D' then c.id end) as jumlah_diskusi,
            count(case when c.type='K' then c.id end) as jumlah_komentar
            from posts p
            left join comments c on p.id = c.post_id
            left join post_assign pa on p.id = pa.post_id
            left join unitkerja u on p.dinas_id = u.idunit
            left join users us on p.user_id = us.id
            left join post_vote pv on p.id = pv.post_id and pv.user_id=".$user_id."
            $this->_filter
            group by p.id,u.name,us.id, pv.id,pa.user_id
            order by p.created_at $order_by ";
        if($page!=='semua'){
            $sql .=" limit $this->limit offset $page";
        }
        if($is_die){
            echo nl2br($sql);
            die();
        }
        $rows = dbGetRows($sql);
        if($rows){
            return $rows;
        }else{
            return null;
        }
        // return $rows;
    }

    public function getAllRates($page=0, $user_id,$order_by='asc',$role=Role::OPERATOR, $is_die=false){
        $sql = "select p.*, u.name as nama_dinas, 'posts/photos/medium-'||MD5(MD5(CAST (p.user_id AS character varying)||'".$this->config->item('encryption_key')."')) ||'-'||p.file as nama_file,p.file,
            us.name as nama_user, us.photo as foto_user,
            case
                when us.photo is null then  'users/photos/nopic.png'
                else 'users/photos/thumb-'||MD5(CAST (p.user_id AS character varying)) ||'-'||us.photo
            end as foto_profil,
            pr.rating as rating,
            pr.id as rating_exist,
            (select name from users where id = pa.user_id) as petugas,
            count(case when c.type='D' then c.id end) as jumlah_diskusi,
            count(case when c.type='K' then c.id end) as jumlah_komentar
            from posts p
            left join comments c on p.id = c.post_id
            left join post_assign pa on p.id = pa.post_id
            left join unitkerja u on p.dinas_id = u.idunit
            left join users us on p.user_id = us.id
            left join post_rating pr on pr.post_id = p.id and pr.tipe='".$role."'
            where p.status='S'
            $this->_filter
            group by p.id,u.name,us.id,pr.rating,pr.id,pa.user_id
            order by p.created_at $order_by ";
        if($page!=='semua'){
            $sql .=" limit $this->limit offset $page";
        }
        if($is_die){
            echo nl2br($sql);
            die();
        }
        $rows = dbGetRows($sql);
        if($rows){
            return $rows;
        }else{
            return null;
        }
        // return $rows;
    }

    public function getFavorit($filter=null, $is_die = false){
        $sql = "select p.*,'posts/photos/thumb-'||MD5(MD5(CAST (p.user_id AS character varying)||'".$this->config->item('encryption_key')."')) ||'-'||p.file as nama_file,p.file,
            us.name as nama_user, us.photo as foto_user,
            p.vote as vote
            from posts p
            left join users us on p.user_id = us.id
            where p.vote != 0 $filter
            group by p.id,us.id
            order by p.vote desc limit 5";
        if($is_die){
            echo nl2br($sql);
            die();
        }
        $rows = dbGetRows($sql);
        if($rows){
            return $rows;
        }else{
            return null;
        }
        // return $rows;
    }

    public function getDetail($post_id, $user_id, $is_die=false){
        $ciConfig = $this->config->item('utils');
        $sql = "select p.*, u.name as nama_dinas, '".$ciConfig['full_upload_dir_ip']."posts/photos/medium-'||MD5(MD5(CAST (p.user_id AS character varying)||'".$this->config->item('encryption_key')."')) ||'-'||p.file as nama_file,p.file,
            us.name as nama_user, us.photo as foto_user,us.no_hp as no_hp,
            case
                when us.photo is null then  'users/photos/nopic.png'
                else 'users/photos/thumb-'||MD5(CAST (p.user_id AS character varying)) ||'-'||us.photo
            end
             as foto_profil,
            p.vote as vote,
            pv.id as vote_exist,
            (select name from users where id = pa.user_id) as petugas, pa.user_id as petugas_id,
            count(case when c.type='D' then c.id end) as jumlah_diskusi,
            count(case when c.type='K' then c.id end) as jumlah_komentar
            from posts p
            left join comments c on p.id = c.post_id
            left join post_assign pa on p.id = pa.post_id
            left join unitkerja u on p.dinas_id = u.idunit
            left join users us on p.user_id = us.id
            left join post_vote pv on p.id = pv.post_id and pv.user_id=".$user_id."
            where p.id=".$post_id."
            group by p.id,u.name,us.id, pv.id,pa.user_id
            ";
        if($is_die){
            echo nl2br($sql);
            die();
        }
        $rows = dbGetRow($sql);
        if($rows){
            return $rows;
        }else{
            return null;
        }
    }



    public function updatePrivate($post_id, $private){
        $update = dbUpdate("posts", array('is_private' => $private), "id=".$post_id);
        if($update){
            return true;
        }else{
            return false;
        }
    }

    public function updateStatus($post_id, $status){
        $update = dbUpdate("posts", array('status' => $status), "id=".$post_id);
        if($update){
            if($status == Status::STATUS_DIARSIPKAN){
                self::updatePrivate($post_id, Status::STATUS_PRIVATE);
            }
            return true;
        }else{
            return false;
        }
    }

    public function isOperator($user_id, $post_id){
        $true = dbGetOne("select id from post_assign where user_id=".$user_id." and post_id=".$post_id);
        if($true){
            return true;
        }else{
            return false;
        }
    }

    public function changeOperator($user_id, $post_id){
        $this->_database->trans_begin();
        if(dbGetRow("select * from post_assign where post_id=".$post_id)){
            $change = dbUpdate("post_assign", array('user_id' => $user_id, 'updated_at' => Util::timeNow()), "post_id=".$post_id);
            if($change){
                return true;
            }
            return false;
        }else{
            $dataInsert = array(
                'user_id' => $user_id,
                'post_id' => $post_id
            );
            $insert = dbInsert("post_assign", $dataInsert);
            if($insert){
                return true;
            }
            return false;
        }
    }

    public function changeDinas($post_id, $dinas_id){
        $this->_database->trans_begin();
        $update = dbUpdate($this->_table, array('dinas_id' => (int)$dinas_id), "id=".$post_id);
        if($update){
            $hapusOperator = dbQuery("delete from post_assign where post_id=".$post_id);
            if($hapusOperator){
                $this->rotateTicket($post_id, $dinas_id);
            }
        }
        return false;
    }


    public function upVote($post_id, $user_id){
        $dataInsert = array(
            'user_id'       => $user_id,
            'post_id'       => $post_id
        );
        $insert = dbInsert('post_vote', $dataInsert);
        if($insert){
            $addVote = dbQuery("update $this->_table set vote = vote+1 where id=".$post_id);
            if($addVote){
                return true;
            }else{
                return false;;
            }
        }else{
            return false;
        }
    }

    public function unVote($post_id, $user_id){
        $delete = dbQuery("delete from post_vote where post_id=".$post_id." and user_id=".$user_id);
        if($delete){
            $addVote = dbQuery("update posts set vote = vote-1 where id=".$post_id);
            if($addVote){
                return true;
            }else{
                return false;;
            }
        }else{
            return false;
        }
    }

    public function checkView($post_id, $user_id){
        $exist = dbGetOne("select 1 from post_view where post_id=".$post_id." and user_id=".$user_id);
        if(!$exist){
            $insert = dbInsert("post_view", array("post_id" => $post_id, "user_id" => $user_id, "waktu_lihat" =>  date("Y-m-d H:i:s")));
            if($insert){
                $addView = dbQuery("update posts set view = view+1 where id=".$post_id);
            }
        }
    }

}