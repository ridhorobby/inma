<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends AppModel {

    private $_is_role = FALSE;
    private $_is_private = FALSE;
    public $limit = 20;
    public $user_id='';
    protected $_table = 'users';
    protected $_view = 'user_view';
    protected $_filter = "";
    protected $_column = "";

    /**
     * Digunakan untuk menampilkan data yang akan dikembalikan dari model 
     * Jika semua ditampilkan tidak perlu ditambahkan attribute $soft_data pada model
     * 
     * @var array 
     */
    protected $soft_data = array('id', 'name', 'username', 'photo', 'role', 'id_organisasi', 'email', 'no_hp', 'bank_rekening', 'no_rekening','is_active');
    private $public_data = array('id', 'name', 'username', 'photo', 'role', 'id_organisasi', 'email', 'no_hp', 'bank_rekening', 'no_rekening','is_active');

    /**
     * Digunakan untuk relasi antar model
     * 
     * @var array 
     */
    protected $has_many = array(
        'Post',
        'Comment',
        'Group_member',
        'Post_user',
        'Notification',
    );

    /**
     * Digunakan untuk mengcustom nama mapper
     * Jika penamaan standard (user_name => userName) tidak perlu ditambahkan pada attribute $mapper pada model
     * 
     * @var array 
     */
    protected $mapper = array(
        'password_reset_token' => 'resetToken',
        'no_hp'     => 'no_hp',
        'bank_rekening' => 'bank_rekening',
        'no_rekening' => 'no_rekening',
        'is_active' => 'is_active'
    );

    /**
     * Digunakan untuk labeling jika label (user_name => User Name) hanya menyertakan nama fieldnya saja.
     * @var array 
     */
    protected $label = array();

    /**
     * Digunakan untuk validasi create dan update data
     * 
     * @var array 
     */
    protected $validation = array(
        //'username' => 'required|min_length[3]|max_length[50]',
        //'email' => 'required|max_length[50]|valid_email',
    );

    public function filter($filter){
        $this->_filter= $filter;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function getUserID(){
        return $this->user_id;
    }

    public function setUserID($user_id){
        $this->user_id = $user_id;
        return $this->user_id;
    }

    public function is_role($role = FALSE) {
        $this->_is_role = $role;
        return $this;
    }

    public function is_private($private = FALSE) {
        $this->_is_private = $private;
        return $this;
    }

    public function getRole($role) {
        $roleUser = array(
            'id' => $role,
            'name' => Role::name($role)
        );
        return $roleUser;
    }

    public function mapper($row) {
        $newRow = array();
        foreach ($row as $key => $val) {
            //MAPPER ATRIBUTE
            if (is_null($this->soft_data) || in_array($key, $this->soft_data) || $this->show_all || $this->_is_private) {
                $newKey = key_exists($key, $this->mapper) ? $this->mapper[$key] : camelize($key);
                $newRow[$newKey] = $val;
            }
        }
        return $newRow;
    }

    protected function property($row) {
        if ($this->_is_role || $this->_is_private) {
            $row['role'] = $this->getRole($row['role']);
        } else {
            foreach ($row as $key => $val) {
                if (!in_array($key, $this->public_data) && !$this->show_all) {
                    unset($row[$key]);
                }
            }
        }
        if ( preg_match('/\s/',$row['photo']) ){
            $row['photo'] = str_replace(' ', '_', $row['photo']);
        }
        if (isset($row['photo'])) {
            $row['photo'] = Image::getImage($row['id'], $row['photo'], 'users/photos');
        }
        return $row;
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        $create = parent::create($data, $skip_validation, $return);
        return $create;
    }

    

    public function getListFor($userId) {
        return $this->order_by('name', 'ASC')->get_many_by(array('id !=' => $userId));
    }

    function getCount($role=NULL,$param=NULL) {
        if ($role!=NULL) {
            $filter = " where role='$role' ";
            if (!empty($param)) {
                $filter2 = " and (name ilike '%" . $param . "%' or username ilike '%" . $param . "%' or nm_lemb ilike '%" . $param . "%')";
            }
        } else {
            if (!empty($param)) {
                $filter2 .= " where (name ilike '%" . $param . "%' or username ilike '%" . $param . "%' or nm_lemb ilike '%" . $param . "%')";
            }
        }
        $sql = "select count(*) from users u 
        left join organisasi o on o.id_organisasi=u.id_organisasi $filter $filter2";
        return dbGetOne($sql);
    }

    function getUserNonMemberGroup($id) {
        $this->load->model('Group_model');
        $group = $this->Group_model->getRow($id);
        if ($group['type']=='H'){
            $filter = " and role in ('A','S','R') ";
            $sql = "select * from users where not exists 
                    (select 1 from group_members left join groups g on g.id=group_members.group_id where users.id = group_members.user_id and g.type='H') $filter";
        }else {
            $sql = "select * from users where not exists (select 1 from group_members where users.id = group_members.user_id and group_members.group_id = $id) $filter";
        }
        
        
        // echo nl2br($sql);
        // die();
        return dbGetRows($sql);
    }

    function getUsers($user_id,$page=0,$role=NULL,$param=NULL) {
        $offset = $page*$this->limit;

    /*    if (!self::isSuperAdministrator($user_id)){
            $my_helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup($user_id);
            $my_helpdesk_group_id=$my_helpdesk_group['id'];
            $filter_my_helpdesk = " and (gm.group_id is not null or u.role='O') ";
            $join_group_members = " left join group_members gm on gm.user_id=u.id and gm.group_id= $my_helpdesk_group_id ";

        }
    */    
        if ($role!=NULL) {
            $filter = " and u.role='$role' ";
            if (!empty($param)) {
                $filter2 = " and (u.name ilike '%" . $param . "%' or username ilike '%" . $param . "%' or uk.name ilike '%" . $param . "%')";
            }
        } else {
            if (!empty($param)) {
                $filter2 .= " and (u.name ilike '%" . $param . "%' or username ilike '%" . $param . "%' or uk.name ilike '%" . $param . "%')";
            }
        }

        $sql = "select u.*, uk.name as nm_lemb
            from users u 
                left join unitkerja uk on u.id_dinas = uk.iddinas and level = 2
            where u.id is not null $filter $filter2
            order by u.name asc limit $this->limit offset $offset";
        $users = dbGetRows($sql);

        $offset=($page+1)*$this->limit;
        $sql = "select u.*, uk.name as nm_lemb
            from users u 
                left join unitkerja uk on u.id_dinas = uk.iddinas and level = 2
            where u.id is not null $filter $filter2
            order by u.name asc limit $this->limit offset $offset";
        $data = dbGetRows($sql);
    /*    
        $sql = "select u.*, o.nm_lemb from users u 
        left join organisasi o on o.id_organisasi=u.id_organisasi 
        $join_group_members
        where u.id is not null $filter_my_helpdesk $filter $filter2 order by u.name asc limit $this->limit  offset $offset";
        $users = dbGetRows($sql);
        // die($sql);
        $offset=($page+1)*$this->limit;
        $sql = "select u.*, o.nm_lemb from users u 
        left join organisasi o on o.id_organisasi=u.id_organisasi 
        $join_group_members
        where u.id is not null $filter_my_helpdesk $filter $filter2 order by u.name asc limit $this->limit  offset $offset";
        
        $data = dbGetRows($sql);
    */
        foreach ($users as $key => $value) {
            $users[$key]['user_photo'] = $this->getUserPhoto($value['id'],$value['photo']);
            // if ($this->isSuperAdministrator($value['id'])){
            //     $users[$key]['helpdesk_name'] = "Semua Helpdesk";
            // }
            /*
            if ($this->isStaff($value['id']) and !$this->isSuperAdministrator($value['id'])) {
                $helpdesk = $this->Group_member_model->getMyHelpdeskGroup($value['id']);
                $users[$key]['helpdesk_name'] = $helpdesk['name'];
            */
                // if ($helpdesk['name']==NULL) {
                //     $users[$key]['helpdesk_name']="Belum Masuk Helpdesk";
                // } else {
                //     $users[$key]['helpdesk_name'] = 'Helpdesk '.$helpdesk['helpdesk_name'];
                // }
            // }
            
        }
        if (empty($data)) {
            $users[0]['user_offset']=0;
        } else {
            $users[0]['user_offset']=1;
        }
        // echo "<pre>";print_r($users);die();
        return $users;
    }
    
    function getUser() {
        $sql = "select $this->_column from $this->_table $this->_filter";
        //die($sql);
        return dbGetRows($sql);
    }

    function getUserById() {
        $sql = "select $this->_column from $this->_table $this->_filter";
        return dbGetRow($sql);
    }   

    public function isAdministrator($user_id){
        $sql = "select 1 from users u where id='$user_id' and role in ('A','R')";
        if (dbGetOne($sql))
            return true;
        return false;
    }


    function updateUserRole($id, $role) {
        $record = array();
        $record['role'] = $role;
        return dbUpdate('users', $record, "id='$id'");
    }

    function updateUserExclude($id, $exclude) {
        $record = array();
        $record['is_excluded'] = $exclude;
        if ($exclude==NULL || $exclude==0){
            $record['is_excluded']=1;
        } else {
            $record['is_excluded']=0;
        }
        return dbUpdate('users', $record, "id='$id'");
    }

    


    public function getOne(){
        $sql = "select $this->_column from users $this->_filter";
        return dbGetOne($sql);
    }

    public function activate($user_id){
        $update = dbUpdate($this->table, array('is_active' => 1),"id=".$user_id);
        if($update){
            return true;
        }else{
            return false;
        }
    }

    public function setNonactive($user_id){
        $update = dbUpdate($this->_table, array('is_active' => 0, 'updated_at' =>date('Y-m-d G:i:s', time())),"id=".$user_id);
        if($update){
            return true;
        }else{
            return false;
        }
    }
    public function setActive($user_id){
        $update = dbUpdate($this->_table, array('is_active' => 1, 'updated_at' =>date('Y-m-d G:i:s', time())),"id=".$user_id);
        if($update){
            return true;
        }else{
            return false;
        }
    }

    public function save($data, $user_id){
        $save = parent::save($user_id, $data, true);
        if($save){
            return true;
        }else{
            return false;
        }
    }

    public function getAll($filter){
        $sql = "select * from $this->_view
                $filter
        ";
        $data = dbGetRows($sql);
        return $data;
    }

    public function getDesainerWithGajiBulan(){
        $sql = "select * from $this->_view us
                left outer join gaji_desainer gd on us.id = gd.desainer_id 
                where role='".Role::DESAINER."'
        ";
    }

}