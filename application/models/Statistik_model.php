<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik_model extends AppModel {

    function getUserBeban($chosen_helpdesk=NULL, $is_kopertis=false){
        if ($chosen_helpdesk!=NULL and $chosen_helpdesk!=''){
            $group = $this->Group_model->getGroupBy('name',$chosen_helpdesk);
            $group_default = $this->Group_model->getGroupDefault();
            // if ($group_default['id']!=$group['id']) {
                $members = $this->Group_member_model->getAllMembers($group['id']);
                $member_ids = array();
                foreach ($members as $key => $value) {
                    $member_ids[] = "'".$value['id']."'";
                }
                $member_id = implode(',',$member_ids);
                $filter_member = " and u.id in ($member_id) ";
            // }
            
            $group_id = $group['id'];
            // if ($chosen_helpdesk==$helpdesk_default){
            //     $filter_helpdesk = " and (p.group_id is null or p.group_id=$group_id) ";
            // } else {
                $filter_helpdesk = " and p.group_id=$group_id ";
            // }
        }

        $role = "'A', 'S', 'R'";
        if ($is_kopertis){
            $this->load->model('Group_member_model');
            $members = $this->Group_member_model->getAllAdminKopertis();
            $member_ids = array();
            foreach ($members as $key => $value) {
                $member_ids[] = "'".$value['id']."'";
            }
            $member_id = implode(',',$member_ids);
            $filter_member = " and u.id in ($member_id) ";
            $role = "'O'";
        }


        $sql = 
        "
select id, name, photo as user_photo, sum(case when bobot is null then 0 else bobot end) as bobot, sum(B) as B, sum(P) as P, sum(S) as S 
from (
    select t.id, u.photo, u.name,sum(case when t.status='B' then 1 when t.status='P' then 1 when (t.status='S' and 
     month=extract(month from now()) and year=extract(year from now())) then 0.01 when t.status is null then 0 end) as bobot, 
    sum(case when t.status='B' then 1 else 0 end) as B, sum(case when t.status='P' then 1 else 0 end) as P, sum(case when (t.status='S' and 
     month=extract(month from now()) and year=extract(year from now())) then 1 else 0 end) as S
    from (
        select u.id, u.photo, p.status, p.created_at, extract(month from p.created_at) as month, extract(year from p.created_at) as year
        from (
            select id,photo from users u where role in ($role) and (is_excluded is NULL or is_excluded=0) $filter_member) u 
        left join post_assign a on u.id=a.user_id
        left join posts p on a.post_id=p.id and p.status in ('B', 'P', 'S') $filter_helpdesk
        ) t
    join users u on u.id=t.id group by t.id, u.name,u.photo) s
group by id, name,photo
order by bobot, name
        ";
        // echo nl2br($sql);
        // die();
        $data = dbGetRows($sql);
        foreach ($data as $key => $d) {
            $data[$key]['user_photo'] = $this->User_model->getUserPhoto($d['id'],$d['user_photo']);
        }
        return $data;
    }

    function getAvgUserBeban($chosen_helpdesk=NULL, $is_kopertis=false){
        if ($chosen_helpdesk!=NULL and $chosen_helpdesk!=''){
            $group = $this->Group_model->getGroupBy('name',$chosen_helpdesk);
            $group_default = $this->Group_model->getGroupDefault();
            // if ($group_default['id']!=$group['id']) {
                $members = $this->Group_member_model->getAllMembers($group['id']);
                $member_ids = array();
                foreach ($members as $key => $value) {
                    $member_ids[] = "'".$value['id']."'";
                }
                $member_id = implode(',',$member_ids);
                $filter_member = " and u.id in ($member_id) ";
            // }
            
            $group_id = $group['id'];
            // if ($chosen_helpdesk==$helpdesk_default){
            //     $filter_helpdesk = " and (p.group_id is null or p.group_id=$group_id) ";
            // } else {
                $filter_helpdesk = " and p.group_id=$group_id ";
            // }
        }

        $role = "'A', 'S', 'R'";
        if ($is_kopertis){
            $this->load->model('Group_member_model');
            $members = $this->Group_member_model->getAllAdminKopertis();
            $member_ids = array();
            foreach ($members as $key => $value) {
                $member_ids[] = "'".$value['id']."'";
            }
            $member_id = implode(',',$member_ids);
            $filter_member = " and u.id in ($member_id) ";
            $role = "'O'";
        }

        $sql = 
        "select avg(bobot) as avg_bobot from
(select id, name, sum(bobot) as bobot, sum(B) as B, sum(P) as P, sum(S) as S 
from (
    select t.id, u.name,sum(case when t.status='B' then 1 when t.status='P' then 1 when (t.status='S' and 
     month=extract(month from now()) and year=extract(year from now())) then 0.01 when t.status is null then 0 end) as bobot, 
    sum(case when t.status='B' then 1 else 0 end) as B, sum(case when t.status='P' then 1 else 0 end) as P, sum(case when (t.status='S' and 
     month=extract(month from now()) and year=extract(year from now())) then 1 else 0 end) as S
    from (
        select u.id, p.status, p.created_at, extract(month from p.created_at) as month, extract(year from p.created_at) as year
        from (
            select id from users u where role in ($role) and (is_excluded is NULL or is_excluded=0) $filter_member) u 
        left join post_assign a on u.id=a.user_id
        left join posts p on a.post_id=p.id and p.status in ('B', 'P', 'S')
        ) t
    join users u on u.id=t.id group by t.id, u.name) s
group by id, name
order by bobot, name) x
        ";
        $data = dbGetRows($sql);
        return $data;
    }

    function getUserStatandBeban(){
        //bobot + response time + processing time + solving time
        $sql = "select s.id, name, sum(bobot) as bobot, sum(B) as B, sum(P) as P, sum(S) as S,round(avg(r.response_time)) as response_time, round(avg(r.processing_time)) as processing_time, 
        round(avg(r.solving_time)) as solving_time 
    from (
        select t.id, u.name, sum(case when t.status='B' then 1 when t.status='P' then 1 when t.status='S' then 0.1 when t.status is null then 0 end) as bobot, 0 as B, 0 as P, 0 as S 
        from (select u.id, p.status from (select id from users where role in ('A', 'S') and (is_excluded is NULL or is_excluded=0)) u 
        left join post_assign a on u.id=a.user_id left join posts p on a.post_id=p.id and p.status in ('B', 'P', 'S')) t join users u on u.id=t.id group by t.id, u.name 
        union 
        select t.id, u.name,0 as bobot, sum(case when t.status='B' then 1 end) as B, sum(case when t.status='P' then 1 end) as P, sum(case when t.status='S' then 1 end) as S 
        from (select u.id, p.status from (select id from users where role in ('A', 'S') and (is_excluded is NULL or is_excluded=0)) u 
        left join post_assign a on u.id=a.user_id left join posts p on a.post_id=p.id and p.status in ('B', 'P', 'S')) t join users u on u.id=t.id group by t.id, u.name ) s 
    left join (select x.id,x.user_id,x.post_created_at,first_comment,last_comment,round(avg(x.response_time)) as response_time, round(avg(x.processing_time)) as processing_time,
        round(avg(x.solving_time)) as solving_time
        from (select c.id, a.user_id, 
        post_created_at, first_comment, last_comment, 
        DATE_PART('day',first_comment-post_created_at)*24*60+ DATE_PART('hour',first_comment-post_created_at)*60+ 
        DATE_PART('minute',first_comment-post_created_at) as response_time, 
        DATE_PART('day',last_comment-first_comment)*24*60+ DATE_PART('hour',last_comment-first_comment)*60+ 
        DATE_PART('minute',last_comment-first_comment) as processing_time,
        DATE_PART('day',last_comment-post_created_at)*24*60+ DATE_PART('hour',last_comment-post_created_at)*60+ 
        DATE_PART('minute',last_comment-post_created_at) as solving_time
        from (select p.id, p.created_at as post_created_at, min(c.created_at) as first_comment, max(c.created_at) as last_comment from posts p 
        inner join comments c on c.post_id=p.id group by p.id, post_created_at) c 
        left join post_assign a on a.post_id=c.id order by c.id asc) x
        group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
    group by s.id, name order by name
    ";
        $data = dbGetRows($sql);
        return $data;  
    }

    function getUserStat($helpdesk=NULL, $start=NULL, $end=NULL, $is_kopertis=false){
        $filter_member = '';
        if ($helpdesk!=NULL and $helpdesk!='' and $helpdesk!='Semua Unit'){
            $group = $this->Group_model->getGroupBy('name',$helpdesk);
            $group_default = $this->Group_model->getGroupDefault();
            $helpdesk_default = $group_default['name'];
            // if ($group_default['id']!=$group['id']) {
                $members = $this->Group_member_model->getAllMembers($group['id']);
                $member_ids = array();
                foreach ($members as $key => $value) {
                    $member_ids[] = "'".$value['id']."'";
                }
                $member_id = implode(',',$member_ids);
                $filter_member = " and u.id in ($member_id) ";
            // }
            
            $group_id = $group['id'];
            // if ($helpdesk==$helpdesk_default){
            //     $filter_helpdesk = " and (p.group_id is null or p.group_id=$group_id) ";
            // } else {
                // $filter_helpdesk = " and p.group_id=$group_id ";
                // $filter_helpdesk = " and detail.group_id=$group_id ";
            // }
        }
        $role = "'A', 'S', 'R'";
        if ($is_kopertis){
            $this->load->model('Group_member_model');
            $members = $this->Group_member_model->getAllAdminKopertis();
            $member_ids = array();
            foreach ($members as $key => $value) {
                $member_ids[] = "'".$value['id']."'";
            }
            $member_id = implode(',',$member_ids);
            $filter_member = " and u.id in ($member_id) ";
            $role = "'O'";
        }

        $filter_post = 'where 1=1 ';
        $filter_user = 'where 1=1 ';
        if (!empty($start) and !empty($end)){

            $filter_post .= " and to_date(to_char(post_created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(post_created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
            // $filter_user = " where (to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY')) or status is null ";
            $filter_user .= " and ((to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY')) or status is null )";
        } else {
            $this->load->model('Post_model');

            $date_start = $this->Post_model->getOne("to_char(created_at::date,'DD-MM-YYYY')", "where id=(select min(p.id) from posts p  
                left join post_assign pa on pa.post_id=p.id
                left join users u on u.id=pa.user_id
                where u.role in ($role) and p.type='T' )");
            $date_end = $this->Post_model->getOne("to_char(created_at::date,'DD-MM-YYYY')", "where id=(select max(p.id) from posts p  
                left join post_assign pa on pa.post_id=p.id
                left join users u on u.id=pa.user_id
                where u.role in ($role) and p.type='T' )");
        }
        // tanpa optimise
//         $sql="
// select s.id, name, S,COALESCE(round(avg(r.response_time)),0) as response_time, COALESCE(round(avg(r.processing_time)),0) as processing_time, COALESCE(round(avg(r.solving_time)),0) as solving_time, 
// jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
// from (  
//         select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
//         from (
//             select u.id, p.status, p.id as post_id, COALESCE(pr.rating,0) as rating, p.created_at
//             from (
//                 select id from users where role in ($role) and (is_excluded is NULL or is_excluded=0)
//             ) u 
//             left join post_assign a on u.id=a.user_id 
//             left join posts p on a.post_id=p.id and p.status='S' $filter_helpdesk
//             left join post_rating pr on p.id=pr.post_id
//             $filter_user order by post_id
//         ) t 
//         join users u on u.id=t.id $filter_member group by t.id, u.name
//     ) s 
//     left join (
//         select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time-x.holiday_response)),0) as response_time, COALESCE(round(avg(x.processing_time-x.holiday_processing)),0) as processing_time, 
//         COALESCE(round(avg(x.solving_time-x.holiday_response-x.holiday_processing)),0) as solving_time  

//         from (
//             select detail.*, count(hp.date)*60*24 as holiday_processing from(
//             select c.id, a.user_id, post_created_at, first_comment, last_comment, count(hr.date)*60*24 as holiday_response,
//             DATE_PART('day',first_comment-post_created_at)*24*60+ DATE_PART('hour',first_comment-post_created_at)*60+ DATE_PART('minute',first_comment-post_created_at) as response_time, 
//             DATE_PART('day',last_comment-first_comment)*24*60+ DATE_PART('hour',last_comment-first_comment)*60+ DATE_PART('minute',last_comment-first_comment) as processing_time, 
//             DATE_PART('day',last_comment-post_created_at)*24*60+ DATE_PART('hour',last_comment-post_created_at)*60+ DATE_PART('minute',last_comment-post_created_at) as solving_time 
//             from (
//                 select p.id, p.created_at as post_created_at, min(c.created_at) as first_comment, max(c.created_at) as last_comment 
//                 from posts p inner join comments c on c.post_id=p.id
//                 where p.status='S' $filter_helpdesk
//                 group by p.id, post_created_at
//             ) c 
//             left join post_assign a on a.post_id=c.id 
//             left join holidays hr on hr.date between post_created_at and first_comment
//             group by c.id, a.user_id, post_created_at, first_comment, last_comment, response_time, processing_time, solving_time
//             order by c.id asc ) detail 
//             left join holidays hp on hp.date between first_comment and last_comment
//             group by detail.id, user_id, post_created_at, first_comment, last_comment, response_time, processing_time, solving_time, holiday_response
//     ) x 
//     $filter_post
//     group by id, post_created_at, first_comment,last_comment, user_id order by id
// ) r on r.user_id=s.id 
    
// group by s.id, name, jml_rating,S order by solvability desc, s desc, processing_time asc, response_time asc,name";
        
        // optimise perhitungan waktu

//         $sql="
// select s.id, name, S,COALESCE(round(avg(r.response_time)),0) as response_time, COALESCE(round(avg(r.processing_time)),0) as processing_time, COALESCE(round(avg(r.solving_time)),0) as solving_time, 
// jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
// from (  
//         select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
//         from (
//             select u.id, p.status, p.id as post_id, COALESCE(pr.rating,0) as rating, p.created_at
//             from (
//                 select id from users where role in ($role) and (is_excluded is NULL or is_excluded=0)
//             ) u 
//             left join post_assign a on u.id=a.user_id 
//             left join posts p on a.post_id=p.id and p.status='S' and p.group_id=$group_id
//             left join post_rating pr on p.id=pr.post_id
//             $filter_user order by post_id
//         ) t 
//         join users u on u.id=t.id $filter_member group by t.id, u.name
//     ) s 
//     left join (
//         select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time-x.holiday_response)),0) as response_time, COALESCE(round(avg(x.processing_time-x.holiday_processing)),0) as processing_time, 
//         COALESCE(round(avg(x.solving_time-x.holiday_response-x.holiday_processing)),0) as solving_time  

//         from performance_view x 
//     $filter_post and x.group_id=$group_id
//     group by id, post_created_at, first_comment,last_comment, user_id order by id
// ) r on r.user_id=s.id 
    
// group by s.id, name, jml_rating,S order by solvability desc, s desc, processing_time asc, response_time asc,name";

        $sql="
select s.id, name, S,COALESCE(round(avg(r.response_time)),0) as response_time, COALESCE(round(avg(r.processing_time)),0) as processing_time, COALESCE(round(avg(r.solving_time)),0) as solving_time, 
jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
from (  
        select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
        from (
            select * from rating_view p
            $filter_user and group_id=$group_id order by post_id
        ) t 
        join users u on u.id=t.id $filter_member group by t.id, u.name
    ) s 
    left join (
        select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time-x.holiday_response)),0) as response_time, COALESCE(round(avg(x.processing_time-x.holiday_processing)),0) as processing_time, 
        COALESCE(round(avg(x.solving_time-x.holiday_response-x.holiday_processing)),0) as solving_time  

        from performance_view x 
    $filter_post and x.group_id=$group_id
    group by id, post_created_at, first_comment,last_comment, user_id order by id
) r on r.user_id=s.id 
    
group by s.id, name, jml_rating,S order by solvability desc, s desc, processing_time asc, response_time asc,name";

// echo nl2br(str_replace("\t", '&nbsp;&nbsp;&nbsp;&nbsp;', $sql));
// die();
        $data = dbGetRows($sql);
        return $data;
    }

    function getUserStatRatingedTicket( $helpdesk=NULL, $start=NULL, $end=NULL, $is_kopertis=false){
        $filter_member = '';
        if ($helpdesk!=NULL and $helpdesk!='' and $helpdesk!='Semua Unit'){
            $group = $this->Group_model->getGroupBy('name',$helpdesk);
            $group_default = $this->Group_model->getGroupDefault();
            $helpdesk_default = $group_default['name'];
            // $members = $this->Group_member_model->getAllMembers($group['id']);
            // $member_ids = array();
            // foreach ($members as $key => $value) {
            //     $member_ids[] = "'".$value['id']."'";
            // }
            // $member_id = implode(',',$member_ids);
            // $filter_member = " and u.id in ($member_id) ";
            // if ($group_default['id']!=$group['id']) {
                $members = $this->Group_member_model->getAllMembers($group['id']);
                $member_ids = array();
                foreach ($members as $key => $value) {
                    $member_ids[] = "'".$value['id']."'";
                }
                $member_id = implode(',',$member_ids);
                $filter_member = " and u.id in ($member_id) ";
            // }
            $group_id = $group['id'];
            // if ($helpdesk==$helpdesk_default){
            //     $filter_helpdesk = " and (p.group_id is null or p.group_id=$group_id) ";
            // } else {
                $filter_helpdesk = " and p.group_id=$group_id ";
            // }
        }

        $role = "'A', 'S', 'R'";
        if ($is_kopertis){
            $this->load->model('Group_member_model');
            $members = $this->Group_member_model->getAllAdminKopertis();
            $member_ids = array();
            foreach ($members as $key => $value) {
                $member_ids[] = "'".$value['id']."'";
            }
            $member_id = implode(',',$member_ids);
            $filter_member = " and u.id in ($member_id) ";
            $role = "'O'";
        }

        $filter_post = 'where 1=1 ';
        $filter_user = 'where 1=1 ';
        if (!empty($start) and !empty($end)){
            $filter_post .= " and to_date(to_char(post_created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(post_created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
            // $filter_user = " and (to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY')) or status is null ";
            $filter_user .= " and ((to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY')) or status is null )";
        } else {
            $this->load->model('Post_model');

            $date_start = $this->Post_model->getOne("to_char(created_at::date,'DD-MM-YYYY')", "where id=(select min(p.id) from posts p  
                left join post_assign pa on pa.post_id=p.id
                left join users u on u.id=pa.user_id
                where u.role in ($role) and p.type='T' )");
            $date_end = $this->Post_model->getOne("to_char(created_at::date,'DD-MM-YYYY')", "where id=(select max(p.id) from posts p  
                left join post_assign pa on pa.post_id=p.id
                left join users u on u.id=pa.user_id
                where u.role in ($role) and p.type='T' )");
        }

//         $sql="select s.id, name, S,COALESCE(round(avg(r.response_time)),0) as response_time, COALESCE(round(avg(r.processing_time)),0) as processing_time, COALESCE(round(avg(r.solving_time)),0) as solving_time, 
// jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
// from (  select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
//     from (select u.id, p.status, p.id as post_id, COALESCE(pr.rating,0) as rating, p.created_at
//         from (select id from users where role in ($role) and (is_excluded is NULL or is_excluded=0)) u 
//         left join post_assign a on u.id=a.user_id 
//         left join posts p on a.post_id=p.id and p.status='S' $filter_helpdesk
//         left join post_rating pr on p.id=pr.post_id
//         where (pr.rating is not null or pr.rating!=0 )  $filter_user order by post_id) t 
//     join users u on u.id=t.id $filter_member group by t.id, u.name) s 
// left join (
//     select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time-x.holiday_response)),0) as response_time, COALESCE(round(avg(x.processing_time-x.holiday_processing)),0) as processing_time, 
//         COALESCE(round(avg(x.solving_time-x.holiday_response-x.holiday_processing)),0) as solving_time  

//         from (
//             select detail.*, count(hp.date)*60*24 as holiday_processing from(
//             select c.id, a.user_id, post_created_at, first_comment, last_comment, count(hr.date)*60*24 as holiday_response,
//             DATE_PART('day',first_comment-post_created_at)*24*60+ DATE_PART('hour',first_comment-post_created_at)*60+ DATE_PART('minute',first_comment-post_created_at) as response_time, 
//             DATE_PART('day',last_comment-first_comment)*24*60+ DATE_PART('hour',last_comment-first_comment)*60+ DATE_PART('minute',last_comment-first_comment) as processing_time, 
//             DATE_PART('day',last_comment-post_created_at)*24*60+ DATE_PART('hour',last_comment-post_created_at)*60+ DATE_PART('minute',last_comment-post_created_at) as solving_time 
//             from (
//                 select p.id, p.created_at as post_created_at, min(c.created_at) as first_comment, max(c.created_at) as last_comment 
//                 from posts p inner join comments c on c.post_id=p.id
//                 where p.status='S' $filter_helpdesk
//                 group by p.id, post_created_at
//             ) c 
//             left join post_assign a on a.post_id=c.id 
//             left join holidays hr on hr.date between post_created_at and first_comment
//             group by c.id, a.user_id, post_created_at, first_comment, last_comment, response_time, processing_time, solving_time
//             order by c.id asc ) detail 
//             left join holidays hp on hp.date between first_comment and last_comment
//             group by detail.id, user_id, post_created_at, first_comment, last_comment, response_time, processing_time, solving_time, holiday_response) x 
//     $filter_post
//     group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
    
// group by s.id, name, jml_rating,S order by solvability desc, s desc, processing_time asc, response_time asc,name";

// optimise performance view
//         $sql="select s.id, name, S,COALESCE(round(avg(r.response_time)),0) as response_time, COALESCE(round(avg(r.processing_time)),0) as processing_time, COALESCE(round(avg(r.solving_time)),0) as solving_time, 
// jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
// from (  select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
//     from (select u.id, p.status, p.id as post_id, COALESCE(pr.rating,0) as rating, p.created_at
//         from (select id from users where role in ($role) and (is_excluded is NULL or is_excluded=0)) u 
//         left join post_assign a on u.id=a.user_id 
//         left join posts p on a.post_id=p.id and p.status='S' $filter_helpdesk
//         left join post_rating pr on p.id=pr.post_id
//         where (pr.rating is not null or pr.rating!=0 )  $filter_user order by post_id) t 
//     join users u on u.id=t.id $filter_member group by t.id, u.name) s 
// left join (
//     select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time-x.holiday_response)),0) as response_time, COALESCE(round(avg(x.processing_time-x.holiday_processing)),0) as processing_time, 
//         COALESCE(round(avg(x.solving_time-x.holiday_response-x.holiday_processing)),0) as solving_time  

//         from performance_view x 
//     $filter_post and x.group_id=$group_id
//     group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
    
// group by s.id, name, jml_rating,S order by solvability desc, s desc, processing_time asc, response_time asc,name";

        $sql="select s.id, name, S,COALESCE(round(avg(r.response_time)),0) as response_time, COALESCE(round(avg(r.processing_time)),0) as processing_time, COALESCE(round(avg(r.solving_time)),0) as solving_time, 
jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
from (  select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
    from rating_view p 
    $filter_user and group_id=$group_id order by post_id) t 
    join users u on u.id=t.id $filter_member group by t.id, u.name) s 
left join (
    select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time-x.holiday_response)),0) as response_time, COALESCE(round(avg(x.processing_time-x.holiday_processing)),0) as processing_time, 
        COALESCE(round(avg(x.solving_time-x.holiday_response-x.holiday_processing)),0) as solving_time  

        from performance_view x 
    $filter_post and x.group_id=$group_id
    group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
    
group by s.id, name, jml_rating,S order by solvability desc, s desc, processing_time asc, response_time asc,name";

// echo nl2br($sql);
// die();
        $data = dbGetRows($sql);
        return $data;
    }

    function getStd( $helpdesk=NULL,$start=NULL, $end=NULL, $is_kopertis=false){
        if ($helpdesk!=NULL and $helpdesk!='' and $helpdesk!='Semua Unit'){
            $group = $this->Group_model->getGroupBy('name',$helpdesk);
            $group_default = $this->Group_model->getGroupDefault();
            $helpdesk_default = $group_default['name'];
            $group_id = $group['id'];
            // if ($helpdesk==$helpdesk_default){
            //     $filter_helpdesk = " and (p.group_id is null or p.group_id=$group_id) ";
            // } else {
                $filter_helpdesk = " and p.group_id=$group_id ";
            // }
        }
        $role = "'A', 'S', 'R'";
        if ($is_kopertis){
            $this->load->model('Group_member_model');
            $members = $this->Group_member_model->getAllAdminKopertis();
            $member_ids = array();
            foreach ($members as $key => $value) {
                $member_ids[] = "'".$value['id']."'";
            }
            $member_id = implode(',',$member_ids);
            $filter_member = " and u.id in ($member_id) ";
            $role = "'O'";
        }
        $filter_post = 'where 1=1';
        $filter_user = 'where 1=1';
        if (!empty($start) and !empty($end)){
            $filter_post .= " and to_date(to_char(post_created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(post_created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
            $filter_user .= " and ((to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY')) or status is null )";
            // $filter_user = " and ((to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY')) or status is null )";
        } else {
            $this->load->model('Post_model');

            $date_start = $this->Post_model->getOne("to_char(created_at::date,'DD-MM-YYYY')", "where id=(select min(p.id) from posts p  
                left join post_assign pa on pa.post_id=p.id
                left join users u on u.id=pa.user_id
                where u.role in ($role) and p.type='T' )");
            $date_end = $this->Post_model->getOne("to_char(created_at::date,'DD-MM-YYYY')", "where id=(select max(p.id) from posts p  
                left join post_assign pa on pa.post_id=p.id
                left join users u on u.id=pa.user_id
                where u.role in ($role) and p.type='T' )");
        }
//         $sql="select sum(s) as total_s,avg(s) as avg_s, avg(response_time) as avg_response,avg(processing_time) as avg_process, avg(solving_time) as avg_solving, avg(solvability) as solvability from (select s.id, name, S,round(avg(r.response_time)) as response_time, round(avg(r.processing_time)) as processing_time, round(avg(r.solving_time)) as solving_time,jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
// from (  select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
//     from (select u.id, p.status, p.id as post_id, COALESCE(pr.rating,0) as rating, p.created_at
//         from (select id from users where role in ($role) and (is_excluded is NULL or is_excluded=0)) u 
//         left join post_assign a on u.id=a.user_id 
//         left join posts p on a.post_id=p.id and p.status='S' $filter_helpdesk
//         left join post_rating pr on p.id=pr.post_id
//         $filter_user order by post_id) t 
//     join users u on u.id=t.id group by t.id, u.name) s 
// left join (
//     select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time-x.holiday_response)),0) as response_time, COALESCE(round(avg(x.processing_time-x.holiday_processing)),0) as processing_time, 
//         COALESCE(round(avg(x.solving_time-x.holiday_response-x.holiday_processing)),0) as solving_time  

//         from (
//             select detail.*, count(hp.date)*60*24 as holiday_processing from(
//             select c.id, a.user_id, post_created_at, first_comment, last_comment, count(hr.date)*60*24 as holiday_response,
//             DATE_PART('day',first_comment-post_created_at)*24*60+ DATE_PART('hour',first_comment-post_created_at)*60+ DATE_PART('minute',first_comment-post_created_at) as response_time, 
//             DATE_PART('day',last_comment-first_comment)*24*60+ DATE_PART('hour',last_comment-first_comment)*60+ DATE_PART('minute',last_comment-first_comment) as processing_time, 
//             DATE_PART('day',last_comment-post_created_at)*24*60+ DATE_PART('hour',last_comment-post_created_at)*60+ DATE_PART('minute',last_comment-post_created_at) as solving_time 
//             from (
//                 select p.id, p.created_at as post_created_at, min(c.created_at) as first_comment, max(c.created_at) as last_comment 
//                 from posts p inner join comments c on c.post_id=p.id
//                 where p.status='S' $filter_helpdesk
//                 group by p.id, post_created_at
//             ) c 
//             left join post_assign a on a.post_id=c.id 
//             left join holidays hr on hr.date between post_created_at and first_comment
//             group by c.id, a.user_id, post_created_at, first_comment, last_comment, response_time, processing_time, solving_time
//             order by c.id asc ) detail 
//             left join holidays hp on hp.date between first_comment and last_comment
//             group by detail.id, user_id, post_created_at, first_comment, last_comment, response_time, processing_time, solving_time, holiday_response) x 
//     $filter_post
//     group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
// group by s.id, name, jml_rating,S order by name) avg";

        // performance view
//         $sql="select sum(s) as total_s,avg(s) as avg_s, avg(response_time) as avg_response,avg(processing_time) as avg_process, avg(solving_time) as avg_solving, avg(solvability) as solvability from (select s.id, name, S,round(avg(r.response_time)) as response_time, round(avg(r.processing_time)) as processing_time, round(avg(r.solving_time)) as solving_time,jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
// from (  select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
//     from (select u.id, p.status, p.id as post_id, COALESCE(pr.rating,0) as rating, p.created_at
//         from (select id from users where role in ($role) and (is_excluded is NULL or is_excluded=0)) u 
//         left join post_assign a on u.id=a.user_id 
//         left join posts p on a.post_id=p.id and p.status='S' $filter_helpdesk
//         left join post_rating pr on p.id=pr.post_id
//         $filter_user order by post_id) t 
//     join users u on u.id=t.id group by t.id, u.name) s 
// left join (
//     select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time-x.holiday_response)),0) as response_time, COALESCE(round(avg(x.processing_time-x.holiday_processing)),0) as processing_time, 
//         COALESCE(round(avg(x.solving_time-x.holiday_response-x.holiday_processing)),0) as solving_time  

//         from performance_view x 
//     $filter_post and x.group_id=$group_id
//     group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
// group by s.id, name, jml_rating,S order by name) avg";

        $sql="select sum(s) as total_s,avg(s) as avg_s, avg(response_time) as avg_response,avg(processing_time) as avg_process, avg(solving_time) as avg_solving, avg(solvability) as solvability from (select s.id, name, S,round(avg(r.response_time)) as response_time, round(avg(r.processing_time)) as processing_time, round(avg(r.solving_time)) as solving_time,jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
from (  select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
    from (select * from rating_view
        $filter_user and group_id=$group_id order by post_id) t 
    join users u on u.id=t.id group by t.id, u.name) s 
left join (
    select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time-x.holiday_response)),0) as response_time, COALESCE(round(avg(x.processing_time-x.holiday_processing)),0) as processing_time, 
        COALESCE(round(avg(x.solving_time-x.holiday_response-x.holiday_processing)),0) as solving_time  

        from performance_view x 
    $filter_post and x.group_id=$group_id
    group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
group by s.id, name, jml_rating,S order by name) avg";
// echo nl2br($sql);
// die();
        $data = dbGetRows($sql);
        return $data;
    }

    function getStdRatingedTicket( $helpdesk=NULL,$start=NULL, $end=NULL, $is_kopertis=false){
        if ($helpdesk!=NULL and $helpdesk!='' and $helpdesk!='Semua Unit'){
            $group_default = $this->Group_model->getGroupDefault();
            $helpdesk_default = $group_default['name'];
            $group = $this->Group_model->getGroupBy('name',$helpdesk);
            $group_id = $group['id'];
            // if ($helpdesk==$helpdesk_default){
            //     $filter_helpdesk = " and (p.group_id is null or p.group_id=$group_id) ";
            // } else {
                $filter_helpdesk = " and p.group_id=$group_id ";
            // }
        }

        $role = "'A', 'S', 'R'";
        if ($is_kopertis){
            $this->load->model('Group_member_model');
            $members = $this->Group_member_model->getAllAdminKopertis();
            $member_ids = array();
            foreach ($members as $key => $value) {
                $member_ids[] = "'".$value['id']."'";
            }
            $member_id = implode(',',$member_ids);
            $filter_member = " and u.id in ($member_id) ";
            $role = "'O'";
        }

        $filter_post = 'where 1=1';
        $filter_user = '';
        if (!empty($start) and !empty($end)){
            $filter_post .= " and to_date(to_char(post_created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(post_created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
            // $filter_user = " and (to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY')) or status is null ";
            $filter_user .= " and ((to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY')) or status is null) ";
        } else {
            $this->load->model('Post_model');

            $date_start = $this->Post_model->getOne("to_char(created_at::date,'DD-MM-YYYY')", "where id=(select min(p.id) from posts p  
                left join post_assign pa on pa.post_id=p.id
                left join users u on u.id=pa.user_id
                where u.role in ($role) and p.type='T' )");
            $date_end = $this->Post_model->getOne("to_char(created_at::date,'DD-MM-YYYY')", "where id=(select max(p.id) from posts p  
                left join post_assign pa on pa.post_id=p.id
                left join users u on u.id=pa.user_id
                where u.role in ($role) and p.type='T' )");
        }
//         $sql="select sum(s) as total_s,avg(s) as avg_s, avg(response_time) as avg_response,avg(processing_time) as avg_process, avg(solving_time) as avg_solving, avg(solvability) as solvability from (select s.id, name, S,round(avg(r.response_time)) as response_time, round(avg(r.processing_time)) as processing_time, round(avg(r.solving_time)) as solving_time,jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
// from (  select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
//     from (select u.id, p.status, p.id as post_id, COALESCE(pr.rating,0) as rating, p.created_at
//         from (select id from users where role in ($role) and (is_excluded is NULL or is_excluded=0)) u 
//         left join post_assign a on u.id=a.user_id 
//         left join posts p on a.post_id=p.id and p.status='S' $filter_helpdesk
//         left join post_rating pr on p.id=pr.post_id
//         where (pr.rating is not null or pr.rating!=0 ) $filter_user order by post_id) t 
//     join users u on u.id=t.id group by t.id, u.name) s 
// left join (select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time-x.holiday_response)),0) as response_time, COALESCE(round(avg(x.processing_time-x.holiday_processing)),0) as processing_time, 
//         COALESCE(round(avg(x.solving_time-x.holiday_response-x.holiday_processing)),0) as solving_time  

//         from (
//             select detail.*, count(hp.date)*60*24 as holiday_processing from(
//             select c.id, a.user_id, post_created_at, first_comment, last_comment, count(hr.date)*60*24 as holiday_response,
//             DATE_PART('day',first_comment-post_created_at)*24*60+ DATE_PART('hour',first_comment-post_created_at)*60+ DATE_PART('minute',first_comment-post_created_at) as response_time, 
//             DATE_PART('day',last_comment-first_comment)*24*60+ DATE_PART('hour',last_comment-first_comment)*60+ DATE_PART('minute',last_comment-first_comment) as processing_time, 
//             DATE_PART('day',last_comment-post_created_at)*24*60+ DATE_PART('hour',last_comment-post_created_at)*60+ DATE_PART('minute',last_comment-post_created_at) as solving_time 
//             from (
//                 select p.id, p.created_at as post_created_at, min(c.created_at) as first_comment, max(c.created_at) as last_comment 
//                 from posts p inner join comments c on c.post_id=p.id
//                 where p.status='S' $filter_helpdesk
//                 group by p.id, post_created_at
//             ) c 
//             left join post_assign a on a.post_id=c.id 
//             left join holidays hr on hr.date between post_created_at and first_comment
//             group by c.id, a.user_id, post_created_at, first_comment, last_comment, response_time, processing_time, solving_time
//             order by c.id asc ) detail 
//             left join holidays hp on hp.date between first_comment and last_comment
//             group by detail.id, user_id, post_created_at, first_comment, last_comment, response_time, processing_time, solving_time, holiday_response) x 
//     $filter_post
//     group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
// group by s.id, name, jml_rating,S order by name) avg";

        $sql="select sum(s) as total_s,avg(s) as avg_s, avg(response_time) as avg_response,avg(processing_time) as avg_process, avg(solving_time) as avg_solving, avg(solvability) as solvability from (select s.id, name, S,round(avg(r.response_time)) as response_time, round(avg(r.processing_time)) as processing_time, round(avg(r.solving_time)) as solving_time,jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
from (  select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
    from (select * from rating_view
        where (rating is not null or rating!=0 ) $filter_user and group_id=$group_id
        order by post_id) t 
    join users u on u.id=t.id group by t.id, u.name) s 
left join (select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time-x.holiday_response)),0) as response_time, COALESCE(round(avg(x.processing_time-x.holiday_processing)),0) as processing_time, 
        COALESCE(round(avg(x.solving_time-x.holiday_response-x.holiday_processing)),0) as solving_time  

        from performance_view x 
    $filter_post and x.group_id=$group_id
    group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
group by s.id, name, jml_rating,S order by name) avg";
echo "<pre>";print_r($sql);
die();
        $data = dbGetRows($sql);
        return $data;
    }

    function getStatTicket($start=NULL, $end=NULL){
        $filter_created=
        " and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
        $filter_solved=
        " and to_date(to_char(c.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(c.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
        if (empty($start) and empty($end)){
            $filter_solved='';
            $filter_created='';
        }
        $sql = "
select (case when x.status='S' then month_solved_at else month_created_at end) as month,(case when x.status='S' then year_solved_at else year_created_at end) as year,
 sum(case when x.status='B' or x.status='P' or x.status='S' then 1 else 0 end) as open,
    sum(case when x.status='S' then 1 else 0 end) as solved
from (
    select t.id, t.status, month_created_at, year_created_at, month_solved_at,year_solved_at 
    from (
        select p.id,p.status, EXTRACT(month from p.created_at) month_created_at, EXTRACT(year from p.created_at) year_created_at
        from posts p where p.type='T' and p.status!='D' $filter_created group by p.id, p.status, month_created_at,year_created_at ) t 
        join (
            select p.id,p.status, (case when p.status='S' then EXTRACT(month from max(c.created_at)) else 0 end) month_solved_at,
                (case when p.status='S' then EXTRACT(year from max(c.created_at)) else 0 end) year_solved_at 
            from posts p 
            left join comments c on p.id=c.post_id 
            where p.type='T' and p.status!='D' $filter_solved group by p.id, p.status ) s on s.id=t.id ) x
group by month, year order by month, year";
        // $sql = "select (case when x.status='S' then month_solved_at else month_created_at end) as month,(case when x.status='S' then year_solved_at else year_created_at end) as year,
        //     sum(case when x.status='B' or x.status='P' or x.status='S' then 1 else 0 end) as open,
        //     sum(case when x.status='S' then 1 else 0 end) as solved
        //     from (
        //         select t.id, t.status, month_created_at, year_created_at, month_solved_at,year_solved_at 
        //         from (
        //             select p.id,p.status, EXTRACT(month from p.created_at) month_created_at, EXTRACT(year from p.created_at) year_created_at
        //             from posts p where p.type='T' and p.status!='D' and (p.is_delete!=1 or p.is_delete is null) group by p.id, p.status, month_created_at,year_created_at ) t 
        //         join (
        //             select p.id,p.status,  (case when p.status='S' then EXTRACT(month from max(p.last_comment)) else 0 end) month_solved_at,
        //             (case when p.status='S' then EXTRACT(year from max(p.last_comment)) else 0 end) year_solved_at 
        //             from (
        //             select c.id, c.status, last_comment 
        //             from (
        //                 select p.id,  p.status, max(c.created_at) as last_comment from posts p inner join comments c on c.post_id=p.id where p.type='T' and p.status!='D' 
        //                 and (p.is_delete!=1 or p.is_delete is null)
        //                 group by p.id) c order by c.id asc ) p group by p.id, p.status ) s on s.id=t.id 
        //     ) x
        //     group by month, year order by month, year
        //     ";
// echo nl2br($sql);
// die();
        $data = dbGetRows($sql);
        return $data;
    }

    function getTicketRange($chosen_helpdesk=NULL,$start=NULL, $end=NULL){
        if ($chosen_helpdesk!=NULL and $chosen_helpdesk!='' and $chosen_helpdesk!='Semua Unit'){
            $group = $this->Group_model->getGroupBy('name',$chosen_helpdesk);
            $group_id = $group['id'];
            $filter_helpdesk = " and p.group_id=$group_id ";
        }
        // if ($chosen_helpdesk!=NULL) {
        //     $helpdesk = $this->Group_model->getGroupBy('name',$chosen_helpdesk);
        //     $helpdesk_id = $helpdesk['id'];
        //     $filter_helpdesk = " and p.group_id=$helpdesk_id ";
        // }
        $filter_created=
        " and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
        $filter_solved=
        " and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
        if (empty($start) and empty($end)){
            $filter_solved='';
            $filter_created='';
        }
        $sql = "select EXTRACT(month from p.created_at) as month, EXTRACT(year from p.created_at) as year from posts p where p.type='T' and p.status!='D' and (p.is_delete is null or p.is_delete!='1') $filter_created $filter_solved $filter_helpdesk
            group by month, year order by year, month";
        $data = dbGetRows($sql);
        return $data;
    }

    function getTicketCreated($chosen_helpdesk=NULL,$month, $year){
        if ($chosen_helpdesk!=NULL and $chosen_helpdesk!='' and $chosen_helpdesk!='Semua Unit') {
            $helpdesk = $this->Group_model->getGroupBy('name',$chosen_helpdesk);
            $helpdesk_id = $helpdesk['id'];
            $filter_helpdesk = " and p.group_id=$helpdesk_id ";
        }
        $sql = "select count(*) as created from posts p where extract(month from p.created_at)=$month and extract(year from p.created_at)=$year and p.type='T' and (p.is_delete!='1' or p.is_delete is null) $filter_helpdesk ";
        $data = dbGetRow($sql);
        return $data;
    }

    function getTicketSolved($chosen_helpdesk=NULL,$month, $year){
        if ($chosen_helpdesk!=NULL and $chosen_helpdesk!='' and $chosen_helpdesk!='Semua Unit') {
            $helpdesk = $this->Group_model->getGroupBy('name',$chosen_helpdesk);
            $helpdesk_id = $helpdesk['id'];
            $filter_helpdesk = " and p.group_id=$helpdesk_id ";
        }
        $sql = "select count(*) as solved 
from (select max(c.created_at) as last_comment from posts p inner join comments c on c.post_id=p.id where p.type='T' and p.status!='D' 
        and (p.is_delete!=1 or p.is_delete is null) $filter_helpdesk
        group by p.id) p 
        where extract(month from p.last_comment)=$month and extract(year from p.last_comment)=$year  ";
        $data = dbGetRow($sql);
        return $data;
    }


    function getStatTicketOpen($start=NULL, $end=NULL){
        $filter = " and extract(year from to_date(to_char(created_at::date,'MM-YYYY'),'MM-YYYY')) = extract(year from to_date('06-2017','MM-YYYY'))
        and 
        extract(month from to_date(to_char(created_at::date,'MM-YYYY'),'MM-YYYY')) = extract(month from to_date('$start','MM-YYYY'))";
        $filter='';
        $sql = 
        "select month_created_at, sum(case when t.status='B' or t.status='P' then 1 end) as Open
        from (select  p.id,p.status, EXTRACT(month from p.created_at) month_created_at
            from posts p
            where p.type='T' and p.status!='D' 
            $filter
            group by p.id, p.status, month_created_at
        ) t 
        group by month_created_at order by month_created_at";
        // die($sql);
        $data = dbGetRows($sql);
        return $data;
    }

    function getStatTicketSolved($start=NULL, $end=NULL){

        $filter = " and extract(year from to_date(to_char(created_at::date,'MM-YYYY'),'MM-YYYY')) = extract(year from to_date('06-2017','MM-YYYY'))
        and 
        extract(month from to_date(to_char(created_at::date,'MM-YYYY'),'MM-YYYY')) = extract(month from to_date('$start','MM-YYYY'))";
        $filter='';
        $sql = 
        "select month_solved_at, sum(case when month_solved_at!=0 then 1 end) as Solved 
        from (select  p.id,p.status, (case when p.status='S' then EXTRACT(month from max(c.created_at)) else 0 end) month_solved_at
            from posts p
            left join comments c on p.id=c.post_id
            where p.type='T' and p.status!='D'
            $filter
            group by p.id, p.status
        ) t 
        group by month_solved_at";
        // die($sql);
        $data = dbGetRows($sql);
        return $data;
    }

    function getRatingData($chosen_helpdesk=NULL,$start=NULL, $end=NULL){
        // $filter_created=
        // " and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
        $filter=
        " and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
        if (empty($start) and empty($end)){
            $filter='';
            // $filter_created='';
        }
        // if ($chosen_helpdesk!=NULL) {
        //     $filter_helpdesk = " and p.group_id=$chosen_helpdesk ";
        // }
        if ($chosen_helpdesk!=NULL and $chosen_helpdesk!='' and $chosen_helpdesk!='Semua Unit') {
            $helpdesk = $this->Group_model->getGroupBy('name',$chosen_helpdesk);
            $helpdesk_id = $helpdesk['id'];
            $filter_helpdesk = " and p.group_id=$helpdesk_id ";
        }
        $sql = 
        "select sum(case when rating=1 then 1 else 0 end) + 
                sum(case when rating=2 then 1 else 0 end) as rating_tidak_puas,
                sum(case when rating=3 then 1 else 0 end) + 
                sum(case when rating=4 then 1 else 0 end) + 
                sum(case when rating=5 then 1 else 0 end) as rating_puas
        from post_rating pr 
        left join posts p on p.id=pr.post_id 
        where p.status='S' 
        $filter 
        $filter_helpdesk
        group by to_char(p.created_at::date,'MM-YYYY')
        order by to_char(p.created_at::date,'MM-YYYY')";

        $data = dbGetRows($sql);
        return $data;
    }

    function getStatRating($chosen_helpdesk=NULL,$start=NULL, $end=NULL){
        // $filter_created=
        // " and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
        $filter=
        " and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
        if (empty($start) and empty($end)){
            $filter='';
            // $filter_created='';
        }
        // if ($chosen_helpdesk!=NULL) {
        //     $filter_helpdesk = " and p.group_id=$chosen_helpdesk ";
        // }
        if ($chosen_helpdesk!=NULL and $chosen_helpdesk!='' and $chosen_helpdesk!='Semua Unit') {
            $helpdesk = $this->Group_model->getGroupBy('name',$chosen_helpdesk);
            $helpdesk_id = $helpdesk['id'];
            $filter_helpdesk = " and p.group_id=$helpdesk_id ";
        }
        $sql = 
        "select sum(case when rating=1 then 1 else 0 end) as rating_satu,sum(case when rating=2 then 1 else 0 end) as rating_dua,
        sum(case when rating=3 then 1 else 0 end) as rating_tiga,sum(case when rating=4 then 1 else 0 end) as rating_empat,
        sum(case when rating=5 then 1 else 0 end) as rating_lima from post_rating pr left join posts p on p.id=pr.post_id where p.status='S' $filter $filter_helpdesk";
        // echo nl2br($sql);
        // die();
        $data = dbGetRow($sql);
        return $data;
    }

    function getStatWaktu($chosen_helpdesk=NULL,$start=NULL, $end=NULL){
        if ($chosen_helpdesk!=NULL and $chosen_helpdesk!='' and $chosen_helpdesk!='Semua Unit') {
            $helpdesk = $this->Group_model->getGroupBy('name',$chosen_helpdesk);
            $helpdesk_id = $helpdesk['id'];
            $filter_helpdesk = " and p.group_id=$helpdesk_id ";
        }
        $filter_created=
        " and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
        $filter_solved=
        " and to_date(to_char(c.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(c.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
        if (empty($start) and empty($end)){
            $filter_solved='';
            $filter_created='';
        }
        $sql="
select extract(month from last_comment) as month,extract(year from last_comment) as year,
COALESCE(round(avg(x.response_time-holiday_response)),0) as response_time, COALESCE(round(avg(x.processing_time-holiday_processing)),0) as processing_time, COALESCE(round(avg(x.solving_time-holiday_response-holiday_processing)),0) 
as solving_time 
from (
    select detail.*, count(h.date)*60*24 as holiday_processing from  (select post_created_at, first_comment, last_comment,count(h.date)*60*24 as holiday_response,  DATE_PART('day',first_comment-post_created_at)*24*60+ 
    DATE_PART('hour',first_comment-post_created_at)*60+ DATE_PART('minute',first_comment-post_created_at) as response_time, 
    DATE_PART('day',last_comment-first_comment)*24*60+ DATE_PART('hour',last_comment-first_comment)*60+ 
    DATE_PART('minute',last_comment-first_comment) as processing_time, DATE_PART('day',last_comment-post_created_at)*24*60+ 
    DATE_PART('hour',last_comment-post_created_at)*60+ DATE_PART('minute',last_comment-post_created_at) as solving_time 
    from (
    select p.id, p.created_at as post_created_at, min(c.created_at) as first_comment, max(c.created_at) as last_comment 
    from posts p join comments c on c.post_id=p.id where p.status='S' and (p.is_delete is null or p.is_delete!='1') $filter_helpdesk $filter_solved
    group by p.id, post_created_at) c 
    left join holidays h on h.date between post_created_at and first_comment 
    group by c.post_created_at, first_comment, last_comment, response_time, processing_time, solving_time, c.id
    order by c.id asc ) detail
    left join holidays h on h.date between first_comment and last_comment
    group by post_created_at, first_comment, last_comment, response_time, processing_time, solving_time, holiday_response
) x 
group by year, month
order by year, month
";
        $data = dbGetRows($sql);
        return $data;
    }

    function getStatKeyword(){
        $sql = "select tags.*, t.count as tiket, k.count as knowledge
                from tags
                left join
                    (select p.type, t.id, count(t.*) count
                        from posts p, post_tags pt, tags t
                        where p.id = pt.post_id
                        and t.id = pt.tag_id
                        and p.type = 'T'
                        group by p.type, t.id) t on tags.id = t.id
                left join 
                    (select p.type, t.id, count(t.*) count
                        from posts p, post_tags pt, tags t
                        where p.id = pt.post_id
                        and t.id = pt.tag_id
                        and p.type = 'K'
                        group by p.type, t.id) k on tags.id = k.id
                where t.count is not null or k.count is not null
                order by coalesce(t.count, 0) desc";
        $data = dbGetRows($sql);
        return $data;
    }

    /**
     * Get Search Statistik
     * @param   string('yyyy-mm-dd') $date_start 
     *          string('yyyy-mm-dd') $date_end
     */
    function getStatSearch($date_start = NULL, $date_end = NULL){
        $sql = "select search.keyword, coalesce(search.count, 0) search_count, coalesce(knowledge.count,0) knowledge_count
                from    (select w.keyword, 
                        sum(w.count) as count 
                    from warehouse_search_logs w ";

        if($date_start != '' && $date_end != '')
            $sql .= "where date between '$date_start' and '$date_end'";

        $sql .= "    group by w.keyword) search
                left join
                    (select t.name as keyword, 
                        count(t.name) as count
                    from posts p, post_tags pt, tags t
                    where pt.post_id = p.id
                    and pt.tag_id = t.id
                    and p.type = 'K' ";

        if($date_start != '' && $date_end != '')
            $sql .= "and p.created_at between '2018-05-01' and '2018-06-30' ";

        $sql .= "   group by t.name) knowledge on search.keyword = knowledge.keyword";

        $data = dbGetRows($sql);
        return $data;
    }

    function getLog(){
        $sql = "select distinct id_organisasi from users";
        $data['rows_jumlah_organisasi'] = dbGetRows($sql);
        $sql = "select distinct u.id_organisasi from user_log ul 
                left join users u on u.id=ul.user_id
                where aksi='login' and u.role='O'";
        $data['rows_organisasi_log'] = dbGetRows($sql);
        $sql = "select count(*) 
            from (select user_id, username from user_log where aksi='login' group by user_id, username) x";
        $data['rows_user_log'] = dbGetRow($sql);
        $sql = "select count(*) from posts";
        $data['rows_posts'] = dbGetRow($sql);
        $sql = "select count(*) from posts where type='T'";
        $data['rows_ticket'] = dbGetRow($sql);
        $sql = "select count(*) from posts where type='G'";
        $data['rows_Group'] = dbGetRow($sql);
        $sql = "select count(*) from posts where type='K'";
        $data['rows_KM'] = dbGetRow($sql);
        $sql = "select pg_size_pretty(pg_database_size('helpdesk'))";
        $data['storage_heldesk'] = dbGetRow($sql);
        $sql = "select pg_size_pretty(pg_database_size('helpdesk_upload'))";
        $data['storage_blob'] = dbGetRow($sql);
        $sql = "select  distinct l.id,
                        l.name,
                        m.jml_organisasi,
                        l.jml_organisasi_login,
                        (cast(l.jml_organisasi_login as float)/cast(m.jml_organisasi as float)) as persentase 
                from
                (   select log.id,log.name,count(log.id_organisasi) as jml_organisasi_login 
                    from (
                        select distinct g.id,g.name, u.id_organisasi from groups g 
                        left join group_members gm on gm.group_id=g.id
                        left join user_log ul on ul.user_id = gm.user_id
                        left join users u on u.id=gm.user_id
                        where (g.name ilike '%Kopertis%' or g.name ilike '%PTN%' or g.name ilike '%PTK%' or g.name ilike '%PTA%') and ul.aksi='login'
                        order by g.id 
                    ) log
                    group by id,name
                ) l
                left join
                (   select members.id,members.name,count(id_organisasi) as jml_organisasi
                    from(
                        select distinct g.id, g.name,id_organisasi from groups g 
                        left join group_members gm on gm.group_id=g.id
                        left join users u on u.id=gm.user_id
                        where (g.name ilike '%Kopertis%' or g.name ilike '%PTN%' or g.name ilike '%PTK%' or g.name ilike '%PTA%')
                        order by g.id
                    ) members
                    group by members.id, members.name
                ) m on m.id=l.id
                order by persentase";
        $data['map_kopertis'] = dbGetRows($sql);

        return $data;
    }
}
