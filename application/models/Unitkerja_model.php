<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Unitkerja_model extends AppModel {


    private $limit = 50;
    private $table = 'unitkerja';
    private $table_view = 'vw_unitkerja';
    protected $_show_sql = false;
    protected $_order = 'idunit';
    protected $_column = "*";

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql){
        $this->_show_sql = $show_sql;
        return $this;
    }


    public function getAll($filter=NULL){
        $column = $this->_column;
        $sql = "select $column from $this->table_view  $filter order by $this->_order limit $this->limit";
        if ($this->_show_sql){
            $this->_show_sql=false;
            die($sql);
        }
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function getBy($id){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where id='$id'";
        $row = dbGetRow($sql);
        return $row;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }
    
    public function getOne($id, $col){
        if (empty($id) and empty($col))
            return false;
        $sql = "select $col as \"$col\" from $this->_table where id='$id' $this->_filter" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneBy($id, $col){
        $sql = "select $col from $this->_table where id='$id'" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneFilter($col = ''){
        $sql = "select $col from $this->_table $this->_filter";
        if ($this->_show_sql){
            die($sql);
        }
        return dbGetOne($sql);
    }

    public function delete($id){
        $sql = "update $this->_table set is_delete=1 where id=$id";
        return dbQuery($sql);
    }
}
