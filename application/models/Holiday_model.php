<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Holiday_model extends AppModel {

    public function getWorkDays($start_date, $end_date){
        if (!defined('SATURDAY')) define('SATURDAY', 6);
        if (!defined('SUNDAY')) define('SUNDAY', 0);

        $sql = "select date::date
                from generate_series(
                    (select min(start_date)
                    from holidays
                    where '$start_date' between start_date and end_date
                    or '$end_date' between start_date and end_date),
                    (select max(end_date)
                    from holidays
                    where '$start_date' between start_date and end_date
                    or '$end_date' between start_date and end_date)
                    , '1 day'::interval) date
                where date between '$start_date' and '$end_date'";
        $publicHolidays = Util::toList(dbGetRows($sql), 'date');

        $start = strtotime($start_date);
        $end   = strtotime($end_date);
        $workdays = 0;
        for ($i = $start; $i <= $end; $i = strtotime("+1 day", $i)) {
            $day = date("w", $i);  // 0=sun, 1=mon, ..., 6=sat
            $mmgg = date('Y-m-d', $i);

            if ($day != SUNDAY &&
                $day != SATURDAY &&
                !in_array($mmgg, $publicHolidays)) {
                $workdays++;
            }
        }
        return $workdays;
    }

    public function getHolidays($start_date, $end_date){
        if (!defined('SATURDAY')) define('SATURDAY', 6);
        if (!defined('SUNDAY')) define('SUNDAY', 0);

        $sql = "select date::date
                from generate_series(
                    (select min(start_date)
                    from holidays
                    where '$start_date' between start_date and end_date
                    or '$end_date' between start_date and end_date),
                    (select max(end_date)
                    from holidays
                    where '$start_date' between start_date and end_date
                    or '$end_date' between start_date and end_date)
                    , '1 day'::interval) date
                where date between '$start_date' and '$end_date'";
        $publicHolidays = Util::toList(dbGetRows($sql), 'date');

        $start = strtotime($start_date);
        $end   = strtotime($end_date);
        $holidays = 0;
        for ($i = $start; $i <= $end; $i = strtotime("+1 day", $i)) {
            $day = date("w", $i);  // 0=sun, 1=mon, ..., 6=sat
            $mmgg = date('Y-m-d', $i);

            if ($day == SUNDAY or
                $day == SATURDAY or
                in_array($mmgg, $publicHolidays)) {
                $holidays++;
            }
        }
        return $holidays;
    }

    private function isHoliday(){

    }
} 
