<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dinas_diversion_model extends AppModel {

    const PENGAJUAN = 'S'; // submission
    const DITERIMA = 'A'; // approve
    const DITOLAK   = 'R'; //reject
    const DIALIHKAN   = 'D'; //diversion

    private $limit = 50;
    protected $_table = "dinas_diversion";
    protected $_table_alias = "dd";
    protected $_columns = array(
        "id"        => "id",
        "post_id"   => "post_id",
        "dinas_id"  => "dinas_id",
        "petugas_id" => "petugas_id",
        "status"    => "status",
        "reason"    => "reason",
        "reason_confirm"        => "reason_confirm"
    );
    protected $_filter = '';

    protected $_show_sql = false;

    public static function statusName($status = NULL) {
        $names = array(
            self::PENGAJUAN => 'Pengajuan',
            self::DITERIMA => 'Disetujui',
            self::DITOLAK => 'Ditolak',
            self::DIALIHKAN => 'Dialihkan',
        );
        return empty($status) ? '-' : $names[$status];
    }

    function column($column){
        $this->_column = $column;
        return $this;
    }

    function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    function create($post_id, $dinas_id, $petugas_id, $reason, $is_admin=false){
        if($is_admin){
            $dataInsert = array(
                'post_id'   => $post_id,
                'dinas_id'  => $dinas_id,
                'petugas_id'=> $petugas_id,
                'status'    => self::DITERIMA,
                'reason'    => "<b style=\"color:red;\">Diubah oleh Admin: </b>".$reason
            );
        }else{
            $dataInsert = array(
                'post_id'   => $post_id,
                'dinas_id'  => $dinas_id,
                'petugas_id'=> $petugas_id,
                'status'    => self::PENGAJUAN,
                'reason'    => $reason
            );
        }
        
        $this->_database->trans_begin();
        $create = dbInsert($this->_table, $dataInsert);
        if($create){
            return $create;
        }
        return false;
    }

    function update($diversion_id, $status, $reason, $dinas_id){
        $dataUpdate = array(
            'status'    => $status,
            'reason_confirm'    => $reason,
            'dinas_id_2'=> (int)$dinas_id,
            'updated_at' =>  date("Y-m-d H:i:s")
        );
        $this->_database->trans_begin();
        $update = dbUpdate($this->_table, $dataUpdate, "id=".$diversion_id);
        if($update){
            return $update;
        }
        return false;
    }

    function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    function _getColumn($alias=null){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            if ($alias!=null){
                $column .= " $alias.$key as $value";
            }
            else{
                $column .= " $key as $value";
            }
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }


    function getDiversion($post_id){
        $column = $this->_getColumn($this->_table_alias);
        $sql ="select $column, uk.name as nama_dinas, u.name as nama_petugas, uj.name as nama_dinas_2 from $this->_table $this->_table_alias
                left join unitkerja uk on ".$this->_table_alias.".dinas_id = uk.idunit
                left join unitkerja uj on ".$this->_table_alias.".dinas_id_2 = uj.idunit
                left join users u on ".$this->_table_alias.".petugas_id = u.id
                where post_id=$post_id";
        if ($this->_show_sql){
            die($sql);
        }
        $rows = dbGetRow($sql);
        return $rows;
    }

    function getOne($column){
        $sql = "select $column from $this->_table $this->_filter";
        if ($this->_show_sql){
            die($sql);
        }
        $row = dbGetOne($sql);
        return $row;
    }
}
