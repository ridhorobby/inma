<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->model('Post_model');

class Issue_model extends Post_model {

    //PRIORITY
    const PRIORITY_MINOR = 1;
    const PRIORITY_NORMAL = 2;
    const PRIORITY_IMPORTANT = 3;
    const PRIORITY_URGENT = 4;
    //STATUS
    const STATUS_OPEN = 1;
    const STATUS_START = 2;
    const STATUS_PROGRESS = 3;
    const STATUS_DONE = 4;
    const STATUS_REOPEN = 5;
    const STATUS_CLOSE = 6;
    const STATUS_INVALID = 7;
    const STATUS_DUPLICATE = 8;

    protected $_table = 'posts';

    public static function getType() {
        return array(
            Category_model::BUG => 'BUG',
            Category_model::ENHANCEMENT => 'ENHANCEMENT',
            Category_model::TASK => 'TASK',
            Category_model::PROPOSAL => 'PROPOSAL'
        );
    }

    public static function getPriority() {
        return array(
            self::PRIORITY_MINOR => 'MINOR',
            self::PRIORITY_NORMAL => 'NORMAL',
            self::PRIORITY_IMPORTANT => 'IMPORTANT',
            self::PRIORITY_URGENT => 'URGENT'
        );
    }

    public static function getStatus() {
        return array(
            self::STATUS_OPEN => 'OPEN',
            self::STATUS_START => 'START',
            self::STATUS_PROGRESS => 'PROGRESS',
            self::STATUS_DONE => 'DONE',
            self::STATUS_REOPEN => 'REOPEN',
            self::STATUS_CLOSE => 'CLOSE',
            self::STATUS_INVALID => 'INVALID',
            self::STATUS_DUPLICATE => 'DUPLICATE'
        );
    }

    protected function join_or_where($row) {
        $this->_database->select("posts.*, issues.*");
        $this->_database->join('issues', 'posts.id = issues.post_id');
        $this->_database->join('users', 'users.id = posts.user_id');
        $this->_database->where('posts.category_id IN (' . implode(',', array_keys(self::getType())) . ')');
        $this->with(array('Category', 'User', 'Comment' => 'User', 'Post_user', 'Group'));
        $this->order_by(array('issues.deadline' => 'DESC', 'posts.id' => 'ASC'));
        return $row;
    }

    public function getAll($page = 0, $param = null, $showall = false) {
        $condition = array();
        if (!empty($param)) {
            $condition[] = "(users.name ilike '%" . $param . "%' or posts.description ilike '%" . $param . "%')";
        }
        if (empty($showall)) {
            $condition[] = "(issues.status not in('" . self::STATUS_DONE . "','" . self::STATUS_CLOSE . "','" . self::STATUS_INVALID . "','" . self::STATUS_DUPLICATE . "'))";
        }
        $this->limit(10, ($page * 10));
        return $this->get_many_by($condition);
    }

    public function getMe($userId, $page = 0, $param = null, $showall = false) {
        $condition = array(
            'posts.user_id' => $userId
        );
        if (!empty($param)) {
            $condition[] = "(users.name ilike '%" . $param . "%' or posts.description ilike '%" . $param . "%')";
        }
        if (empty($showall)) {
            $condition[] = "(issues.status not in('" . self::STATUS_DONE . "','" . self::STATUS_CLOSE . "','" . self::STATUS_INVALID . "','" . self::STATUS_DUPLICATE . "'))";
        }
        $this->limit(10, ($page * 10));
        return $this->get_many_by($condition);
    }

    public function getForMe($userId, $page = 0, $param = null, $showall = false) {
        $condition = array(
            '(SELECT COUNT(*) 
                    FROM post_users 
                    WHERE post_users.post_id = posts.id AND post_users.user_id = ' . $userId . ' 
                    LIMIT 1) = 1'
        );
        if (!empty($param)) {
            $condition[] = "(users.name ilike '%" . $param . "%' or posts.description ilike '%" . $param . "%')";
        }
        if (empty($showall)) {
            $condition[] = "(issues.status not in('" . self::STATUS_DONE . "','" . self::STATUS_CLOSE . "','" . self::STATUS_INVALID . "','" . self::STATUS_DUPLICATE . "'))";
        }
        $this->limit(10, ($page * 10));
        return $this->get_many_by($condition);
    }

    private function setForDashboard($userId, $condition = array()) {
        $this->load->model('Group_model');
        $this->_database->join("post_users", 'post_users.post_id = posts.id', 'LEFT');
        $this->_database->join("groups", "groups.id = posts.group_id AND groups.type = '" . Group_model::TYPE_PROJECT . "' ", 'LEFT');
        $this->_database->join("group_members", 'group_members.group_id = groups.id', 'LEFT');
        if (!empty($userId)) {
            $condition[] = "(post_users.user_id = $userId OR group_members.user_id = $userId OR posts.user_id = $userId)";
        }
        $condition[] = '((issues.status <' . self::STATUS_CLOSE . ') or (issues.status = ' . self::STATUS_CLOSE . " and posts.updated_at < '" . date('Y-m-d', strtotime('-90 days')) . "'))";
        $this->_database->group_by('posts.id, issues.post_id');
        $this->order_by(array('issues.deadline' => 'ASC', 'issues.priority' => 'DESC', 'posts.id' => 'ASC'));
        return $condition;
    }

    public function getDashboard($userId, $condition = array()) {
        $condition = $this->setForDashboard($userId, $condition);
        $this->with(array('Category', 'User', 'Post_user', 'Group', 'Comment' => 'User'));

        $issues = $this->get_many_by($condition);
        $data = array(
            self::STATUS_OPEN => array(),
            self::STATUS_PROGRESS => array(),
            self::STATUS_DONE => array(),
            self::STATUS_CLOSE => array()
        );
        foreach ($issues as $issue) {
            if (in_array($issue['status'], array(self::STATUS_OPEN, self::STATUS_REOPEN))) {
                $data[self::STATUS_OPEN][] = $issue;
            } elseif (in_array($issue['status'], array(self::STATUS_START, self::STATUS_PROGRESS))) {
                $data[self::STATUS_PROGRESS][] = $issue;
            } else {
                $data[$issue['status']][] = $issue;
            }
        }
        
        ksort($data);
        
        return $data;
    }

    public function getDashboardByStatus($status, $userId, $condition = array()) {
        $condition = $this->setForDashboard($userId, $condition);
        $condition['issues.status'] = $status;
        $this->with(array('Category', 'User', 'Post_user', 'Group', 'Comment' => 'User'));

        $data = $this->get_many_by($condition);
        return $data;
    }

    public function getChartDashboard($userId, $condition = array()) {
        $condition = $this->setForDashboard($userId, $condition);
        $this->with(array('Category'));

        $data = $this->get_many_by($condition);
        return $data;
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        if (empty($data['description']) && empty($data['link']) && empty($data['image']) && empty($data['video']) && empty($data['file'])) {
            return 'Data yang dikirim tidak lengkap.';
        }

        $validation = array(
            'title' => 'Judul tugas harus diisi',
            'postUsers' => 'Tujuan tugas harus diisi.',
            'date' => 'Tanggal tugas harus diisi',
            'deadline' => 'Tanggal Deadline harus diisi.',
            'priority' => 'Prioritas harus diisi.',
            'status' => 'Status pengerjaan harus diisi'
        );
        $data['groupId'] = (empty($data['groupId']) || $data['groupId'] == 0) ? NULL : $data['groupId'];
        $isValid = $this->validateRequired($data, $validation);
        if ($isValid !== TRUE) {
            return $isValid;
        }
        $this->_database->trans_begin();
        $data['type'] = self::TYPE_FRIEND;
        $create = parent::defaultCreate($data, $skip_validation, $return);
        if ($create) {
            $data['postId'] = $create;
            $createDailyReport = $this->_database->insert('issues', $this->setFieldDB($data, $this->_database->list_fields('issues')));
            if (!$createDailyReport) {
                $this->_database->trans_rollback();
                return FALSE;
            }
            if (isset($data['postUsers'])) {
                $toUser = explode(',', $data['postUsers']);
                $this->load->model('Post_user_model');
                foreach ($toUser as $user) {
                    $dataToUser = array(
                        'post_id' => $create,
                        'user_id' => $user
                    );
                    $insert = $this->Post_user_model->insert($dataToUser, TRUE, FALSE);
                    if (!$insert) {
                        $this->_database->trans_rollback();
                        return FALSE;
                    }
                }
            }
            $upload = TRUE;
            if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                $upload = Image::upload('image', $create, $_FILES['image']['name'], 'posts/photos');
            }
            if (isset($_FILES['video']) && !empty($_FILES['video'])) {
                $upload = File::upload('video', $create, 'posts/videos');
            }
            if (isset($_FILES['file']) && !empty($_FILES['file'])) {
                $upload = File::upload('file', $create, 'posts/files');
            }
            if ($upload === TRUE) {
                $this->load->model('Notification_model');
                $this->Notification_model->generate(Notification_model::ACTION_POST_CREATE, $create);
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function update($primary_value, $data = array()) {
        $this->_database->trans_begin();

        // posts, untuk updated_at
        $ok = parent::update($primary_value, array(), true);

        // issues, tanpa pengecekan
        if ($ok) {
            $ok = $this->_database->where('post_id', $primary_value)
                    ->set($data)
                    ->update('issues');
        }

        if ($ok)
            $this->_database->trans_commit();
        else
            $this->_database->trans_rollback();

        return $ok;
    }

}
