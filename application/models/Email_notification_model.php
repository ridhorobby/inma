<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Email_notification_model extends AppModel {

    private $_limit = "limit 50";
    private $table = 'email_notifications';
    private $_order = "order by";


    const EMAIL_REGISTER = 'R';
   	public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function column($col){
        $this->_columns = $col;
        return $this;
    }

    public function limit($limit){
        $this->_limit = "limit ".$limit;
        return $this;
    }

    public function order($order='id', $asc="asc"){
        $this->_order = "order by ".$order." ".$asc;
        return $this;
    }

    public function group_by($group_by=''){
        $this->_group_by = $group_by;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getBy(){
        $sql = "select $this->_columns from $this->_table $this->_filter $this->_limit $this->_group_by $this->_order";
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $row = dbGetRow($sql);
        return $row;
    }

    public function getAll(){
        $sql = "select $this->_columns
        		from $this->table pl 
        		$this->_filter $this->_group_by $this->_order $this->_limit";
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $rows = dbGetRows($sql);
        if($rows){
        	return $rows;
        }else{
        	return array();
        }
        // echo "<pre>";print_r($rows);echo "</pre>";die();
        
    }
    
    public function getOne($col){
        $sql = "select $col from $this->_table $this->_filter" ;
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $column = dbGetOne($sql);
        return $column;
    }


    public function create($type, $user_id){
    	// echo $type."<br>";
    	// echo self::LOG_POST_TICKET;die();
    	switch ($type) {
    		case self::EMAIL_REGISTER:
                $name = dbGetOne("select name from users where id=".$user_id);
	    		$message = "Selamat datang di Meole, sebelum menggunakan Meole, silahkan melakukan aktivasi terlebih dahulu";
                $url = site_url()."web/site/activation";
    		break;
    		
    	}
    	$data = array(
    		'user_id'	=> $user_id,
    		'message'   => $message,
    		'type'		=> $type,
    		'url'	    => $url,
            'is_send'   => 0
    	);
        // echo "<pre>";print_r($data);die();
        $create = dbInsert($this->table,$data);

    }

}
