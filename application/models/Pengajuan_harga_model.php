<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan_harga_model extends AppModel {

    public $limit = 10;

    const HARI = 1;
    const PEKAN = 2;
    const BULAN = 3;

    protected $_filter = "";
    protected $_group_by = "";
    protected $_order_by = "";
    protected $_order = "";
    protected $_table = "pengajuan_harga";
    // protected $_view = "harga_view";
    protected $_column = "*";
    protected $_join = "";
    
    // Penjelasan Status
    // 1 : DRAFT                : User masih bebas update jumlah 
    // 2 : MENUNGGU APPROVAL    : Menunggu approval dari direktur
    // 3 : REVISI               : Pengajuan ditolak oleh direktur dan harus direvisi
    // 4 : DISETUJUI            : Pengajuan disetujui oleh direktur
    // 5 : REVISI HPP           : Revisi Hpp oleh Manager Produksi dan menunggu approval direktur


    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function getLeadOrDeal($satuan, $start, $end,$type='l'){
        // echo $type."<br>";
        $column_ref = ($type=='l') ? "tgl_pengajuan" : "p.tgl_order_masuk";
        switch ($satuan) {
            case self::HARI:
                $this->_column   = "$column_ref,
                                    count(ph.id) as jumlah";
                $this->_group_by = "group by $column_ref";
                $this->_order_by = "order by $column_ref";
                $this->_filter = "and
                                    ($column_ref>='$start' and $column_ref<='$end') ";
            break;
            case self::PEKAN:
                $this->_column   = "CONCAT(year($column_ref), '-', week($column_ref,1)) as tahun_pekan,
                                    count(ph.id) as jumlah";
                $this->_group_by = "group by concat(year($column_ref), '-', week($column_ref,1))";
                $this->_order_by = "order by concat(year($column_ref), '-', week($column_ref,1))";
                $this->_filter = "and
                                    (concat(year($column_ref), '-', week($column_ref,1))>='$start' and concat(year($column_ref), '-', week($column_ref,1))<='$end') ";
            break;
            case self::BULAN :
                $this->_column   = "year($column_ref) as tahun,month($column_ref) as bulan,
                                    DATE_FORMAT($column_ref,'%Y-%m') as tahun_bulan,
                                    count(ph.id) as jumlah";
                $this->_group_by = "group by year($column_ref),month($column_ref)";
                $this->_order_by = "order by year($column_ref),month($column_ref)";
                $this->_filter = "and
                                    ($column_ref>='$start' and $column_ref<='$end') ";
            break;
            
            
            // default:
            //     # code...
            //     break;
        }
        $string_konsumen = ($type=='l') ? '1=1' : 'id_konsumen!=0';
        if($type!='l'){
            $this->_column .="
                ,(CASE `jenis`
                   WHEN 1 THEN sum(`harga_setuju_granit`)
                   WHEN 2 THEN sum(`harga_setuju_keramik`)
                END) as `omzet`,
                (CASE `jenis`
                   WHEN 1 THEN sum(`harga_setuju_pokok_granit`)
                   WHEN 2 THEN sum(`harga_setuju_pokok_keramik`)
                END) as `harga_produksi`
            ";
        }
        $sql = "select $this->_column
                from $this->_table ph
                left join pesanan p on ph.id_konsumen = p.konsumen_id and p.tipe='P'
                where $string_konsumen and status=4 $this->_filter
                $this->_group_by
                $this->_order_by
                ";
        // if($type!='l'){
            // die($sql);    
        // }
        
        return dbGetRows($sql);

    }

    public function create($data, $user_id, $skip_validation = TRUE, $return = TRUE) {
        $dataInsert = array(
            'tgl_pengajuan'     => $data['tgl_pengajuan'],
            // 'tgl_disetujui'     => $data['tgl_disetujui'],
            // 'id_konsumen'       => $data['id_konsumen'],
            'nama_konsumen'     => $data['nama_konsumen'],
            'alamat_konsumen'   => $data['alamat_konsumen'],
            'no_hp_konsumen'   => $data['no_hp_konsumen'],
            // 'hrg_setuju'        => $data['hrg_setuju'],
            // 'uang_muka'         => $data['uang_muka'],
            // 'tanda_jadi'        => $data['tanda_jadi'],
            'status'            => '1',
            'jenis'             => $data['jenis'],
            // 'pelunasan'         => $data['pelunasan'],
            'keterangan'        => $data['keterangan'],
            // 'is_deleted'        => $data['is_deleted'],
            'user_id_pengaju'   => $user_id,
            'created_by'        => $user_id,
            // 'user_id_approve'   => $data['user_id_approve'],
            // 'created_at'        => date('Y-m-d G:i:s', time()),
            // 'updated_at'        => date('Y-m-d G:i:s', time())
        );

        $create = parent::create($dataInsert, true, true);
        if ($create) {
            return $create;
        }else{
            return false;
        }
    }

    public function updateHarga($data){
        $dataUpdate = array(
            'tgl_pengajuan'     => $data['tgl_pengajuan'],
            'hrg_setuju'        => $data['hrg_setuju'],
            'DP'                => $data['DP'],
            'tanda_jadi'        => $data['tanda_jadi'],
            'status'            => $data['status'],
            'pelunasan'         => $data['pelunasan'],
            'keterangan'        => $data['keterangan'],
            'updated_at'        => date('Y-m-d G:i:s', time()),
            'updated_by'        => $data['user_id']
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }
    
    public function ajukanHarga($data){
        $dataUpdate = array(
            'status'                => '2',
            'updated_at'            => date('Y-m-d G:i:s', time())
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }
    
    public function approveHarga($data){
        $dataUpdate = array(
            'harga_setuju_granit'           => $data['harga_setuju_granit'],
            'harga_setuju_keramik'          => $data['harga_setuju_keramik'],
            'harga_setuju_pokok_granit'     => $data['harga_setuju_pokok_granit'],
            'harga_setuju_pokok_keramik'    => $data['harga_setuju_pokok_keramik'],
            // 'tanda_jadi'            => $data['tanda_jadi'],
            'user_id_approve'               => $data['user_id'],
            'status'                        => '4',
            'updated_at'                    => date('Y-m-d G:i:s', time()),
            'tgl_disetujui'                 => date('Y-m-d', time())
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }
    
    public function updateHargaSetuju($data){
        $dataUpdate = array(
            'harga_setuju_granit'           => $data['harga_setuju_granit'],
            'harga_setuju_keramik'          => $data['harga_setuju_keramik'],
            'harga_setuju_pokok_granit'     => $data['harga_setuju_pokok_granit'],
            'harga_setuju_pokok_keramik'    => $data['harga_setuju_pokok_keramik'],
            'user_id_approve'               => $data['user_id'],
            'updated_at'                    => date('Y-m-d G:i:s', time())
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }
    
    public function tolakPengajuanHarga($data){
        $dataUpdate = array(
            'status'                => '3',
            'updated_at'            => date('Y-m-d G:i:s', time())
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }
    
    public function revisiPengajuan($data) {
        $dataUpdate = array(
            'status'            => '3',
            'updated_at'        => date('Y-m-d G:i:s', time())
        );
        $save = parent::save($data['id_pengajuan_harga'], $dataUpdate, true);
        return $save;
    }
    
    public function revisiHpp($data) {
        $dataUpdate = array(
            'status'            => '5',
            'revisi_hpp'        => '1',
            'updated_at'        => date('Y-m-d G:i:s', time())
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }
    
    public function ubahDiskon($data) {
        $dataUpdate = array(
            'diskon'            => $data['diskon'],
            'updated_by'        => $data['user_id'],
            'updated_at'        => date('Y-m-d G:i:s', time())
        );
        $save = parent::save($data['id_pengajuan_harga'], $dataUpdate, true);
        return $save;
    }
    
    public function getPengajuan($page=0, $status="0", $role="", $user_id, $bulan="", $tahun="", $keyword=""){
        if ($status != "0") {
            $filter = " AND status = '".$status."'";
            
            if ($status == "4") {
                $filter = $filter . " AND pengajuan_harga.id_konsumen <> '0'";
            }
        } else {
            if($role == Role::PRODUKSI){
                $filter = " AND status in(4,5) ";
            }elseif($role == Role::MARKETING || $role == Role::DESAIN){
                $filter = " AND status != '5' ";
            }else{
                $filter = "";
            }
            
        }

        if($keyword!=""){
            $key = strtolower($keyword);
            $filter .= " and (lower(pengajuan_harga.nama_konsumen) like '%$key%' or lower(konsumen.nama) like '%$key%') ";
        }
        
        if ($bulan != "") {
            $filter = $filter . " AND MONTH(pengajuan_harga.updated_at) = '".$bulan."' AND YEAR(pengajuan_harga.updated_at) = '".$tahun."'";
        }

        if($role == Role::MITRA_MARKETING){
            $filter .=" and user_id_pengaju=".$user_id;
        }
        
        $sql = "SELECT pengajuan_harga.*, konsumen.nama, konsumen.alamat, konsumen.no_hp, pesanan.kode_order, u1.name AS 'user_pengaju', u2.name AS 'user_approve'
                FROM pengajuan_harga
                LEFT JOIN konsumen ON pengajuan_harga.id_konsumen = konsumen.id
                LEFT JOIN pesanan ON konsumen.id = pesanan.konsumen_id
                LEFT JOIN users u1 ON pengajuan_harga.user_id_pengaju = u1.id
                LEFT JOIN users u2 ON pengajuan_harga.user_id_approve = u2.id
                WHERE 1 AND pengajuan_harga.is_deleted = '0' ".$filter."
                ORDER BY tgl_pengajuan DESC";
                
                
        // echo $sql;
        // die();

        if ($page==='count') {
            return dbGetCount($sql);
        }else{
            
            if ($status != "0" || $keyword != "") {
                $offset = (int) $page * $this->limit;
                $sql .= " limit $this->limit offset $offset ";
            }
        }
                
        return dbGetRows($sql);
    }
    
    public function getOnePengajuan($page=0, $id, $status="0"){
        if ($status != "0") {
            $filter = " AND status = '".$status."'";
        } else {
            $filter = "";
        }
        
        $sql = "SELECT pengajuan_harga.*, konsumen.nama, konsumen.alamat, konsumen.no_hp, pesanan.kode_order, u1.name AS 'user_pengaju', u2.name AS 'user_approve'
                FROM pengajuan_harga
                LEFT JOIN konsumen ON pengajuan_harga.id_konsumen = konsumen.id
                LEFT JOIN pesanan ON konsumen.id = pesanan.konsumen_id
                LEFT JOIN users u1 ON pengajuan_harga.user_id_pengaju = u1.id
                LEFT JOIN users u2 ON pengajuan_harga.user_id_approve = u2.id
                WHERE 1 AND pengajuan_harga.is_deleted = '0' AND pengajuan_harga.id = '".$id."'
                ORDER BY tgl_pengajuan DESC";
                
                
        // echo $sql;
        // die();
                
        return dbGetRow($sql);
    }
    
    public function getDetailPengajuan($id) {
        $sql = "SELECT dp.*, harga.nama, harga.kategori, kategori_produk.nama AS 'nama_kategori'
                FROM detail_pengajuan dp
                LEFT JOIN harga ON dp.id_harga = harga.id
                LEFT JOIN kategori_produk ON harga.kategori = kategori_produk.id
                WHERE 1 AND dp.is_deleted = '0' AND dp.id_pengajuan_harga = ".$id;
        return dbGetRows($sql);
    }
    
    public function updateKonsumen($data) {
        $dataUpdate = array(
            'id_konsumen'           => $data['konsumen_id'],
            'nama_konsumen'         => $data['nama'],
            'alamat_konsumen'       => $data['alamat'],
            'no_hp_konsumen'        => $data['no_hp'],
            'updated_by'            => $data['user_id']
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }
    
    public function updateJenis($data) {
        $dataUpdate = array(
            'jenis'         => $data['jenis'],
            'updated_by'    => $data['user_id']
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }
    
    // Mendapatkan margin keuntungan per bulan
    // NOTE : Query masih blm dapat menampilkan data bulan berikutnya jika hanya ada pengeluaran
    public function getLabaRugiBulanan($tahun) {
        $sql = "SELECT
                    MONTH(p.tgl_order_masuk) AS 'bulan',
                    SUM(
                        IF(
                            ph.jenis = '1',
                            ph.harga_setuju_granit,
                            ph.harga_setuju_keramik
                        ) - IF(
                            ph.jenis = '1',
                            ph.harga_setuju_pokok_granit,
                            ph.harga_setuju_pokok_keramik
                        ) - ph.diskon
                    ) AS 'pendapatan',
                    (SELECT COALESCE(SUM(pl.nominal), 0) FROM pembayaran_log pl WHERE pl.tipe = '2'  AND pl.jenis IN ('1','5') AND MONTH(pl.tgl_input) = MONTH(p.tgl_order_masuk)) AS 'pengeluaran'
                FROM
                    pengajuan_harga ph
                LEFT JOIN konsumen k ON
                    k.id = ph.id_konsumen
                LEFT JOIN pesanan p ON
                    p.konsumen_id = ph.id_konsumen
                WHERE
                    ph.status = '4' AND YEAR(p.tgl_order_masuk) = '".$tahun."' AND ph.is_deleted = '0'
                GROUP BY
                    MONTH(p.tgl_order_masuk)";
                    // die($sql);
        
        return dbGetRows($sql);
    }
    
    // Mendapatkan rincian pemasukan
    public function getDetailLaba($bulan, $tahun) {
        $sql = "SELECT
                    ph.id,
                    p.tgl_order_masuk,
                    p.kode_order,
                    k.nama,
                    k.alamat,
                    k.no_hp,
                    IF(
                        ph.jenis = '1',
                        ph.harga_setuju_pokok_granit,
                        ph.harga_setuju_pokok_keramik
                    ) AS 'harga_pokok',
                    (IF(
                        ph.jenis = '1',
                        ph.harga_setuju_granit,
                        ph.harga_setuju_keramik
                    ) - ph.diskon) AS 'harga_jual',
                    (IF(
                        ph.jenis = '1',
                        ph.harga_setuju_granit,
                        ph.harga_setuju_keramik
                    ) - ph.diskon) - IF(
                        ph.jenis = '1',
                        ph.harga_setuju_pokok_granit,
                        ph.harga_setuju_pokok_keramik
                    ) AS 'pendapatan'
                FROM
                    pengajuan_harga ph
                LEFT JOIN konsumen k ON
                    k.id = ph.id_konsumen
                LEFT JOIN pesanan p ON
                    p.konsumen_id = ph.id_konsumen
                WHERE
                    ph.status = '4' AND MONTH(p.tgl_order_masuk) = '".$bulan."' AND YEAR(p.tgl_order_masuk) = '".$tahun."'  AND ph.is_deleted = '0'";
                    
        return dbGetRows($sql);            
    }

    // Mendapatkan rincian pengeluaran 
    public function getDetailPengeluaran($bulan, $tahun) {
        $sql = "SELECT pl.id, pl.nominal AS 'pengeluaran', pl.keterangan, pl.tgl_input, pl.jenis
                FROM pembayaran_log pl
                WHERE pl.tipe = '2' AND pl.jenis IN ('1','5') AND MONTH(pl.tgl_input) = '".$bulan."' AND YEAR(pl.tgl_input) = '".$tahun."' AND pl.is_deleted = '0'";
                    
        return dbGetRows($sql);            
    }
    
    // DEV :
    public function getPengajuanDev($page=0, $status="0", $role, $bulan, $tahun){
        if($role == Role::PRODUKSI){
            $filter = " AND status in(4,5) ";
        } elseif ($role == Role::DIREKTUR || $role == Role::ADMINISTRATOR) {
            $filter = " AND status in(1,2,3,4,5)";
        } else {
            $filter = " AND status in(1,2,3,4) ";
        }
        // if ($status != "0") {
        //     $filter = " AND status = '".$status."'";
        // } else {
        //     $filter = "";
        // }
        
        $sql = "SELECT pengajuan_harga.*, konsumen.nama, konsumen.alamat, konsumen.no_hp, pesanan.kode_order, u1.name AS 'user_pengaju', u2.name AS 'user_approve'
                FROM pengajuan_harga
                LEFT JOIN konsumen ON pengajuan_harga.id_konsumen = konsumen.id
                LEFT JOIN pesanan ON konsumen.id = pesanan.konsumen_id
                LEFT JOIN users u1 ON pengajuan_harga.user_id_pengaju = u1.id
                LEFT JOIN users u2 ON pengajuan_harga.user_id_approve = u2.id
                WHERE 1 AND pengajuan_harga.is_deleted = '0' ".$filter." AND MONTH(pengajuan_harga.tgl_pengajuan) = '".$bulan."' AND YEAR(pengajuan_harga.tgl_pengajuan) = '".$tahun."'
                ORDER BY tgl_pengajuan DESC";
                
                
        // echo $sql;
        // die();
                
        return dbGetRows($sql);
    }
}

