<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gaji_desainer_model extends AppModel {

    protected $_filter = "";
    protected $_order = "";
    protected $_table = "gaji_desainer";
    protected $_view = "gaji_desainer";
    protected $_column = "*";
    protected $_join = "";
    
    // Penjelasan Status
    // 1 : DRAFT                : User masih bebas update jumlah 
    // 2 : MENUNGGU APPROVAL    : Menunggu approval dari direktur
    // 3 : REVISI               : Pengajuan ditolak oleh direktur dan harus direvisi
    // 4 : DISETUJUI            : Pengajuan disetujui oleh direktur
    // 5 : REVISI HPP           : Revisi Hpp oleh Manager Produksi dan menunggu approval direktur


    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function getGaji($desainer_id){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "select $this->_column from $this->_view gp where desainer_id=$desainer_id $this->_filter";
        return dbGetRows($sql);
    }

    public function getGajiRow($desainer_id){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "select $this->_column from $this->_view gp where desainer_id=$desainer_id $this->_filter";
        // die($sql);
        return dbGetRow($sql);
    }

    public function getAll(){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "select $this->_column from $this->_view gp $this->_filter";
        return dbGetRow($sql);
    }


    public function create($data, $skip_validation = TRUE, $return = TRUE) {
        $dataInsert = array(
            'desainer_id'        => $data['desainer_id'],
            'bulan'              => $data['bulan'],
            'tahun'              => $data['tahun'],
            'gaji_pokok'         => $data['gaji_pokok'],
            'gaji_tunjangan'     => $data['gaji_tunjangan'],
            'created_at'        => date('Y-m-d G:i:s', time()),
            'updated_at'        => date('Y-m-d G:i:s', time())
        );
        // echo "<pre>";print_r($dataInsert);die();
        // $create = dbInsert();
        $create = parent::create($dataInsert, true, true);
        if ($create) {
            return $create;

        }else{
            return false;
        }
    }

    public function edit($data, $gaji_desainer_id,  $skip_validation = TRUE) {
        $dataUpdate = $data; // gaji_pokok,gaji_tunjangan
        $dataUpdate['updated_at'] =date('Y-m-d G:i:s', time());
        // echo $pesanan_id."<br>";
        // echo "<pre>";print_r($dataUpdate);die();
        // $save =dbUpdate($this->_table, $dataUpdate, "id=$pesanan_id");
        // // die();
        // return true;
        $save = parent::save($gaji_desainer_id,$dataUpdate, false);
        return $save;

    }

    

}
?>