<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Konsumen_model extends AppModel {

    public $limit = 10;

    // FLOW
    const FLOW_ORDERMASUK = 1;
    const FLOW_PENGAJUANKUSEN = 2;


    //STATUS
    const STATUS_PROSES = 'P';
    const STATUS_SELESAI = 'S';
    const STATUS_ARSIP = 'A';

    public $limit_quota = 3;   
    protected $_filter = "";
    protected $_order = "";
    protected $_table = "konsumen";
    protected $_column = "*";
    protected $_join = "";

    protected $has_many = array(
        'Comment',
    );
    protected $belongs_to = array(
        'User',
        'Group',
        'Category',
        'Updated_by' => array('primary_key' => 'userIdUpdated', 'model' => 'User_model')
    );
    protected $many_to_many = array(
        'Post_tag' => array('Post', 'Tag')
    );
    protected $label = array(
        'category_id' => 'Kategori',
        'image' => 'Gambar',
        'file' => 'Berkas',
        'video' => 'Video'
    );
    protected $validation = array(
        'link' => 'valid_url',
        'category_id' => 'required|integer'
    );

    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function resetVariables(){
        $this->_filter = '';
        $this->column = '*';
        $this->_join = '';
        $this->_show_sql = false;
    }

    public function updateKonsumen($data){
        $dataUpdate = array(
            'nama'      => $data['nama'],
            'alamat'    => $data['alamat'],
            'no_hp'     => $data['no_hp']
        );
        // $save = dbUpdate($this->_table, $dataUpdate, "id=".$data['id_konsumen']);
        $save = parent::save($data['id_konsumen'], $dataUpdate, true);
        return $save;
    }

    public function create($data, $skip_validation = TRUE, $return = TRUE) {
        $data['created_at'] = date('Y-m-d G:i:s', time());
        $data['updated_at'] = date('Y-m-d G:i:s', time());
        $create = parent::create($data, true, true);
        if ($create) {
            return $create;

        }else{
            return false;
        }
    }
    
    // Digunakan untuk select konsumen di :
    // - UPDATE DATA Pengajuan Konsumen
    // - INPUT Kas
    public function getKonsumenPengajuan($status='0', $keyword='') {
        if ($status == '4') {
            $filterstatus = "";
        } else {
            $filterstatus = "NOT";
        }
        
        $sql = "SELECT
                    p.id,
                    k.id AS konsumen_id,
                    k.nama AS nama_konsumen,
                    k.alamat AS alamat_konsumen,
                    k.no_hp AS no_hp_konsumen,
                    p.kode_order,
                    p.status_produksi
                FROM
                    pesanan p
                LEFT JOIN konsumen k ON
                    p.konsumen_id = k.id
                WHERE
                    (
                        k.nama LIKE '%".$keyword."%' OR p.kode_order LIKE '%".$keyword."%'
                    ) AND tipe = 'P' AND p.is_deleted = '0' AND k.id IN(
                    SELECT
                        ph.id_konsumen
                    FROM
                        pengajuan_harga ph
                    WHERE
                        ph.id_konsumen <> '0' AND ph.is_deleted = '0'
                ) 
            --    AND(
            --        SELECT
            --            IF(
            --                EXISTS(
            --                SELECT
            --                    *
            --                FROM
            --                    pembayaran_log pl
            --                WHERE
            --                    `jenis` = 3 AND pl.id_konsumen = k.id
            --            ), 1, 0)) = 0
                AND NOT EXISTS(
                        SELECT * from pembayaran_log pl 
                        where `jenis` = 3 AND pl.id_konsumen = k.id
                    )
                ORDER BY
                    p.created_at
                DESC";
        return dbGetRows($sql);
    }

    public function getKonsumenDealPengajuan($keyword='', $filter) {
        $sql = "SELECT
                    id AS id_pengajuan,
                    nama_konsumen,
                    alamat_konsumen,
                    no_hp_konsumen
                FROM
                    pengajuan_harga 
                WHERE
                    (id_konsumen = 0 or id_konsumen=null )AND status = 4 AND is_deleted = 0 AND LOWER(nama_konsumen) LIKE '%".strtolower($keyword)."%' $filter
                ORDER BY id DESC";
        return dbGetRows($sql);
    }
    
    public function getKonsumenUpdatePengajuan($keyword='') {
        $sql = "SELECT
                    p.id,
                    k.id AS konsumen_id,
                    k.nama AS nama_konsumen,
                    k.alamat AS alamat_konsumen,
                    k.no_hp AS no_hp_konsumen,
                    p.kode_order,
                    p.status_produksi
                FROM
                    pesanan p
                LEFT JOIN konsumen k ON
                    p.konsumen_id = k.id
                WHERE
                    (
                        k.nama LIKE '%".$keyword."%' OR p.kode_order LIKE '%".$keyword."%'
                    ) AND tipe = 'P' AND p.is_deleted = '0' AND k.id NOT IN(
                    SELECT
                        ph.id_konsumen
                    FROM
                        pengajuan_harga ph
                    WHERE
                        ph.id_konsumen = '0' AND ph.is_deleted = '0' AND ph.status = '4'
                ) AND p.flow_id < 8
                ORDER BY
                    p.created_at
                DESC";
        return dbGetRows($sql);
    }
    
    public function createDev($data, $skip_validation = TRUE, $return = TRUE) {
        $data['created_at'] = date('Y-m-d G:i:s', time());
        $data['updated_at'] = date('Y-m-d G:i:s', time());
        
        
        // echo "<pre>";print_r($data);die();
        
        $create = parent::create($data, true, true);
        if ($create) {
            return $create;

        }else{
            return false;
        }
    }

    public function getRow($konsumen_id){
        if($konsumen_id){
            $sql = "select $this->_column from $this->_table where id=$konsumen_id";
            $data = dbGetRow($sql);
            return $data;
        }
        return null;
    }

    public function getAllKonsumen($keyword='') {
        $sql = "SELECT
                    k.id AS konsumen_id,
                    k.nama AS nama_konsumen,
                    k.alamat AS alamat_konsumen,
                    k.no_hp AS no_hp_konsumen
                FROM
                    konsumen k
                WHERE
                    LOWER(nama) LIKE '%".strtolower($keyword)."%' OR LOWER(alamat) LIKE '%".strtolower($keyword)."%'
                ORDER BY id DESC
                LIMIT 100";
        return dbGetRows($sql);
    }



}