<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Detail_pengajuan_komplain_model extends AppModel {

    protected $_filter = "";
    protected $_order = "";
    protected $_table = "detail_pengajuan_komplain";
    // protected $_view = "harga_view";
    protected $_column = "*";
    protected $_join = "";


    public function order(){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join = $join;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function create($data, $user_id, $skip_validation = TRUE, $return = TRUE) {
        // echo "<pre>";
        // var_dump($data);
        // die();
        
        $create = parent::create($data, true, true);
        if ($create) {
            return $create;
        }else{
            return false;
        }
    }

    public function updateHarga($data){
        $dataUpdate = array(
            'hrg_pokok_granit'      => $data['hrg_pokok_granit'],
            'hrg_pokok_keramik'     => $data['hrg_pokok_keramik'],
            'hrg_jual_granit'       => $data['hrg_jual_granit'],
            'hrg_jual_keramik'      => $data['hrg_jual_keramik'],
            'jumlah'                => $data['jumlah'],
            'created_at'            => date('Y-m-d G:i:s', time()),
            'updated_at'            => date('Y-m-d G:i:s', time()),
            'updated_by'            => $data['user_id']
        );
        $save = parent::save($data['id'], $dataUpdate, true);
        return $save;
    }
    
    public function getDetailPengajuan($id_pengajuan){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "SELECT h.nama, d.*
                FROM ".$_table." d, harga_maintenance h
                WHERE 1 AND d.id_harga_maintenance = h.id AND d.id_pengajuan_harga_komplain = ".$id_pengajuan;
        return dbGetRows($sql);
    }
    
    public function deleteDetail($id){
        if(!$this->_column){
            $this->_column = '*';        
        }
        $sql = "UPDATE detail_pengajuan_komplain SET is_deleted = '1' WHERE id = ".$id;
        return dbQuery($sql);
    }

}

