
<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Daftar User</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: right;">
		  <a href="<?= site_url("web/$class/adduser")  ?>" class="btn btn-sm btn-add"><i class="fas fa-plus"></i> Tambah</a>			  
        </span>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

		<!-- Tab panes -->
		<div class="tab-content">
		  <div role="tabpanel" class="tab-pane fade in active show" id="baru">
		  	<?php foreach ($user as $key): ?>
				<a href="<?= site_url('web/user/detail/'.$key['id'])  ?>" class="row mt-4 row-order-baru" style="cursor:pointer;text-decoration:none;color:#212529">
					<div class="col-7 col-sm-6 col-md-9 list-order-baru">
						<label><?= $key['name']  ?></label>
						<p><?= $key['no_hp']  ?></p>
					</div>
					<div class="col-5 col-sm-6 col-md-3 list-order-baru text-right">
						<label></label>
						<p><?= $key['no_hp_konsumen']  ?></p>
						<div class="status-produksi">
							<?= $key['username'] ?>
						</div>
					</div>
				</a>
			<?php endforeach; ?>
		  </div>

		  
		</div>
    </div>
</div>
<!-- /.row -->

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pencarian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post">
      <div class="modal-body">
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="kode_order" class="form-control" placeholder="Kode Order">
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="nama" class="form-control" placeholder="Nama">
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="alamat" class="form-control" placeholder="Alamat">
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="tlp" class="form-control" placeholder="No HP">
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-sm btn-secondary" >Reset</a>
        <button type="submit" class="btn btn-sm btn-add">Terapkan</button>
      </div>
	  </form>
    </div>
  </div>
</div>