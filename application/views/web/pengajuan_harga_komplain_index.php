<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Pengajuan Harga Komplain</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
    <div class="flashdata">
      <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
          <?php echo $srvmsg ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
    </div>
    <?php } ?>

<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button>
        </span>
        <span style="float: right;">
        <?php if(in_array(SessionManagerWeb::getRole(), array(Role::PEMASANG, Role::PRODUKSI))){ ?>
		  <a href="<?php echo site_url('web/pengajuan_harga_komplain/getpengajuanadd')?>" class="btn btn-sm btn-add"><i class="fas fa-plus"></i> Tambah</a>
        <?php } ?>
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
    	<div class="kalender-kas text-center">
    		<button type="button" id="prev" class="btn btn-sm btn-light float-left"><i class="fas fa-angle-left"></i></button>
    		<label id="tanggal_kas"><?= xIndoMonth($_SESSION['pengajuan_harga_komplain']['bulan']).' '.$_SESSION['pengajuan_harga_komplain']['tahun'] ?></label>
        
            <button type="button" id="next" class="btn btn-sm btn-light float-right"><i class="fas fa-angle-right"></i></button>
            <form id="main_form" action="<?php echo site_url('web/pengajuan_harga_komplain')?>" method="post">
              <input type="hidden" name="bulan" id="month" value="<?= $_SESSION['pengajuan_harga_komplain']['bulan']?>">
              <input type="hidden" name="tahun" id="year" value="<?= $_SESSION['pengajuan_harga_komplain']['tahun']?>">
            </form>
    	</div>
    </div>
    <div class="col-md-12">
       	<?php foreach ($data as $key) { ?>
       <div class="row mt-4 row-order-baru" onclick="detailpengajuan('<?= $key['id'] ?>')" style="cursor:pointer;color:#212529;">
          <div class="col-7 col-sm-6 col-md-8 list-order-baru">
            <label><?= (!empty($key['nama'])) ? $key['nama'].' ('.$key['kode_order'].')' : $key['nama_konsumen']  ?></label>
            <p><?= $key['user_pengaju']  ?></p>
            <p><?= $key['keterangan']  ?></p>
          </div>
          <div class="col-5 col-sm-6 col-md-4 list-order-baru text-right">
            <p><?= xFormatDateInd($key['tgl_pengajuan'])?></p>
            <p><?= $key['no_hp_konsumen']  ?></p>
            <div class="status-produksi">
              <?php if($key['status'] == 1) {
                echo '<span style="color: #3f51b5;"><b>Draft</b></span>';
              }elseif($key['status'] == 2) {
                echo '<span style="color: #ffc107;"><b>Menunggu Approval</b></span>';
              }elseif($key['status'] == 3) {
                echo '<span style="color: #f44336;"><b>Revisi</b></span>';
              }elseif($key['status'] == 4) {
                echo '<span style="color: #4caf50;"><b>Disetujui</b></span>';

                if ($key['revisi_hpp'] == 1 && (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) ) {              
                  echo '<br><span style="background-color: #f44336; color: #fff;"><b><i class="fas fa-spinner"></i> HPP</b></span>';
                }
              }elseif($key['status'] == 5) {
                echo '<span style="color: #f44336;"><b>Revisi HPP</b></span>';
              } ?>

            </div>
          </div>
        </div>
        <?php } ?>
    </div>
</div>

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Urutkan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post">
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-12 col-md-12 col-md-12">
            <input type="text" name="keyword" class="form-control" placeholder="Kata Kunci" value="<?= $filter_keyword ?>">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12 col-md-12 col-md-12">
            <select name="status" class="form-control">
         
                <option value="0" <?= ($filter_status == 0) ? 'selected' : ''   ?>>Semua</option>
                <option value="1" <?= ($filter_status == 1) ? 'selected' : ''   ?>>Draft</option>
                <option value="2" <?= ($filter_status == 2) ? 'selected' : ''   ?>>Menunggu Approval</option>
                <option value="3" <?= ($filter_status == 3) ? 'selected' : ''   ?>>Revisi</option>
                <option value="4" <?= ($filter_status == 4) ? 'selected' : ''   ?>>Disetujui</option>
                <option value="5" <?= ($filter_status == 5) ? 'selected' : ''   ?>>Revisi Hpp</option>
              
            </select>
          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <a href="<?= site_url("web/$class/$method") ?>" class="btn btn-sm btn-secondary" >Reset</a>
        <button type="submit" class="btn btn-sm btn-add">Terapkan</button>
      </div>
    </form>
    </div>
  </div>
</div>

<script>
	$(document).ready(function(){
    
        $('#prev').click(function(){
          if($('#month').val() == '01' ){
            var bulan = 12;
            var tahun = parseInt($('#year').val()) - 1;
            $('#year').val(tahun);
          }else if (parseInt($('#month').val()) <= 12) {
            var bulan = parseInt($('#month').val()) - 1;
            var tahun = $('#year').val();
          }

          bulan = tambahNol(bulan);
          $('#month').val(bulan);
          $('#main_form').submit();

        })

        $('#next').click(function(){
          if($('#month').val() == '12' ){
            var bulan = 1;
            var tahun = parseInt($('#year').val()) + 1;
            $('#year').val(tahun);
          }else if(parseInt($('#month').val()) >= 1){
            var bulan = parseInt($('#month').val()) + 1;
            var tahun = $('#year').val();
          }
          
          bulan = tambahNol(bulan);

          $('#month').val(bulan);
          $('#main_form').submit();
          
        })
      })

      function tambahNol(x){
         y=(x>9)?x:'0'+x;
         return y;
      }

      function bulanTahunInd(x){
        var dt = new Date(x);
        var monthNames = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
        
        var y = monthNames[dt.getMonth()]+' '+dt.getFullYear();

        return y;
      }

      function tanggalBulanTahunInd(x){
        var dt = new Date(x);
        var monthNames = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
        
        var y = dt.getDay()+' '+monthNames[dt.getMonth()]+' '+dt.getFullYear();

        return y;
      }

      function detailpengajuan(id){
        window.location.href = '<?= site_url('web/pengajuan_harga_komplain/getonepengajuan')?>/'+id;
      }
</script>