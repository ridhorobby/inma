<style>
  .history-konsumen-row:hover{
    box-shadow: 2px 2px 5px 2px #ddd;
  }
</style>
<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Piutang Konsumen</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<!-- <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button> -->
        </span>
        <span style="float: right;">
		  <a href="<?php echo site_url('web/pembayaran_log/getliststatuspiutangkonsumenadd')?>" class="btn btn-sm btn-add"><i class="fas fa-plus"></i> Tambah</a>
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
    	<div class="kalender-kas text-center">
    		<button type="button" id="prev" class="btn btn-sm btn-light float-left"><i class="fas fa-angle-left"></i></button>
    		<label><?= xIndoMonth($_SESSION['piutang_konsumen']['bulan']).' '.$_SESSION['piutang_konsumen']['tahun'] ?></label>
        <button type="button" id="next" class="btn btn-sm btn-light float-right"><i class="fas fa-angle-right"></i></button>
        <form id="main_form" action="<?php echo site_url('web/pembayaran_log/getListStatusPiutangKonsumen')?>" method="post">
        <input type="hidden" id="month" name="bulan" value="<?= $_SESSION['piutang_konsumen']['bulan'] ?>">
        <input type="hidden" id="year" name="tahun" value="<?= $_SESSION['piutang_konsumen']['tahun'] ?>">
        </form>
    	</div>
    </div>
    <div class="col-md-12">

      <?php 
      $icon_check = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content" style="margin-top: -20px;"><div class="history-icon"><div class="history-bullet-green"><i class="fas fa-check"></i></div></div></div>';

      $icon_pause = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content" style="margin-top: -20px;"><div class="history-icon"><div class="history-bullet-red"><i class="fas fa-pause"></i></div></div></div>';

      $icon_process = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content" style="margin-top: -20px;"><div class="history-icon"><div class="history-bullet-red"><i class="fas fa-spinner"></i></div></div></div>';

      $icon_grey = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content" style="margin-top: -20px;"><div class="history-icon"><div class="history-bullet-default"></div></div></div>';
      
      $omzet = 0;
      $hutang = 0;
      foreach ($data as $key) { 
        $omzet += $key['harga_jual'];
        $hutang += $key['sisa_pembayaran'];
        ?>
    	<div class="row history-konsumen-row" style="margin-left: 0;margin-right: 0;padding-bottom: 25px;padding-top: 10px;cursor: pointer;" onclick="prviewstruk('<?= $key['id_pengajuan']?>')">
        <div class="col-12 col-sm-12 col-md-12">
          <label><?= $key['kode_order'] ?></label>
          <span class="pl-2 float-right"><?= $key['nama']?></span>
        </div>
        <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-green">
          <h6 class="text-truncate">Total Nilai</h6>
          <p><?= number_format($key['harga_jual'],0,",",".") ?></p>
          <p><?= xFormatDateInd($key['tgl_order_masuk'])?></p>
        </div>
        <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-yellow">
          <h6 class="text-truncate">Tanda Jadi</h6>
          <p><?= number_format($key['tanda_jadi'],0,",",".")?></p>
          <p><?= xFormatDateInd($key['tgl_tanda_jadi'])?></p>
        </div>
        <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-purple">
          <h6>Dp</h6>
          <p><?= number_format($key['dp'],0,",",".")?></p>
          <p><?= xFormatDateInd($key['tgl_dp'])?></p>
        </div>
        <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-blue">
          <h6>Pelunasan</h6>
          <p><?= number_format($key['pelunasan'],0,",",".")?></p>
          <p><?= xFormatDateInd($key['tgl_pelunasan'])?></p>
        </div>
        <div class="col-12 col-sm-12 col-md-12 text-right" style="position: relative;top: 30px;">
          <p class="text-red"><?= number_format($key['harga_jual']-$key['tanda_jadi']-$key['dp']-$key['pelunasan'],0,",",".")?></p>
        </div>

        <?php 
          if($key['flow_id'] == 1){
            echo $icon_check.$icon_grey.$icon_grey.$icon_grey;
          }
          else if($key['flow_id'] == 2){
            echo $icon_check.$icon_pause.$icon_grey.$icon_grey;
          }
          else if($key['flow_id'] == 3 || $key['flow_id'] == 4){
            echo $icon_check.$icon_process.$icon_grey.$icon_grey;
          }
          else if($key['flow_id'] == 5){
            echo $icon_check.$icon_check.$icon_pause.$icon_grey;
          }
          else if($key['flow_id'] == 6){
            echo $icon_check.$icon_check.$icon_process.$icon_grey;
          }
          else if($key['flow_id'] == 7){
            echo $icon_check.$icon_check.$icon_check.$icon_process;
          }
          else if($key['flow_id'] == 8){
            echo $icon_check.$icon_check.$icon_check.$icon_check;
          } 
        ?>

        
      </div>
      <?php } ?>
    </div>
    <div class="col-md-12">
      <div class="row total-hutang-produksi-row" style="cursor: pointer;"  data-toggle="modal" data-target="#detailRincianPiutangModal">
        <?php 
        $allomzet = 0;
        $allhutang = 0;
        foreach ($sum as $key) {
          $allomzet += $key['harga_jual'];
          $allhutang += $key['sisa_pembayaran'];
          
        } ?>
        <div class="col-3 col-sm-3 col-md-4">
          <span>Omzet</span>
          <br>
          <label class="text-green" style="cursor: pointer;"><?= number_format($omzet,0,'.',',') ?></label>
          <p class="text-green">(<?= number_format($allomzet,0,'.',',') ?>)</p>
        </div>
        <div class="col-6 col-sm-6 col-md-4 text-center">
          <span>Akumulasi <?= $_SESSION['piutang_konsumen']['tahun'] ?></span>
        </div>
        <div class="col-3 col-sm-3 col-md-4 text-right">
          <span class="text-red">Piutang Konsumen</span>
          <br>
          <label class="text-red" style="cursor: pointer;"><?= number_format($hutang,0,'.',',') ?></label>
          <p class="text-red">(<?= number_format($allhutang,0,'.',',') ?>)</p>
        </div>
      </div>
    </div>
</div>

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Urutkan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row filter-kas">
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Tanggal Input Lama -> Terbaru</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Tanggal Input Terbaru -> Lama</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Jenis A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Jenis Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kode A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kode Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Debit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Debit Max -> Min</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kredit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kredit Max -> Min</div>
        		</a>
        	</div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<!-- Filter Modal -->
<div class="modal fade" id="detailRincianPiutangModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Rincian Piutang <?= $_SESSION['piutang_konsumen']['tahun'] ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row filter-kas">
          <div class="col-md-12">
             <div class="row total-hutang-produksi-row" style="margin: 0;border-top: none;">
              <div class="col-3 col-sm-3 col-md-4">
                <span class="text-green">Omzet</span>
                <br>
                <label class="text-green"><?= number_format($allomzet,0,'.',',') ?></label>
              </div>
              <div class="col-6 col-sm-6 col-md-4 text-center">
                <span>Total</span>
              </div>
              <div class="col-3 col-sm-3 col-md-4 text-right">
                <span class="text-red">Sisa Piutang</span>
                <br>
                <label class="text-red"><?= number_format($allhutang,0,'.',',') ?></label>
              </div>
            </div>
            <?php foreach ($sum as $key) { ?>
            <div class="row total-hutang-produksi-row" style="margin: 0;border-top: none;">
              <div class="col-3 col-sm-3 col-md-4">
                <p class="text-green"><?= number_format($key['harga_jual'],0,'.',',') ?></p>
              </div>
              <div class="col-6 col-sm-6 col-md-4 text-center">
                <span><?= xIndoMonth($key['bulan']) ?></span>
              </div>
              <div class="col-3 col-sm-3 col-md-4 text-right">
                <p class="text-red"><?= number_format($key['sisa_pembayaran'],0,'.',',') ?></p>
              </div>
            </div>
            <?php } ?>

          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<script>
	$(document).ready(function(){
    
    $('#prev').click(function(){
      if($('#month').val() == '01' ){
        var bulan = 12;
        var tahun = parseInt($('#year').val()) - 1;
        $('#year').val(tahun);
      }else if (parseInt($('#month').val()) <= 12) {
        var bulan = parseInt($('#month').val()) - 1;
        var tahun = $('#year').val();
      }

      bulan = tambahNol(bulan);
      $('#month').val(bulan);
      $('#main_form').submit();

    })

    $('#next').click(function(){
      if($('#month').val() == '12' ){
        var bulan = 1;
        var tahun = parseInt($('#year').val()) + 1;
        $('#year').val(tahun);
      }else if(parseInt($('#month').val()) >= 1){
        var bulan = parseInt($('#month').val()) + 1;
        var tahun = $('#year').val();
      }
      
      bulan = tambahNol(bulan);

      $('#month').val(bulan);
      $('#main_form').submit();
      
    })
  })

  function tambahNol(x){
     y=(x>9)?x:'0'+x;
     return y;
  }

  function prviewstruk(id){
    var url = '<?= site_url('web/pembayaran_log/getstruk')?>/'+id;
    window.location.href = url;
  //   window.open(url, '_blank');
  }
</script>