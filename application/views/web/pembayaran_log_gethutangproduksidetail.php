<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Hutang Produksi <?= xIndoMonth($bulan).' '.$tahun ?></h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
          <!-- <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button> -->
        </span>
        <span style="float: right;">
          <a class="btn btn-sm btn-add" href="<?php echo site_url('web/'.$class.'/gethutangproduksiperbulan')?>"><i class="fas fa-chevron-left"></i> Kembali</a>
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
       	<table class="table table-sm table-kas">
          <thead>
            <tr>
              <th class="jenis-kas-th">Kode Order</th>
              <th class="kode-kas-th"></th>
              <th class="debet-kas-th">Nilai Prod</th>
              <th class="kredit-kas-th">Pembayaran</th>
            </tr>
          </thead>
          <tbody>
          <?php
          $hutang_produksi = 0;
          $omzet = 0;
          if (!empty($beban)) {
            foreach ($beban as $v) {
              $no++;
              $hutang_produksi += $v['hutang_produksi']; 
              $omzet += $v['omzet']; 
          ?>
            <tr id="<?= $v['id_pengajuan']?>" class="rowkas" onclick="(prviewstruk('<?= $v['id_pengajuan']?>'))">
              <td colspan="4">
              	<div class="row kas-row">
	              	<div class="col-12 col-sm-12 col-md-12">
		              	<span style="float: left;"><?= xFormatDateInd($v['tgl_disetujui']) ?></span>
		              </div>
	              	<div class="col-12 col-sm-12 col-md-12">
	              		<div class="row">
	              			<div class="col-4 col-sm-4 col-md-6" style="font-weight: bold;">
                        <?= $v['kode_order'].' ('.$v['nama'].')' ?>
                        
                      </div>
  				            <div class="col-2 col-sm-2 col-md-2 text-right">
                        <label class="text-red ">(<?= (100/$v['omzet'])*$v['hutang_produksi'].'%'?>)</label>
                      </div>
  				            <div class="col-3 col-sm-3 col-md-2 text-center debet"><?= number_format($v['hutang_produksi'],0,',','.')?></div>
  				            <div class="col-3 col-sm-3 col-md-2 text-center kredit">0</div>
	              		</div>
              		</div>
              	</div>
              </td>
            </tr>
          <?php }
          } 

          $kredit = 0;
          if (!empty($lunas)) {
            foreach ($lunas as $v) {
              $no++;
              $kredit += $v['kredit']; 
          ?>
            <tr id="<?= $v['id']?>" class="rowkas">
              <td colspan="4">
                <div class="row kas-row">
                  <div class="col-12 col-sm-12 col-md-12">
                    <span style="float: left;"><?= xFormatDateInd($v['tgl_input']) ?></span>
                  </div>
                  <div class="col-12 col-sm-12 col-md-12">
                    <div class="row">
                      <div class="col-4 col-sm-4 col-md-6"></div>
                      <div class="col-2 col-sm-2 col-md-2"></div>
                      <div class="col-3 col-sm-3 col-md-2 text-center debet">0</div>
                      <div class="col-3 col-sm-3 col-md-2 text-center kredit"><?= number_format($v['kredit'],0,',','.')?></div>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          <?php }
          } ?>
          </tbody>
          <tfoot>
            <th colspan="2">Total</th>
            <th class="debet-kas-th">
              <div><?= number_format($hutang_produksi,0,'.',',') ?></div>
              <div>Omzet (<?= number_format($omzet,0,'.',',') ?>)</div>
                
            </th>
            <th class="kredit-kas-th"><?= number_format($kredit,0,'.',',') ?></th>
          </tfoot>
        </table>
    </div>
</div>

<script>
  function prviewstruk(id){
    var url = '<?= site_url('web/pembayaran_log/getstrukhutangproduksi')?>/'+id;
    window.location.href = url;
  //   window.open(url, '_blank');
  }
</script>
