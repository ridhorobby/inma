
 <div class="row mb-4">
    <div class="col-md-12 title-page">
            <h1>Set Gaji Pokok Desainer <?= $user['name']?></h1>
        </div>
  </div>

  <?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>

  <div class="row mb-4">
    <div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
            <?php if ($without_desainer_id==true){ ?>
              <a class="btn btn-sm btn-add" href="<?php echo site_url('web/fee_desainer/index/1')?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            <?php } else { ?>
              <a class="btn btn-sm btn-add" href="<?php echo site_url('web/fee_desainer/detail/'.$desainer_id)?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            <?php } ?>
              
            </span>
        </div>
  </div>    

    <div class="row harga-add">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<form action="<?= site_url("web/$class/") ?>/<?= ($data['id']==null) ? "create/$pesanan_id" : "update/$fee_desainer_id" ?>" method="POST">
              <?php if (!$without_desainer_id): ?>
                <input type="hidden" name="desainer_id" value="<?= $desainer_id ?>">
              <?php endif ?>
              <?php if ($desainer_id == $pesanan['desainer_design'] || $without_desainer_id==true): ?>
              <div class="form-group row">
                <label class="col-12 col-sm-12 col-md-2">PIC Desainer Desain</label>
                <div class="col-12 col-sm-12 col-md-4">
                  <label><?= ($pesanan['nama_desainer_design']) ? $pesanan['nama_desainer_design'] : '-'  ?></label>
                  <input type="hidden" name="desainer_design_id" value="<?= $pesanan['desainer_design']  ?>">
                </div>
              </div>



              <div class="form-group row">
                <label class="col-12 col-sm-12 col-md-2">Fee Desainer Desain</label>
                <div class="col-12 col-sm-12 col-md-4">
                  <input type="text" name="fee_design" class="form-control form-control-sm" value="<?= $data['fee_design']  ?>">
                </div>
              </div>
              <?php endif ?>
              
              <?php if ($desainer_id == $pesanan['desainer_eksekusi'] || $without_desainer_id==true): ?>
              <div class="form-group row">
                <label class="col-12 col-sm-12 col-md-2">PIC Desainer Eksekusi</label>
                <div class="col-12 col-sm-12 col-md-4">
                  <label><?= ($pesanan['nama_desainer_eksekusi']) ? $pesanan['nama_desainer_eksekusi'] : '-' ?></label>
                  <input type="hidden" name="desainer_eksekusi_id" value="<?= $pesanan['desainer_eksekusi']  ?>">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-12 col-sm-12 col-md-2">Presentase Eksekusi (%)</label>
                <div class="col-12 col-sm-12 col-md-4">
                  <input type="number" id="persentase_eksekusi" name="persentase_eksekusi" class="form-control form-control-sm" value="<?= $data['persentase_eksekusi']  ?>" step="any">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-12 col-sm-12 col-md-2">Fee Eksekusi</label>
                <div class="col-12 col-sm-12 col-md-4">
                  <input type="number" id="fee_eksekusi" name="fee_eksekusi" class="form-control form-control-sm" value="<?= $data['fee_eksekusi']  ?>" readonly>
                </div>
              </div>
              <?php endif ?>
              

              
              <div class="form-group text-center mt-5">
                <button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
              </div>
        	</form>
            
        </div>
    </div>

<script type="text/javascript">
	$("#persentase_eksekusi").on('keyup',function(){
	var harga_deal = parseFloat(<?= ($pesanan['harga_deal']==null || $pesanan['harga_deal']=='') ? 0 : $pesanan['harga_deal']  ?>)
	  var fee_eksekusi = Math.round((parseFloat($(this).val())/100) * harga_deal);
    console.log(harga_deal);
    console.log(fee_eksekusi);
	  $("#fee_eksekusi").val(fee_eksekusi);
	})
</script>
<?php 
	// echo "<pre>";var_dump($data);echo "</pre>";
?>
