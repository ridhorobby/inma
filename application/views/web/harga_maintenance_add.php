<div class="row mb-4">
    <div class="col-md-12 title-page">
            <h1>Input Harga Maintenance</h1>
        </div>
  </div>

  <?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>

  <div class="row mb-4">
    <div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
          <a class="btn btn-sm btn-add" href="<?php echo site_url('web/harga_maintenance')?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            </span>
        </div>
  </div>    

    <div class="row harga-add">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <form action="<?php echo site_url('web/'.$class)?>/<?= ($method=='edit' ? 'update' : 'create') ?>" method="post">
          <?php if($method=='edit'){ ?>
            <input type="hidden" name="id" value="<?= $data['id'] ?>">
          <?php } ?>
              <div class="form-group row">
                <label class="col-12 col-sm-12 col-md-2">Nama</label>
                <div class="col-12 col-sm-12 col-md-4">
                  <input type="text" name="nama" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['nama'] : '') ?>">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-12 col-sm-12 col-md-2">Harga</label>
                <div class="col-12 col-sm-12 col-md-4">
                  <input type="number" name="harga" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['harga'] : '') ?>">
                </div>
              </div>

              
              <div class="form-group text-center mt-5">
                <button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
                <button type="button" onclick="hapus(<?= $data['id'] ?>)" class="btn btn-sm btn-add"><i class="fas fa-trash"></i> Hapus</button>
              </div>
              <div class="form-group text-center col-md-6 mt-5">
                
              </div>
          </form>
            
        </div>
    </div>

 <script type="text/javascript">
  function hapus(id){
    var conf = confirm("Apakah anda yakin ingin menghapus data?");
    if(conf){
      window.location.href = "<?php echo site_url('web/harga_maintenance/delete')?>/"+id;
    }
  }
 </script>
