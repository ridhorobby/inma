<style>
  .logo-nasatech{
    text-align: right;
  }
  .logo-nasatech > label{
    font-weight: 400;
      font-size: 2em;
      margin: 0;
  }
  .logo-nasatech > label > b{
    color: #f71212;
  }
  .logo-nasatech > span{
    float: right;
      font-size: .75rem;
        position: relative;
      top: -15px;
      right: 10px;
  }
</style>

 <div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Fee Desainer <?= $data[0]['nama_desainer'] ?></h1>
        <h3><?= date('F', mktime(0, 0, 0, $_SESSION['order_desainer']['bulan'], 10)) ." ".$_SESSION['order_desainer']['tahun'] ?></h3>
    </div>
</div>

<?php if (isset($srvok)) { ?>
    <div class="flashdata">
      <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
          <?php echo $srvmsg ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
    </div>
<?php } ?>

<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
        </span>
        <span style="float: right;">
		  <a href="<?php echo site_url('web/fee_desainer/index/1')?>" class="btn btn-sm btn-add"><i class="fas fa-chevron-left"></i> Kembali</a>
		  <a href="<?= site_url('web/fee_desainer/gajidesain/'.$desainer_id.'/'.$bulan.'/'.$tahun) ?>" class="btn btn-sm btn-info"><i class="fas fa-plus"></i> Set Gaji Pokok</a>
      <!-- <a href="<?= site_url('web/fee_desainer/getgaji/'.$desainer_id.'/'.$bulan.'/'.$tahun) ?>" class="btn btn-sm btn-warning"><i class="fas fa-file-image"></i> Cetak Slip Gaji</a> -->
		  <a href="#" class="btn btn-sm btn-warning cetak-slip-gaji"><i class="fas fa-file-image"></i> Slip Gaji</a>
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-sm-12 col-md-12 mb-4">
      <h4>Staff</h4>
      <div class="row">
        <div class="col-4 col-sm-2 col-md-2">Nama</div>
        <div class="col-8 col-sm-10 col-md-4">
          <?= $user['name'] ?>
        </div>
      </div>
      <div class="row">
        <div class="col-4 col-sm-2 col-md-2">No Rekening</div>
        <div class="col-8 col-sm-10 col-md-4">
          <?= $user['no_rekening'] ?>
        </div>
      </div>

      
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <h4>Rincian Fee</h4>
      <div class="row">
        <div class="col-4 col-sm-2 col-md-2">Gaji Pokok</div>
        <div class="col-8 col-sm-10 col-md-4 text-right">
          Rp <?= (!empty($gaji_pokok)) ? number_format($gaji_pokok,0,",",".") : 0; ?>
        </div>
      </div>
      <div class="row mb-4">
        <div class="col-4 col-sm-2 col-md-2">Tunjangan</div>
        <div class="col-8 col-sm-10 col-md-4 text-right">
          Rp <?= (!empty($gaji_tunjangan)) ? number_format($gaji_tunjangan,0,",",".") : 0; ?>
        </div>
      </div>  

      <div class="row mb-4">
        <div class="col-12 col-sm-12 col-md-12">List Pekerjaan</div>
        <div class="col-12 col-sm-12 col-md-12">
          
          

        <?php 
        $arr_fee_eksekusi = 0;
        $arr_fee_desain = 0;
        foreach ($data as $key) { 
          $arr_fee_eksekusi += (!empty($key['fee_eksekusi']) && ($key['desainer_eksekusi'] == $desainer_id)) ? $key['fee_eksekusi'] : 0;
          $arr_fee_desain += (!empty($key['fee_design']) && ($key['desainer_design'] == $desainer_id)) ? $key['fee_design'] : 0;
        ?>
      
        <div class="row row-order-baru" onclick="listorder(<?= $desainer_id ?>,<?= $key['id'] ?>)" style="cursor:pointer;color:#212529;border-bottom: 0;">
          <div class="col-7 col-sm-6 col-md-9 list-order-baru pr-0">
            <div style="font-weight: bold;border-bottom: 1px solid #ccc;"><?= $key['kode_order'] ?></div>
            <div>Fee Desain</div>
            <div>Fee Eksekusi</div>
            <div style="border-top: 1px solid #ccc;">Total</div>
          </div>
          <div class="col-5 col-sm-6 col-md-3 list-order-baru text-right pl-0">
            <div style="border-bottom: 1px solid #ccc;">Rp 0</div>
            
            <div>Rp <?= (!empty($key['fee_design']) && ($key['desainer_design'] == $desainer_id)) ? number_format($key['fee_design'],0,",",".") : 0; ?></div>
            <div>Rp <?= (!empty($key['fee_eksekusi']) && ($key['desainer_eksekusi'] == $desainer_id)) ? number_format($key['fee_eksekusi'],0,",",".") : 0; ?></div>
            <?php  
              $fee_eksekusi = ($key['desainer_eksekusi'] == $desainer_id) ? $key['fee_eksekusi'] : 0;
              $fee_design   = ($key['desainer_design'] == $desainer_id) ? $key['fee_design'] : 0;
            ?>
              
            <div style="border-top: 1px solid #ccc;">Rp <?= number_format($fee_eksekusi+$fee_design,0,",","."); ?></div>
          </div>
          
        </div>
        <?php } ?>
        
        </div>
      </div>    
      <div class="row">
        <div class="col-4 col-sm-2 col-md-2">Fee Desain</div>
        <div class="col-8 col-sm-10 col-md-10 text-right">
          Rp <?= number_format($arr_fee_desain,0,",","."); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-4 col-sm-2 col-md-2">Fee Eksekusi</div>
        <div class="col-8 col-sm-10 col-md-10 text-right">
          Rp <?= number_format($arr_fee_eksekusi,0,",","."); ?>
        </div>
      </div>

      <div class="row mb-4" style="border-top: 1px solid #ccc;">
        <div class="col-4 col-sm-2 col-md-2">Take Home Pay</div>
        <div class="col-8 col-sm-10 col-md-10 text-right">
          Rp <?= number_format($arr_fee_desain+$arr_fee_eksekusi+$gaji_pokok+$gaji_tunjangan,0,",","."); ?>
        </div>
      </div>

      <!-- <table class="table table-striped">
		 	<thead>
		 		<tr>
		 			<th>Kode Pesanan</th>
		 			<th>Fee</th>
		 		</tr>
		 	</thead>
		 	<tbody>
		 		<?php foreach ($data as $key): ?>
		 		<tr>
		 			<td><?= $key['kode_order']  ?></td>
		 			<td style="width: 20%;"><a href="<?= site_url("web/$class/feedesain/$desainer_id/".$key['id']) ?>" class="btn btn-sm btn-outline-secondary"> <i class="fas fa-dollar-sign"></i> Set Fee</a></td>
		 		</tr>
		 		<?php endforeach ?>
		 	</tbody>
		 </table> -->
    </div>
</div>

<!-- Cetak Slip gaji Modal -->
<div class="modal fade" id="cetakSlipGajiModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: auto;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    <div class="row">

        <div class="col-12 col-sm-12 col-md-12" id="canvasslipgaji" style="background-color: #fff;border: 1px solid #ccc;">
          <div style="position: absolute;top: 10px;left: 20px;">
            <h3>Slip Gaji</h3>
          </div>
          <div class="logo-nasatech">
            <label>Nasa<b>tech</b></label>
            <br>
            <span>Kitchenset</span>
          </div>
          <table class="table table-borderless" style="margin-top: 15px;">
            <tr>
              <td style="font-weight: bold;width:25%;">Nama</td>
              <td style="width:30%;"><?= $user['name'] ?></td>
              <td style="font-weight: bold;width: 17%;">Bulan</td>
              <td><?= xIndoMonth($bulan) ?></td>
            </tr>
            <tr>
              <td style="font-weight: bold;">Gaji Pokok</td>
              <td><?= number_format($cetak_gaji['gaji_pokok'],0,",",".") ?></td>
              <td style="font-weight: bold;">No. Rekening</td>
              <td><?= $user['bank_rekening'].' - '.$user['no_rekening'] ?></td>
            </tr>
            <tr>
              <td style="font-weight: bold;">Tunjangan</td>
              <td><?= number_format($cetak_gaji['gaji_tunjangan'],0,",",".") ?></td>
            </tr>
            <tr>
              <td style="font-weight: bold;">Fee Desain</td>
              <td><?= number_format($cetak_gaji['fee_design'],0,",",".") ?></td>
            </tr>
            <tr>
              <td style="font-weight: bold;">Fee Eksekusi</td>
              <td><?= number_format($cetak_gaji['fee_eksekusi'],0,",",".") ?></td>
            </tr>
            <tr>
              <td style="font-weight: bold;">Total Take Home Pay</td>
              <td><?= number_format($cetak_gaji['total_gaji'],0,",",".") ?></td>
            </tr>
          </table>
          
        </div>

        <div class="col-12 col-sm-12 col-md-12 mt-2 text-right">
          <a class="btn btn-sm btn-add download-slip-gaji float-right" href="#"><i class="fas fa-download"></i> Download</a>
        </div>

    </div>

      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url()?>assets/web/js/html2canvas.js"></script>

<script>
  $(document).ready(function(){
    
    $('.cetak-slip-gaji').click(function(){
      $('#cetakSlipGajiModal').modal('show');

      setInterval(function(){ 
        getslipgaji();
        // $('.download-slip-gaji').click(); 
      }, 2000);
    })


    // $('.download-slip-gaji').click(function(){

    

      
      
    // });

  })

  function listorder(desainer_id, pesanan_id){
    window.location.href="<?= site_url('web/fee_desainer/feeDesain/') ?>/"+pesanan_id+"/"+desainer_id;
  }
  function getslipgaji(){
    html2canvas($('#canvasslipgaji'), {
    height: 1000,
    onrendered: function (canvas) {

            var imgageData =  
          canvas.toDataURL("image/png",1); 
         
          var newData = imgageData.replace( 
          /^data:image\/png/, "data:application/octet-stream"); 
         
          $(".download-slip-gaji").attr( 
          "download", "slipgaji.png").attr( 
          "href", newData);


        }
    });
  }
</script>
 <?php 
	// echo "<pre>";print_r($data);echo "</pre>";
?>
