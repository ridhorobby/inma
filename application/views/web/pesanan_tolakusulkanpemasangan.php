<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/web/plugins/evo-calendar/css/evo-calendar.css">
<script src="<?php echo base_url();?>assets/web/plugins/evo-calendar/js/evo-calendar.js"></script>

	<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Tolak</h1>
        </div>
	</div>

	<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>

	<div class="flashdata">
	  <div class="alert alert-danger alert-dismissible" role="alert">
	    <i class="fas fa-exclamation-triangle"></i> Usulan tanggal akan ditolak, silahkan usulkan tanggal lain.
	  </div>
	</div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
           	<div id="calendar"></div>
        </div>
  	</div>
  <!-- /.row -->
  	<form action="<?= site_url('web/pesanan/usulan/'.$detail['id'])  ?>" method="post">
  		<input type="hidden" name="back" value="pesanan/detail">
  		<input type="hidden" name="id" value="<?= $detail['id']?>">
  		<input type="hidden" name="jadwal_usulan" id="tanggal_usulkan" value="<?php echo date('Y-m-d') ?>">
  		<input type="hidden" name="confirm" value="0">

  		<div class="form-group row mt-5">
       		<label class="col-12 col-sm-12 col-md-2">Jam</label>
       		<div class="col-12 col-sm-12 col-md-2">
				<div class="input-group clockpicker">
					<input type="text" name="jam_usulan" class="form-control form-control-sm" value="" readonly required placeholder="Masukkan Jam" style="background-color: transparent;">
				</div>
       		</div>
       	</div>
  		
  		<div class="form-group text-center mt-5">
       		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
       	</div>
  	</form>
<script type="text/javascript">
	
	var active_events = [];

	var bulan = '<?php echo date('m');?>';
	var tahun = '<?php echo date('Y');?>';

	// var bulan = '08';
	// var tahun = '2020';

	getKalendarView(bulan,tahun);

	$(document).ready(function(){

		$(document).on('click','.day', function(){
			var date = $(this).attr('data-date-val');
			$('#tanggal_usulkan').val(date);

		})

		$(document).on('click','.month', function(){
			var datavalue = $(this).attr('data-month-val');
			var bulan_temp = parseInt(datavalue) + 1;

			var bulan = tambahNol(bulan_temp);

			var tahun = $('.calendar-year p').html();

			getKalendarView(bulan,tahun);

		})

		$(document).on('click','.calendar-year button:first', function(){
			var datavalue = $('.calendar-months').find('.active-month').attr('data-month-val');
			var bulan_temp = parseInt(datavalue) + 1;

			var bulan = tambahNol(bulan_temp);
			var tahun = $('.calendar-year p').html();
			
			getKalendarView(bulan,tahun);

		})

		$(document).on('click','.event-container', function(){
			var id = $(this).attr('data-event-index');
			window.location.href = '<?php echo site_url('web/pesanan/detail')?>/'+id;
		})
	});

	$('#calendar').evoCalendar({
		language: 'id',
    	theme: 'Royal Navy',
    	todayHighlight: true,
	    format: 'yyyy-mm-dd',
	})

	$('.clockpicker').clockpicker({
		donetext: 'Done'
	});

	function getKalendarView(bulan,tahun){

		$.ajax({
	        url : "<?php echo site_url('/web/pesanan/ajaxkalendarviewundone');?>",
	        type: 'post',
	        dataType: 'json',
	        data: {bulan:bulan, tahun:tahun},
	        success:function(respon){
	        	// console.log(respon);

				var data_event = [];
            	
                $.each(respon, function(k, v) {

                	var tglmasuk = v.tgl_masuk;
                	var tglpasangkusen = v.tanggal_pasang_kusen;
                	var tgljatuhtempo = v.tgl_jatuh_tempo;
                	var tglpasangfinish = v.tanggal_pasang_finish;

                	if (tglmasuk != null) {
	                	var tglmasuk = tglmasuk.split('-');
	                	if ( tglmasuk[0] == tahun && tglmasuk[1] == bulan) {

		                	var arr_data = {
								        id: v.id, // Event's ID (required)
								        name: 'Order Masuk <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.tgl_masuk, // Event date (required)
								        type: "selesai", // Event type (required)
								    }

							data_event.push(arr_data);
	                	}
                	}


                	if (tglpasangkusen != null) {
	                	var tglpasangkusen 	= tglpasangkusen.split('-');
	                	if (tglpasangkusen[0] == tahun && tglpasangkusen[1] == bulan) {
		                	var arr_data2 = {
								        id: v.id, // Event's ID (required)
								        name: 'Cek Ruangan / Pasang Kusen <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.tanggal_pasang_kusen, // Event date (required)
								        type: "holiday", // Event type (required)
								    }

							data_event.push(arr_data2);
	                	}
	                }

                	if (tgljatuhtempo != null) {
	                	var tgljatuhtempo = tgljatuhtempo.split('-');
	                	if (tgljatuhtempo[0] == tahun && tgljatuhtempo[1] == bulan) {
		                	var arr_data3 = {
								        id: v.id, // Event's ID (required)
								        name: 'Jatuh Tempo <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.tgl_jatuh_tempo, // Event date (required)
								        type: "event", // Event type (required)
								    }

							data_event.push(arr_data3);
	                	}
	                }

                	if (tglpasangfinish != null) {
	                	var tglpasangfinish = tglpasangfinish.split('-');
	                	if (tglpasangfinish[0] == tahun && tglpasangfinish[1] == bulan) {
		                	var arr_data4 = {
								        id: v.id, // Event's ID (required)
								        name: 'Pemasangan Unit <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.tanggal_pasang_finish, // Event date (required)
								        type: "pemasangan", // Event type (required)
								    }

							data_event.push(arr_data4);

	                	}
                	}
	        		
                });

				var indic = $('.calendar-table').find('.event-indicator').length;
				
				if (indic == 0) {
					$("#calendar").evoCalendar("addCalendarEvent", data_event);
				}

	        } 
	    });
		
	}

	function tambahNol(x){
	   y=(x>9)?x:'0'+x;
	   return y;
	}
</script>