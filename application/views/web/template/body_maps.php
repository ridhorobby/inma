<?php echo $header ?>
<?php echo $body ?>
<script>
		// membuat bayangan dari marker
		var LeafIcon = L.Icon.extend({
			options: {
				shadowUrl: '<?=base_url()?>assets/web/img/leaflet/marker-shadow.png',
				iconSize:     [25, 40],
				shadowSize:   [50, 64],
				iconAnchor:   [15, 47],
				shadowAnchor: [18, 68],
				popupAnchor:  [-3, -76]
			}
		});

		// membuat layergroup per dinas
		<?php foreach ($dinas as $din): ?>
			var dinas_<?php echo $din['idunit']?>_public = new L.LayerGroup();
			var dinas_<?php echo $din['idunit']?>_private = new L.LayerGroup();
		<?php endforeach ?>

		//mark yang digunakan
		var	pb_L = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/lapor_public.png'}),
			pb_R = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/respon_public.png'}),
			pb_O = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/open_public.png'}),
			pb_P = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/progress_public.png'}),
			pb_S = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/done_public.png'});
			pb_I = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/archive_public.png'});
			pb_A = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/alih_public.png'});


		
		///////////////////PETA 1///////////////////////////////////////

		<?php foreach($listPost_public as $key){ ?>

			L.marker([<?=$key['latitude']?>, <?=$key['longitude']?>], {icon: pb_<?= $key['status'] ?>}).bindPopup('<h5 style="text-align:center;"><b> <?php echo $key['nama_dinas'] ?></b></h5><h5 style="text-align:center;"><?php echo $key['description'] ?><h5><a href="<?= site_url('web/post/detail/'.$key['id']) ?>"><center><img width=100px; src=<?=base_url()?>assets/uploads/<?php echo $key['nama_file'] ?>><br><?=$key['alamat']?><center></a>').addTo(dinas_<?php echo $key['dinas_id']?>_public);
			
		<?php } ?>	

		var mbAttr = '',
			mbUrl = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';

		// console.log('http://{s}.tile.osm.org/{z}/{x}/{y}.png');

		var grayscale   = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr}),
			streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr});

		var map = L.map('map', {
			center: [-1.3921069, 120.7535967],
			zoom: 13,
			layers: [grayscale, <?php foreach ($dinas as $din): ?>
					dinas_<?= $din['idunit'] ?>_public,
			<?php endforeach ?>]
		});


		var baseLayers = {
		};
		var overlays = {
			<?php foreach ($dinas as $din): ?>
				'<?php echo $din['name'] ?>' : dinas_<?= $din['idunit'] ?>_public,
			<?php endforeach ?>
		};

		L.control.layers(baseLayers, overlays).addTo(map);
		///////////////////PETA 1///////////////////////////////////////










		///////////////////////////////////PETA 2///////////////////////////////////////////


		// var bangunan_swd2 = new L.LayerGroup();
		// var patok_swd2=new L.LayerGroup();

		//mark yang digunakan
		var	pr_L = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/lapor_private.png'}),
			pr_R = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/respon_private.png'}),
			pr_O = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/open_private.png'}),
			pr_P = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/progress_private.png'}),
			pr_S = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/done_private.png'});
			pr_I = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/archive_private.png'});
			pr_A = new LeafIcon({iconUrl: '<?=base_url()?>assets/web/img/leaflet/alih_private.png'});
		

		<?php foreach($listPost_private as $key){ ?>

			L.marker([<?=$key['latitude']?>, <?=$key['longitude']?>], {icon: pr_<?= $key['status'] ?>}).bindPopup('<h5 style="text-align:center;"><b> <?php echo $key['nama_dinas'] ?></b></h5><h5 style="text-align:center;"><?php echo $key['description'] ?><h5><a href="<?= site_url('web/post/detail/'.$key['id']) ?>"><center><img width=100px; src=<?=base_url()?>assets/uploads/<?php echo $key['nama_file'] ?>><br><?=$key['alamat']?><center></a>').addTo(dinas_<?php echo $key['dinas_id']?>_private);
			
		<?php } ?>		

		


		var mbAttr = '',
			mbUrl = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';

		var grayscale   = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr}),
			streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr});

		var map2 = L.map('map2', {
			center: [-1.3921069, 120.7535967],
			zoom: 12,
			layers: [grayscale, <?php foreach ($dinas as $din): ?>
					dinas_<?= $din['idunit'] ?>_private,
			<?php endforeach ?>]
		});

		var baseLayers = {
			
		};

		var overlays = {
			<?php foreach ($dinas as $din): ?>
				'<?php echo $din['name'] ?>' : dinas_<?= $din['idunit'] ?>_private,
			<?php endforeach ?>
		};

		L.control.layers(baseLayers, overlays).addTo(map2);

		///////////////////////////////////PETA 2///////////////////////////////////////////


















		



		$(document).ready(function(){
		    $("#swd2").click(function(){
		        $("#map2").show();
		    });
		    $("#swd1").click(function(){
		        $("#map2").hide();
		    });

		    $("#map2").hide();
		});
		

	</script>