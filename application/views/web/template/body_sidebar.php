<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-3">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <!-- <img src="<?php echo base_url('assets/web/img/logo.png')?>" alt="AdminLTE Logo" class="brand-image elevation-3" style="opacity: .8"> -->
      <label class="logo-title">Nasa<b>tech</b></label><br>
      <span class="logo-sub-title">Kitchenset</span>
    </a>

    <a href="<?php echo site_url('web/pesanan/getkpibulanan')?>" class="kpi-bulanan">
      <span><i class="fas fa-chart-bar"></i></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <?php 
      $uri = $this->uri->segment(2);
      $uri2 = $this->uri->segment(3);
      $uri3 = $this->uri->segment(4);
      ?>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-legacy" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <!-- <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Jadwal
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./index.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v1</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v2</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v3</p>
                </a>
              </li>
            </ul>
          </li> -->

          <!-- <li class="nav-header">EXAMPLES</li> -->
          <li class="nav-item">
            <a href="<?= site_url('web/pesanan')?>" class="nav-link <?php echo ($uri == 'pesanan' && $uri2 == 'index') ? 'active' : ''; ?>">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Jadwal
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('web/pesanan/daftarorder')?>" class="nav-link <?php echo ($uri == 'pesanan' && $uri2 == 'daftarOrder') ? 'active' : ''; ?>">
              <i class="far fa-clipboard nav-icon"></i>
              <p>
                Daftar Order
              </p>
            </a>
          </li>
          <?php if (SessionManagerWeb::getRole() != Role::PEMASANG): ?>

          <?php if (SessionManagerWeb::getRole() != Role::ADMINISTRATOR): ?>
          <li class="nav-item">
            <a href="<?= site_url('web/komplain')?>" class="nav-link <?php echo ($uri == 'komplain') ? 'active' : ''; ?>">
              <i class="fas fa-comments nav-icon"></i>
              <p>
                Daftar Komplain
              </p>
            </a>
          </li>
          <?php endif ?>
          
          <li class="nav-item">
            <a href="<?= site_url('web/pesanan/history_konsumen')?>" class="nav-link <?php echo ($uri == 'pesanan' && $uri2 == 'history_konsumen') ? 'active' : ''; ?>">
              <i class="fas fa-history nav-icon"></i>
              <p>History Konsumen</p>
            </a>
          </li>
          <?php if (SessionManagerWeb::getRole() == Role::ADMINISTRATOR or SessionManagerWeb::getRole() == Role::DIREKTUR): ?>
          <li class="nav-item">
            <a href="<?= site_url('web/harga')?>" class="nav-link <?php echo ($uri == 'harga') ? 'active' : ''; ?>">
              <i class="fas fa-dollar-sign nav-icon"></i>
              <p>Daftar Harga</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('web/harga_maintenance')?>" class="nav-link <?php echo ($uri == 'harga_maintenance') ? 'active' : ''; ?>">
              <i class="fas fa-dollar-sign nav-icon"></i>
              <p>Daftar Harga Maintenance</p>
            </a>
          </li>
          <?php endif ?>
          <li class="nav-item">
            <a href="<?= site_url('web/pengajuan_harga')?>" class="nav-link <?php echo ($uri == 'pengajuan_harga' && $uri2 == '') ? 'active' : ''; ?>">
              <i class="fas fa-list-alt nav-icon"></i>
              <p>Daftar Pengajuan Harga</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('web/pengajuan_harga_komplain')?>" class="nav-link <?php echo ($uri == 'pengajuan_harga_komplain' && $uri2 == '') ? 'active' : ''; ?>">
              <i class="fas fa-list-alt nav-icon"></i>
              <p>Daftar Pengajuan Harga</p>
            </a>
          </li>
          <?php if (SessionManagerWeb::getRole() != Role::PRODUKSI): ?>
          <li class="nav-item">
            <a href="<?= site_url('web/pembayaran_log')?>" class="nav-link  <?php echo ($uri == 'pembayaran_log' && ($uri2=='' || $uri2=='index')) ? 'active' : ''; ?>">
              <i class="fas fa-book nav-icon"></i>
              <p>Kas</p>
            </a>
          </li>
          <?php endif ?>
          <?php if (SessionManagerWeb::getRole() == Role::ADMINISTRATOR OR SessionManagerWeb::getRole() == Role::PRODUKSI OR SessionManagerWeb::getRole() == Role::DIREKTUR): ?>
          <li class="nav-item">
            <a href="<?= site_url('web/pembayaran_log/gethutangproduksiperbulan')?>" class="nav-link <?php echo ($uri == 'pembayaran_log' && $uri2=='gethutangproduksiperbulan') ? 'active' : ''; ?>">
              <i class="fas fa-dollar-sign nav-icon"></i>
              <p>Hutang Produksi</p>
            </a>
          </li>
          <?php endif ?>
          <?php if (SessionManagerWeb::getRole() != Role::PRODUKSI): ?>
          <li class="nav-item">
            <a href="<?= site_url('web/pembayaran_log/getListStatusPiutangKonsumen')?>" class="nav-link <?php echo ($uri == 'pembayaran_log' && $uri2=='getListStatusPiutangKonsumen') ? 'active' : ''; ?>">
              <i class="fas fa-dollar-sign nav-icon"></i>
              <p>Piutang Konsumen</p>
            </a>
          </li>
          <?php endif ?>
          <?php if (SessionManagerWeb::getRole() == Role::ADMINISTRATOR or SessionManagerWeb::getRole() == Role::DIREKTUR): ?>
          <li class="nav-item">
            <a href="<?= site_url('web/pengajuan_harga/getLabaRugiBulanan')?>" class="nav-link <?php echo ($uri == 'pengajuan_harga' && $uri2=='getLabaRugiBulanan') ? 'active' : ''; ?>">
              <i class="fas fa-wallet nav-icon"></i>
              <p>Laba Rugi</p>
            </a>
          </li>
          <?php endif ?>
          <?php if (SessionManagerWeb::getRole() == Role::ADMINISTRATOR or SessionManagerWeb::getRole() == Role::DIREKTUR): ?>
          <li class="nav-item">
            <a href="<?= site_url('web/user')?>" class="nav-link  <?php echo ($uri == 'user') ? 'active' : ''; ?>">
              <i class="fas fa-user nav-icon"></i>
              <p>Daftar User</p>
            </a>
          </li>
          <?php endif ?>
          <?php endif ?>
          
          
          <li class="nav-item">
            <a href="<?= site_url('web/notification/me')?>" class="nav-link <?php echo ($uri == 'notification') ? 'active' : ''; ?>" >
              <i class="fas fa-bell nav-icon"></i>
              <p>Notifikasi 
                <?php if ($jmlnotif!=0): ?>
                <span style="    background-color: #f71212;
    color: white;
    font-size: 13px;
    padding: 6px;
    border-radius: 100%;"><?= $jmlnotif ?></span>
              <?php endif ?></p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>