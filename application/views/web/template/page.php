<!DOCTYPE html>
<html lang="id">

<head>
    <?php echo $head ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    <?php echo $header; ?>

    <?php echo $sidebar; ?>

    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <?php echo $body ?>
            </div>
        </section>

    </div>

    <div class="clearfix"></div>

    <?php echo $footer; ?>

</div>
    <!-- <footer>
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-4 footer-text">
                <strong><b>Kementerian Riset, Teknologi, dan Pendidikan Tinggi</b></strong>
                <p>Pusat Data dan Informasi, Ilmu Pengetahuan, Teknologi, dan Pendidikan Tinggi. Gedung D Lantai 2, Jalan Jendral Sudirman Pintu 1 Senayan Jakarta Pusat 10270</p>

            </div>
            <div class="col-md-5 col-sm-3" align="center">
                <img class="footer-img" src="<?php echo base_url('assets/web/img/Logo_Kemenristekdikti.png') ?>" style="margin-right:30px;"/>
                <img class="footer-img" src="<?php echo base_url('assets/web/img/pddikti.png') ?>"/>
            </div>
            <div class="col-md-2 col-sm-3" align="center">
                <h4><b>Follow Us</b></h4>
                <ul class="footer-sosmed">
                    <li><img class="footer-img" src="<?php echo base_url('assets/web/img/facebook-logo-in-circular-button-outlined-social-symbol.svg') ?>" style="height:35px;" /></li>
                    <li><img class="footer-img" src="<?php echo base_url('assets/web/img/twitter-circular-button.svg') ?>"  style="height:35px;" /></li>
                </ul>
            </div>

        </div>
    </div>
    </footer> -->
</body>


</html>