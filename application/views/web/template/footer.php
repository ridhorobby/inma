<!-- Main Footer -->
<footer class="main-footer">
  <!-- <strong>Copyright &copy; 2021 <a href="#">Nasatech INMA</a>.</strong> -->
  <div class="float-right d-none d-sm-inline-block">
    <b>Version</b> 1.1.72
  </div>
</footer>

<!-- overlayScrollbars -->
<script src="<?php echo base_url()?>assets/web/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>assets/web/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<!-- <script src="plugins/jquery-mousewheel/jquery.mousewheel.js"></script> -->
<!-- <script src="plugins/raphael/raphael.min.js"></script> -->
<!-- <script src="plugins/jquery-mapael/jquery.mapael.min.js"></script> -->
<!-- <script src="plugins/jquery-mapael/maps/usa_states.min.js"></script> -->
<!-- ChartJS -->
<!-- <script src="plugins/chart.js/Chart.min.js"></script> -->

<!-- AdminLTE for demo purposes -->
<!-- <script src="dist/js/demo.js"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="dist/js/pages/dashboard2.js"></script> -->

