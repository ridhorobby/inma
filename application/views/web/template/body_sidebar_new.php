<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-3">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <!-- <img src="<?php echo base_url('assets/web/img/logo.png')?>" alt="AdminLTE Logo" class="brand-image elevation-3" style="opacity: .8"> -->
      <label class="logo-title">Nasa<b>tech</b></label><br>
      <span class="logo-sub-title">Kitchenset</span>
    </a>

    <a href="<?php echo site_url('web/pesanan/getkpibulanan')?>" class="kpi-bulanan">
      <span><i class="fas fa-chart-bar"></i></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <?php 
      $uri = $this->uri->segment(2);
      $uri2 = $this->uri->segment(3);
      $uri3 = $this->uri->segment(4);
      ?>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-legacy" data-widget="treeview" role="menu" data-accordion="false">
          <?php $menu = Role::list_menu(SessionManagerWeb::getRole()); ?>
          <?php foreach ($menu as $key => $value): ?>
            <?php $exp_link = explode('/', $value['link']) ?>
          <li class="nav-item">
            <a href="<?= site_url('web'.$value['link'])?>" class="nav-link <?php echo ($uri == $exp_link[1] && $uri2 == $exp_link[2]) ? 'active' : ''; ?>">
              <i class="<?= $value['icon'] ?>"></i>
              <p>
                <?= $value['name']  ?>
              </p>
            </a>
          </li>
          <?php endforeach ?>
          
          
          
          
          <li class="nav-item">
            <a href="<?= site_url('web/notification/me')?>" class="nav-link <?php echo ($uri == 'notification') ? 'active' : ''; ?>" >
              <i class="fas fa-bell nav-icon"></i>
              <p>Notifikasi 
                <?php if ($jmlnotif!=0): ?>
                <span style="    background-color: #f71212;
    color: white;
    font-size: 13px;
    padding: 6px;
    border-radius: 100%;"><?= $jmlnotif ?></span>
              <?php endif ?></p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>