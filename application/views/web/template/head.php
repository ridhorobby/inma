<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?= $title ?></title>
<!-- Favicons -->
<link href="<?php echo base_url()?>assets/web/img/favicon.png" rel="icon">
<!-- Google Font: Source Sans Pro -->
<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> -->
<!-- Font Awesome Icons -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/fontawesome-free/css/all.min.css">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/web/css/adminlte.css">

<!-- jQuery -->
<script src="<?php echo base_url()?>assets/web/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url()?>assets/web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/web/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>


<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/clockpicker/dist/bootstrap-clockpicker.css">
<script src="<?php echo base_url()?>assets/web/plugins/clockpicker/dist/bootstrap-clockpicker.js"></script>
