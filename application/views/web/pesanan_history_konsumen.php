<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>History Konsumen</h1>
    </div>
</div>

<style>
  .page-item.active .page-link{
    background-color: #f71212!important;
    border-color: #f71212!important;
  }
  .page-item .page-link a{
    color: #757575!important;
  }
</style>
<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button>
        </span>
        <span style="float: right;">
        </span>
    </div>
</div>    

<div class="row">
    <!-- <div class="col-md-12">
    	<div class="kalender-kas text-center">
    		<button type="button" id="prev" class="btn btn-sm btn-light float-left"><i class="fas fa-angle-left"></i></button>
    		<label id="tanggal_history"></label>
    		<input type="hidden" id="month" value="<?= date('m')?>">
    		<input type="hidden" id="year" value="<?= date('Y')?>">
    		<button type="button" id="next" class="btn btn-sm btn-light float-right"><i class="fas fa-angle-right"></i></button>
    	</div>
    </div> -->
    <div class="col-md-12">
       	<table class="table table-sm table-kas">
          <tbody id="tbody_order">
            
          </tbody>
        </table>
    </div>

    <div class="col-md-12">
      <div id="ajax-pagination" style="float:right;"></div>
    </div>
</div>

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pencarian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url("web/pesanan/history_konsumen") ?>" method="post">
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-12 col-sm-12 col-md-12">
            <input type="text" name="kode_order" id="kode_order" class="form-control" placeholder="Kode Order" value="<?= (!empty($kode_order)) ? $kode_order : '';?>">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12 col-sm-12 col-md-12">
            <input type="text" name="nama_konsumen" id="nama_konsumen" class="form-control" placeholder="Nama" value="<?= (!empty($nama_konsumen)) ? $nama_konsumen : '';?>">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12 col-sm-12 col-md-12">
            <input type="text" name="alamat_konsumen" id="alamat_konsumen" class="form-control" placeholder="Alamat" value="<?= (!empty($alamat_konsumen)) ? $alamat_konsumen : '';?>">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12 col-sm-12 col-md-12">
            <input type="text" name="no_hp_konsumen" id="no_hp_konsumen" class="form-control" placeholder="No HP" value="<?= (!empty($no_hp_konsumen)) ? $no_hp_konsumen : '';?>">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12 col-sm-6 col-md-6">
            <select name="bulan" class="form-control" id="filter_bulan">
              <option value="">- Bulan -</option>
              <option value="01" <?= ($bulan == '01') ? 'selected' : ''; ?> >Januari</option>
              <option value="02" <?= ($bulan == '02') ? 'selected' : ''; ?> >Februari</option>
              <option value="03" <?= ($bulan == '03') ? 'selected' : ''; ?> >Maret</option>
              <option value="04" <?= ($bulan == '04') ? 'selected' : ''; ?> >April</option>
              <option value="05" <?= ($bulan == '05') ? 'selected' : ''; ?> >Mei</option>
              <option value="06" <?= ($bulan == '06') ? 'selected' : ''; ?> >Juni</option>
              <option value="07" <?= ($bulan == '07') ? 'selected' : ''; ?> >Juli</option>
              <option value="08" <?= ($bulan == '08') ? 'selected' : ''; ?> >Agustus</option>
              <option value="09" <?= ($bulan == '09') ? 'selected' : ''; ?> >September</option>
              <option value="10" <?= ($bulan == '10') ? 'selected' : ''; ?> >Oktober</option>
              <option value="11" <?= ($bulan == '11') ? 'selected' : ''; ?> >November</option>
              <option value="12" <?= ($bulan == '12') ? 'selected' : ''; ?> >Desember</option>
            </select>
          </div>

          <div class="col-12 col-sm-6 col-md-6">
            <select name="tahun" class="form-control" id="filter_tahun">
              <option value="">- Tahun -</option>
              <?php for($i = date('Y'); $i >= date('Y')-10 ; $i--) { ?>
                  <option value="<?= $i ?>" <?= ($tahun == $i) ? 'selected' : ''; ?> ><?= $i ?></option>
              <?php } ?>
            </select>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-sm btn-secondary" onclick="$('#kode_order').val('');$('#nama').val('');$('#alamat_konsumen').val('');$('#no_hp_konsumen').val('');$('#filter_bulan').val('');$('#filter_tahun').val('');" >Reset</button>
        <button type="submit" class="btn btn-sm btn-add">Terapkan</button>
      </div>
    </form>
    </div>
  </div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/web/plugins/pagination.js/pagination.css">
<script src="<?php echo base_url();?>assets/web/plugins/pagination.js/pagination.min.js"></script>

<script>

	var bulan = '<?php echo $bulan ?>';
	var tahun = '<?php echo $tahun ?>';


  // var hari = ['Minggu','Senin', 'Selasa', 'Rabu', 'Kamis','Jumat', 'Sabtu'];
  // var bulan = ['Januari','Februari','Maret','April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember' ];

	var open_tr_td_a_div = '<tr><td colspan="4">';

	var html_div_label = '<div class="col-12 col-sm-12 col-md-12"><label id="label_order"></label><span class="pl-2" id="konsumen_order">Edo</span></div>';

	var html_div_order_masuk = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-green"><h6 class="text-truncate">Order Masuk</h6><p id="tanggal_order_masuk"></p></div>';

	var html_div_cek_ruangan = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-yellow"><h6 class="text-truncate">Cek Ruangan / Pasang Kusen</h6><p id="tanggal_cek_ruangan"></p></div>';

	var html_div_pasang_unit = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-purple"><h6>Pemasangan Unit</h6><p id="tanggal_pasang_unit">-</p></div>';

	var html_div_set_selesai = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-blue"><h6>Menunggu Set Selesai</h6><p id="tanggal_set_selesai">-</p></div>';


	var icon_check = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content"><div class="history-icon"><div class="history-bullet-green"><i class="fas fa-check"></i></div></div></div>';

	var icon_pause = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content"><div class="history-icon"><div class="history-bullet-red"><i class="fas fa-pause"></i></div></div></div>';

	var icon_process = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content"><div class="history-icon"><div class="history-bullet-yellow"><i class="fas fa-spinner"></i></div></div></div>';

	var icon_grey = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content"><div class="history-icon"><div class="history-bullet-default"></div></div></div>';

	var close_tr_td_a_div = '</div></a></td></tr>';

  var kode_order = $('#kode_order').val();
  var nama_konsumen = $('#nama_konsumen').val();
  var alamat_konsumen = $('#alamat_konsumen').val();
  var no_hp_konsumen = $('#no_hp_konsumen').val();

  var $page=0;

   // Detect pagination click
  $('#ajax-pagination').on('click','a',function(e){
     e.preventDefault(); 
     var page = $(this).attr('data-ci-pagination-page');
     loadPagination(page);
  });

  loadPagination(0);

  // Load pagination
  function loadPagination(page){
    $.ajax({
       url: '<?php echo site_url('/web/pesanan/ajaxkalendarview');?>/'+page,
       type: 'post',
       dataType: 'json',
       data: {kode_order:kode_order, nama_konsumen:nama_konsumen, alamat_konsumen:alamat_konsumen, no_hp_konsumen:no_hp_konsumen, bulan:bulan, tahun:tahun, history:true},
       success: function(response){
          $('#ajax-pagination').html(response.pagination);
          // createTable(response.result,response.row);

          $('#tbody_order').html('');

          if(response.result!=false){
            $.each(response.result, function(k, v) {
              if(v.tipe=='P'){
                direct = 'pesanan';
                // console.log(pesanan);
              }else{
                direct = 'komplain';
              }
              if(v.flow_id==8 && v.tipe=='P'){
                var html_div_label = '<a href="<?= site_url() ?>web/'+direct+'/detail/'+v.id+'"  class="text-reset"><div class="row history-konsumen-row"><div class="col-6 col-sm-6 col-md-6"><label id="label_order_'+v.id+'"></label><span class="pl-2" id="konsumen_order_'+v.id+'"></span></div><div class="col-6 col-sm-6 col-md-6"><p style="text-align: right">'; 

                if(v.link_sosmed!=null){
                  
                  html_div_label +='<i class="fa fa-link" style="color:red;padding-right:10px;"></i>';
                }
                if(v.foto_after!='0'){
                  html_div_label +='<i class="fa fa-camera" style="color:red;padding-right:10px;"></i>';
                }
                if(v.edt_katalog!='0'){
                  html_div_label += '<i class="fas fa-image" style="color:red"></i>';
                }
                
                html_div_label += '</p></div>';
              }else{
                var html_div_label = '<a href="<?= site_url() ?>web/'+direct+'/detail/'+v.id+'"  class="text-reset"><div class="row history-konsumen-row"><div class="col-12 col-sm-12 col-md-12"><label id="label_order_'+v.id+'"></label><span class="pl-2" id="konsumen_order_'+v.id+'"></span></div>';
              }
              

              var html_div_order_masuk = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-green"><h6 class="text-truncate">Order Masuk</h6><p id="tanggal_order_masuk_'+v.id+'"></p></div>';

              var html_div_cek_ruangan = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-yellow"><h6 class="text-truncate">Cek Ruangan / Pasang Kusen</h6><p id="tanggal_cek_ruangan_'+v.id+'"></p></div>';

              var html_div_pasang_unit = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-purple"><h6>Pemasangan Unit</h6><p id="tanggal_pasang_unit_'+v.id+'">-</p></div>';
              
              var html_div_set_selesai = '<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-blue"><h6>Menunggu Set Selesai</h6><p id="tanggal_set_selesai_'+v.id+'">-</p></div>';

              var append_html = open_tr_td_a_div+html_div_label+html_div_order_masuk+html_div_cek_ruangan+html_div_pasang_unit+html_div_set_selesai;

              if(v.flow_id == 1){
                var icon_flow = icon_check+icon_grey+icon_grey+icon_grey;
              }
              else if(v.flow_id==2){
                var icon_flow = icon_check+icon_pause+icon_grey+icon_grey;
              }
              else if(v.flow_id==3 || v.flow_id==4){
                var icon_flow = icon_check+icon_process+icon_grey+icon_grey;
              }
              else if(v.flow_id==5){
                var icon_flow = icon_check+icon_check+icon_pause+icon_grey;
              }
              else if(v.flow_id==6){
                var icon_flow = icon_check+icon_check+icon_process+icon_grey;
              }
              else if(v.flow_id==7){
                var icon_flow = icon_check+icon_check+icon_check+icon_process;
              }
              else if(v.flow_id==8){
                var icon_flow = icon_check+icon_check+icon_check+icon_check;
              }
              $('#tbody_order').append(append_html+icon_flow);
              //$('#tbody_order').append(append_html);

              $('#label_order_'+v.id).html(v.kode_order);
              $('#konsumen_order_'+v.id).html(v.nama_konsumen);
              $('#tanggal_order_masuk_'+v.id).html(tanggalBulanTahunInd(v.tgl_masuk));
              $('#tanggal_cek_ruangan_'+v.id).html(tanggalBulanTahunInd(v.tanggal_pasang_kusen));
              $('#tanggal_pasang_unit_'+v.id).html(tanggalBulanTahunInd(v.tanggal_pasang_finish));
              $('#tanggal_set_selesai_'+v.id).html(tanggalBulanTahunInd(v.tgl_acc_konsumen));
          
            });
          }else{
            var open = '<tr><td colspan="4"><a href="#" class="text-reset"><div class="row history-konsumen-row" style="text-align:center">';
            var html_div_label = '<div class="col-12 col-sm-12 col-md-12"><label id="label_order">Tidak ada data</label><span class="pl-2" id="konsumen_order"></span></div>';
            $('#tbody_order').append(open+html_div_label);
          }
       }
    });
  }

 

  var bulantahun = bulanTahunInd(tahun+'-'+bulan);
  $('#tanggal_history').html(bulantahun);

	$(document).ready(function(){
    
    $('#prev').click(function(){
      if($('#month').val() == '01' ){
        var bulan = 12;
        var tahun = parseInt($('#year').val()) - 1;
        $('#year').val(tahun);
      }else if (parseInt($('#month').val()) <= 12) {
        var bulan = parseInt($('#month').val()) - 1;
        var tahun = $('#year').val();
      }

      bulan = tambahNol(bulan);
      $('#month').val(bulan);
      bulantahun = bulanTahunInd(tahun+'-'+bulan);

      $('#tanggal_history').html(bulantahun);
			getKalendarView(bulan,tahun)
		})

		$('#next').click(function(){
      if($('#month').val() == '12' ){
        var bulan = 1;
        var tahun = parseInt($('#year').val()) + 1;
        $('#year').val(tahun);
      }else if(parseInt($('#month').val()) >= 1){
  			var bulan = parseInt($('#month').val()) + 1;
	   		var tahun = $('#year').val();
      }
			
      bulan = tambahNol(bulan);

      $('#month').val(bulan);
			bulantahun = bulanTahunInd(tahun+'-'+bulan);
      $('#tanggal_history').html(bulantahun);
			getKalendarView(bulan,tahun)
		})
	})

  function tambahNol(x){
     y=(x>9)?x:'0'+x;
     return y;
  }

  function bulanTahunInd(x){
    var dt = new Date(x);
    var monthNames = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    
    var y = monthNames[dt.getMonth()]+' '+dt.getFullYear();

    return y;
  }

  function tanggalBulanTahunInd(x){
    if(x==null || x=='' || x=='0000-00-00'){
      return '-';
    }else{
      var dt = new Date(x);
      var monthNames = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
      
      var y = dt.getDate()+' '+monthNames[dt.getMonth()]+' '+dt.getFullYear();

      return y;

    }
    
    
  }
</script>