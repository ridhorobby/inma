
<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Daftar Order</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-filter"></i> Filter</button>
        </span>
        <span style="float: right;">
         
        </span>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
       	<ul class="nav nav-tabs" role="tablist">
		  <li class="nav-item">
		    <a class="nav-link active" href="#order" role="tab" data-toggle="tab">Order</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="#komplain" role="tab" data-toggle="tab">Komplain</a>
		  </li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		  <div role="tabpanel" class="tab-pane fade in active show" id="order">
		  	<?php foreach ($need_action as $key): ?>
        <?php $color = ($key['rencana_kerja_id']==1) ? '#ffe0b2' : '#e1bee7' ?>
        <div class="row mt-4 row-order-menunggu" onclick='setselesaiproses(<?= $key['id']  ?>,<?= json_encode($key)  ?>)' style="cursor:pointer;color:#212529; background-color:<?= $color ?>;">
            <div class="col-4 col-sm-4 col-md-2 list-order-menunggu">
              <div>Kode Order</div>
              <div>Nama</div>
              <div>Jadwal</div>
              <div>Rencana Kerja</div>
            </div>
            <div class="col-8 col-sm-8 col-md-10">
              <div><?= $key['kode_order']  ?></div>
              <div><?= $key['nama_konsumen']  ?></div>
              
              <?php 
                if($key['rencana_kerja_id']== 1){
                    $nama_proses = 'Pasang Kusen';
                    $tanggal = $key['tanggal_pasang_kusen'];
                  
                  }else{
                    $nama_proses = 'Pemasangan Unit & Selesai Pasang Unit';
                    $tanggal = $key['tanggal_pasang_finish'];
                  }

               ?>
               <div><?= xFormatDateInd($tanggal) ?></div>
              <div style="color:red"><?= $nama_proses  ?></div>
            </div>
            <?php if (($key['flow_id']==3 && $key['jadwal_usulan']!='') || ($key['flow_id']==4 && $key['tanggal_pasang_kusen']=='') || ($key['flow_id']==6 && $key['jadwal_usulan']!='') || ($key['flow_id']==7 && $key['tanggal_pasang_finish']=='')): ?>
            <div class="col-12 col-sm-12 col-md-12" style="color:red">
                Menunggu Approval Desainer/Marketing
            </div>
            <?php endif ?>
            
        </div>  
        <?php endforeach; ?>  
		  </div>

		  <div role="tabpanel" class="tab-pane fade" id="komplain">
         <?php foreach ($need_action_komplain as $key): ?>
          <?php $color = ($key['rencana_kerja_id']==1) ? '#ffe0b2' : '#e1bee7' ?>
        <div class="row mt-4 row-order-menunggu" onclick='komplainselesaiproses(<?= $key['id']  ?>,<?= json_encode($key)  ?>)'
style="cursor:pointer;text-decoration:none;color:#212529;background-color:<?= $color ?>; ">
            <div class="col-4 col-sm-4 col-md-2 list-order-menunggu">
              <div>Kode Order</div>
              <div>Nama</div>
              <div>Jadwal</div>
              <div>Rencana Kerja</div>
            </div>
            <div class="col-8 col-sm-8 col-md-10">
              <div><?= $key['kode_order']  ?></div>
              <div><?= $key['nama_konsumen']  ?></div>
              
              <?php 
                if($key['rencana_kerja_id']== 1){
                    $nama_proses = 'Pasang Kusen';
                    $tanggal = $key['tanggal_pasang_kusen'];
                  
                  }else{
                    $nama_proses = 'Pemasangan Unit & Selesai Pasang Unit';
                    $tanggal = $key['tanggal_pasang_finish'];
                  }

               ?>
               <div><?= xFormatDateInd($tanggal) ?></div>
              <div style="color:red"><?= $nama_proses  ?></div>
            </div>             
        </div>  
        <?php endforeach; ?>  
      </div>

		</div>
    </div>
</div>
<!-- /.row -->

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pencarian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post">
      <div class="modal-body">
        <input type="hidden" name="filterExist" value="1">
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="kode_order" class="form-control" placeholder="Kode Order" value="<?= ($filter['kode_order']) ? $filter['kode_order'] : null ?>">
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="nama_konsumen" class="form-control" placeholder="Nama" value="<?= ($filter['nama_konsumen']) ? $filter['nama_konsumen'] : null ?>">
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="alamat_konsumen" class="form-control" placeholder="Alamat" value="<?= ($filter['alamat_konsumen']) ? $filter['alamat_konsumen'] : null ?>">
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="no_hp_konsumen" class="form-control" placeholder="No HP" value="<?= ($filter['no_hp_konsumen']) ? $filter['no_hp_konsumen'] : null ?>">
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="<?= site_url("web/$class/$method") ?>" class="btn btn-sm btn-secondary" >Reset</a>
        <button type="submit" class="btn btn-sm btn-add">Terapkan</button>
      </div>
	  </form>
    </div>
  </div>
</div>

<!-- Detail Modal -->
<div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="kodeOrderModal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row" >

        </div>
      </div>
    </div>
  </div>
</div>

<!-- Status Modal -->
<div class="modal fade" id="statusModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="statusProduksiModal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row list-status">
          
        </div>
      </div>
    </div>
  </div>
</div>



<!-- Order Menunggu Modal -->
<div class="modal fade" id="orderMenungguModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Usulan Jadwal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-4 col-sm-4 col-md-4">Tanggal</div>
          <div class="col-8 col-sm-8 col-md-8" id="jadwal_pengajuan"></div>

          <div class="col-4 col-sm-4 col-md-4">Rencana Kerja</div>
          <div class="col-8 col-sm-8 col-md-8" id="rencana_kerja_nama"></div>

          <div class="col-4 col-sm-4 col-md-4">Tim</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_pemasang"></div>

          <div class="col-12 col-sm-12 col-md-12">
            <label class="mb-0 mt-2">Data Order</label>
          </div>

          <div class="col-4 col-sm-4 col-md-4">Kode Order</div>
          <div class="col-8 col-sm-8 col-md-8" id="kode_order"></div>

          <div class="col-4 col-sm-4 col-md-4">Tgl. Order Masuk</div>
          <div class="col-8 col-sm-8 col-md-8" id="tgl_order_masuk"></div>

          <div class="col-4 col-sm-4 col-md-4">Nama</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">Alamat</div>
          <div class="col-8 col-sm-8 col-md-8" id="alamat_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">No HP</div>
          <div class="col-8 col-sm-8 col-md-8" id="no_hp_konsumen"></div>
          <input type="hidden" name="type_direct" value="P">
        </div>
      </div>
      <div class="modal-footer" style="border-top: none;">
          <a href="#" class="btn btn-sm btn-add" id="tolakusulkanpemasangan"><i class="fas fa-times"></i> Tolak</a>
          <button  onclick="terimaUsulan()" class="btn btn-sm btn-success" id="terimausulkanpemasangan"><i class="fas fa-check"></i> Setuju</button>
      </div>
    </div>
  </div>
</div>

<!-- Order Proses Modal -->
<div class="modal fade" id="orderProsesModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="orderProsesTitleModal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row list-order-proses">
          
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Order Proses Selesai Modal -->
<div class="modal fade" id="orderProsesSelesaiModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="orderProsesSelesaiTitleModal">Pekerjaan Selesai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="#" method="post" id="formProsesSelesai">
      <!-- <input type="hidden" name="">    -->
      <div class="modal-body">
        <div style="font-size: .9em;">Anda akan merubah status pekerjaan menjadi selesai pada order berikut</div>
        <div class="row list-order-proses-selesai mt-1" style="font-size: .9em;background-color: #eee;">
          <div class="col-4 col-sm-4 col-md-4">Kode Order</div>
          <div class="col-8 col-sm-8 col-md-8" id="kode_order"></div>

          <div class="col-4 col-sm-4 col-md-4">Nama</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">Jadwal</div>
          <div class="col-8 col-sm-8 col-md-8" id="jadwal"></div>

          <div class="col-4 col-sm-4 col-md-4">Rencana Kerja</div>
          <div class="col-8 col-sm-8 col-md-8" id="rencana_kerja"></div>
        </div>
      </div>
      <div class="modal-footer" style="border-top: none;">
          <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</a>
          <button type="submit" class="btn btn-sm btn-add"><i class="fas fa-check"></i> Selesai</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Order Proses Selesai Modal -->
<div class="modal fade" id="orderProsesSelesaiModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="orderProsesSelesaiTitleModal">Pekerjaan Selesai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="#" method="post" id="formProsesSelesai">
      <input type="hidden" name="konfirmasi_selesai" id="konfirmasi_selesai">
      <div class="modal-body">
        <div style="font-size: .9em;">Anda akan merubah status pekerjaan menjadi selesai pada order berikut</div>
        <div class="row list-order-proses-selesai mt-1" style="font-size: .9em;background-color: #eee;">
          <div class="col-4 col-sm-4 col-md-4">Kode Order</div>
          <div class="col-8 col-sm-8 col-md-8" id="kode_order"></div>

          <div class="col-4 col-sm-4 col-md-4">Nama</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">Jadwal</div>
          <div class="col-8 col-sm-8 col-md-8" id="jadwal"></div>

          <div class="col-4 col-sm-4 col-md-4">Rencana Kerja</div>
          <div class="col-8 col-sm-8 col-md-8" id="rencana_kerja"></div>
        </div>
      </div>
      </form>
      <div class="modal-footer" style="border-top: none;">
          <button class="btn btn-sm btn-add" id="langsungselesai">Langsung Selesai</button>
          <button class="btn btn-sm btn-add" id="belumselesai">Jadwalkan Lagi</button>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="komplainProsesSelesaiModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="komplainProsesSelesaiTitleModal">Pekerjaan Selesai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="#" method="post" id="formKomplainProsesSelesai">
      <input type="hidden" name="konfirmasi_selesai" id="konfirmasi_selesai">
      <div class="modal-body">
        <div style="font-size: .9em;">Anda akan merubah status pekerjaan menjadi selesai pada order berikut</div>
        <div class="row list-order-proses-selesai mt-1" style="font-size: .9em;background-color: #eee;">
          <div class="col-4 col-sm-4 col-md-4">Kode Order</div>
          <div class="col-8 col-sm-8 col-md-8" id="kode_order"></div>

          <div class="col-4 col-sm-4 col-md-4">Nama</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">Jadwal</div>
          <div class="col-8 col-sm-8 col-md-8" id="jadwal"></div>

          <div class="col-4 col-sm-4 col-md-4">Rencana Kerja</div>
          <div class="col-8 col-sm-8 col-md-8" id="rencana_kerja"></div>
        </div>
      </div>
      </form>
      <div class="modal-footer" style="border-top: none;">
          <button class="btn btn-sm btn-add" id="langsungselesai">Langsung Selesai</button>
          <button class="btn btn-sm btn-add" id="belumselesai">Jadwalkan Lagi</button>
      </div>
      
    </div>
  </div>
</div>

<form id="formApprove" action="#" method="post">
  <input type="hidden" name="id" id="id_pesanan" value="" >
  <input type="hidden" name="confirm" value="1">
</form>

<!-- Order Menunggu Modal -->
<div class="modal fade" id="orderMenungguModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Usulan Jadwal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-4 col-sm-4 col-md-4">Tanggal</div>
          <div class="col-8 col-sm-8 col-md-8" id="jadwal_pengajuan"></div>

          <div class="col-4 col-sm-4 col-md-4">Jam</div>
          <div class="col-8 col-sm-8 col-md-8" id="jam_pengajuan"></div>

          <div class="col-4 col-sm-4 col-md-4">Rencana Kerja</div>
          <div class="col-8 col-sm-8 col-md-8" id="rencana_kerja_nama"></div>

          <div class="col-4 col-sm-4 col-md-4">Tim</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_pemasang"></div>

          <div class="col-12 col-sm-12 col-md-12">
            <label class="mb-0 mt-2">Data Order</label>
          </div>

          <div class="col-4 col-sm-4 col-md-4">Kode Order</div>
          <div class="col-8 col-sm-8 col-md-8" id="kode_order"></div>

          <div class="col-4 col-sm-4 col-md-4">Tgl. Order Masuk</div>
          <div class="col-8 col-sm-8 col-md-8" id="tgl_order_masuk"></div>

          <div class="col-4 col-sm-4 col-md-4">Nama</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">Alamat</div>
          <div class="col-8 col-sm-8 col-md-8" id="alamat_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">No HP</div>
          <div class="col-8 col-sm-8 col-md-8" id="no_hp_konsumen"></div>
        </div>
      </div>
      <div class="modal-footer" style="border-top: none;">
          <?php if (SessionManagerWeb::getRole() != Role::PRODUKSI): ?>
            <input type="text" class="form-control" name="link" id="link" value="" readonly="">
          <button  onclick="copyLink()" class="btn btn-sm btn-secondary" id="terimausulkanpemasangan"><i class="fas fa-link"></i> Copy Link</button>
          <?php endif ?>
          
          <a href="#" class="btn btn-sm btn-add" id="tolakusulkanpemasangan"><i class="fas fa-times"></i> Tolak</a>

          <button  onclick="terimaUsulan()" class="btn btn-sm btn-success" id="terimausulkanpemasangan"><i class="fas fa-check"></i> Setuju</button>
      </div>
    </div>
  </div>
</div>

<script>
  function setselesaiproses(id,data){
    if (data.rencana_kerja_id == 1) {
      if((data.flow_id=='2' || data.flow_id=='5') && (data.jadwal_usulan==null || data.jadwal_usulan=='')){
        window.location.href = "<?php echo site_url("web/$class/usulkanpemasangan")?>/"+id;
      }else{
        if(data.role_pengusul!=null){
          $('#tolakusulkanpemasangan').attr('href','<?php echo site_url('web/pesanan/tolakusulkanpemasangan')?>/'+id);
          $('#orderMenungguModal').modal('show');
          $('#jadwal_pengajuan').html(data.jadwal_usulan);
          $('#jam_pengajuan').html(data.jam_usulan);
          $('#rencana_kerja_nama').html(data.rencana_kerja_nama);
          if(data.rencana_kerja_id== 1 && data.nama_pemasang_kusen!=''){
            var nama = data.nama_pemasang_kusen;
                      
          }
          else if(data.rencana_kerja_id > 2 && data.nama_pemasang_finish!=''){
            var nama = data.nama_pemasang_finish;
          }else{
            var nama = '-';
          }
          $('#nama_pemasang').html(nama);
          $('#kode_order').html(data.kode_order);
          $('#tgl_order_masuk').html(data.tgl_order_masuk);
          $('#nama_konsumen').html(data.nama_konsumen);
          $('#alamat_konsumen').html(data.alamat_konsumen);
          $('#no_hp_konsumen').html(data.no_hp_konsumen);
          $('#id_pesanan').val(id);
          $('#type_direct').val('P');
        }else{
          $('#orderProsesModal').modal('hide');
          $('#orderProsesSelesaiModal').modal('show');
          
          $(' #orderProsesSelesaiModal #kode_order').html(data.kode_order);
          $('#orderProsesSelesaiModal #nama_konsumen').html(data.nama_konsumen);
          $('#orderProsesSelesaiModal #jadwal').html(data.tanggal_pasang_kusen);
          $('#orderProsesSelesaiModal #rencana_kerja').html(data.rencana_kerja_nama);
          $('#orderProsesSelesaiModal #formProsesSelesai').attr('action', '<?= site_url("web/$class/usulan/")  ?>/'+id);
        }
        
      }
      
    }else{
      if((data.flow_id=='2' || data.flow_id=='5') && (data.jadwal_usulan==null || data.jadwal_usulan=='')){
        window.location.href = "<?php echo site_url("web/$class/usulkanpemasangan")?>/"+id;
      }else{
        window.location.href = "<?php echo site_url("web/$class/setselesaiproses")?>/"+id;
      }
      
    }
  }

  function komplainselesaiproses(id,data){

    if (data.rencana_kerja_id == 1) {
      if((data.flow_id=='2' || data.flow_id=='5') && (data.jadwal_usulan==null || data.jadwal_usulan=='')){
        window.location.href = "<?php echo site_url("web/komplain/usulkanpemasangan")?>/"+id;
      }
      else{
        if(data.role_pengusul!=null){
          $('#tolakusulkanpemasangan').attr('href','<?php echo site_url('web/komplain/tolakusulkanpemasangan')?>/'+id);
          $('#orderMenungguModal').modal('show');
          $('#jadwal_pengajuan').html(data.jadwal_usulan);
          $('#jam_pengajuan').html(data.jam_usulan);
          $('#rencana_kerja_nama').html(data.rencana_kerja_nama);
          if(data.rencana_kerja_id== 1 && data.nama_pemasang_kusen!=''){
            var nama = data.nama_pemasang_kusen;
                      
          }
          else if(data.rencana_kerja_id > 2 && data.nama_pemasang_finish!=''){
            var nama = data.nama_pemasang_finish;
          }else{
            var nama = '-';
          }
          $('#nama_pemasang').html(nama);
          $('#kode_order').html(data.kode_order);
          $('#tgl_order_masuk').html(data.tgl_order_masuk);
          $('#nama_konsumen').html(data.nama_konsumen);
          $('#alamat_konsumen').html(data.alamat_konsumen);
          $('#no_hp_konsumen').html(data.no_hp_konsumen);
          $('#id_pesanan').val(id);
          $('#type_direct').val('K');
        }else{
          $('#komplainProsesModal').modal('hide');
          $('#komplainProsesSelesaiModal').modal('show');
          
          $('#komplainProsesSelesaiModal #kode_order').html(data.kode_order);
          $('#komplainProsesSelesaiModal #nama_konsumen').html(data.nama_konsumen);
          $('#komplainProsesSelesaiModal #jadwal').html(data.tanggal_pasang_kusen);
          $('#komplainProsesSelesaiModal #rencana_kerja').html(data.rencana_kerja_nama);
          $('#komplainProsesSelesaiModal #formKomplainProsesSelesai').attr('action', '<?= site_url("web/komplain/usulan/")  ?>/'+id);
        }
      }
      
    }else{
      window.location.href = "<?php echo site_url("web/komplain/setselesaiproses")?>/"+id;
    }
  }
  $(document).ready(function(){
    
    $(document).on('click','#langsungselesai',function(){
      $('#komplainProsesSelesaiModal #konfirmasi_selesai').val(1);
      $('#komplainProsesSelesaiModal #formKomplainProsesSelesai').submit();
     
    });

    $(document).on('click','#belumselesai',function(){
      $('#komplainProsesSelesaiModal #konfirmasi_selesai').val(0);
      $('#komplainProsesSelesaiModal #formKomplainProsesSelesai').submit();
     
    });

    $(document).on('click','.detail-order',function(){
      var id = $(this).attr('id');
      window.location.href = "<?= site_url('web/pesanan/detail')?>/"+id;
    })

    $(document).on('click','.update-status',function(){
      var kode = $(this).attr('kode');
      var status = $(this).attr('status');
      $('#statusProduksiModal').html('Status Produksi - '+kode);
      $('.list-status').html('');
      if (status == 1) {            
      $('.list-status').append('<div class="col-12 col-md-12 col-md-12">'+
                                  '<h6 style="color: #aaa;">Bisa Cek Ruangan</h6>'+
                                  '<span class="text-red" style="font-size: 12px;">Status saat ini</span>'+
                              '</div>');
      }else{
      $('.list-status').append('<div class="col-12 col-md-12 col-md-12" onclick="changestatusproduksi(1)" style="cursor: pointer;">'+
                                  '<h6>Bisa Cek Ruangan</h6>'+
                              '</div>');
      }

      if (status == 2) {
      $('.list-status').append('<div class="col-12 col-md-12 col-md-12">'+
                                  '<h6 style="color: #aaa;">Bisa Pasang Rangka</h6>'+
                                  '<span class="text-red" style="font-size: 12px;">Status saat ini</span>'+
                              '</div>');
      }else{
      $('.list-status').append('<div class="col-12 col-md-12 col-md-12" onclick="changestatusproduksi(2)" style="cursor: pointer;">'+
                                  '<h6>Bisa Pasang Rangka</h6>'+
                              '</div>');
      }

      if (status == 3) {            
      $('.list-status').append('<div class="col-12 col-md-12 col-md-12">'+
                                '<h6 style="color: #aaa;">Menunggu Cor Siap</h6>'+
                                '<span class="text-red" style="font-size: 12px;">Status saat ini</span>'+
                              '</div>');
      }else{
      $('.list-status').append('<div class="col-12 col-md-12 col-md-12" onclick="changestatusproduksi(3)" style="cursor: pointer;">'+
                                '<h6>Menunggu Cor Siap</h6>'+
                              '</div>');
      }

      $('#detailModal').modal('hide');
      $('#statusModal').modal('show');

      // window.location.href = "<?= site_url('web/pesanan/detail')?>/"+id;
    })
  
  })

  function detailorder(id, kode, status){
    $('#idorder').val(id);
    //console.log(kode);
    //console.log(status);
    //$('.update-status').attr('kode','bbb');
    //$('.update-status').attr('status','bbb');
    $('#kodeOrderModal').html(kode);

    $('#detailModal .modal-body .row').html('');
    var role = '<?php echo SessionManagerWeb::getRole() ?>';
    if (status == 3) {
      if(role == 'P'){
	      var html = '<div class="col-12 col-md-12 col-md-12">'+
	            '<h6 style="color: #aaa;">Menunggu Cor Siap</h6>'+
	            '<span class="text-red" style="font-size: 12px;">Status saat ini</span>'+
	          '</div>'+
	          '<div class="col-12 col-md-12 col-md-12 detail-order" style="cursor: pointer;">'+
	            '<label style="cursor: pointer;">Detail Order</label>'+
	            '<p>Melihat detail informasi pada order</p>'+
	          '</div>';
       }else{
       	var html = '<div class="col-12 col-md-12 col-md-12 update-status" kode="'+kode+'" status="'+status+'" style="cursor: pointer;">'+
            '<label style="cursor: pointer;">Update Status Produksi</label>'+
            '<p>Ubah status untuk proses produksi</p>'+
          '</div>'+
          '<div class="col-12 col-md-12 col-md-12 detail-order" style="cursor: pointer;">'+
            '<label style="cursor: pointer;">Detail Order</label>'+
            '<p>Melihat detail informasi pada order</p>'+
          '</div>';
       }
    }else{

      if(role == 'P'){
        var html = '<a href="<?= site_url('web/pesanan/usulkanpemasangan') ?>/'+id+'" class="col-12 col-md-12 col-md-12" style="cursor: pointer;">'+
            '<label style="cursor: pointer;text-decoration:none;color:#212529;">Usulkan Jadwal Pemasangan</label>'+
            '<p style="text-decoration:none;color:#212529;">Mengajukan jadwal kepada Tim Marketing / Tim Desain</p>'+
          '</a>'+
          '<div class="col-12 col-md-12 col-md-12 detail-order" style="cursor: pointer;">'+
            '<label style="cursor: pointer;">Detail Order</label>'+
            '<p>Melihat detail informasi pada order</p>'+
          '</div>';
      }else{
        var html = '<div class="col-12 col-md-12 col-md-12 update-status" kode="'+kode+'" status="'+status+'" style="cursor: pointer;">'+
            '<label style="cursor: pointer;">Update Status Produksi</label>'+
            '<p>Ubah status untuk proses produksi</p>'+
          '</div>'+
          '<div class="col-12 col-md-12 col-md-12 detail-order" style="cursor: pointer;">'+
            '<label style="cursor: pointer;">Detail Order</label>'+
            '<p>Melihat detail informasi pada order</p>'+
          '</div>';
      }
      
    }
          
    $('#detailModal .modal-body .row').html(html);
    $('.detail-order').attr('id',id);

    $('#detailModal').modal('show');
  }

  function changestatusproduksi(status){
    $('#status_produksi').val(status);
    $('#formSubmit').submit();
  }

  function terimaUsulan(){
    // console.log($('#idorder').val());
    var id = $('#id_pesanan').val();
    var type_direct = $('#type_direct').val();
    if(type_direct=='P'){
      $('#formApprove').attr('action','<?php echo site_url('web/pesanan/usulan')?>/'+id);
    }else{
      $('#formApprove').attr('action','<?php echo site_url('web/komplain/usulan')?>/'+id);
    }
    
    // console.log($('#formApprove').attr('action'));
    $('#formApprove').submit();
  }




  function reschedule(id){
      window.location.href = "<?php echo site_url('web/pesanan/reschedulejadwal')?>/"+id;
  }

</script>