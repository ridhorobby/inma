<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Input Harga</h1>
        </div>
	</div>

	<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>

	<div class="row mb-4">
		<div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
			    <a class="btn btn-sm btn-add" href="<?php echo site_url('web/harga')?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            </span>
        </div>
	</div>    

    <div class="row harga-add">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<form action="<?php echo site_url('web/'.$class)?>/<?= ($method=='edit' ? 'update' : 'create') ?>" method="post">
        	<?php if($method=='edit'){ ?>
	          <input type="hidden" name="id" value="<?= $data['id'] ?>">
	        <?php } ?>
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Nama</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="text" name="nama" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['nama'] : '') ?>">
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Kategori</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<select name="kategori" class="form-control form-control-sm">
	           				<option hidden="true">-- Pilih Kategori --</option>
	           				<?php foreach ($listkategori as $key => $obj) { ?>	           					
	                          <option value="<?= $obj['id']  ?>" <?= ($obj['id'] == $data['kategori'] ? 'selected' : '') ?>> 
	                          	<?= $obj['nama']  ?>	                          	
	                          </option>
	           				<?php } ?>
	           			</select>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Keterangan Granit</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="text" name="note_granit" class="form-control form-control-sm"  value="<?= ($method=='edit' ? $data['note_granit'] : '') ?>">
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Keterangan Keramik</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="text" name="note_keramik" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['note_keramik'] : '') ?>">
	           		</div>
	           	</div>

	           	<h6 class="mt-4">Harga Produksi</h6>
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Harga Granit</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="number" name="harga_pokok_granit" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['harga_pokok_granit'] : '') ?>">
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Harga Keramik</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="number" name="harga_pokok_keramik" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['harga_pokok_keramik'] : '') ?>">
	           		</div>
	           	</div>

	           	<h6 class="mt-4">Harga Konsumen</h6>
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Harga Granit</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="number" name="harga_jual_granit" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['harga_jual_granit'] : '') ?>">
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Harga Keramik</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="number" name="harga_jual_keramik" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['harga_jual_keramik'] : '') ?>">
	           		</div>
	           	</div>
	           	<div class="form-group text-center mt-5">
	           		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
	           		<button type="button" onclick="hapus(<?= $data['id'] ?>)" class="btn btn-sm btn-add"><i class="fas fa-trash"></i> Hapus</button>
	           	</div>
	           	<div class="form-group text-center col-md-6 mt-5">
	           		
	           	</div>
        	</form>
        		
        </div>
  	</div>

 <script type="text/javascript">
 	function hapus(id){
 		var conf = confirm("Apakah anda yakin ingin menghapus data?");
 		if(conf){
 			window.location.href = "<?php echo site_url('web/harga/delete')?>/"+id;
 		}
 	}
 </script>
