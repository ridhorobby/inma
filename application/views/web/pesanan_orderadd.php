<link href="<?php echo base_url()?>assets/web/plugins/dropzone/dropzone.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/web/plugins/dropzone/dropzone.js"></script>


<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Input Konsumen</h1>
        </div>
	</div>

<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
	<div class="row mb-4">
		<div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
			    <a class="btn btn-sm btn-add" href="<?php echo site_url('web/pesanan/daftarorder')?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            </span>
        </div>
	</div>    

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<form action="<?= site_url("web/$class/create") ?>" method="post">
        		<input type="hidden" name="id_pengajuan" id="id_pengajuan" value="0">
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Kode Order</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<input type="text" name="kode_order" class="form-control form-control-sm" required>
	           		</div>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<button class="btn btn-secondary" data-target="#pilihKonsumenModal" data-toggle="modal"><i class="fas fa-list nav-icon"></i></button>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Nama</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<input type="text" name="nama" id="nama_konsumen" class="form-control form-control-sm" required>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Alamat</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<textarea name="alamat" id="alamat_konsumen" class="form-control form-control-sm" required> </textarea>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">No HP</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<input type="text" name="no_hp" id="no_hp_konsumen" class="form-control form-control-sm" required>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Waktu</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<button type="button" class="btn btn-sm btn-outline-secondary btn-block btn-pilih-tanggal">-- Pilih Tanggal --</button>
						<input type="text" name="tgl_order_masuk" class="datepicker" style="visibility:hidden;position: absolute;top: 50px;">

	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">PIC Desainer Design</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<select class="form-control form-control-sm" name="desainer_design">
							<option value="">-- Pilih --</option>
							<?php foreach ($desainer as $key => $value): ?>
								<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>	
							<?php endforeach ?>
						</select>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">PIC Desainer Eksekusi</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<select class="form-control form-control-sm" name="desainer_eksekusi">
							<option value="">-- Pilih --</option>
							<?php foreach ($desainer as $key => $value): ?>
								<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>	
							<?php endforeach ?>
						</select>
	           		</div>
	           	</div>

	           	<!-- <div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Jam</label>
	           		<div class="col-12 col-sm-12 col-md-2">
						<div class="input-group clockpicker">
							<input type="text" name="jam" class="form-control form-control-sm" value="" readonly required style="background-color: transparent;">
						</div>
	           		</div>
	           	</div> -->

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Status Produksi</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<select name="status_produksi" class="form-control form-control-sm" required> 
							<option value="">-- Pilih --</option>
							<option value="1">Bisa Cek Ruangan</option>
							<option value="2">Bisa Pasang Rangka</option>
							<option value="3">Menunggu Cor Siap</option>
						</select>
	           		</div>
	           	</div>
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Catatan</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<textarea class="form-control" name="catatan"></textarea>
	           		</div>
	           	</div>
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Lampiran Gambar</label>
	           		<div class="col-12 col-sm-12 col-md-4">
					  	<div class="dropzone dz-upload-1">
							<div class="dz-message">
							  	<label for="file-1">
							  		<i class="fa fa-cloud-upload" aria-hidden="true"></i>
							  		<h5>Upload <span>File</span></h5>
							  	</label>
								  <p> Klik atau Drag file kesini</p>
							</div>
					 	</div>
				 	</div>
				</div>
				
	           	
	           	

	           	<div class="form-group text-center mt-5">
	           		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
	           	</div>
        	</form>
        </div>
  	</div>


<!-- Pilih Konsumen Modal -->
<div class="modal fade" id="pilihKonsumenModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pilih Konsumen Deal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="col-md-12">

  			<div class="form-group row search-deal">
           		<div class="col-12 col-sm-12 col-md-10">
					<input type="text" name="keyword-deal" id="keyword-deal" class="form-control form-control-sm" placeholder="Keyword">
           		</div>
           		<div class="col-12 col-sm-12 col-md-2">
           			<button class="btn btn-primary" type="button" onclick="cariDeal()"> Cari</button>
           		</div>
           	</div>
      	
      	</div>

      	<div class="col-md-12 list-deal">
      		<?php foreach ($konsumen_deal as $key): ?>
				<div class="row mt-2 row-order-baru pilih-konsumen" style="cursor:pointer;color:#212529; ">
					<div class="col-7 col-sm-6 col-md-8 list-order-baru">
						<p class="nama_konsumen"><?= $key['nama_konsumen']  ?></p>
						<p class="alamat_konsumen"><?= substr($key['alamat_konsumen'],0,50).'...'  ?></p>
						<p class="no_hp_konsumen"><?= $key['no_hp_konsumen']  ?></p>
						<p style="display: none;" class="id_pengajuan"><?= $key['id_pengajuan']  ?></p>
						<p style="display: none;" class="alamat_konsumen_extend"><?= $key['alamat_konsumen']  ?></p>

					</div>
				</div>
			<?php endforeach; ?>
			
        
      	</div>
      	<div id="right">
					<div id="loader" style="display: none">Loading ...</div>
				</div>

      	
      </div>
    </div>
  </div>
</div>

<script>

	function cariDeal(){
		$('#loader').fadeIn('slow');
		$('.list-deal').empty();
		var keyword = $('#keyword-deal').val();
		$.ajax({
			type: "post",
			url: "<?= site_url('web/pesanan') ?>/orderadd",
			data: {keyword: keyword},
			success:function(respon){
				var html = '';
				if(respon!='false'){
					var data = JSON.parse(respon);

	         		
	         		for (var i =0; i < data.length; i++) {
	         			alamat = data[i].alamat_konsumen;
	         			html += 
	         				'<div class="row mt-2 row-order-baru pilih-konsumen" style="cursor:pointer;color:#212529; ">'+
	         				'<div class="col-7 col-sm-6 col-md-8 list-order-baru">'+
	         					'<p class="nama_konsumen">'+data[i].nama_konsumen+'</p>'+
		         				'<p class="alamat_konsumen">'+alamat+'....</p>'+
		         				'<p class="no_hp_konsumen">'+data[i].no_hp_konsumen+'</p>'+
		         				'<p style="display: none;" class="id_pengajuan">'+data[i].id_pengajuan+'</p>'+
		         				'<p style="display: none;" class="alamat_konsumen_extend">'+data[i].alamat_konsumen+'</p>'+
		         				'</div></div>';
	         		}
				}else{
					html+='<div>Data tidak ditemukan..</div>';
				}

         		
         		$('.list-deal').html(html);
         		$('#loader').css("display", "none");
	        	
	        }
		});
		// console.log(keyword);
	}
	Dropzone.autoDiscover = false;
	
	$(document).ready(function(){

		$(document).on('click','.pilih-konsumen', function(){
			var kode_order = $(this).find('.kode_order').html();
			var nama_konsumen = $(this).find('.nama_konsumen').html();
			var alamat_konsumen = $(this).find('.alamat_konsumen_extend').html();
			var no_hp_konsumen = $(this).find('.no_hp_konsumen').html();
			var id_pengajuan = $(this).find('.id_pengajuan').html();

			$("#nama_konsumen").val(nama_konsumen);
			$("#nama_konsumen").val(nama_konsumen);
			$("#alamat_konsumen").val(alamat_konsumen);
			$("#no_hp_konsumen").val(no_hp_konsumen);
			$("#id_pengajuan").val(id_pengajuan);
			$('#pilihKonsumenModal').modal('hide');
		})

		$('.btn-pilih-tanggal').on('click', function() {
		      $('.datepicker').datepicker('show');
		});

		var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
		  "Juli", "Agustus", "September", "Oktober", "November", "Desember"
		];
		$('.datepicker').datepicker({
		    format: 'yyyy-mm-dd',
		    autoclose: true
		}).on('change', function(e) {
		    var tanggal = $('.datepicker').val();
		    var dt = new Date(tanggal);
		    var html = dt.getDate()+' '+monthNames[dt.getMonth()]+' '+dt.getFullYear();
		    $('.btn-pilih-tanggal').html(html);
		  
		});

		// $('.clockpicker').clockpicker({
		// 	donetext: 'Done'
		// });

		$(document).on('click', '.btn-pilih-gambar', function(){
			$('#pilihGambarModal').modal('show');
		})

		if ($('.dropzone').length) {
			var myDropzone= new Dropzone(".dz-upload-1",{
				url: "<?= site_url('web/pesanan') ?>/sendimage",
				addRemoveLinks:true,
				maxFilesize: 5, // MB
				dictRemoveFile : "remove",
				removedfile: function(file){
					var name = file.name;

					$.ajax({
						type: "post",
						url: "<?= site_url('web/pesanan') ?>/removetemp",
						data: {file: name},
						dataType: "html"
					});

					//remove thumbnail
					var previewElement;
					return (previewElement = file.previewElement) != null ? (previewElement.parentNode.removeChild(file.previewElement)) : (void 0);
				},
			});
		}
	})
</script>