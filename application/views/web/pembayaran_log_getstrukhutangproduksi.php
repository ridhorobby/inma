<link href="<?php echo base_url()?>assets/web/plugins/bootstrap/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/web/plugins/bootstrap/js/bootstrap-toggle.min.js"></script>
<style>
	.table-harga th,
	.table-harga td{
		border-top: 1px solid #777;
	}
	.table-harga .th-harga,
	.table-harga .th-subtotal,
	.table-harga .th-jml,
	.table-harga .jml,
	.table-harga .granit,
	.table-harga .keramik{
		text-align: center;
	}
	.btn-granit{
		background-color: #dee2e6;
	    padding: 5px 50px;
	}
	.btn-keramik{
		background-color: #dcedc8;
	    padding: 5px 50px;
	}
	.share-icon{
	    cursor: pointer;
		float: right;
		right: 14%;
	    font-size: 1.25rem;
		position: absolute;
		color: #f71212;
		z-index: 1;
	}
	.logo-nasatech{
		position: absolute;
	    right: 2%;
	    top: 25px;
	}
	.logo-nasatech > label{
		font-weight: 400;
	    font-size: 2em;
	    margin: 0;
	}
	.logo-nasatech > label > b{
		color: #f71212;
	}
	.logo-nasatech > span{
		float: right;
	    font-size: .75rem;
        position: relative;
    	top: -15px;
	    right: 10px;
	}
	#canvas{
	    padding: 10px 15px;
	    background-color: #fff;
	}
	.hide-hrg-kons{
		display: none!important;
	}
	.hide-hrg-prod{
		display: none!important;
	}
</style>

<div class="row mb-4">
	<div class="col-sm-12 col-md-12 title-page">
        <!-- <h1>Tambah User</h1> -->
    </div>
</div>

<div class="row">
    
    <span class="share-icon" id="granit"><i class="fas fa-share-alt"></i></span>
    <div class="col-12 col-sm-12 col-md-12">
    	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>		              			
		<div style="position: absolute;z-index: 1;top: 150px;left: 15px;">
			<label style="padding-right: 5px;color: #dc3545;">Harga Produksi</label>
			<input class="check-harga-produksi" type="checkbox" data-toggle="toggle" data-size="xs" style="float:right;">
	    </div>
		<?php } ?>
	</div>
    <div class="col-12 col-sm-12 col-md-12" id="canvas">
    	<div class="logo-nasatech">
    		<label>Nasa<b>tech</b></label>
    		<br>
    		<span>Kitchenset</span>
    	</div>
    	<table style="margin-bottom: 15px;">
    		<input type="hidden" id="idpengajuan" value="<?= $data['id']?>">
    		<tr>
    			<td width="100px"><label>Kode Order</label></td>
    			<td><label class="text-red"><?= $data['kode_order']?></label></td>
    		</tr>
    		<tr>
    			<td style="font-weight: bold;">Nama</td>
    			<td><?= $data['nama']?></td>
    		</tr>
    		<tr>
    			<td style="font-weight: bold;">Alamat</td>
    			<td><?= $data['alamat']?></td>
    		</tr>
    		<tr>
    			<td style="font-weight: bold;">No HP</td>
    			<td><?= $data['no_hp']?></td>
    		</tr>
    	</table>
	    <div class="col-12 col-sm-12 col-md-12">
    		<div style="text-align: center;">
	    		<button type="button" class="btn btn-sm btn-granit" style="margin:10px 0;"><b>Granit</b></button>	
    		</div>
    	</div>
    	<table class="table table-sm table-harga">
    		<thead>
    			<th>No</th>
    			<th>Nama</th>
    			<th class="th-jml">Jml</th>
    			<th class="th-harga" style="background-color: #dee2e6;">@Harga</th>
    			<th class="th-subtotal" style="background-color: #dee2e6;">Subtotal</th>
    		</thead>
          <tbody>
          <?php 
          $no = 1;
          $totalgranitprod = 0;
          $totalkeramikprod = 0;
          $totalgranit = 0;
          $totalkeramik = 0;
          foreach ($detail as $index => $row){ ?>
				<tr style="background-color:#424242;color: #fff;">
				  <td colspan="5"><b><?= $index; ?></b></td>
				</tr>
        		<?php foreach ($row as $v) {
        			$totalgranitprod += $v['harga_pokok_granit']*$v['jumlah'];
        			$totalkeramikprod += $v['harga_pokok_keramik']*$v['jumlah'];

        			$totalgranit += $v['harga_jual_granit']*$v['jumlah'];
        			$totalkeramik += $v['harga_jual_keramik']*$v['jumlah'];
        			?>
		            <tr id="<?= $v['id_pengajuan']?>" class="rowharga">
		              <td><?= $no++; ?></td>
		              <td><?= $v['nama']; ?></td>
		              <td class="jml"><?= number_format($v['jumlah'],1)?></td>

		              <?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
		              	<td class="granit" style="background-color: #dee2e6;"><?= number_format($v['harga_pokok_granit'],0,",",".")?></td>
		              	<td class="keramik" style="display: none;background-color: #dcedc8;"><?= number_format($v['harga_pokok_keramik'],0,",",".")?></td>

		              	<td class="granit" style="background-color: #dee2e6;"><?= number_format($v['harga_pokok_granit']*$v['jumlah'],0,",",".")?></td>
		              	<td class="keramik" style="display: none;background-color: #dcedc8;"><?= number_format($v['harga_pokok_keramik']*$v['jumlah'],0,",",".")?></td>
			          <?php }else{ ?>
			          	<td class="granit hrg-kons" style="background-color: #dee2e6;"><?= number_format($v['harga_jual_granit'],0,",",".")?></td>
		              	<td class="keramik hrg-kons" style="display: none;background-color: #dcedc8;"><?= number_format($v['harga_jual_keramik'],0,",",".")?></td>

		              	<td class="granit hrg-kons" style="background-color: #dee2e6;"><?= number_format($v['harga_jual_granit']*$v['jumlah'],0,",",".")?></td>
		              	<td class="keramik hrg-kons" style="display: none;background-color: #dcedc8;"><?= number_format($v['harga_jual_keramik']*$v['jumlah'],0,",",".")?></td>

		              	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>		              			
			              	<td class="granit hrg-prod hide-hrg-prod" style="background-color: #dee2e6;"><?= number_format($v['harga_pokok_granit'],0,",",".")?></td>
			              	<td class="keramik hrg-prod hide-hrg-prod" style="display: none;background-color: #dcedc8;"><?= number_format($v['harga_pokok_keramik'],0,",",".")?></td>

			              	<td class="granit hrg-prod hide-hrg-prod" style="background-color: #dee2e6;"><?= number_format($v['harga_pokok_granit']*$v['jumlah'],0,",",".")?></td>
			              	<td class="keramik hrg-prod hide-hrg-prod" style="display: none;background-color: #dcedc8;"><?= number_format($v['harga_pokok_keramik']*$v['jumlah'],0,",",".")?></td>
				        <?php } ?>
			          <?php } ?>

		            </tr>
          <?php } 
          } ?>
          <tr>
          	<td colspan="3" style="text-align: right"><b>Total</b></td>
          	<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
	        	<td colspan="2" class="granit" style="text-align: right"><b><?= number_format($totalgranitprod,0,",",".") ?></b></td>
	          	<td colspan="2" class="keramik" style="text-align: right;display: none;"><b><?= number_format($totalkeramikprod,0,",",".") ?></b></td>
        	<?php }else{ ?>
	          	<td colspan="2" class="granit hrg-kons" style="text-align: right"><b><?= number_format($totalgranit,0,",",".") ?></b></td>
	          	<td colspan="2" class="keramik hrg-kons" style="text-align: right;display: none;"><b><?= number_format($totalkeramik,0,",",".") ?></b></td>

	          	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>		              			
		        	<td colspan="2" class="granit hrg-prod hide-hrg-prod" style="text-align: right"><b><?= number_format($totalgranitprod,0,",",".") ?></b></td>
		          	<td colspan="2" class="keramik hrg-prod hide-hrg-prod" style="text-align: right;display: none;"><b><?= number_format($totalkeramikprod,0,",",".") ?></b></td>
		        <?php } ?>
	        <?php } ?>
          </tr>
          </tbody>
        </table>
    </div>

</div>

<div class="modal fade" id="downloadStrukModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Apakah anda ingin mendownload / preview pdf stuk ini ?</div>
        <div class="modal-footer">
          <!-- <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button> -->
          <button type="button" id="preview-granit" class="btn btn-sm btn-info preview-struk" tipeharga="kons"><i class="fas fa-eye"></i> Preview Pdf</button>
          <a class="btn btn-sm btn-add download-struk" href="#"><i class="fas fa-download"></i> Download</a>
        </div>
      </div>
    </div>
</div>

<script src="<?php echo base_url()?>assets/web/js/html2canvas.js"></script>

<script>
	$(document).ready(function(){
		$(document).on('click','.btn-granit', function(){
			$(this).removeClass('btn-granit');
			var btnclass = $(this).addClass('btn-keramik');

			$('.btn-keramik').html('<b>Keramik</b>');

			$('#granit').attr('id','keramik');
			$('#preview-granit').attr('id','preview-keramik');
			$('.th-harga').css({'background-color':'#dcedc8'});
			$('.th-subtotal').css({'background-color':'#dcedc8'});
			$('.granit').hide();
			$('.keramik').show();
		})

		$(document).on('click','.btn-keramik', function(){
			$(this).removeClass('btn-keramik');
			var btnclass = $(this).addClass('btn-granit');

			$('.btn-granit').html('<b>Granit</b>');

			$('#keramik').attr('id','granit');
			$('#preview-keramik').attr('id','preview-granit');
			$('.th-harga').css({'background-color':'#dee2e6'});
			$('.th-subtotal').css({'background-color':'#dee2e6'});
			$('.keramik').hide();
			$('.granit').show();
		})

		$('.preview-struk').click(function(){
			var id = $('#idpengajuan').val();
			var jenis = $(this).attr('id');
			var jenis = jenis.replace("preview-", "");
			var tipeharga = $(this).attr('tipeharga');
			var url = '<?= site_url('web/pembayaran_log/previewstrukhutangproduksipdf')?>/'+id+'/'+jenis+'/'+tipeharga;
		    window.open(url, '_blank');
		})

		$('.share-icon').click(function(){

			var jenis = $(this).attr('id');
            html2canvas($('#canvas'), {
	        onrendered: function (canvas) {

	                var imgageData =  
		            canvas.toDataURL("image/png",1); 
		           
		            var newData = imgageData.replace( 
		            /^data:image\/png/, "data:application/octet-stream"); 
		           
		            $(".download-struk").attr( 
		            "download", jenis+".png").attr( 
		            "href", newData);

	                $('#downloadStrukModal').modal('show');

	            }
	        });
		})

		$(".download-struk").click(function(){
            $('#downloadStrukModal').modal('hide');			
		})

		<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>		              					
		$('.check-harga-produksi').change(function() {
		    if(this.checked) {
		        $('.hrg-kons').addClass('hide-hrg-kons');
		        $('.hrg-prod').removeClass('hide-hrg-prod');
		        $('.preview-struk').attr('tipeharga','prod');
		    }else{
				$('.hrg-prod').addClass('hide-hrg-prod');
		        $('.hrg-kons').removeClass('hide-hrg-kons');
		        $('.preview-struk').attr('tipeharga','kons');
		    }
		});
		<?php } ?>
	})
</script>