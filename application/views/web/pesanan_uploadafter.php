<link href="<?php echo base_url()?>assets/web/plugins/dropzone/dropzone.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/web/plugins/dropzone/dropzone.js"></script>

<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Upload Gambar After</h1>
        </div>
	</div>

<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
	<div class="row mb-4">
		<div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
			    <a class="btn btn-sm btn-add" href="<?php echo site_url('web/pesanan/detail/'.$pesanan_id)?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            </span>
        </div>
	</div>    

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<form action="<?= site_url("web/$class/saveFotoAfter/".$pesanan_id) ?>" method="post">
	           	
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Foto After</label>
	           		<div class="col-12 col-sm-12 col-md-4">
					  	<div class="dropzone dz-upload-1">
							<div class="dz-message">
							  	<label for="file-1">
							  		<i class="fa fa-cloud-upload" aria-hidden="true"></i>
							  		<h5>Upload <span>File</span></h5>
							  	</label>
								  <p> Klik atau Drag file kesini</p>
							</div>
					 	</div>
				 	</div>

				</div>
				<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Edit Katalog</label>
	           		<div class="col-12 col-sm-12 col-md-4">
					  	<div class="dropzone dz-upload-1-ek">
							<div class="dz-message">
							  	<label for="file-1">
							  		<i class="fa fa-cloud-upload" aria-hidden="true"></i>
							  		<h5>Upload <span>File</span></h5>
							  	</label>
								  <p> Klik atau Drag file kesini</p>
							</div>
					 	</div>
				 	</div>

				</div>
				<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Posting IG</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<textarea name="link" class="form-control"><?= $detail['link_sosmed'] ?></textarea>
	           		</div>
	           	</div>
	           	
	           	

	           	<div class="form-group text-center mt-5">
	           		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
	           	</div>
        	</form>
        </div>
  	</div>



<script>
	Dropzone.autoDiscover = false;
	
	$(document).ready(function(){

		$('.btn-pilih-tanggal').on('click', function() {
		      $('.datepicker').datepicker('show');
		});

		var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
		  "Juli", "Agustus", "September", "Oktober", "November", "Desember"
		];
		$('.datepicker').datepicker({
		    format: 'yyyy-mm-dd',
		    autoclose: true
		}).on('change', function(e) {
		    var tanggal = $('.datepicker').val();
		    var dt = new Date(tanggal);
		    var html = dt.getDate()+' '+monthNames[dt.getMonth()]+' '+dt.getFullYear();
		    $('.btn-pilih-tanggal').html(html);
		  
		});

		$(document).on('click', '.btn-pilih-gambar', function(){
			$('#pilihGambarModal').modal('show');
		})

		if ($('.dropzone').length) {
			var myDropzone= new Dropzone(".dz-upload-1",{
				url: "<?= site_url('web/pesanan') ?>/sendimagetemp?directory=foto_after",
				addRemoveLinks:true,
				maxFilesize: 5, // MB
				dictRemoveFile : "remove",
				removedfile: function(file){
					var name = file.name;

					$.ajax({
						type: "post",
						url: "<?= site_url('web/pesanan') ?>/removetemp",
						data: {file: name},
						dataType: "html"
					});

					//remove thumbnail
					var previewElement;
					return (previewElement = file.previewElement) != null ? (previewElement.parentNode.removeChild(file.previewElement)) : (void 0);
				},
			});
		}

		if ($('.dropzone').length) {
			var myDropzone= new Dropzone(".dz-upload-1-ek",{
				url: "<?= site_url('web/pesanan') ?>/sendimagetemp?directory=edit_katalog",
				addRemoveLinks:true,
				maxFilesize: 5, // MB
				dictRemoveFile : "remove",
				removedfile: function(file){
					var name = file.name;

					$.ajax({
						type: "post",
						url: "<?= site_url('web/pesanan') ?>/removetemp",
						data: {file: name},
						dataType: "html"
					});

					//remove thumbnail
					var previewElement;
					return (previewElement = file.previewElement) != null ? (previewElement.parentNode.removeChild(file.previewElement)) : (void 0);
				},
			});
		}
	})
</script>