

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title><?php echo $title; ?></title>

<link rel="icon" href="<?php echo base_url()?>assets/web/img/favicon.png">

<link href="<?php echo base_url();?>assets/web/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<!-- <link href="<?php echo base_url();?>assets/web/vendor/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet"> -->
<link href="<?php echo base_url();?>assets/web/css/login.css" rel="stylesheet">
<!-- <script src="<?php echo base_url();?>assets/web/vendor/jquery/jquery-1.12.0.min.js"></script> -->
<!-- <script src="<?php echo base_url();?>assets/web/vendor/bootstrap/js/bootstrap.min.js"></script> -->
<script src="<?php echo base_url();?>assets/web/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url();?>assets/web/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!--
    you can substitue the span of reauth email for a input with the email and
    include the remember me checkbox
    -->
</head>
<body>


    <div class="container">
      <div class="row">
        <div class="col-12">

          <div class="card card-container">
            <div style="text-align: center;">
              <img id="profile-img" class="" src="<?php echo base_url();?>assets/web/img/logo.png" style="width: 125px;"/>
            </div>
              <p id="profile-name" class="profile-name-card"></p>
                <?php echo form_open(); ?>

                     <?php if (!empty($errmsg)) { 
                      ?>
                      <div class="flashdata">
                          <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
                              <?php echo implode('<br>',$errmsg) ?>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                      </div>
                      <?php
                      }?>
                    <div class="form-group">                    
                        <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
                    </div>
                    <div class="form-group">                    
                        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
                        <span id="icon-pwd" toggle="#inputPassword" class="fa fa-eye-slash"></span>
                    </div>
                    <div class="form-group mb-0">                    
                        <button type="submit" class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Login</button>
                    </div>
              </form><!-- /form -->
              <!-- <a href="<?php echo site_url('auth/register')?>" class="forgot-password">
                  Belum punya akun ?
              </a> -->
          </div><!-- /card-container -->
        </div>
      </div>
    </div><!-- /container -->

    <script>
        $(document).ready(function(){
            $("#icon-pwd").click(function() {

              $(this).toggleClass("fa-eye fa-eye-slash");
              var input = $($(this).attr("toggle"));
              if (input.attr("type") == "password") {
                input.attr("type", "text");
              } else {
                input.attr("type", "password");
              }
            });
        })
    </script>
</body>
</html>