<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>History Konsumen</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button>
        </span>
        <span style="float: right;">
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
    	<div class="kalender-kas text-center">
    		<button type="button" id="prev" class="btn btn-sm btn-light float-left"><i class="fas fa-angle-left"></i></button>
    		<label>Januari 2021</label>
    		<input type="hidden" id="month" value="<?= date('m')?>">
    		<input type="hidden" id="year" value="<?= date('Y')?>">
    		<button type="button" id="next" class="btn btn-sm btn-light float-right"><i class="fas fa-angle-right"></i></button>
    	</div>
    </div>
    <div class="col-md-12">
       	<table class="table table-sm table-kas">
          <tbody>
            <tr>
              <td colspan="4">
                <a href="#" class="text-reset">
              	<div class="row history-konsumen-row">
                  <div class="col-12 col-sm-12 col-md-12">
                    <label>TESTINGNEW</label><span class="pl-2">Edo</span>
                  </div>
	              	<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-green">
		              	<h6>Order Masuk</h6>
		              	<p>13 Jan</p>
		              </div>
	              	<div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-yellow">
	              		<h6>Cek Ruangan / Pasang Kusen</h6>
                    <p>-</p>
              		</div>
                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-purple">
                    <h6>Pemasangan Unit</h6>
                    <p>-</p>
                  </div>
                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-blue">
                    <h6>Menunggu Set Selesai</h6>
                    <p>-</p>
                  </div>

                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content">
                    <div class="history-icon">
                      <div class="history-bullet-green"><i class="fas fa-check"></i></div>
                    </div>
                  </div>

                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content">
                    <div class="history-icon">
                      <div class="history-bullet-red"><i class="fas fa-pause"></i></div>
                    </div>
                  </div>

                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content">
                    <div class="history-icon">
                      <div class="history-bullet-default"></div>
                    </div>
                  </div>

                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content">
                    <div class="history-icon">
                      <div class="history-bullet-default"></div>
                    </div>
                  </div>
              	</div>
                </a>
              </td>
            </tr>

            <tr>
              <td colspan="4">
                <a href="#" class="text-reset">
                <div class="row history-konsumen-row">
                  <div class="col-12 col-sm-12 col-md-12">
                    <label>TESTINGNEW</label><span class="pl-2">Edo</span>
                  </div>
                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-green">
                    <h6 class="text-truncate">Order Masuk</h6>
                    <p>13 Jan</p>
                  </div>
                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-yellow">
                    <h6 class="text-truncate">Cek Ruangan / Pasang Kusen</h6>
                    <p>-</p>
                  </div>
                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-purple">
                    <h6 class="text-truncate">Pemasangan Unit</h6>
                    <p>-</p>
                  </div>
                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center div-blue">
                    <h6 class="text-truncate">Menunggu Set Selesai</h6>
                    <p>-</p>
                  </div>

                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content">
                    <div class="history-icon">
                      <div class="history-bullet-green"><i class="fas fa-check"></i></div>
                    </div>
                  </div>

                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content">
                    <div class="history-icon">
                      <div class="history-bullet-red"><i class="fas fa-pause"></i></div>
                    </div>
                  </div>

                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content">
                    <div class="history-icon">
                      <div class="history-bullet-default"></div>
                    </div>
                  </div>

                  <div class="col-3 col-sm-3 col-md-3 pt-2 pb-2 text-center history-icon-content">
                    <div class="history-icon">
                      <div class="history-bullet-default"></div>
                    </div>
                  </div>
                </div>
                </a>
              </td>
            </tr>
            
          </tbody>
        </table>
    </div>
</div>

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pencarian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post">
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-12 col-md-12 col-md-12">
            <input type="text" name="kode_order" class="form-control" placeholder="Kode Order">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12 col-md-12 col-md-12">
            <input type="text" name="nama" class="form-control" placeholder="Nama">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12 col-md-12 col-md-12">
            <input type="text" name="alamat" class="form-control" placeholder="Alamat">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12 col-md-12 col-md-12">
            <input type="text" name="tlp" class="form-control" placeholder="No HP">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-sm btn-secondary" >Reset</a>
        <button type="submit" class="btn btn-sm btn-add">Terapkan</button>
      </div>
    </form>
    </div>
  </div>
</div>

<script>
	$(document).ready(function(){
		$('#prev').click(function(){
			var month = $('#month').val();
			var year = $('#year').val();
			
			alert('prev date');
		})

		$('#next').click(function(){
			var month = $('#month').val();
			var year = $('#year').val();
			
			alert('next date');
		})
	})
</script>