
<style>
	.event-container > .event-icon{
	    margin: 25px 15px 0 10px;
	}
	.event-container::before{
		width: 1.5px;
		left: 5px;
		z-index: 0;
	    background-color: #757575;
	}
	.event-container:hover{
	    background-color: transparent;
	}
	
	@media screen and (max-width: 1280px) {
		.event-container::before{
			left: -17.5px;
			top: 10px;
		}
		.event-container > .event-icon {
		    margin: 25px 10px 0 0px;
		}
		.event-container > .event-info > p.event-title > span{
			right: -30px;
		}
	}
</style>
<div class="row mb-4">
	<div class="col-sm-12 col-md-12 title-page">
        <h1>Detail User</h1>
    </div>
</div>    

<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
<div class="row">

    <div class="col-sm-12 col-md-8">
    	<form method="POST" action="<?= site_url("web/$class/update/".$user['id'])  ?>">
		  <div class="form-group">
		    <label for="exampleInputEmail1">Username</label>
		    <input type="text" name="username" class="form-control"  placeholder="Masukkan username" value="<?= $user['username'] ?>" disabled>
		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Nama</label>
		    <input type="text" name="name" class="form-control"  placeholder="Masukkan nama" value="<?= $user['name'] ?>">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Jabatan</label>
		    <select class="form-control" name="jabatan">
		    	<?php foreach ($list_role as $key => $value): ?>
		    		<option value="<?= $key  ?>" <?= ($key == $user['role']) ? 'selected' : ''  ?>><?= $value  ?></option>
		    	<?php endforeach ?>
		    </select>
		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">No. HP</label>
		    <input type="text" name="no_hp" class="form-control"  placeholder="Masukkan no hp" value="<?= $user['no_hp'] ?>">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Rekening Bank</label>
		    <input type="text" name="bank_rekening" class="form-control"  placeholder="Masukkan Rekening bank" value="<?= $user['bank_rekening'] ?>">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">No. Rekening</label>
		    <input type="text" name="no_rekening" class="form-control"  placeholder="Masukkan Nomor Rekening" value="<?= $user['no_rekening'] ?>">
		  </div>
		  <button onclick="showPassField(1)" type="button" id="button_show" class="btn btn-primary" style="background-color: #f71212; border-color: #f71212">Ganti Password</button>
		  <button onclick="showPassField(0)" type="button" id="button_hide" class="btn btn-primary" style="background-color: #f71212; border-color: #f71212; display:none;">Tanpa Password</button>
		  <div id="pass_lama" class="form-group" style="display:none">
		    <label for="exampleInputEmail1">Password Lama</label>
		    <input type="password" name="oldPassword" class="form-control"  placeholder="Masukkan password lama">
		  </div>
		  <div id="pass_baru" class="form-group" style="display:none">
		    <label for="exampleInputEmail1">Password Baru</label>
		    <input type="password" name="newPassword" class="form-control"  placeholder="Masukkan password baru">
		  </div>
		  <div id="pass_conf" class="form-group" style="display:none">
		    <label for="exampleInputEmail1">Konfirmasi Password</label>
		    <input type="password" name="confirmPassword" class="form-control"  placeholder="Konfirmasi password baru">
		  </div>
		  <br>
		  <br>
		  <button type="submit" class="btn btn-primary" style="background-color: #f71212; border-color: #f71212">Simpan</button>
		</form>
		<br>
		<a href="<?= site_url('web/'.$class.'/resetpassword/'.$user['id']) ?>" class="btn btn-primary" style="background-color: #f71212; border-color: #f71212;">Reset Password</a>
		<br>
		<br>

		<?php if ($user['is_active'] == 1){ ?>
		<a href="<?= site_url('web/'.$class.'/nonactive/'.$user['id']) ?>" class="btn btn-primary" style="background-color: #f71212; border-color: #f71212;">Nonaktifkan</a>
		<?php } else { ?>
		<a href="<?= site_url('web/'.$class.'/activated/'.$user['id']) ?>" class="btn btn-primary" style="background-color: #f71212; border-color: #f71212;">Aktifkan</a>
		<?php } ?>
    </div>

   

</div>

<script>
	function showPassField(a){
		if(a == 1){
			$('#pass_lama').show();
			$('#pass_baru').show();
			$('#pass_conf').show();
			$('#button_hide').show();
			$('#button_show').hide();
		}else{
			$('#pass_lama').hide();
			$('#pass_baru').hide();
			$('#pass_conf').hide();
			$('#button_hide').hide();
			$('#button_show').show();
		}
		
	}
</script>