<link href="<?php echo base_url()?>assets/web/plugins/bootstrap/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/web/plugins/bootstrap/js/bootstrap-toggle.min.js"></script>
<style>
	.pilih-jenis:hover{
		background-color: #eee;
	}
	.granit,
	.granit-jual,
	.granit-pokok{
		background-color: #dee2e6;
	    padding: 0 5px;
	}
	.keramik,
	.keramik-jual,
	.keramik-pokok{
		background-color: #dcedc8;
	    padding: 0 5px;
	}
	.jumlah,
	.jml{
	    width: 55px;
	    text-align: center;
        padding: 10px 0;
	}

	.table-harga th,
	.table-harga td{
		border-top: 1px solid #777;
	}
	.table-harga .th-harga,
	.table-harga .th-subtotal,
	.table-harga .th-jml,
	.table-harga .jml,
	.table-harga .granit,
	.table-harga .keramik{
		text-align: center;
	}
	.btn-granit{
		background-color: #dee2e6;
	    padding: 5px 50px;
	}
	.btn-keramik{
		background-color: #dcedc8;
	    padding: 5px 50px;
	}
	.share-icon-check,
	.share-icon{
	    cursor: pointer;
		float: right;
		right: 20%;
	    font-size: 1.25rem;
		position: absolute;
		color: #f71212;
		z-index: 1;
	}
	.logo-nasatech{
		position: absolute;
	    right: 1%;
	    top: 25px;
	}
	.logo-nasatech > label{
		font-weight: 400;
	    font-size: 2em;
	    margin: 0;
	}
	.logo-nasatech > label > b{
		color: #f71212;
	}
	.logo-nasatech > span{
		float: right;
	    font-size: .75rem;
        position: relative;
    	top: -15px;
	    right: 10px;
	}
	.hide-hrg-kons{
		display: none!important;
	}
	.hide-hrg-prod{
		display: none!important;
	}

	@media (max-width: 768px) {
		.logo-nasatech {
		    top: 0px;
		}
		.share-icon-check, 
		.share-icon{
			top: 0;
		    font-size: 16px;
	        right: 26%;
		}

		#strukModal,
		#strukCheckModal{
		    font-size: 11px;
		}

		.logo-nasatech > span {
		    top: -13px;
		    right: 3px;
		}

		<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) { ?>
		#strukCheckModal .btn-granit,
		#strukModal .btn-granit,
		#strukCheckModal .btn-keramik,
		#strukModal .btn-keramik{
		    font-size: 10px;
	        margin-top: 25px!important;
		}

		.swithproduksi-div > div {
		    top: 95px!important;
		    left: 8px!important;
		}
		<?php }else{ ?>
		.swithproduksi-div > div {
		    top: 90px!important;
		    left: 8px!important;
		}
		#strukCheckModal .btn-granit,
		#strukModal .btn-granit,
		#strukCheckModal .btn-keramik,
		#strukModal .btn-keramik{
		    font-size: 10px;
		}
		<?php } ?>

		.swithproduksi-div > div > label{
			padding-right: 20px!important;
		}

		#strukCheckModal .toggle.btn-xs,
		#strukModal .toggle.btn-xs{
			min-height: 1.2rem;
		}
		.btn-group-xs>.btn, .btn-xs {
		    padding: .35rem .3rem .15rem .3rem!important;
		    font-size: 0.75rem!important;
		    line-height: .4!important;
		}
	}
	@media (min-width: 576px) {
		/*#inputHargaModal .modal-dialog {
		    max-width: 650px;
		    margin: 1.75rem auto;
		}

		#strukModal .modal-dialog {
		    max-width: 700px;
		    margin: 1.75rem auto;
		}*/
	}
</style>
<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) {
$harga_jual = 'display: none;';		              	
$harga_produksi = 'display: block;';		              	
}else{
$harga_jual = 'display: block;';		              	
$harga_produksi = 'display: none;';		              	

} ?>
	<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Detail Harga</h1>
        </div>
	</div>

	<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
	<div class="row mb-4">
		<div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
			    <a class="btn btn-sm btn-add" href="<?php echo site_url('web/'.$class)?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            </span>
        </div>
	</div>    

    <div class="row harga-add">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<!-- <form action="<?php echo site_url('web/'.$class)?>/<?= ($method=='edit' ? 'update' : 'create') ?>" method="post"> -->
	           	<h6>Konsumen</h6>
	        	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Kode Order</div>
	           		<div class="col-7 col-sm-7 col-md-4">
	           			<?= $data['kode_order'] ?>
	           		</div>
	           	</div>
	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Nama</div>
	           		<div class="col-7 col-sm-7 col-md-4">
	           			<?= ($data['id_konsumen'] != 0) ? $data['nama'] : $data['nama_konsumen']  ?>
	           		</div>
	           	</div>
	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Alamat</div>
	           		<div class="col-7 col-sm-7 col-md-4">
	           			<?= ($data['id_konsumen'] != 0) ? $data['alamat'] : $data['alamat_konsumen']  ?>
	           		</div>
	           	</div>
	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">No HP</div>
	           		<div class="col-7 col-sm-7 col-md-4">
	           			<?= ($data['id_konsumen'] != 0) ? $data['no_hp'] : $data['no_hp_konsumen']  ?>
	           		</div>
	           	</div>

	           	<?php if (SessionManagerWeb::getRole() != Role::PRODUKSI) { ?>
	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2"></div>
	           		<div class="col-7 col-sm-7 col-md-4">
			           	<button type="button" class="btn btn-sm btn-outline-secondary btn-block" data-target="#updateKonsumenModal" data-toggle="modal">Update Konsumen</button>
			        </div>
			    </div>
	           	<?php } ?>
	           	
	           	<h6 class="mt-2">Pengajuan Harga</h6>
	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Diajukan Oleh</div>
	           		<div class="col-7 col-sm-7 col-md-4">
	           			<?= $data['user_pengaju'] ?>
	           		</div>
	           	</div>
	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Tanggal Pengajuan</div>
	           		<div class="col-7 col-sm-7 col-md-4">
	           			<?= xFormatDateInd($data['tgl_pengajuan']) ?>
	           		</div>
	           	</div>
	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Jenis</div>
	           		<div class="col-7 col-sm-7 col-md-4">
	           			<?php if($data['jenis'] == 1){
	           				echo "Granit";
	           			}elseif($data['jenis'] == 2){ 
	           				echo "Keramik";
	           			}else{
	           				echo "-";
	           			} 
	           			if ($data['jenis'] == 0 || (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) ) {
	           				echo '<button class="btn btn-sm btn-outline-secondary" style="float: right;" data-target="#jenisModal" data-toggle="modal"><i class="fas fa-pencil-alt"></i></button>';
	           			}
	           			?>
	           		</div>
	           	</div>
	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Keterangan</div>
	           		<div class="col-7 col-sm-7 col-md-4">
	           			<?= $data['keterangan']?>
	           		</div>
	           	</div>

	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Status</div>
	           		<div class="col-7 col-sm-7 col-md-4">
	           			<?php if($data['status'] == 1) {
			                echo '<span style="color: #3f51b5;"><b>Draft</b></span>';
			              }elseif($data['status'] == 2) {
			                echo '<span style="color: #ffc107;"><b>Menunggu Approval</b></span>';
			              }elseif($data['status'] == 3) {
			                echo '<span style="color: #f44336;"><b>Revisi</b></span>';
			              }elseif($data['status'] == 4) {
			                echo '<span style="color: #4caf50;"><b>Disetujui</b></span>';
			              }elseif($data['status'] == 5) {
			                echo '<span style="color: #f44336;"><b>Revisi HPP</b></span>';
			              } ?>
	           		</div>
	           	</div>

	           	<?php if($data['status'] == 1 || $data['status'] == 3) { ?>
	           	<div class="row mt-2 mb-2">
	           		<label class="col-12 col-sm-12 col-md-2"></label>
           			<div class="col-12 col-sm-12 col-md-4">
	           			<button type="button" class="btn btn-sm btn-outline-secondary btn-block" data-target="#inputHargaModal" data-toggle="modal"><i class="fas fa-pencil-alt" style="float: left;padding: 3px;"></i> Edit Pengajuan</button>
	           		</div>
	           	</div>

	           	<div class="row mb-2">
	           		<label class="col-12 col-sm-12 col-md-2"></label>
           			<div class="col-12 col-sm-12 col-md-4">
           				<form id="main_form_ajukan" action="<?php echo site_url('web/pengajuan_harga/ajukanharga')?>" method="post">
						    <input type="hidden" name="id" value="<?= $data['id']?>">       					
	           				<button type="button" id="kirimpengajuan" class="btn btn-sm btn-add btn-block"><i class="fas fa-paper-plane" style="float: left;padding: 3px;"></i> Kirim Pengajuan</button>
           				</form>
	           		</div>
	           	</div>
	            <?php }elseif($data['status'] == 2 && (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) ) { ?>
            	<div class="row mt-2 mb-2">
	           		<label class="col-12 col-sm-12 col-md-2"></label>
           			<div class="col-6 col-sm-6 col-md-2">
           				<form id="main_form_tolak_harga" action="<?php echo site_url('web/pengajuan_harga/tolakharga')?>" method="post">
						    <input type="hidden" name="id" value="<?= $data['id']?>">       					
		           			<button type="button" id="tolakharga" class="btn btn-sm btn-add btn-block"><i class="fas fa-times" style="float: left;padding: 3px;"></i>Tolak</button>
           				</form>
	           		</div>
           			<div class="col-6 col-sm-6 col-md-2">
           				<form id="main_form_approve_harga" action="<?php echo site_url('web/pengajuan_harga/approveharga')?>" method="post">
						    <input type="hidden" name="id" value="<?= $data['id']?>">
						    <input type="hidden" name="harga_setuju_granit" value="<?= $sum['total_harga_jual_granit']?>">
						    <input type="hidden" name="harga_setuju_keramik" value="<?= $sum['total_harga_jual_keramik']?>">
						    <input type="hidden" name="harga_setuju_pokok_granit" value="<?= $sum['total_harga_pokok_granit']?>">
						    <input type="hidden" name="harga_setuju_pokok_keramik" value="<?= $sum['total_harga_pokok_keramik']?>">
	           				<button type="button" id="approveharga" class="btn btn-sm btn-success btn-block"><i class="fas fa-check" style="float: left;padding: 3px;"></i>Setuju</button>
           				</form>
           			</div>
	           	</div>
	            <?php }elseif($data['status'] == 4 && (SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::ADMINISTRATOR)) { ?>
            	<div class="row mt-2 mb-2">
	           		<label class="col-12 col-sm-12 col-md-2"></label>
           			<div class="col-12 col-sm-12 col-md-4">
	           			<button type="button" class="btn btn-sm btn-outline-secondary btn-block" data-target="#inputHargaModal" data-toggle="modal"><i class="fas fa-pencil-alt" style="float: left;padding: 3px;"></i> Revisi Harga</button>
	           		</div>
	           	</div>
	            <?php }elseif($data['status'] == 5 && (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) ) { ?>
	            <div class="row mt-2 mb-2">
	           		<label class="col-12 col-sm-12 col-md-2"></label>
           			<div class="col-12 col-sm-12 col-md-4">
           				<form id="main_form_approve_harga" action="<?php echo site_url('web/pengajuan_harga/approveharga')?>" method="post">
						    <input type="hidden" name="id" value="<?= $data['id']?>">
						    <input type="hidden" name="harga_setuju_granit" value="<?= $sum['total_harga_jual_granit']?>">
						    <input type="hidden" name="harga_setuju_keramik" value="<?= $sum['total_harga_jual_keramik']?>">
						    <input type="hidden" name="harga_setuju_pokok_granit" value="<?= $sum['total_harga_pokok_granit']?>">
						    <input type="hidden" name="harga_setuju_pokok_keramik" value="<?= $sum['total_harga_pokok_keramik']?>">
	           				<button type="button" id="approveharga" class="btn btn-sm btn-success btn-block"><i class="fas fa-check" style="float: left;padding: 3px;"></i>Setuju</button>
           				</form>
           			</div>
	           	</div>
	            <?php }elseif ($data['status'] == 4 && (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) ) { ?>
	            <div class="row mt-2 mb-2">
	           		<label class="col-12 col-sm-12 col-md-2"></label>
           			<div class="col-12 col-sm-12 col-md-4">
	           			<button type="button" class="btn btn-sm btn-outline-secondary btn-block" data-target="#inputHargaModal" data-toggle="modal"><i class="fas fa-pencil-alt" style="float: left;padding: 3px;"></i> Edit Pengajuan</button>
	           		</div>
	           	</div>
	            <?php }

	            if($data['revisi_hpp'] == 1 && (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) ) { ?>

				<div class="row mt-2 mb-2">
	           		<label class="col-12 col-sm-12 col-md-2"></label>
           			<div class="col-12 col-sm-12 col-md-4">
	           			<button type="button" class="btn btn-sm btn-outline-secondary btn-block" data-target="#inputHargaModal" data-toggle="modal"><i class="fas fa-pencil-alt" style="float: left;padding: 3px;"></i> Revisi Harga</button>
	           		</div>
	           	</div>
	            <?php } ?>

	           	<div class="row">
	           		<label class="col-5 col-sm-5 col-md-2">Daftar Harga</label>
           			<div class="col-7 col-sm-7 col-md-4 text-right">

	           			<button type="button" class="btn btn-sm btn-outline-secondary" onclick="getcheckstruk()" style="color: #f71212;border-color: #f71212;"><i class="fas fa-tasks nav-icon"></i></button>
	           			<button type="button" class="btn btn-sm btn-add" onclick="getstruk()"><i class="fas fa-list-alt nav-icon"></i></button>
	           		</div>
	           	</div>

	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Jumlah Item</div>
	           		<div class="col-7 col-sm-7 col-md-4 text-right">
	           			<?= $sum['jumlah']?>
	           		</div>
	           	</div>

	           	<?php if(SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Harga Granit</div>
	           		<div class="col-7 col-sm-7 col-md-4 text-right">
	           			<?= number_format($sum['total_harga_pokok_granit'],0,",",".")?>
	           		</div>
	           	</div>
	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Harga Keramik</div>
	           		<div class="col-7 col-sm-7 col-md-4 text-right">
	           			<?= number_format($sum['total_harga_pokok_keramik'],0,",",".")?>
	           		</div>
	           	</div>
	           <?php }else{ ?>
	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Harga Granit</div>
	           		<div class="col-7 col-sm-7 col-md-4 text-right">
	           			<?= number_format($sum['total_harga_jual_granit'],0,",",".")?>
	           		</div>
	           	</div>
	           	<div class="row">
	           		<div class="col-5 col-sm-5 col-md-2">Harga Keramik</div>
	           		<div class="col-7 col-sm-7 col-md-4 text-right">
	           			<?= number_format($sum['total_harga_jual_keramik'],0,",",".")?>
	           		</div>
	           	</div>
	           <?php } ?>

	           <div class="row mt-2 mb-2">
	           		<div class="col-5 col-sm-5 col-md-2">Diskon</div>
	       			<div class="col-7 col-sm-7 col-md-4 text-right">
	           			<?= number_format($data['diskon'],0,",",".") ?>
	           		</div>
	           	</div>

	           	<?php if(SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR ) { ?>

	           	<div class="row mt-2 mb-2">
	           		<label class="col-12 col-sm-12 col-md-2"></label>
           			<div class="col-12 col-sm-12 col-md-4">
	           			<button type="button" class="btn btn-sm btn-outline-secondary btn-block" data-target="#diskonModal" data-toggle="modal">Ubah Diskon</button>
	           		</div>
	           	</div>
	           <?php } ?>

	           	<div class="table-temp">
	           		<table class="table table-sm table-harga">
			          <tbody>
		           		<?php
		           		foreach ($detail as $index => $row){ ?>
						<tr style="background-color: #f4f6f9;color: #424242;">
						  <td><b><?= $index; ?></b></td>
						  <td><b>m/unit</b></td>
						  <td class="text-center"><b>@Harga</b></td>
						</tr>
		        		<?php foreach ($row as $v) { ?>
				            <tr id="<?= $v['id']?>" class="rowhargadetail">
				              <td><?= $v['nama']; ?></td>
				              <td class="jml"><?= number_format($v['jumlah'],1)?></td>

				              <?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
				              	<td>
					              	<div class="granit" style="background-color: #dee2e6;"><?= number_format($v['harga_pokok_granit'],0,",",".")?></div>
					              	<div class="keramik" style="background-color: #dcedc8;"><?= number_format($v['harga_pokok_keramik'],0,",",".")?></div>
				              	</td>
					          <?php }else{ ?>
					          	<td>
						          	<div class="granit" style="background-color: #dee2e6;"><?= number_format($v['harga_jual_granit'],0,",",".")?></div>
					              	<div class="keramik" style="background-color: #dcedc8;"><?= number_format($v['harga_jual_keramik'],0,",",".")?></div>
					          	</td>
					          	

					          <?php } ?>

				            </tr>
				          <?php } 
				          } ?>
				      </tbody>
				  </table>
	           	</div>

	           	<div class="form-group text-center mt-5">
	           		<a href="<?php echo site_url('web/pengajuan_harga/deletepengajuan/'.$data['id'])?>" class="btn btn-sm btn-add">Hapus</a>
	           	</div>
        	<!-- </form> -->
        </div>        
  	</div>

  	<!-- Update Konsumen Modal -->
	<div class="modal fade" id="updateKonsumenModal" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: auto;">
	  <div class="modal-dialog" style="max-width: 550px;">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Update Konsumen</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<form id="main_form_konsumen" action="<?php echo site_url('web/pengajuan_harga/updatekonsumen')?>" method="post">
   				<input type="hidden" name="id_pengajuan" id="id_pengajuan" value="<?= $data['id'] ?>">
   				<div class="form-group row">
	           		<label class="col-6 col-sm-6 col-md-3">Konsumen</label>
           			<div class="col-6 col-sm-6 col-md-9 text-right">
           				<input type="hidden" name="konsumen_id" id="id_konsumen" value="<?= $data['id_konsumen'] ?>">
	           			<button type="button" class="btn btn-sm btn-add" data-target="#pilihKonsumenModal" data-toggle="modal"><i class="fas fa-list nav-icon"></i></button>
	           		</div>
	           	</div>
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-3">Kode Order</label>
	           		<div class="col-12 col-sm-12 col-md-9">
	           			<input type="text" name="kode_order" id="kode_order" class="form-control form-control-sm" value="<?= $data['kode_order'] ?>" readonly>
	           		</div>
	           	</div>
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-3">Nama</label>
	           		<div class="col-12 col-sm-12 col-md-9">
	           			<input type="text" name="nama" id="nama_konsumen" class="form-control form-control-sm" value="<?= ($data['id_konsumen'] != 0) ? $data['nama'] : $data['nama_konsumen']  ?>" required>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-3">Alamat</label>
	           		<div class="col-12 col-sm-12 col-md-9">
	           			<textarea type="text" name="alamat" id="alamat_konsumen" class="form-control form-control-sm"  required><?= ($data['id_konsumen'] != 0) ? $data['alamat'] : $data['alamat_konsumen']  ?></textarea>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-3">No HP</label>
	           		<div class="col-12 col-sm-12 col-md-9">
	           			<input type="text" name="no_hp" id="no_hp_konsumen" class="form-control form-control-sm" value="<?= ($data['id_konsumen'] != 0) ? $data['no_hp'] : $data['no_hp_konsumen']  ?>" required>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<div class="col-12 col-sm-12 col-md-12 text-center">
	           			<button type="submit" class="btn btn-sm btn-add">Update</button>
	           		</div>
	           	</div>
	      	</form>
	      </div>
	    </div>
	  </div>
	</div>

  	<!-- Pilih Konsumen Modal -->
	<div class="modal fade" id="pilihKonsumenModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Pilih...</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <?php foreach ($konsumen_pengajuan as $key): ?>
				<div class="row mt-2 row-order-baru pilih-konsumen" style="cursor:pointer;color:#212529; ">
					<div class="col-7 col-sm-6 col-md-8 list-order-baru">
						<input type="hidden" class="konsumen_id" value="<?= $key['konsumen_id']  ?>">
						<label class="kode_order"><?= $key['kode_order']  ?></label>
						<p class="nama_konsumen"><?= $key['nama_konsumen']  ?></p>
						<p class="alamat_konsumen"><?= substr($key['alamat_konsumen'],0,50).'...'  ?></p>
						<p style="display: none;" class="alamat_konsumen_extend"><?= $key['alamat_konsumen']  ?></p>
						<p style="display: none;" class="no_hp_konsumen"><?= $key['no_hp_konsumen']  ?></p>
					</div>
					<div class="col-5 col-sm-6 col-md-4 list-order-baru text-right">
						<label></label>
						<p><?= $key['no_hp_konsumen']  ?></p>
						<div class="status-produksi">
							<?php if($key['status_produksi'] == 1) {
								echo '<span style="background-color: #4caf50;">Bisa Cek Ruangan</span>';
							}elseif($key['status_produksi'] == 2) {
								echo '<span style="background-color: #4caf50;">Bisa Pasang Rangka</span>';
							}elseif($key['status_produksi'] == 3) {
								echo '<span style="background-color: #f71212;">Menunggu Cor Siap</span>';
							} ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
	      </div>
	    </div>
	  </div>
	</div>

  	<div class="modal fade" id="jenisModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-sm">
          <div class="modal-content">
      		 <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Jenis Produk yang Deal</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		     </div>
             <div class="modal-body pt-0 pb-2">
             	<form id="main_form_jenis" action="<?= site_url('web/pengajuan_harga/updatejenis') ?>" method="post">
             		<input type="hidden" name="id" id="jenis_id_pengajuan">
             		<input type="hidden" name="jenis" id="jenis">
             	</form>
               <div class="row">
               	<div class="col-12 col-sm-12 col-sm-12 pb-2 pt-2 pilih-jenis" onclick="updatejenis('<?= $data['id']?>',1)" style="cursor: pointer;">Granit</div>
               	<div class="col-12 col-sm-12 col-sm-12 pb-2 pt-2 pilih-jenis" onclick="updatejenis('<?= $data['id']?>',2)" style="cursor: pointer;">Keramik</div>
               </div>
             </div>
          </div>
       </div>
    </div>

    <!-- Daftar Harga Modal -->
	<div class="modal fade" id="inputHargaModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: auto;">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Daftar Harga</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<form id="main_form_revisi_detail" action="#" method="post">
				<table class="table table-sm table-harga">
		          <tbody>
		          <?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) {
					$harga_jual = 'display: none;';		              	
					$harga_produksi = 'display: block;';		              	
				  }else{
					$harga_jual = 'display: block;';		              	
					$harga_produksi = 'display: none;';		              	

				  } ?>
		          <?php foreach ($detail_full as $index => $row){ ?>
		          <tr style="background-color:#dedede">
		              <td><b><?= $index; ?></b></td>
		              <td><b>m/unit</b></td>
		              <td><b>@Harga</b></td>
		          </tr>
		          <?php foreach ($row as $v) { ?>
		            <tr id="<?= $v['id_harga']?>" class="rowharga">
		              	<input type="hidden" name="id_pengajuan_harga" class="id_pengajuan_harga" value="<?= $data['id']?>">
		              	<input type="hidden" name="id[]" class="id_harga" value="<?= $v['id_harga']?>">
		              	<input type="hidden" name="nama_harga[]" class="nama_harga" value="<?= $v['nama']?>">
		              	<input type="hidden" name="harga_jual_granit[]" min="0" class="harga-jual-granit" value="<?= $v['harga_jual_granit']?>">
		              	<input type="hidden" name="harga_jual_keramik[]" min="0" class="harga-jual-keramik" value="<?= $v['harga_jual_keramik']?>">

		              	<input type="hidden" name="harga_pokok_granit[]" min="0" class="harga-pokok-granit" value="<?= $v['harga_pokok_granit']?>">
		              	<input type="hidden" name="harga_pokok_keramik[]" min="0" class="harga-pokok-keramik" value="<?= $v['harga_pokok_keramik']?>">
		              <td>
		              	<?= $v['nama']; ?>
		              </td>
		              <td>
		              	<input type="text" name="jumlah[]" min="0" class="jml no-modal allownumericwithdecimal" value="<?= (!empty($v['jumlah'])) ? $v['jumlah'] : ''; ?>">
		              </td>
		              <td style="padding: 5px 0;">
		              	<div class="granit-jual text-right" style="<?= $harga_jual ?>"><?= number_format($v['harga_jual_granit']*$v['jumlah'],0,",",".")?></div>
		              	<div class="keramik-jual text-right" style="<?= $harga_jual ?>"><?= number_format($v['harga_jual_keramik']*$v['jumlah'],0,",",".")?></div>

		              	<div class="granit-pokok text-right" style="<?= $harga_produksi ?>"><?= number_format($v['harga_pokok_granit']*$v['jumlah'],0,",",".")?></div>
		              	<div class="keramik-pokok text-right" style="<?= $harga_produksi ?>"><?= number_format($v['harga_pokok_keramik']*$v['jumlah'],0,",",".")?></div>
		              </td>
		            </tr>
		          <?php } 
		          } ?>
		          </tbody>
		        </table>

		        <div class="form-group text-center mt-5">
		          <?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
	           		<button type="button" id="simpanajukanhargaproduksi" dataid="3" class="btn btn-sm btn-add">Simpan & Ajukan</button>

		          <?php }else{ ?>

	           		<button type="button" id="simpandaftarharga" dataid="1" class="btn btn-sm btn-outline-secondary">Simpan</button>
	           		<?php if($data['status'] == 1 || $data['status'] == 3) { ?>

	           			<button type="button" id="simpanajukandaftarharga" dataid="2" class="btn btn-sm btn-add">Simpan & Ajukan</button>
		          	<?php } ?>
		          <?php } ?>

	           	</div>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Update Harga Modal -->
	<div class="modal fade" id="updateHargaModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="hargaModalLabel"></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) {
				$row_harga_edit = 'display: flex;';		              	
				$harga_jual_edit = 'display: flex;';		              	
				$harga_produksi_edit = 'display: flex;';
				$harga_readonly = '';
			}elseif (SessionManagerWeb::getRole() == Role::PRODUKSI) {
				$harga_produksi_edit = 'display: none;';
				$row_harga_edit = 'display: flex;';		              	
			}else{
				$row_harga_edit = 'display: none;';		              	
				$harga_jual_edit = 'display: none;';		              	
				$harga_produksi_edit = 'display: flex;';
				$harga_readonly = 'readonly';	              	

			} ?>
	      	<input type="hidden" id="id_harga">
			<div class="form-group row">
           		<div class="col-12 col-sm-12 col-md-4">Jumlah</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="text" name="jml_edit" id="jml_edit" min="0" class="form-control form-control-sm allownumericwithdecimal" required>
           		</div>
           	</div>	
           	<h5 style="<?= $harga_produksi_edit ?>">Harga Konsumen</h5>
           	<div class="form-group row" style="<?= $harga_produksi_edit ?>">
           		<div class="col-12 col-sm-12 col-md-4 pl-3">Harga Granit</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="number" name="harga_jual_granit" id="harga_jual_granit" class="form-control form-control-sm" required <?= $harga_readonly ?> >
           		</div>
           	</div>
           	<div class="form-group row" style="<?= $harga_produksi_edit ?>">
           		<div class="col-12 col-sm-12 col-md-4 pl-3">Harga Keramik</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="number" name="harga_jual_keramik" id="harga_jual_keramik" class="form-control form-control-sm" required <?= $harga_readonly ?>>
           		</div>
           	</div>

           	<h5 style="<?= $row_harga_edit ?>">Harga Produksi</h5>
           	<div class="form-group row" style="<?= $row_harga_edit ?>">
           		<div class="col-12 col-sm-12 col-md-4 pl-3">Harga Granit</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="number" name="harga_pokok_granit" id="harga_pokok_granit" class="form-control form-control-sm" required>
           		</div>
           	</div>
           	<div class="form-group row" style="<?= $row_harga_edit ?>">
           		<div class="col-12 col-sm-12 col-md-4 pl-3">Harga Keramik</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="number" name="harga_pokok_keramik" id="harga_pokok_keramik" class="form-control form-control-sm" required>
           		</div>
           	</div>

           	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) { ?>
           	<table class="table table-sm">
           		<tr>
           			<td></td>
           			<td class="text-right">Hrg Produksi</td>
           			<td class="text-right">Hrg Konsumen</td>
           		</tr>
           		<tr class="granit">
           			<td>Granit</td>
           			<td class="hrg-produksi text-right"></td>
           			<td class="hrg-konsumen text-right"></td>
           		</tr>
           		<tr class="keramik">
           			<td>Keramik</td>
           			<td class="hrg-produksi text-right"></td>
           			<td class="hrg-konsumen text-right"></td>
           		</tr>
           	</table>
           <?php } ?>

		        <div class="form-group text-center mt-5">
	           		<button type="button" id="removeharga" class="btn btn-sm btn-outline-secondary">Hapus</button>
	           		<button type="button" id="simpanharga" class="btn btn-sm btn-add">Simpan</button>
	           	</div>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Diskon Modal-->
	<div class="modal fade" id="diskonModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-sm">
          <div class="modal-content">
          	<div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Ubah Diskon</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		     </div>
             <div class="modal-body">
               <form action="<?php echo site_url('web/pengajuan_harga/ubahDiskon');?>" method="post">
               	<input type="hidden" name="id_pengajuan_harga" value="<?= $data['id'] ?>">
               	<div class="form-group row">
	           		<div class="col-12 col-sm-12 col-md-12">Diskon</div>
	       			<div class="col-12 col-sm-12 col-md-12 text-center">
	           			<input type="number" name="diskon" class="form-control" min="0" value="<?= $data['diskon'] ?>"> 
	           		</div>
	           	</div>
	           	<div class="form-group text-center mt-4">
	           		<button type="button" class="btn btn-sm btn-outline-secondary">Tutup</button>
	           		<button type="submit" class="btn btn-sm btn-add">Ok</button>
	           	</div>
               </form>
             </div>
          </div>
       </div>
    </div>

	<!-- Struk Check Modal -->
	<div class="modal fade" id="strukCheckModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: auto;">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<div class="row">
		    	<span class="share-icon-check" id="granit"><i class="fas fa-share-alt"></i></span>
			    <div class="col-12 col-sm-12 col-md-12 swithproduksi-div">
			    	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>		              			
					<div style="position: absolute;z-index: 1;top: 150px;left: 15px;">
						<label style="padding-right: 5px;color: #dc3545;">Harga Produksi</label>
						<input class="check-harga-produksi" type="checkbox" data-toggle="toggle" data-size="xs" style="float:right;">
				    </div>
					<?php } ?>
			    </div>
			    <div class="col-12 col-sm-12 col-md-12" id="canvascheck" style="background-color: #fff;">
			    	<div class="logo-nasatech">
			    		<label>Nasa<b>tech</b></label>
			    		<br>
			    		<span>Kitchenset</span>
			    	</div>
			    	<table style="margin-bottom: 15px;">
			    		<input type="hidden" id="idpengajuan" value="<?= $data['id']?>">
			    		<tr>
			    			<td width="100px"><label>Kode Order</label></td>
			    			<td><label class="text-red"><?= $data['kode_order']?></label></td>
			    		</tr>
			    		<tr>
			    			<td style="font-weight: bold;">Nama</td>
			    			<td><?= ($data['id_konsumen'] != 0) ? $data['nama'] : $data['nama_konsumen']  ?></td>
			    		</tr>
			    		<tr>
			    			<td style="font-weight: bold;">Alamat</td>
			    			<td><?= ($data['id_konsumen'] != 0) ? $data['alamat'] : $data['alamat_konsumen']  ?></td>
			    		</tr>
			    		<tr>
			    			<td style="font-weight: bold;">No HP</td>
			    			<td><?= ($data['id_konsumen'] != 0) ? $data['no_hp'] : $data['no_hp_konsumen']  ?></td>
			    		</tr>
			    	</table>
				    <div class="col-12 col-sm-12 col-md-12">
				    	<div style="text-align: center;">
					    	<button type="button" class="btn btn-sm btn-granit" style="margin:10px 0;"><b>Granit</b></button>		
				    	</div>
			    	</div>
			    	<table class="table table-sm table-harga">
			    		<thead>
			    			<th>No</th>
			    			<th>Nama</th>
			    			<th class="th-jml">Jml</th>
			    			<th class="th-harga" style="background-color: #dee2e6;">@Harga</th>
			    			<th class="th-subtotal" style="background-color: #dee2e6;">Subtotal</th>
			    		</thead>
			          <tbody>
			          <?php 
			          $no = 1;
			          $totalgranitprod = 0;
			          $totalkeramikprod = 0;
			          $totalgranit = 0;
			          $totalkeramik = 0;
			          foreach ($detail as $index => $row){ ?>
							<tr style="background-color:#424242;color: #fff;">
							  <td colspan="5"><b><?= $index; ?></b></td>
							</tr>
			        		<?php foreach ($row as $v) { 
			        			$totalgranitprod += $v['harga_pokok_granit']*$v['jumlah'];
			        			$totalkeramikprod += $v['harga_pokok_keramik']*$v['jumlah'];
			
			        			$totalgranit += $v['harga_jual_granit']*$v['jumlah'];
			        			$totalkeramik += $v['harga_jual_keramik']*$v['jumlah'];
			        			
			        			if ($v['harga_pokok_granit_revisi'] == true) {
			        				$granit_red = 'color: #f71212;';
			        			}else{
			        				$granit_red = '';
			        			}

			        			if ($v['harga_pokok_keramik_revisi'] == true) {
			        				$keramik_red = 'color: #f71212;';
			        			}else{
			        				$keramik_red = '';
			        			}
			        			?>
					            <tr id="<?= $v['id_pengajuan']?>" class="rowharga">
					              <td><?= $no++; ?></td>
					              <td><?= $v['nama']; ?></td>
					              <td class="jml"><?= number_format($v['jumlah'],1)?></td>

					              <?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
					              	<td class="granit" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_pokok_granit'],0,",",".")?></td>
					              	<td class="keramik" style="display: none;background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_pokok_keramik'],0,",",".")?></td>

					              	<td class="granit" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_pokok_granit']*$v['jumlah'],0,",",".")?></td>
					              	<td class="keramik" style="display: none;background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_pokok_keramik']*$v['jumlah'],0,",",".")?></td>
						          <?php }else{ ?>
						          	<td class="granit hrg-kons-check" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_jual_granit'],0,",",".")?></td>
					              	<td class="keramik hrg-kons-check" style="display: none;background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_jual_keramik'],0,",",".")?></td>

					              	<td class="granit hrg-kons-check" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_jual_granit']*$v['jumlah'],0,",",".")?></td>
					              	<td class="keramik hrg-kons-check" style="display: none;background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_jual_keramik']*$v['jumlah'],0,",",".")?></td>

						              <?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
						              	<td class="granit hrg-prod-check hide-hrg-prod" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_pokok_granit'],0,",",".")?></td>
						              	<td class="keramik hrg-prod-check hide-hrg-prod" style="display: none;background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_pokok_keramik'],0,",",".")?></td>

						              	<td class="granit hrg-prod-check hide-hrg-prod" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_pokok_granit']*$v['jumlah'],0,",",".")?></td>
						              	<td class="keramik hrg-prod-check hide-hrg-prod" style="display: none;background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_pokok_keramik']*$v['jumlah'],0,",",".")?></td>
							          <?php } ?>
						          <?php } ?>

					            </tr>
			          <?php } 
			          } ?>
			          <tr>
			          	<td colspan="3" style="text-align: right"><b>Total</b></td>
          				<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
          					<td colspan="2" class="granit" style="text-align: right"><b><?= number_format($totalgranitprod,0,",",".") ?></b></td>
				          	<td colspan="2" class="keramik" style="text-align: right;display: none;"><b><?= number_format($totalkeramikprod,0,",",".") ?></b></td>
			        	<?php }else{ ?>
				          	<td colspan="2" class="granit hrg-kons-check" style="text-align: right"><b><?= number_format($totalgranit,0,",",".") ?></b></td>
				          	<td colspan="2" class="keramik hrg-kons-check" style="text-align: right;display: none;"><b><?= number_format($totalkeramik,0,",",".") ?></b></td>
				          	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>		              			
					        	<td colspan="2" class="granit hrg-prod-check hide-hrg-prod" style="text-align: right"><b><?= number_format($totalgranitprod,0,",",".") ?></b></td>
					          	<td colspan="2" class="keramik hrg-prod-check hide-hrg-prod" style="text-align: right;display: none;"><b><?= number_format($totalkeramikprod,0,",",".") ?></b></td>
					        <?php } ?>
		        		<?php } ?>

			          </tr>

			          <?php if($data['diskon'] > 0) { ?>
			          <tr>
			          	<td colspan="3" style="text-align: right"><b>Diskon</b></td>
          				<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
          					<td colspan="2" class="granit" style="text-align: right"><b>0</b></td>
				          	<td colspan="2" class="keramik" style="text-align: right;display: none;"><b>0</b></td>
			        	<?php }else{ ?>

				          	<td colspan="2" class="granit hrg-kons-check" style="text-align: right"><b><?= number_format($data['diskon'],0,",",".") ?></b></td>
				          	<td colspan="2" class="keramik hrg-kons-check" style="text-align: right;display: none;"><b><?= number_format($data['diskon'],0,",",".") ?></b></td>

				          	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>		              			<td colspan="2" class="granit hrg-prod-check hide-hrg-prod" style="text-align: right"><b>0</b></td>
					          	<td colspan="2" class="keramik hrg-prod-check hide-hrg-prod" style="text-align: right;display: none;"><b>0</b></td>
		        			<?php } ?>
		        		<?php } ?>

			          </tr>
			          <tr>
			          	<td colspan="3" style="text-align: right"><b>Grand Total</b></td>
          				<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
          					<td colspan="2" class="granit" style="text-align: right"><b><?= number_format($totalgranitprod-0,0,",",".") ?></b></td>
				          	<td colspan="2" class="keramik" style="text-align: right;display: none;"><b><?= number_format($totalkeramikprod-0,0,",",".") ?></b></td>
			        	<?php }else{ ?>

			          		<td colspan="2" class="granit hrg-kons-check" style="text-align: right"><b><?= number_format($totalgranit-$data['diskon'],0,",",".") ?></b></td>
			          		<td colspan="2" class="keramik hrg-kons-check" style="text-align: right;display: none;"><b><?= number_format($totalkeramik-$data['diskon'],0,",",".") ?></b></td>

				          	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
			          			<td colspan="2" class="granit hrg-prod-check hide-hrg-prod" style="text-align: right"><b><?= number_format($totalgranitprod-0,0,",",".") ?></b></td>
				          		<td colspan="2" class="keramik hrg-prod-check hide-hrg-prod" style="text-align: right;display: none;"><b><?= number_format($totalkeramikprod-0,0,",",".") ?></b></td>
		        			<?php } ?>
		        		<?php } ?>

			          </tr>
			          <?php }?>
			          </tbody>
			        </table>
			    </div>

			</div>

	      </div>
	    </div>
	  </div>
	</div>

	<!-- Struk Modal -->
	<div class="modal fade" id="strukModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: auto;">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="row">
		    	<span class="share-icon" id="granit"><i class="fas fa-share-alt"></i></span>
			    <div class="col-12 col-sm-12 col-md-12 swithproduksi-div">
			    	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>		              			
					<div style="position: absolute;z-index: 1;top: 150px;left: 15px;">
						<label style="padding-right: 5px;color: #dc3545;">Harga Produksi</label>
						<input class="check-harga-produksi-full" type="checkbox" data-toggle="toggle" data-size="xs" style="float:right;">
				    </div>
					<?php } ?>
			    </div>
			    <div class="col-12 col-sm-12 col-md-12" id="canvas" style="background-color: #fff;">
			    	<div class="logo-nasatech">
			    		<label>Nasa<b>tech</b></label>
			    		<br>
			    		<span>Kitchenset</span>
			    	</div>
			    	<table style="margin-bottom: 15px;">
			    		<input type="hidden" id="idpengajuanfull" value="<?= $data['id']?>">
			    		<tr>
			    			<td width="100px"><label>Kode Order</label></td>
			    			<td><label class="text-red"><?= $data['kode_order']?></label></td>
			    		</tr>
			    		<tr>
			    			<td style="font-weight: bold;">Nama</td>
			    			<td><?= ($data['id_konsumen'] != 0) ? $data['nama'] : $data['nama_konsumen']  ?></td>
			    		</tr>
			    		<tr>
			    			<td style="font-weight: bold;">Alamat</td>
			    			<td><?= ($data['id_konsumen'] != 0) ? $data['alamat'] : $data['alamat_konsumen']  ?></td>
			    		</tr>
			    		<tr>
			    			<td style="font-weight: bold;">No HP</td>
			    			<td><?= ($data['id_konsumen'] != 0) ? $data['no_hp'] : $data['no_hp_konsumen']  ?></td>
			    		</tr>
			    	</table>
				    <div class="col-12 col-sm-12 col-md-12">
				    	<div style="text-align: center;">
					    	<button type="button" class="btn btn-sm btn-granit" style="margin:10px 0;"><b>Granit</b></button>		
				    	</div>
			    	</div>
			    	<table class="table table-sm table-harga">
			    		<thead>
			    			<th>No</th>
			    			<th>Nama</th>
			    			<th class="th-jml">Jml</th>
			    			<th class="th-harga" style="background-color: #dee2e6;">@Harga</th>
			    			<th class="th-subtotal" style="background-color: #dee2e6;">Subtotal</th>
			    		</thead>
			          <tbody>
			          <?php 
			          $no = 1;
			          $totalgranitprod = 0;
			          $totalkeramikprod = 0;
			          $totalgranit = 0;
			          $totalkeramik = 0;
			          foreach ($detail_full as $index => $row){ ?>
							<tr style="background-color:#424242;color: #fff;">
							  <td colspan="5"><b><?= $index; ?></b></td>
							</tr>
			        		<?php foreach ($row as $v) { 
			        			$totalgranitprod += $v['harga_pokok_granit']*$v['jumlah'];
			        			$totalkeramikprod += $v['harga_pokok_keramik']*$v['jumlah'];

			        			$totalgranit += $v['harga_jual_granit']*$v['jumlah'];
			        			$totalkeramik += $v['harga_jual_keramik']*$v['jumlah'];

			        			if ($v['harga_pokok_granit_revisi'] == true) {
			        				$granit_red = 'color: #f71212;';
			        			}else{
			        				$granit_red = '';
			        			}

			        			if ($v['harga_pokok_keramik_revisi'] == true) {
			        				$keramik_red = 'color: #f71212;';
			        			}else{
			        				$keramik_red = '';
			        			}
			        			?>
					            <tr id="<?= $v['id_pengajuan']?>" class="rowharga">
					              <td><?= $no++; ?></td>
					              <td><?= $v['nama']; ?></td>
					              <td class="jml"><?= number_format($v['jumlah'],1)?></td>

					              <?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
					              	<td class="granit" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_pokok_granit'],0,",",".")?></td>
					              	<td class="keramik" style="display: none;background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_pokok_keramik'],0,",",".")?></td>

					              	<td class="granit" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_pokok_granit']*$v['jumlah'],0,",",".")?></td>
					              	<td class="keramik" style="display: none;background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_pokok_keramik']*$v['jumlah'],0,",",".")?></td>
						          <?php }else{ ?>
						          	<td class="granit hrg-kons" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_jual_granit'],0,",",".")?></td>
					              	<td class="keramik hrg-kons" style="display: none;background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_jual_keramik'],0,",",".")?></td>

					              	<td class="granit hrg-kons" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_jual_granit']*$v['jumlah'],0,",",".")?></td>
					              	<td class="keramik hrg-kons" style="display: none;background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_jual_keramik']*$v['jumlah'],0,",",".")?></td>

					              	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
						              	<td class="granit hrg-prod hide-hrg-prod" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_pokok_granit'],0,",",".")?></td>
						              	<td class="keramik hrg-prod hide-hrg-prod" style="display: none;background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_pokok_keramik'],0,",",".")?></td>

						              	<td class="granit hrg-prod hide-hrg-prod" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_pokok_granit']*$v['jumlah'],0,",",".")?></td>
						              	<td class="keramik hrg-prod hide-hrg-prod" style="display: none;background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_pokok_keramik']*$v['jumlah'],0,",",".")?></td>
							          <?php } ?>
						          <?php } ?>

					            </tr>
			          <?php } 
			          } ?>
			          <tr>
			          	<td colspan="3" style="text-align: right"><b>Total</b></td>
			          	<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
          					<td colspan="2" class="granit" style="text-align: right"><b><?= number_format($totalgranitprod,0,",",".") ?></b></td>
				          	<td colspan="2" class="keramik" style="text-align: right;display: none;"><b><?= number_format($totalkeramikprod,0,",",".") ?></b></td>
			        	<?php }else{ ?>
				          	<td colspan="2" class="granit hrg-kons" style="text-align: right"><b><?= number_format($totalgranit,0,",",".") ?></b></td>
				          	<td colspan="2" class="keramik hrg-kons" style="text-align: right;display: none;"><b><?= number_format($totalkeramik,0,",",".") ?></b></td>
				          	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>		              			
					        	<td colspan="2" class="granit hrg-prod hide-hrg-prod" style="text-align: right"><b><?= number_format($totalgranitprod,0,",",".") ?></b></td>
					          	<td colspan="2" class="keramik hrg-prod hide-hrg-prod" style="text-align: right;display: none;"><b><?= number_format($totalkeramikprod,0,",",".") ?></b></td>
					        <?php } ?>
		        		<?php } ?>
			          </tr>

			          <?php if($data['diskon'] > 0) { ?>
			          <tr>
			          	<td colspan="3" style="text-align: right"><b>Diskon</b></td>
			          	<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
          					<td colspan="2" class="granit" style="text-align: right"><b>0</b></td>
				          	<td colspan="2" class="keramik" style="text-align: right;display: none;"><b>0</b></td>
			        	<?php }else{ ?>

				          	<td colspan="2" class="granit hrg-kons" style="text-align: right"><b><?= number_format($data['diskon'],0,",",".") ?></b></td>
				          	<td colspan="2" class="keramik hrg-kons" style="text-align: right;display: none;"><b><?= number_format($data['diskon'],0,",",".") ?></b></td>

				          	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>		              			<td colspan="2" class="granit hrg-prod hide-hrg-prod" style="text-align: right"><b>0</b></td>
					          	<td colspan="2" class="keramik hrg-prod hide-hrg-prod" style="text-align: right;display: none;"><b>0</b></td>
		        			<?php } ?>
		        		<?php } ?>

			          </tr>
			          <tr>
			          	<td colspan="3" style="text-align: right"><b>Grand Total</b></td>
			          	<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
          					<td colspan="2" class="granit" style="text-align: right"><b><?= number_format($totalgranitprod-0,0,",",".") ?></b></td>
				          	<td colspan="2" class="keramik" style="text-align: right;display: none;"><b><?= number_format($totalkeramikprod-0,0,",",".") ?></b></td>
			        	<?php }else{ ?>

			          		<td colspan="2" class="granit hrg-kons" style="text-align: right"><b><?= number_format($totalgranit-$data['diskon'],0,",",".") ?></b></td>
			          		<td colspan="2" class="keramik hrg-kons" style="text-align: right;display: none;"><b><?= number_format($totalkeramik-$data['diskon'],0,",",".") ?></b></td>

				          	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
			          			<td colspan="2" class="granit hrg-prod hide-hrg-prod" style="text-align: right"><b><?= number_format($totalgranitprod-0,0,",",".") ?></b></td>
				          		<td colspan="2" class="keramik hrg-prod hide-hrg-prod" style="text-align: right;display: none;"><b><?= number_format($totalkeramikprod-0,0,",",".") ?></b></td>
		        			<?php } ?>
		        		<?php } ?>
			          </tr>
			          <?php }?>
			          </tbody>
			        </table>
			    </div>

			</div>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Download Modal -->
	<div class="modal fade" id="downloadStrukModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h5 class="modal-title" id="exampleModalLabel"></h5>
	          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">×</span>
	          </button>
	        </div>
	        <div class="modal-body">Apakah anda ingin mendownload / preview pdf stuk ini ?</div>
	        <div class="modal-footer">
	          <!-- <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button> -->
          	  <button type="button" id="preview-check-granit" class="btn btn-sm btn-info preview-struk-check" tipeharga="kons" style="display: none;"><i class="fas fa-eye"></i> Preview Pdf</button>
          	  <button type="button" id="preview-granit" class="btn btn-sm btn-info preview-struk" tipeharga="kons" style="display: none;"><i class="fas fa-eye"></i> Preview Pdf</button>
	          <a class="btn btn-sm btn-add download-struk" href="#"><i class="fas fa-download"></i> Download</a>
	        </div>
	      </div>
	    </div>
	</div>

<script src="<?php echo base_url()?>assets/web/js/html2canvas.js"></script>

<script>
	function updatejenis(id,jenis){
		$('#jenis_id_pengajuan').val(id);
		$('#jenis').val(jenis);
		$('#main_form_jenis').submit();
	}

	function getcheckstruk(){
		$('#strukCheckModal').modal('show');
	}

	function getstruk(){
		$('#strukModal').modal('show');
	}

	$(document).ready(function(){

		$(document).on('click','.pilih-konsumen', function(){
			var kode_order = $(this).find('.kode_order').html();
			var konsumen_id = $(this).find('.konsumen_id').val();
			var nama_konsumen = $(this).find('.nama_konsumen').html();
			var alamat_konsumen = $(this).find('.alamat_konsumen_extend').html();
			var no_hp_konsumen = $(this).find('.no_hp_konsumen').html();

			$("#kode_order").val(kode_order);
			$("#id_konsumen").val(konsumen_id);
			$("#nama_konsumen").val(nama_konsumen);
			$("#alamat_konsumen").val(alamat_konsumen);
			$("#no_hp_konsumen").val(no_hp_konsumen);

			// $('#main_form_konsumen').submit();
			$('#pilihKonsumenModal').modal('hide');
		})

		$(document).on('click','.btn-granit', function(){
			// $(this).removeClass('btn-granit');
			// var btnclass = $(this).addClass('btn-keramik');

			var btnclass = $('.btn-granit').addClass('btn-keramik');
			$('.btn-granit').removeClass('btn-granit');

			$('#strukCheckModal .btn-keramik').html('<b>Keramik</b>');

			$('#strukCheckModal #granit').attr('id','keramik');
			$('#preview-check-granit').attr('id','preview-check-keramik');
			$('#strukCheckModal .th-harga').css({'background-color':'#dcedc8'});
			$('#strukCheckModal .th-subtotal').css({'background-color':'#dcedc8'});
			$('#strukCheckModal .granit').hide();
			$('#strukCheckModal .keramik').show();
		})

		$(document).on('click','.btn-keramik', function(){
			// $(this).removeClass('btn-keramik');
			// var btnclass = $(this).addClass('btn-granit');

			var btnclass = $('.btn-keramik').addClass('btn-granit');
			$('.btn-keramik').removeClass('btn-keramik');

			$('#strukCheckModal .btn-granit').html('<b>Granit</b>');

			$('#strukCheckModal #keramik').attr('id','granit');
			$('#preview-check-keramik').attr('id','preview-check-granit');
			$('#strukCheckModal .th-harga').css({'background-color':'#dee2e6'});
			$('#strukCheckModal .th-subtotal').css({'background-color':'#dee2e6'});
			$('#strukCheckModal .keramik').hide();
			$('#strukCheckModal .granit').show();
		})

		$('.preview-struk-check').click(function(){
			var id = $('#idpengajuan').val();
			var jenis = $(this).attr('id');
			var jenis = jenis.replace("preview-check-", "");
			var tipeharga = $(this).attr('tipeharga');
			var url = '<?= site_url('web/pengajuan_harga/previewstrukcheckpdf')?>/'+id+'/'+jenis+'/'+tipeharga;
		    window.open(url, '_blank');
		})

		$('.share-icon-check').click(function(){
			$('.preview-struk').hide();
			$('.preview-struk-check').show();

			var jenis = $(this).attr('id');
            html2canvas($('#canvascheck'), {
            	height: 2000,
	        onrendered: function (canvas) {

	                var imgageData =  
		            canvas.toDataURL("image/png",1); 
		           
		            var newData = imgageData.replace( 
		            /^data:image\/png/, "data:application/octet-stream"); 
		           
		            $(".download-struk").attr( 
		            "download", jenis+".png").attr( 
		            "href", newData);

	                $('#downloadStrukModal').modal('show');

	            }
	        });
		})

		$(".download-struk").click(function(){
            $('#downloadStrukModal').modal('hide');			
		})

		$(document).on('click','.btn-granit', function(){
			$(this).removeClass('btn-granit');
			var btnclass = $(this).addClass('btn-keramik');

			$('#strukModal .btn-keramik').html('<b>Keramik</b>');

			$('#strukModal #granit').attr('id','keramik');
			$('#preview-granit').attr('id','preview-keramik');
			$('#strukModal .th-harga').css({'background-color':'#dcedc8'});
			$('#strukModal .th-subtotal').css({'background-color':'#dcedc8'});
			$('#strukModal .granit').hide();
			$('#strukModal .keramik').show();
		})

		$(document).on('click','.btn-keramik', function(){
			$(this).removeClass('btn-keramik');
			var btnclass = $(this).addClass('btn-granit');

			$('#strukModal .btn-granit').html('<b>Granit</b>');

			$('#strukModal #keramik').attr('id','granit');
			$('#preview-keramik').attr('id','preview-granit');
			$('#strukModal .th-harga').css({'background-color':'#dee2e6'});
			$('#strukModal .th-subtotal').css({'background-color':'#dee2e6'});
			$('#strukModal .keramik').hide();
			$('#strukModal .granit').show();
		})

		$('.preview-struk').click(function(){
			var id = $('#idpengajuanfull').val();
			var jenis = $(this).attr('id');
			var jenis = jenis.replace("preview-", "");
			var tipeharga = $(this).attr('tipeharga');
			var url = '<?= site_url('web/pengajuan_harga/previewstrukpdf')?>/'+id+'/'+jenis+'/'+tipeharga;
		    window.open(url, '_blank');
		})

		$('.share-icon').click(function(){
			$('.preview-struk-check').hide();
			$('.preview-struk').show();

			var jenis = $(this).attr('id');
            html2canvas($('#canvas'), {
            	height: 2000,
	        onrendered: function (canvas) {

	                var imgageData =  
		            canvas.toDataURL("image/png",1); 
		           
		            var newData = imgageData.replace( 
		            /^data:image\/png/, "data:application/octet-stream"); 
		           
		            $(".download-struk").attr( 
		            "download", jenis+".png").attr( 
		            "href", newData);

	                $('#downloadStrukModal').modal('show');

	            }
	        });
		})

		$(document).on('keyup mouseup','.jml', function(){
			var id = $(this).parents('.rowharga').attr('id');
			var jml = $(this).val();
			var jml = jml.replace(/(\.\d{1})\d+/g, '$1');
			
			$.ajax({
				type: "post",
				url: "<?= site_url('web/pengajuan_harga/ajaxgetharga') ?>",
				data: {id: id},
				dataType: "json",
				success: function(res){
			    	// console.log(jml);
			    	var hjg = $('#'+id).find('.harga-jual-granit').val();
					var hjk = $('#'+id).find('.harga-jual-keramik').val();
					var hpg = $('#'+id).find('.harga-pokok-granit').val();
					var hpk = $('#'+id).find('.harga-pokok-keramik').val();
			    	
			    	if (hjg == '0' || hjk == '0' || hpg == '0' || hpk == '0') {
				    	var harga_jual_granit = res.harga_jual_granit;
				    	var harga_jual_keramik = res.harga_jual_keramik;

				    	var harga_pokok_granit = res.harga_pokok_granit;
				    	var harga_pokok_keramik = res.harga_pokok_keramik;
			    	}else{
			    		var harga_jual_granit = parseInt(hjg);
				    	var harga_jual_keramik = parseInt(hjk);

				    	var harga_pokok_granit = parseInt(hpg);
				    	var harga_pokok_keramik = parseInt(hpk);
			    	}


			    	$('#'+id).find('.jml').val(jml);

			    	$('#'+id).find('.harga-jual-granit').val(harga_jual_granit);
					$('#'+id).find('.harga-jual-keramik').val(harga_jual_keramik);

					$('#'+id).find('.granit-jual').html(formatRupiah(Math.round(harga_jual_granit*jml)));
					$('#'+id).find('.keramik-jual').html(formatRupiah(Math.round(harga_jual_keramik*jml)));

					$('#'+id).find('.harga-pokok-granit').val(harga_pokok_granit);
					$('#'+id).find('.harga-pokok-keramik').val(harga_pokok_keramik);

					$('#'+id).find('.granit-pokok').html(formatRupiah(Math.round(harga_pokok_granit*jml)));
					$('#'+id).find('.keramik-pokok').html(formatRupiah(Math.round(harga_pokok_keramik*jml)));

					// updateHargaArray(id,jml,harga_jual_granit,harga_jual_keramik,harga_pokok_granit,harga_pokok_keramik,hargaArray);
					// console.log(hargaArray);

		    	},
			});
		})

		$(document).on('click','.no-modal', function(e){
		    e.stopPropagation();
		})

		$(document).on('click','.rowharga', function(){

			var id = $(this).attr('id');
			var jml = $(this).find('.jml').val();

			if (jml == 0) {
				jml = 1;
			}

			<?php if(SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DESAIN){ ?>
			if (id == '26') {
				$('#updateHargaModal #harga_jual_granit').attr('readonly', false);
				$('#updateHargaModal #harga_jual_keramik').attr('readonly', false);
			}else{
				$('#updateHargaModal #harga_jual_granit').attr('readonly', true);
				$('#updateHargaModal #harga_jual_keramik').attr('readonly', true);
			}
			<?php } ?>

			$.ajax({
				type: "post",
				url: "<?= site_url('web/pengajuan_harga/ajaxgetharga') ?>",
				data: {id: id},
				dataType: "json",
				success: function(res){
			       // console.log(jml);
			  //   	var nama_harga = $(this).find('.nama_harga').val();

			  		var hjg = $('#inputHargaModal #'+id).find('.harga-jual-granit').val();
					var hjk = $('#inputHargaModal #'+id).find('.harga-jual-keramik').val();
					var hpg = $('#inputHargaModal #'+id).find('.harga-pokok-granit').val();
					var hpk = $('#inputHargaModal #'+id).find('.harga-pokok-keramik').val();
			    	
			    	if (hjg == '0' || hjk == '0' || hpg == '0' || hpk == '0') {
				    	var harga_jual_granit = res.harga_jual_granit;
				    	var harga_jual_keramik = res.harga_jual_keramik;

				    	var harga_pokok_granit = res.harga_pokok_granit;
				    	var harga_pokok_keramik = res.harga_pokok_keramik;
			    	}else{
			    		var harga_jual_granit = parseInt(hjg);
				    	var harga_jual_keramik = parseInt(hjk);

				    	var harga_pokok_granit = parseInt(hpg);
				    	var harga_pokok_keramik = parseInt(hpk);
			    	}

					$('#updateHargaModal #hargaModalLabel').html(res.nama);
					$('#updateHargaModal #id_harga').val(id);
					$('#updateHargaModal #jml_edit').val(jml);

					$('#updateHargaModal #harga_jual_granit').val(harga_jual_granit);
					$('#updateHargaModal #harga_jual_keramik').val(harga_jual_keramik);
					$('#updateHargaModal #harga_pokok_granit').val(harga_pokok_granit);
					$('#updateHargaModal #harga_pokok_keramik').val(harga_pokok_keramik);

					$('#updateHargaModal .granit .hrg-produksi').html(formatRupiah(res.harga_pokok_granit));
					$('#updateHargaModal .granit .hrg-konsumen').html(formatRupiah(res.harga_jual_granit));
					$('#updateHargaModal .keramik .hrg-produksi').html(formatRupiah(res.harga_pokok_keramik));
					$('#updateHargaModal .keramik .hrg-konsumen').html(formatRupiah(res.harga_jual_keramik));

					$('#updateHargaModal').modal('show');
			        	
			    },
			});
		})

		$(document).on('click','#simpanharga', function(){
			var id = $('#updateHargaModal #id_harga').val();
			var jml = $('#updateHargaModal #jml_edit').val();

			var harga_jual_granit = $('#updateHargaModal #harga_jual_granit').val();
			var harga_jual_keramik = $('#updateHargaModal #harga_jual_keramik').val();
			var harga_pokok_granit = $('#updateHargaModal #harga_pokok_granit').val();
			var harga_pokok_keramik = $('#updateHargaModal #harga_pokok_keramik').val();

			$('#'+id).find('.jml').val(jml);

			$('#'+id).find('.harga-jual-granit').val(harga_jual_granit);
			$('#'+id).find('.harga-jual-keramik').val(harga_jual_keramik);

			$('#'+id).find('.granit-jual').html(formatRupiah(Math.round(harga_jual_granit*jml)));
			$('#'+id).find('.keramik-jual').html(formatRupiah(Math.round(harga_jual_keramik*jml)));

			$('#'+id).find('.harga-pokok-granit').val(harga_pokok_granit);
			$('#'+id).find('.harga-pokok-keramik').val(harga_pokok_keramik);

			$('#'+id).find('.granit-pokok').html(formatRupiah(Math.round(harga_pokok_granit*jml)));
			$('#'+id).find('.keramik-pokok').html(formatRupiah(Math.round(harga_pokok_keramik*jml)));

			// updateHargaArray(id,jml,harga_jual_granit,harga_jual_keramik,harga_pokok_granit,harga_pokok_keramik,hargaArray);

			$('#updateHargaModal').modal('hide');

		})

		$(document).on('click','#removeharga', function(){
			var id = $('#updateHargaModal #id_harga').val();

			$('#'+id).find('.jml').val(0);
			$('#'+id).find('.harga-jual-granit').val(0);
			$('#'+id).find('.harga-jual-keramik').val(0);

			$('#'+id).find('.granit-jual').html(0);
			$('#'+id).find('.keramik-jual').html(0);

			$('#'+id).find('.harga-pokok-granit').val(0);
			$('#'+id).find('.harga-pokok-keramik').val(0);

			$('#'+id).find('.granit-pokok').html(0);
			$('#'+id).find('.keramik-pokok').html(0);

			// updateHargaArray(id,"0","0","0","0","0",hargaArray);

			$('#updateHargaModal').modal('hide');

		})

		$('#kirimpengajuan').click(function(){
			if (!confirm("Apakah anda akan melakukan pengajuan ?")) {
				return false;
			}

			$('#main_form_ajukan').submit();
		})

		$('#tolakharga').click(function(){
			if (!confirm("Apakah anda akan meminta revisi pengajuan ini ?")) {
				return false;
			}

			$('#main_form_tolak_harga').submit();
		})

		$('#approveharga').click(function(){
			if (!confirm("Apakah anda akan approve pengajuan ini ?")) {
				return false;
			}

			$('#main_form_approve_harga').submit();
		})

		$('#simpandaftarharga').click(function(){
			$('#main_form_revisi_detail').attr('action','<?php echo site_url('web/pengajuan_harga/revisidetail')?>');
			$('#main_form_revisi_detail').submit();
		})

		$('#simpanajukandaftarharga').click(function(){
			if (!confirm("Apakah anda akan simpan dan melakukan pengajuan ?")) {
				return false;
			}

			$('#main_form_revisi_detail').attr('action','<?php echo site_url('web/pengajuan_harga/simpanajukanharga')?>');
			$('#main_form_revisi_detail').submit();
		})

		$('#simpanajukanhargaproduksi').click(function(){
			if (!confirm("Apakah anda akan simpan dan melakukan pengajuan ?")) {
				return false;
			}

			$('#main_form_revisi_detail').attr('action','<?php echo site_url('web/pengajuan_harga/simpanajukanhargaproduksi')?>');
			$('#main_form_revisi_detail').submit();
		})


		<?php if ($data['jenis'] == 2) { ?>
			$('#strukCheckModal .btn-granit').trigger('click');
		<?php }?>

		<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>		              					
		$('.check-harga-produksi').change(function() {
		    if(this.checked) {
		        $('.hrg-kons-check').addClass('hide-hrg-kons');
		        $('.hrg-prod-check').removeClass('hide-hrg-prod');
		        $('.preview-struk-check').attr('tipeharga','prod');
		    }else{
				$('.hrg-prod-check').addClass('hide-hrg-prod');
		        $('.hrg-kons-check').removeClass('hide-hrg-kons');
		        $('.preview-struk-check').attr('tipeharga','kons');
		    }
		});

		$('.check-harga-produksi-full').change(function() {
		    if(this.checked) {
		        $('.hrg-kons').addClass('hide-hrg-kons');
		        $('.hrg-prod').removeClass('hide-hrg-prod');
		        $('.preview-struk').attr('tipeharga','prod');
		    }else{
				$('.hrg-prod').addClass('hide-hrg-prod');
		        $('.hrg-kons').removeClass('hide-hrg-kons');
		        $('.preview-struk').attr('tipeharga','kons');
		    }
		});
		<?php } ?>

		$(document).on("keypress keyup blur",".allownumericwithdecimal", function (event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
		    $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
	})
	
	function formatRupiah(bilangan){

	    var bilangan = bilangan.toString().replace(/\./g,"");
	    var number_string = bilangan.toString(),
	    sisa    = number_string.length % 3,
	    rupiah  = number_string.substr(0, sisa),
	    ribuan  = number_string.substr(sisa).match(/\d{3}/g);
	        
	    if (ribuan) {
	        separator = sisa ? '.' : '';
	        rupiah += separator + ribuan.join('.');
	    }

	    return rupiah;
	}
</script>
