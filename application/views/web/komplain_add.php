<link href="<?php echo base_url()?>assets/web/plugins/dropzone/dropzone.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/web/plugins/dropzone/dropzone.js"></script>

<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Input Komplain</h1>
        </div>
	</div>

<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
	<div class="row mb-4">
		<div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
			    <a class="btn btn-sm btn-add" href="<?php echo site_url('web/komplain/komplainbaru')?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            </span>
        </div>
	</div>    

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<form action="<?= site_url("web/$class/create") ?>" method="post">
	           	<div class="form-group row">
	           		<input type="hidden" name="adaFoto" id="adaFoto" value="0">
	           		<label class="col-12 col-sm-12 col-md-2">Kode Order</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<input type="text" name="kode_order" id="kode_order" class="form-control form-control-sm" required>
	           		</div>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<button class="btn btn-secondary" data-target="#pilihKonsumenModal" data-toggle="modal"><i class="fas fa-list nav-icon"></i></button>
	           		</div>
	           		
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Nama</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<input type="text" name="nama" id="nama_konsumen" class="form-control form-control-sm" required>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Alamat</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<textarea name="alamat" id="alamat_konsumen" class="form-control form-control-sm" required> </textarea>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">No HP</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<input type="text" name="no_hp" id="no_hp_konsumen" class="form-control form-control-sm" required>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Tanggal Komplain</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<button type="button" class="btn btn-sm btn-outline-secondary btn-block btn-pilih-tanggal">-- Pilih Tanggal --</button>
						<input type="text" name="tgl_komplain" class="datepicker" style="visibility:hidden;position: absolute;top: 50px;">

	           		</div>
	           	</div>

	           	<!-- <div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Jam</label>
	           		<div class="col-12 col-sm-12 col-md-2">
						<div class="input-group clockpicker">
							<input type="text" name="jam" class="form-control form-control-sm" value="" readonly required style="background-color: transparent;">
						</div>
	           		</div>
	           	</div> -->

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Komplain</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<textarea class="form-control" name="description"></textarea>
	           		</div>
	           	</div>
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Catatan</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<textarea class="form-control" name="catatan"></textarea>
	           		</div>
	           	</div>
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Gambar Komplain</label>
	           		<div class="col-12 col-sm-12 col-md-4">

					  	<div class="dropzone dz-upload-1">
							<div class="dz-message">
							  	<label for="file-1">
							  		<i class="fa fa-cloud-upload" aria-hidden="true"></i>
							  		<h5>Upload <span>File</span></h5>
							  	</label>
								  <p> Klik atau Drag file kesini</p>
							</div>
					 	</div>
					 	<p style="color:red">NB: Wajib menginputkan gambar</p>
				 	</div>

				</div>
	           	
	           	

	           	<div class="form-group text-center mt-5">
	           		<button type="submit" class="btn btn-sm btn-add" id="btn-simpan-tambah" disabled=""><i class="fas fa-save"></i> Simpan</button>
	           	</div>
        	</form>
        </div>
  	</div>

<!-- Pilih Konsumen Modal -->
<div class="modal fade" id="pilihKonsumenModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pilih...</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php foreach ($pesanan_baru as $key): ?>
			<div class="row mt-2 row-order-baru pilih-konsumen" style="cursor:pointer;color:#212529; ">
				<div class="col-7 col-sm-6 col-md-8 list-order-baru">
					<label class="kode_order"><?= $key['kode_order']  ?></label>
					<p class="nama_konsumen"><?= $key['nama_konsumen']  ?></p>
					<p class="alamat_konsumen"><?= substr($key['alamat_konsumen'],0,50).'...'  ?></p>
					<p style="display: none;" class="alamat_konsumen_extend"><?= $key['alamat_konsumen']  ?></p>
					<p style="display: none;" class="no_hp_konsumen"><?= $key['no_hp_konsumen']  ?></p>
				</div>
				<div class="col-5 col-sm-6 col-md-4 list-order-baru text-right">
					<label></label>
					<p><?= $key['no_hp_konsumen']  ?></p>
					<div class="status-produksi">
						<?php if($key['status_produksi'] == 1) {
							echo '<span style="background-color: #4caf50;">Bisa Cek Ruangan</span>';
						}elseif($key['status_produksi'] == 2) {
							echo '<span style="background-color: #4caf50;">Bisa Pasang Rangka</span>';
						}elseif($key['status_produksi'] == 3) {
							echo '<span style="background-color: #f71212;">Menunggu Cor Siap</span>';
						} ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
      </div>
    </div>
  </div>
</div>

<script>
	Dropzone.autoDiscover = false;
	
	$(document).ready(function(){
		$(document).on('click','.pilih-konsumen', function(){
			var kode_order = $(this).find('.kode_order').html();
			var nama_konsumen = $(this).find('.nama_konsumen').html();
			var alamat_konsumen = $(this).find('.alamat_konsumen_extend').html();
			var no_hp_konsumen = $(this).find('.no_hp_konsumen').html();

			$("#kode_order").val(kode_order);
			$("#nama_konsumen").val(nama_konsumen);
			$("#alamat_konsumen").val(alamat_konsumen);
			$("#no_hp_konsumen").val(no_hp_konsumen);
			$('#pilihKonsumenModal').modal('hide');
		})

		$('.btn-pilih-tanggal').on('click', function() {
		      $('.datepicker').datepicker('show');
		});

		var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
		  "Juli", "Agustus", "September", "Oktober", "November", "Desember"
		];
		$('.datepicker').datepicker({
		    format: 'yyyy-mm-dd',
		    autoclose: true
		}).on('change', function(e) {
		    var tanggal = $('.datepicker').val();
		    var dt = new Date(tanggal);
		    var html = dt.getDate()+' '+monthNames[dt.getMonth()]+' '+dt.getFullYear();
		    $('.btn-pilih-tanggal').html(html);
		  
		});

		// $('.clockpicker').clockpicker({
		// 	donetext: 'Done'
		// });

		$(document).on('click', '.btn-pilih-gambar', function(){
			$('#pilihGambarModal').modal('show');
		})

		if ($('.dropzone').length) {
			var myDropzone= new Dropzone(".dz-upload-1",{
				url: "<?= site_url('web/komplain') ?>/sendimage?folder=komplain",
				success: function(file, response){
			        if(response==1){
			        	// console.log($('#adaFoto').val());
			        	var val = parseInt($('#adaFoto').val());
			        	var valplus = parseInt(val)+1;
			        	$('#adaFoto').val(valplus);
			        }else{
			        	var val = $('#adaFoto').val();
			        	if(val != 0){
			        		var valmin = parseInt(val)-1;
			        		$('#adaFoto').val(valmin);
			        	}
			        	
			        }

			        var check = parseInt($('#adaFoto').val());
			        // console.log(check);
			        if(check>0){
			        	$('#btn-simpan-tambah').attr('disabled', false);
			        }else{
			        	$('#btn-simpan-tambah').attr('disabled', true);
			        }
			        // console.log($('#adaFoto').val());
			    },
				addRemoveLinks:true,
				maxFilesize: 5, // MB
				dictRemoveFile : "remove",
				removedfile: function(file){
					var name = file.name;

					$.ajax({
						type: "post",
						url: "<?= site_url('web/komplain') ?>/removetemp/komplain",
						data: {file: name},
						dataType: "html",
						success: function(file, response){
					        var val = $('#adaFoto').val();
					        if(val != 0){
					        	var valmin = parseInt(val)-1;
					        	$('#adaFoto').val(valmin);
					        }

					        var check = parseInt($('#adaFoto').val());
					        if(check>0){
					        	$('#btn-simpan-tambah').removeClass('disabled');
					        }else{
					        	$('#btn-simpan-tambah').addClass('disabled');
					        }
					        	
					    },
					});

					//remove thumbnail
					var previewElement;
					return (previewElement = file.previewElement) != null ? (previewElement.parentNode.removeChild(file.previewElement)) : (void 0);
				},
			});
		}
	})
</script>