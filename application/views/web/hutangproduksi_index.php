<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Hutang Produksi</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<!-- <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button> -->
        </span>
        <span style="float: right;">
		  <a href="#" class="btn btn-sm btn-add"><i class="fas fa-plus"></i> Tambah</a>
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
    	<div class="kalender-kas text-center">
    		<button type="button" id="prev" class="btn btn-sm btn-light float-left"><i class="fas fa-angle-left"></i></button>
    		<label>2021</label>
    		<input type="hidden" id="month" value="<?= date('m')?>">
    		<input type="hidden" id="year" value="<?= date('Y')?>">
    		<button type="button" id="next" class="btn btn-sm btn-light float-right"><i class="fas fa-angle-right"></i></button>
    	</div>
    </div>
    <div class="col-md-12">
    	<div class="row hutang-produksi-row">
      	<div class="col-12 col-sm-12 col-md-12">
          <label>MEI</label>
        </div>
        <div class="col-3 col-sm-3 col-md-4">
          <span>Beban</span>
          <p class="text-red">(5.570.000)</p>
          <label class="text-red">5.570.000</label>
        </div>
        <div class="col-6 col-sm-6 col-md-4 text-center">
          <span>Pembayaran Produksi</span>
          <h5 class="text-green">0</h5>
        </div>
        <div class="col-3 col-sm-3 col-md-4 text-right">
          <span class="text-red">Kurang</span>
          <p>(5.570.000)</p>
          <label class="text-red">5.570.000</label>
        </div>
      </div>

      <div class="row hutang-produksi-row">
        <div class="col-12 col-sm-12 col-md-12">
          <label>JULI</label>
        </div>
        <div class="col-3 col-sm-3 col-md-4">
          <span>Beban</span>
          <p class="text-red">(6.075.000)</p>
          <label class="text-red">6.075.000</label>
        </div>
        <div class="col-6 col-sm-6 col-md-4 text-center">
          <span>Pembayaran Produksi</span>
          <h5 class="text-green">0</h5>
        </div>
        <div class="col-3 col-sm-3 col-md-4 text-right">
          <span class="text-red">Kurang</span>
          <p>(6.075.000)</p>
          <label class="text-red">6.075.000</label>
        </div>
      </div>

      <div class="row hutang-produksi-row">
        <div class="col-12 col-sm-12 col-md-12">
          <label>AGUSTUS</label>
        </div>
        <div class="col-3 col-sm-3 col-md-4">
          <span>Beban</span>
          <p class="text-red">(7.790.000)</p>
          <label class="text-red">3.895.000</label>
        </div>
        <div class="col-6 col-sm-6 col-md-4 text-center">
          <span>Pembayaran Produksi</span>
          <h5 class="text-green">11.000.000</h5>
        </div>
        <div class="col-3 col-sm-3 col-md-4 text-right">
          <span class="text-green">Lebih</span>
          <p>(-3.210.000)</p>
          <label class="text-green">7.105.000</label>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="row total-hutang-produksi-row">
        <div class="col-3 col-sm-3 col-md-4">
          <span>Omzet</span>
          <br>
          <label class="text-red">15.540.000</label>
          <p class="text-red">(19.435.000)</p>
        </div>
        <div class="col-6 col-sm-6 col-md-4 text-center">
          <span>Pembayaran Produksi</span>
          <div>
            <h5>11.000.000</h5>
          </div>
        </div>
        <div class="col-3 col-sm-3 col-md-4 text-right">
          <span class="text-green">Kekurangan</span>
          <br>
          <label class="text-green">4.540.000</label>
          <p>(8.435.000)</p>
        </div>
      </div>
    </div>
</div>

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Urutkan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row filter-kas">
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Tanggal Input Lama -> Terbaru</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Tanggal Input Terbaru -> Lama</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Jenis A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Jenis Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kode A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kode Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Debit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Debit Max -> Min</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kredit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kredit Max -> Min</div>
        		</a>
        	</div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<script>
	$(document).ready(function(){
		$('#prev').click(function(){
			var month = $('#month').val();
			var year = $('#year').val();
			
			alert('prev date');
		})

		$('#next').click(function(){
			var month = $('#month').val();
			var year = $('#year').val();
			
			alert('next date');
		})
	})
</script>