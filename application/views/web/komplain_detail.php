
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/web/plugins/evo-calendar/css/evo-calendar.css">
<style>
	.event-container > .event-icon{
	    margin: 25px 15px 0 10px;
	}
	.event-container::before{
		width: 1.5px;
		left: 5px;
		z-index: 0;
	    background-color: #757575;
	}
	.event-container:hover{
	    background-color: transparent;
	}
	
	@media screen and (max-width: 1280px) {
		.event-container::before{
			left: -17.5px;
			top: 10px;
		}
		.event-container > .event-icon {
		    margin: 25px 10px 0 0px;
		}
		.event-container > .event-info > p.event-title > span{
			right: -30px;
		}
	}
</style>
<div class="row mb-4">
	<div class="col-sm-12 col-md-12 title-page">
        <h1>Detail Komplain</h1>
    </div>
</div>    

<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
<div class="row">
    <div class="col-sm-12 col-md-12 mb-4">
    	<h4>Konsumen</h4>
    	<div class="row">
    		<div class="col-4 col-sm-2 col-md-2">Kode Order</div>
    		<div class="col-8 col-sm-10 col-md-10">
    			<?= $detail['kode_order'] ?>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-4 col-sm-2 col-md-2">Nama</div>
    		<div class="col-8 col-sm-10 col-md-10">
    			<?= $detail['nama_konsumen'] ?>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-4 col-sm-2 col-md-2">Alamat</div>
    		<div class="col-8 col-sm-10 col-md-10">
    			<?= $detail['alamat_konsumen'] ?>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-4 col-sm-2 col-md-2">No HP</div>
    		<div class="col-8 col-sm-10 col-md-10">
    			<?= $detail['no_hp_konsumen'] ?>
    		</div>
    	</div>
      <div class="row">
        <div class="col-4 col-sm-2 col-md-2">Catatan</div>
        <div class="col-8 col-sm-10 col-md-10">
          <p id="content-catatan"><?= $detail['catatan'] ?></p>
          <?php if (in_array(SessionManagerWeb::getRole(), array(Role::MARKETING)) && in_array($detail['flow_id'], array(2,5))): ?>
          <button onclick="activeFormCatatan(1)" class="btn btn-primary" id="ubah-catatan">Ubah</button>
          
          <div class="d-none" id="form-catatan">
            <form method="POST" action="<?= site_url("web/$class/edit/".$detail['id'])  ?>" class="form-group">
            <textarea class="form-control" id="catatan_change" name="catatan"><?= $detail['catatan'] ?></textarea>
            <button onclick="activeFormCatatan(0)" type="button" class="btn btn-danger d-none" id="batal-catatan">Batal</button>
            <button class="btn btn-success" type="submit"> Simpan</button>
          </form>
          </div>
          
          <?php endif ?>
          
        </div>
      </div>
    </div>
    
    <div class="col-sm-12 col-md-12 mb-4">
    	<h4>Gambar Komplain</h4>
    	<div class="row">
    		<?php foreach($detail['foto_order'] as $key) { ?>
    			<div class="col-6 col-sm-6 col-md-4">
    				<img src="<?= $key;?>" width="100%" onclick="expandImg('<?= $key;?>');" style="cursor: pointer;">
    			</div>
	    	<?php } ?>
    	</div>
	</div>

	<div class="col-sm-12 col-md-12 mb-4">
    	<h4>Status Kerja</h4>
    	<div class="row">
			<div class="col-md-12 status-kerja-div">
				<label>
					<?php
					switch ($detail['flow_id']) {
						case 2:
							echo "Pengajuan Kunjungan Perbaikan";
						break;
						case 4:
							echo "Menunggu Set Selesai Cek Ruangan / Pasang Kusen";
						break;
						case 7:
							echo "Menunggu Set Selesai Cek Ruangan / Pasang Kusen";
						break;
						default:
							echo $detail['flow_name'];
							break;
					}
					 ?>
				</label>
			</div>
    	</div>
    	<div class="row">
    		<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI){ ?>
    			<?php if ($detail['status_produksi']!=3): ?>
        			<?php if (($detail['flow_id']==2 || $detail['flow_id']==5) && $detail['jadwal_usulan']==null){ ?>
        			<div class="col-md-12 text-center mt-2">
    	       			<button type="button" class="btn btn-sm btn-add btn-ubah-status-produksi usulkan-jadwal" idorder="<?= $detail['id'] ?>" onclick="usuljadwal(<?= $detail['id']  ?>)" kode="<?= $detail['kode_order'] ?>" status="<?= $detail['status_produksi'] ?>">Usulkan Jadwal Pasang <?= ($detail['flow_id']==2) ? 'Kusen' : 'Unit'  ?></button>
    				</div>
        			<?php }
                elseif(($detail['flow_id']==2 || $detail['flow_id']==5) && $detail['jadwal_usulan']!=null){
                  ?>
                <div class="col-md-12 text-center mt-2">
                    <div class="col-md-12 text-center mt-2">
                      <button type="button" class="btn btn-sm btn-add btn-ubah-status-produksi" onclick='ordermenunggu(<?= $detail['id'] ?>, <?= json_encode($detail) ?>)'> Konfirmasi Jadwal</button>
                </div>
              </div>
                  <?php
                }
               elseif($detail['flow_id']==4 || $detail['flow_id']==7){
        				?>
        			<div class="col-md-12 text-center mt-2">
        				<button type="button" class="btn btn-sm btn-add btn-ubah-status-produksi usulkan-jadwal" idorder="<?= $detail['id'] ?>" onclick="setselesaiproses(<?= $detail['id']  ?>,'<?= $detail['rencana_kerja_id']  ?>', '<?= $detail['kode_order']  ?>','<?= $detail['nama_konsumen']  ?>','<?= $detail['jadwal_usulan']  ?>','<?= $detail['rencana_kerja_nama']  ?>')" kode="<?= $detail['kode_order'] ?>" status="<?= $detail['status_produksi'] ?>">Selesai</button>
        			</div>
        			<?php
        			}?>
    			
    			
    		<?php endif ?>
    			
    		<?php } elseif(SessionManagerWeb::getRole() != Role::PRODUKSI && SessionManagerWeb::getRole() != Role::PEMASANG) {
          ?>

          <?php if ($detail['flow_id'] == 2 && $detail['jadwal_usulan']==null): ?>
          <div class="col-md-12 text-center mt-2">
              <button type="button" class="btn btn-sm btn-add btn-ubah-status-produksi update-status" idorder="<?= $detail['id'] ?>" kode="<?= $detail['kode_order'] ?>" status="<?= $detail['status_produksi'] ?>">Ubah Status Produksi</button>
        </div>
          <?php endif ?>
          <?php if ($detail['jadwal_usulan']!=null): ?>
          <div class="col-md-12 text-center mt-2">
                <button type="button" class="btn btn-sm btn-add btn-ubah-status-produksi" onclick='ordermenunggu(<?= $detail['id'] ?>, <?= json_encode($detail) ?>)'> Konfirmasi Jadwal</button>
          </div>
          <?php endif ?>
          
          <?php
        }?>
    		
			
		</div>
	</div>



	<div class="col-sm-12 col-md-12 mb-4">
    	<h4>Detail Order</h4>
    	<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="event-list">
					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-selesai"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Order Masuk <span><?= xFormatDateInd($detail['tgl_order_masuk']) ?></span></p>
							<p class="event-desc"><?= $detail['nama_user']?></p>
						</div>
					</div>					

					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-holiday"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Cek Ruangan / Pasang Kusen <span><?= xFormatDateInd($detail['tanggal_pasang_kusen']) ?></span></p>
							<p class="event-desc"><?= $detail['nama_pemasang_kusen']?></p>
						</div>
					</div>					

					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-pemasangan"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Pemasangan Unit <span><?= xFormatDateInd($detail['tanggal_pasang_finish']) ?></span></p>
							<p class="event-desc"><?= $detail['nama_pemasang_finish']?></p>
						</div>
					</div>					

					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-birthday"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Menunggu Set Pemasangan Selesai <span><?= xFormatDateInd($detail['tgl_acc_konsumen']) ?></span></p>
							<p class="event-desc"><?= $detail['nama_konsumen']?></p>
						</div>
					</div>					
				</div>
			</div>
    	</div>
	</div>
</div>

<!-- Order Proses Selesai Modal -->
<div class="modal fade" id="orderProsesSelesaiModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="orderProsesSelesaiTitleModal">Pekerjaan Selesai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="#" method="post" id="formProsesSelesai">
      <input type="hidden" name="konfirmasi_selesai" id="konfirmasi_selesai">
      <div class="modal-body">
        <div style="font-size: .9em;">Anda akan merubah status pekerjaan menjadi selesai pada order berikut</div>
        <div class="row list-order-proses-selesai mt-1" style="font-size: .9em;background-color: #eee;">
          <div class="col-4 col-sm-4 col-md-4">Kode Order</div>
          <div class="col-8 col-sm-8 col-md-8" id="kode_order"></div>

          <div class="col-4 col-sm-4 col-md-4">Nama</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">Jadwal</div>
          <div class="col-8 col-sm-8 col-md-8" id="jadwal"></div>

          <div class="col-4 col-sm-4 col-md-4">Rencana Kerja</div>
          <div class="col-8 col-sm-8 col-md-8" id="rencana_kerja"></div>
        </div>
      </div>
      </form>
      <div class="modal-footer" style="border-top: none;">
          <button class="btn btn-sm btn-add" id="langsungselesai">Langsung Selesai</button>
          <button class="btn btn-sm btn-add" id="belumselesai">Jadwalkan Lagi</button>
      </div>
      
    </div>
  </div>
</div>

<!-- Expand Image Modal-->
<div class="modal fade" id="expandImagenModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
       </div>
         <div class="modal-body">
           <div class="img-div">
            <img src="" style="width: 100%;">
           </div>
         </div>
      </div>
   </div>
</div>

<!-- Order Menunggu Modal -->
<div class="modal fade" id="orderMenungguModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Usulan Jadwal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-4 col-sm-4 col-md-4">Tanggal</div>
          <div class="col-8 col-sm-8 col-md-8" id="jadwal_pengajuan"></div>

          <div class="col-4 col-sm-4 col-md-4">Rencana Kerja</div>
          <div class="col-8 col-sm-8 col-md-8" id="rencana_kerja_nama"></div>

          <div class="col-4 col-sm-4 col-md-4">Tim</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_pemasang"></div>

          <div class="col-12 col-sm-12 col-md-12">
            <label class="mb-0 mt-2">Data Order</label>
          </div>

          <div class="col-4 col-sm-4 col-md-4">Kode Order</div>
          <div class="col-8 col-sm-8 col-md-8" id="kode_order"></div>

          <div class="col-4 col-sm-4 col-md-4">Tgl. Order Masuk</div>
          <div class="col-8 col-sm-8 col-md-8" id="tgl_order_masuk"></div>

          <div class="col-4 col-sm-4 col-md-4">Nama</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">Alamat</div>
          <div class="col-8 col-sm-8 col-md-8" id="alamat_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">No HP</div>
          <div class="col-8 col-sm-8 col-md-8" id="no_hp_konsumen"></div>
        </div>
      </div>
      <div class="modal-footer" style="border-top: none;">
        <?php if (SessionManagerWeb::getRole() != Role::PRODUKSI): ?>
        <input type="text" class="form-control" name="link" id="link" value="" readonly="">
          <button  onclick="copyLink()" class="btn btn-sm btn-secondary" id="terimausulkanpemasangan"><i class="fas fa-link"></i> Copy Link</button>
        <?php endif ?>
        
          <a href="#" class="btn btn-sm btn-add" id="tolakusulkanpemasangan"><i class="fas fa-times"></i> Tolak</a>
          <button  onclick="terimaUsulan()" class="btn btn-sm btn-success" id="terimausulkanpemasangan"><i class="fas fa-check"></i> Setuju</button>
      </div>
    </div>
  </div>
</div>

<form id="formApprove" action="#" method="post">
  <input type="hidden" name="confirm" value="1">
  <input type="hidden" name="id" id="idorder">
</form>

<script type="text/javascript">
  function expandImg(path){
    $('#expandImagenModal .modal-body img').attr('src', path);
    $('#expandImagenModal').modal('show');
  }
  
	function usuljadwal(id){
		window.location= "<?= site_url('web/komplain/usulkanpemasangan')  ?>/"+id;
	}
	$(document).ready(function(){
    

    $(document).on('click','#langsungselesai',function(){
      // var id =$('#idorder').val();
      // $('#formKonfirmSelesai').attr('action', '<?= site_url("web/$class/usulan/")  ?>/'+id);
      $('#konfirmasi_selesai').val(1);
      $('#formProsesSelesai').submit();
     
    });

    $(document).on('click','#belumselesai',function(){

      // var id =$('#idorder').val();
      // $('#formKonfirmSelesai').attr('action', '<?= site_url("web/$class/usulan/")  ?>/'+id);
      $('#konfirmasi_selesai').val(0);
      $('#formProsesSelesai').submit();
     
    });

    
  
  })
	function setselesaiproses(id,rencana_kerja_id, kode_order, nama_konsumen, tgl_jadwal, rencana_kerja_nama){

    if (rencana_kerja_id == 1) {
      $('#orderProsesModal').modal('hide');
      $('#orderProsesSelesaiModal').modal('show');
      $('#formProsesSelesai').attr('action', '<?= site_url("web/$class/usulan/")  ?>/'+id);
      $('#orderProsesSelesaiModal #kode_order').html(kode_order);
      $('#orderProsesSelesaiModal #nama_konsumen').html(nama_konsumen);
      $('#orderProsesSelesaiModal #jadwal').html(tgl_jadwal);
      $('#orderProsesSelesaiModal #rencana_kerja').html(rencana_kerja_nama);
    }else{
      window.location.href = "<?php echo site_url('web/pesanan/setselesaiproses')?>/"+id;
    }
  }

  <?php if(SessionManagerWeb::getRole() != Role::PRODUKSI ){ ?> //untuk non produksi
    function ordermenunggu(id,data){
      console.log(data);
      $('#idorder').val(id);
      $('#link').val('<?php echo site_url('web/site/approve_penjadwalan_komplain')?>/'+data.konsumen_id);
      $('#tolakusulkanpemasangan').attr('href','<?php echo site_url('web/komplain/tolakusulkanpemasangan')?>/'+id);
      
      $('#orderMenungguModal').modal('show');
      $('.btn-pilih-mitrapemasang').html('Ubah');
      $('#jadwal_pengajuan').html(data.jadwal_usulan);
      $('#rencana_kerja_nama').html(data.rencana_kerja_nama);
      if(data.rencana_kerja_id== 1 && data.nama_pemasang_kusen!=''){
        var nama = data.nama_pemasang_kusen;
                  
      }
      else if(data.rencana_kerja_id> 1 && data.nama_pemasang_finish!=''){
        var nama = data.nama_pemasang_finish;
      }else{
        var nama = '-';
      }
      $('#nama_pemasang').html(nama);
      $('#kode_order').html(data.kode_order);
      $('#tgl_order_masuk').html(data.tgl_order_masuk);
      $('#nama_konsumen').html(data.nama_konsumen);
      $('#alamat_konsumen').html(data.alamat_konsumen);
      $('#no_hp_konsumen').html(data.no_hp_konsumen);

    }

  <?php }else{ ?> //untuk produksi
    function ordermenunggu(id,data){
      // $('#link').val('<?php echo site_url('web/site/approve_penjadwalan_komplain')?>/'+data.konsumen_id);
      $('#idorder').val(id);
      if(data.jadwal_usulan==null){
        window.location.href='<?= site_url('web/komplain/usulkanpemasangan/') ?>/'+id;
      }else{
        if(data.role_pengusul==''){
          window.location.href = "<?php echo site_url('web/komplain/usulkanpemasangan')?>/"+id;
        }else{
          $('#tolakusulkanpemasangan').attr('href','<?php echo site_url('web/komplain/tolakusulkanpemasangan')?>/'+id);
          $('#orderMenungguModal').modal('show');
          $('.btn-pilih-mitrapemasang').html('Ubah');
          $('#jadwal_pengajuan').html(data.jadwal_usulan);
          $('#rencana_kerja_nama').html(data.rencana_kerja_nama);
          if(data.rencana_kerja_id== 1 && data.nama_pemasang_kusen!=''){
            var nama = data.nama_pemasang_kusen;
                      
          }
          else if(data.rencana_kerja_id > 1 && data.nama_pemasang_finish!=''){
            var nama = data.nama_pemasang_finish;
          }else{
            var nama = '-';
          }
          $('#nama_pemasang').html(nama);
          $('#kode_order').html(data.kode_order);
          $('#tgl_order_masuk').html(data.tgl_order_masuk);
          $('#nama_konsumen').html(data.nama_konsumen);
          $('#alamat_konsumen').html(data.alamat_konsumen);
          $('#no_hp_konsumen').html(data.no_hp_konsumen);
        }
      }
      
      
    }
  <?php } ?>

  function terimaUsulan(){
    //console.log($('#idorder').val());
    var id = $('#idorder').val();
    $('#formApprove').attr('action','<?php echo site_url('web/komplain/usulan')?>/'+id);
    // console.log($('#formApprove').attr('action'));
    $('#formApprove').submit();
  }

  function activeFormCatatan(type){
    if(type == 1){
      $('#form-catatan').removeClass('d-none');
      $('#batal-catatan').removeClass('d-none');
      $('#ubah-catatan').addClass('d-none');
      $('#content-catatan').addClass('d-none');

    }
    else{
      $('#form-catatan').addClass('d-none');
      $('#batal-catatan').addClass('d-none');
      $('#ubah-catatan').removeClass('d-none');
      $('#content-catatan').removeClass('d-none');
    }
    
  }
</script>