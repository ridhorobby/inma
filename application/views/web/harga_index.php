<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Daftar Harga</h1>
    </div>
</div>

  <?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>

<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button>
        </span>
        <span style="float: right;">
		  <a href="<?php echo site_url('web/harga/add')?>" class="btn btn-sm btn-add"><i class="fas fa-plus"></i> Tambah</a>
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
       	<table class="table table-sm table-harga">
          <tbody>
          <?php
            foreach ($data as $index => $row){            
          ?>
          <tr style="background-color:#dedede">
              <td><b><?= $index; ?></b></td>
          </tr>
          <?php
              foreach ($row as $v) {
              $no++; 
          ?>
            <tr id="<?= $v['id']?>" class="rowharga">
              <td><?= $v['nama_harga']; ?></td>
            </tr>
          <?php } 
          } ?>
          </tbody>
        </table>
    </div>
</div>


<script>
	$(document).ready(function(){
		$('.rowharga').click(function(){
      var id = $(this).attr('id');
			window.location.href = "<?php echo site_url('web/harga/edit')?>/"+id;
		})
	})
</script>