
<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $title ?></title>
	<!-- Favicons -->
	<link href="<?php echo base_url()?>assets/web/img/favicon.png" rel="icon">
	<!-- Google Font: Source Sans Pro -->
	<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> -->
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/fontawesome-free/css/all.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/css/adminlte.css">

	<!-- jQuery -->
	<script src="<?php echo base_url()?>assets/web/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url()?>assets/web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url()?>assets/web/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    <a href="index3.html" class="brand-link" style="background-color: #424242;">
	      <label class="logo-title">Nasa<b>tech</b></label><br>
	      <span class="logo-sub-title">Kitchenset</span>
    </a>
    <nav class="main-header navbar navbar-expand navbar-red-2 navbar-light" style="height: 56px;">
	    <!-- Left navbar links -->
	    <!-- <ul class="navbar-nav">
	      <li class="nav-item">
	        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
	      </li>
	    </ul> -->

	    <!-- Right navbar links -->
	    <ul class="navbar-nav ml-auto">
	    	<li class="nav-item dropdown no-arrow">
	              <!-- Dropdown - User Information -->
                <a class="dropdown-item text-white" href="<?php echo site_url('web/site/logout')?>">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
	    </ul>
	</nav>


    <div class="content-wrapper" style="margin-left: 0;">
        <!-- Main content -->
        <section class="content mt-5">
            <div class="container-fluid">

<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/web/plugins/evo-calendar/css/evo-calendar.css">

<link href="<?php echo base_url()?>assets/web/plugins/dropzone/dropzone.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/web/plugins/dropzone/dropzone.js"></script>
<style>
	.event-container > .event-icon{
	    margin: 25px 15px 0 10px;
	}
	.event-container::before{
		width: 1.5px;
		left: 2px;
		top: -5px;
		z-index: 0;
	    background-color: #757575;
	}
	.event-container:hover{
	    background-color: transparent;
	}
	.dropdown-item:hover, .dropdown-item:focus{
	    background-color: #de1515;
	}
	@media screen and (max-width: 1280px) {
		.event-container::before{
			left: -17.5px;
			top: 10px;
		}
		.event-container > .event-icon {
		    margin: 25px 10px 0 0px;
		}
		.event-container > .event-info > p.event-title > span{
			right: -30px;
		}
		.info h5{
			font-size: 14px;
		}
	}
</style>
<div class="row mb-4">
	<div class="col-sm-12 col-md-12 title-page">
        <h3>Nasatech mengusulkan jadwal <b><?= ($data['rencana_kerja_id']==1) ? 'Pasang Kusen' : 'Pasang Finish' ?></b> </h3>
    </div>
</div>     

<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>

	
	
<div class="row">
    <div class="col-sm-12 col-md-12 mb-4">
    	<h5>Yang terhormat : </h5>
    	<div style="background-color: #EEEEEE;padding: 10px 15px;">
	    	<div class="row">
	    		<div class="col-4 col-sm-2 col-md-2">Nama</div>
	    		<div class="col-8 col-sm-10 col-md-10">
	    			<?= $data['nama_konsumen'] ?>
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col-4 col-sm-2 col-md-2">Alamat</div>
	    		<div class="col-8 col-sm-10 col-md-10">
	    			<?= $data['alamat_konsumen'] ?>
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col-4 col-sm-2 col-md-2">No HP</div>
	    		<div class="col-8 col-sm-10 col-md-10">
	    			<?= $data['no_hp_konsumen'] ?>
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col-4 col-sm-2 col-md-2">Kode Order</div>
	    		<div class="col-8 col-sm-10 col-md-10">
	    			<?= $data['kode_order'] ?>
	    		</div>
	    	</div>
    	</div>

    	<div class="mt-3 info">
        	<h5>Nasatech mengusulkan jadwal kedatangan dengan tanggal dan pekerjaan : </h5>
    	</div>

    	<div style="border-top: 1px solid #EEEEEE;border-bottom: 1px solid #EEEEEE;padding: 10px 15px;">
	    	<div class="row">
	    		<div class="col-4 col-sm-2 col-md-2">Tanggal</div>
	    		<div class="col-8 col-sm-10 col-md-10 text-red">
	    			<?php 
	    				switch ($data['flow_id']) {
	    					case '4':
	    						echo xFormatDateInd($data['tanggal_pasang_kusen']);
	    					break;
	    					case '7':
	    						echo xFormatDateInd($data['tanggal_pasang_finish']);
	    					break;	
	    					default:
	    						echo xFormatDateInd($data['jadwal_usulan']);
	    					break;
	    				}
  					?>
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col-4 col-sm-2 col-md-2">Jam</div>
	    		<div class="col-8 col-sm-10 col-md-10 text-red">
	    			<?php 
	    				switch ($data['flow_id']) {
	    					case '4':
	    						echo xFormatDateInd($data['jam_pasang_kusen']);
	    					break;
	    					case '7':
	    						echo xFormatDateInd($data['jam_pasang_finish']);
	    					break;	
	    					default:
	    						echo xFormatDateInd($data['jam_usulan']);
	    					break;
	    				}
	    			// ($data['rencana_kerja_id']==1) ? $data['jam_pasang_kusen'] : $data['jam_pasang_finish']  
	    			?>
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col-4 col-sm-2 col-md-2">Pekerjaan</div>
	    		<div class="col-8 col-sm-10 col-md-10 text-red">
	    			<?= ($data['rencana_kerja_id']==1) ? 'Pasang Kusen' : 'Pasang Finish' ?>
	    		</div>
	    	</div>
    	</div>

        <?php if($data['flow_id'] == 3 || $data['flow_id'] == 6) { ?>
    	<div class="mt-3 mb-3 info">
	        <h5>Apakah anda setuju dengan pengusulan ini ?</h5>
	    </div>

        <div class="row">

			<div class="col-sm-12 col-md-12 mb-4 text-center">
				<div class="waiting d-none">
				  <div class="alert alert-danger alert-dismissible" role="alert">
				      Silahkan tunggu proses berlangsung, jangan tinggalkan halaman ini...
				  </div>
				</div>
		        <a class="btn btn-add tolak" href="<?php echo site_url('web/site/tolakpesanankonsumen/'.$data['id'])?>"><i class="fas fa-times"></i> Tolak</a>
			    <a class="btn btn-success setuju" onclick="terimaUsulan('<?= $data['id'] ?>','<?= $data['jadwal_usulan']  ?>','<?= $data['jam_usulan']  ?>')"><i class="fas fa-check"></i> Setuju</a>
		    </div>
        </div>

	    <form id="formApprove" action="#" method="post">
		  <input type="hidden" name="confirm" value="1">
		  <input type="hidden" name="jadwal_usulan" id="jadwal_usulan" value="">
		  <input type="hidden" name="jam_usulan" id="jam_usulan" value="">
		</form>
		<?php } ?>

    </div>
    
    

	

	<!-- <div class="col-sm-12 col-md-12 mb-4">
    	<h4>Detail Order</h4>
    	<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="event-list">
					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-selesai"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Order Masuk <span><?= xFormatDateInd($data['tgl_order_masuk']) ?></span></p>
						</div>
					</div>					

					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-holiday"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Cek Ruangan / Pasang Kusen <span><?= xFormatDateInd($data['tanggal_pasang_kusen']) ?></span></p>
						</div>
					</div>					

					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-pemasangan"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Pemasangan Unit <span><?= xFormatDateInd($data['tanggal_pasang_finish']) ?></span></p>
						</div>
					</div>					

					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-birthday"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Menunggu Set Pemasangan Selesai <span><?= xFormatDateInd($data['tgl_order_masuk']) ?></span></p>
						</div>
					</div>					
				</div>
			</div>
    	</div>
	</div> -->

	
</div>

</div>
</section>

</div>

<div class="clearfix"></div>

<footer class="main-footer">
  <!-- <strong>Copyright &copy; 2021 <a href="#">Nasatech INMA</a>.</strong> -->
  <div class="float-right d-none d-sm-inline-block">
    <!-- <b>Version</b> 1.1.72 -->
  </div>
</footer>

<!-- overlayScrollbars -->
<script src="<?php echo base_url()?>assets/web/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>assets/web/js/adminlte.js"></script>


</div>
</body>


</html>

<script type="text/javascript">
	function terimaUsulan(id,jadwal_usulan, $jam_usulan){
    // var id = $('#idorder').val();
    $('.setuju').addClass('disabled');
    $('.tolak').addClass('disabled');
    $('.waiting').removeClass('d-none');
    $('#formApprove').attr('action','<?php echo site_url('web/site/confirmKonsumen')?>/'+id);
    $('#jam_usulan').val(jam_usulan);
    $('#jadwal_usulan').val(jadwal_usulan);
    $('#formApprove').submit();
  }
</script>