<link href="<?php echo base_url()?>assets/web/plugins/dropzone/dropzone.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/web/plugins/dropzone/dropzone.js"></script>

<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Laporan Selesai</h1>
        </div>
	</div>

<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
	<div class="row mb-4">
		<div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
			    <a class="btn btn-sm btn-add" href="<?php echo site_url('web/pesanan/daftarorder')?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            </span>
        </div>
	</div>    

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<div class="alert alert-danger" role="alert" style="padding: .5rem 4rem .5rem .75rem;font-size: 13px;">
			      Mohon foto bersama konsumen sebagai bukti pekerjaan telah selesai dilakukan.
			</div>
        	
        	<form action="<?= site_url("web/$class/usulan/$komplain_id") ?>" method="post">
        		<input type="hidden" name="adaFoto" id="adaFoto" value="0">
				<input type="hidden" name="id" class="form-control form-control-sm" value="<?php echo $pesanan_id ?>" required>

	           	
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Lampiran Gambar</label>
	           		<div class="col-12 col-sm-12 col-md-4">
					  	<div class="dropzone dz-upload-1">
							<div class="dz-message">
							  	<label for="file-1">
							  		<i class="fa fa-cloud-upload" aria-hidden="true"></i>
							  		<h5>Upload <span>File</span></h5>
							  	</label>
								  <p> Klik atau Drag file kesini</p>
							</div>
					 	</div>
				 	</div>

				</div>

	           	<div class="form-group text-center mt-5">
	           		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
	           	</div>
        	</form>
        </div>
  	</div>



<script>
	Dropzone.autoDiscover = false;
	
	$(document).ready(function(){

		$(document).on('click', '.btn-pilih-gambar', function(){
			$('#pilihGambarModal').modal('show');
		})

		if ($('.dropzone').length) {
			var myDropzone= new Dropzone(".dz-upload-1",{
				url: "<?= site_url('web/pesanan') ?>/sendimage?folder=komplain",
				success: function(file, response){
			        if(response==1){
			        	// console.log($('#adaFoto').val());
			        	var val = parseInt($('#adaFoto').val());
			        	var valplus = parseInt(val)+1;
			        	$('#adaFoto').val(valplus);
			        }else{
			        	var val = $('#adaFoto').val();
			        	if(val != 0){
			        		var valmin = parseInt(val)-1;
			        		$('#adaFoto').val(valmin);
			        	}
			        	
			        }
			        // console.log($('#adaFoto').val());
			    },
				addRemoveLinks:true,
				maxFilesize: 20, // MB
				dictRemoveFile : "remove",
				removedfile: function(file){
					var name = file.name;

					$.ajax({
						type: "post",
						url: "<?= site_url('web/pesanan') ?>/removetemp/komplain",
						data: {file: name},
						dataType: "html",
						success: function(file, response){
					        var val = $('#adaFoto').val();
					        if(val != 0){
					        	var valmin = parseInt(val)-1;
					        	$('#adaFoto').val(valmin);
					        }
					        	
					    },
					});

					//remove thumbnail
					var previewElement;
					return (previewElement = file.previewElement) != null ? (previewElement.parentNode.removeChild(file.previewElement)) : (void 0);
				},
			});
		}
	})
</script>