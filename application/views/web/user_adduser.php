
<style>
	.event-container > .event-icon{
	    margin: 25px 15px 0 10px;
	}
	.event-container::before{
		width: 1.5px;
		left: 5px;
		z-index: 0;
	    background-color: #757575;
	}
	.event-container:hover{
	    background-color: transparent;
	}
	
	@media screen and (max-width: 1280px) {
		.event-container::before{
			left: -17.5px;
			top: 10px;
		}
		.event-container > .event-icon {
		    margin: 25px 10px 0 0px;
		}
		.event-container > .event-info > p.event-title > span{
			right: -30px;
		}
	}
</style>
<div class="row mb-4">
	<div class="col-sm-12 col-md-12 title-page">
        <h1>Tambah User</h1>
    </div>
</div>    

<div class="row">

    <div class="col-sm-12 col-md-8">
    	<form method="POST" action="<?= site_url("web/$class/create")  ?>">
		  <div class="form-group">
		    <label for="exampleInputEmail1">Username</label>
		    <input type="text" name="username" class="form-control"  placeholder="Masukkan username" value="<?= $user['username'] ?>">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Password</label>
		    <input type="password" name="password" class="form-control"  placeholder="Masukkan password" value="<?= $user['name'] ?>">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Nama</label>
		    <input type="text" name="name" class="form-control"  placeholder="Masukkan nama" value="<?= $user['name'] ?>">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Jabatan</label>
		    <select class="form-control" name="role">
		    	<?php foreach ($list_role as $key => $value): ?>
		    		<?php if ($key != Role::ADMINISTRATOR): ?>
		    		<option value="<?= $key  ?>"><?= $value  ?></option>
		    		<?php endif ?>
		    		
		    	<?php endforeach ?>
		    </select>
		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">No. HP</label>
		    <input type="text" name="no_hp" class="form-control"  placeholder="Masukkan no hp" value="<?= $user['no_hp'] ?>">
		  </div>
		  <button type="submit" class="btn btn-primary" style="background-color: #f71212; border-color: #f71212">Simpan</button>
		</form>
    </div>

    
   

</div>