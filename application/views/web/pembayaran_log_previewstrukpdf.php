<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="<?php echo base_url()?>assets/web/img/favicon.png" rel="icon">
</head>
<body style="font-family: 'helvetica';">
	<style>
		label {
			font-weight: bold;
		}
		.text-red{
			color: #f71212;
		}
		.table-harga th,
		.table-harga td{
			border-top: 1px solid #777;
		}
		.table-harga .th-harga,
		.table-harga .th-subtotal,
		.table-harga .th-jml,
		.table-harga .jml,
		.table-harga .granit,
		.table-harga .keramik{
			text-align: center;
		}
		.btn-granit{
			background-color: #dee2e6;
		    padding: 5px 50px;
		}
		.btn-keramik{
			background-color: #dcedc8;
		    padding: 5px 50px;
		}
		.share-icon{
		    cursor: pointer;
			float: right;
			right: 10%;
			position: absolute;
			color: #f71212;
		}
		.logo-nasatech{
			position: absolute;
		    right: 0%;
		    top: 25px;
		}
		.logo-nasatech > label{
			font-weight: 400;
		    font-size: 2em;
		    margin: 0;
		}
		.logo-nasatech > label > b{
			color: #f71212;
		}
		.logo-nasatech > span{
		    font-size: .75rem;
		    margin-left: 5rem;
		}
	</style>
		<div class="logo-nasatech">
    		<label>Nasa<b>tech</b></label>
    		<br>
    		<span>Kitchenset</span>
    	</div>
		<table style="margin-bottom: 15px;">
    		<tr>
    			<td width="100px"><label>Kode Order</label></td>
    			<td><label class="text-red"><?= $data['kode_order']?></label></td>
    		</tr>
    		<tr>
    			<td style="font-weight: bold;">Nama</td>
    			<td><?= $data['nama']?></td>
    		</tr>
    		<tr>
    			<td style="font-weight: bold;">Alamat</td>
    			<td><?= $data['alamat']?></td>
    		</tr>
    		<tr>
    			<td style="font-weight: bold;">No HP</td>
    			<td><?= $data['no_hp']?></td>
    		</tr>
    	</table>
    	<?php $bgcolor = ($jenis == 'granit') ? "#dee2e6" : '#dcedc8'; ?>
		<div style="text-align: center;width: 100%;margin-left: 40%;">
	    	<button type="button" class="btn btn-sm btn-<?= $jenis; ?>" style="margin:10px 0;"><b><?= ($jenis == 'granit') ? 'Granit' : 'Keramik';?></b></button>
		</div>
    	<table class="table table-sm table-harga" style="width: 100%;">
    		<tr>
    			<th>No</th>
    			<th>Nama</th>
    			<th class="th-jml">Jml</th>
    			<th class="th-harga" style="background-color: <?= $bgcolor ?>;">@Harga</th>
    			<th class="th-subtotal" style="background-color: <?= $bgcolor ?>;">Subtotal</th>
    		</tr>
          <tbody>
          <?php 
          $no = 1;
          $totalgranitprod = 0;
          $totalkeramikprod = 0;
          $totalgranit = 0;
          $totalkeramik = 0;
          foreach ($detail as $index => $row){ ?>
				<tr style="background-color:#424242;color: #fff;">
				  <td colspan="5"><b><?= $index; ?></b></td>
				</tr>
        		<?php foreach ($row as $v) { 
        			$totalgranitprod += $v['harga_pokok_granit']*$v['jumlah'];
        			$totalkeramikprod += $v['harga_pokok_keramik']*$v['jumlah'];

        			$totalgranit += $v['harga_jual_granit']*$v['jumlah'];
        			$totalkeramik += $v['harga_jual_keramik']*$v['jumlah'];

        			if ($v['harga_pokok_granit_revisi'] == true) {
        				$granit_red = 'color: #f71212;';
        			}else{
        				$granit_red = '';
        			}

        			if ($v['harga_pokok_keramik_revisi'] == true) {
        				$keramik_red = 'color: #f71212;';
        			}else{
        				$keramik_red = '';
        			}
        			?>
		            <tr id="<?= $v['id']?>" class="rowharga">
		              <td><?= $no++; ?></td>
		              <td><?= $v['nama']; ?></td>
		              <td class="jml"><?= number_format($v['jumlah'],1)?></td>

		              <?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
			          	<?php if($jenis == 'granit') { ?>

		              		<td class="granit" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_pokok_granit'],0,",",".")?></td>
		              		<td class="granit" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_pokok_granit']*$v['jumlah'],0,",",".")?></td>

				        <?php }else{ ?>

		              		<td class="keramik" style="background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_pokok_keramik'],0,",",".")?></td>
		              		<td class="keramik" style="background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_pokok_keramik']*$v['jumlah'],0,",",".")?></td>
		              	<?php } ?>

			          <?php }else{ ?>

			          	<?php if($jenis == 'granit') { ?>
			          		<?php if($tipeharga == 'kons') { ?>

				          	<td class="granit" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_jual_granit'],0,",",".")?></td>
			              	<td class="granit" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_jual_granit']*$v['jumlah'],0,",",".")?></td>
					        
					        <?php }else{ ?>

			              	<td class="granit" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_pokok_granit'],0,",",".")?></td>
		              		<td class="granit" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_pokok_granit']*$v['jumlah'],0,",",".")?></td>

					        <?php }?>

				          <?php }else{ ?>
			          		<?php if($tipeharga == 'kons') { ?>

			              	<td class="keramik" style="background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_jual_keramik'],0,",",".")?></td>
		              		<td class="keramik" style="background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_jual_keramik']*$v['jumlah'],0,",",".")?></td>

		              		<?php }else{ ?>

			              	<td class="keramik" style="background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_pokok_keramik'],0,",",".")?></td>
		              		<td class="keramik" style="background-color: #dcedc8;<?= $keramik_red; ?>"><?= number_format($v['harga_pokok_keramik']*$v['jumlah'],0,",",".")?></td>

					        <?php }?>
		              	<?php } ?>
			          <?php } ?>

		            </tr>
          <?php } 
          } ?>
          <tr>
          	<td colspan="3" style="text-align: right"><b>Total</b></td>
        	<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
				<?php if($jenis == 'granit') { ?>
    		      	<td colspan="2" class="granit" style="text-align: right"><b><?= number_format($totalgranitprod,0,",",".") ?></b></td>
	        	<?php }else{ ?>	          		
          			<td colspan="2" class="keramik" style="text-align: right;"><b><?= number_format($totalkeramikprod,0,",",".") ?>
	          	<?php } ?>

			<?php }else{ ?>

	          	<?php if($jenis == 'granit') { ?>
	          		<?php if($tipeharga == 'kons') { ?>

	    		      	<td colspan="2" class="granit" style="text-align: right"><b><?= number_format($totalgranit,0,",",".") ?></b></td>
	        		<?php }else{ ?>
	    		      	<td colspan="2" class="granit" style="text-align: right"><b><?= number_format($totalgranitprod,0,",",".") ?></b></td>
	        		<?php } ?>
	        	<?php }else{ ?>
	          		<?php if($tipeharga == 'kons') { ?>

	          			<td colspan="2" class="keramik" style="text-align: right;"><b><?= number_format($totalkeramik,0,",",".") ?>
	        		<?php }else{ ?>
	          			<td colspan="2" class="keramik" style="text-align: right;"><b><?= number_format($totalkeramikprod,0,",",".") ?>

		          	<?php } ?>
	          	<?php } ?>
	        <?php } ?>
          		
          	</b></td>
          </tr>

          <?php if($data['diskon'] > 0) { ?>
          <tr>
          	<td colspan="3" style="text-align: right"><b>Diskon</b></td>
			<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
    			<?php if($jenis == 'granit') { ?>
					<td colspan="2" class="granit" style="text-align: right"><b>0</b></td>
	        	<?php }else{ ?>
          			<td colspan="2" class="keramik" style="text-align: right;"><b>0</b></td>
          		<?php } ?>
    		<?php }else{ ?>
    			<?php if($jenis == 'granit') { ?>
	          		<?php if($tipeharga == 'kons') { ?>
	          			<td colspan="2" class="granit hrg-kons" style="text-align: right"><b><?= number_format($data['diskon'],0,",",".") ?></b></td>
	        		<?php }else{ ?>
						<td colspan="2" class="granit" style="text-align: right"><b>0</b></td>
	          		<?php } ?>
	        	<?php }else{ ?>
	          		<?php if($tipeharga == 'kons') { ?>
		          		<td colspan="2" class="keramik hrg-kons" style="text-align: right;"><b><?= number_format($data['diskon'],0,",",".") ?></b></td>
	        		<?php }else{ ?>
          				<td colspan="2" class="keramik" style="text-align: right;"><b>0</b></td>

		          	<?php } ?>
	          	<?php } ?>
    		<?php } ?>

          </tr>
          <tr>
          	<td colspan="3" style="text-align: right"><b>Grand Total</b></td>
			<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
    			<?php if($jenis == 'granit') { ?>
					<td colspan="2" class="granit" style="text-align: right"><b><?= number_format($totalgranitprod-0,0,",",".") ?></b></td>
	        	<?php }else{ ?>
		          	<td colspan="2" class="keramik" style="text-align: right;"><b><?= number_format($totalkeramikprod-0,0,",",".") ?></b></td>
          		<?php } ?>
        	<?php }else{ ?>
        		<?php if($jenis == 'granit') { ?>
	          		<?php if($tipeharga == 'kons') { ?>
          				<td colspan="2" class="granit hrg-kons" style="text-align: right"><b><?= number_format($totalgranit-$data['diskon'],0,",",".") ?></b></td>
	        		<?php }else{ ?>
						<td colspan="2" class="granit" style="text-align: right"><b><?= number_format($totalgranitprod-0,0,",",".") ?></b></td>
	          		<?php } ?>
          		<?php }else{ ?>
	          		<?php if($tipeharga == 'kons') { ?>	
          				<td colspan="2" class="keramik hrg-kons" style="text-align: right;"><b><?= number_format($totalkeramik-$data['diskon'],0,",",".") ?></b></td>
	        		<?php }else{ ?>
		          		<td colspan="2" class="keramik" style="text-align: right;"><b><?= number_format($totalkeramikprod-0,0,",",".") ?></b></td>

          			<?php } ?>
	          	<?php } ?>
    		<?php } ?>

          </tr>
          <?php }?>
          </tbody>
        </table>
</body>
</html>