<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Hutang Produksi</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<!-- <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button> -->
        </span>
        <span style="float: right;">
		  <a href="<?php echo site_url('web/pembayaran_log/hutangproduksiperbulanadd')?>" class="btn btn-sm btn-add"><i class="fas fa-plus"></i> Tambah</a>
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
    	<div class="kalender-kas text-center">
    		<button type="button" id="prev" class="btn btn-sm btn-light float-left"><i class="fas fa-angle-left"></i></button>
    		<label><?= $_SESSION['hutangproduksi']['tahun'] ?></label>
        <button type="button" id="next" class="btn btn-sm btn-light float-right"><i class="fas fa-angle-right"></i></button>
        <form id="main_form" action="<?php echo site_url('web/pembayaran_log/gethutangproduksiperbulan')?>" method="post">
        <input type="hidden" id="year" name="tahun" value="<?= $_SESSION['hutangproduksi']['tahun'] ?>">
        </form>
    	</div>
    </div>
    <div class="col-md-12">
    <?php
    $kekurangan = 0;
    $lebih = 0;
      foreach ($data as $v) {
        $no++; 
        $omzet += $v['omzet'];
        $hutang += $v['hutang_produksi'];
        $lunas += $v['pelunasan']; 


        $cls = "text-green";
        if ($v['pelunasan'] < $v['hutang_produksi']){
          $cls= "text-red";
          $txt= "Kurang";
          $kekurangan += $v['hutang_produksi'] - $v['pelunasan'];

        }else if ($v['pelunasan'] > $v['hutang_produksi']){
          $txt = "Lebih";
          $lebih += $v['hutang_produksi'] - $v['pelunasan'];
        }else
          $txt = "Pas";
    ?>
    	<div class="row hutang-produksi-row" style="cursor: pointer;" onclick="getdetail('<?= $v['bulan'] ?>')" >
      	<div class="col-12 col-sm-12 col-md-12">
          <label><?= xIndoMonth($v['bulan']) ?></label>
        </div>
        <div class="col-3 col-sm-3 col-md-4">
          <span>Beban</span>
          <p class="text-red">(<?= number_format($v['omzet'],0,'.',',') ?>)</p>
          <label class="text-red"><?= number_format($v['hutang_produksi'],0,'.',',') ?></label>
        </div>
        <div class="col-6 col-sm-6 col-md-4 text-center">
          <span>Pembayaran Produksi</span>
          <h5 class="text-green"><?= number_format($v['pelunasan'],0,'.',',') ?></h5>
        </div>
        <div class="col-3 col-sm-3 col-md-4 text-right">
          <span class="<?= $cls ?>"><?= $txt ?></span>
          <p>(<?= number_format($v['omzet'] - $v['pelunasan'],0,'.',',') ?>)</p>
          <label class="<?= $cls ?>"><?= number_format(abs($v['hutang_produksi'] - $v['pelunasan']),0,'.',',') ?></label>
        </div>
      </div>
    <?php } ?>
    </div>

    <div class="col-md-12">
      <div class="row total-hutang-produksi-row">
        <div class="col-3 col-sm-3 col-md-4">
          <span>Omzet</span>
          <br>
          <label class="text-red"><?= number_format($hutang,0,'.',',') ?></label>
          <p class="text-red">(<?= number_format($omzet,0,'.',',') ?>)</p>
        </div>
        <div class="col-6 col-sm-6 col-md-4 text-center">
          <span>Pembayaran Produksi</span>
          <div>
            <h5><?= number_format($lunas,0,'.',',') ?></h5>
          </div>
        </div>
        <div class="col-3 col-sm-3 col-md-4 text-right">
          <span>Kekurangan</span>
          <br>
          <?php
          if (abs($kekurangan) < abs($lebih) ) {
            $txt_style = 'text-green';
          }elseif(abs($kekurangan) > abs($lebih) ){
            $txt_style = 'text-red';
          } 
          $total = abs($kekurangan) - abs($lebih);
          ?>
          <label class="<?= $txt_style; ?>"><?= number_format(abs($total),0,'.',',') ?></label>
          <p>(<?= number_format($omzet - $lunas,0,'.',',') ?>)</p>
        </div>
      </div>
    </div>
</div>

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Urutkan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row filter-kas">
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Tanggal Input Lama -> Terbaru</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Tanggal Input Terbaru -> Lama</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Jenis A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Jenis Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kode A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kode Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Debit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Debit Max -> Min</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kredit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kredit Max -> Min</div>
        		</a>
        	</div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<form id="detail_form" action="<?php echo site_url('web/pembayaran_log/gethutangproduksidetail');?>" method="post">
    <input type="hidden" name="bulan" id="detail_bulan">
    <input type="hidden" name="tahun" id="detail_tahun" value="<?= $_SESSION['hutangproduksi']['tahun'] ?>">
</form>

<script>
	$(document).ready(function(){
    
    $('#prev').click(function(){
      
      var tahun = parseInt($('#year').val()) - 1;
      $('#year').val(tahun);
      $('#main_form').submit();

    })

    $('#next').click(function(){
      
      var tahun = parseInt($('#year').val()) + 1;
      $('#year').val(tahun);
      $('#main_form').submit();
      
    })
  })

   function getdetail(bulan){
        $('#detail_bulan').val(bulan);
        $('#detail_form').submit();
    }
</script>