<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $title ?></title>
	<!-- Favicons -->
	<link href="<?php echo base_url()?>assets/web/img/favicon.png" rel="icon">
	<!-- Google Font: Source Sans Pro -->
	<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> -->
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/fontawesome-free/css/all.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/css/adminlte.css">

	<!-- jQuery -->
	<script src="<?php echo base_url()?>assets/web/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url()?>assets/web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url()?>assets/web/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/clockpicker/dist/bootstrap-clockpicker.css">
	<script src="<?php echo base_url()?>assets/web/plugins/clockpicker/dist/bootstrap-clockpicker.js"></script>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    <a href="index3.html" class="brand-link" style="background-color: #424242;">
	      <label class="logo-title">Nasa<b>tech</b></label><br>
	      <span class="logo-sub-title">Kitchenset</span>
    </a>
    <nav class="main-header navbar navbar-expand navbar-red-2 navbar-light" style="height: 56px;">
	    <!-- Left navbar links -->
	    <!-- <ul class="navbar-nav">
	      <li class="nav-item">
	        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
	      </li>
	    </ul> -->

	    <!-- Right navbar links -->
	    <ul class="navbar-nav ml-auto">
	    	<li class="nav-item dropdown no-arrow">
	              <!-- Dropdown - User Information -->
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
	    </ul>
	</nav>


    <div class="content-wrapper" style="margin-left: 0;">
        <!-- Main content -->
        <section class="content mt-5">
            <div class="container-fluid">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/web/plugins/evo-calendar/css/evo-calendar.css">
<script src="<?php echo base_url();?>assets/web/plugins/evo-calendar/js/evo-calendar.js"></script>

	<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Tolak</h1>
        </div>
	</div>

	<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>

	<div class="flashdata">
	  <div class="alert alert-danger alert-dismissible" role="alert">
	    <i class="fas fa-exclamation-triangle"></i> Usulan jadwal akan ditolak, mohon mengusulkan jadwal kedatangan lain untuk <b>Nasatech Kitchenset</b>
	  </div>
	</div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
           	<div id="calendar"></div>
        </div>
  	</div>
  <!-- /.row -->
  	<form action="<?= site_url('web/site/confirmkonsumen/'.$detail['id'].'/2')  ?>" onsubmit="simpan.disabled = true; return true;"  method="post">
  		<input type="hidden" name="id" value="<?= $detail['id']?>">
  		<input type="hidden" name="jadwal_usulan" id="tanggal_usulkan" value="<?php echo date('Y-m-d') ?>">
  		<input type="hidden" name="confirm" value="0">

  		<div class="form-group row mt-5">
       		<label class="col-12 col-sm-12 col-md-2">Jam</label>
       		<div class="col-12 col-sm-12 col-md-2">
				<div class="input-group clockpicker">
					<input type="text" name="jam_usulan" class="form-control form-control-sm" value="" readonly required placeholder="Masukkan Jam" style="background-color: transparent;">
				</div>
       		</div>
       	</div>
  		
  		<div class="form-group text-center mt-5">
  			<div class="waiting d-none">
			  <div class="alert alert-danger alert-dismissible" role="alert">
			      Silahkan tunggu proses berlangsung, jangan tinggalkan halaman ini...
			  </div>
			</div>
       		<button type="submit" class="btn btn-sm btn-add simpan"><i class="fas fa-save"></i> Simpan</button>
       	</div>
  	</form>
<script type="text/javascript">
	
	var active_events = [];

	var bulan = '<?php echo date('m');?>';
	var tahun = '<?php echo date('Y');?>';

	// var bulan = '08';
	// var tahun = '2020';

	getKalendarView(bulan,tahun);

	$(document).ready(function(){

		$(document).on('click','.day', function(){
			var date = $(this).attr('data-date-val');
			$('#tanggal_usulkan').val(date);

		})

		$(document).on('click','.month', function(){
			var datavalue = $(this).attr('data-month-val');
			var bulan_temp = parseInt(datavalue) + 1;

			var bulan = tambahNol(bulan_temp);

			var tahun = $('.calendar-year p').html();

			getKalendarView(bulan,tahun);

		})

		$(document).on('click','.calendar-year button:first', function(){
			var datavalue = $('.calendar-months').find('.active-month').attr('data-month-val');
			var bulan_temp = parseInt(datavalue) + 1;

			var bulan = tambahNol(bulan_temp);
			var tahun = $('.calendar-year p').html();
			
			getKalendarView(bulan,tahun);

		})

		$(document).on('click','.event-container', function(){
			var id = $(this).attr('data-event-index');
			window.location.href = '<?php echo site_url('web/pesanan/detail')?>/'+id;
		})

		$(document).on('click','.simpan', function(){
			$(this).addClass('disabled');
			$('waiting').removeClass('d-none');

		})
	});

	$('#calendar').evoCalendar({
		language: 'id',
    	theme: 'Royal Navy',
    	todayHighlight: true,
	    format: 'yyyy-mm-dd',
	})

	$('.clockpicker').clockpicker({
		donetext: 'Done'
	});

	function getKalendarView(bulan,tahun){

		$.ajax({
	        url : "<?php echo site_url('/web/pesanan/ajaxkalendarviewundone');?>",
	        type: 'post',
	        dataType: 'json',
	        data: {bulan:bulan, tahun:tahun},
	        success:function(respon){
	        	// console.log(respon);

				var data_event = [];
            	
                $.each(respon, function(k, v) {

                	var tglmasuk = v.tgl_masuk;
                	var tglpasangkusen = v.tanggal_pasang_kusen;
                	var tgljatuhtempo = v.tgl_jatuh_tempo;
                	var tglpasangfinish = v.tanggal_pasang_finish;

                	if (tglmasuk != null) {
	                	var tglmasuk = tglmasuk.split('-');
	                	if ( tglmasuk[0] == tahun && tglmasuk[1] == bulan) {

		                	var arr_data = {
								        id: v.id, // Event's ID (required)
								        name: 'Order Masuk <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.tgl_masuk, // Event date (required)
								        type: "selesai", // Event type (required)
								    }

							data_event.push(arr_data);
	                	}
                	}


                	if (tglpasangkusen != null) {
	                	var tglpasangkusen 	= tglpasangkusen.split('-');
	                	if (tglpasangkusen[0] == tahun && tglpasangkusen[1] == bulan) {
		                	var arr_data2 = {
								        id: v.id, // Event's ID (required)
								        name: 'Cek Ruangan / Pasang Kusen <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.tanggal_pasang_kusen, // Event date (required)
								        type: "holiday", // Event type (required)
								    }

							data_event.push(arr_data2);
	                	}
	                }

                	if (tgljatuhtempo != null) {
	                	var tgljatuhtempo = tgljatuhtempo.split('-');
	                	if (tgljatuhtempo[0] == tahun && tgljatuhtempo[1] == bulan) {
		                	var arr_data3 = {
								        id: v.id, // Event's ID (required)
								        name: 'Jatuh Tempo <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.tgl_jatuh_tempo, // Event date (required)
								        type: "event", // Event type (required)
								    }

							data_event.push(arr_data3);
	                	}
	                }

                	if (tglpasangfinish != null) {
	                	var tglpasangfinish = tglpasangfinish.split('-');
	                	if (tglpasangfinish[0] == tahun && tglpasangfinish[1] == bulan) {
		                	var arr_data4 = {
								        id: v.id, // Event's ID (required)
								        name: 'Pemasangan Unit <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.tanggal_pasang_finish, // Event date (required)
								        type: "pemasangan", // Event type (required)
								    }

							data_event.push(arr_data4);

	                	}
                	}
	        		
                });

				var indic = $('.calendar-table').find('.event-indicator').length;
				
				if (indic == 0) {
					$("#calendar").evoCalendar("addCalendarEvent", data_event);
				}

	        } 
	    });
		
	}

	function tambahNol(x){
	   y=(x>9)?x:'0'+x;
	   return y;
	}
</script>

</div>
</section>

</div>

<div class="clearfix"></div>

<footer class="main-footer">
  <!-- <strong>Copyright &copy; 2021 <a href="#">Nasatech INMA</a>.</strong> -->
  <div class="float-right d-none d-sm-inline-block">
    <!-- <b>Version</b> 1.1.72 -->
  </div>
</footer>

<!-- overlayScrollbars -->
<script src="<?php echo base_url()?>assets/web/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>assets/web/js/adminlte.js"></script>


</div>
</body>


</html>