<link href="<?php echo base_url()?>assets/web/plugins/bootstrap/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/web/plugins/bootstrap/js/bootstrap-toggle.min.js"></script>
<style>
	.granit,
	.granit-jual,
	.granit-pokok{
		background-color: #dee2e6;
	    padding: 0 5px;
	}
	.keramik,
	.keramik-jual,
	.keramik-pokok{
		background-color: #dcedc8;
	    padding: 0 5px;
	}
	.jumlah,
	.jml{
	    width: 55px;
	    text-align: center;
        padding: 10px 0;
	}

	.table-harga th,
	.table-harga td{
		border-top: 1px solid #777;
	}
	.table-harga .th-harga,
	.table-harga .th-subtotal,
	.table-harga .th-jml,
	.table-harga .jml,
	.table-harga .granit,
	.table-harga .keramik{
		text-align: center;
	}
	.btn-granit{
		background-color: #dee2e6;
	    padding: 5px 50px;
	}
	.btn-keramik{
		background-color: #dcedc8;
	    padding: 5px 50px;
	}
	.share-icon-check,
	.share-icon{
	    cursor: pointer;
		float: right;
		right: 20%;
	    font-size: 1.25rem;
		position: absolute;
		color: #f71212;
		z-index: 1;
	}
	.logo-nasatech{
		position: absolute;
	    right: 2%;
	    top: 25px;
	}
	.logo-nasatech > label{
		font-weight: 400;
	    font-size: 2em;
	    margin: 0;
	}
	.logo-nasatech > label > b{
		color: #f71212;
	}
	.logo-nasatech > span{
		float: right;
	    font-size: .75rem;
        position: relative;
    	top: -15px;
	    right: 10px;
	}
	.hide-hrg-kons{
		display: none!important;
	}
	.hide-hrg-prod{
		display: none!important;
	}

	@media (max-width: 768px) {
		.logo-nasatech {
		    top: 0px;
		}
		.share-icon-check, 
		.share-icon{
			top: 0;
		    font-size: 16px;
	        right: 26%;
		}

		#strukModal,
		#strukCheckModal{
		    font-size: 11px;
		}

		.logo-nasatech > span {
		    top: -13px;
		    right: 3px;
		}

		<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) { ?>
		#strukCheckModal .btn-granit,
		#strukModal .btn-granit,
		#strukCheckModal .btn-keramik,
		#strukModal .btn-keramik{
		    font-size: 10px;
	        margin-top: 25px!important;
		}

		.swithproduksi-div > div {
		    top: 95px!important;
		    left: 8px!important;
		}
		<?php }else{ ?>
		.swithproduksi-div > div {
		    top: 90px!important;
		    left: 8px!important;
		}
		#strukCheckModal .btn-granit,
		#strukModal .btn-granit,
		#strukCheckModal .btn-keramik,
		#strukModal .btn-keramik{
		    font-size: 10px;
		}
		<?php } ?>

		.swithproduksi-div > div > label{
			padding-right: 20px!important;
		}

		#strukCheckModal .toggle.btn-xs,
		#strukModal .toggle.btn-xs{
			min-height: 1.2rem;
		}
		.btn-group-xs>.btn, .btn-xs {
		    padding: .35rem .3rem .15rem .3rem!important;
		    font-size: 0.75rem!important;
		    line-height: .4!important;
		}
	}

	/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
	<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Tambah Slip Gaji</h1>
        </div>
	</div>

	<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
	<div class="row mb-4">
		<div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
			    <a class="btn btn-sm btn-add" href="<?php echo site_url('web/'.$class)?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            </span>
        </div>
	</div>    

    <div class="row harga-add">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<form action="<?php echo site_url('web/'.$class)?>/<?= ($method=='edit' ? 'update' : 'create') ?>" method="post">
	        	<input type="hidden" name="idorder" value="<?= $idorder ?>">
	           	<div class="form-group row">
	           		<label class="col-6 col-sm-6 col-md-2">Staf</label>
           			<div class="col-6 col-sm-6 col-md-4 text-right">
           				<input type="hidden" name="id_user" id="id_user" value="0">
	           			<button type="button" class="btn btn-sm btn-add" data-target="#pilihUserModal" data-toggle="modal"><i class="fas fa-list nav-icon"></i></button>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Nama</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="text" name="nama" id="nama" class="form-control form-control-sm" value="<?= (!empty($data['name'])) ? $data['name'] : '' ?>" required>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">No HP</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="text" name="no_hp" id="no_hp" class="form-control form-control-sm" value="<?= (!empty($data['no_hp'])) ? $data['no_hp'] : ''; ?>" required>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Gaji Pokok</label>
	           		<div class="col-12 col-sm-12 col-md-3">
	           			<input type="number" name="gaji_pokok" id="gaji_pokok" class="form-control form-control-sm"  required>
	           			
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Tunjangan</label>
	           		<div class="col-12 col-sm-12 col-md-3">
	           			<input type="number" name="tunjangan" id="tunjangan" class="form-control form-control-sm"  required>
	           			
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Fee Desainer</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Fee Eksekusi</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Total Take Home Pay</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			
	           		</div>
	           	</div>

	           	<div class="form-group text-center mt-5">
	           		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
	           	</div>
        	</form>
        </div>        
  	</div>

	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg">
          <div class="modal-content" style="border-color: #007fff;">
             <div class="modal-body" style="max-height: calc(100vh - 210px);overflow-y: auto;">
               <fieldset style="display: block;">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row">
                        
                    </div>
               </fieldset>
             </div>
          </div>
       </div>
    </div>

    <!-- Pilih Konsumen Modal -->
	<div class="modal fade" id="pilihUserModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Pilih...</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="col-md-12">

	  			<div class="form-group row search-deal">
	           		<div class="col-12 col-sm-12 col-md-10">
						<input type="text" name="keyword-deal" id="keyword-deal" class="form-control form-control-sm" placeholder="Keyword">
	           		</div>
	           		<div class="col-12 col-sm-12 col-md-2">
	           			<button class="btn btn-primary" type="button" onclick="cariDeal()"> Cari</button>
	           		</div>
	           	</div>
	      	
	      	</div>

	      	<div class="col-md-12 list-deal">

		        <?php foreach ($user as $key): ?>
					<div class="row mt-2 row-order-baru pilih-user" style="cursor:pointer;color:#212529; ">
						<div class="col-7 col-sm-6 col-md-9 list-order-baru">
							<input type="hidden" class="id_user" value="<?= $key['id_user']  ?>">
							<label class="nama"><?= $key['name']  ?></label>
							<p class="no_hp"><?= $key['no_hp']  ?></p>
						</div>
						<div class="col-5 col-sm-6 col-md-3 list-order-baru text-right">
							<label></label>
							<div class="status-produksi">
								<?= $key['username'] ?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>

			<div id="right">
				<div id="loader" style="display: none">Loading ...</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>

	

<script>

	
	$(document).ready(function(){

		$(document).on('click','.pilih-user', function(){
			var id_user = $(this).find('.id_user').html();
			var nama = $(this).find('.nama').html();
			var no_hp = $(this).find('.no_hp').html();

			$("#id_user").val(id_user);
			$("#nama").val(nama);
			$("#no_hp").val(no_hp);
			$('#pilihUserModal').modal('hide');
		})
		
	

	})

	

</script>
