<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Usulkan Jadwal Pemasangan</h1>
        </div>
	</div>

<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
	<div class="row mb-4">
		<div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
			    <a class="btn btn-sm btn-add" href="<?php echo site_url('web/komplain/komplainbaru')?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            </span>
        </div>
	</div>    

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<form action="<?= site_url('web/komplain/usulan/'.$detail['id']) ?>" method="post">
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Konsumen</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<div class="konsumen-temp" style="font-size: 12px;">
	           				<div class="row">
	           					<div class="col-4 col-sm-4 col-md-3">Kode Order</div>
	           					<div class="col-8 col-sm-8 col-md-8"><?= $detail['kode_order']?></div>
	           				</div>
							<div class="row">
								<div class="col-4 col-sm-4 col-md-3">Nama</div>
								<div class="col-8 col-sm-8 col-md-8"><?= $detail['nama_konsumen']?></div>
							</div>
							<div class="row">
								<div class="col-4 col-sm-4 col-md-3">Alamat</div>
								<div class="col-8 col-sm-8 col-md-8"><?= $detail['alamat_konsumen']?></div>
							</div>
	           			</div>
	           			<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI): ?>
	           			<button type="button" class="btn btn-sm btn-outline-secondary btn-block btn-pilih-konsumen" data-toggle="modal" data-target="#pilihKonsumenModal">Ubah</button>
	           			<?php endif ?>
						
	           		</div>
	           	</div>

	           	<div class="form-group row" id="rencanakerja" style="display: none;">
	           		<label class="col-12 col-sm-12 col-md-2">Rencana Kerja</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<button type="button" class="btn btn-sm btn-outline-secondary btn-block" disabled>PASANG KUSEN</button>

	           		</div>
	           	</div>

	           	<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI): ?>
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Mitra Pemasang</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<div class="mitrapemasang-temp" style="font-size: 12px;">
	           				<?php if(!empty($user_mitra_pemasang)) { ?>
           					<div class="row">
								<div class="col-4 col-sm-4 col-md-3">Nama</div>
								<div class="col-8 col-sm-8 col-md-8"><?= $user_mitra_pemasang['name']?></div>
							</div>
							<div class="row">
								<div class="col-4 col-sm-4 col-md-3">No Hp</div>
								<div class="col-8 col-sm-8 col-md-8"><?= $user_mitra_pemasang['no_hp']?></div>
							</div>
	           				<?php } ?>
	           			</div>
						<input type="hidden" name="id_mitra_pemasang" id="id_mitra_pemasang" value="<?= (!empty($user_mitra_pemasang)) ? $user_mitra_pemasang['id'] : ''; ?>">
						<button type="button" class="btn btn-sm btn-outline-secondary btn-block btn-pilih-mitrapemasang" data-toggle="modal" data-target="#pilihMitraPemasangModal"><?= (!empty($user_mitra_pemasang)) ? 'Ubah' : '-- Pilih Mitra Pemasang --'; ?></button>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Catatan</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<textarea class="form-control" disabled><?= $detail['catatan']  ?></textarea>
	           		</div>
	           	</div>
	           	<?php endif ?>
	           	

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Waktu</label>
	           		<div class="col-12 col-sm-12 col-md-4">
						<button type="button" class="btn btn-sm btn-outline-secondary btn-block btn-pilih-tanggal">-- Pilih Tanggal --</button>
						<input type="text" name="" class="datepicker" style="visibility:hidden;position: absolute;top: 50px;">
						<input type="hidden" name="jadwal_usulan" id="jadwal_usulan">
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Jam</label>
	           		<div class="col-12 col-sm-12 col-md-2">
						<div class="input-group clockpicker">
							<input type="text" name="jam_usulan" class="form-control form-control-sm" value="" readonly required style="background-color: transparent;">
						</div>
	           		</div>
	           	</div>

	           	<div class="form-group text-center mt-5">
	           		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
	           	</div>
        	</form>
        </div>
  	</div>

<!-- Pilih Konsumen Modal -->
<div class="modal fade" id="pilihKonsumenModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pilih...</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php foreach ($komplain_baru as $key): ?>
			<div class="row mt-2 row-order-baru pilih-konsumen" style="cursor:pointer;color:#212529; ">
				<div class="col-7 col-sm-6 col-md-8 list-order-baru">
					<label class="kode_order"><?= $key['kode_order']  ?></label>
					<p class="nama_user"><?= $key['nama_user']  ?></p>
					<p class="alamat_konsumen"><?= substr($key['alamat_konsumen'],0,50).'...'  ?></p>
				</div>
				<div class="col-5 col-sm-6 col-md-4 list-order-baru text-right">
					<label></label>
					<p><?= $key['no_hp_konsumen']  ?></p>
					<div class="status-produksi">
						<?php if($key['status_produksi'] == 1) {
							echo '<span style="background-color: #4caf50;">Bisa Cek Ruangan</span>';
						}elseif($key['status_produksi'] == 2) {
							echo '<span style="background-color: #4caf50;">Bisa Pasang Rangka</span>';
						}elseif($key['status_produksi'] == 3) {
							echo '<span style="background-color: #f71212;">Menunggu Cor Siap</span>';
						} ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
      </div>
    </div>
  </div>
</div>

<!-- Pilih Mitra Pemasang Modal -->
<div class="modal fade" id="pilihMitraPemasangModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pilih...</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<?php foreach ($mitra_pemasang as $key => $value): ?>
      	<div class="row mt-4 row-order-baru pilih-mitrapemasang" style="cursor:pointer;text-decoration:none;color:#212529">
			<div class="col-7 col-sm-6 col-md-9 list-order-baru">
				<label class="nama-pemasang"><?= $value['name']  ?></label>
				<p class="nohp-pemasang"><?= $value['no_hp'] ?></p>
				<p class="id_mitra_pemasang" style="display:none"><?= $value['id'] ?></p>
			</div>
			<div class="col-5 col-sm-6 col-md-3 list-order-baru text-right">
				<label></label>
				<p></p>
				<div class="status-produksi">
					<?= $value['username']  ?>
				</div>
			</div>
		</div>
      	<?php endforeach ?>
        
      </div>
    </div>
  </div>
</div>

<script>
	$(document).ready(function(){
		$(document).on('click','.pilih-konsumen', function(){
			var kode_order = $(this).find('.kode_order').html();
			var nama_user = $(this).find('.nama_user').html();
			var alamat_konsumen = $(this).find('.alamat_konsumen').html();

			var html = '<div class="row"><div class="col-4 col-sm-4 col-md-3">Kode Order</div><div class="col-8 col-sm-8 col-md-8">'+kode_order+'</div></div>'+
			'<div class="row"><div class="col-4 col-sm-4 col-md-3">Nama</div><div class="col-8 col-sm-8 col-md-8">'+nama_user+'</div></div>'+
			'<div class="row"><div class="col-4 col-sm-4 col-md-3">Alamat</div><div class="col-8 col-sm-8 col-md-8">'+alamat_konsumen+'</div></div>';
			$('.konsumen-temp').html(html);
			$('.btn-pilih-konsumen').html('Ubah');
			$('#rencanakerja').show();
			$('#pilihKonsumenModal').modal('hide');
		})

		$(document).on('click','.pilih-mitrapemasang', function(){
			var nama_pemasang = $(this).find('.nama-pemasang').html();
			var nohp_pemasang = $(this).find('.nohp-pemasang').html();
			var html = 
			'<div class="row"><div class="col-4 col-sm-4 col-md-3">Nama</div><div class="col-8 col-sm-8 col-md-8">'+nama_pemasang+'</div></div>'+
			'<div class="row"><div class="col-4 col-sm-4 col-md-3">No Hp</div><div class="col-8 col-sm-8 col-md-8">'+nohp_pemasang+'</div></div>';
			$('.mitrapemasang-temp').html(html);
			$('.btn-pilih-mitrapemasang').html('Ubah');
			$('#pilihMitraPemasangModal').modal('hide');
			$('#id_mitra_pemasang').val($(this).find('.id_mitra_pemasang').html());
		})

		$('.btn-pilih-tanggal').on('click', function() {
			// alert();
		      $('.datepicker').datepicker('show');
		});

		var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
		  "Juli", "Agustus", "September", "Oktober", "November", "Desember"
		];
		$('.datepicker').datepicker({
		    format: 'yyyy-mm-dd',
		    autoclose: true
		}).on('change', function(e) {
		    var tanggal = $('.datepicker').val();
		    var dt = new Date(tanggal);
		    var html = dt.getDate()+' '+monthNames[dt.getMonth()]+' '+dt.getFullYear();
		    $('.btn-pilih-tanggal').html(html);
			$('#jadwal_usulan').val(tanggal);

		    // alert(monthNames[dt.getMonth()]);
		    // alert(dt.getFullYear());
		    // alert(dt.getDate());
		});

		$('.clockpicker').clockpicker({
			donetext: 'Done'
		});
	})
</script>