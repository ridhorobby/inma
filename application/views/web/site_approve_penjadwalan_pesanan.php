
<?php if(!$pesanan_id) { ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title><?php echo $title; ?></title>

<link rel="icon" href="<?php echo base_url()?>assets/web/img/favicon.png">

<link href="<?php echo base_url();?>assets/web/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="<?php echo base_url();?>assets/web/vendor/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/web/css/login.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/web/vendor/jquery/jquery-1.12.0.min.js"></script>
<script src="<?php echo base_url();?>assets/web/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/web/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url();?>assets/web/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!--
    you can substitue the span of reauth email for a input with the email and
    include the remember me checkbox
    -->
</head>
<body>

        <div class="container">
      <div class="row">
        <div class="col-12">

          <div class="card card-container">
            <div style="text-align: center;">
              <img id="profile-img" class="" src="<?php echo base_url();?>assets/web/img/logo.png" style="width: 125px;"/>
            </div>
              <p id="profile-name" class="profile-name-card"></p>
              <form action="<?php echo site_url('web/site/approve_penjadwalan_pesanan/'.$konsumen_id)?>" method="post">
                    <?php if(!empty($errmsg)) { ?>
                    <div class="login-message" style="margin-bottom: 3%">
                        <?php echo implode('<br>',$errmsg) ?>
                    </div>
                    <?php } ?>
                    <div class="form-group">                    
                        <input type="text" name="kode_order"  class="form-control" placeholder="Masukkan Kode Order" required autofocus>
                    </div>
                    <div class="form-group mb-0">                    
                        <button type="submit" class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Kirim</button>
                    </div>
              </form><!-- /form -->
          </div><!-- /card-container -->
        </div>
      </div>
    </div><!-- /container -->
    
        
        
    
</body>
</html>
<?php } else {
  include('_konfirmasi_penjadwalan_pesanan.php');
}?>