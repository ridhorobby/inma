<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="<?php echo base_url()?>assets/web/img/favicon.png" rel="icon">
</head>
<body style="font-family: 'helvetica';">
	<style>
		label {
			font-weight: bold;
		}
		.text-red{
			color: #f71212;
		}
		.table-harga th,
		.table-harga td{
			border-top: 1px solid #777;
		}
		.table-harga .th-harga,
		.table-harga .th-subtotal,
		.table-harga .th-jml,
		.table-harga .jml,
		.table-harga .granit,
		.table-harga .keramik{
			text-align: center;
		}
		.btn-granit{
			background-color: #dee2e6;
		    padding: 5px 50px;
		}
		.btn-keramik{
			background-color: #dcedc8;
		    padding: 5px 50px;
		}
		.share-icon{
		    cursor: pointer;
			float: right;
			right: 10%;
			position: absolute;
			color: #f71212;
		}
		.logo-nasatech{
			position: absolute;
		    right: 0%;
		    top: 25px;
		}
		.logo-nasatech > label{
			font-weight: 400;
		    font-size: 2em;
		    margin: 0;
		}
		.logo-nasatech > label > b{
			color: #f71212;
		}
		.logo-nasatech > span{
		    font-size: .75rem;
		    margin-left: 5rem;
		}
	</style>
		<div class="logo-nasatech">
    		<label>Nasa<b>tech</b></label>
    		<br>
    		<span>Kitchenset</span>
    	</div>
		<table style="margin-bottom: 15px;">
    		<tr>
    			<td width="100px"><label>Kode Order</label></td>
    			<td><label class="text-red"><?= $data['kode_order']?></label></td>
    		</tr>
    		<tr>
    			<td style="font-weight: bold;">Nama</td>
    			<td><?= $data['nama']?></td>
    		</tr>
    		<tr>
    			<td style="font-weight: bold;">Alamat</td>
    			<td><?= $data['alamat']?></td>
    		</tr>
    		<tr>
    			<td style="font-weight: bold;">No HP</td>
    			<td><?= $data['no_hp']?></td>
    		</tr>
    	</table>
    	<?php $bgcolor = ($jenis == 'granit') ? "#dee2e6" : '#dcedc8'; ?>
		
    	<table class="table table-sm table-harga" style="width: 100%;">
    		<tr>
    			<th>No</th>
    			<th>Item</th>
    			<th class="th-jml">Qty</th>
    			<th class="th-harga" style="background-color: <?= $bgcolor ?>;">@Harga</th>
    			<th class="th-subtotal" style="background-color: <?= $bgcolor ?>;">Subtotal</th>
    		</tr>
          <tbody>
          <?php 
          $no = 1;
          $totalharga = 0;

          foreach ($detail as $v) { 
        			$totalharga += $v['harga_maintenance']*$v['jumlah'];
        			
        			?>
		            <tr id="<?= $v['id']?>" class="rowharga">
		              <td><?= $no++; ?></td>
		              <td><?= $v['nama']; ?></td>
		              <td class="jml"><?= number_format($v['jumlah'],1)?></td>


	              		<td class="granit" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_maintenance'],0,",",".")?></td>
	              		<td class="granit" style="background-color: #dee2e6;<?= $granit_red; ?>"><?= number_format($v['harga_maintenance']*$v['jumlah'],0,",",".")?></td>

				       

		            </tr>
          <?php 
          } ?>
          <tr>
          	<td colspan="3" style="text-align: right"><b>Total</b></td>
    		      	<td colspan="2" class="granit" style="text-align: right"><b><?= number_format($totalharga,0,",",".") ?></b></td>
	        	
          		
          	</b></td>
          </tr>

   
          </tbody>
        </table>
</body>
</html>