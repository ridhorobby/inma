	<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Input Pembukuan</h1>
        </div>
	</div>

	<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
	<div class="row mb-4">
		<div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
			    <a class="btn btn-sm btn-add" href="<?php echo site_url('web/'.$class)?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            </span>
        </div>
	</div>    

    <div class="row harga-add">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<form action="<?php echo site_url('web/'.$class)?>/<?= ($method=='edit' ? 'update' : 'create') ?>" method="post">
        	<?php if($method=='edit'){ ?>
	          <input type="hidden" name="id" value="<?= $data['id'] ?>">
	        <?php } ?>
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Nominal</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="number" name="nominal" class="form-control form-control-sm" min="0" value="<?= ($method=='edit' ? $data['nominal'] : '') ?>" required>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Kode Order</label>
	           		<div class="col-12 col-sm-12 col-md-4" style="display: flex;">
	           			<input type="hidden" name="id_konsumen" id="konsumen_id" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['id_konsumen'] : '') ?>">
	           			<input type="hidden" name="kode_order" id="kode_order" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['kode_order'] : '') ?>">
	           			<button type="button" class="btn btn-sm btn-outline-secondary btn-block btn-pilih-konsumen" data-toggle="modal" data-target="#pilihKonsumenModal"><?= ($method=='edit' ? $data['kode_order'] : '-- Pilih Konsumen --') ?></button><span style="padding: 3px 10px;cursor: pointer;" onclick="$('#kode_order').val('');$('.btn-pilih-konsumen').html('-- Pilih Konsumen --');"><i class="fas fa-times"></i></span>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Jenis Pembayaran</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<select name="jenis" class="form-control form-control-sm" id="jenispembayaran" required>
	           				<option hidden="true">-- Pilih Jenis --</option>
	           				<?php foreach ($listjenis as $key => $obj) { ?>	           					
	                          <option value="<?= $obj['id'] ?>" tipe="<?= $obj['tipe']?>" tipetext="<?= ($obj['tipe'] == 1? 'KREDIT' : 'DEBIT') ?>" <?= ($obj['id'] == $data['jenis'] ? 'selected' : '') ?>> 
	                          	<b><?= $obj['nama']  ?></b> (<?= ($obj['tipe'] == 1? 'KREDIT' : 'DEBIT')?>)                           	
	                          </option>
	           				<?php } ?>
	           			</select>
	           		</div>
	           	</div>

	           	<div class="form-group row sub-tipe-pembayaran" style="display: none;">
	           		<label class="col-12 col-sm-12 col-md-2"></label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<select name="subjenis" class="form-control form-control-sm" id="subjenispembayaran" disabled required>
	           				<option hidden="true">-- Pilih Sub Jenis --</option>
	           			</select>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Debit / Kredit</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<?php if ($method=='edit') {
		           			$tipetext = ($data['tipe']) == 1? 'KREDIT' : 'DEBIT';
	           			}?>
	           			<input type="hidden" name="tipe" class="form-control form-control-sm" value="<?= $method=='edit' ? $data['tipe'] : '' ?>" id="tipe">
	           			<input type="text" class="form-control form-control-sm" id="tipe_text" value="<?= $method=='edit' ? $tipetext : '' ?>" readonly>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Tanggal</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<button type="button" class="btn btn-sm btn-outline-secondary btn-block btn-pilih-tanggal"><?= xFormatDateInd(date('Y-m-d'))?></button>
	           			<input type="text" name="tgl_input" class="form-control form-control-sm" id="tgl_input" value="<?= $method=='edit' ? $data['tgl_input'] : date('Y-m-d') ?>" readonly style="visibility:hidden">
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Keterangan</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="text" name="keterangan" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['keterangan'] : '') ?>">
	           		</div>
	           	</div>
	           	<div class="form-group text-center mt-5">
	           		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
	           	</div>
        	</form>
        </div>

        <?php if($method == 'edit') { ?>
	        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style="background-color: #eee">	
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Diinput Oleh </label>
		           	<div class="col-12 col-sm-12 col-md-4">
	           			<input type="text" class="form-control form-control-sm" value="<?= $data['created_by_nama'] ?>" readonly>
	           		</div>
	           	</div>
	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Diupdate Oleh </label>
	           		<div class="col-12 col-sm-12 col-md-4">
		           		<input type="text" class="form-control form-control-sm" value="<?= $data['updated_by_nama'] ?>" readonly>
		           	</div>
	           	</div>
	           	<div class="form-group text-center mt-5">
	           		<a class="btn btn-sm btn-add" href="#" onclick="deleteKas(<?= $data['id']?>);"><i class="fas fa-save"></i> Hapus</a>
	           	</div>
	        </div>
        <?php } ?>
  	</div>

	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg">
          <div class="modal-content" style="border-color: #007fff;">
             <div class="modal-body" style="max-height: calc(100vh - 210px);overflow-y: auto;">
               <fieldset style="display: block;">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row">
                        
                    </div>
               </fieldset>
             </div>
          </div>
       </div>
    </div>

    <!-- Pilih Konsumen Modal -->
	<div class="modal fade" id="pilihKonsumenModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Pilih...</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="col-md-12">

	  			<div class="form-group row search-deal">
	           		<div class="col-12 col-sm-12 col-md-10">
						<input type="text" name="keyword-konsumen" id="keyword-konsumen" class="form-control form-control-sm" placeholder="Keyword">
	           		</div>
	           		<div class="col-12 col-sm-12 col-md-2">
	           			<button class="btn btn-primary" type="button" onclick="cariKonsumen()"> Cari</button>
	           		</div>
	           	</div>
	      	
	      	</div>

	      	<div class="col-md-12 list-konsumen">
	      		<?php foreach ($listkonsumen as $key): ?>
					<div class="row mt-2 row-order-baru pilih-konsumen" style="cursor:pointer;color:#212529; ">
						<div class="col-7 col-sm-6 col-md-8 list-order-baru">
							<p class="konsumen_id" style="display: none;"><?= $key['konsumen_id']  ?></p>
							<label class="kode_order"><?= $key['kode_order']  ?></label>
							<p class="nama_konsumen"><?= $key['nama_konsumen']  ?></p>
							<p class="alamat_konsumen"><?= substr($key['alamat_konsumen'],0,50).'...'  ?></p>
						</div>
						<div class="col-5 col-sm-6 col-md-4 list-order-baru text-right">
							<label></label>
							<p><?= $key['no_hp_konsumen']  ?></p>
							<div class="status-produksi">
								<?php if($key['status_produksi'] == 1) {
									echo '<span style="background-color: #4caf50;">Bisa Cek Ruangan</span>';
								}elseif($key['status_produksi'] == 2) {
									echo '<span style="background-color: #4caf50;">Bisa Pasang Rangka</span>';
								}elseif($key['status_produksi'] == 3) {
									echo '<span style="background-color: #f71212;">Menunggu Cor Siap</span>';
								} ?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>

	      	</div>
	      	<div id="right">
				<div id="loader" style="display: none">Loading ...</div>
			</div>
	        
	      </div>
	    </div>
	  </div>
	</div>

<script>

	$(document).ready(function(){
		$('#jenispembayaran').change(function(){
			var id = $('option:selected',this).val();
			var idjenis = $('option:selected',this).attr('tipe');
			var jenis_text = $('option:selected',this).attr('tipetext');
			// console.log(idjenis);
			$('#tipe').val(idjenis);
			$('#tipe_text').val(jenis_text);

			if(id == 5){
				$('.sub-tipe-pembayaran').show();

				
				$.ajax({
					type: "post",
					url: "<?= site_url('web/pembayaran_log') ?>/ajaxgetsubjenispembayaran",
					data: {id: id},
					success:function(respon){
						$('#subjenispembayaran').html('<option>-- Pilih Sub Jenis --</option>');
						if(respon!='false'){
							var data = JSON.parse(respon);

							$.each( data, function( k, v ) {
								$('#subjenispembayaran').append('<option value="'+v.id+'">'+v.nama+'</option>');
							});
						}

						$('#subjenispembayaran').prop('disabled', false);
						$('#subjenispembayaran').prop('required',true);

			        }
				});
			}else{
				$('.sub-tipe-pembayaran').hide();
			}
		})

		$(document).on('click','.pilih-konsumen', function(){
			var kode_order = $(this).find('.kode_order').html();
			var konsumen_id = $(this).find('.konsumen_id').html();
		
			$('#kode_order').val(kode_order);
			$('#konsumen_id').val(konsumen_id);
			$('.btn-pilih-konsumen').html(kode_order);
		
			$('#pilihKonsumenModal').modal('hide');
		})
		
		$('.btn-pilih-tanggal').on('click', function() {
		      $('#tgl_input').datepicker('show');
		});

		var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
		  "Juli", "Agustus", "September", "Oktober", "November", "Desember"
		];
		$('#tgl_input').datepicker({
		    format: 'yyyy-mm-dd',
		    autoclose: true
		}).on('change', function(e) {
		    var tanggal = $('#tgl_input').val();
		    var dt = new Date(tanggal);
		    var html = dt.getDate()+' '+monthNames[dt.getMonth()]+' '+dt.getFullYear();
		    $('.btn-pilih-tanggal').html(html);
		    // alert(monthNames[dt.getMonth()]);
		    // alert(dt.getFullYear());
		    // alert(dt.getDate());
		});
	})

	function deleteKas(id){
		if(!confirm("Apakah anda akan menghapus order ini ?")){
		  	return false;
		}

		window.location.href = '<?= site_url('web/pembayaran_log/delete')?>/'+id;
	}

	function cariKonsumen(){
		$('#loader').fadeIn('slow');
		$('.list-konsumen').empty();
		var keyword = $('#keyword-konsumen').val();
		$.ajax({
			type: "post",
			url: "<?= site_url('web/pembayaran_log') ?>/add",
			data: {keyword: keyword,ajax:1},
			success:function(respon){
				var html = '';
				if(respon!='false'){
					var data = JSON.parse(respon);

	         		
	         		for (var i =0; i < data.length; i++) {
	         			alamat = data[i].alamat_konsumen;
	         			html += 
	         				'<div class="row mt-2 row-order-baru pilih-konsumen" style="cursor:pointer;color:#212529; ">'+
		         				'<div class="col-7 col-sm-6 col-md-8 list-order-baru">'+
			         				'<input type="hidden" class="konsumen_id" value="'+data[i].konsumen_id+'">'+
									'<label class="kode_order">'+data[i].kode_order+'</label>'+
									'<p class="nama_konsumen">'+data[i].nama_konsumen+'</p>'+
									'<p class="alamat_konsumen">'+alamat+'...</p>'+
									'<p style="display: none;" class="alamat_konsumen_extend">'+data[i].alamat_konsumen+'  ?></p>'+
									'<p style="display: none;" class="no_hp_konsumen">'+data[i].no_hp_konsumen+'</p>'+
								'</div>'+
								'<div class="col-5 col-sm-6 col-md-4 list-order-baru text-right">'+
									'<label></label>'+
									'<p>'+data[i].no_hp_konsumen+'</p>'+
									'<div class="status-produksi">';

										if (data[i].status_produksi == 1) {
											html += '<span style="background-color: #4caf50;">Bisa Cek Ruangan</span>';
										}
										else if(data[i].status_produksi == 2){
											html += '<span style="background-color: #4caf50;">Bisa Pasang Rangka</span>';
										}
										else if(data[i].status_produksi == 3){
											html +='<span style="background-color: #f71212;">Menunggu Cor Siap</span>';
										}

								html +=
									'</div>'+
								'</div>'+
							'</div>';
		         					
	         		}
				}else{
					html+='<div>Data tidak ditemukan..</div>';
				}

         		
         		$('.list-konsumen').html(html);
         		$('#loader').css("display", "none");
	        	
	        }
		});
		console.log(keyword);
	}

	<?php if ($method=='edit') { ?>
	
	getSubJenis();
	
	function getSubJenis(){
		var id = $('#jenispembayaran').val(); 
		var idsubjenis = <?= $data['subjenis']?>;
		$.ajax({
			type: "post",
			url: "<?= site_url('web/pembayaran_log') ?>/ajaxgetsubjenispembayaran",
			data: {id: id},
			success:function(respon){
				$('#subjenispembayaran').html('<option>-- Pilih Sub Jenis --</option>');
				if(respon!='false'){
					var data = JSON.parse(respon);

					$.each( data, function( k, v ) {

						if (v.id==idsubjenis) {
							selected = 'selected';
						}else{
							selected = '';
						}

						$('#subjenispembayaran').append('<option value="'+v.id+'" '+selected+'>'+v.nama+'</option>');
					});
				}

				$('#subjenispembayaran').prop('disabled', false);
				$('#subjenispembayaran').prop('required',true);

	        }
		});
	}
	<?php } ?>

</script>
