<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $title ?></title>
	<!-- Favicons -->
	<link href="<?php echo base_url()?>assets/web/img/favicon.png" rel="icon">
	<!-- Google Font: Source Sans Pro -->
	<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> -->
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/fontawesome-free/css/all.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/web/css/adminlte.css">

	<!-- jQuery -->
	<script src="<?php echo base_url()?>assets/web/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url()?>assets/web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url()?>assets/web/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    <a href="index3.html" class="brand-link" style="background-color: #424242;">
	      <label class="logo-title">Nasa<b>tech</b></label><br>
	      <span class="logo-sub-title">Kitchenset</span>
    </a>
    <nav class="main-header navbar navbar-expand navbar-red-2 navbar-light" style="height: 56px;">
	    <!-- Left navbar links -->
	    <!-- <ul class="navbar-nav">
	      <li class="nav-item">
	        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
	      </li>
	    </ul> -->

	    <!-- Right navbar links -->
	    <ul class="navbar-nav ml-auto">
	    	<li class="nav-item dropdown no-arrow">
	              <!-- Dropdown - User Information -->
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
	    </ul>
	</nav>


    <div class="content-wrapper" style="margin-left: 0;">
        <!-- Main content -->
        <section class="content mt-5">
            <div class="container-fluid">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/web/plugins/evo-calendar/css/evo-calendar.css">
<style>
	.event-container > .event-icon{
	    margin: 25px 15px 0 10px;
	}
	.event-container::before{
		width: 1.5px;
		left: 5px;
		z-index: 0;
	    background-color: #757575;
	}
	.event-container:hover{
	    background-color: transparent;
	}
	
	@media screen and (max-width: 1280px) {
		.event-container::before{
			left: -17.5px;
			top: 10px;
		}
		.event-container > .event-icon {
		    margin: 25px 10px 0 0px;
		}
		.event-container > .event-info > p.event-title > span{
			right: -30px;
		}
	}
</style>
<div class="row mb-4">
	<div class="col-sm-12 col-md-12 title-page">
        <h1>Detail Komplain</h1>
    </div>
</div>    

<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
<div class="row">
    <div class="col-sm-12 col-md-12 mb-4">
    	<h4>Konsumen</h4>
    	<div class="row">
    		<div class="col-4 col-sm-2 col-md-2">Kode Order</div>
    		<div class="col-8 col-sm-10 col-md-10">
    			<?= $detail['kode_order'] ?>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-4 col-sm-2 col-md-2">Nama</div>
    		<div class="col-8 col-sm-10 col-md-10">
    			<?= $detail['nama_konsumen'] ?>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-4 col-sm-2 col-md-2">Alamat</div>
    		<div class="col-8 col-sm-10 col-md-10">
    			<?= $detail['alamat_konsumen'] ?>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-4 col-sm-2 col-md-2">No HP</div>
    		<div class="col-8 col-sm-10 col-md-10">
    			<?= $detail['no_hp_konsumen'] ?>
    		</div>
    	</div>
    </div>
    
    <div class="col-sm-12 col-md-12 mb-4">
    	<h4>Gambar Kerja</h4>
    	<div class="row">
    		<?php foreach($detail['foto_order'] as $key) { ?>
    			<div class="col-6 col-sm-6 col-md-4">
    				<img src="<?= $key;?>" width="100%">
    			</div>
	    	<?php } ?>
    	</div>
	</div>

	<div class="col-sm-12 col-md-12 mb-4">
    	<h4>Status Kerja</h4>
    	<div class="row">
			<div class="col-md-12 status-kerja-div">
				<label><?= $detail['flow_name']?></label>
			</div>
    	</div>
	</div>

	<div class="col-sm-12 col-md-12 mb-4">
    	<h4>Detail Order</h4>
    	<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="event-list">
					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-selesai"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Order Masuk <span><?= xFormatDateInd($detail['tgl_order_masuk']) ?></span></p>
							<p class="event-desc"><?= $detail['nama_user']?></p>
						</div>
					</div>					

					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-holiday"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Cek Ruangan / Pasang Kusen <span><?= xFormatDateInd($detail['tanggal_pasang_kusen']) ?></span></p>
							<p class="event-desc"><?= $detail['nama_pemasang_kusen']?></p>
						</div>
					</div>					

					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-pemasangan"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Pemasangan Unit <span><?= xFormatDateInd($detail['tanggal_pasang_finish']) ?></span></p>
							<p class="event-desc"><?= $detail['nama_pemasang_finish']?></p>
						</div>
					</div>					

					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-birthday"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Menunggu Set Pemasangan Selesai <span><?= xFormatDateInd($detail['tgl_order_masuk']) ?></span></p>
							<p class="event-desc"><?= $detail['nama_konsumen']?></p>
						</div>
					</div>					
				</div>
			</div>
    	</div>
	</div>

	<?php if($detail['flow_id'] == 3 || $detail['flow_id'] == 6) { ?>
	<div class="col-sm-12 col-md-12 mb-4 text-center">
        <a class="btn btn-add" href="<?php echo site_url('web/site/tolakkomplainkonsumen/'.$detail['id'])?>"><i class="fas fa-times"></i> Tolak</a>
	    <a class="btn btn-success" href="<?php echo site_url('')?>"><i class="fas fa-check"></i> Setuju</a>
    </div>
	<?php } ?>
</div>

</div>
</section>

</div>

<div class="clearfix"></div>

<footer class="main-footer">
  <!-- <strong>Copyright &copy; 2021 <a href="#">Nasatech INMA</a>.</strong> -->
  <div class="float-right d-none d-sm-inline-block">
    <!-- <b>Version</b> 1.1.72 -->
  </div>
</footer>

<!-- overlayScrollbars -->
<script src="<?php echo base_url()?>assets/web/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>assets/web/js/adminlte.js"></script>


</div>
</body>


</html>