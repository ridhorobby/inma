	<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>KPI Marketing</h1>
        </div>
	</div>

	<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
	<div class="row mb-4">
		<div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
            </span>
        </div>
	</div>    

    <div class="row harga-add">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<form action="<?php echo site_url('web/'.$class)?>/getkpimarketing" method="post">
        		
        		<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Satuan</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<select name="satuan" class="form-control form-control-sm" id="satuan" required>
	           				<!-- <option value="">-- Pilih Satuan --</option> -->
	           				<option value="1" <?php if ($_SESSION['kpi_marketing']['satuan'] == 1) { echo 'selected'; } ?> >Hari</option>
	           				<option value="2" <?php if ($_SESSION['kpi_marketing']['satuan'] == 2) { echo 'selected'; } ?> >Minggu</option>
	           				<option value="3" <?php if ($_SESSION['kpi_marketing']['satuan'] == 3) { echo 'selected'; } ?> >Bulan</option>
	           			</select>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Tanggal Mulai</label>
	           		<div class="col-12 col-sm-12 col-md-2">
	           			<!-- <input type="text" name="start" class="form-control form-control-sm" id="tgl_start" autocomplete="off" value="<?php if ($_SESSION['kpi_marketing']['start']) { echo $_SESSION['kpi_marketing']['start']; } ?>"> -->

	           			<!-- <input type="text" name="start" class="form-control form-control-sm" id="tgl_start" autocomplete="off" onfocus="getCalendar(this)" value="<?php if ($_SESSION['kpi_marketing']['start']) { echo $_SESSION['kpi_marketing']['start']; } ?>"> -->
	           			
	           			<input type="text" name="start" class="form-control form-control-sm" id="tgl_start" autocomplete="off" value="<?php if ($_SESSION['kpi_marketing']['start']) { echo $_SESSION['kpi_marketing']['start']; } ?>">
	           			
	           			<input type="text" name="start" class="form-control form-control-sm weekday" id="weeklyDatePickerStart" autocomplete="off" value="<?php if ($_SESSION['kpi_marketing']['start']) { echo $_SESSION['kpi_marketing']['start']; } ?>" style="display:none;" disabled>

	           			<input type="text" name="start" class="form-control form-control-sm monthly" id="monthlyDatePickerStart" autocomplete="off" value="<?php if ($_SESSION['kpi_marketing']['start']) { echo $_SESSION['kpi_marketing']['start']; } ?>" style="display:none;" disabled>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Tanggal Akhir</label>
	           		<div class="col-12 col-sm-12 col-md-2">
	           			<!-- <input type="text" name="end" class="form-control form-control-sm" id="tgl_end" autocomplete="off" value="<?php if ($_SESSION['kpi_marketing']['end']) { echo $_SESSION['kpi_marketing']['end']; } ?>"> -->

	           			<input type="text" name="end" class="form-control form-control-sm" id="tgl_end" autocomplete="off" value="<?php if ($_SESSION['kpi_marketing']['end']) { echo $_SESSION['kpi_marketing']['end']; } ?>">
	           			
	           			<input type="text" name="end" class="form-control form-control-sm weekday" id="weeklyDatePickerEnd" autocomplete="off" value="<?php if ($_SESSION['kpi_marketing']['end']) { echo $_SESSION['kpi_marketing']['end']; } ?>" style="display:none;" disabled>

	           			<input type="text" name="end" class="form-control form-control-sm monthly" id="monthlyDatePickerEnd" autocomplete="off" value="<?php if ($_SESSION['kpi_marketing']['end']) { echo $_SESSION['kpi_marketing']['end']; } ?>" style="display:none;" disabled>

	           		</div>
	           	</div>

	           	<div class="form-group text-center mt-5">
	           		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-search"></i> Search</button>
	           	</div>
        	</form>
        </div>

	</div>


<script src="<?php echo base_url()?>assets/web/plugins/chart.js/Chart.min.js"></script>
<script src="<?php echo base_url()?>assets/web/plugins/moment/moment.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/web/plugins/jquery-ui/jquery-ui.css">
<script src="<?php echo base_url()?>assets/web/plugins/jquery-ui/jquery-ui.js"></script>

<style>
	.datepicker .datepicker-days table tbody tr:hover {
    background-color: #eee;
}
</style>
<?php if ($data) { ?>
	<div class="row harga-add mt-5">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style="overflow-x: auto;">
        	<table class="table table-bordered">
        		<tbody>
        			<?php 
    				echo "<tr>";
    				echo "<td><b>Parameter</b></td>";
        			foreach ($data as $k) {
        				echo "<td><b>".$k['label']."</b></td>";
        				$label[] = $k['label'];
        			} 
    				echo "<tr>";

    				echo "<tr>";
    				echo "<td><b>Lead</b></td>";
        			foreach ($data as $k) {
        				echo "<td>".$k['lead']."</td>";
        				$lead[] = $k['lead'];
        			} 
    				echo "<tr>";

    				echo "<tr>";
    				echo "<td><b>Deal</b></td>";
        			foreach ($data as $k) {
        				echo "<td>".$k['deal']."</td>";
        				$deal[] = $k['deal'];
        			} 
    				echo "<tr>";

    				echo "<tr>";
    				echo "<td><b>Persentase</b></td>";
        			foreach ($data as $k) {
        				echo "<td>".number_format($k['persentase'],0,",",".")."%</td>";
        				$persentase[] = number_format($k['persentase'],0,",",".");
        			} 
    				echo "<tr>";

    				echo "<tr>";
    				echo "<td><b>Total Omset</b></td>";
        			foreach ($data as $k) {
        				echo "<td>".number_format($k['omzet'],0,",",".")."</td>";
        				$omzet[] = $k['omzet'];
        			} 
    				echo "<tr>";

    				echo "<tr>";
    				echo "<td><b>Profit</b></td>";
        			foreach ($data as $k) {
        				echo "<td>".number_format($k['profit'],0,",",".")."</td>";
        				$profit[] = $k['profit'];
        			} 
    				echo "<tr>";

    				echo "<tr>";
    				echo "<td><b>Pengeluaran</b></td>";
        			foreach ($data as $k) {
        				echo "<td>".number_format($k['pengeluaran'],0,",",".")."</td>";
        				$pengeluaran[] = $k['pengeluaran'];
        			} 
    				echo "<tr>";

    				echo "<tr>";
    				echo "<td><b>Margin Profit</b></td>";
        			foreach ($data as $k) {
        				echo "<td>".number_format($k['margin_profit'],0,",",".")."</td>";
        				$margin_profit[] = $k['margin_profit'];
        			} 
    				echo "<tr>";

    				echo "<tr>";
    				echo "<td><b>Persentase Profit</b></td>";
        			foreach ($data as $k) {
        				echo "<td>".number_format($k['persentase_profit'],0,",",".")."%</td>";
        				$persentase_profit[] = number_format($k['persentase_profit'],0,",",".");
        			} 
    				echo "<tr>";
        			
        			?>
        		</tbody>
        	</table>
        </div>
    </div>

    <div class="row harga-add">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style="overflow-x: auto;">
		    <canvas id="myAreaChart"></canvas>
		</div>
	</div>


    <script>
    	console.log('<?= json_encode($label);?>');
    	var ctx = document.getElementById("myAreaChart");
    	ctx.height = 500;
		var myLineChart = new Chart(ctx, {
		  type: 'line',
		  data: {
		    // labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
		    labels: <?= json_encode($label);?>,
		    datasets: [{
		      label: "Lead",
		      lineTension: 0.3,
		      backgroundColor: "#fff",
		      borderColor: "rgba(78, 115, 223, 1)",
		      pointRadius: 3,
		      pointBackgroundColor: "rgba(78, 115, 223, 1)",
		      pointBorderColor: "rgba(78, 115, 223, 1)",
		      pointHoverRadius: 3,
		      pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
		      pointHoverBorderColor: "rgba(78, 115, 223, 1)",
		      pointHitRadius: 10,
		      pointBorderWidth: 2,
		      // data: [0, 100, 5000, 15000, 10000, 20000, 15000, 25000, 20000, 30000, 25000, 4000],
		      data: <?= json_encode($lead);?>,
		    },
		    {
		      label: "Deal",
		      lineTension: 0.3,
		      backgroundColor: "#fff",
		      borderColor: "#4caf50",
		      pointRadius: 3,
		      pointBackgroundColor: "#4caf50",
		      pointBorderColor: "#4caf50",
		      pointHoverRadius: 3,
		      pointHoverBackgroundColor: "#4caf50",
		      pointHoverBorderColor: "#4caf50",
		      pointHitRadius: 10,
		      pointBorderWidth: 2,
		      data: <?= json_encode($deal);?>,
		    },
		    {
		      label: "Persentase (%)",
		      lineTension: 0.3,
		      backgroundColor: "#fff",
		      borderColor: "#ffc107",
		      pointRadius: 3,
		      pointBackgroundColor: "#ffc107",
		      pointBorderColor: "#ffc107",
		      pointHoverRadius: 3,
		      pointHoverBackgroundColor: "#ffc107",
		      pointHoverBorderColor: "#ffc107",
		      pointHitRadius: 10,
		      pointBorderWidth: 2,
		      data: <?= json_encode($persentase);?>,
		    },
		    {
		      label: "Total Omzet",
		      lineTension: 0.3,
		      backgroundColor: "#fff",
		      borderColor: "#9c27b0",
		      pointRadius: 3,
		      pointBackgroundColor: "#9c27b0",
		      pointBorderColor: "#9c27b0",
		      pointHoverRadius: 3,
		      pointHoverBackgroundColor: "#9c27b0",
		      pointHoverBorderColor: "#9c27b0",
		      pointHitRadius: 10,
		      pointBorderWidth: 2,
		      data: <?= json_encode($omzet);?>,
		    },
		    {
		      label: "Profit",
		      lineTension: 0.3,
		      backgroundColor: "#fff",
		      borderColor: "#f012be",
		      pointRadius: 3,
		      pointBackgroundColor: "#f012be",
		      pointBorderColor: "#f012be",
		      pointHoverRadius: 3,
		      pointHoverBackgroundColor: "#f012be",
		      pointHoverBorderColor: "#f012be",
		      pointHitRadius: 10,
		      pointBorderWidth: 2,
		      data: <?= json_encode($profit);?>,
		    },
		    {
		      label: "Pengeluaran",
		      lineTension: 0.3,
		      backgroundColor: "#fff",
		      borderColor: "#f44336",
		      pointRadius: 3,
		      pointBackgroundColor: "#f44336",
		      pointBorderColor: "#f44336",
		      pointHoverRadius: 3,
		      pointHoverBackgroundColor: "#f44336",
		      pointHoverBorderColor: "#f44336",
		      pointHitRadius: 10,
		      pointBorderWidth: 2,
		      data: <?= json_encode($pengeluaran);?>,
		    },
		    {
		      label: "Margin Profit",
		      lineTension: 0.3,
		      backgroundColor: "#fff",
		      borderColor: "#607d8b",
		      pointRadius: 3,
		      pointBackgroundColor: "#607d8b",
		      pointBorderColor: "#607d8b",
		      pointHoverRadius: 3,
		      pointHoverBackgroundColor: "#607d8b",
		      pointHoverBorderColor: "#607d8b",
		      pointHitRadius: 10,
		      pointBorderWidth: 2,
		      data: <?= json_encode($margin_profit);?>,
		    },
		    {
		      label: "Persentase Profit (%)",
		      lineTension: 0.3,
		      backgroundColor: "#fff",
		      borderColor: "#795548",
		      pointRadius: 3,
		      pointBackgroundColor: "#795548",
		      pointBorderColor: "#795548",
		      pointHoverRadius: 3,
		      pointHoverBackgroundColor: "#795548",
		      pointHoverBorderColor: "#795548",
		      pointHitRadius: 10,
		      pointBorderWidth: 2,
		      data: <?= json_encode($persentase_profit);?>,
		    }],
		  },
		  options: {
		    maintainAspectRatio: false,
		    responsive: true,
		    layout: {
		      padding: {
		        left: 10,
		        right: 25,
		        top: 50,
		        bottom: 0
		      }
		    },
		    scales: {
		      xAxes: [{
		        time: {
		          unit: 'date'
		        },
		        gridLines: {
		          display: true,
		          drawBorder: true
		        },
		        ticks: {
		          // maxTicksLimit: 12
		        }
		      }],
		      yAxes: [{
		        ticks: {
		          // maxTicksLimit: 10,
		          // padding: 15,
		          // Include a dollar sign in the ticks
		          callback: function(value, index, values) {
		            // return number_format(value);
		            return formatRupiah(value);
		          }
		        },
		        gridLines: {
		          color: "rgb(234, 236, 244)",
		          zeroLineColor: "rgb(234, 236, 244)",
		          drawBorder: true,
		          borderDash: [2],
		          zeroLineBorderDash: [2]
		        }
		      }],
		    },
		    legend: {
		      display: true,
		      // labels: {
		      //   padding: 20
		      // },
		    
		    }
		  }
		});

	function formatRupiah(bilangan){

	    var bilangan = bilangan.toString().replace(/\./g,"");
	    var number_string = bilangan.toString(),
	    sisa    = number_string.length % 3,
	    rupiah  = number_string.substr(0, sisa),
	    ribuan  = number_string.substr(sisa).match(/\d{3}/g);
	        
	    if (ribuan) {
	        separator = sisa ? '.' : '';
	        rupiah += separator + ribuan.join('.');
	    }

	    return rupiah;
	}
    </script>
<?php } ?>
	
	
	<script>
		$(document).ready(function(){

			<?php if ($_SESSION['kpi_marketing']['satuan'] == 3) { ?>
				$('#tgl_start').prop('disabled', true);
				$('#weeklyDatePickerStart').prop('disabled', true);
				$('#monthlyDatePickerStart').prop('disabled', false);

				$('#tgl_start').css({'display':'none'});
				$('#weeklyDatePickerStart').css({'display':'none'});
				$('#monthlyDatePickerStart').css({'display':'block'});

				$('#tgl_end').prop('disabled', true);
				$('#weeklyDatePickerEnd').prop('disabled', true);
				$('#monthlyDatePickerEnd').prop('disabled', false);

				$('#tgl_end').css({'display':'none'});
				$('#weeklyDatePickerEnd').css({'display':'none'});
				$('#monthlyDatePickerEnd').css({'display':'block'});

			<?php }elseif($_SESSION['kpi_marketing']['satuan'] == 2){ ?>
				$('#tgl_start').prop('disabled', true);
				$('#weeklyDatePickerStart').prop('disabled', false);
				$('#monthlyDatePickerStart').prop('disabled', true);

				$('#tgl_start').css({'display':'none'});
				$('#weeklyDatePickerStart').css({'display':'block'});
				$('#monthlyDatePickerStart').css({'display':'none'});

				$('#tgl_end').prop('disabled', true);
				$('#weeklyDatePickerEnd').prop('disabled', false);
				$('#monthlyDatePickerEnd').prop('disabled', true);

				$('#tgl_end').css({'display':'none'});
				$('#weeklyDatePickerEnd').css({'display':'block'});
				$('#monthlyDatePickerEnd').css({'display':'none'});
			<?php }else{ ?>
				$('#tgl_start').prop('disabled', false);
				$('#weeklyDatePickerStart').prop('disabled', true);
				$('#monthlyDatePickerStart').prop('disabled', true);

				$('#tgl_start').css({'display':'block'});
				$('#weeklyDatePickerStart').css({'display':'none'});
				$('#monthlyDatePickerStart').css({'display':'none'});

				$('#tgl_end').prop('disabled', false);
				$('#weeklyDatePickerEnd').prop('disabled', true);
				$('#monthlyDatePickerEnd').prop('disabled', true);

				$('#tgl_end').css({'display':'block'});
				$('#weeklyDatePickerEnd').css({'display':'none'});
				$('#monthlyDatePickerEnd').css({'display':'none'});
			<?php } ?>

			$('#satuan').on('change', function(){
				var id = $(this).val();
				if (id == 1) {
					$('#tgl_start').prop('disabled', false);
					$('#weeklyDatePickerStart').prop('disabled', true);
					$('#monthlyDatePickerStart').prop('disabled', true);

					$('#tgl_start').css({'display':'block'});
					$('#weeklyDatePickerStart').css({'display':'none'});
					$('#monthlyDatePickerStart').css({'display':'none'});

					$('#tgl_end').prop('disabled', false);
					$('#weeklyDatePickerEnd').prop('disabled', true);
					$('#monthlyDatePickerEnd').prop('disabled', true);

					$('#tgl_end').css({'display':'block'});
					$('#weeklyDatePickerEnd').css({'display':'none'});
					$('#monthlyDatePickerEnd').css({'display':'none'});
				}else if(id == 2){
					$('#tgl_start').prop('disabled', true);
					$('#weeklyDatePickerStart').prop('disabled', false);
					$('#monthlyDatePickerStart').prop('disabled', true);

					$('#tgl_start').css({'display':'none'});
					$('#weeklyDatePickerStart').css({'display':'block'});
					$('#monthlyDatePickerStart').css({'display':'none'});

					$('#tgl_end').prop('disabled', true);
					$('#weeklyDatePickerEnd').prop('disabled', false);
					$('#monthlyDatePickerEnd').prop('disabled', true);

					$('#tgl_end').css({'display':'none'});
					$('#weeklyDatePickerEnd').css({'display':'block'});
					$('#monthlyDatePickerEnd').css({'display':'none'});
				}else if(id == 3){
					$('#tgl_start').prop('disabled', true);
					$('#weeklyDatePickerStart').prop('disabled', true);
					$('#monthlyDatePickerStart').prop('disabled', false);

					$('#tgl_start').css({'display':'none'});
					$('#weeklyDatePickerStart').css({'display':'none'});
					$('#monthlyDatePickerStart').css({'display':'block'});

					$('#tgl_end').prop('disabled', true);
					$('#weeklyDatePickerEnd').prop('disabled', true);
					$('#monthlyDatePickerEnd').prop('disabled', false);

					$('#tgl_end').css({'display':'none'});
					$('#weeklyDatePickerEnd').css({'display':'none'});
					$('#monthlyDatePickerEnd').css({'display':'block'});

				}
			})

			moment.locale('en', {
		      week: { dow: 1 } // Monday is the first day of the week
		    });

			// var startDate;
			// var endDate;
			$( "#weeklyDatePickerStart").datepicker({

				firstDay: 1,
			    dateFormat: 'dd-mm-yy',
				showWeek: true,
				onSelect: function(dateText, inst) {
			        var value = $("#weeklyDatePickerStart").val();
				    var firstDate = moment(value, "DD-MM-YYYY").day(1).format("DD-MM-YYYY");
				    // alert(value);
				    $(this).val(firstDate);
			    },
			});

			$( "#weeklyDatePickerEnd").datepicker({

				firstDay: 1,
			    dateFormat: 'dd-mm-yy',
				showWeek: true,
				onSelect: function(dateText, inst) {
			        var value = $("#weeklyDatePickerEnd").val();
				    var firstDate = moment(value, "DD-MM-YYYY").day(7).format("DD-MM-YYYY");
				    // alert(value);
				    $(this).val(firstDate);
			    },
			});

			$( "#monthlyDatePickerStart").datepicker({

				firstDay: 1,
			    dateFormat: 'mm-yy',
			    changeMonth: true,
		        changeYear: true,
		        showButtonPanel: true,
		        onClose: function(dateText, inst) { 
	                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			        $(this).datepicker('setDate', new Date(year, month, 1));
	            }
			});

			$( "#monthlyDatePickerEnd").datepicker({

				firstDay: 1,
			    dateFormat: 'mm-yy',
			    changeMonth: true,
		        changeYear: true,
		        showButtonPanel: true,
		        onClose: function(dateText, inst) { 
	                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
	            }
			});

			$("#monthlyDatePickerStart, #monthlyDatePickerEnd").focus(function () {
			  $(".ui-datepicker-calendar").hide();
			})

			// $("#weeklyDatePicker").weekpicker({

			// 	firstDay: 1,
			// 	dateFormat: "dd-mm-yy",
			// 	showWeek: true,
			// 	showOtherMonths: true,
			// 	selectOtherMonths: true,


			// });


			$('#tgl_start, #tgl_end').datepicker({
				firstDay: 1,
			    dateFormat: 'dd-mm-yy',
			    autoclose: true
			})

			
		})

	</script>

	<?php
        // echo "<pre>";print_r($data);
	?>


