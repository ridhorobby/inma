<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Kas</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button>
        </span>
        <span style="float: right;">
		  <a href="#" class="btn btn-sm btn-add"><i class="fas fa-plus"></i> Tambah</a>
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
    	<div class="kalender-kas text-center">
    		<button type="button" id="prev" class="btn btn-sm btn-light float-left"><i class="fas fa-angle-left"></i></button>
    		<label>Januari 2021</label>
    		<input type="hidden" id="month" value="<?= date('m')?>">
    		<input type="hidden" id="year" value="<?= date('Y')?>">
    		<button type="button" id="next" class="btn btn-sm btn-light float-right"><i class="fas fa-angle-right"></i></button>
    	</div>
    </div>
    <div class="col-md-12">
       	<table class="table table-sm table-kas">
          <thead>
            <tr>
              <th class="jenis-kas-th">Jenis</th>
              <th class="kode-kas-th">Kode</th>
              <th class="debet-kas-th">Debet</th>
              <th class="kredit-kas-th">Kredit</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="4">
              	<div class="row kas-row">
	              	<div class="col-12 col-sm-12 col-md-12">
		              	<span style="float: left;">23 Agustus 2020</span>
		              	<span style="float: right;">Administrator</span>
		            </div>
	              	<div class="col-12 col-sm-12 col-md-12">
	              		<div class="row">
	              			<div class="col-4 col-sm-4 col-md-6">PELUNASAN</div>
				            <div class="col-2 col-sm-2 col-md-2 kode">PKMD170</div>
				            <div class="col-3 col-sm-3 col-md-2 text-center debet">-</div>
				            <div class="col-3 col-sm-3 col-md-2 text-center kredit">3.000.000</div>
	              		</div>
              		</div>
              	</div>
              </td>
            </tr>
            <tr>
              <td colspan="4">
              	<div class="row kas-row">
	              	<div class="col-12 col-sm-12 col-md-12">
		              	<span style="float: left;">23 Agustus 2020</span>
		              	<span style="float: right;">Administrator</span>
		            </div>
	              	<div class="col-12 col-sm-12 col-md-12">
	              		<div class="row">
	              			<div class="col-4 col-sm-4 col-md-6">PELUNASAN</div>
				            <div class="col-2 col-sm-2 col-md-2 kode">PKMD170</div>
				            <div class="col-3 col-sm-3 col-md-2 text-center debet">-</div>
				            <div class="col-3 col-sm-3 col-md-2 text-center kredit">3.000.000</div>
	              		</div>
              		</div>
              	</div>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <th colspan="2">Total</th>
            <th class="debet-kas-th">16.500.000</th>
            <th class="kredit-kas-th">11.000.000</th>
          </tfoot>
        </table>
    </div>
</div>

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Urutkan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row filter-kas">
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Tanggal Input Lama -> Terbaru</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Tanggal Input Terbaru -> Lama</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Jenis A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Jenis Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kode A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kode Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Debit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Debit Max -> Min</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kredit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kredit Max -> Min</div>
        		</a>
        	</div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<script>
	$(document).ready(function(){
		$('#prev').click(function(){
			var month = $('#month').val();
			var year = $('#year').val();
			
			alert('prev date');
		})

		$('#next').click(function(){
			var month = $('#month').val();
			var year = $('#year').val();
			
			alert('next date');
		})
	})
</script>