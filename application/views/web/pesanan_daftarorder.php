
<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Daftar Order</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-filter"></i> Filter</button>
        </span>
        <span style="float: right;">
          <?php if(SessionManagerWeb::getRole() == Role::ADMINISTRATOR || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){ ?>
         	  <a href="<?php echo site_url('web/pesanan/orderadd') ?>" class="btn btn-sm btn-add"><i class="fas fa-plus"></i> Tambah</a>			  
          <?php } ?>
        </span>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
       	<ul class="nav nav-tabs" role="tablist">
		  <li class="nav-item">
		    <a class="nav-link active" href="#baru" role="tab" data-toggle="tab">Baru</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="#menunggu" role="tab" data-toggle="tab">Menunggu</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="#proses" role="tab" data-toggle="tab">Proses</a>
		  </li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		  <div role="tabpanel" class="tab-pane fade in active show" id="baru">
		  	<?php foreach ($pesanan_baru as $key): ?>
				<!-- <a href="<?= site_url('web/pesanan/detail/'.$key['id'])  ?>" class="row mt-4 row-order-baru" style="cursor:pointer;text-decoration:none;color:#212529; ">
					<div class="col-7 col-sm-6 col-md-9 list-order-baru">
						<label><?= $key['kode_order']  ?></label>
						<p><?= $key['nama_user']  ?></p>
						<p><?= substr($key['alamat_konsumen'],0,50).'...'  ?></p>
					</div>
					<div class="col-5 col-sm-6 col-md-3 list-order-baru text-right">
						<label></label>
						<p><?= $key['no_hp_konsumen']  ?></p>
						<div class="status-produksi">
							<?php if($key['status_produksi'] == 1) {
								echo '<span style="background-color: #4caf50;">Bisa Cek Ruangan</span>';
							}elseif($key['status_produksi'] == 2) {
								echo '<span style="background-color: #4caf50;">Bisa Pasang Rangka</span>';
							}elseif($key['status_produksi'] == 3) {
								echo '<span style="background-color: #f71212;">Menunggu Cor Siap</span>';
							} ?>
						</div>
					</div>
				</a> -->

        <div class="row mt-4 row-order-baru" onclick="detailorder(<?= $key['id'] ?>,'<?= $key['kode_order'] ?>',<?= $key['status_produksi']?>)" style="cursor:pointer;color:#212529; background-color:#c8e6c9;">
          <div class="col-7 col-sm-6 col-md-9 list-order-baru">
            <label><?= $key['kode_order']  ?></label>
            <p><?= $key['nama_user']  ?></p>
            <p><?= substr($key['alamat_konsumen'],0,50).'...'  ?></p>
            <?= ($key['reminder_kusen'] == 1) ? '<p><b>'.$key['selisih_tanggal_kusen'].' hari belum pasang kusen</b></p>' : '' ?>
          </div>
          <div class="col-5 col-sm-6 col-md-3 list-order-baru text-right">
            <label></label>
            <p><?= $key['no_hp_konsumen']  ?></p>
            <div class="status-produksi">
              <?php if($key['status_produksi'] == 1) {
                echo '<span style="background-color: #4caf50;">Bisa Cek Ruangan</span>';
              }elseif($key['status_produksi'] == 2) {
                echo '<span style="background-color: #4caf50;">Bisa Pasang Rangka</span>';
              }elseif($key['status_produksi'] == 3) {
                echo '<span style="background-color: #f71212;">Menunggu Cor Siap</span>';
              } ?>
            </div>
          </div>
        </div>
			<?php endforeach; ?>
		  </div>

		  <div role="tabpanel" class="tab-pane fade" id="menunggu">
        <?php foreach ($need_action as $key): ?>
        <?php $color = ($key['rencana_kerja_id']==1) ? '#ffe0b2' : '#e1bee7' ?>
        <div class="row mt-4 row-order-menunggu" onclick='ordermenunggu(<?= $key['id'] ?>,<?= json_encode($key) ?>)' style="cursor:pointer;color:#212529; background-color:<?= $color ?>;">
            <div class="col-4 col-sm-4 col-md-2 list-order-menunggu">
              <div>Kode Order</div>
              <div>Usulan Jadwal</div>
              <div>Rencana Kerja</div>
              <div>Tim</div>
              <?php 
                if($key['rencana_kerja_id'] == 1){
                  echo ($key['reminder_kusen'] == 1) ? '<div><b>'.$key['selisih_tanggal_kusen'].' hari belum pasang rangka</b></div>' : '';
                }else{
                  echo ($key['reminder_finish'] == 1) ? '<div><b>'.$key['selisih_tanggal_finish'].' hari belum pasang unit</b></div>' : '';
                }
                
               ?>
            </div>
            <div class="col-8 col-sm-8 col-md-10">
              <div><?= $key['kode_order']  ?></div>
              <div><?= ($key['jadwal_usulan']) ?  xFormatDateInd($key['jadwal_usulan'])  : '-'  ?></div>
              <div><?= ($key['rencana_kerja_nama']) ? $key['rencana_kerja_nama'] : '-'  ?></div>
              <?php 
                  if($key['rencana_kerja_id']== 1 && $key['nama_pemasang_kusen']){
                    $nama = $key['nama_pemasang_kusen'];
                  }elseif($key['rencana_kerja_id']> 2 && $key['nama_pemasang_finish']){
                    $nama = $key['nama_pemasang_finish'];
                  }else{
                    $nama = '-';
                  }
               ?>
              <div><?= $nama  ?></div>
            </div>
            
            <span style="position: absolute;right: 10px;background-color: <?= ($key['jadwal_usulan']) ? '#388e3c' : '#1976d2'; ?>;padding: 0px 5px;color: #fff;font-size: 13px;    margin-top: 5px;"><?= ($key['jadwal_usulan']) ? 'Acc' : 'Usul' ?></span>
             
        </div>
        <?php endforeach; ?>  
      </div>

		  <div role="tabpanel" class="tab-pane fade" id="proses">
        <?php foreach ($in_progress as $key): ?>
        <?php $color = ($key['rencana_kerja_id']==1) ? '#ffe0b2' : '#e1bee7' ?>
        <div class="row mt-4 row-order-menunggu" onclick="orderproses(<?= $key['id'] ?>,'<?= $key['kode_order'] ?>','<?= $key['flow_id']  ?>','<?= $key['jadwal_usulan']  ?>','<?= $key['tanggal_pasang_kusen']  ?>','<?= $key['tanggal_pasang_finish']  ?>','<?= $key['rencana_kerja_id']?>','<?= $key['rencana_kerja_nama']?>','<?= $key['nama_konsumen'] ?>')" style="cursor:pointer;color:#212529; background-color:<?= $color ?>;">
            <div class="col-4 col-sm-4 col-md-2 list-order-menunggu">
              <div>Kode Order</div>
              <div>Nama</div>
              <div>Rencana Kerja</div>
              <div>Tim</div>
            </div>
            <div class="col-8 col-sm-8 col-md-10">
              <div><?= $key['kode_order']  ?></div>
              <div><?= $key['nama_konsumen']  ?></div>
              <div>Selesai</div>
              <?php 
                if($key['rencana_kerja_id']== 1 && $key['nama_pemasang_kusen']){
                    $nama = $key['nama_pemasang_kusen'];
                  }elseif($key['rencana_kerja_id']> 1 && $key['nama_pemasang_finish']){
                    $nama = $key['nama_pemasang_finish'];
                  }else{
                    $nama = '-';
                  }

               ?>
              <div><?= $nama  ?></div>
            </div>
            <?php 
              if($key['rencana_kerja_id'] == 1){
                echo ($key['reminder_kusen'] == 1) ? '<div><b>'.$key['selisih_tanggal_kusen'].' hari belum pasang rangka</b></div>' : '';
              }else{
                echo ($key['reminder_finish'] == 1) ? '<div><b>'.$key['selisih_tanggal_finish'].' hari belum pasang unit</b></div>' : '';
              }
              
             ?>
            <?php if (SessionManagerWeb::getRole() != Role::DIREKTUR){ ?>
             <?php if (($key['flow_id']==3 && $key['jadwal_usulan']!='') || ($key['flow_id']==4 && $key['tanggal_pasang_kusen']=='') || ($key['flow_id']==6 && $key['jadwal_usulan']!='') || ($key['flow_id']==7 && $key['tanggal_pasang_finish']=='')): ?>
            <div class="col-12 col-sm-12 col-md-12" style="color:red">
                Menunggu Approval Desainer/Marketing
            </div>
            <?php endif ?>
           
            <?php } else { ?>
              <?php 
                 $echo = "";
                switch ($key['flow_id']) {
                  case 2:
                    if ($key['nama_pemasang_kusen']!='' || $key['nama_pemasang_kusen']!=null) {
                        $echo = "Menunggu Approval Manager Produksi";
                    } else {
                        $echo = "Menunggu pengajuan Jadwal Manager Produksi";
                    }
                  break;
                  case 3:
                    $echo = "Menunggu Approval Desainer/Marketing";
                  break;
                  case 5:
                     if ($key['nama_pemasang_finish']!='' || $key['nama_pemasang_finish']!=null) {
                        $echo = "Menunggu Approval Manager Produksi";
                    } else {
                        $echo = "Menunggu pengajuan Jadwal Manager Produksi";
                    }
                  break;
                  case 6:
                    $echo = "Menunggu Approval Desainer/Marketing";
                  break;
                  
                  default:
                    $echo = "";  
                  break;
                } 

                if($echo != ""){
                   ?>
                   <div class="col-12 col-sm-12 col-md-12" style="color:red">
                       <?= $echo  ?>
                    </div>
                   <?php
                }
                ?>
              


            <?php } ?>
            
        </div>  
        <?php endforeach; ?>  
      </div>
		</div>
    </div>
</div>
<!-- /.row -->

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pencarian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post">
      <div class="modal-body">
        <input type="hidden" name="filterExist" value="1">
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="kode_order" class="form-control" placeholder="Kode Order" value="<?= ($filter['kode_order']) ? $filter['kode_order'] : null ?>">
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="nama_konsumen" class="form-control" placeholder="Nama" value="<?= ($filter['nama_konsumen']) ? $filter['nama_konsumen'] : null ?>">
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="alamat_konsumen" class="form-control" placeholder="Alamat" value="<?= ($filter['alamat_konsumen']) ? $filter['alamat_konsumen'] : null ?>">
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="no_hp_konsumen" class="form-control" placeholder="No HP" value="<?= ($filter['no_hp_konsumen']) ? $filter['no_hp_konsumen'] : null ?>">
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="<?= site_url("web/$class/$method") ?>" class="btn btn-sm btn-secondary" >Reset</a>
        <button type="submit" class="btn btn-sm btn-add">Terapkan</button>
      </div>
	  </form>
    </div>
  </div>
</div>

<!-- Detail Modal -->
<div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="kodeOrderModal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row" >

        </div>
      </div>
    </div>
  </div>
</div>

<!-- Status Modal -->
<div class="modal fade" id="statusModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="statusProduksiModal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row list-status">
          
        </div>
      </div>
    </div>
  </div>
</div>

<form id="formSubmit" action="<?php echo site_url('web/pesanan/updateStatusProduksi')?>" method="post">
  <input type="hidden" name="id" id="idorder">
  <input type="hidden" name="status_produksi" id="status_produksi">
</form>

<form id="formApprove" action="#" method="post">
  <input type="hidden" name="confirm" value="1">
</form>

<!-- Order Menunggu Modal -->
<div class="modal fade" id="orderMenungguModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Usulan Jadwal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-4 col-sm-4 col-md-4">Tanggal</div>
          <div class="col-8 col-sm-8 col-md-8" id="jadwal_pengajuan"></div>

          <div class="col-4 col-sm-4 col-md-4">Jam</div>
          <div class="col-8 col-sm-8 col-md-8" id="jam_pengajuan"></div>

          <div class="col-4 col-sm-4 col-md-4">Rencana Kerja</div>
          <div class="col-8 col-sm-8 col-md-8" id="rencana_kerja_nama"></div>

          <div class="col-4 col-sm-4 col-md-4">Tim</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_pemasang"></div>

          <div class="col-12 col-sm-12 col-md-12">
            <label class="mb-0 mt-2">Data Order</label>
          </div>

          <div class="col-4 col-sm-4 col-md-4">Kode Order</div>
          <div class="col-8 col-sm-8 col-md-8" id="kode_order"></div>

          <div class="col-4 col-sm-4 col-md-4">Tgl. Order Masuk</div>
          <div class="col-8 col-sm-8 col-md-8" id="tgl_order_masuk"></div>

          <div class="col-4 col-sm-4 col-md-4">Nama</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">Alamat</div>
          <div class="col-8 col-sm-8 col-md-8" id="alamat_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">No HP</div>
          <div class="col-8 col-sm-8 col-md-8" id="no_hp_konsumen"></div>
        </div>
      </div>
      <div class="modal-footer" style="border-top: none;">
          <?php if (SessionManagerWeb::getRole() != Role::PRODUKSI): ?>
            <input type="text" class="form-control" name="link" id="link" value="" readonly="">
          <button  onclick="copyLink()" class="btn btn-sm btn-secondary" id="terimausulkanpemasangan"><i class="fas fa-link"></i> Copy Link</button>
          <?php endif ?>
          
          <a href="#" class="btn btn-sm btn-add" id="tolakusulkanpemasangan"><i class="fas fa-times"></i> Tolak</a>

          <button  onclick="terimaUsulan()" class="btn btn-sm btn-success" id="terimausulkanpemasangan"><i class="fas fa-check"></i> Setuju</button>
      </div>
    </div>
  </div>
</div>

<!-- Order Proses Modal -->
<div class="modal fade" id="orderProsesModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="orderProsesTitleModal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row list-order-proses">
          
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Order Proses Selesai Modal -->
<div class="modal fade" id="orderProsesSelesaiModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="orderProsesSelesaiTitleModal">Pekerjaan Selesai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="#" method="post" id="formProsesSelesai">        
      <div class="modal-body">
        <div style="font-size: .9em;">Anda akan merubah status pekerjaan menjadi selesai pada order berikut</div>
        <div class="row list-order-proses-selesai mt-1" style="font-size: .9em;background-color: #eee;">
          <div class="col-4 col-sm-4 col-md-4">Kode Order</div>
          <div class="col-8 col-sm-8 col-md-8" id="kode_order"></div>

          <div class="col-4 col-sm-4 col-md-4">Nama</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">Jadwal</div>
          <div class="col-8 col-sm-8 col-md-8" id="jadwal"></div>

          <div class="col-4 col-sm-4 col-md-4">Rencana Kerja</div>
          <div class="col-8 col-sm-8 col-md-8" id="rencana_kerja"></div>
        </div>
      </div>
      <div class="modal-footer" style="border-top: none;">
          <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</a>
          <button type="submit" class="btn btn-sm btn-add"><i class="fas fa-check"></i> Selesai</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){
    
    $(document).on('click','.detail-order',function(){
      var id = $(this).attr('id');
      window.location.href = "<?= site_url('web/pesanan/detail')?>/"+id;
    })

    $(document).on('click','.update-status',function(){
      var kode = $(this).attr('kode');
      var status = $(this).attr('status');
      $('#statusProduksiModal').html('Status Produksi - '+kode);
      $('.list-status').html('');
      if (status == 1) {            
      $('.list-status').append('<div class="col-12 col-md-12 col-md-12">'+
                                  '<h6 style="color: #aaa;">Bisa Cek Ruangan</h6>'+
                                  '<span class="text-red" style="font-size: 12px;">Status saat ini</span>'+
                              '</div>');
      }else{
      $('.list-status').append('<div class="col-12 col-md-12 col-md-12" onclick="changestatusproduksi(1)" style="cursor: pointer;">'+
                                  '<h6>Bisa Cek Ruangan</h6>'+
                              '</div>');
      }

      if (status == 2) {
      $('.list-status').append('<div class="col-12 col-md-12 col-md-12">'+
                                  '<h6 style="color: #aaa;">Bisa Pasang Rangka</h6>'+
                                  '<span class="text-red" style="font-size: 12px;">Status saat ini</span>'+
                              '</div>');
      }else{
      $('.list-status').append('<div class="col-12 col-md-12 col-md-12" onclick="changestatusproduksi(2)" style="cursor: pointer;">'+
                                  '<h6>Bisa Pasang Rangka</h6>'+
                              '</div>');
      }

      if (status == 3) {            
      $('.list-status').append('<div class="col-12 col-md-12 col-md-12">'+
                                '<h6 style="color: #aaa;">Menunggu Cor Siap</h6>'+
                                '<span class="text-red" style="font-size: 12px;">Status saat ini</span>'+
                              '</div>');
      }else{
      $('.list-status').append('<div class="col-12 col-md-12 col-md-12" onclick="changestatusproduksi(3)" style="cursor: pointer;">'+
                                '<h6>Menunggu Cor Siap</h6>'+
                              '</div>');
      }

      $('#detailModal').modal('hide');
      $('#statusModal').modal('show');

      // window.location.href = "<?= site_url('web/pesanan/detail')?>/"+id;
    })
  
  })

  function copyLink(){
   /* Get the text field */
    var copyText = document.getElementById("link");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /* For mobile devices */

    /* Copy the text inside the text field */
    document.execCommand("copy");

    /* Alert the copied text */
    alert("Copied the text: " + copyText.value);
  }
  function detailorder(id, kode, status){
    $('#idorder').val(id);
    //console.log(kode);
    //console.log(status);
    //$('.update-status').attr('kode','bbb');
    //$('.update-status').attr('status','bbb');
    $('#kodeOrderModal').html(kode);

    $('#detailModal .modal-body .row').html('');
    var role = '<?php echo SessionManagerWeb::getRole() ?>';
    if (status == 3) {
      if(role == 'P'){
	      var html = '<div class="col-12 col-md-12 col-md-12">'+
	            '<h6 style="color: #aaa;">Menunggu Cor Siap</h6>'+
	            '<span class="text-red" style="font-size: 12px;">Status saat ini</span>'+
	          '</div>'+
	          '<div class="col-12 col-md-12 col-md-12 detail-order" style="cursor: pointer;">'+
	            '<label style="cursor: pointer;">Detail Order</label>'+
	            '<p>Melihat detail informasi pada order</p>'+
	          '</div>';
       }else{
       	var html = '<div class="col-12 col-md-12 col-md-12 update-status" kode="'+kode+'" status="'+status+'" style="cursor: pointer;">'+
            '<label style="cursor: pointer;">Update Status Produksi</label>'+
            '<p>Ubah status untuk proses produksi</p>'+
          '</div>'+
          '<div class="col-12 col-md-12 col-md-12 detail-order" style="cursor: pointer;">'+
            '<label style="cursor: pointer;">Detail Order</label>'+
            '<p>Melihat detail informasi pada order</p>'+
          '</div>';
       }
    }else{

      if(role == 'P'){
        var html = '<a href="<?= site_url('web/pesanan/usulkanpemasangan') ?>/'+id+'" class="col-12 col-md-12 col-md-12" style="cursor: pointer;">'+
            '<label style="cursor: pointer;text-decoration:none;color:#212529;">Usulkan Jadwal Pemasangan</label>'+
            '<p style="text-decoration:none;color:#212529;">Mengajukan jadwal kepada Tim Marketing / Tim Desain</p>'+
          '</a>'+
          '<div class="col-12 col-md-12 col-md-12 detail-order" style="cursor: pointer;">'+
            '<label style="cursor: pointer;">Detail Order</label>'+
            '<p>Melihat detail informasi pada order</p>'+
          '</div>';
      }else{
        var html = '<div class="col-12 col-md-12 col-md-12 update-status" kode="'+kode+'" status="'+status+'" style="cursor: pointer;">'+
            '<label style="cursor: pointer;">Update Status Produksi</label>'+
            '<p>Ubah status untuk proses produksi</p>'+
          '</div>'+
          '<div class="col-12 col-md-12 col-md-12 detail-order" style="cursor: pointer;">'+
            '<label style="cursor: pointer;">Detail Order</label>'+
            '<p>Melihat detail informasi pada order</p>'+
          '</div>';
      }
      
    }
          
    $('#detailModal .modal-body .row').html(html);
    $('.detail-order').attr('id',id);

    $('#detailModal').modal('show');
  }

  function changestatusproduksi(status){
    $('#status_produksi').val(status);
    $('#formSubmit').submit();
  }

  function terimaUsulan(){
    //console.log($('#idorder').val());
    var id = $('#idorder').val();
    $('#formApprove').attr('action','<?php echo site_url('web/pesanan/usulan')?>/'+id);
    console.log($('#formApprove').attr('action'));
    $('#formApprove').submit();
  }

  <?php if(SessionManagerWeb::getRole() != Role::PRODUKSI ){ ?> //untuk non produksi
    function ordermenunggu(id,data){
      $('#idorder').val(id);
      $('#tolakusulkanpemasangan').attr('href','<?php echo site_url('web/pesanan/tolakusulkanpemasangan')?>/'+id);
      $('#link').val('<?php echo site_url('web/site/approve_penjadwalan_pesanan')?>/'+data.konsumen_id);
      $('#orderMenungguModal').modal('show');
      $('.btn-pilih-mitrapemasang').html('Ubah');
      $('#jadwal_pengajuan').html(data.jadwal_usulan);
      $('#jam_pengajuan').html(data.jam_usulan);
      $('#rencana_kerja_nama').html(data.rencana_kerja_nama);
      if(data.rencana_kerja_id== 1 && data.nama_pemasang_kusen!=''){
        var nama = data.nama_pemasang_kusen;
                  
      }
      else if(data.rencana_kerja_id > 1 && data.nama_pemasang_finish!=''){
        var nama = data.nama_pemasang_finish;
      }else{
        var nama = '-';
      }
      $('#nama_pemasang').html(nama);
      $('#kode_order').html(data.kode_order);
      $('#tgl_order_masuk').html(data.tgl_order_masuk);
      $('#nama_konsumen').html(data.nama_konsumen);
      $('#alamat_konsumen').html(data.alamat_konsumen);
      $('#no_hp_konsumen').html(data.no_hp_konsumen);

    }
  <?php }else{ ?> //untuk produksi
    function ordermenunggu(id,data){
      // $('#link').val('<?php echo site_url('web/site/approve_penjadwalan_pesanan')?>/'+data.konsumen_id);
      $('#idorder').val(id);
    	if(data.jadwal_usulan==null){
    		window.location.href='<?= site_url('web/pesanan/usulkanpemasangan/') ?>/'+id;
    	}else{
    		if(data.role_pengusul==''){
	    		window.location.href = "<?php echo site_url('web/pesanan/usulkanpemasangan')?>/"+id;
	    	}else{
	    	  $('#tolakusulkanpemasangan').attr('href','<?php echo site_url('web/pesanan/tolakusulkanpemasangan')?>/'+id);
		      $('#orderMenungguModal').modal('show');
		      $('.btn-pilih-mitrapemasang').html('Ubah');
		      $('#jadwal_pengajuan').html(data.jadwal_usulan);
          $('#jam_pengajuan').html(data.jam_usulan);
		      $('#rencana_kerja_nama').html(data.rencana_kerja_nama);
          if(data.rencana_kerja_id== 1 && data.nama_pemasang_kusen!=''){
            var nama = data.nama_pemasang_kusen;
                      
          }
          else if(data.rencana_kerja_id > 2 && data.nama_pemasang_finish!=''){
            var nama = data.nama_pemasang_finish;
          }else{
            var nama = '-';
          }
		      $('#nama_pemasang').html(nama);
		      $('#kode_order').html(data.kode_order);
		      $('#tgl_order_masuk').html(data.tgl_order_masuk);
		      $('#nama_konsumen').html(data.nama_konsumen);
		      $('#alamat_konsumen').html(data.alamat_konsumen);
		      $('#no_hp_konsumen').html(data.no_hp_konsumen);
	    	}
    	}
    	
      
    }

    function setselesaiproses(id,rencana_kerja_id){

      if (rencana_kerja_id == 1) {
        $('#orderProsesModal').modal('hide');
        $('#orderProsesSelesaiModal').modal('show');
        $('#formProsesSelesai').attr('action', '<?= site_url("web/$class/usulan/")  ?>/'+id);
      }else{
        window.location.href = "<?php echo site_url('web/pesanan/setselesaiproses')?>/"+id;
      }
    }
  <?php } ?>


  function orderproses(id, kode,flow_id, jadwal_usulan,tanggal_pasang_kusen, tanggal_pasang_finish, rencana_kerja_id, rencana_kerja_nama, nama_konsumen){
    $('#orderProsesTitleModal').html(kode);

    <?php if(SessionManagerWeb::getRole() == Role::PRODUKSI){ ?>
    	if((flow_id==3 && jadwal_usulan!='') || (flow_id==4 && tanggal_pasang_kusen=='') || (flow_id==6 && jadwal_usulan!='') || (flow_id==7 && tanggal_pasang_finish=='')){
	        var html = '<div class="col-12 col-md-12 col-md-12">'+
	          '<label style="color: #aaa;">Set Selesai</label>'+
	          '<p class="text-red">Menunggu Approval Desainer/Marketing</p>'+
	        '</div>'+
	        '<div class="col-12 col-md-12 col-md-12 detail-order" style="cursor: pointer;">'+
	          '<label style="cursor: pointer;">Detail Order</label>'+
	          '<p>Melihat detail informasi pada order</p>'+
	        '</div>';
	    }else{
	    	var html = '<div class="col-12 col-md-12 col-md-12" onclick="setselesaiproses('+id+','+rencana_kerja_id+');" style="cursor: pointer;">'+
          '<label style="cursor: pointer;">Set Selesai</label>'+
	        '</div>'+
	        '<div class="col-12 col-md-12 col-md-12 detail-order" style="cursor: pointer;">'+
	          '<label style="cursor: pointer;">Detail Order</label>'+
	          '<p>Melihat detail informasi pada order</p>'+
	        '</div>';

          var dt = new Date(tanggal_pasang_kusen);
          var monthNames = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

          $('#orderProsesSelesaiModal #kode_order').html(kode);
          $('#orderProsesSelesaiModal #nama_konsumen').html(nama_konsumen);
          $('#orderProsesSelesaiModal #jadwal').html( dt.getDate()+' '+monthNames[dt.getMonth()]+' '+dt.getFullYear());
          $('#orderProsesSelesaiModal #rencana_kerja').html(rencana_kerja_nama);
	    }
      
    <?php }
    else{ ?>
    var html = '<div class="col-12 col-md-12 col-md-12 reschedule-jadwal" onclick="reschedule('+id+')" style="cursor: pointer;">'+
          '<label style="cursor: pointer;">Reschedule</label>'+
          '<p>Usulkan jadwal baru kepada manager produksi</p>'+
        '</div>'+
        '<div class="col-12 col-md-12 col-md-12 detail-order" style="cursor: pointer;">'+
          '<label style="cursor: pointer;">Detail Order</label>'+
          '<p>Melihat detail informasi pada order</p>'+
        '</div>';
    <?php } ?>
          
    $('#orderProsesModal .modal-body .row').html(html);
    $('.detail-order').attr('id',id);
    $('#orderProsesModal').modal('show');

  }

  function reschedule(id){
      window.location.href = "<?php echo site_url('web/pesanan/reschedulejadwal')?>/"+id;
  }

</script>