<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/web/plugins/evo-calendar/css/evo-calendar.css">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/web/plugins/evo-calendar/css/evo-calendar.royal-navy.css"/> -->
<script src="<?php echo base_url();?>assets/web/plugins/evo-calendar/js/evo-calendar.js"></script>

	<!-- <?php echo $class.'::'.$method."::".$class ?> -->
	<!-- <?php echo "<pre>";print_r($pesanan_jatuh_tempo);echo "</pre>"; ?> -->
	<!-- <button onclick="getKalendarView()">Tes</button> -->
	<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Jadwal</h1>
        </div>
	</div>

	<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>

	<div class="row mb-4">
		<div class="col-md-12">
            <span style="float: left;">
				<button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-filter"></i> Filter</button>
            </span>
            <span style="float: right;">
            	<div class="btn-group">
				  <button type="button" class="btn btn-sm btn-add dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fas fa-plus"></i> Tambah
				  </button>
				  <div class="dropdown-menu dropdown-menu-right">
				  	<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI){ ?>
				  	<a class="dropdown-item" href="<?php echo site_url('web/pesanan/addpemasangan')?>">Usulkan Jadwal Pemasangan</a>
				    <a class="dropdown-item" href="<?php echo site_url('web/pesanan/addpenanganan')?>">Usulkan Jadwal Penanganan</a>
				  	<?php } else { ?>
				  	<a class="dropdown-item" href="<?php echo site_url('web/pesanan/orderadd')?>">Tambahkan Konsumen</a>
				    <a class="dropdown-item" href="<?php echo site_url('web/komplain/add')?>">Ajukan Komplain</a>
				  	<?php } ?>
				    
				  </div>
				</div>
            </span>
        </div>
	</div>    

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
           	<div id="calendar"></div>
        </div>
  	</div>
  <!-- /.row -->

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pencarian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post">
      <div class="modal-body">
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="kode_order" class="form-control" placeholder="Kode Order">
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="nama" class="form-control" placeholder="Nama">
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="alamat" class="form-control" placeholder="Alamat">
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-12 col-md-12 col-md-12">
        		<input type="text" name="tlp" class="form-control" placeholder="No HP">
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-sm btn-secondary" >Reset</a>
        <button type="submit" class="btn btn-sm btn-add">Terapkan</button>
      </div>
	  </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	
	var active_events = [];

	var bulan = '<?php echo date('m');?>';
	var tahun = '<?php echo date('Y');?>';

	// var bulan = '08';
	// var tahun = '2020';

	getKalendarView(bulan,tahun);

	$(document).ready(function(){

		$(document).on('click','.month', function(){
			var datavalue = $(this).attr('data-month-val');
			var bulan_temp = parseInt(datavalue) + 1;

			var bulan = tambahNol(bulan_temp);

			var tahun = $('.calendar-year p').html();

			getKalendarView(bulan,tahun);

		})

		$(document).on('click','.calendar-year button:first', function(){
			var datavalue = $('.calendar-months').find('.active-month').attr('data-month-val');
			var bulan_temp = parseInt(datavalue) + 1;

			var bulan = tambahNol(bulan_temp);
			var tahun = $('.calendar-year p').html();
			
			getKalendarView(bulan,tahun);

		})

		$(document).on('click','.event-container', function(){
			var str = $(this).attr('data-event-index');
			var data = str.split('_');
			var id = data[0];
			var tipe = data[1];

			if (tipe == 'P') {
				window.location.href = '<?php echo site_url('web/pesanan/detail')?>/'+id;
			}else{
				window.location.href = '<?php echo site_url('web/komplain/detail')?>/'+id;
			}
		})
	});

	$('#calendar').evoCalendar({
		language: 'id',
    	theme: 'Royal Navy',
    	todayHighlight: true,
	    format: 'yyyy-mm-dd',
	})

	function getKalendarView(bulan,tahun){

		$.ajax({
	        url : "<?php echo site_url('/web/pesanan/ajaxkalendarviewundone');?>",
	        type: 'post',
	        dataType: 'json',
	        data: {bulan:bulan, tahun:tahun},
	        success:function(respon){
	        	console.log(respon);

				var data_event = [];
            	
                $.each(respon, function(k, v) {

                	var tglmasuk = v.tgl_masuk;
                	var tglpasangkusen = v.tanggal_pasang_kusen;
                	var tgljatuhtempo = v.tgl_jatuh_tempo;
                	var tglpasangfinish = v.tanggal_pasang_finish;
                	var tgljatuhtempokusen = v.jatuh_tempo_kusen;
                	var tgljatuhtempofinish = v.jatuh_tempo_finish;


                	if (tglmasuk != null) {
	                	var tglmasuk = tglmasuk.split('-');
	                	if ( tglmasuk[0] == tahun && tglmasuk[1] == bulan) {

		                	var arr_data = {
								        id: v.id+'_'+v.tipe, // Event's ID (required)
								        name: 'Order Masuk <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.tgl_masuk, // Event date (required)
								        type: "selesai", // Event type (required)
								    }

							data_event.push(arr_data);
	                	}
                	}


                	if (tglpasangkusen != null) {
	                	var tglpasangkusen 	= tglpasangkusen.split('-');
	                	if (tglpasangkusen[0] == tahun && tglpasangkusen[1] == bulan) {
		                	var arr_data2 = {
								        id: v.id+'_'+v.tipe, // Event's ID (required)
								        name: 'Cek Ruangan / Pasang Kusen <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.tanggal_pasang_kusen, // Event date (required)
								        type: "holiday", // Event type (required)
								    }

							data_event.push(arr_data2);
	                	}
	                }

                	if (tgljatuhtempo != null) {
	                	var tgljatuhtempo = tgljatuhtempo.split('-');
	                	if (tgljatuhtempo[0] == tahun && tgljatuhtempo[1] == bulan) {
		                	var arr_data3 = {
								        id: v.id+'_'+v.tipe, // Event's ID (required)
								        name: 'Jatuh Tempo <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.tgl_jatuh_tempo, // Event date (required)
								        type: "event", // Event type (required)
								    }

							data_event.push(arr_data3);
	                	}
	                }

                	if (tglpasangfinish != null) {
	                	var tglpasangfinish = tglpasangfinish.split('-');
	                	if (tglpasangfinish[0] == tahun && tglpasangfinish[1] == bulan) {
		                	var arr_data4 = {
								        id: v.id+'_'+v.tipe, // Event's ID (required)
								        name: 'Pemasangan Unit <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.tanggal_pasang_finish, // Event date (required)
								        type: "pemasangan", // Event type (required)
								    }

							data_event.push(arr_data4);

	                	}
                	}

                	if (tgljatuhtempokusen != null) {
	                	var tgljatuhtempokusen = tgljatuhtempokusen.split('-');
	                	if (tgljatuhtempokusen[0] == tahun && tgljatuhtempokusen[1] == bulan) {
		                	var arr_data5 = {
								        id: v.id+'_'+v.tipe, // Event's ID (required)
								        name: 'Jatuh Tempo Pengajuan Pasang Kusen <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.jatuh_tempo_kusen, // Event date (required)
								        type: "event", // Event type (required)
								    }

							data_event.push(arr_data5);
	                	}
	                }

	                if (tgljatuhtempofinish != null) {
	                	var tgljatuhtempofinish = tgljatuhtempofinish.split('-');
	                	if (tgljatuhtempofinish[0] == tahun && tgljatuhtempofinish[1] == bulan) {
		                	var arr_data6 = {
								        id: v.id+'_'+v.tipe, // Event's ID (required)
								        name: 'Jatuh Tempo Pengajuan Pasang Finish <span>'+v.kode_order+'</span>', // Event name (required)
								        description: v.nama_konsumen,
								        date: v.jatuh_tempo_finish, // Event date (required)
								        type: "event", // Event type (required)
								    }

							data_event.push(arr_data6);
	                	}
	                }
	        		
                });

				var indic = $('.calendar-table').find('.event-indicator').length;
				
				if (indic == 0) {
					$("#calendar").evoCalendar("addCalendarEvent", data_event);
				}

	        } 
	    });
		
	}

	function tambahNol(x){
	   y=(x>9)?x:'0'+x;
	   return y;
	}
</script>