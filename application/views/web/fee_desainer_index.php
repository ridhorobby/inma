<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>List Desainer</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
    <div class="flashdata">
      <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
          <?php echo $srvmsg ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
    </div>
    <?php } ?>

<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<!-- <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button> -->
      <label> Tampilkan Berdasarkan</label>
      <select class="form-control tampilan">
        <option value="o" <?= ($tampilan == 'o') ? "selected" : ""  ?>>Order</option>
        <option value="d" <?= ($tampilan == 'd') ? "selected" : ""  ?>>Desainer</option>
      </select>
        </span>
        <span style="float: right;">
        <?php if(SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR ){ ?>
		      <a href="<?php echo site_url('web/fee_desainer/gajidesainadd/'.$_SESSION['order_desainer']['bulan'].'/'.$_SESSION['order_desainer']['tahun'])?>" class="btn btn-sm btn-add mt-4">Tambah Gaji</a>
        <?php } ?>
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
    	<div class="kalender-kas text-center">
    		<button type="button" id="prev" class="btn btn-sm btn-light float-left"><i class="fas fa-angle-left"></i></button>
    		<label id="tanggal_kas"><?= xIndoMonth($_SESSION['order_desainer']['bulan']).' '.$_SESSION['order_desainer']['tahun'] ?></label>
        
            <button type="button" id="next" class="btn btn-sm btn-light float-right"><i class="fas fa-angle-right"></i></button>
            <form id="main_form" action="<?php echo site_url('web/fee_desainer')?>" method="post">
              <input type="hidden" name="bulan" id="month" value="<?= $_SESSION['order_desainer']['bulan']?>">
              <input type="hidden" name="tahun" id="year" value="<?= $_SESSION['order_desainer']['tahun']?>">
            </form>
    	</div>
    </div>
    <?php if ($tampilan == 'o') { ?>
      <div class="col-md-12">
        <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>Kode Order</th>
              <th>Fee Design</th>
              <th>Fee Eksekusi</th>
              <th>Nilai Eksekusi</th>
              <th>PIC Desain</th>
              <th>PIC Eksekusi</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($data as $key): ?>
              <tr>
                <td><?= $key['kode_order'] ?></td>
                <td><?= ($key['fee_design']) ? $key['fee_design'] : '0' ?></td>
                <td><?= ($key['persentase_eksekusi']) ? $key['persentase_eksekusi']." %" : "-"  ?></td>
                <td><?= ($key['fee_eksekusi']) ? $key['fee_eksekusi'] : "0" ?></td>
                <td><?= ($key['nama_desainer_design']) ? $key['nama_desainer_design'] : '-' ?></td>
                <td><?= ($key['nama_desainer_eksekusi']) ? $key['nama_desainer_eksekusi'] : '-' ?></td>
                <td><a class="btn btn-primary" href="<?= site_url("web/$class/feedesain/".$key['id']) ?>"> detail</a></td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
        </div>

      </div>
    <?php } else { ?>
      <div class="col-md-12">
        <?php foreach ($data as $key) { ?>
        
          <div class="row mt-4 row-order-baru" onclick="listorder(<?= $key['id'] ?>)" style="cursor:pointer;color:#212529; background-color:#c8e6c9;">
            <div class="col-7 col-sm-6 col-md-9 list-order-baru">
              <label><?= $key['name']  ?></label>
              <!-- <p>Staff Desainer</p> -->
              <p><?= $key['no_hp'] ?></p>
            </div>
            
          </div>
        <?php } ?>
      </div>

    <?php } ?>
    
</div>

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pencarian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post">
      <div class="modal-body">
        <input type="hidden" name="filterExist" value="1">
        <div class="form-group row">
          <div class="col-12 col-md-12 col-md-12">
            <input type="text" name="kode_order" class="form-control" placeholder="Kode Order" value="<?= ($filter['kode_order']) ? $filter['kode_order'] : null ?>">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12 col-md-12 col-md-12">
            <input type="text" name="nama_konsumen" class="form-control" placeholder="Nama" value="<?= ($filter['nama_konsumen']) ? $filter['nama_konsumen'] : null ?>">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12 col-md-12 col-md-12">
            <input type="text" name="alamat_konsumen" class="form-control" placeholder="Alamat" value="<?= ($filter['alamat_konsumen']) ? $filter['alamat_konsumen'] : null ?>">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12 col-md-12 col-md-12">
            <input type="text" name="no_hp_konsumen" class="form-control" placeholder="No HP" value="<?= ($filter['no_hp_konsumen']) ? $filter['no_hp_konsumen'] : null ?>">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="<?= site_url("web/$class/$method") ?>" class="btn btn-sm btn-secondary" >Reset</a>
        <button type="submit" class="btn btn-sm btn-add">Terapkan</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- Detail Modal -->
<div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="kodeOrderModal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id" id="idorder">
        <div class="row" >

        </div>
      </div>
    </div>
  </div>
</div>

<script>
	$(document).ready(function(){
    
        $('#prev').click(function(){
          if($('#month').val() == '01' ){
            var bulan = 12;
            var tahun = parseInt($('#year').val()) - 1;
            $('#year').val(tahun);
          }else if (parseInt($('#month').val()) <= 12) {
            var bulan = parseInt($('#month').val()) - 1;
            var tahun = $('#year').val();
          }

          bulan = tambahNol(bulan);
          $('#month').val(bulan);
          $('#main_form').submit();

        })

        $('#next').click(function(){
          if($('#month').val() == '12' ){
            var bulan = 1;
            var tahun = parseInt($('#year').val()) + 1;
            $('#year').val(tahun);
          }else if(parseInt($('#month').val()) >= 1){
            var bulan = parseInt($('#month').val()) + 1;
            var tahun = $('#year').val();
          }
          
          bulan = tambahNol(bulan);

          $('#month').val(bulan);
          $('#main_form').submit();
          
        })

        $(document).on('click','.detail-order',function(){
          var id = $(this).attr('id');
          window.location.href = "<?= site_url('web/pesanan/detail')?>/"+id;
        })

        $(document).on('change','.tampilan',function(){
          var val= $(this).val();
          // alert(val);
          window.location.href = "<?= site_url('web/fee_desainer')?>/?filter="+val;
        })

        $(document).on('click','.add-slip-gaji',function(){
          var id = $(this).attr('id');
          window.location.href = "<?= site_url('web/order_desainer/slipgajiadd')?>/"+id;
        })

        
      })

      function tambahNol(x){
         y=(x>9)?x:'0'+x;
         return y;
      }

      function bulanTahunInd(x){
        var dt = new Date(x);
        var monthNames = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
        
        var y = monthNames[dt.getMonth()]+' '+dt.getFullYear();

        return y;
      }

      function tanggalBulanTahunInd(x){
        var dt = new Date(x);
        var monthNames = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
        
        var y = dt.getDay()+' '+monthNames[dt.getMonth()]+' '+dt.getFullYear();

        return y;
      }

      function listorder(desainer){
        window.location.href = "<?= site_url('web/fee_desainer/detail')?>/"+desainer;
      }
</script>