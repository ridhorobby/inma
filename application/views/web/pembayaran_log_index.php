<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Kas</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button>
        </span>
        <span style="float: right;">
		  <a href="<?php echo site_url('web/pembayaran_log/add')?>" class="btn btn-sm btn-add"><i class="fas fa-plus"></i> Tambah</a>
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
    	<div class="kalender-kas text-center">
    		<button type="button" id="prev" class="btn btn-sm btn-light float-left"><i class="fas fa-angle-left"></i></button>
    		<label id="tanggal_kas"><?= xIndoMonth($_SESSION['pembayaran_log']['bulan']).' '.$_SESSION['pembayaran_log']['tahun'] ?></label>
        
    		<button type="button" id="next" class="btn btn-sm btn-light float-right"><i class="fas fa-angle-right"></i></button>
        <form id="main_form" action="<?php echo site_url('web/pembayaran_log')?>" method="post">
          <input type="hidden" name="bulan" id="month" value="<?= $_SESSION['pembayaran_log']['bulan']?>">
          <input type="hidden" name="tahun" id="year" value="<?= $_SESSION['pembayaran_log']['tahun']?>">
        </form>
    	</div>
    </div>
    <div class="col-md-12">
       	<table class="table table-sm table-kas">
          <thead>
            <tr>
              <th class="jenis-kas-th">Jenis</th>
              <th class="kode-kas-th">Kode</th>
              <th class="debet-kas-th">Debet</th>
              <th class="kredit-kas-th">Kredit</th>
            </tr>
          </thead>
          <tbody>
          <?php
            foreach ($data as $v) {
              $no++;
              $totdebet += ($v['tipe'] == 2 ? $v['nominal'] : 0); 
              $totkredit += ($v['tipe'] == 1 ? $v['nominal'] : 0); 
          ?>
            <tr id="<?= $v['id']?>" class="rowkas">
              <td colspan="4">
              	<div class="row kas-row">
	              	<div class="col-12 col-sm-12 col-md-12">
		              	<span style="float: left;"><?= xFormatDateInd($v['tgl_input']) ?></span>
		              	<span style="float: right;"><?= $v['created_by_nama'] ?></span>
		              </div>
	              	<div class="col-12 col-sm-12 col-md-12">
	              		<div class="row">
	              			<div class="col-4 col-sm-4 col-md-6"><?= $v['nama_jenis'] ?></div>
  				            <div class="col-2 col-sm-2 col-md-2 kode"><?= $v['kode_order'] ?></div>
  				            <div class="col-3 col-sm-3 col-md-2 text-center debet"><?= ($v['tipe'] == 2 ? number_format($v['nominal'],0,'.','.') : '-')?></div>
  				            <div class="col-3 col-sm-3 col-md-2 text-center kredit"><?= ($v['tipe'] == 1 ? number_format($v['nominal'],0,'.',',') : '-')?></div>
	              		</div>
              		</div>
              	</div>
              </td>
            </tr>
          <?php } ?>
          </tbody>
          <tfoot>
            <th colspan="2">Total</th>
            <th class="debet-kas-th"><?= number_format($totdebet,0,'.',',') ?></th>
            <th class="kredit-kas-th"><?= number_format($totkredit,0,'.',',') ?></th>
          </tfoot>
        </table>
    </div>
</div>

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Urutkan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row filter-kas">
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="<?= site_url("web/$class/index/ia") ?>">
        			<div>Tanggal Input Lama -> Terbaru</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="<?= site_url("web/$class/index/id") ?>">
        			<div>Tanggal Input Terbaru -> Lama</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="<?= site_url("web/$class/index/ja") ?>">
        			<div>Jenis A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="<?= site_url("web/$class/index/jd") ?>">
        			<div>Jenis Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="<?= site_url("web/$class/index/ka") ?>">
        			<div>Kode A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="<?= site_url("web/$class/index/kd") ?>">
        			<div>Kode Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="<?= site_url("web/$class/index/da") ?>">
        			<div>Debit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="<?= site_url("web/$class/index/dd") ?>">
        			<div>Debit Max -> Min</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="<?= site_url("web/$class/index/kra") ?>">
        			<div>Kredit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="<?= site_url("web/$class/index/krd") ?>">
        			<div>Kredit Max -> Min</div>
        		</a>
        	</div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<script>

  $(document).ready(function(){
    $('.rowkas').click(function(){
      var id = $(this).attr('id');
      window.location.href = "<?php echo site_url('web/pembayaran_log/edit')?>/"+id;
    })
  })

  $(document).ready(function(){
    
    $('#prev').click(function(){
      if($('#month').val() == '01' ){
        var bulan = 12;
        var tahun = parseInt($('#year').val()) - 1;
        $('#year').val(tahun);
      }else if (parseInt($('#month').val()) <= 12) {
        var bulan = parseInt($('#month').val()) - 1;
        var tahun = $('#year').val();
      }

      bulan = tambahNol(bulan);
      $('#month').val(bulan);
      $('#main_form').submit();

    })

    $('#next').click(function(){
      if($('#month').val() == '12' ){
        var bulan = 1;
        var tahun = parseInt($('#year').val()) + 1;
        $('#year').val(tahun);
      }else if(parseInt($('#month').val()) >= 1){
        var bulan = parseInt($('#month').val()) + 1;
        var tahun = $('#year').val();
      }
      
      bulan = tambahNol(bulan);

      $('#month').val(bulan);
      $('#main_form').submit();
      
    })
  })

  function tambahNol(x){
     y=(x>9)?x:'0'+x;
     return y;
  }

  function bulanTahunInd(x){
    var dt = new Date(x);
    var monthNames = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    
    var y = monthNames[dt.getMonth()]+' '+dt.getFullYear();

    return y;
  }

  function tanggalBulanTahunInd(x){
    var dt = new Date(x);
    var monthNames = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    
    var y = dt.getDay()+' '+monthNames[dt.getMonth()]+' '+dt.getFullYear();

    return y;
  }
</script>