<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Laba Rugi</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
    <div class="flashdata">
      <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
          <?php echo $srvmsg ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
    </div>
    <?php } ?>
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button>
        </span>
        <span style="float: right;">
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
    	<div class="kalender-kas text-center">
    		<button type="button" id="prev" class="btn btn-sm btn-light float-left"><i class="fas fa-angle-left"></i></button>
    		<label><?= $_SESSION['labarugi']['tahun'] ?></label>
    		<button type="button" id="next" class="btn btn-sm btn-light float-right"><i class="fas fa-angle-right"></i></button>
            <form id="main_form" action="<?php echo site_url('web/pengajuan_harga/getlabarugibulanan')?>" method="post">
            <input type="hidden" id="year" name="tahun" value="<?= $_SESSION['labarugi']['tahun'] ?>">
            </form>
    	</div>
    </div>
    <div class="col-md-12">
        <?php
          foreach ($data as $key) {
            $no++; 
            $pengeluaran += $key['pengeluaran'];
            $pendapatan += $key['pendapatan'];
            
        ?>
          <div class="row hutang-produksi-row" style="cursor: pointer;" onclick="getdetail('<?= $key['bulan'] ?>');">
            <div class="col-12 col-sm-12 col-md-12">
              <label><?= xIndoMonth($key['bulan']) ?></label>
            </div>
            <div class="col-3 col-sm-3 col-md-4">
              <span>Pengeluaran</span>
              <p class="text-red">(<?= number_format($key['pengeluaran'],0,'.',',') ?>)</p>
            </div>
            <div class="col-6 col-sm-6 col-md-4 text-center">
              <span>Pendapatan</span>
              <p class="text-green"><?= number_format($key['pendapatan'],0,'.',',') ?></p>
            </div>
            <div class="col-3 col-sm-3 col-md-4 text-right">
              <span class="">Nett Profit</span>
              <p><?= number_format($key['pendapatan'] - $key['pengeluaran'],0,'.',',') ?></p>
            </div>
          </div>
        <?php } ?>
    </div>

    <div class="col-md-12">
      <div class="row total-hutang-produksi-row" style="border-top: 1px solid #aaa;">
        <div class="col-3 col-sm-3 col-md-4 text-center" style="background-color: #ffebee;">
          <p class="text-red"><?= number_format($pengeluaran,0,'.',',') ?></p>
        </div>
        <div class="col-6 col-sm-6 col-md-4 text-center" style="background-color: #e8f5e9;">
            <p class="text-green"><?= number_format($pendapatan,0,'.',',') ?></p>
        </div>
        <div class="col-3 col-sm-3 col-md-4 text-center">
          <p><?= number_format($pendapatan - $pengeluaran,0,'.',',') ?></p>
        </div>
      </div>
    </div>
</div>

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Urutkan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row filter-kas">
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Tanggal Input Lama -> Terbaru</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Tanggal Input Terbaru -> Lama</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Jenis A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Jenis Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kode A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kode Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Debit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Debit Max -> Min</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kredit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kredit Max -> Min</div>
        		</a>
        	</div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<form id="detail_form" action="<?php echo site_url('web/pengajuan_harga/getdetaillaba');?>" method="post">
    <input type="hidden" name="bulan" id="detail_bulan">
    <input type="hidden" name="tahun" id="detail_tahun" value="<?= $_SESSION['labarugi']['tahun'] ?>">
</form>

<script>
	$(document).ready(function(){
    
    $('#prev').click(function(){
      
      var tahun = parseInt($('#year').val()) - 1;
      $('#year').val(tahun);
      $('#main_form').submit();

    })

    $('#next').click(function(){
      
      var tahun = parseInt($('#year').val()) + 1;
      $('#year').val(tahun);
      $('#main_form').submit();
      
    })
  })

    function getdetail(bulan){
        $('#detail_bulan').val(bulan);
        $('#detail_form').submit();

    }
</script>