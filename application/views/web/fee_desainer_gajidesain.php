
 <div class="row mb-4">
    <div class="col-md-12 title-page">
            <h1>Set Gaji Pokok Desainer <?= $user['name']?></h1>
        </div>
  </div>

  <?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>

  <div class="row mb-4">
    <div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
          <a class="btn btn-sm btn-add" href="<?php echo site_url('web/fee_desainer/detail/'.$desainer_id)?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            </span>
        </div>
  </div>    

    <div class="row harga-add">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<form action="<?= site_url("web/$class/") ?>/<?= ($data==null) ? "createpokok/$desainer_id" : "updatepokok/$gaji_desainer_id/$desainer_id" ?>" method="POST">
			 	<input type="hidden" name="bulan" value="<?= $bulan  ?>">
			 	<input type="hidden" name="tahun" value="<?= $tahun  ?>">
              
              <div class="form-group row">
                <label class="col-12 col-sm-12 col-md-2">Gaji Pokok</label>
                <div class="col-12 col-sm-12 col-md-4">
                  <input type="text" name="gaji_pokok" class="form-control form-control-sm" value="<?= $data['gaji_pokok']  ?>">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-12 col-sm-12 col-md-2">Gaji Tunjangan</label>
                <div class="col-12 col-sm-12 col-md-4">
                  <input type="number" name="gaji_tunjangan" class="form-control form-control-sm" value="<?= $data['gaji_tunjangan']  ?>">
                </div>
              </div>

              
              <div class="form-group text-center mt-5">
                <button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
              </div>
        	</form>
            
        </div>
    </div>

<?php 
	// echo "<pre>";var_dump($data);echo "</pre>";
?>
