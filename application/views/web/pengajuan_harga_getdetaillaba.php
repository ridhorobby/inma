<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>Laba Rugi <?= xIndoMonth($bulan).' '.$tahun ?></h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
          <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button>
        </span>
        <span style="float: right;">
          <a class="btn btn-sm btn-add" href="<?php echo site_url('web/'.$class.'/getlabarugibulanan')?>"><i class="fas fa-chevron-left"></i> Kembali</a>
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
       	<table class="table table-sm table-kas">
          <thead>
            <tr>
              <th class="jenis-kas-th">Keterangan</th>
              <th class="kode-kas-th"></th>
              <th class="debet-kas-th">Pengeluaran</th>
              <th class="kredit-kas-th">Pendapatan</th>
            </tr>
          </thead>
          <tbody>
          <?php
          $pengeluaran = 0;
          $pendapatan = 0;
            foreach ($data as $v) {
              $no++;
              $pengeluaran += $v['pengeluaran']; 
              $pendapatan += $v['pendapatan']; 
          ?>
            <tr id="<?= $v['id']?>" class="rowkas">
              <td colspan="4">
              	<div class="row kas-row">
	              	<div class="col-12 col-sm-12 col-md-12">
		              	<span style="float: left;"><?= xFormatDateInd($v['tgl_order_masuk']) ?></span>
		              </div>
	              	<div class="col-12 col-sm-12 col-md-12">
	              		<div class="row">
	              			<div class="col-4 col-sm-4 col-md-6">
                        <?= $v['kode_order'] ?>
                      </div>
  				            <div class="col-2 col-sm-2 col-md-2"></div>
  				            <div class="col-3 col-sm-3 col-md-2 text-center debet"><?= number_format($v['pengeluaran'],0,',','.')?></div>
  				            <div class="col-3 col-sm-3 col-md-2 text-center kredit"><?= number_format($v['pendapatan'],0,',','.')?></div>
	              		</div>
              		</div>
              	</div>
              </td>
            </tr>
          <?php } ?>
          </tbody>
          <tfoot>
            <th colspan="2">Total</th>
            <th class="debet-kas-th"><?= number_format($pengeluaran,0,'.',',') ?></th>
            <th class="kredit-kas-th"><?= number_format($pendapatan,0,'.',',') ?></th>
          </tfoot>
        </table>
    </div>
</div>

<!-- Filter Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Urutkan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row filter-kas">
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Tanggal Input Lama -> Terbaru</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Tanggal Input Terbaru -> Lama</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Jenis A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Jenis Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kode A -> Z</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kode Z -> A</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Debit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Debit Max -> Min</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kredit Min -> Max</div>
        		</a>
        	</div>
        	<div class="col-12 col-md-12 col-md-12">
        		<a href="#">
        			<div>Kredit Max -> Min</div>
        		</a>
        	</div>
        </div>
        
      </div>
    </div>
  </div>
</div>
