<link href="<?php echo base_url()?>assets/web/plugins/bootstrap/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/web/plugins/bootstrap/js/bootstrap-toggle.min.js"></script>
<style>
	.granit,
	.granit-jual,
	.granit-pokok{
		background-color: #dee2e6;
	    padding: 0 5px;
	}
	.keramik,
	.keramik-jual,
	.keramik-pokok{
		background-color: #dcedc8;
	    padding: 0 5px;
	}
	.jumlah,
	.jml{
	    width: 55px;
	    text-align: center;
        padding: 10px 0;
	}

	.table-harga th,
	.table-harga td{
		border-top: 1px solid #777;
	}
	.table-harga .th-harga,
	.table-harga .th-subtotal,
	.table-harga .th-jml,
	.table-harga .jml,
	.table-harga .granit,
	.table-harga .keramik{
		text-align: center;
	}
	.btn-granit{
		background-color: #dee2e6;
	    padding: 5px 50px;
	}
	.btn-keramik{
		background-color: #dcedc8;
	    padding: 5px 50px;
	}
	.share-icon-check,
	.share-icon{
	    cursor: pointer;
		float: right;
		right: 20%;
	    font-size: 1.25rem;
		position: absolute;
		color: #f71212;
		z-index: 1;
	}
	.logo-nasatech{
		position: absolute;
	    right: 2%;
	    top: 25px;
	}
	.logo-nasatech > label{
		font-weight: 400;
	    font-size: 2em;
	    margin: 0;
	}
	.logo-nasatech > label > b{
		color: #f71212;
	}
	.logo-nasatech > span{
		float: right;
	    font-size: .75rem;
        position: relative;
    	top: -15px;
	    right: 10px;
	}
	.hide-hrg-kons{
		display: none!important;
	}
	.hide-hrg-prod{
		display: none!important;
	}

	@media (max-width: 768px) {
		.logo-nasatech {
		    top: 0px;
		}
		.share-icon-check, 
		.share-icon{
			top: 0;
		    font-size: 16px;
	        right: 26%;
		}

		#strukModal,
		#strukCheckModal{
		    font-size: 11px;
		}

		.logo-nasatech > span {
		    top: -13px;
		    right: 3px;
		}

		<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) { ?>
		#strukCheckModal .btn-granit,
		#strukModal .btn-granit,
		#strukCheckModal .btn-keramik,
		#strukModal .btn-keramik{
		    font-size: 10px;
	        margin-top: 25px!important;
		}

		.swithproduksi-div > div {
		    top: 95px!important;
		    left: 8px!important;
		}
		<?php }else{ ?>
		.swithproduksi-div > div {
		    top: 90px!important;
		    left: 8px!important;
		}
		#strukCheckModal .btn-granit,
		#strukModal .btn-granit,
		#strukCheckModal .btn-keramik,
		#strukModal .btn-keramik{
		    font-size: 10px;
		}
		<?php } ?>

		.swithproduksi-div > div > label{
			padding-right: 20px!important;
		}

		#strukCheckModal .toggle.btn-xs,
		#strukModal .toggle.btn-xs{
			min-height: 1.2rem;
		}
		.btn-group-xs>.btn, .btn-xs {
		    padding: .35rem .3rem .15rem .3rem!important;
		    font-size: 0.75rem!important;
		    line-height: .4!important;
		}
	}

	@media (min-width: 576px) {
		/*#inputHargaModal .modal-dialog {
		    max-width: 650px;
		    margin: 1.75rem auto;
		}*/

		/*#strukModal .modal-dialog {
		    max-width: 700px;
		    margin: 1.75rem auto;
		}*/
	}

	/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) {
$harga_jual = 'display: none;';		              	
$harga_produksi = 'display: block;';		              	
}else{
$harga_jual = 'display: block;';		              	
$harga_produksi = 'display: none;';		              	

} ?>
	<div class="row mb-4">
		<div class="col-md-12 title-page">
            <h1>Pengajuan Harga</h1>
        </div>
	</div>

	<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
	<div class="row mb-4">
		<div class="col-md-12">
            <span style="float: left;">
            </span>
            <span style="float: right;">
			    <a class="btn btn-sm btn-add" href="<?php echo site_url('web/'.$class)?>"><i class="fas fa-chevron-left"></i> Kembali</a>
            </span>
        </div>
	</div>    

    <div class="row harga-add">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        	<form action="<?php echo site_url('web/'.$class)?>/<?= ($method=='edit' ? 'update' : 'create') ?>" method="post">
        	<?php if($method=='edit'){ ?>
	          <input type="hidden" name="id" value="<?= $data['id'] ?>">
	        <?php } ?>
	        	<input type="hidden" name="status" value="1">
	           	<div class="form-group row">
	           		<label class="col-6 col-sm-6 col-md-2">Konsumen</label>
           			<div class="col-6 col-sm-6 col-md-4 text-right">
           				<input type="hidden" id="kode_order">
           				<input type="hidden" name="id_konsumen" id="id_konsumen" value="0">
	           			<button type="button" class="btn btn-sm btn-add" data-target="#pilihKonsumenModal" data-toggle="modal"><i class="fas fa-list nav-icon"></i></button>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Nama</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="text" name="nama_konsumen" id="nama_konsumen" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['nama_konsumen'] : '') ?>" required>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Alamat</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<textarea type="text" name="alamat_konsumen" id="alamat_konsumen" class="form-control form-control-sm"  required><?= ($method=='edit' ? $data['alamat_konsumen'] : '') ?></textarea>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">No HP</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="text" name="no_hp_konsumen" id="no_hp_konsumen" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['no_hp_konsumen'] : '') ?>" required>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Pilih Jenis</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<select name="jenis" class="form-control form-control-sm" id="jenis" required>
	           				<option value="">-- Pilih Jenis --</option>
	           				<option value="1">Granit</option>
	           				<option value="2">Keramik</option>
	           			</select>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Tanggal Pengajuan</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<button type="button" class="btn btn-sm btn-outline-secondary btn-block btn-pilih-tanggal"><?= xFormatDateInd(date('Y-m-d'))?></button>
	           			<input type="text" name="tgl_pengajuan" class="form-control form-control-sm" id="tgl_pengajuan" value="<?= $method=='edit' ? $data['tgl_pengajuan'] : date('Y-m-d') ?>" readonly style="visibility:hidden">
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Keterangan</label>
	           		<div class="col-12 col-sm-12 col-md-4">
	           			<input type="text" name="keterangan" class="form-control form-control-sm" value="<?= ($method=='edit' ? $data['keterangan'] : '') ?>">
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2"></label>
           			<div class="col-12 col-sm-12 col-md-4">
	           			<button type="button" class="btn btn-sm btn-outline-secondary btn-block" data-target="#inputHargaModal" data-toggle="modal">Input Harga</button>
	           		</div>
	           	</div>

	           	<div class="form-group row">
	           		<label class="col-12 col-sm-12 col-md-2">Daftar Harga</label>
           			<div class="col-12 col-sm-12 col-md-4 text-right">
	           			<button type="button" class="btn btn-sm btn-outline-secondary" onclick="getcheckstruk()" style="color: #f71212;border-color: #f71212;"><i class="fas fa-tasks nav-icon"></i></button>
	           			<button type="button" class="btn btn-sm btn-add" onclick="getstruk()"><i class="fas fa-list-alt nav-icon"></i></button>
	           		</div>
	           	</div>

	           	<div class="table-temp"></div>

	           	<div class="form-group text-center mt-5">
	           		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
	           	</div>
        	</form>
        </div>        
  	</div>

	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg">
          <div class="modal-content" style="border-color: #007fff;">
             <div class="modal-body" style="max-height: calc(100vh - 210px);overflow-y: auto;">
               <fieldset style="display: block;">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row">
                        
                    </div>
               </fieldset>
             </div>
          </div>
       </div>
    </div>

    <!-- Pilih Konsumen Modal -->
	<div class="modal fade" id="pilihKonsumenModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Pilih...</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="col-md-12">

	  			<div class="form-group row search-deal">
	           		<div class="col-12 col-sm-12 col-md-10">
						<input type="text" name="keyword-deal" id="keyword-deal" class="form-control form-control-sm" placeholder="Keyword">
	           		</div>
	           		<div class="col-12 col-sm-12 col-md-2">
	           			<button class="btn btn-primary" type="button" onclick="cariDeal()"> Cari</button>
	           		</div>
	           	</div>
	      	
	      	</div>

	      	<div class="col-md-12 list-deal">

		        <?php foreach ($konsumen_pengajuan as $key): ?>
					<div class="row mt-2 row-order-baru pilih-konsumen" style="cursor:pointer;color:#212529; ">
						<div class="col-7 col-sm-6 col-md-8 list-order-baru">
							<input type="hidden" class="konsumen_id" value="<?= $key['konsumen_id']  ?>">
							<label class="kode_order"><?= $key['kode_order']  ?></label>
							<p class="nama_konsumen"><?= $key['nama_konsumen']  ?></p>
							<p class="alamat_konsumen"><?= substr($key['alamat_konsumen'],0,50).'...'  ?></p>
							<p style="display: none;" class="alamat_konsumen_extend"><?= $key['alamat_konsumen']  ?></p>
							<p style="display: none;" class="no_hp_konsumen"><?= $key['no_hp_konsumen']  ?></p>
						</div>
						<div class="col-5 col-sm-6 col-md-4 list-order-baru text-right">
							<label></label>
							<p><?= $key['no_hp_konsumen']  ?></p>
							<div class="status-produksi">
								<?php if($key['status_produksi'] == 1) {
									echo '<span style="background-color: #4caf50;">Bisa Cek Ruangan</span>';
								}elseif($key['status_produksi'] == 2) {
									echo '<span style="background-color: #4caf50;">Bisa Pasang Rangka</span>';
								}elseif($key['status_produksi'] == 3) {
									echo '<span style="background-color: #f71212;">Menunggu Cor Siap</span>';
								} ?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>

			<div id="right">
				<div id="loader" style="display: none">Loading ...</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Daftar Harga Modal -->
	<div class="modal fade" id="inputHargaModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: auto;">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Daftar Harga</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
				<table class="table table-sm table-harga">
		          <tbody>
		          <?php foreach ($list_harga as $index => $row){ ?>
		          <tr style="background-color:#dedede">
		              <td><b><?= $index; ?></b></td>
		              <td><b>m/unit</b></td>
		              <td><b>@Harga</b></td>
		          </tr>
		          <?php foreach ($row as $v) {
		              $no++; 
		          ?>
		            <tr id="<?= $v['id']?>" class="rowharga">
		              	<input type="hidden" name="nama_harga[]" class="nama_harga" value="<?= $v['nama_harga']?>">
		              	<input type="hidden" name="granit_jual[]" min="0" class="harga-jual-granit" value="0">
		              	<input type="hidden" name="keramik_jual[]" min="0" class="harga-jual-keramik" value="0">

		              	<input type="hidden" name="granit_pokok[]" min="0" class="harga-pokok-granit" value="0">
		              	<input type="hidden" name="keramik_pokok[]" min="0" class="harga-pokok-keramik" value="0">
		              <td>
		              	<?= $v['nama_harga']; ?>
		              </td>
		              <td>
		              	<input type="text" name="jml[]" min="0" class="jml no-modal allownumericwithdecimal" value="">
		              </td>
		              <td style="padding: 5px 0;">
		              	<div class="granit-jual text-right" style="<?= $harga_jual ?>">0</div>
		              	<div class="keramik-jual text-right" style="<?= $harga_jual ?>">0</div>

		              	<div class="granit-pokok text-right" style="<?= $harga_produksi ?>">0</div>
		              	<div class="keramik-pokok text-right" style="<?= $harga_produksi ?>">0</div>
		              </td>
		            </tr>
		          <?php } 
		          } ?>
		          </tbody>
		        </table>

		        <div class="form-group text-center mt-5">
	           		<button type="button" id="simpandaftarharga" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
	           	</div>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Update Harga Modal -->
	<div class="modal fade" id="updateHargaModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="hargaModalLabel"></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) {
				$row_harga_edit = 'display: flex;';		              	
				$harga_jual_edit = 'display: block;';		              	
				$harga_produksi_edit = 'display: block;';
				$harga_readonly = '';
			}else{
				$row_harga_edit = 'display: none;';		              	
				$harga_jual_edit = 'display: none;';		              	
				$harga_produksi_edit = 'display: none;';
				$harga_readonly = 'readonly';	              	

			} ?>
	      	<input type="hidden" id="id_harga">
			<div class="form-group row">
           		<div class="col-12 col-sm-12 col-md-4">Jumlah</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="text" name="jml_edit" id="jml_edit" min="0" class="form-control form-control-sm allownumericwithdecimal" required>
           		</div>
           	</div>	
           	<h5>Harga Konsumen</h5>
           	<div class="form-group row">
           		<div class="col-12 col-sm-12 col-md-4 pl-3">Harga Granit</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="number" name="harga_jual_granit" id="harga_jual_granit" class="form-control form-control-sm" required <?= $harga_readonly ?> >
           		</div>
           	</div>
           	<div class="form-group row">
           		<div class="col-12 col-sm-12 col-md-4 pl-3">Harga Keramik</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="number" name="harga_jual_keramik" id="harga_jual_keramik" class="form-control form-control-sm" required <?= $harga_readonly ?>>
           		</div>
           	</div>

           	<h5 style="<?= $row_harga_edit ?>">Harga Produksi</h5>
           	<div class="form-group row" style="<?= $row_harga_edit ?>">
           		<div class="col-12 col-sm-12 col-md-4 pl-3">Harga Granit</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="number" name="harga_pokok_granit" id="harga_pokok_granit" class="form-control form-control-sm" required>
           		</div>
           	</div>
           	<div class="form-group row" style="<?= $row_harga_edit ?>">
           		<div class="col-12 col-sm-12 col-md-4 pl-3">Harga Keramik</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="number" name="harga_pokok_keramik" id="harga_pokok_keramik" class="form-control form-control-sm" required>
           		</div>
           	</div>

           	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) { ?>
           	<table class="table table-sm">
           		<tr>
           			<td></td>
           			<td class="text-right">Hrg Produksi</td>
           			<td class="text-right">Hrg Konsumen</td>
           		</tr>
           		<tr class="granit">
           			<td>Granit</td>
           			<td class="hrg-produksi text-right"></td>
           			<td class="hrg-konsumen text-right"></td>
           		</tr>
           		<tr class="keramik">
           			<td>Keramik</td>
           			<td class="hrg-produksi text-right"></td>
           			<td class="hrg-konsumen text-right"></td>
           		</tr>
           	</table>
           <?php } ?>

		        <div class="form-group text-center mt-5">
	           		<button type="button" id="removeharga" class="btn btn-sm btn-outline-secondary">Hapus</button>
	           		<button type="button" id="simpanharga" class="btn btn-sm btn-add">Simpan</button>
	           	</div>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Update List Harga Modal -->
	<div class="modal fade" id="updateListHargaModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="hargaModalLabel"></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) {
				$row_harga_edit = 'display: flex;';		              	
				$harga_jual_edit = 'display: block;';		              	
				$harga_produksi_edit = 'display: block;';
				$harga_readonly = '';
			}else{
				$row_harga_edit = 'display: none;';		              	
				$harga_jual_edit = 'display: none;';		              	
				$harga_produksi_edit = 'display: none;';
				$harga_readonly = 'readonly';	              	

			} ?>
	      	<input type="hidden" id="id_harga">
			<div class="form-group row">
           		<div class="col-12 col-sm-12 col-md-4">Jumlah</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="text" name="jml_edit" id="jml_edit" min="0" class="form-control form-control-sm allownumericwithdecimal" required>
           		</div>
           	</div>	
           	<h5>Harga Konsumen</h5>
           	<div class="form-group row">
           		<div class="col-12 col-sm-12 col-md-4 pl-3">Harga Granit</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="number" name="harga_jual_granit" id="harga_jual_granit" class="form-control form-control-sm" required <?= $harga_readonly ?> >
           		</div>
           	</div>
           	<div class="form-group row">
           		<div class="col-12 col-sm-12 col-md-4 pl-3">Harga Keramik</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="number" name="harga_jual_keramik" id="harga_jual_keramik" class="form-control form-control-sm" required <?= $harga_readonly ?>>
           		</div>
           	</div>

           	<h5 style="<?= $row_harga_edit ?>">Harga Produksi</h5>
           	<div class="form-group row" style="<?= $row_harga_edit ?>">
           		<div class="col-12 col-sm-12 col-md-4 pl-3">Harga Granit</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="number" name="harga_pokok_granit" id="harga_pokok_granit" class="form-control form-control-sm" required>
           		</div>
           	</div>
           	<div class="form-group row" style="<?= $row_harga_edit ?>">
           		<div class="col-12 col-sm-12 col-md-4 pl-3">Harga Keramik</div>
           		<div class="col-12 col-sm-12 col-md-8">
           			<input type="number" name="harga_pokok_keramik" id="harga_pokok_keramik" class="form-control form-control-sm" required>
           		</div>
           	</div>

           	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR) { ?>
           	<table class="table table-sm">
           		<tr>
           			<td></td>
           			<td class="text-right">Hrg Produksi</td>
           			<td class="text-right">Hrg Konsumen</td>
           		</tr>
           		<tr class="granit">
           			<td>Granit</td>
           			<td class="hrg-produksi text-right"></td>
           			<td class="hrg-konsumen text-right"></td>
           		</tr>
           		<tr class="keramik">
           			<td>Keramik</td>
           			<td class="hrg-produksi text-right"></td>
           			<td class="hrg-konsumen text-right"></td>
           		</tr>
           	</table>
           <?php } ?>

		        <div class="form-group text-center mt-5">
	           		<button type="button" id="removeharga_list" class="btn btn-sm btn-outline-secondary">Hapus</button>
	           		<button type="button" id="simpanharga_list" class="btn btn-sm btn-add">Simpan</button>
	           	</div>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Struk Check Modal -->
	<div class="modal fade" id="strukCheckModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: auto;">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
				

	      </div>
	    </div>
	  </div>
	</div>

	<!-- Struk Modal -->
	<div class="modal fade" id="strukModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: auto;">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
				

	      </div>
	    </div>
	  </div>
	</div>

	<!-- Download Modal -->
	<div class="modal fade" id="downloadStrukModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h5 class="modal-title" id="exampleModalLabel"></h5>
	          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">×</span>
	          </button>
	        </div>
	        <div class="modal-body">Apakah anda ingin mendownload / preview pdf stuk ini ?</div>
	        <div class="modal-footer">
	          <!-- <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button> -->
          	  <button type="button" id="preview-check-granit" class="btn btn-sm btn-info preview-struk-check" tipeharga="kons" style="display: none;"><i class="fas fa-eye"></i> Preview Pdf</button>
          	  <button type="button" id="preview-granit" class="btn btn-sm btn-info preview-struk" tipeharga="kons" style="display: none;">><i class="fas fa-eye"></i> Preview Pdf</button>
	          <a class="btn btn-sm btn-add download-struk" href="#"><i class="fas fa-download"></i> Download</a>
	        </div>
	      </div>
	    </div>
	</div>

<script src="<?php echo base_url()?>assets/web/js/html2canvas.js"></script>

<script>

	var hargaArray = [];
	getListHarga();
	// console.log(hargaArray);

	$(document).ready(function(){

		$(document).on('click','.preview-struk-check', function(){
			var jenis = $(this).attr('id');
			var jenis = jenis.replace("preview-check-", "");
			var tipeharga = $(this).attr('tipeharga');
			var kode_order = $('#kode_order').val();
			var nama_konsumen = $('#nama_konsumen').val();
			var alamat_konsumen = $('#alamat_konsumen').val();
			var no_hp_konsumen = $('#no_hp_konsumen').val();

			$.ajax({
				type: "post",
				url: "<?= site_url('web/pengajuan_harga/ajaxgetsavetosession') ?>",
				data: {arrharga: hargaArray,jenis: jenis, tipeharga: tipeharga, kode_order: kode_order,nama_konsumen: nama_konsumen, alamat_konsumen: alamat_konsumen,no_hp_konsumen: no_hp_konsumen},
				// dataType: "json",
				success: function(res){
					var url = '<?php echo site_url('web/pengajuan_harga/ajaxpreviewcheckpdf') ?>'
					window.open(url, '_blank');
				}
			})
		})

		$(document).on('click','.share-icon-check', function(){
			$('.preview-struk').hide();
			$('.preview-struk-check').show();

			var jenis = $(this).attr('id');
            html2canvas($('#canvas'), {
            	height: 2000,
	        onrendered: function (canvas) {

	                var imgageData =  
		            canvas.toDataURL("image/png",1); 
		           
		            var newData = imgageData.replace( 
		            /^data:image\/png/, "data:application/octet-stream"); 
		           
		            $(".download-struk").attr( 
		            "download", jenis+".png").attr( 
		            "href", newData);

	                $('#downloadStrukModal').modal('show');

	            }
	        });
		})

		$(document).on('click','.preview-struk', function(){
			var jenis = $(this).attr('id');
			var jenis = jenis.replace("preview-", "");
			var tipeharga = $(this).attr('tipeharga');
			var kode_order = $('#kode_order').val();
			var nama_konsumen = $('#nama_konsumen').val();
			var alamat_konsumen = $('#alamat_konsumen').val();
			var no_hp_konsumen = $('#no_hp_konsumen').val();

			$.ajax({
				type: "post",
				url: "<?= site_url('web/pengajuan_harga/ajaxgetsavetosession') ?>",
				data: {arrharga: hargaArray,jenis: jenis, tipeharga: tipeharga, kode_order: kode_order,nama_konsumen: nama_konsumen, alamat_konsumen: alamat_konsumen,no_hp_konsumen: no_hp_konsumen},
				// dataType: "json",
				success: function(res){
					var url = '<?php echo site_url('web/pengajuan_harga/ajaxpreviewpdf') ?>'
					window.open(url, '_blank');
				}
			})
		})

		$(document).on('click','.share-icon', function(){
			$('.preview-struk').show();
			$('.preview-struk-check').hide();

			var jenis = $(this).attr('id');
            html2canvas($('#canvas'), {
            	height: 2000,
	        onrendered: function (canvas) {

	                var imgageData =  
		            canvas.toDataURL("image/png",1); 
		           
		            var newData = imgageData.replace( 
		            /^data:image\/png/, "data:application/octet-stream"); 
		           
		            $(".download-struk").attr( 
		            "download", jenis+".png").attr( 
		            "href", newData);

	                $('#downloadStrukModal').modal('show');

	            }
	        });
		})

		$(".download-struk").click(function(){
            $('#downloadStrukModal').modal('hide');			
		})

		$(document).on('click','.pilih-konsumen', function(){
			var kode_order = $(this).find('.kode_order').html();
			var konsumen_id = $(this).find('.konsumen_id').html();
			var nama_konsumen = $(this).find('.nama_konsumen').html();
			var alamat_konsumen = $(this).find('.alamat_konsumen_extend').html();
			var no_hp_konsumen = $(this).find('.no_hp_konsumen').html();

			$("#kode_order").val(kode_order);
			$("#id_konsumen").val(konsumen_id);
			$("#nama_konsumen").val(nama_konsumen);
			$("#alamat_konsumen").val(alamat_konsumen);
			$("#no_hp_konsumen").val(no_hp_konsumen);
			$('#pilihKonsumenModal').modal('hide');
		})
		
		$('.btn-pilih-tanggal').on('click', function() {
		      $('#tgl_pengajuan').datepicker('show');
		});

		var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
		  "Juli", "Agustus", "September", "Oktober", "November", "Desember"
		];
		$('#tgl_pengajuan').datepicker({
		    format: 'yyyy-mm-dd',
		    autoclose: true
		}).on('change', function(e) {
		    var tanggal = $('#tgl_pengajuan').val();
		    var dt = new Date(tanggal);
		    var html = dt.getDate()+' '+monthNames[dt.getMonth()]+' '+dt.getFullYear();
		    $('.btn-pilih-tanggal').html(html);
		    
		});
		$(document).on('keyup mouseup','.jumlah', function(){
			var id = $(this).parents('.rowharga_tr').attr('dataid');
			var jml = $(this).val();
			var jml = jml.replace(/(\.\d{1})\d+/g, '$1');

			$.ajax({
				type: "post",
				url: "<?= site_url('web/pengajuan_harga/ajaxgetharga') ?>",
				data: {id: id},
				dataType: "json",
				success: function(res){
			    	// console.log(jml);
			    	var hjg = $('.harga_'+id).find('.harga-jual-granit').val();
					var hjk = $('.harga_'+id).find('.harga-jual-keramik').val();
					var hpg = $('.harga_'+id).find('.harga-pokok-granit').val();
					var hpk = $('.harga_'+id).find('.harga-pokok-keramik').val();
			    	
			    	if (hjg == '0' || hjk == '0' || hpg == '0' || hpk == '0') {
				    	var harga_jual_granit = res.harga_jual_granit;
				    	var harga_jual_keramik = res.harga_jual_keramik;

				    	var harga_pokok_granit = res.harga_pokok_granit;
				    	var harga_pokok_keramik = res.harga_pokok_keramik;
			    	}else{
			    		var harga_jual_granit = parseInt(hjg);
				    	var harga_jual_keramik = parseInt(hjk);

				    	var harga_pokok_granit = parseInt(hpg);
				    	var harga_pokok_keramik = parseInt(hpk);
			    	}
			    	
			    	$('.harga_'+id).find('.jml').val(jml);

			    	$('.harga_'+id).find('.harga-jual-granit').val(harga_jual_granit);
					$('.harga_'+id).find('.harga-jual-keramik').val(harga_jual_keramik);

					$('.harga_'+id).find('.granit-jual').html(formatRupiah(Math.round(harga_jual_granit*jml)));
					$('.harga_'+id).find('.keramik-jual').html(formatRupiah(Math.round(harga_jual_keramik*jml)));

					$('.harga_'+id).find('.harga-pokok-granit').val(harga_pokok_granit);
					$('.harga_'+id).find('.harga-pokok-keramik').val(harga_pokok_keramik);

					$('.harga_'+id).find('.granit-pokok').html(formatRupiah(Math.round(harga_pokok_granit*jml)));
					$('.harga_'+id).find('.keramik-pokok').html(formatRupiah(Math.round(harga_pokok_keramik*jml)));

			    	$('#'+id).find('.jml').val(jml);

			    	$('#'+id).find('.harga-jual-granit').val(harga_jual_granit);
					$('#'+id).find('.harga-jual-keramik').val(harga_jual_keramik);

					$('#'+id).find('.granit-jual').html(formatRupiah(Math.round(harga_jual_granit*jml)));
					$('#'+id).find('.keramik-jual').html(formatRupiah(Math.round(harga_jual_keramik*jml)));

					$('#'+id).find('.harga-pokok-granit').val(harga_pokok_granit);
					$('#'+id).find('.harga-pokok-keramik').val(harga_pokok_keramik);

					$('#'+id).find('.granit-pokok').html(formatRupiah(Math.round(harga_pokok_granit*jml)));
					$('#'+id).find('.keramik-pokok').html(formatRupiah(Math.round(harga_pokok_keramik*jml)));

					updateHargaArray(id,jml,harga_jual_granit,harga_jual_keramik,harga_pokok_granit,harga_pokok_keramik,hargaArray);
					// console.log(hargaArray);

		    	},
			});
		})


		$(document).on('keyup mouseup','.jml', function(){
			var id = $(this).parents('.rowharga').attr('id');
			var jml = $(this).val();
			var jml = jml.replace(/(\.\d{1})\d+/g, '$1');

			$.ajax({
				type: "post",
				url: "<?= site_url('web/pengajuan_harga/ajaxgetharga') ?>",
				data: {id: id},
				dataType: "json",
				success: function(res){
			    	// console.log(jml);
			    	var hjg = $('#'+id).find('.harga-jual-granit').val();
					var hjk = $('#'+id).find('.harga-jual-keramik').val();
					var hpg = $('#'+id).find('.harga-pokok-granit').val();
					var hpk = $('#'+id).find('.harga-pokok-keramik').val();
			    	
			    	if (hjg == '0' || hjk == '0' || hpg == '0' || hpk == '0') {
				    	var harga_jual_granit = res.harga_jual_granit;
				    	var harga_jual_keramik = res.harga_jual_keramik;

				    	var harga_pokok_granit = res.harga_pokok_granit;
				    	var harga_pokok_keramik = res.harga_pokok_keramik;
			    	}else{
			    		var harga_jual_granit = parseInt(hjg);
				    	var harga_jual_keramik = parseInt(hjk);

				    	var harga_pokok_granit = parseInt(hpg);
				    	var harga_pokok_keramik = parseInt(hpk);
			    	}


			    	$('#'+id).find('.jml').val(jml);

			    	$('#'+id).find('.harga-jual-granit').val(harga_jual_granit);
					$('#'+id).find('.harga-jual-keramik').val(harga_jual_keramik);

					$('#'+id).find('.granit-jual').html(formatRupiah(Math.round(harga_jual_granit*jml)));
					$('#'+id).find('.keramik-jual').html(formatRupiah(Math.round(harga_jual_keramik*jml)));

					$('#'+id).find('.harga-pokok-granit').val(harga_pokok_granit);
					$('#'+id).find('.harga-pokok-keramik').val(harga_pokok_keramik);

					$('#'+id).find('.granit-pokok').html(formatRupiah(Math.round(harga_pokok_granit*jml)));
					$('#'+id).find('.keramik-pokok').html(formatRupiah(Math.round(harga_pokok_keramik*jml)));

					updateHargaArray(id,jml,harga_jual_granit,harga_jual_keramik,harga_pokok_granit,harga_pokok_keramik,hargaArray);
					// console.log(hargaArray);

		    	},
			});
		})

		$(document).on('keyup mouseup','#jml_edit', function(){
			var jml = $(this).val();
			var jml = jml.replace(/(\.\d{1})\d+/g, '$1');
			$(this).val(jml);
		});

		$(document).on('click','.no-modal', function(e){
		    e.stopPropagation();
		})

		$(document).on('click','.rowharga', function(){

			var id = $(this).attr('id');
			var jml = $(this).find('.jml').val();

			if (jml == 0) {
				jml = 1;
			}

			<?php if(SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DESAIN){ ?>
			if (id == '26') {
				$('#updateHargaModal #harga_jual_granit').attr('readonly', false);
				$('#updateHargaModal #harga_jual_keramik').attr('readonly', false);
			}else{
				$('#updateHargaModal #harga_jual_granit').attr('readonly', true);
				$('#updateHargaModal #harga_jual_keramik').attr('readonly', true);
			}
			<?php } ?>

			$.ajax({
				type: "post",
				url: "<?= site_url('web/pengajuan_harga/ajaxgetharga') ?>",
				data: {id: id},
				dataType: "json",
				success: function(res){
			       // console.log(jml);
			  //   	var nama_harga = $(this).find('.nama_harga').val();

			  		var hjg = $('#inputHargaModal #'+id).find('.harga-jual-granit').val();
					var hjk = $('#inputHargaModal #'+id).find('.harga-jual-keramik').val();
					var hpg = $('#inputHargaModal #'+id).find('.harga-pokok-granit').val();
					var hpk = $('#inputHargaModal #'+id).find('.harga-pokok-keramik').val();
			    	
			    	if (hjg == '0' || hjk == '0' || hpg == '0' || hpk == '0') {
				    	var harga_jual_granit = res.harga_jual_granit;
				    	var harga_jual_keramik = res.harga_jual_keramik;

				    	var harga_pokok_granit = res.harga_pokok_granit;
				    	var harga_pokok_keramik = res.harga_pokok_keramik;
			    	}else{
			    		var harga_jual_granit = parseInt(hjg);
				    	var harga_jual_keramik = parseInt(hjk);

				    	var harga_pokok_granit = parseInt(hpg);
				    	var harga_pokok_keramik = parseInt(hpk);
			    	}

					$('#updateHargaModal #hargaModalLabel').html(res.nama);
					$('#updateHargaModal #id_harga').val(id);
					$('#updateHargaModal #jml_edit').val(jml);

					$('#updateHargaModal #harga_jual_granit').val(harga_jual_granit);
					$('#updateHargaModal #harga_jual_keramik').val(harga_jual_keramik);
					$('#updateHargaModal #harga_pokok_granit').val(harga_pokok_granit);
					$('#updateHargaModal #harga_pokok_keramik').val(harga_pokok_keramik);

					$('#updateHargaModal .granit .hrg-produksi').html(formatRupiah(res.harga_pokok_granit));
					$('#updateHargaModal .granit .hrg-konsumen').html(formatRupiah(res.harga_jual_granit));
					$('#updateHargaModal .keramik .hrg-produksi').html(formatRupiah(res.harga_pokok_keramik));
					$('#updateHargaModal .keramik .hrg-konsumen').html(formatRupiah(res.harga_jual_keramik));

					$('#updateHargaModal').modal('show');
			        	
			    },
			});
		})

		$(document).on('click','#simpanharga', function(){
			var id = $('#updateHargaModal #id_harga').val();
			var jml = $('#updateHargaModal #jml_edit').val();

			var harga_jual_granit = $('#updateHargaModal #harga_jual_granit').val();
			var harga_jual_keramik = $('#updateHargaModal #harga_jual_keramik').val();
			var harga_pokok_granit = $('#updateHargaModal #harga_pokok_granit').val();
			var harga_pokok_keramik = $('#updateHargaModal #harga_pokok_keramik').val();

			$('#'+id).find('.jml').val(jml);

			$('#'+id).find('.harga-jual-granit').val(harga_jual_granit);
			$('#'+id).find('.harga-jual-keramik').val(harga_jual_keramik);

			$('#'+id).find('.granit-jual').html(formatRupiah(Math.round(harga_jual_granit*jml)));
			$('#'+id).find('.keramik-jual').html(formatRupiah(Math.round(harga_jual_keramik*jml)));

			$('#'+id).find('.harga-pokok-granit').val(harga_pokok_granit);
			$('#'+id).find('.harga-pokok-keramik').val(harga_pokok_keramik);

			$('#'+id).find('.granit-pokok').html(formatRupiah(Math.round(harga_pokok_granit*jml)));
			$('#'+id).find('.keramik-pokok').html(formatRupiah(Math.round(harga_pokok_keramik*jml)));

			updateHargaArray(id,jml,harga_jual_granit,harga_jual_keramik,harga_pokok_granit,harga_pokok_keramik,hargaArray);

			$('#updateHargaModal').modal('hide');

		})

		$(document).on('click','#removeharga', function(){
			var id = $('#updateHargaModal #id_harga').val();

			$('#'+id).find('.jml').val(0);
			$('#'+id).find('.harga-jual-granit').val(0);
			$('#'+id).find('.harga-jual-keramik').val(0);

			$('#'+id).find('.granit-jual').html(0);
			$('#'+id).find('.keramik-jual').html(0);

			$('#'+id).find('.harga-pokok-granit').val(0);
			$('#'+id).find('.harga-pokok-keramik').val(0);

			$('#'+id).find('.granit-pokok').html(0);
			$('#'+id).find('.keramik-pokok').html(0);

			updateHargaArray(id,"0","0","0","0","0",hargaArray);

			$('#updateHargaModal').modal('hide');

		})

		$(document).on('click','.rowharga_tr', function(){

			var id = $(this).attr('dataid');
			var jml = $(this).find('.jumlah').val();

			if (jml == 0) {
				jml = 1;
			}

			<?php if(SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DESAIN){ ?>
			if (id == '26') {
				$('#updateListHargaModal #harga_jual_granit').attr('readonly', false);
				$('#updateListHargaModal #harga_jual_keramik').attr('readonly', false);
			}else{
				$('#updateListHargaModal #harga_jual_granit').attr('readonly', true);
				$('#updateListHargaModal #harga_jual_keramik').attr('readonly', true);
			}
			<?php } ?>

			$.ajax({
				type: "post",
				url: "<?= site_url('web/pengajuan_harga/ajaxgetharga') ?>",
				data: {id: id},
				dataType: "json",
				success: function(res){
			       // console.log(jml);
			  //   	var nama_harga = $(this).find('.nama_harga').val();
			  		var hjg = $('.harga_'+id).find('.harga-jual-granit').val();
					var hjk = $('.harga_'+id).find('.harga-jual-keramik').val();
					var hpg = $('.harga_'+id).find('.harga-pokok-granit').val();
					var hpk = $('.harga_'+id).find('.harga-pokok-keramik').val();
			    	
			    	if (hjg == '0' || hjk == '0' || hpg == '0' || hpk == '0') {
				    	var harga_jual_granit = res.harga_jual_granit;
				    	var harga_jual_keramik = res.harga_jual_keramik;

				    	var harga_pokok_granit = res.harga_pokok_granit;
				    	var harga_pokok_keramik = res.harga_pokok_keramik;
			    	}else{
			    		var harga_jual_granit = parseInt(hjg);
				    	var harga_jual_keramik = parseInt(hjk);

				    	var harga_pokok_granit = parseInt(hpg);
				    	var harga_pokok_keramik = parseInt(hpk);
			    	}

					$('#updateListHargaModal #hargaModalLabel').html(res.nama);
					$('#updateListHargaModal #id_harga').val(id);
					$('#updateListHargaModal #jml_edit').val(jml);

					$('#updateListHargaModal #harga_jual_granit').val(harga_jual_granit);
					$('#updateListHargaModal #harga_jual_keramik').val(harga_jual_keramik);
					$('#updateListHargaModal #harga_pokok_granit').val(harga_pokok_granit);
					$('#updateListHargaModal #harga_pokok_keramik').val(harga_pokok_keramik);

					$('#updateListHargaModal .granit .hrg-produksi').html(formatRupiah(res.harga_pokok_granit));
					$('#updateListHargaModal .granit .hrg-konsumen').html(formatRupiah(res.harga_jual_granit));
					$('#updateListHargaModal .keramik .hrg-produksi').html(formatRupiah(res.harga_pokok_keramik));
					$('#updateListHargaModal .keramik .hrg-konsumen').html(formatRupiah(res.harga_jual_keramik));

					$('#updateListHargaModal').modal('show');
			        	
			    },
			});
		})
		$(document).on('click','#simpanharga_list', function(){
			var id = $('#updateListHargaModal #id_harga').val();
			var jml = $('#updateListHargaModal #jml_edit').val();

			var harga_jual_granit = $('#updateListHargaModal #harga_jual_granit').val();
			var harga_jual_keramik = $('#updateListHargaModal #harga_jual_keramik').val();
			var harga_pokok_granit = $('#updateListHargaModal #harga_pokok_granit').val();
			var harga_pokok_keramik = $('#updateListHargaModal #harga_pokok_keramik').val();

			$('.harga_'+id).find('.jumlah').val(jml);

			$('.harga_'+id).find('.harga-jual-granit').val(harga_jual_granit);
			$('.harga_'+id).find('.harga-jual-keramik').val(harga_jual_keramik);

			$('.harga_'+id).find('.granit-jual').html(formatRupiah(Math.round(harga_jual_granit*jml)));
			$('.harga_'+id).find('.keramik-jual').html(formatRupiah(Math.round(harga_jual_keramik*jml)));

			$('.harga_'+id).find('.harga-pokok-granit').val(harga_pokok_granit);
			$('.harga_'+id).find('.harga-pokok-keramik').val(harga_pokok_keramik);

			$('.harga_'+id).find('.granit-pokok').html(formatRupiah(Math.round(harga_pokok_granit*jml)));
			$('.harga_'+id).find('.keramik-pokok').html(formatRupiah(Math.round(harga_pokok_keramik*jml)));

			$('#'+id).find('.jml').val(jml);

	    	$('#'+id).find('.harga-jual-granit').val(harga_jual_granit);
			$('#'+id).find('.harga-jual-keramik').val(harga_jual_keramik);

			$('#'+id).find('.granit-jual').html(formatRupiah(Math.round(harga_jual_granit*jml)));
			$('#'+id).find('.keramik-jual').html(formatRupiah(Math.round(harga_jual_keramik*jml)));

			$('#'+id).find('.harga-pokok-granit').val(harga_pokok_granit);
			$('#'+id).find('.harga-pokok-keramik').val(harga_pokok_keramik);

			$('#'+id).find('.granit-pokok').html(formatRupiah(Math.round(harga_pokok_granit*jml)));
			$('#'+id).find('.keramik-pokok').html(formatRupiah(Math.round(harga_pokok_keramik*jml)));

			updateHargaArray(id,jml,harga_jual_granit,harga_jual_keramik,harga_pokok_granit,harga_pokok_keramik,hargaArray);

			$('#updateListHargaModal').modal('hide');

		})

		$(document).on('click','#removeharga_list', function(){
			var id = $('#updateListHargaModal #id_harga').val();

			$('.harga_'+id).find('.jml').val(0);
			$('.harga_'+id).find('.harga-jual-granit').val(0);
			$('.harga_'+id).find('.harga-jual-keramik').val(0);

			$('.harga_'+id).find('.granit-jual').html(0);
			$('.harga_'+id).find('.keramik-jual').html(0);

			$('.harga_'+id).find('.harga-pokok-granit').val(0);
			$('.harga_'+id).find('.harga-pokok-keramik').val(0);

			$('.harga_'+id).find('.granit-pokok').html(0);
			$('.harga_'+id).find('.keramik-pokok').html(0);

			$('.harga_'+id).remove();

			$('#'+id).find('.jml').val(0);
			$('#'+id).find('.harga-jual-granit').val(0);
			$('#'+id).find('.harga-jual-keramik').val(0);

			$('#'+id).find('.granit-jual').html(0);
			$('#'+id).find('.keramik-jual').html(0);

			$('#'+id).find('.harga-pokok-granit').val(0);
			$('#'+id).find('.harga-pokok-keramik').val(0);

			$('#'+id).find('.granit-pokok').html(0);
			$('#'+id).find('.keramik-pokok').html(0);

			updateHargaArray(id,"0","0","0","0","0",hargaArray);

			$('#updateListHargaModal').modal('hide');

		})

		$(document).on('click','#simpandaftarharga', function(){
			$('.table-temp').html('');

			<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
				var harga_jual = 'display: none;';		              	
				var harga_produksi = 'display: block;';		              	
			<?php }else{ ?>
				var harga_jual = 'display: block;';		              	
				var harga_produksi = 'display: none;';		              	

			<?php } ?>
			var html = '<table class="table table-sm"></table>';
			$('.table-temp').html(html);
			// var result = [];

			$.each(hargaArray, function(key, row) {
				

				var head = '<tr id="found_'+key+'" style="background-color:#dedede;display: none;">'+
				              '<td><b>'+row[0].kategori+'</b></td>'+
				              '<td><b>m/unit</b></td>'+
				              '<td><b>@Harga</b></td>'+
				          '</tr>';
		        $('.table-temp table').append(head); 

				$.each(row, function(i, v) {

					if (v.jml != '0') {

					var content = '<tr dataid="'+v.id+'" class="rowharga_tr harga_'+v.id+'" style="cursor: pointer;">'+
			              	'<input type="hidden" name="id[]" class="id" value="'+v.id+'">'+
			              	'<input type="hidden" name="harga_jual_granit[]" min="0" class="harga-jual-granit" value="'+v.harga_jual_granit+'">'+
			              	'<input type="hidden" name="harga_jual_keramik[]" min="0" class="harga-jual-keramik" value="'+v.harga_jual_keramik+'">'+

			              	'<input type="hidden" name="harga_pokok_granit[]" min="0" class="harga-pokok-granit" value="'+v.harga_pokok_granit+'">'+
			              	'<input type="hidden" name="harga_pokok_keramik[]" min="0" class="harga-pokok-keramik" value="'+v.harga_pokok_keramik+'">'+
			              '<td>'+v.nama_harga+'</td>'+
			              '<td><input type="text" name="jumlah[]" min="0" class="jumlah no-modal allownumericwithdecimal" value="'+v.jml+'"></td>'+
			              '<td style="padding: 5px 0;">'+
			              	'<div class="granit-jual text-right" style="'+harga_jual+'">'+formatRupiah(Math.round(v.harga_jual_granit*v.jml))+'</div>'+
			              	'<div class="keramik-jual text-right" style="'+harga_jual+'">'+formatRupiah(Math.round(v.harga_jual_keramik*v.jml))+'</div>'+

			              	'<div class="granit-pokok text-right" style="'+harga_produksi+'">'+formatRupiah(Math.round(v.harga_pokok_granit*v.jml))+'</div>'+
			              	'<div class="keramik-pokok text-right" style="'+harga_produksi+'">'+formatRupiah(Math.round(v.harga_pokok_keramik*v.jml))+'</div>'+
			              '</td>'+
			            '</tr>';
			        $('.table-temp table').append(content); 
					$('#found_'+key).show();


			    	}

				});

			});

			$('#inputHargaModal').modal('hide');
		})

		$(document).on('click','.btn-granit', function(){
			$(this).removeClass('btn-granit');
			var btnclass = $(this).addClass('btn-keramik');

			$('#strukModal .btn-keramik').html('<b>Keramik</b>');

			$('#strukModal #granit').attr('id','keramik');
			$('#preview-granit').attr('id','preview-keramik');
			$('#strukModal .th-harga').css({'background-color':'#dcedc8'});
			$('#strukModal .th-subtotal').css({'background-color':'#dcedc8'});
			$('#strukModal .granit').hide();
			$('#strukModal .keramik').show();

			$('#strukCheckModal .btn-keramik').html('<b>Keramik</b>');

			$('#strukCheckModal #granit').attr('id','keramik');
			$('#preview-check-granit').attr('id','preview-check-keramik');
			$('#strukCheckModal .th-harga').css({'background-color':'#dcedc8'});
			$('#strukCheckModal .th-subtotal').css({'background-color':'#dcedc8'});
			$('#strukCheckModal .granit').hide();
			$('#strukCheckModal .keramik').show();
		})

		$(document).on('click','.btn-keramik', function(){
			$(this).removeClass('btn-keramik');
			var btnclass = $(this).addClass('btn-granit');

			$('#strukModal .btn-granit').html('<b>Granit</b>');

			$('#strukModal #keramik').attr('id','granit');
			$('#preview-keramik').attr('id','preview-granit');
			$('#strukModal .th-harga').css({'background-color':'#dee2e6'});
			$('#strukModal .th-subtotal').css({'background-color':'#dee2e6'});
			$('#strukModal .keramik').hide();
			$('#strukModal .granit').show();

			$('#strukCheckModal .btn-granit').html('<b>Granit</b>');

			$('#strukCheckModal #keramik').attr('id','granit');
			$('#preview-check-keramik').attr('id','preview-check-granit');
			$('#strukCheckModal .th-harga').css({'background-color':'#dee2e6'});
			$('#strukCheckModal .th-subtotal').css({'background-color':'#dee2e6'});
			$('#strukCheckModal .keramik').hide();
			$('#strukCheckModal .granit').show();
		})

		<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
		$(document).on('change','.check-harga-produksi', function() {
		    if(this.checked) {
		        $('.hrg-kons-check').addClass('hide-hrg-kons');
		        $('.hrg-prod-check').removeClass('hide-hrg-prod');
		        $('.preview-struk-check').attr('tipeharga','prod');
		    }else{
				$('.hrg-prod-check').addClass('hide-hrg-prod');
		        $('.hrg-kons-check').removeClass('hide-hrg-kons');
		        $('.preview-struk-check').attr('tipeharga','kons');
		    }
		});

		$(document).on('change','.check-harga-produksi-full', function() {
		    if(this.checked) {
		        $('.hrg-kons').addClass('hide-hrg-kons');
		        $('.hrg-prod').removeClass('hide-hrg-prod');
		        $('.preview-struk').attr('tipeharga','prod');
		    }else{
				$('.hrg-prod').addClass('hide-hrg-prod');
		        $('.hrg-kons').removeClass('hide-hrg-kons');
		        $('.preview-struk').attr('tipeharga','kons');
		    }
		});
		<?php } ?>

		$(document).on("keypress keyup blur",".allownumericwithdecimal", function (event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
		    $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });


	})

	function getListHarga(){
		$.ajax({
			type: "post",
			url: "<?= site_url('web/pengajuan_harga/ajaxGetListHargaTemp') ?>",
			// data: {id: id},
			dataType: "json",
			success: function(res){
		    	// console.log(res);
		    	$.each(res, function(index, value) {
				    hargaArray.push(value);
				});
				// hargaArray.push(res);
		    	// console.log(res);


	    	},
		});
	}

	function updateHargaArray(idharga,jml,harga_jual_granit,harga_jual_keramik,harga_pokok_granit,harga_pokok_keramik,hargaArray){
		// var found = false;
		for(var x=0; x < hargaArray.length; x++){
			var arr = hargaArray[x];
			for (var i = 0; i < arr.length; i++) {
			    if(arr[i].id == idharga ){
			        arr[i].jml = jml;
			        arr[i].harga_jual_granit = harga_jual_granit;
			        arr[i].harga_jual_keramik = harga_jual_keramik;
			        arr[i].harga_pokok_granit = harga_pokok_granit;
			        arr[i].harga_pokok_keramik = harga_pokok_keramik;
			        // found = true;
			        break;
			    }
			}

		}

		// console.log(hargaArray);

	}

	function formatRupiah(bilangan){

	    var bilangan = bilangan.toString().replace(/\./g,"");
	    var number_string = bilangan.toString(),
	    sisa    = number_string.length % 3,
	    rupiah  = number_string.substr(0, sisa),
	    ribuan  = number_string.substr(sisa).match(/\d{3}/g);
	        
	    if (ribuan) {
	        separator = sisa ? '.' : '';
	        rupiah += separator + ribuan.join('.');
	    }

	    return rupiah;
	}

	function getcheckstruk(){
		$('#strukCheckModal .modal-body').html('');

		var kode_order = $('#kode_order').val();
		var nama_konsumen = $('#nama_konsumen').val();
		var alamat_konsumen = $('#alamat_konsumen').val();
		var no_hp_konsumen = $('#no_hp_konsumen').val();
		var html = '<div class="row">'+
		    	'<span class="share-icon-check" id="granit"><i class="fas fa-share-alt"></i></span>'+
			    '<div class="col-12 col-sm-12 col-md-12 swithproduksi-div">'+
			    	
					<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
			    	'<div style="position: absolute;z-index: 1;top: 150px;left: 8px;">'+
						'<label style="padding-right: 5px;color: #dc3545;">Harga Produksi</label>'+
						'<input class="check-harga-produksi" type="checkbox" data-toggle="toggle" data-size="xs" style="float:right;">'+
				    '</div>'+
					<?php } ?>

		    	'</div>'+    
			    '<div class="col-12 col-sm-12 col-md-12" id="canvas" style="background-color: #fff;">'+
			    	'<div class="logo-nasatech">'+
			    		'<label>Nasa<b>tech</b></label>'+
			    		'<br>'+
			    		'<span>Kitchenset</span>'+
			    	'</div>'+
			    	'<table style="margin-bottom: 15px;">'+
			    		'<input type="hidden" id="idpengajuan" value="<?= $data['id']?>">'+
			    		'<tr>'+
			    			'<td width="100px"><label>Kode Order</label></td>'+
			    			'<td><label class="text-red">'+kode_order+'</label></td>'+
			    		'</tr>'+
			    		'<tr>'+
			    			'<td style="font-weight: bold;">Nama</td>'+
			    			'<td>'+nama_konsumen+'</td>'+
			    		'</tr>'+
			    		'<tr>'+
			    			'<td style="font-weight: bold;">Alamat</td>'+
			    			'<td>'+alamat_konsumen+'</td>'+
			    		'</tr>'+
			    		'<tr>'+
			    			'<td style="font-weight: bold;">No HP</td>'+
			    			'<td>'+no_hp_konsumen+'</td>'+
			    		'</tr>'+
			    	'</table>'+
			    	'<div class="col-12 col-sm-12 col-md-12">'+
				    	'<div style="text-align: center;">'+
					    	'<button type="button" class="btn btn-sm btn-granit" style="margin:10px 0;"><b>Granit</b></button>'+
				    	'</div>'+
			    	'</div>'+
			    	'<table class="table table-sm table-harga">'+
			    		'<thead>'+
			    			'<th>No</th>'+
			    			'<th>Nama</th>'+
			    			'<th class="th-jml">Jml</th>'+
			    			'<th class="th-harga" style="background-color: #dee2e6;">@Harga</th>'+
			    			'<th class="th-subtotal" style="background-color: #dee2e6;">Subtotal</th>'+
			    		'</thead>'+
			          '<tbody>'+
			          '</tbody>'+
			        '</table>'+
			    '</div>'+

			'</div>';
		$('#strukCheckModal .modal-body').html(html);
		
		<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
		$('.check-harga-produksi').bootstrapToggle();
		<?php } ?>
					
					var nomor = 1;
					var totalgranitprod = 0;
		        	var totalkeramikprod = 0;
			        var totalgranit = 0;
			        var totalkeramik = 0;

					$.each(hargaArray, function(key, row) {
// console.log(row);
		var content =	'<tr class="kategori_'+key+'" style="background-color:#424242;color: #fff;">'+
							  '<td colspan="5"><b>'+row[0].kategori+'</b></td>'+
							'</tr>';

						$('#strukCheckModal .modal-body .table-harga tbody').append(content);
							
							var count_item = 0;
            				var j = 1;
							$.each(row, function(i, v) {

			        			if (v.jml > 0) {
			        			var no = nomor++;

			        			count_item += j++;

			        			totalgranitprod += Math.round(v.harga_pokok_granit*v.jml);
			        			totalkeramikprod += Math.round(v.harga_pokok_keramik*v.jml);

			        			totalgranit += Math.round(v.harga_jual_granit*v.jml);
			        			totalkeramik += Math.round(v.harga_jual_keramik*v.jml);
		var content_2 =         '<tr>'+
		          				  '<td>'+no+'</td>'+
					              '<td>'+v.nama_harga+'</td>'+
					              '<td class="jml">'+formatNumber(Number(v.jml))+'</td>'+

					              <?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
					              	'<td class="granit" style="background-color: #dee2e6;">'+formatRupiah(v.harga_pokok_granit)+'</td>'+
					              	'<td class="keramik" style="display: none;background-color: #dcedc8;">'+formatRupiah(v.harga_pokok_keramik)+'</td>'+

					              	'<td class="granit" style="background-color: #dee2e6;">'+formatRupiah(Math.round(v.harga_pokok_granit*v.jml))+'</td>'+
					              	'<td class="keramik" style="display: none;background-color: #dcedc8;">'+formatRupiah(Math.round(v.harga_pokok_keramik*v.jml))+'</td>'+
						          <?php }else{ ?>
						          	'<td class="granit hrg-kons-check" style="background-color: #dee2e6;">'+formatRupiah(v.harga_jual_granit)+'</td>'+
					              	'<td class="keramik hrg-kons-check" style="display: none;background-color: #dcedc8;">'+formatRupiah(v.harga_jual_keramik)+'</td>'+

					              	'<td class="granit hrg-kons-check" style="background-color: #dee2e6;">'+formatRupiah(Math.round(v.harga_jual_granit*v.jml))+'</td>'+
					              	'<td class="keramik hrg-kons-check" style="display: none;background-color: #dcedc8;">'+formatRupiah(Math.round(v.harga_jual_keramik*v.jml))+'</td>'+
									
									<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
										'<td class="granit hrg-prod-check hide-hrg-prod" style="background-color: #dee2e6;">'+formatRupiah(v.harga_pokok_granit)+'</td>'+
						              	'<td class="keramik hrg-prod-check hide-hrg-prod" style="display: none;background-color: #dcedc8;">'+formatRupiah(v.harga_pokok_keramik)+'</td>'+

						              	'<td class="granit hrg-prod-check hide-hrg-prod" style="background-color: #dee2e6;">'+formatRupiah(Math.round(v.harga_pokok_granit*v.jml))+'</td>'+
						              	'<td class="keramik hrg-prod-check hide-hrg-prod" style="display: none;background-color: #dcedc8;">'+formatRupiah(Math.round(v.harga_pokok_keramik*v.jml))+'</td>'+
						          	<?php } ?>
						          
						          <?php } ?>

					            '</tr>';

								$('#strukCheckModal .modal-body .table-harga tbody').append(content_2);
			        			}

			          		})

			          		if (count_item == 0) {
			          			$('.kategori_'+key).remove();
			          		} 
			          });
		var html_2 = '<tr>'+
          	'<td colspan="3" style="text-align: right"><b>Total</b></td>'+
          	<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
          		'<td colspan="2" class="granit" style="text-align: right"><b>'+formatRupiah(totalgranitprod)+'</b></td>'+
          		'<td colspan="2" class="keramik" style="text-align: right;display: none;"><b>'+formatRupiah(totalkeramikprod)+'</b></td>'+
        	<?php }else{ ?>

          	'<td colspan="2" class="granit hrg-kons-check" style="text-align: right"><b>'+formatRupiah(totalgranit)+'</b></td>'+
          	'<td colspan="2" class="keramik hrg-kons-check" style="text-align: right;display: none;"><b>'+formatRupiah(totalkeramik)+'</b></td>'+
				<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
					'<td colspan="2" class="granit hrg-prod-check hide-hrg-prod" style="text-align: right"><b>'+formatRupiah(totalgranitprod)+'</b></td>'+
          			'<td colspan="2" class="keramik hrg-prod-check hide-hrg-prod" style="text-align: right;display: none;"><b>'+formatRupiah(totalkeramikprod)+'</b></td>'+
	        	<?php } ?>
        	<?php } ?>

          '</tr>';

		$('#strukCheckModal .modal-body .table-harga tbody').append(html_2);
		
		$('#strukCheckModal').modal('show');
	}

	function getstruk(){
		$('#strukModal .modal-body').html('');

		var kode_order = $('#kode_order').val();
		var nama_konsumen = $('#nama_konsumen').val();
		var alamat_konsumen = $('#alamat_konsumen').val();
		var no_hp_konsumen = $('#no_hp_konsumen').val();
		var html = '<div class="row">'+
		    	'<span class="share-icon" id="granit"><i class="fas fa-share-alt"></i></span>'+
			    '<div class="col-12 col-sm-12 col-md-12 swithproduksi-div">'+
			    	
					<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
			    	'<div style="position: absolute;z-index: 1;top: 150px;left: 8px;">'+
						'<label style="padding-right: 5px;color: #dc3545;">Harga Produksi</label>'+
						'<input class="check-harga-produksi-full" type="checkbox" data-toggle="toggle" data-size="xs" style="float:right;">'+
				    '</div>'+
					<?php } ?>

		    	'</div>'+    
			    '<div class="col-12 col-sm-12 col-md-12" id="canvas" style="background-color: #fff;">'+
			    	'<div class="logo-nasatech">'+
			    		'<label>Nasa<b>tech</b></label>'+
			    		'<br>'+
			    		'<span>Kitchenset</span>'+
			    	'</div>'+
			    	'<table style="margin-bottom: 15px;">'+
			    		'<input type="hidden" id="idpengajuan" value="<?= $data['id']?>">'+
			    		'<tr>'+
			    			'<td width="100px"><label>Kode Order</label></td>'+
			    			'<td><label class="text-red">'+kode_order+'</label></td>'+
			    		'</tr>'+
			    		'<tr>'+
			    			'<td style="font-weight: bold;">Nama</td>'+
			    			'<td>'+nama_konsumen+'</td>'+
			    		'</tr>'+
			    		'<tr>'+
			    			'<td style="font-weight: bold;">Alamat</td>'+
			    			'<td>'+alamat_konsumen+'</td>'+
			    		'</tr>'+
			    		'<tr>'+
			    			'<td style="font-weight: bold;">No HP</td>'+
			    			'<td>'+no_hp_konsumen+'</td>'+
			    		'</tr>'+
			    	'</table>'+
			    	'<div class="col-12 col-sm-12 col-md-12">'+
				    	'<div style="text-align: center;">'+
					    	'<button type="button" class="btn btn-sm btn-granit" style="margin:10px 0;"><b>Granit</b></button>'+
				    	'</div>'+
			    	'</div>'+
			    	'<table class="table table-sm table-harga">'+
			    		'<thead>'+
			    			'<th>No</th>'+
			    			'<th>Nama</th>'+
			    			'<th class="th-jml">Jml</th>'+
			    			'<th class="th-harga" style="background-color: #dee2e6;">@Harga</th>'+
			    			'<th class="th-subtotal" style="background-color: #dee2e6;">Subtotal</th>'+
			    		'</thead>'+
			          '<tbody>'+
			          '</tbody>'+
			        '</table>'+
			    '</div>'+

			'</div>';
		$('#strukModal .modal-body').html(html);
		
		<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
		$('.check-harga-produksi-full').bootstrapToggle();
		<?php } ?>
					
					var nomor = 1;
					var totalgranitprod = 0;
		        	var totalkeramikprod = 0;
			        var totalgranit = 0;
			        var totalkeramik = 0;

					$.each(hargaArray, function(key, row) {
// console.log(row);
		var content =	'<tr style="background-color:#424242;color: #fff;">'+
							  '<td colspan="5"><b>'+row[0].kategori+'</b></td>'+
							'</tr>';

						$('#strukModal .modal-body .table-harga tbody').append(content);

							$.each(row, function(i, v) {
			        			var no = nomor++;

			        			totalgranitprod += Math.round(v.harga_pokok_granit*v.jml);
			        			totalkeramikprod += Math.round(v.harga_pokok_keramik*v.jml);

			        			totalgranit += Math.round(v.harga_jual_granit*v.jml);
			        			totalkeramik += Math.round(v.harga_jual_keramik*v.jml);
		var content_2 =         '<tr>'+
		          				  '<td>'+no+'</td>'+
					              '<td>'+v.nama_harga+'</td>'+
					              '<td class="jml">'+formatNumber(Number(v.jml))+'</td>'+

					              <?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
					              	'<td class="granit" style="background-color: #dee2e6;">'+formatRupiah(v.harga_pokok_granit)+'</td>'+
					              	'<td class="keramik" style="display: none;background-color: #dcedc8;">'+formatRupiah(v.harga_pokok_keramik)+'</td>'+

					              	'<td class="granit" style="background-color: #dee2e6;">'+formatRupiah(Math.round(v.harga_pokok_granit*v.jml))+'</td>'+
					              	'<td class="keramik" style="display: none;background-color: #dcedc8;">'+formatRupiah(Math.round(v.harga_pokok_keramik*v.jml))+'</td>'+
						          <?php }else{ ?>
						          	'<td class="granit hrg-kons" style="background-color: #dee2e6;">'+formatRupiah(v.harga_jual_granit)+'</td>'+
					              	'<td class="keramik hrg-kons" style="display: none;background-color: #dcedc8;">'+formatRupiah(v.harga_jual_keramik)+'</td>'+

					              	'<td class="granit hrg-kons" style="background-color: #dee2e6;">'+formatRupiah(Math.round(v.harga_jual_granit*v.jml))+'</td>'+
					              	'<td class="keramik hrg-kons" style="display: none;background-color: #dcedc8;">'+formatRupiah(Math.round(v.harga_jual_keramik*v.jml))+'</td>'+
									
									<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
										'<td class="granit hrg-prod hide-hrg-prod" style="background-color: #dee2e6;">'+formatRupiah(v.harga_pokok_granit)+'</td>'+
						              	'<td class="keramik hrg-prod hide-hrg-prod" style="display: none;background-color: #dcedc8;">'+formatRupiah(v.harga_pokok_keramik)+'</td>'+

						              	'<td class="granit hrg-prod hide-hrg-prod" style="background-color: #dee2e6;">'+formatRupiah(Math.round(v.harga_pokok_granit*v.jml))+'</td>'+
						              	'<td class="keramik hrg-prod hide-hrg-prod" style="display: none;background-color: #dcedc8;">'+formatRupiah(Math.round(v.harga_pokok_keramik*v.jml))+'</td>'+
						          	<?php } ?>
						          
						          <?php } ?>

					            '</tr>';

								$('#strukModal .modal-body .table-harga tbody').append(content_2);

			          		}) 
			          });
		var html_2 = '<tr>'+
          	'<td colspan="3" style="text-align: right"><b>Total</b></td>'+
          	<?php if (SessionManagerWeb::getRole() == Role::PRODUKSI) { ?>
          		'<td colspan="2" class="granit" style="text-align: right"><b>'+formatRupiah(totalgranitprod)+'</b></td>'+
          		'<td colspan="2" class="keramik" style="text-align: right;display: none;"><b>'+formatRupiah(totalkeramikprod)+'</b></td>'+
        	<?php }else{ ?>

          	'<td colspan="2" class="granit hrg-kons" style="text-align: right"><b>'+formatRupiah(totalgranit)+'</b></td>'+
          	'<td colspan="2" class="keramik hrg-kons" style="text-align: right;display: none;"><b>'+formatRupiah(totalkeramik)+'</b></td>'+
				<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR) { ?>
					'<td colspan="2" class="granit hrg-prod hide-hrg-prod" style="text-align: right"><b>'+formatRupiah(totalgranitprod)+'</b></td>'+
          			'<td colspan="2" class="keramik hrg-prod hide-hrg-prod" style="text-align: right;display: none;"><b>'+formatRupiah(totalkeramikprod)+'</b></td>'+
	        	<?php } ?>
        	<?php } ?>

          '</tr>';

		$('#strukModal .modal-body .table-harga tbody').append(html_2);
		
		$('#strukModal').modal('show');
	}

	function  formatNumber(num) {
        var curr = num.toFixed(1).replace(',', '.');
        var parts = curr.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, "");
        return parts.join(".");           
	}

	function cariDeal(){
		$('#loader').fadeIn('slow');
		$('.list-deal').empty();
		var keyword = $('#keyword-deal').val();
		$.ajax({
			type: "post",
			url: "<?= site_url('web/pengajuan_harga') ?>/getpengajuanadd",
			data: {keyword: keyword,ajax:1},
			success:function(respon){
				var html = '';
				if(respon!='false'){
					var data = JSON.parse(respon);

	         		
	         		for (var i =0; i < data.length; i++) {
	         			alamat = data[i].alamat_konsumen;
	         			html += 
	         				'<div class="row mt-2 row-order-baru pilih-konsumen" style="cursor:pointer;color:#212529; ">'+
		         				'<div class="col-7 col-sm-6 col-md-8 list-order-baru">'+
			         				'<input type="hidden" class="konsumen_id" value="'+data[i].konsumen_id+'">'+
									'<label class="kode_order">'+data[i].kode_order+'</label>'+
									'<p class="nama_konsumen">'+data[i].nama_konsumen+'</p>'+
									'<p class="alamat_konsumen">'+alamat+'...</p>'+
									'<p style="display: none;" class="alamat_konsumen_extend">'+data[i].alamat_konsumen+'  ?></p>'+
									'<p style="display: none;" class="no_hp_konsumen">'+data[i].no_hp_konsumen+'</p>'+
								'</div>'+
								'<div class="col-5 col-sm-6 col-md-4 list-order-baru text-right">'+
									'<label></label>'+
									'<p>'+data[i].no_hp_konsumen+'</p>'+
									'<div class="status-produksi">';

										if (data[i].status_produksi == 1) {
											html += '<span style="background-color: #4caf50;">Bisa Cek Ruangan</span>';
										}
										else if(data[i].status_produksi == 2){
											html += '<span style="background-color: #4caf50;">Bisa Pasang Rangka</span>';
										}
										else if(data[i].status_produksi == 3){
											html +='<span style="background-color: #f71212;">Menunggu Cor Siap</span>';
										}

								html +=
									'</div>'+
								'</div>'+
							'</div>';
		         					
	         		}
				}else{
					html+='<div>Data tidak ditemukan..</div>';
				}

         		
         		$('.list-deal').html(html);
         		$('#loader').css("display", "none");
	        	
	        }
		});
		// console.log(keyword);
	}

	// function disableScroll() {
	// 	document.body.style.overflow = 'hidden';
	// 	document.querySelector('html').scrollTop = window.scrollY;
	// }

	// function enableScroll() {
	// 	document.body.style.overflow = null;
	// }

</script>
