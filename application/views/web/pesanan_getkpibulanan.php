<div class="row mb-4">
	<div class="col-md-12 title-page">
        <h1>KPI Bulanan</h1>
    </div>
</div>

<?php if (isset($srvok)) { ?>
  <div class="flashdata">
    <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
        <?php echo $srvmsg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  <?php } ?>
  
<div class="row mb-4">
	<div class="col-md-12">
        <span style="float: left;">
			<!-- <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#filterModal"><i class="fas fa-sort-amount-down"></i> Filter</button> -->
        </span>
        <span style="float: right;">
		  <a href="<?php echo site_url('web/pesanan/addpembukuan')?>" class="btn btn-sm btn-add"><i class="fas fa-plus"></i> Tambah</a>
        </span>
    </div>
</div>    

<div class="row">
    <div class="col-md-12">
    	<div class="kalender-kas text-center">
    		<button type="button" id="prev" class="btn btn-sm btn-light float-left"><i class="fas fa-angle-left"></i></button>
    		<label id="tahun_kpi"></label>
    		<input type="hidden" id="year" value="<?= date('Y')?>">
    		<button type="button" id="next" class="btn btn-sm btn-light float-right"><i class="fas fa-angle-right"></i></button>
    	</div>
    </div>
    <div class="col-md-12">
    	<div class="row hutang-produksi-row" style="margin-left: 0;margin-right: 0;">
	      	<div class="col-10 col-sm-10 col-md-10">
	      		<div class="row">
			      	<div class="col-6 col-sm-6 col-md-6">
			          <label>Bulan</label>
			        </div>
			        <div class="col-6 col-sm-6 col-md-6 text-right">
			          <span>Total Pesanan</span>
			        </div>
			        <div class="col-4 col-sm-4 col-md-4 text-center" style="background-color: #ffebee;">
			          <span>Blm Selesai</span>
			        </div>
			        <div class="col-4 col-sm-4 col-md-4 text-center" style="background-color: #E3F2FD;">
			          <span>Selesai</span>
			        </div>
			        <div class="col-4 col-sm-4 col-md-4 text-center" style="background-color: #e8f5e9;">
			          <span>Tepat Waktu</span>
			        </div>
	      		</div>
	      	</div>
	      	<div class="col-2 col-sm-2 col-md-2 text-right">
	      		<span>KPI %</span>
	      	</div>
	    </div>
	</div>

    <div class="col-md-12 kpi-div">
    </div>
</div>

<script>
	var tahun = '<?php echo date('Y');?>';

	var bulantahun = tahunInd(tahun);
  	$('#tahun_kpi').html(tahun);
  	getKpiBulanan(tahun);

	$(document).ready(function(){
    
	    $('#prev').click(function(){
	        var tahun = parseInt($('#year').val()) - 1;
	        $('#year').val(tahun);
	      
	    	$('#tahun_kpi').html(tahun);
			getKpiBulanan(tahun);
		})

		$('#next').click(function(){
			var tahun = parseInt($('#year').val()) + 1;
	        $('#year').val(tahun);

	    	$('#tahun_kpi').html(tahun);
			getKpiBulanan(tahun)
		})
	})

	function getKpiBulanan(tahun){
		$('.kpi-div').html('');
		$.ajax({
	        url : "<?php echo site_url('/web/pesanan/ajaxgetkpibulanan');?>",
	        type: 'post',
	        dataType: 'json',
	        data: {tahun:tahun},
	        success:function(respon){
	        	$.each(respon, function(k, v) {
	        	// console.log(bulanInd(v.bulan));
	        		var blm_selesai = parseInt(v.total_pesanan)-parseInt(v.pesanan_selesai);
	        		if (parseInt(v.pesanan_selesai) != 0) {
		        		var kpi = (100 / parseInt(v.pesanan_selesai)) * parseInt(v.pesanan_selesai_tepat);
	        		}else{
	        			var kpi = 0;
	        		}
	        		
	        		var html = 
	        		'<div class="row hutang-produksi-row" style="margin-left: 0;margin-right: 0;">'+
				      	'<div class="col-10 col-sm-10 col-md-10">'+
				      		'<div class="row">'+
						      	'<div class="col-6 col-sm-6 col-md-6">'+
						          '<label>'+bulanInd(v.bulan)+'</label>'+
						        '</div>'+
						        '<div class="col-6 col-sm-6 col-md-6 text-right">'+
						          '<span>Total Pesanan : '+v.total_pesanan+'</span>'+
						        '</div>'+
						        '<div class="col-4 col-sm-4 col-md-4 text-center" style="background-color: #ffebee;">'+
						          '<span>'+blm_selesai+'</span>'+
						        '</div>'+
						        '<div class="col-4 col-sm-4 col-md-4 text-center" style="background-color: #E3F2FD;">'+
						          '<span>'+v.pesanan_selesai+'</span>'+
						        '</div>'+
						        '<div class="col-4 col-sm-4 col-md-4 text-center" style="background-color: #e8f5e9;">'+
						          '<span>'+v.pesanan_selesai_tepat+'</span>'+
						        '</div>'+
				      		'</div>'+
				      	'</div>'+
				      	'<div class="col-2 col-sm-2 col-md-2 text-right" style="padding-top: .75rem;">'+
				      		'<span>'+
				      		kpi.toFixed(2)+'%'+
				      		'</span>'+
				      	'</div>'+
				    '</div>';

				    $('.kpi-div').append(html);
	        	})
	        }
	    });
	}

	function tambahNol(x){
	 y=(x>9)?x:'0'+x;
	 return y;
	}

	function bulanInd(x){
		var x = '2020-'+tambahNol(x)+'-01';
		var dt = new Date(x);
		var monthNames = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

		var y = monthNames[dt.getMonth()];

		return y;
	}

	function tahunInd(x){
		var x = x+'-01-01';
		var dt = new Date(x);    
		var y = dt.getFullYear();

		return y;
	}
</script>