
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/web/plugins/evo-calendar/css/evo-calendar.css">

<link href="<?php echo base_url()?>assets/web/plugins/dropzone/dropzone.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/web/plugins/dropzone/dropzone.js"></script>
<style>
	.event-container > .event-icon{
	    margin: 25px 15px 0 10px;
	}
	.event-container::before{
		width: 1.5px;
		left: 2px;
		top: -5px;
		z-index: 0;
	    background-color: #757575;
	}
	.event-container:hover{
	    background-color: transparent;
	}
	
	@media screen and (max-width: 1280px) {
		.event-container::before{
			left: -17.5px;
			top: 10px;
		}
		.event-container > .event-icon {
		    margin: 25px 10px 0 0px;
		}
		.event-container > .event-info > p.event-title > span{
			right: -30px;
		}
	}
</style>
<div class="row mb-4">
	<div class="col-sm-12 col-md-12 title-page">
        <h1>Detail Order</h1>
    </div>
</div>    

<?php if (isset($srvok)) { ?>
	<div class="flashdata">
	  <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissible" role="alert">
	      <?php echo $srvmsg ?>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	      </button>
	  </div>
	</div>
	<?php } ?>
	
<div class="row">
    <div class="col-sm-12 col-md-12 mb-4">
    	<h4>Konsumen</h4>
    	<div class="row">
    		<div class="col-4 col-sm-2 col-md-2">Kode Order</div>
    		<div class="col-8 col-sm-10 col-md-10">
    			<?= $detail['kode_order'] ?>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-4 col-sm-2 col-md-2">Nama</div>
    		<div class="col-8 col-sm-10 col-md-10">
    			<?= $detail['nama_konsumen'] ?>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-4 col-sm-2 col-md-2">Alamat</div>
    		<div class="col-8 col-sm-10 col-md-10">
    			<?= $detail['alamat_konsumen'] ?>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-4 col-sm-2 col-md-2">No HP</div>
    		<div class="col-8 col-sm-10 col-md-10">
    			<?= $detail['no_hp_konsumen'] ?>
    		</div>
    	</div>

      <!-- tulisan -->
      <div class="row" id="text-desainer-desain">
        <div class="col-4 col-sm-2 col-md-2">PIC Desainer Desain</div>
        <div class="col-8 col-sm-10 col-md-10">
          <p id="content-desainer"><?= ($detail['nama_desainer_design']) ? $detail['nama_desainer_design'] : '-' ?></p>
        </div>
      </div>
      <div class="row" id="text-desainer-eksekusi">
        <div class="col-4 col-sm-2 col-md-2">PIC Desainer Eksekusi</div>
        <div class="col-8 col-sm-10 col-md-10">
          <p id="content-desainer"><?= ($detail['nama_desainer_eksekusi']) ? $detail['nama_desainer_eksekusi'] : '-' ?></p>
          <?php if (in_array(SessionManagerWeb::getRole(), array(Role::MARKETING,Role::DIREKTUR))): ?>
          <button onclick="activeFormDesainer(1)" class="btn btn-sm btn-primary" id="ubah-desainer">Ubah</button>
          <?php endif ?>
        </div>
      </div>
      <!-- tulisan -->

      <!-- form -->

      <div class="d-none" id="form-desainer">
        <form method="POST" action="<?= site_url("web/$class/edit/".$detail['id'])  ?>" class="form-group">
        <div class="row">
          <div class="col-4 col-sm-2 col-md-2">PIC Desainer Desain</div>
          <div class="col-8 col-sm-10 col-md-10">
            <?php if (in_array(SessionManagerWeb::getRole(), array(Role::MARKETING,Role::DIREKTUR))): ?>
              <select class="form-control" name="desainer_design">
                <option>--Pilih--</option>
                <?php foreach ($desainer as $key => $value): ?>
                <option value="<?= $value['id'] ?>" <?= ($value['id'] == $detail['desainer_design']) ? "selected" : "" ?>><?= $value['name'] ?></option>
                <?php endforeach ?>
              </select>
            <?php endif ?>
          </div>
        </div>
        <div class="row">
          <div class="col-4 col-sm-2 col-md-2">PIC Desainer Eksekusi</div>
          <div class="col-8 col-sm-10 col-md-10">
            <?php if (in_array(SessionManagerWeb::getRole(), array(Role::MARKETING,Role::DIREKTUR))): ?>
              <select class="form-control" name="desainer_eksekusi">
                <option>--Pilih--</option>
                <?php foreach ($desainer as $key => $value): ?>
                <option value="<?= $value['id'] ?>" <?= ($value['id'] == $detail['desainer_eksekusi']) ? "selected" : "" ?>><?= $value['name'] ?></option>
                <?php endforeach ?>
              </select>
            <?php endif ?>
            <button onclick="activeFormDesainer(0)" type="button" class="btn btn-sm btn-danger d-none" id="batal-desainer">Batal</button>
            <button class="btn btn-sm btn-success" type="submit"> Simpan</button>
          </div>
        </div>
        </form>
      </div>
      <div class="row">
        <div class="col-4 col-sm-2 col-md-2">Catatan</div>
        <div class="col-8 col-sm-10 col-md-10">
          <p id="content-catatan"><?= $detail['catatan'] ?></p>
          <?php if (in_array(SessionManagerWeb::getRole(), array(Role::MARKETING,Role::DIREKTUR))): ?>
          <button onclick="activeFormCatatan(1)" class="btn btn-sm btn-primary" id="ubah-catatan">Ubah</button>
          
          <div class="d-none" id="form-catatan">
            <form method="POST" action="<?= site_url("web/$class/edit/".$detail['id'])  ?>" class="form-group">
            <textarea class="form-control" id="catatan_change" name="catatan"><?= $detail['catatan'] ?></textarea>
            <button onclick="activeFormCatatan(0)" type="button" class="btn btn-sm btn-danger d-none" id="batal-catatan">Batal</button>
            <button class="btn btn-sm btn-success" type="submit"> Simpan</button>
          </form>
          </div>
          
          <?php endif ?>
          
        </div>
      </div>


    	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR): ?>
    	<div class="row">
    		<div class="col-12 col-sm-12 col-md-12 text-center mt-4">    			
    		<button type="button" class="btn btn-sm btn btn-outline-secondary btn-pilih-konsumen pl-3 pr-3">Ubah</button>
    		</div>
    	</div>
    	<?php endif ?>
    	
    </div>
    
    <div class="col-sm-12 col-md-12 mb-4">
    	<button type="button" class="btn btn-sm btn-outline-secondary float-right btn-pilih-gambar"><i class="fas fa-pencil-alt"></i></button>
    	<h4>Gambar Kerja</h4>
    	<hr>
    	<div class="row">
    		<?php if ($detail['foto_order']){ ?>
	    		<?php foreach($detail['foto_order'] as $key) { ?>
	    			<div class="col-6 col-sm-6 col-md-4 mb-2">
	    				<img src="<?= $key;?>" width="100%" onclick="expandImg('<?= $key;?>');" style="cursor: pointer;">
	    				<center><button class="btn btn-secondary" onclick='hapusFile(<?= json_encode(explode('/', $key)) ?>)'><i class="fa fa-times"></i> Hapus</button></center>
	    			</div>
		    	<?php } ?>
		    <?php } else { ?>
		    	<div class="col-6 col-sm-6 col-md-4 mb-2">
    				<p>Belum ada foto</p>
    				
    			</div>
		    <?php } ?>
    	</div>
	</div>
	<?php if ($detail['flow_id']==8): ?>
	<div class="col-sm-12 col-md-12 mb-4">
    	<h4>Gambar Acc</h4>
    	<hr>
    	<div class="row">
    		<?php if ($detail['foto_acc']){ ?>
    			<?php foreach($detail['foto_acc'] as $key) { ?>
	    			<div class="col-6 col-sm-6 col-md-4 mb-2" >
	    				<img src="<?= $key;?>" width="100%" onclick="expandImg('<?= $key;?>');" style="cursor: pointer;">
	    				
	    			</div>
		    	<?php } ?>
    		<?php } else {?>
    			<div class="col-6 col-sm-6 col-md-4 mb-2">
	    				<p>Belum ada foto</p>
	    				
	    			</div>
    		<?php } ?>
    		
    	</div>
	</div>

	<div class="col-sm-12 col-md-12 mb-4">
    	<h4>Foto After</h4>
    	<hr>
    	<div class="row">
    		<?php if ($detail['foto_fa']){ ?>
	    		<?php foreach($detail['foto_fa'] as $key) { ?>
	    			<div class="col-6 col-sm-6 col-md-4 mb-2">
	    				<img src="<?= $key;?>" width="100%" onclick="expandImg('<?= $key;?>');" style="cursor: pointer;">
	    				<center><button class="btn btn-secondary" onclick='hapusFile(<?= json_encode(explode('/', $key)) ?>,"FA")'><i class="fa fa-times"></i> Hapus</button></center>
	    			</div>
		    	<?php } ?>
		    <?php } else { ?>
		    	<div class="col-6 col-sm-6 col-md-4 mb-2">
    				<p>Belum ada foto</p>
    				
    			</div>
		    <?php } ?>
    	</div>
	</div>

	<div class="col-sm-12 col-md-12 mb-4">
    	<h4>Edit Katalog</h4>
    	<div class="row">
    		<?php if ($detail['foto_ek']){ ?>
	    		<?php foreach($detail['foto_ek'] as $key) { ?>
	    			<div class="col-6 col-sm-6 col-md-4 mb-2">
	    				<img src="<?= $key;?>" width="100%" onclick="expandImg('<?= $key;?>');" style="cursor: pointer;">
	    				<center><button class="btn btn-secondary" onclick='hapusFile(<?= json_encode(explode('/', $key)) ?>,"EK")'><i class="fa fa-times"></i> Hapus</button></center>
	    			</div>
		    	<?php } ?>
		    <?php } else { ?>
		    	<div class="col-6 col-sm-6 col-md-4 mb-2">
    				<p>Belum ada foto</p>
    				
    			</div>
		    <?php } ?>
		    <div class="col-12 col-sm-12 col-md-12 mb-2">
				<p>Link Ref <a href="$detail['link_sosmed']"><?= $detail['link_sosmed'] ?></a></p>
				
			</div>
    	</div>
	</div>

	<div class="col-md-12 text-center mt-2">
		<button type="button" class="btn btn-sm btn-add btn-pilih-after">UPLOAD GAMBAR AFTER</button>
	</div>
	<?php endif ?>

	
	

	<div class="col-sm-12 col-md-12 mb-4">
    	<h4>Status Kerja</h4>
    	<div class="row">
  			<div class="col-md-12">
  				<div class="status-kerja-div">
  					<label><?= $detail['flow_name']?></label>
  				</div>
  			</div>
    	</div>

    	<?php if ($detail['flow_id']==2): ?>
    	<div class="row">
    			<div class="col-md-12 text-white text-center">
    				<?php if($detail['status_produksi'] == 1) {
    	                echo '<div style="background-color: #4caf50;">Bisa Cek Ruangan</div>';
    	            }elseif($detail['status_produksi'] == 2) {
    	                echo '<div style="background-color: #4caf50;">Bisa Pasang Rangka</div>';
    	            }elseif($detail['status_produksi'] == 3) {
    	                echo '<div style="background-color: #f71212;">Menunggu Cor Siap</div>';
    	            } ?>
    			</div>
      </div>

    	<?php endif ?>
    	

    	<div class="row">
    		<?php if (SessionManagerWeb::getRole() != Role::PRODUKSI && SessionManagerWeb::getRole() != Role::PEMASANG){ ?>
    			<?php if ($detail['flow_id'] == 2 && $detail['jadwal_usulan']==null): ?>
    			<div class="col-md-12 text-center mt-2">
	       			<button type="button" class="btn btn-sm btn-add btn-ubah-status-produksi update-status" idorder="<?= $detail['id'] ?>" kode="<?= $detail['kode_order'] ?>" status="<?= $detail['status_produksi'] ?>">Ubah Status Produksi</button>
				</div>
    			<?php endif ?>
          <?php if ($detail['jadwal_usulan']!=null): ?>
          <div class="col-md-12 text-center mt-2">
                <button type="button" class="btn btn-sm btn-add btn-ubah-status-produksi" onclick='ordermenunggu(<?= $detail['id'] ?>, <?= json_encode($detail) ?>)'> Konfirmasi Jadwal</button>
          </div>
          <?php endif ?>
    			
    		<?php } else { ?>
    		<?php if ($detail['status_produksi']!=3): ?>
    			<?php if (($detail['flow_id']==2 || $detail['flow_id']==5) && $detail['jadwal_usulan']==null){ ?>
    			<div class="col-md-12 text-center mt-2">
	       			<button type="button" class="btn btn-sm btn-add btn-ubah-status-produksi usulkan-jadwal" idorder="<?= $detail['id'] ?>" onclick="usuljadwal(<?= $detail['id']  ?>)" kode="<?= $detail['kode_order'] ?>" status="<?= $detail['status_produksi'] ?>">Usulkan Jadwal Pasang <?= ($detail['flow_id']==2) ? 'Kusen' : 'Unit'  ?></button>
				</div>
    			<?php } 

          elseif(($detail['flow_id']==2 || $detail['flow_id']==5) && $detail['jadwal_usulan']!=null){
            ?>
            <div class="col-md-12 text-center mt-2">
                <button type="button" class="btn btn-sm btn-add btn-ubah-status-produksi" onclick='ordermenunggu(<?= $detail['id'] ?>, <?= json_encode($detail) ?>)'> Konfirmasi Jadwal</button>
          </div>
            <?php
          }

          elseif($detail['flow_id']==4 || $detail['flow_id']==7){
    				?>
    			<div class="col-md-12 text-center mt-2">
    				<button type="button" class="btn btn-sm btn-add btn-ubah-status-produksi usulkan-jadwal" idorder="<?= $detail['id'] ?>" onclick="usuljadwal(<?= $detail['id']  ?>)" kode="<?= $detail['kode_order'] ?>" status="<?= $detail['status_produksi'] ?>">Set Selesai</button>
    			</div>
    			<?php
    			}?>
    			
    			
    		<?php endif ?>
    		<?php } ?>
			
		</div>
	</div>

	<div class="col-sm-12 col-md-12 mb-4">
    	<h4>Detail Order</h4>
    	<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="event-list">
					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-selesai"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Order Masuk <span><?= xFormatDateInd($detail['tgl_order_masuk']) ?></span></p>
							<p class="event-desc"><?= $detail['nama_user']?></p>
						</div>
					</div>					

					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-holiday"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Cek Ruangan / Pasang Kusen <span><?= xFormatDateInd($detail['tanggal_pasang_kusen']) ?></span></p>
							<p class="event-desc"><?= $detail['nama_pemasang_kusen']?></p>
						</div>
					</div>					

					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-pemasangan"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Pemasangan Unit <span><?= xFormatDateInd($detail['tanggal_pasang_finish']) ?></span></p>
							<p class="event-desc"><?= $detail['nama_pemasang_finish']?></p>
						</div>
					</div>					

					<div class="event-container" role="button">
						<div class="event-icon">
							<div class="event-bullet-birthday"></div>
						</div>
						<div class="event-info">
							<p class="event-title">Menunggu Set Pemasangan Selesai <span><?= xFormatDateInd($detail['tgl_acc_konsumen']) ?></span></p>
							<p class="event-desc"><?= $detail['nama_konsumen']?></p>
						</div>
					</div>					
				</div>
			</div>
    	</div>
	</div>

	<?php if (SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DESAIN): ?>
	<div class="col-md-12 text-center mt-2">
		<button type="button" class="btn btn-sm btn-add btn-hapus-pesanan">Hapus Pesanan</button>
	</div>
		
	<?php endif ?>
	
</div>

<!-- Pilih Konsumen Modal -->
<div class="modal fade" id="pilihKonsumenModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ubah</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url("web/$class/updateKonsumen/".$detail['id']) ?>" method="post">
      <div class="modal-body">
        <div class="form-group row">
        	<label class="col-4 col-sm-2 col-md-3">Kode Order</label>
        	<div class="col-8 col-sm-10 col-md-9">
        		<input type="text" name="kode_order" class="form-control form-control-sm" value="<?= $detail['kode_order'] ?>">
        	</div>
        </div>
        <div class="form-group row">
        	<label class="col-4 col-sm-2 col-md-3">Nama</label>
        	<div class="col-8 col-sm-10 col-md-9">
        		<input type="text" name="nama" class="form-control form-control-sm" value="<?= $detail['nama_konsumen'] ?>">
        	</div>
        </div>
        <div class="form-group row">
        	<label class="col-4 col-sm-2 col-md-3">Alamat</label>
        	<div class="col-8 col-sm-10 col-md-9">
        		<textarea name="alamat" class="form-control form-control-sm"><?= $detail['alamat_konsumen'] ?></textarea>
        	</div>
        </div>
        <div class="form-group row">
        	<label class="col-4 col-sm-2 col-md-3">No HP</label>
        	<div class="col-8 col-sm-10 col-md-9">
        		<input type="text" name="no_hp" class="form-control form-control-sm" value="<?= $detail['no_hp_konsumen'] ?>">
        	</div>
        </div>
      </div>
      <div class="modal-footer">
       		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>


<!-- Pilih Gambar Modal -->
<div class="modal fade" id="pilihGambarModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Gambar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="img-upload" action="<?= site_url('web/pesanan/saveEditGambar/'.$detail['id']) ?>" method="post">
      <div class="modal-body">

      	<div class="form-group">
		  	<div class="dropzone dz-upload-1">
				<div class="dz-message">
				  	<label for="file-1">
				  		<i class="fa fa-cloud-upload" aria-hidden="true"></i>
				  		<h5>Upload <span>File</span></h5>
				  	</label>
					  <p> Klik atau Drag file kesini</p>
				</div>
		 	</div>

		</div>
        
      </div>
      <div class="modal-footer">
       		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Pilih Gambar Modal -->
<div class="modal fade" id="pilihGambarAfterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Gambar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="img-upload" action="<?= site_url('web/pesanan/saveEditGambar/'.$detail['id']) ?>" method="post">
      <div class="modal-body">

      	<div class="form-group">
		  	<div class="dropzone dz-upload-1">
				<div class="dz-message">
				  	<label for="file-1">
				  		<i class="fa fa-cloud-upload" aria-hidden="true"></i>
				  		<h5>Upload <span>File</span></h5>
				  	</label>
					  <p> Klik atau Drag file kesini</p>
				</div>
		 	</div>

		</div>
        
      </div>
      <div class="modal-footer">
       		<button type="submit" class="btn btn-sm btn-add"><i class="fas fa-save"></i> Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Status Modal -->
<div class="modal fade" id="statusModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="statusProduksiModal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row list-status">
          
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Expand Image Modal-->
<div class="modal fade" id="expandImagenModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
      	<div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	     </div>
         <div class="modal-body">
           <div class="img-div">
           	<img src="" style="width: 100%;">
           </div>
         </div>
      </div>
   </div>
</div>


<!-- Order Menunggu Modal -->
<div class="modal fade" id="orderMenungguModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Usulan Jadwal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-4 col-sm-4 col-md-4">Tanggal</div>
          <div class="col-8 col-sm-8 col-md-8" id="jadwal_pengajuan"></div>

          <div class="col-4 col-sm-4 col-md-4">Rencana Kerja</div>
          <div class="col-8 col-sm-8 col-md-8" id="rencana_kerja_nama"></div>

          <div class="col-4 col-sm-4 col-md-4">Tim</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_pemasang"></div>

          <div class="col-12 col-sm-12 col-md-12">
            <label class="mb-0 mt-2">Data Order</label>
          </div>

          <div class="col-4 col-sm-4 col-md-4">Kode Order</div>
          <div class="col-8 col-sm-8 col-md-8" id="kode_order"></div>

          <div class="col-4 col-sm-4 col-md-4">Tgl. Order Masuk</div>
          <div class="col-8 col-sm-8 col-md-8" id="tgl_order_masuk"></div>

          <div class="col-4 col-sm-4 col-md-4">Nama</div>
          <div class="col-8 col-sm-8 col-md-8" id="nama_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">Alamat</div>
          <div class="col-8 col-sm-8 col-md-8" id="alamat_konsumen"></div>

          <div class="col-4 col-sm-4 col-md-4">No HP</div>
          <div class="col-8 col-sm-8 col-md-8" id="no_hp_konsumen"></div>
        </div>
      </div>
      <div class="modal-footer" style="border-top: none;">
          <?php if (SessionManagerWeb::getRole() != Role::PRODUKSI): ?>
            <input type="text" class="form-control" name="link" id="link" value="" readonly="">
          <button  onclick="copyLink()" class="btn btn-sm btn-secondary" id="terimausulkanpemasangan"><i class="fas fa-link"></i> Copy Link</button>
          <?php endif ?>
          
          <a href="#" class="btn btn-sm btn-add" id="tolakusulkanpemasangan"><i class="fas fa-times"></i> Tolak</a>

          <button  onclick="terimaUsulan()" class="btn btn-sm btn-success" id="terimausulkanpemasangan"><i class="fas fa-check"></i> Setuju</button>
      </div>
    </div>
  </div>
</div>


<form id="formApprove" action="#" method="post">
  <input type="hidden" name="back" value="pesanan/detail">
  <input type="hidden" name="confirm" value="1">
</form>


<form id="formSubmit" action="<?php echo site_url('web/pesanan/updateStatusProduksi')?>" method="post">
  <input type="hidden" name="id" id="idorder">
  <input type="hidden" name="status_produksi" id="status_produksi">
  <input type="hidden" name="direct" value="detail">
</form>

<script>
	function expandImg(path){
		$('#expandImagenModal .modal-body img').attr('src', path);
		$('#expandImagenModal').modal('show');
	}

	function hapusFile(data,tipe='OM'){
		// console.log(data);
		var r = confirm("Apakah anda akan menghapus gambar ini secara permanen?");
		if (r == true) {
			var length = data.length;
		    var name = data[length-1].split('-');
		    if(name.length > 3){
		    	var nama_file = name[2];
		    	for (var i = 3; i < name.length; i++) {
		    		nama_file += '-'+name[i];
		    		
		    	}
		    }else{
		    	var nama_file = name[2];
		    }
			var pesanan_id = '<?= $detail['id'] ?>';
			var id_hash = name[1];
			// alert(nama_file);
			$.ajax({
				type: "post",
				url: "<?= site_url('web/pesanan') ?>/removeImagePesanan",
				data: {pesanan_id: pesanan_id, nama_file : nama_file, id_hash:id_hash,tipe:tipe},
				dataType: "html",
				success: function(response){
			       alert(response);
			       window.location= "<?= site_url('web/pesanan/detail')  ?>/"+pesanan_id;
			        	
			    },
			});
		} 
		
	}
	function usuljadwal(id){
		window.location= "<?= site_url('web/pesanan/usulkanpemasangan')  ?>/"+id;
	}
	Dropzone.autoDiscover = false;

	$(document).ready(function(){
		$(document).on('click', '.btn-pilih-konsumen', function(){
			$('#pilihKonsumenModal').modal('show');
		})

		$(document).on('click', '.btn-pilih-gambar', function(){
			$('#pilihGambarModal').modal('show');
		})

		$(document).on('click', '.btn-pilih-after', function(){
			window.location.href = "<?= site_url('web/pesanan/uploadafter/'.$detail['id'])?>";
		})

		$(document).on('click', '.btn-hapus-pesanan', function(){
			var r = confirm("Apakah yakin akan menghapus pesanan ini?");
			if (r == true) {
				window.location.href = "<?= site_url('web/pesanan/deletepesanan/'.$detail['id'])?>";
			}
		})

		$(document).on('click','.update-status',function(){
	      var idorder = $(this).attr('idorder');
	      var kode = $(this).attr('kode');
	      var status = $(this).attr('status');
	      $('#statusProduksiModal').html('Status Produksi - '+kode);

	      if (status == 1) {            
	      $('.list-status').append('<div class="col-12 col-md-12 col-md-12">'+
	                                  '<h6 style="color: #aaa;">Bisa Cek Ruangan</h6>'+
	                                  '<span class="text-red" style="font-size: 12px;">Status saat ini</span>'+
	                              '</div>');
	      }else{
	      $('.list-status').append('<div class="col-12 col-md-12 col-md-12" onclick="changestatusproduksi(1)" style="cursor: pointer;">'+
	                                  '<h6>Bisa Cek Ruangan</h6>'+
	                              '</div>');
	      }

	      if (status == 2) {
	      $('.list-status').append('<div class="col-12 col-md-12 col-md-12">'+
	                                  '<h6 style="color: #aaa;">Bisa Pasang Rangka</h6>'+
	                                  '<span class="text-red" style="font-size: 12px;">Status saat ini</span>'+
	                              '</div>');
	      }else{
	      $('.list-status').append('<div class="col-12 col-md-12 col-md-12" onclick="changestatusproduksi(2)" style="cursor: pointer;">'+
	                                  '<h6>Bisa Pasang Rangka</h6>'+
	                              '</div>');
	      }

	      if (status == 3) {            
	      $('.list-status').append('<div class="col-12 col-md-12 col-md-12">'+
	                                '<h6 style="color: #aaa;">Menunggu Cor Siap</h6>'+
	                                '<span class="text-red" style="font-size: 12px;">Status saat ini</span>'+
	                              '</div>');
	      }else{
	      $('.list-status').append('<div class="col-12 col-md-12 col-md-12" onclick="changestatusproduksi(3)" style="cursor: pointer;">'+
	                                '<h6>Menunggu Cor Siap</h6>'+
	                              '</div>');
	      }

	      $('#idorder').val(idorder);
	      $('#statusModal').modal('show');

	      // window.location.href = "<?= site_url('web/pesanan/detail')?>/"+id;
	    })

		if ($('.dropzone').length) {
			var myDropzone= new Dropzone(".dz-upload-1",{
				url: "<?= site_url('web/pesanan') ?>/sendimage",
				addRemoveLinks:true,
				maxFilesize: 5, // MB
				dictRemoveFile : "remove",
				removedfile: function(file){
					var name = file.name;

					$.ajax({
						type: "post",
						url: "<?= site_url('web/pesanan') ?>/removetemp",
						data: {file: name},
						dataType: "html"
					});

					//remove thumbnail
					var previewElement;
					return (previewElement = file.previewElement) != null ? (previewElement.parentNode.removeChild(file.previewElement)) : (void 0);
				},
			});
		}
	})

	function ubahstatusproduksi(id, status){
	    $('#idorder').val(id);

	    $('.update-status').attr('kode',kode);
	    $('.update-status').attr('status',status);
	    $('.detail-order').attr('id',id);
	    $('#kodeOrderModal').html(kode);
	    $('#detailModal').modal('show');
	  }

	  function changestatusproduksi(status){
	    $('#status_produksi').val(status);
	    $('#formSubmit').submit();
	  }


    <?php if(SessionManagerWeb::getRole() != Role::PRODUKSI ){ ?> //untuk non produksi
    function ordermenunggu(id,data){
      $('#idorder').val(id);
      $('#tolakusulkanpemasangan').attr('href','<?php echo site_url('web/pesanan/tolakusulkanpemasangan')?>/'+id);
      $('#link').val('<?php echo site_url('web/site/approve_penjadwalan_pesanan')?>/'+data.konsumen_id);
      $('#orderMenungguModal').modal('show');
      $('.btn-pilih-mitrapemasang').html('Ubah');
      $('#jadwal_pengajuan').html(data.jadwal_usulan);
      $('#rencana_kerja_nama').html(data.rencana_kerja_nama);
      if(data.rencana_kerja_id== 1 && data.nama_pemasang_kusen!=''){
        var nama = data.nama_pemasang_kusen;
                  
      }
      else if(data.rencana_kerja_id > 1 && data.nama_pemasang_finish!=''){
        var nama = data.nama_pemasang_finish;
      }else{
        var nama = '-';
      }
      $('#nama_pemasang').html(nama);
      $('#kode_order').html(data.kode_order);
      $('#tgl_order_masuk').html(data.tgl_order_masuk);
      $('#nama_konsumen').html(data.nama_konsumen);
      $('#alamat_konsumen').html(data.alamat_konsumen);
      $('#no_hp_konsumen').html(data.no_hp_konsumen);

    }
  <?php }else{ ?> //untuk produksi
    function ordermenunggu(id,data){
      console.log(data);
      // $('#link').val('<?php echo site_url('web/site/approve_penjadwalan_pesanan')?>/'+data.konsumen_id);
      $('#idorder').val(id);
      if(data.jadwal_usulan==null){
        window.location.href='<?= site_url('web/pesanan/usulkanpemasangan/') ?>/'+id;
      }else{
        if(data.role_pengusul==''){
          window.location.href = "<?php echo site_url('web/pesanan/usulkanpemasangan')?>/"+id;
        }else{
          $('#tolakusulkanpemasangan').attr('href','<?php echo site_url('web/pesanan/tolakusulkanpemasangan')?>/'+id);
          $('#orderMenungguModal').modal('show');
          $('.btn-pilih-mitrapemasang').html('Ubah');
          $('#jadwal_pengajuan').html(data.jadwal_usulan);
          $('#rencana_kerja_nama').html(data.rencana_kerja_nama);
          if(data.rencana_kerja_id== 1 && data.nama_pemasang_kusen!=''){
            var nama = data.nama_pemasang_kusen;
                      
          }
          else if(data.rencana_kerja_id > 2 && data.nama_pemasang_finish!=''){
            var nama = data.nama_pemasang_finish;
          }else{
            var nama = '-';
          }
          $('#nama_pemasang').html(nama);
          $('#kode_order').html(data.kode_order);
          $('#tgl_order_masuk').html(data.tgl_order_masuk);
          $('#nama_konsumen').html(data.nama_konsumen);
          $('#alamat_konsumen').html(data.alamat_konsumen);
          $('#no_hp_konsumen').html(data.no_hp_konsumen);
        }
      }
      
      
    }
  <?php } ?>

  function terimaUsulan(){
    //console.log($('#idorder').val());
    var id = <?= $detail['id'] ?>;
    $('#formApprove').attr('action','<?php echo site_url('web/pesanan/usulan')?>/'+id);
    // console.log($('#formApprove').attr('action'));
    $('#formApprove').submit();
  }

  function activeFormCatatan(type){
    if(type == 1){
      $('#form-catatan').removeClass('d-none');
      $('#batal-catatan').removeClass('d-none');
      $('#ubah-catatan').addClass('d-none');
      $('#content-catatan').addClass('d-none');

    }
    else{
      $('#form-catatan').addClass('d-none');
      $('#batal-catatan').addClass('d-none');
      $('#ubah-catatan').removeClass('d-none');
      $('#content-catatan').removeClass('d-none');
    }
    
  }

  function activeFormDesainer(type){
    if(type == 1){
      $('#form-desainer').removeClass('d-none');
      $('#batal-desainer').removeClass('d-none');
      $('#ubah-desainer').addClass('d-none');
      $('#text-desainer-desain').addClass('d-none');
      $('#text-desainer-eksekusi').addClass('d-none');

    }
    else{
      $('#form-desainer').addClass('d-none');
      $('#batal-desainer').addClass('d-none');
      $('#ubah-desainer').removeClass('d-none');
      $('#text-desainer-desain').removeClass('d-none');
      $('#text-desainer-eksekusi').removeClass('d-none');
    }
    
  }
</script>