<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Dompdf\Dompdf;

class Fee_desainer extends WEB_Controller {

    function __construct() {
        parent::__construct();
        // $this->load->model('Pesanan_model');
        if(!in_array(SessionManagerWeb::getRole(), array(Role::ADMINISTRATOR, Role::DIREKTUR, Role::MARKETING))){
            SessionManagerWeb::setFlashMsg(false, "Anda tidak bisa mengakses fungsi ini");
            redirect('web/pesanan');
        }
    }

    public function index($keepmonth=0) {
        $this->load->model('Pesanan_model');
        $this->load->model('User_model');
        $data = $this->postData;
        $tampilan = 'o';
        if($_GET['filter']){
            $tampilan = $_GET['filter'];
        }
        
        // echo $filter;die();
        if ($this->postData['tahun'] && $this->postData['bulan']) {

            $_SESSION['order_desainer']['tahun'] = $tahun = $this->postData['tahun'];
            $_SESSION['order_desainer']['bulan'] = $bulan = $this->postData['bulan'];
            $tampilan = $_SESSION['order_desainer']['tampilan'];
        }
        elseif($keepmonth==0){

            if($data['status'] || $data['keyword']){
                $tahun = $_SESSION['order_desainer']['tahun'];
                $bulan = $_SESSION['order_desainer']['bulan'];
                $tampilan = $_SESSION['order_desainer']['tampilan'];
            }else{
                $_SESSION['order_desainer']['tahun'] = $tahun = date('Y');
                $_SESSION['order_desainer']['bulan'] = $bulan = date('m');
                $_SESSION['order_desainer']['tampilan'] = $tampilan;
            }
            
        }else{
            $tahun = $_SESSION['order_desainer']['tahun'];
            $bulan = $_SESSION['order_desainer']['bulan'];
            $tampilan = $_SESSION['order_desainer']['tampilan'];
        }

        if($data['status'] || $data['keyword']){
            $this->data['filter_status'] = $data['status'];
            $this->data['filter_keyword'] = $data['keyword'];
        }

        if($tampilan == 'o'){
            $this->data['data'] = $this->Pesanan_model->filter($filter)->getFeeFromPesanan($bulan, $tahun);
        }else{
            $this->data['data'] = $this->User_model->column("*")->filter("where is_active=1 and role!='".Role::ADMINISTRATOR."' ")->getUser();
        }

        $this->data['tampilan'] = $tampilan;
        
        // echo '<pre>';print_r($this->data);die();
        // $this->data['data'] = $this->Gaji_desainer_model->filter("where ")
        
        // echo "<pre>";print_r($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }

    public function slipgajiadd($id) {
        $this->load->model('User_model');
        $this->load->model('Pesanan_model');

        $this->data['idorder'] = $id;
        $pesanan = $this->Pesanan_model->getDetailPesanan($id);
        $this->data['data'] = $this->User_model->get($pesanan['desainer']);

        // var_dump($this->data['data']);die();
        $this->data['user'] = $this->User_model->getAll($filter);

        $this->template->viewDefault($this->view,$this->data);

    }

    public function feeDesain($pesanan_id, $desainer_id){
        $this->load->model('Pesanan_model');
        $this->data['data'] = $this->model->getFeeRow($pesanan_id,$desainer_id);
        $this->data['pesanan'] = $this->Pesanan_model->getDetailPesanan($pesanan_id);
        if($this->data['data']){
            $this->data['fee_desainer_id'] = $this->data['data']['id'];
            $this->data['desainer_id'] = $desainer_id;
            $this->data['srvok'] = true;
            $this->data['srvmsg'] = 'Fee Desainer ditemukan, edit untuk mengubah';
            // SessionManagerWeb::setFlashMsg(true, );
        }else{
            $this->data['data']['fee_design'] = 50000;
            if ($this->data['pesanan']['harga_deal']>=7500000 && $this->data['pesanan']['harga_deal']<10000000) {
                $this->data['data']['fee_design'] = 75000;
            }elseif($this->data['pesanan']['harga_deal']>=10000000){
                $this->data['data']['fee_design'] = 100000;
            }
            $this->data['desainer_id'] = $desainer_id;
            $this->data['pesanan_id'] = $pesanan_id;
            $this->data['srvok'] = false;
            $this->data['srvmsg'] = 'Fee Desainer belum ada, silahkan tambahkan fee';
            // SessionManagerWeb::setFlashMsg(false, 'Fee Desainer belum ada, silahkan tambahkan fee'); 
        }

        $this->data['without_desainer_id'] = true;
        if($desainer_id){
            $this->data['without_desainer_id'] = false;
        }
        // echo "<pre>";print_r($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }

    public function create($pesanan_id) {
        $data = $this->postData;
        if($this->input->post('desainer_design_id')){
            $data['desainer_design_id'] = $this->input->post('desainer_design_id');
            $data['fee_design'] = $this->input->post('fee_design');
        }
        if($this->input->post('desainer_eksekusi_id')){
            $data['desainer_eksekusi_id'] = $this->input->post('desainer_eksekusi_id');
            $data['fee_eksekusi'] = $this->input->post('fee_eksekusi');
            $data['persentase_eksekusi'] = $this->input->post('persentase_eksekusi');
        
        }
        
        

        $data['pesanan_id'] = $pesanan_id;

        if($this->input->post('desainer_id')){
            $desainer_id = $this->input->post('desainer_id');
            unset($data['desainer_id']);
        }
        // echo $desainer_id;die();
        // echo $desainer_id."<br>";
        // echo "<pre>";print_r($data);die();
        $check = $this->model->getFeeRow($pesanan_id);
        if($check){
            unset($data['pesanan_id']);
            $insert = $this->model->edit($data, $check['id']);
        }else{
            $insert = $this->model->create($data);
        }
    
        if ($insert) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat fee untuk desainer');
        } else {
            SessionManagerWeb::setFlashMsg(false, 'Gagal membuat fee untuk desainer'); 
        }

        if($desainer_id){
            redirect("web/$this->class/detail/$desainer_id");
        }else{
            redirect("web/$this->class/index/1");
        }
        
    }

    public function gajiDesain($desainer_id, $bulan, $tahun){
        $filter = " and bulan='".$bulan."' and tahun='".$tahun."'";
        $this->load->model('Gaji_desainer_model');
        $this->data['data'] = $this->Gaji_desainer_model->column('*')->filter($filter)->getGajiRow($desainer_id);

        $this->load->model('User_model');
        $this->data['user'] = $this->User_model->getUserById($desainer_id);

        $this->data['bulan'] = $bulan;
        $this->data['tahun'] = $tahun;
        if($this->data['data']){
            $this->data['gaji_desainer_id'] = $this->data['data']['id'];
            $this->data['desainer_id'] = $desainer_id;
            $this->data['srvok'] = true;
            $this->data['srvmsg'] = 'Gaji Desainer ditemukan, edit untuk mengubah';
        }else{
            $this->data['desainer_id'] = $desainer_id;
            $this->data['srvok'] = false;
            $this->data['srvmsg'] = 'Gaji Desainer belum ada, silahkan tambahkan gaji';
        }
        $this->template->viewDefault($this->view,$this->data);
    }

    public function gajidesainadd($bulan, $tahun){
        $filter = " and bulan='".$bulan."' and tahun='".$tahun."'";
        $this->load->model('Gaji_desainer_model');
        $this->data['data'] = $this->Gaji_desainer_model->column('*')->filter($filter)->getGajiRow($desainer_id);

        $filter = " where role!='".Role::ADMINISTRATOR."' and is_active=1";
        $this->load->model('User_model');
        $this->data['user'] = $this->User_model->column('*')->filter($filter)->getUser();

// var_dump($this->data['user']);die();
        // $this->data['bulan'] = $bulan;
        // $this->data['tahun'] = $tahun;
        // if($this->data['data']){
        //     $this->data['gaji_desainer_id'] = $this->data['data']['id'];
        //     $this->data['desainer_id'] = $desainer_id;
        //     $this->data['srvok'] = true;
        //     $this->data['srvmsg'] = 'Gaji Desainer ditemukan, edit untuk mengubah';
        // }else{
        //     $this->data['desainer_id'] = $desainer_id;
        //     $this->data['srvok'] = false;
        //     $this->data['srvmsg'] = 'Gaji Desainer belum ada, silahkan tambahkan gaji';
        // }
        $this->template->viewDefault($this->view,$this->data);
    }

    public function createPokok($desainer_id){

        $this->load->model('Gaji_desainer_model');
        $data = $this->postData;
        $data['desainer_id'] = $desainer_id;
        $insert = $this->Gaji_desainer_model->create($data);
        
        if ($insert) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil menambahkan gaji');
        } else {
            SessionManagerWeb::setFlashMsg(false, 'gagal menambahkan gaji');
        }

        redirect("web/$this->class/detail/$desainer_id");
    }

    public function createPokokAdd(){

        $this->load->model('Gaji_desainer_model');
        $data = $this->postData;
        // var_dump($data);die();
        $insert = $this->Gaji_desainer_model->create($data);
        
        if ($insert) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil menambahkan gaji');
        } else {
            SessionManagerWeb::setFlashMsg(false, 'gagal menambahkan gaji');
        }

        $desainer_id = $data['desainer_id'];

        redirect("web/$this->class/detail/$desainer_id");
    }

    public function update($fee_desainer_id,$desainer_id) {
        $data = $this->postData;  
        // echo "<pre>";print_r($data);die();
        if($data['desainer_id']){
            $desainer_id = $data['desainer_id'];
            unset($data['desainer_id']);
        }
        $update = $this->model->edit($data, $fee_desainer_id);
        
        if ($update) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil update fee desainer');
        } else {
            SessionManagerWeb::setFlashMsg(false, 'Gagal update fee desainer');
        }

        if($desainer_id){
            redirect("web/$this->class/detail/$desainer_id");
        }else{
            redirect("web/$this->class/index/1");
        }
        
    }

    public function updatePokok($gaji_desainer_id, $desainer_id) {
        $this->load->model('Gaji_desainer_model');
        $data = $this->postData;

        
        $update = $this->Gaji_desainer_model->edit($data, $gaji_desainer_id);
        
        if ($update) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil update gaji desainer');
        } else {
            SessionManagerWeb::setFlashMsg(false, 'Gagal update gaji desainer');
        }
        redirect("web/$this->class/detail/$desainer_id");
    }

    public function detail($desainer_id){
        $this->load->model('Pesanan_model');
        // echo "<pre>";print_r($_SESSION['order_desainer']['tahun']);die();
        $this->data['data'] = $this->Pesanan_model->filter($filter)->getOrderDesainer($desainer_id,$_SESSION['order_desainer']['bulan'], $_SESSION['order_desainer']['tahun']);
        $this->data['bulan'] = $_SESSION['order_desainer']['bulan'];
        $this->data['tahun'] = $_SESSION['order_desainer']['tahun'];
        $this->data['desainer_id'] = $desainer_id;

        $this->load->model('User_model');
        $this->data['user'] = $this->User_model->column("*")->filter("where id=".$desainer_id)->getUserById();

        $this->load->model('Gaji_desainer_model');
        $filter = " and bulan='".$_SESSION['order_desainer']['bulan']."' and tahun='".$_SESSION['order_desainer']['tahun']."'";
        $gaji = $this->Gaji_desainer_model->column('*')->filter($filter)->getGajiRow($desainer_id);
        $this->data['gaji_pokok'] = $gaji['gaji_pokok'];
        $this->data['gaji_tunjangan'] = $gaji['gaji_tunjangan'];

        $this->data['cetak_gaji'] = $this->getGaji($desainer_id,$_SESSION['order_desainer']['bulan'], $_SESSION['order_desainer']['tahun']);
        // echo "<pre>";print_r($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }

    public function getGaji($desainer_id, $bulan, $tahun) {
        $this->load->model('Pesanan_model');
        $this->load->model('Gaji_desainer_model');

        $fee = $this->Pesanan_model->filter($filter)->getOrderDesainer($desainer_id,$bulan, $tahun);
        $data['fee_design'] = 0;
        $data['fee_eksekusi'] = 0;

        foreach ($fee as $key => $value) {
            if($value['desainer_design'] == $desainer_id){
                $data['fee_design'] += $value['fee_design'];
            }
            if($value['desainer_eksekusi'] == $desainer_id){
                $data['fee_eksekusi'] += $value['fee_eksekusi'];
            }
            
            
        }
        $filter = " and bulan='".$bulan."' and tahun='".$tahun."'";
        $gaji = $this->Gaji_desainer_model->column('*')->filter($filter)->getGajiRow($desainer_id);
        $data['gaji_pokok'] = $gaji['gaji_pokok'];
        $data['gaji_tunjangan'] = $gaji['gaji_tunjangan'];

        $data['total_gaji'] = $data['fee_design'] + $data['fee_eksekusi'] + $data['gaji_pokok'] + $data['gaji_tunjangan'];

        return $data;
        
        $this->data['data'] = $data;
        // echo "<pre>";print_r($this->data['data']);die();
        $this->template->viewDefault($this->view,$this->data);
        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

}
?>