<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Harga extends WEB_Controller {

    public function index() {
        $filter = "where 1 ";

        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }

        $data = $this->model->filter($filter)->getHarga($this->page);

        $result = array();
        foreach ($data as $element) {
            $result[$element['kategori']][] = $element;
        }
        $this->data['data'] = $result;

        // echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }

    public function add(){
        $this->load->model('Kategori_produk_model');

        $this->data['listkategori'] = $this->Kategori_produk_model->getKategori();
        $this->template->viewDefault('harga_add',$this->data);   
    }

    public function edit($id){        
        $this->load->model('Kategori_produk_model');

        $this->data['data'] = $this->model->getHargaById($id);
        $this->data['listkategori'] = $this->Kategori_produk_model->getKategori();

        $this->template->viewDefault('harga_add',$this->data);   
    }

    public function create() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        $insert = $this->model->create($data, SessionManagerWeb::getUserID());
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat harga');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal membuat harga');
        }
        redirect('web/'.$this->class);
    }
    
    public function update() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        $insert = $this->model->updateHarga($data);
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil update harga');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal update harga');
        }
        redirect('web/'.$this->class);
    }

    public function delete($id) {
        if (!in_array(SessionManagerWeb::getRole(), array(Role::DIREKTUR, Role::ADMINISTRATOR))) {
            SessionManagerWeb::setFlashMsg(false, 'Anda tidak memiliki akses');
        }
        $delete = $this->model->deleteHarga($id);
        
        if ($delete) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil hapus harga');
        } else {
            SessionManagerWeb::setFlashMsg(false, 'Gagal hapus harga');
        }
        redirect('web/'.$this->class);
    }
    
}

?>