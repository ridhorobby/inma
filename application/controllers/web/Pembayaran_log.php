<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Dompdf\Dompdf;

class Pembayaran_log extends WEB_Controller {

    public function index($orderby=null) {
        
        if ($this->postData['tahun'] && $this->postData['bulan']) {
            $_SESSION['pembayaran_log']['tahun'] = $tahun = $this->postData['tahun'];
            $_SESSION['pembayaran_log']['bulan'] = $bulan = $this->postData['bulan'];
        }else{
            if($orderby==null){
                $_SESSION['pembayaran_log']['tahun'] = $tahun = date('Y');
                $_SESSION['pembayaran_log']['bulan'] = $bulan = date('m');
            }else{
                $tahun = $_SESSION['pembayaran_log']['tahun'];
                $bulan = $_SESSION['pembayaran_log']['bulan'];
            }
            
        }

        switch ($orderby) {
            case 'ia':
                $order = 'tgl_input';
                $by = 'asc';
            break;
            case 'id':
                $order = 'tgl_input';
                $by = 'desc';
            break;
            case 'ja':
                $order = 'jenis';
                $by = 'asc';
            break;
            case 'jd':
               $order = 'jenis';
                $by = 'desc';
            break;
            case 'ka':
                $order = 'kode_order';
                $by = 'asc';
            break;
            case 'kd':
                $order = 'kode_order';
                $by = 'desc';
            break;
            case 'da':
                $order = 'tipe desc';
                $by = ',nominal asc';
            break;
            case 'dd':
                $order = 'tipe desc';
                $by = ',nominal desc';
            break;
            case 'kra':
                 $order = 'tipe asc';
                $by = ',nominal asc';
            break;
            case 'krd':
                $order = 'tipe asc';
                $by = ',nominal desc';
            break;
            default:
                $order = 'tgl_input';
                $by = 'desc';
            break;
            
        }
        if(SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
            $filter = " and created_by=".SessionManagerWeb::getUserID();
        }
        $this->data['data'] = $this->model->getLog($bulan, $tahun,$order, $by, $filter);
        // echo '<pre>';print_r($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }

    public function add(){
        $this->load->model('Konsumen_model');
        $this->load->model('Jenis_pembayaran_model');

        if($this->input->post('ajax')){
            $listkonsumen = $this->Konsumen_model->getKonsumenPengajuan($status, $this->input->post('keyword'));
            die(json_encode($listkonsumen));
        }else{
            $listkonsumen = $this->Konsumen_model->getKonsumenPengajuan($status, $keyword);
        }
        $this->data['listkonsumen'] = $listkonsumen;
        $this->data['listjenis'] = $this->Jenis_pembayaran_model->getJenis();

        // echo '<pre>';var_dump($this->data);die();

        // $this->template->viewDefault('pembayaran_log_add',$this->data);   
        $this->template->viewDefault($this->view,$this->data);   
    }

    public function edit($id){ 
        $this->load->model('Konsumen_model');
        $this->load->model('Jenis_pembayaran_model');
        $this->data['data'] = $this->model->getLogById($id);
        $this->data['listkonsumen'] = $this->Konsumen_model->getKonsumenPengajuan();
        $this->data['listjenis'] = $this->Jenis_pembayaran_model->getJenis();

        // echo '<pre>';var_dump($this->data);die();

        $this->template->viewDefault('pembayaran_log_add',$this->data);   
    }

    public function create() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;

        if (empty($data['keterangan'])) {
            $data['keterangan'] = '';
        }
        $insert = $this->model->create($data, SessionManagerWeb::getUserID());
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil menambahkan kas');
             redirect("web/pembayaran_log");

        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal menambahkan kas');
            redirect("web/pembayaran_log/add");
        }
    }
    
    public function update() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        
        $insert = $this->model->updateLog($data);
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil update data');
             redirect("web/pembayaran_log");
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal update data');
            redirect("web/pembayaran_log/edit/".$data['id']);
        }
    }
    
    public function delete($id) {
        // $hapus = dbQuery("UPDATE FROM pembayaran_log SET is_deleted='1' where id=".$id);
        $hapus = dbQuery("DELETE FROM pembayaran_log WHERE id=".$id);
        if ($hapus) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil hapus data');
             redirect("web/pembayaran_log");

        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal hapus data');
            redirect("web/pembayaran_log/edit/".$id);
        }
    }
    
    public function getBebanStatistikPerMonth() {
        $tahun = $this->postData['tahun'];

        $this->data['data'] = $this->model->filter($filter)->getBebanStatistikPerMonth($tahun);
        echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function getLunasStatistikPerMonth() {
        $tahun = $this->postData['tahun'];

        $this->data['data'] = $this->model->filter($filter)->getLunasStatistikPerMonth($tahun);
        echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function getDetailStatistik() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $this->data['data'] = $this->model->filter($filter)->getDetailStatistik($bulan, $tahun);
        echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function getDetailBebanStatistik() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];
        
        $this->data['persen'] = (100/7790000)*1000000;
        
        $this->data['data'] = $this->model->getDetailBebanPembayaran($bulan, $tahun);
        echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function getDetailLunasStatistik() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];
        
        $bulan = 8;
        $tahun = 2020;
        
        $this->data['data'] = $this->model->getDetailLunasPembayaran($bulan, $tahun);
        echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function getHutangProduksiPerBulan() {
        
        if ($this->postData['tahun']) {
            $_SESSION['hutangproduksi']['tahun'] = $tahun = $this->postData['tahun'];
        }else{
            $_SESSION['hutangproduksi']['tahun'] = $tahun = date('Y');
        }

        // $bulan = 5;
         // $tahun = 2020;

        $this->data['data'] = $this->model->getHutangProduksiPerBulan($bulan, $tahun);
        // echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function hutangProduksiPerBulanAdd(){
        $this->load->model('Konsumen_model');
        $this->load->model('Jenis_pembayaran_model');

        $this->data['listkonsumen'] = $this->Konsumen_model->getKonsumenPengajuan();
        $this->data['listjenis'] = $this->Jenis_pembayaran_model->getJenis();

        // echo '<pre>';var_dump($this->data);die();

        // $this->template->viewDefault('pembayaran_log_add',$this->data);   
        $this->template->viewDefault($this->view,$this->data); 
    }

    public function hutangProduksiPerBulanCreate(){
        $data = $this->postData;
        $insert = $this->model->create($data, SessionManagerWeb::getUserID());
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil menambahkan hutang produksi');
             redirect("web/pembayaran_log/gethutangproduksiperbulan");

        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal menambahkan hutang produksi');
            redirect("web/pembayaran_log/hutangproduksiperbulanadd");
        }
    }

    public function getHutangProduksiDetail() {
        $this->data['tahun'] = $tahun = $this->postData['tahun'];
        $this->data['bulan'] = $bulan = $this->postData['bulan'];

        // $bulan = 8; 
        // $tahun = 2020;

        $this->data['beban'] = $this->model->getDetailBebanPembayaran($bulan, $tahun);
        $this->data['lunas'] = $this->model->getDetailLunasPembayaran($bulan, $tahun);

        // echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }

    public function getstrukHutangProduksi($id){
        

        $this->load->model('Pengajuan_harga_model');

        $this->data['data'] = $this->Pengajuan_harga_model->filter($filter)->getOnePengajuan($this->page, $id);
        $details = $this->Pengajuan_harga_model->filter($filter)->getDetailPengajuan($id);

        $result = array();
        foreach ($details as $element) {
            $result[$element['nama_kategori']][] = $element;
        }
        $this->data['detail'] = $result;
        // echo '<pre>';var_dump($this->data);die();

        $this->template->viewDefault($this->view,$this->data);

    }

    public function previewStrukHutangProduksiPdf($id,$jenis,$tipeharga){
        $this->load->model('Pengajuan_harga_model');

        $this->data['jenis'] = $jenis;
        $this->data['tipeharga'] = $tipeharga;

        $this->data['data'] = $this->Pengajuan_harga_model->filter($filter)->getOnePengajuan($this->page, $id);
        $details = $this->Pengajuan_harga_model->filter($filter)->getDetailPengajuan($id);

        $result = array();
        foreach ($details as $element) {
            $result[$element['nama_kategori']][] = $element;
        }
        $this->data['detail'] = $result;
        // echo '<pre>';var_dump($this->data);die();

        $this->load->view(static::VIEW_PATH.'pembayaran_log_previewstrukpdf',$this->data);

        $html = $this->output->get_output();
// echo $html;
// die();
        $this->load->library('pdfdom');

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->set_option('isRemoteEnabled', TRUE);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("html_contents.pdf", array("Attachment"=> 0));
    }

    public function getListHutangProduksi() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $bulan = 8; 
        $tahun = 2020;

        $this->data['data'] = $this->model->getListHutangProduksi($bulan, $tahun);
        echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function getListStatusPiutangKonsumen() {
        // $tahun = $this->postData['tahun'];
        // $bulan = $this->postData['bulan'];

        if ($this->postData['tahun'] && $this->postData['bulan']) {
            $_SESSION['piutang_konsumen']['tahun'] = $tahun = $this->postData['tahun'];
            $_SESSION['piutang_konsumen']['bulan'] = $bulan = $this->postData['bulan'];
        }else{
            $_SESSION['piutang_konsumen']['tahun'] = $tahun = date('Y');
            $_SESSION['piutang_konsumen']['bulan'] = $bulan = date('m');
        }
        // $bulan = 7; $tahun = 2020;

        $this->data['data'] = $this->model->getListStatusPiutangKonsumen($bulan, $tahun, SessionManagerWeb::getRole(), SessionManagerWeb::getUserID());
        $this->data['sum'] = $this->model->getSumPiutangKonsumenPerBulan($tahun);
        // echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }

    public function getListStatusPiutangKonsumenAdd(){
        $this->load->model('Konsumen_model');
        $this->load->model('Jenis_pembayaran_model');

        $this->data['listkonsumen'] = $this->Konsumen_model->getKonsumenPengajuan();
        $this->data['listjenis'] = $this->Jenis_pembayaran_model->getJenis();

        // echo '<pre>';var_dump($this->data);die();

        // $this->template->viewDefault('pembayaran_log_add',$this->data);   
        $this->template->viewDefault($this->view,$this->data);   
    }

    public function getListStatusPiutangKonsumenCreate() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        if (empty($data['keterangan'])) {
            $data['keterangan'] = '';
        }
        $insert = $this->model->create($data, SessionManagerWeb::getUserID());
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil menambahkan piutang konsumen');
             redirect("web/pembayaran_log/getliststatuspiutangkonsumen");

        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal menambahkan piutang konsumen');
            redirect("web/pembayaran_log/getliststatuspiutangkonsumenadd");
        }
    }
    
    public function getSumPiutangKonsumenPerBulan() {
        $tahun = $this->postData['tahun'];

        $this->data['data'] = $this->model->getSumPiutangKonsumenPerBulan($tahun);
        echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);        
    }
    
    // 19 NOVEMBEr 2020
    public function getOmzet() {
        if ($data['user_id']=="") {
            $data['user_id'] =  SessionManagerWeb::getUserID();
        }
        
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];
        
        $this->data['data'] = $this->model->getOmzet($data['user_id'], $bulan, $tahun);
        echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }

    public function getstruk($id){
        

        $this->load->model('Pengajuan_harga_model');

        $this->data['data'] = $this->Pengajuan_harga_model->filter($filter)->getOnePengajuan($this->page, $id);
        $details = $this->Pengajuan_harga_model->filter($filter)->getDetailPengajuan($id);

        $this->load->model('Harga_model');

        $data = $this->Harga_model->filter($filter)->getHarga($this->page);

        $detail = array();
        $count = count($details) - 1;
        foreach ($data as $element) {
            foreach ($details as $key => $val) {
                if ($element['id'] == $val['id_harga']) {
                    $detail[$val['nama_kategori']][] = $val;
                    $hasil = 1;
                    break;
                }else{
                    if ($count == $key) {
                        $hasil = 0;
                    }
                }
            }

            if ($hasil == 0){

                $harga['id'] = null;
                $harga['id_pengajuan_harga'] = null;
                $harga['id_harga'] = $element['id'];
                $harga['harga_pokok_granit'] = 0;
                $harga['harga_pokok_keramik'] = 0;
                $harga['harga_jual_granit'] = 0;
                $harga['harga_jual_keramik'] = 0;
                $harga['jumlah'] = 0;
                $harga['nama'] = $element['nama_harga'];
                $harga['nama_kategori'] = $element['kategori'];

                $detail[$element['kategori']][] = $harga;
            }

        }
        

        $this->data['detail'] = $detail;

        // echo '<pre>';var_dump($this->data);die();

        $this->template->viewDefault($this->view,$this->data);

    }

    public function previewStrukPdf($id,$jenis,$tipeharga){
        $this->load->model('Pengajuan_harga_model');

        $this->data['jenis'] = $jenis;
        $this->data['tipeharga'] = $tipeharga;

        $this->data['data'] = $this->Pengajuan_harga_model->filter($filter)->getOnePengajuan($this->page, $id);
        $details = $this->Pengajuan_harga_model->filter($filter)->getDetailPengajuan($id);

        $this->load->model('Harga_model');

        $data = $this->Harga_model->filter($filter)->getHarga($this->page);

        $detail = array();
        $count = count($details) - 1;
        foreach ($data as $element) {
            foreach ($details as $key => $val) {
                if ($element['id'] == $val['id_harga']) {
                    $detail[$val['nama_kategori']][] = $val;
                    $hasil = 1;
                    break;
                }else{
                    if ($count == $key) {
                        $hasil = 0;
                    }
                }
            }

            if ($hasil == 0){

                $harga['id'] = null;
                $harga['id_pengajuan_harga'] = null;
                $harga['id_harga'] = $element['id'];
                $harga['harga_pokok_granit'] = 0;
                $harga['harga_pokok_keramik'] = 0;
                $harga['harga_jual_granit'] = 0;
                $harga['harga_jual_keramik'] = 0;
                $harga['jumlah'] = 0;
                $harga['nama'] = $element['nama_harga'];
                $harga['nama_kategori'] = $element['kategori'];

                $detail[$element['kategori']][] = $harga;
            }

        }

        $this->data['detail'] = $detail;
        // echo '<pre>';var_dump($this->data);die();

        $this->load->view(static::VIEW_PATH.$this->view,$this->data);

        $html = $this->output->get_output();
// echo $html;
// die();
        $this->load->library('pdfdom');

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->set_option('isRemoteEnabled', TRUE);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("html_contents.pdf", array("Attachment"=> 0));
    }

    public function getPiutangKonsumenStrukHarga() {
        $this->load->model('Harga_model');
        $filter = "where 1 ";

        $data = $this->Harga_model->filter($filter)->getHarga($this->page);

        $result = array();
        foreach ($data as $element) {
            $result[$element['kategori']][] = $element;
        }
        $this->data['data'] = $result;

        echo '<pre>';var_dump($this->data);die();
    }

    function ajaxgetsubjenispembayaran(){
        $post = $this->input->post();

        $this->load->model('Jenis_pembayaran_model');
        $subjenis = $this->Jenis_pembayaran_model->getSubJenis($post['id']);

        echo json_encode($subjenis);
    }
}

?>