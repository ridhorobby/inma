<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends WEB_Controller {

    /**
     * Digunakan untuk mengambil notifikasi user yang sedang login
     */
    public function me($page=0) {
        $this->load->library('pagination');
        $config['base_url'] = base_url().'web/notification/me/';
        $config['total_rows'] = $this->data['jmlnotif'];
        $config['per_page'] = 20;



        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $this->pagination->initialize($config);
        //SET UNREAD NOTIFICATION
        $this->load->model('Notification_user_model');
        // $this->Notification_user_model->setAsUnRead($this->user['id']);

        $halaman = $page / 20; 
        $this->data['notifications'] = $this->model->getMe(SessionManagerWeb::getUserID(), $halaman, true);
        // echo "<pre>";print_r($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }

    // public function setAllRead() {
    //     $this->load->model('Notification_user_model');
    //     $resp = $this->Notification_user_model->setAsUnRead($this->user['id']);
    //     if ($resp) {
    //         $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Semua notifikasi terbaca'));
    //     } else {
    //         $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Gagal set Notifikasi'));
    //     }
    // }

}

?>