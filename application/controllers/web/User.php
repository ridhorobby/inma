<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller User
 * @author Sevima
 * @version 1.0
 */
class User extends WEB_Controller {

    //protected $title = 'User';


    /**
     * Digunakan untuk mengambil list user
     */
    public function index() {

        if(SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR){
            if($this->postData['filterExist']){
                $filter = "where 1 ";
                $filter .= $this->getFilter($this->postData);
            }
            $this->data['user'] = $this->model->getAll($filter);
            // echo "<pre>";print_r($this->data);die();
            $this->template->viewDefault($this->view,$this->data);
            // echo "<pre>";print_r($this->data['user']);die();
            
            
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Anda tidak bisa mengakses fungsi ini');
            redirect('web/pesanan');
        }
        
    }

    private function getFilter($data){
        $filter = '';
        if($data['username']){
            $username = strtolower($data['username']);
            $filter .= " and lower(username) like '%$username%' ";
        }
        if($data['name']){
            $name = strtolower($data['name']);
            $filter .= " and lower(name) like '%$name%' ";
        }
        if($data['no_hp']){
            $no_hp = $data['no_hp'];
            $filter .= " and no_hp like '%$no_hp%' ";
        }
        if($data['role']){
            $role = strtoupper($data['role']);
            $filter .= " and upper(role) like '%$role%' ";
        }
        return $filter;
    }

    public function detail($id) {
        $this->data['user'] = $this->model->get($id);
        $this->data['list_role'] = Role::getArray();
        // echo "<pre>";print_r($this->data['user']);die();
        $this->template->viewDefault($this->view, $this->data);
        //SET RESPONSE
        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $user);
    }

    public function addUser(){
        $this->data['list_role'] = Role::getArray();
        $this->template->viewDefault($this->view, $this->data);
        // echo "<pre>";print_r($this->data['list_role']);die();
    }

    public function create(){
        if(SessionManagerWeb::getRole() == Role::ADMINISTRATOR || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR){
            $postData = $this->input->post();
            $this->load->model('User_model');
            $auth_key = SecurityManager::generateAuthKey();
            $dataInsert = array(
                'name'          => $postData['name'],
                'username'      => strtolower($postData['username']),
                'role'          => $postData['role'],
                'no_hp'         => $postData['no_hp'],
                'password'      => SecurityManager::hashPassword($postData['password'], $auth_key),
                'password_salt' => $auth_key,
                'created_at'    => date('Y-m-d G:i:s', time()),
                'updated_at'    => date('Y-m-d G:i:s', time())
            );
            // echo "<pre>";print_r($dataInsert);die();
            $this->data['password'] = $postData['password'];
            $this->data['auth_key'] = $auth_key;
            $this->data['has']      = SecurityManager::hashPassword($postData['password'], $auth_key);
            $insert = $this->User_model->create($dataInsert, true, true);
            if($insert){
            	
                SessionManagerWeb::setFlashMsg(true, "Berhasil mendaftarkan user");
            }else{
            	
                SessionManagerWeb::setFlashMsg(false, "Gagal mendaftarkan user");
            }
            redirect('web/user');
        }
        SessionManagerWeb::setFlashMsg(false, 'Anda tidak bisa mengakses fungsi ini');
        redirect('web/pesanan');
    }

    /**
     * Digunakan untuk update profil user
     */
    public function update($id) {
        $data = $this->postData;
        if(isset($data['password'])){
            $user = dbGetRow("select password, password_salt from users where id=".$id);
            // echo "<pre>";print_r($user);die();
            $check = SecurityManager::validate($data['password_lama'], $user['password'], $user['password_salt']);
            if(!$check){
                SessionManagerWeb::setFlashMsg(false, 'Password lama salah');
            }
            $auth_key = SecurityManager::generateAuthKey();
            $data['password']      = SecurityManager::hashPassword($data['password'], $auth_key);
            $data['password_salt'] = $auth_key;
        }
        $update = $this->model->save($data,$id);
        if ($update === TRUE) {
            $newData = $this->model->get($id);
            SessionManagerWeb::setFlashMsg(true, 'Berhasil mengubah profil');
        } elseif (is_string($update)) {
            SessionManagerWeb::setFlashMsg(false, $update);
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                SessionManagerWeb::setFlashMsg(false, 'Gagal mengubah profil');
            } else {
                SessionManagerWeb::setFlashMsg(false, 'Gagal mengubah profil');
            }
        }

        redirect("web/user/detail/".$id);
    }

    public function resetPassword($user_id){
    	if(SessionManagerWeb::getRole() == Role::ADMINISTRATOR || SessionManagerWeb::getRole() == Role::DIREKTUR){
            $auth_key = SecurityManager::generateAuthKey();
            $pass = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);
            $data = array(
                'password'  => SecurityManager::hashPassword($pass, $auth_key),
                'password_salt' => $auth_key,
                'updated_at'    => date('Y-m-d G:i:s', time())
            );

            $save = $this->model->save($data, $user_id);
            if($save){
            	$check = dbGetOne("select id from reset_password where user_id=".$user_id);
            	if($check){
            		$dataPass = array(
	                    'temp_password' => $pass,
	                    'user_id'       => $user_id,
	                    'updated_at'    => date('Y-m-d G:i:s', time())
	                );
	                dbUpdate("reset_password", $dataPass, "id=$check");
            	}else{
            		$dataPass = array(
	                    'temp_password' => $pass,
	                    'user_id'       => $user_id,
	                    'created_at'    => date('Y-m-d G:i:s', time()),
	                    'updated_at'    => date('Y-m-d G:i:s', time())
	                );
	                dbInsert("reset_password", $dataPass);
            	}
            	SessionManagerWeb::setFlashMsg(true, "Berhasil reset password, silahkan salin password dibawah sebelum aplikasi auto logout\n".$pass);
	                redirect('web/user');
                
            }else{
            	SessionManagerWeb::setFlashMsg(false, 'Gagal reset password');
                redirect('web/user');
            }

        }else{
        	SessionManagerWeb::setFlashMsg(false, 'Anda tidak bisa mengakses fungsi ini');
            redirect('web/pesanan');
        }

    }


    public function getUserByRole($role= Role::PEMASANG,$listView=false) {
        $column='*';
        if($listView){
            $column = "id,name";
        }
        $this->data['users'] = $this->model->column($column)->filter("where role='".$role."'")->getUser();
        if($listView){
            $this->data['users'] = Util::toMap($users,'id','name');
            // echo "<pre>";print_r($users);die();
        }
        return $this->data['users'];
        // echo "<pre>";print_r($this->data['users']);
    }

    public function nonactive($user_id){
        if(SessionManagerWeb::getRole() != Role::ADMINISTRATOR){
        	SessionManagerWeb::setFlashMsg(false, 'Anda tidak bisa mengakses fungsi ini');
            redirect('web/pesanan');
        }
        $users = $this->model->setNonactive($user_id);
        SessionManagerWeb::setFlashMsg(true, 'Berhasil menonaktifkan user');
        redirect("web/$this->class");

    }

    public function activated($user_id){
        if(SessionManagerWeb::getRole() != Role::ADMINISTRATOR){
        	SessionManagerWeb::setFlashMsg(false, 'Anda tidak bisa mengakses fungsi ini');
            redirect('web/pesanan');
        }
        $users = $this->model->setActive($user_id);
        SessionManagerWeb::setFlashMsg(true, 'Berhasil mengaktifkan user');
        redirect("web/$this->class");

    }

    /**
     * Logout
     */
    public function logout() {
        AuthManagerWeb::logout();
        redirect($this->getCTL('site'));
    }

    
}
