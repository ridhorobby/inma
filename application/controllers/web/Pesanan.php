<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Pesanan extends WEB_Controller {
	

    /**
	 * Halaman login user
	 */
    public function index() {

        // if(SessionManagerWeb::getRole()== Role::PEMASANG){
        //     redirect('web/pesanan/daftarorder');
        // }
        if($this->postData['filterExist']){
            $where .= $this->getFilter($this->postData);
        }
        $this->data['pesanan_jatuh_tempo'] = $this->model->filter($where)->getPesananJatuhTempo();

        $this->template->viewDefault($this->view,$this->data);

        // $this->load->view('web/pesanan_index',$this->data);
    }

    public function addPemasangan() {
        
        if(SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR || SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::DESAIN){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            $this->data['pesanan_baru'] = $this->model->filter($where)->getPesananBaru($this->page);

            $this->template->viewDefault('pesanan_pemasangan_add',$this->data);
        }
        // $this->load->view('web/pesanan_index',$this->data);
    }

    public function addPenanganan() {
        
        if(SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR || SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::DESAIN){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            $this->data['pesanan_baru'] = $this->model->filter($where)->getPesananBaru($this->page);
            $this->template->viewDefault('pesanan_penanganan_add',$this->data);
        }

        // $this->load->view('web/pesanan_index',$this->data);
    }

    public function ajaxkalendarViewUndone(){
        // echo "<pre>";print_r($this->postData);die();
        $filter = "where 1 ";
        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];
        $filter .= " and flow_id <> 8 and 
             ((YEAR(tgl_masuk) = $tahun AND MONTH(tgl_masuk) = $bulan) 
             OR ( YEAR(tgl_jatuh_tempo)=$tahun AND MONTH(tgl_jatuh_tempo)=$bulan)
             OR ( YEAR(tanggal_pasang_kusen)=$tahun AND MONTH(tanggal_pasang_kusen)=$bulan) 
             OR ( YEAR(jatuh_tempo_kusen) = $tahun AND MONTH(jatuh_tempo_kusen) =$bulan)
             OR ( YEAR(tanggal_pasang_finish) = $tahun AND MONTH(tanggal_pasang_finish) =$bulan)
             OR ( YEAR(jatuh_tempo_finish) = $tahun AND MONTH(jatuh_tempo_finish) =$bulan))

        ";
        if(SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
            $filter .= " and user_id=".SessionManagerWeb::getUserID();
        }
        if(SessionManagerWeb::getRole() == Role::PEMASANG){
            $filter .= " and (mitra_pemasang_kusen=".SessionManagerWeb::getUserID().") or (mitra_pemasang_finish=".SessionManagerWeb::getUserID().")";
        }
        // echo $filter;die();
        $data = $this->model->filter($filter)->getKalendar('unl');
        // echo "<pre>";print_r($data);die();
        echo json_encode($data);
    }

    public function ajaxkalendarView($page=false){
        $filter = "where 1 ";

        // var_dump($this->postData);
        // if($this->postData['filterExist']){
        //     $filter .= $this->getFilter($this->postData);
        // }
        if($this->postData['kode_order'] || $this->postData['nama_konsumen'] || $this->postData['alamat_konsumen'] || $this->postData['no_hp_konsumen']){
            // echo "<pre>";print_r($this->postData);die();
            $filter .= $this->getFilter($this->postData);
        }

        if($this->postData['bulan'] && $this->postData['tahun']){
            $tahun = $this->postData['tahun'];
            $bulan = $this->postData['bulan'];
            $filter .= " and
                 ((YEAR(tgl_masuk) = $tahun AND MONTH(tgl_masuk) = $bulan) 
                 OR ( YEAR(tgl_acc_konsumen)=$tahun AND MONTH(tgl_acc_konsumen)=$bulan)
                 OR ( YEAR(tanggal_pasang_kusen)=$tahun AND MONTH(tanggal_pasang_kusen)=$bulan) 
                 OR (YEAR(tanggal_pasang_finish) = $tahun AND MONTH(tanggal_pasang_finish) =$bulan))
            ";
        }
        

        // var_dump($this->postData);
        // if($this->postData['history']){
        //     $_SESSION['start_history_konsumen']['bulan'] = $bulan;
        //     $_SESSION['start_history_konsumen']['tahun'] = $tahun;
        // }
        // if($bulan && $tahun){
        //     $filter .= " and
        //          ((YEAR(tgl_masuk) = $tahun AND MONTH(tgl_masuk) = $bulan) 
        //          OR ( YEAR(tgl_acc_konsumen)=$tahun AND MONTH(tgl_acc_konsumen)=$bulan)
        //          OR ( YEAR(tanggal_pasang_kusen)=$tahun AND MONTH(tanggal_pasang_kusen)=$bulan) 
        //          OR (YEAR(tanggal_pasang_finish) = $tahun AND MONTH(tanggal_pasang_finish) =$bulan))
        //     ";
        // }
        
        if(SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
            $filter .= " and user_id=".SessionManagerWeb::getUserID();
        }
        if(SessionManagerWeb::getRole() == Role::PEMASANG){
            $filter .= " and (mitra_pemasang_kusen=".SessionManagerWeb::getUserID().") or (mitra_pemasang_finish=".SessionManagerWeb::getUserID().")";
        }
        $data['result'] = $this->model->filter($filter)->getKalendar(($page!==false) ? (($page==0) ? $page : $page-1 ) : 'unl');
        if($page==0 ||$page===false){
             $_SESSION['history_konsumen']['total_data'] = $this->model->filter($filter)->getKalendar('count');
        }

        // Pagination Configuration
        $this->load->library('pagination');

        $config['base_url'] = base_url().'web/pesanan/ajaxkalendarview/';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $_SESSION['history_konsumen']['total_data'];
        $config['per_page'] = 10;

        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = '&raquo';
        $config['prev_link']        = '&laquo';
        $config['full_tag_open']    = '<ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';


        // Initialize
        $this->pagination->initialize($config);

        // Initialize $data Array
        $data['pagination'] = $this->pagination->create_links();
        // die($data);
        echo json_encode($data);
    }

    public function getFilter($data){
        $filter = '';
        if($data['kode_order']){
            $kode_order = strtolower($data['kode_order']);
            $filter .= " and lower(p.kode_order) like '%$kode_order%' ";
        }
        if($data['nama_konsumen']){
            $nama_konsumen = strtolower($data['nama_konsumen']);
            $filter .= " and lower(p.nama_konsumen) like '%$nama_konsumen%' ";
        }
        if($data['alamat_konsumen']){
            $alamat_konsumen = strtolower($data['alamat_konsumen']);
            $filter .= " and lower(p.alamat_konsumen) like '%$alamat_konsumen%' ";
        }
        if($data['no_hp_konsumen']){
            $no_hp_konsumen = strtolower($data['no_hp_konsumen']);
            $filter .= " and lower(p.no_hp_konsumen) like '%$no_hp_konsumen%' ";
        }
        if($data['flow_id']){
            $filter .= " and p.flow_id=".$flow_id." ";
        }

        return $filter;
    }

    public function create() {
        if(SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::PEMASANG){
            SessionManagerWeb::setFlashMsg(false, "Anda tidak bisa mengakses fungsi ini");
        }
        $data = $this->postData;
        // echo "<pre>";print_r($data);die();
        // $temp['user_id'] = $data['user_id'];

        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        $this->load->model('Konsumen_model');
        $dataKonsumen = array(
            'nama'      => $data['nama'],
            'alamat'    => $data['alamat'],
            'no_hp'     => $data['no_hp']    
        );


        /// CHECK KODE ORDER IS EXIST
        // NOTE : KARENA PESANAN BISA DIHAPUS SEMUA, MAKA ATURAN INI TIDAK BERLAKU AGAR BISA INPUT DENGAN KODE YANG SAMA
        // $checkOrder = $this->model->get_by('kode_order', $data['kode_order']);
        // if($checkOrder){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Pesanan, Kode Pesanan sudah ada'));
        // }

        $konsumen_id = $this->Konsumen_model->create($dataKonsumen);
        
        if($konsumen_id){
            $data['konsumen_id'] = $konsumen_id;
            $data['tipe'] = 'P';
            
            if ($data['status_produksi']=="") {
                $data['status_produksi'] = '3';
                // $users = $this->User_model->getUser($data['user_id']);
            }
        
            $insert = $this->model->create($data, SessionManagerWeb::getUserID());
            if ($insert) {
                if($data['id_pengajuan']!=0){
                    dbUpdate('pengajuan_harga', array('id_konsumen' => $konsumen_id),"id=".$data['id_pengajuan']);
                    $this->load->model("Fee_desainer_model");

                    $pesanan['harga_deal'] = dbGetOne("select harga_deal from pesanan_view where id=".$insert);
                    $fee_design = 50000;
                    if ($pesanan['harga_deal']>=7500000 && $pesanan['harga_deal']<10000000) {
                        $fee_design = 75000;
                    }elseif($pesanan['harga_deal']>=10000000){
                        $fee_design = 100000;
                    }
                    $insertFeeDesainer = array(
                        'desainer_design_id'    => $data['desainer_design'],
                        'desainer_eksekusi_id'  => $data['desainer_eksekusi'],
                        'pesanan_id'            => $insert,
                        'fee_design'            => $fee_design
                    );
                    $this->Fee_desainer_model->create($insertFeeDesainer);
                }
                if (!$this->_moveFileTemp($insert)) {
                    $hapus = dbQuery("delete from pesanan where id=".$insert);
                    SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Pesanan');
                }else{
                    $this->load->model('Notification_model');
                    $this->Notification_model->generate(Notification_model::ACTION_PESANAN_CREATE, $insert,SessionManagerWeb::getUserID());
                    SessionManagerWeb::setFlashMsg(true, 'Berhasil Membuat Pesanan');
                }
                
            } 
            else {
                $validation = $this->model->getErrorValidate();
                if (empty($validation)) {
                    SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Pesanan');
                } else {
                    SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Pesanan');
                }
            }
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Pesanan');
        }

        redirect("web/pesanan/daftarorder");
    
    }

    public function updateKonsumen($pesanan_id){
        $data = $this->postData;
        $flow_id = dbGetOne("select flow_id from pesanan where id=".$pesanan_id);
        if($flow_id != 2){
            SessionManagerWeb::setFlashMsg(false, 'Gagal mengubah data Konsumen');
        }
        $konsumen_id = dbGetOne("select konsumen_id from pesanan where id=".$pesanan_id);
        // echo "<pre>";print_r($data);die();
        // nama, alamat no_hp
        $data['updated_at'] = date('Y-m-d G:i:s', time());
        
        $this->load->model('Konsumen_model');
        $data['id_konsumen'] = $konsumen_id;
        $update = $this->Konsumen_model->updateKonsumen($data);
        // echo "<pre>";print_r($update);echo "</pre>"
        if($update){
            $updatePesanan = $this->model->updateKodeOrder($pesanan_id, $data['kode_order']);
            // $this->load->model('Notification_model');
            // $this->Notification_model->generate(Notification_model::ACTION_PESANAN_UPDATE, $pesanan_id,$this->user['id']);
            if($updatePesanan) {
                SessionManagerWeb::setFlashMsg(true,'Berhasil mengubah data Pesanan');
            } else {
                SessionManagerWeb::setFlashMsg(false, 'Gagal mengubah data Konsumen');
            }
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal mengubah data Konsumen');
        }

        redirect('web/pesanan/detail/'.$pesanan_id);
    }

    public function daftarOrder(){
        if(SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR || SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            if(SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
                $where .=" and p.user_id=".SessionManagerWeb::getUserID();
            }
            $this->data['pesanan_baru'] = $this->model->filter($where)->getPesananBaru($this->page);
            $this->data['need_action'] = self::getNeedAction($this->postData);
            $this->data['in_progress'] = self::getInProgress($this->postData);
            $this->data['filter'] = $this->postData;
            // echo "<pre>";print_r($this->data);die();
            // $this->load->view('web/pesanan_daftaroder',$this->data);
            $this->template->viewDefault($this->view,$this->data);
        }elseif (SessionManagerWeb::getRole() == Role::PEMASANG) {
            $this->load->model('Komplain_model');
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }

            // $this->data['in_progress_order'] = self::getInProgress($this->postData);
            $this->data['need_action'] = self::getNeedAction($this->postData);
            $column = "k.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan , us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id";
            $where .= "where (
                                (k.flow_id in (4,7)  and (k.mitra_pemasang_kusen=".SessionManagerWeb::getUserID()." or k.mitra_pemasang_finish=".SessionManagerWeb::getUserID().")) or 
                               (k.flow_id = 2 and tanggal_pasang_kusen is null and k.mitra_pemasang_kusen=".SessionManagerWeb::getUserID().") or
                               (k.flow_id = 5 and tanggal_pasang_kusen is not null and tanggal_pasang_finish is null and k.mitra_pemasang_finish=".SessionManagerWeb::getUserID().")
                           ) 
                        ";
            
            $this->data['need_action_komplain'] = $this->Komplain_model->column($column)->join($join)->filter($where)->getKomplains('unl');
            // $this->data['in_progress_komplain'] = self::getInProgress($this->postData);
            // echo "<pre>";print_r($this->data['need_action_komplain']);die();
            $this->template->viewDefault('pesanan_daftarorderpemasang',$this->data);

        }else{
            SessionManagerWeb::setFlashMsg(false, 'Anda tidak dapat mengakses fungsi ini');
            redirect('web/'.$this->data['class']);
        }
    }

    public function orderAdd(){
        if(SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR ||  SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
            $this->load->model('Konsumen_model');
            $this->load->model('User_model');

            if($this->input->post('keyword')){
                $konsumen_deal = $this->Konsumen_model->getKonsumenDealPengajuan($this->input->post('keyword'));
                die(json_encode($konsumen_deal));
            }else{
                $konsumen_deal = $this->Konsumen_model->getKonsumenDealPengajuan('');
            }
            $this->data['desainer'] = $this->User_model->column('*')->filter("where is_active=1 and role!='".Role::ADMINISTRATOR."' ")->getUser();
            $this->data['konsumen_deal'] = $konsumen_deal;
            // $this->data['desainer_list'] = $this->User->getUserByRole(Role::DESAIN);
            // echo "<pre>";print_r($this->data);die();
        	$this->removeTemp('pesanan',true);
            $this->template->viewDefault($this->view,$this->data);

        }else{
            SessionManagerWeb::setFlashMsg(false, 'Anda tidak dapat mengakses fungsi ini');
            redirect($this->data['class']);
        }
    }

    public function usulkanPemasangan($pesanan_id) {
        
        if(SessionManagerWeb::getRole() == Role::PRODUKSI){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            $this->load->model('User_model');
            $this->data['mitra_pemasang'] = $this->User_model->column('*')->filter("where role='".Role::PEMASANG."'")->getUser();
            $this->data['pesanan_baru'] = $this->model->getPesananBaru(0);
            $back = explode('/', $_SERVER['HTTP_REFERER']);
            $this->data['back'] = $back[5].'/'.$back[6];
            $this->data['detail'] = $detail = $this->model->getDetailPesanan($pesanan_id);

            if($detail['flow_id']==2){
                $this->data['user_mitra_pemasang'] = $this->User_model->column('*')->filter("where id='".$detail['mitra_pemasang_kusen']."'")->getUserById();
            }elseif($detail['flow_id']==5){
                $this->data['user_mitra_pemasang'] = $this->User_model->column('*')->filter("where id='".$detail['mitra_pemasang_finish']."'")->getUserById();
            }else{
                $this->data['user_mitra_pemasang'] = null;
            }
            // echo "<pre>";print_r($this->data['detail']);die();
            // var_dump($this->data['user_mitra_pemasang']);die();

            $this->template->viewDefault($this->view,$this->data);
        }else{
            $detail = $this->model->getDetailPesanan($pesanan_id);
            if(($detail['flow_id']==2 && $detail['tanggal_pasang_kusen']==null && $detail['mitra_pemasang_kusen'] == SessionManagerWeb::getUserID()) or
               ($detail['flow_id']==5 && $detail['tanggal_pasang_finish']==null && $detail['mitra_pemasang_finish'] == SessionManagerWeb::getUserID())
            ){
                $this->data['detail'] = $detail;
                $back = explode('/', $_SERVER['HTTP_REFERER']);
                $this->data['back'] = $back[5].'/'.$back[6];

                $this->template->viewDefault($this->view,$this->data);
            }
        }
        // $this->load->view('web/pesanan_index',$this->data);
    }

    public function tolakUsulkanPemasangan($pesanan_id) {
        
        //if(SessionManagerWeb::getRole() != Role::PRODUKSI){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            $this->data['detail'] = $this->model->getDetailPesanan($pesanan_id);
            $this->template->viewDefault($this->view,$this->data);
        //}
        // $this->load->view('web/pesanan_index',$this->data);
    }

    public function rescheduleJadwal($pesanan_id) {
        
        if(SessionManagerWeb::getRole() != Role::PRODUKSI){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }

            $this->data['detail'] = $this->model->getDetailPesanan($pesanan_id);
            // echo "<pre>";print_r($this->data['detail']);die();
            $this->template->viewDefault($this->view,$this->data);
        }
        // $this->load->view('web/pesanan_index',$this->data);
    }

    public function setselesaiproses($pesanan_id) {
        $this->removeTemp('pesanan',true);
        $this->data['pesanan_id'] = $pesanan_id;
        $this->template->viewDefault($this->view,$this->data);
    }

    public function getInProgress($postData=null){
        $where = "where 1 ";
        if($postData['filterExist']!=null){
            $where .= $this->getFilter($postData);
        }
        if(SessionManagerWeb::getRole() == Role::PRODUKSI){
            $column = "p.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan ,  us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join usulan u on p.id = u.pesanan_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and p.flow_id in(3,4,6,7)";
        }
        elseif(SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
            $where .= "and p.flow_id in (4,7)";
            if(SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
                $where .= "and p.user_id=".SessionManagerWeb::getUserID();
            }
        }
        // NOTE : AGAR DAPAT MELIHAT SEMUA STATUS PESANAN
        elseif(SessionManagerWeb::getRole() == Role::DIREKTUR){
            $where .= "and p.flow_id in (2,4,5,7)";
        }
        elseif(SessionManagerWeb::getRole() == Role::PEMASANG){
            $where .= "and p.flow_id=5 and (p.mitra_pemasang_kusen=".SessionManagerWeb::getUserID()." or p.mitra_pemasang_finish=".SessionManagerWeb::getUserID().")";
        }
        $in_progress = $this->model->filter($where)->getPesanans('unl');
        return $in_progress;
    }

    public function getNeedAction($postData){
        $where = "where 1 ";
        if($postData['filterExist']!=null){
            $where .= $this->getFilter($postData);
        }
        if(SessionManagerWeb::getRole() == Role::PRODUKSI){
            $column = "p.* , u.jadwal_usulan as jadwal_usulan, u.jam_usulan as jam_usulan,  us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join usulan u on p.id = u.pesanan_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and p.flow_id in(2,5) and mitra_pemasang_kusen <> 0 ";
        }
        elseif(SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
            $column = "p.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan , us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join usulan u on p.id = u.pesanan_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and p.flow_id in (3,6)";
            if(SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
                $where .= "and p.user_id=".SessionManagerWeb::getUserID();
            }
        }
        elseif(SessionManagerWeb::getRole() == Role::PEMASANG){
            $column = "p.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan , us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join usulan u on p.id = u.pesanan_id and u.status='P'
                     left join users us on u.user_id = us.id";
            $where .= "and (
                                (p.flow_id in (4,7)  and (p.mitra_pemasang_kusen=".SessionManagerWeb::getUserID()." or p.mitra_pemasang_finish=".SessionManagerWeb::getUserID().")) or 
                               (p.flow_id = 2 and tanggal_pasang_kusen is null and p.mitra_pemasang_kusen=".SessionManagerWeb::getUserID().") or
                               (p.flow_id = 5 and tanggal_pasang_kusen is not null and tanggal_pasang_finish is null and p.mitra_pemasang_finish=".SessionManagerWeb::getUserID().")
                           ) 
                        ";
        }
        $need_action = $this->model->column($column)->join($join)->filter($where)->show_sql(false)->getPesanans('unl');

        return $need_action; 

    }


    public function detail($pesanan_id, $notification_user_id=null){
        if(SessionManagerWeb::getRole() == Role::PEMASANG){
            redirect('web/pesanan');
        }
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($notification_user_id);

        $this->load->model('User_model');
        $this->load->model('Pembayaran_log_model');

        $this->removeTemp('pesanan',true);
        $this->data['detail'] = $this->model->getDetailPesanan($pesanan_id);
        // echo "<pre>";print_r($this->data['detail']);die();
        $dataFotoOrder = $this->model->getPhoto($pesanan_id);
        $dataFotoAfter = $this->model->getGambarAfter($pesanan_id);
        // echo "<pre>";print_r($dataFotoAfter);die();
        // echo SessionManagerWeb::getUserID();
        
        // echo $id;die();
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = base_url().'assets/uploads/pesanan/photos/';
        // echo "<pre>";print_r($id);die();
        foreach ($dataFotoOrder as $fotoOrder) {
            $id = md5($fotoOrder['user_id'] . $this->config->item('encryption_key'));
            // echo $fotoOrder['nama_file']."<br>";
            $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
            if($fotoOrder['tipe'] == 'OM'){
                $file_order[] = $path.$file_name;
            }elseif($fotoOrder['tipe'] == 'PF'){
                $file_acc[] = $path.$file_name;
            }
            
        }

        foreach ($dataFotoAfter as $fotoAfter) {
            $id = md5($fotoAfter['user_id'] . $this->config->item('encryption_key'));
            $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoAfter['nama_file']);
            if($fotoAfter['tipe'] == 'FA'){
                $file_fa[] = base_url().'assets/uploads/foto_after/photos/'.$file_name;

            }elseif($fotoAfter['tipe'] == 'EK'){
                $file_ek[] = base_url().'assets/uploads/edit_katalog/photos/'.$file_name;
            }
        }
        // die();
        $this->data['detail']['foto_order'] = $file_order;
        $this->data['detail']['foto_acc'] = $file_acc;
        $this->data['detail']['foto_fa'] = $file_fa;
        $this->data['detail']['foto_ek'] = $file_ek;

        $this->data['desainer'] = $this->User_model->column('*')->filter("where role='".Role::DESAIN."'")->getUser();

        $this->data['pembayaran'] = $this->Pembayaran_log_model->getPembayaranLogByKonsumen($this->data['detail']['konsumen_id']);
        

        // echo "<pre>";print_r($this->data['pembayaran']);die();
        $this->template->viewDefault('pesanan_detail',$this->data);
        // $this->load->view('web/pesanan_detail',$this->data);
    }

    public function updateStatusProduksi() {

        // postdata status_produksi, id
        $data = $this->postData;        
        $update = $this->model->updateStatusProduksi($data);        
        if ($update) {
            SessionManagerWeb::setFlashMsg(true,'Berhasil update data');
        } else {
            SessionManagerWeb::setFlashMsg(false,'Gagal update data');
        }

        // redirect("web/$this->data['method']/detail/".$data['id']);
        if($data['direct']){
            redirect("web/pesanan/detail/".$data['id']);
        }else{
            redirect("web/pesanan/daftarOrder");
        }
        
    }

    public function deletePesanan($pesanan_id, $next_direct='daftarorder'){
        $flow_id = dbGetOne("select flow_id from pesanan where id=".$pesanan_id);
        // NOTE : DIHAPUS KARENA MINTA SEMUA BISA DIHAPUS
        // if($flow_id != 2){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal menghapus data Konsumen'));
        // }
        $data['is_deleted'] = 1;
        $data['updated_at'] = date('Y-m-d G:i:s', time());
        $update = dbUpdate('pesanan', $data, "id=".$pesanan_id);
        if($update){
            $this->load->model('Pesanan_log_model');
            $this->Pesanan_log_model->generate($pesanan_id, SessionManagerWeb::getUserID(), 'Pesanan dihapus');
            SessionManagerWeb::setFlashMsg(true,'Berhasil menghapus data');
        }else{
            SessionManagerWeb::setFlashMsg(false,'Gagal menghapus data');
        }
        if($next_direct=='detail'){
            redirect("web/pesanan/detail/".$pesanan_id);
        }else{
            redirect("web/pesanan/$next_direct");
        }
    }


    public function usulan($pesanan_id){
        $this->load->model('Usulan_model');
        $this->load->model('Notification_model');
        $renker = dbGetRow("select flow_id,rencana_kerja_id, mitra_pemasang_kusen, mitra_pemasang_finish from pesanan_view where id=".$pesanan_id);
        switch ($renker['flow_id']) {
            case 2:
                if(SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getUserID() == $renker['mitra_pemasang_kusen']){
                    // Melakukan pengajuan

                    $usulan_exist = dbGetRow("select id,jadwal_usulan from usulan where pesanan_id=".$pesanan_id." and flow_id=".$renker['flow_id']." and status='P'");
                    if(!$usulan_exist && SessionManagerWeb::getRole() == Role::PRODUKSI){ // pengajuan pesanan_baru
                        if($this->postData['jadwal_usulan']){
                            $usulan_id = $this->Usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $pesanan_id, SessionManagerWeb::getUserID());
                            if($usulan_id){
                                $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $pesanan_id);
                                $this->model->addFlow($pesanan_id, $renker['flow_id']);
                                $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $pesanan_id,SessionManagerWeb::getUserID());
                                SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan pengajuan');
                            }else{
                                SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan');
                            }
                            
                        }else{
                            $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $pesanan_id);
                            $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $pesanan_id,SessionManagerWeb::getUserID());
                            SessionManagerWeb::setFlashMsg(true,'Berhasil set mitra pemasang');
                        }

                        
                        
                        // if($usulan_id){
                        //     if(SessionManagerWeb::getRole() == Role::PRODUKSI){
                                
                        //     }
                        //     if($this->postData['jadwal_usulan']){
                                
                        //     }
                            
                        //     $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $pesanan_id,SessionManagerWeb::getUserID());
                        //     SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan pengajuan');
                            //$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                        // }else{
                        //     SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan');
                        // }
                        
                    }
                    else if(!$usulan_exist && SessionManagerWeb::getUserID() == $renker['mitra_pemasang_kusen']){ // diisi oleh mitra pemasang
                        $usulan_id = $this->Usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $pesanan_id, SessionManagerWeb::getUserID());
                        if($usulan_id){
                            $this->model->addFlow($pesanan_id, $renker['flow_id']);
                            $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $pesanan_id,SessionManagerWeb::getUserID());
                            SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan pengajuan');
                        }else{
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan');
                        }
                    }
                    else{ // konformasi penolakan
                        if($this->postData['confirm'] == 0 && !$this->postData['jadwal_usulan']){
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan, masukkan tanggal pengajuan');
                        }
                        $konfirmasi = $this->Usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'],$renker['rencana_kerja_id'],$renker['flow_id'], $pesanan_id,SessionManagerWeb::getUserID(),false, 2);
                        if($konfirmasi){
                            if($this->postData['confirm'] == 0){
                                $this->model->addFlow($pesanan_id, $renker['flow_id']);
                                $this->Notification_model->generate(Notification_model::ACTION_REJECT_REVISI_KUSEN, $pesanan_id,SessionManagerWeb::getUserID());
                            }else{
                                $this->model->addFlow($pesanan_id, $renker['flow_id'], 2);
                                $this->Notification_model->generate(Notification_model::ACTION_CONFIRM_REVISI_KUSEN, $pesanan_id,SessionManagerWeb::getUserID());
                            }
                            SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');

                        }else{
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan konfirmasi');
                        }
                    }
                }
            break;
            case 3:
                if(SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
                    // postData: confirm, jadwal_usulan(jika ada)
                    if($this->postData['confirm']==0 && !($this->postData['jadwal_usulan'])){
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan, masukkan tanggal pengajuan');
                    }
                    // echo "<pre>";print_r($this->postData);die();
                    $konfirmasi = $this->Usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'],$renker['rencana_kerja_id'],$renker['flow_id'], $pesanan_id,SessionManagerWeb::getUserID());
                    if($konfirmasi){
                        if($this->postData['confirm'] == 0){
                            $this->model->minFlow($pesanan_id, $renker['flow_id']);
                            $this->Notification_model->generate(Notification_model::ACTION_REJECT_JADWAL_KUSEN, $pesanan_id,SessionManagerWeb::getUserID());
                        }else{
                            $this->model->addFlow($pesanan_id, $renker['flow_id']);
                            $this->Notification_model->generate(Notification_model::ACTION_CONFIRM_JADWAL_KUSEN, $pesanan_id,SessionManagerWeb::getUserID());
                        }
                        SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');
            
                    }else{
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan konfirmasi');
                    }
                }
            break;
            case 4:
                if((SessionManagerWeb::getRole() == Role::PEMASANG && SessionManagerWeb::getUserID() == $renker['mitra_pemasang_kusen']) || SessionManagerWeb::getRole() == Role::PRODUKSI){
                    $this->model->addFlow($pesanan_id, $renker['flow_id']);
                    $this->model->setJatuhTempo($pesanan_id);
                    $this->Notification_model->generate(Notification_model::ACTION_DONE_JADWAL_KUSEN, $pesanan_id,SessionManagerWeb::getUserID());
                    SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');
                }
                elseif(SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR){
                    if(!$this->postData['jadwal_usulan']){
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan reschedule, masukkan tanggal pengajuan');
                    }
                    $konfirmasi = $this->Usulan_model->konfirmasi(0,$this->postData['jadwal_usulan'],$this->postData['jam_usulan'],$renker['rencana_kerja_id'],$renker['flow_id'], $pesanan_id,SessionManagerWeb::getUserID(),true);
                    $this->model->minFlow($pesanan_id, $renker['flow_id'], 2);
                    $this->Notification_model->generate(Notification_model::ACTION_RESCHEDULE_JADWAL_KUSEN, $pesanan_id,SessionManagerWeb::getUserID());
                    SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan reschedule');
                }
            break;
            case 5:
                if(SessionManagerWeb::getRole() == Role::PRODUKSI  || SessionManagerWeb::getUserID() == $renker['mitra_pemasang_finish']){
                    $usulan_exist = dbGetRow("select id,jadwal_usulan from usulan where pesanan_id=".$pesanan_id." and flow_id=".$renker['flow_id']." and status='P'");
                    if(!$usulan_exist && SessionManagerWeb::getRole() == Role::PRODUKSI){ // usulan baru                    
                        if($this->postData['jadwal_usulan']){
                            $usulan_id = $this->Usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $pesanan_id, SessionManagerWeb::getUserID());
                            if($usulan_id){
                                $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $pesanan_id);
                                $this->model->addFlow($pesanan_id, $renker['flow_id']);
                                $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $pesanan_id,SessionManagerWeb::getUserID());
                                SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan pengajuan');
                            }else{
                                SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan');
                            }
                            
                        }else{
                            $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $pesanan_id);
                            $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $pesanan_id,SessionManagerWeb::getUserID());
                            SessionManagerWeb::setFlashMsg(true,'Berhasil set mitra pemasang');
                        }




                        // $usulan_id = $this->Usulan_model->create($this->postData['jadwal_usulan'], $this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $pesanan_id, SessionManagerWeb::getUserID());
                        // if($usulan_id){
                        //     $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $pesanan_id);
                        //     $this->model->addFlow($pesanan_id, $renker['flow_id']);
                        //     $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $pesanan_id,SessionManagerWeb::getUserID());
                        //     SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan pengajuan');
                        // }else{
                        //     SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan');
                        // }
                    }
                    else if(!$usulan_exist && SessionManagerWeb::getUserID() == $renker['mitra_pemasang_finish']){ // diisi oleh mitra pemasang
                        $usulan_id = $this->Usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $pesanan_id, SessionManagerWeb::getUserID());
                        if($usulan_id){
                            $this->model->addFlow($pesanan_id, $renker['flow_id']);
                            $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $pesanan_id,SessionManagerWeb::getUserID());
                            SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan pengajuan');
                        }else{
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan');
                        }
                    }
                    else{ // konfirmasi dari penolakan dan juga reschedule
                        if($this->postData['confirm'] == 0 && !$this->postData['jadwal_usulan']){
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan, masukkan tanggal pengajuan');
                        }
                        $konfirmasi = $this->Usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'] , $renker['rencana_kerja_id'],$renker['flow_id'], $pesanan_id,SessionManagerWeb::getUserID(), false , 5);
                        if($konfirmasi){
                            if($this->postData['confirm'] == 0){
                                $this->model->addFlow($pesanan_id, $renker['flow_id']);
                                $this->Notification_model->generate(Notification_model::ACTION_REJECT_REVISI_FINISH, $pesanan_id,SessionManagerWeb::getUserID());
                            }else{
                                $this->model->addFlow($pesanan_id, $renker['flow_id'], 2);
                                $this->Notification_model->generate(Notification_model::ACTION_CONFIRM_REVISI_FINISH, $pesanan_id,SessionManagerWeb::getUserID());
                            }
                            SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');
                        }else{
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan konfirmasi');
                        }
                    }
                }
            break;
            case 6:
                if(SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
                    // postData: confirm, jadwal_usulan(jika ada)
                    if($this->postData['confirm']==0 && !($this->postData['jadwal_usulan'])){
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan, masukkan tanggal pengajuan');
                    }
                    $konfirmasi = $this->Usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'] , $renker['rencana_kerja_id'],$renker['flow_id'], $pesanan_id,SessionManagerWeb::getUserID());
                    if($konfirmasi){
                        if($this->postData['confirm'] == 0){
                            $this->model->minFlow($pesanan_id, $renker['flow_id']);
                            $this->Notification_model->generate(Notification_model::ACTION_REJECT_JADWAL_FINISH, $pesanan_id,SessionManagerWeb::getUserID());
                        }else{
                            $this->model->addFlow($pesanan_id, $renker['flow_id']);
                            $this->Notification_model->generate(Notification_model::ACTION_CONFIRM_JADWAL_FINISH, $pesanan_id,SessionManagerWeb::getUserID());
                        }
                        
                        SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');
                    }else{
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan konfirmasi');
                    }
                }
            break;
            case 7:
                if((SessionManagerWeb::getRole() == Role::PEMASANG && SessionManagerWeb::getUserID() == $renker['mitra_pemasang_finish']) || SessionManagerWeb::getRole() == Role::PRODUKSI){
                    if($this->postData['adaFoto']<=0){
                        SessionManagerWeb::setFlashMsg(false,'Wajib Menambahkan foto');
                        redirect('web/pesanan/setselesaiproses/'.$pesanan_id);
                    }else{
                        $this->model->addFlow($pesanan_id, $renker['flow_id']);
                        $this->model->setTglAcc($pesanan_id);
                        if (!$this->_moveFileTemp($pesanan_id, 'PF')) {
                            $this->model->minFlow($pesanan_id, 8);
                            dbUpdate("pesanan", array("tgl_acc_konsumen" => null), "id=".$pesanan_id);
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan konfirmasi');
                        }
                        $this->Notification_model->generate(Notification_model::ACTION_DONE_JADWAL_FINISH, $pesanan_id,SessionManagerWeb::getUserID());

                        SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');

                    }
                    
                }
                elseif(SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
                    if(!$this->postData['jadwal_usulan']){
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan reschedule, masukkan tanggal pengajuan');
                    }
                    $konfirmasi = $this->Usulan_model->konfirmasi(0,$this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'],$renker['flow_id'], $pesanan_id,SessionManagerWeb::getUserID(),true);
                    $this->model->minFlow($pesanan_id, $renker['flow_id'], 2);
                    $this->Notification_model->generate(Notification_model::ACTION_RESCHEDULE_JADWAL_FINISH, $pesanan_id,SessionManagerWeb::getUserID());
                    SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan reschedule');
                }
            break;
            
        }

        if($pesanan_id || $usulan_id){
            if($this->postData['back']){
                $exp = explode('/', $this->postData['back']);
                if($exp[1] == 'detail'){
                    redirect('web/pesanan/'.$exp[1].'/'.$pesanan_id);
                }else{
                    redirect('web/pesanan/daftarorder');
                }
                
            }else{
                redirect('web/pesanan/daftarorder');
            }
            
        }
        else{
            redirect('');
        }
    }

    public function history_konsumen($reset=0) {
        if($reset==1){
            $_SESSION['start_history_konsumen']['bulan'] = date('m');
            $_SESSION['start_history_konsumen']['tahun'] = date('Y');
            redirect('web/pesanan/history_konsumen');
        }
        // $this->data['bulan'] = $_SESSION['start_history_konsumen']['bulan'];
        // $this->data['tahun'] = $_SESSION['start_history_konsumen']['tahun'];

        $this->data['kode_order'] = $this->postData['kode_order']; 
        $this->data['nama_konsumen'] = $this->postData['nama_konsumen']; 
        $this->data['alamat_konsumen'] = $this->postData['alamat_konsumen']; 
        $this->data['no_hp_konsumen'] = $this->postData['no_hp_konsumen']; 
        $this->data['bulan'] = $this->postData['bulan']; 
        $this->data['tahun'] = $this->postData['tahun']; 
        // echo $this->data['bulan'];die();
        // $this->data = $this->model->getListHutangProduksi($bulan, $tahun);

        $this->template->viewDefault($this->view,$this->data);

    }

    // Mendapatkan data KPI Bulanan
    public function getKPIBulanan() {

        $this->template->viewDefault('pesanan_getkpibulanan',$this->data);

    }

    public function ajaxgetkpibulanan() {

        $tahun = $this->postData['tahun'];
        $rows = $this->model->getKPIBulanan($tahun);

        echo json_encode($rows);
    }

    public function addPembukuan(){
        $this->load->model('Konsumen_model');
        $this->load->model('Jenis_pembayaran_model');

        $this->data['listkonsumen'] = $this->Konsumen_model->getKonsumenPengajuan();
        $this->data['listjenis'] = $this->Jenis_pembayaran_model->getJenis();

        // echo '<pre>';var_dump($this->data);die();

        $this->template->viewDefault($this->view,$this->data);   
    }

    public function createPembukuan() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $this->load->model('Pembayaran_log_model');

        $data = $this->postData;

        // echo "<pre>";var_dump($data);die();
        $insert = $this->Pembayaran_log_model->create($data, SessionManagerWeb::getUserID());
        
        if ($insert) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil menambahkan Pembukuan');
            redirect("web/pesanan/getkpibulanan");
            
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal menambahkan Pembukuan');
            redirect("web/pesanan/addpembukuan");
        }
    }

    function removeImagePesanan(){
        $data = $this->postData;
        $pesanan_id = $data['pesanan_id'];
        $namaFile = $data['nama_file'];
        $hashId = $data['id_hash'];
        $tipe = 'OM';
        
        if ($data['tipe'] == null) {
            $tipe = 'OM';
        } else {
            $tipe = $data['tipe'];
        }
        
        switch ($tipe) {
            case 'FA':
                $folder = 'foto_after/photos/';
                break;
            case 'EK':
                $folder = 'edit_katalog/photos/';
                break;
            case 'OM':
                $folder = 'pesanan/photos/';
                break;
            case 'PF':
                $folder = 'pesanan/photos/';
                break;
        }
        
        if ($folder == null) {
            $folder = 'pesanan/photos/';
        }

        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'].$folder;
        // echo 'original-'.$hashId.'-'.$namaFile;die();
        if($namaFile){
            if ($this->model->deletePesananImage($pesanan_id, $namaFile)) {
                $file_name_org = 'original-'.$hashId.'-'.$namaFile;
                $file_name_med = 'medium-'.$hashId.'-'.$namaFile;
                $file_name_thm = 'thumb-'.$hashId.'-'.$namaFile;
                
                self::_deleteFiles($path.$file_name_org);
                self::_deleteFiles($path.$file_name_med);
                self::_deleteFiles($path.$file_name_thm);
                echo 'Berhasil menghapus gambar' ;
            } else {
                echo 'Gagal menghapus gambar';
                // SessionManagerWeb::setFlashMsg(false, 'Gagal menghapus gambar');
            }
        } else {
            echo 'Gagal menghapus gambar';
        }
    }

    // ADDITIONAL FEATURE : 1 JULI 2020
    function saveEditGambar($pesanan_id){
        // $hashId = $this->postData['hashId'];
        if ($this->_moveFileTempRev($pesanan_id)) {
            $this->load->model('Notification_model');
            $this->Notification_model->generate(Notification_model::ACTION_REVISI_GAMBAR_KERJA, $pesanan_id,SessionManagerWeb::getUserID());
            SessionManagerWeb::setFlashMsg(true, 'Berhasil simpan file');
            // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil simpan file"));
        } else {
            SessionManagerWeb::setFlashMsg(false, 'Gagal simpan file');
            // $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal simpan file'));
        }
        redirect('web/pesanan/detail/'.$pesanan_id);
    }
    
    // ADDITIONAL FEATURE : 2 JULI 2020
    function _moveFileTempRev($post_id, $tipe='OM', $folder_source='pesanan/temp') {
        $id = md5(SessionManagerWeb::getUserID(). $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $files = glob($path_source.'*'); 
            $arr_image = array();
            $arr_files = array();
            foreach ($files as $file) {
                list($mime,$ext) = explode('/',Image::getMime($file));
                
                $basename = basename($file);
                
                // // Rename
                // $pieces = explode("-", $basename);
                // $basename = $pieces[0]."-".$id."-".$pieces[2].'.jpg';
                
                if ($mime=='image') {
                    $folder_dest = 'pesanan/photos';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $image = Image::getName($basename);
                    if (!in_array($image, $arr_image)) {
                        $arr_image[] = $image;
                        if (!$this->model->insertPesananImage($post_id, $image, $file,$tipe, SessionManagerWeb::getUserID())) {
                            return false;
                        }
                    }
                }
            
                if (!rename($file, $path_dest . '/' . $basename)) {
                    return false;
                }
            }
        }
        else {
            return true;
        }
        return $this->_deleteFiles($path_source);
    }

    public function uploadAfter($pesanan_id){
        $this->data['pesanan_id'] = $pesanan_id;
        $this->data['detail'] = $this->model->getDetailPesanan($pesanan_id);
        $this->template->viewDefault($this->view,$this->data); 
    }

    function saveFotoAfter($pesanan_id){
        $data = $this->postData;
        
        $this->updateLinkPesanan($pesanan_id, $data['link']);
        
        $folder_source = 'foto_after';
        $folder_dest = 'foto_after/photos';
        $tipe = 'FA';
        $this->_moveFileTemp($pesanan_id, $tipe, $folder_source);
        
        $folder_source = 'edit_katalog';
        $folder_dest = 'edit_katalog/photos';
        $tipe = 'EK';
        if ($this->_moveFileTemp($pesanan_id, $tipe, $folder_source)) {
            // $this->load->model('Notification_model');
            // $this->Notification_model->generate(Notification_model::ACTION_FOTO_AFTER, $pesanan_id, $this->user['id']);
            SessionManagerWeb::setFlashMsg(true, 'Berhasil simpan file');
            redirect('web/pesanan/detail/'.$pesanan_id);
            // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil simpan file"));
        } else {
            SessionManagerWeb::setFlashMsg(false, 'Gagal simpan file');
             redirect('web/pesanan/uploadafter/'.$pesanan_id);
        }
    }

    // Untuk update link pada pesanan
    function updateLinkPesanan($pesanan_id, $link) {
        $this->model->updateLinkPesanan($pesanan_id, $link);
    }

    public function sendImageTemp() {
        $data = $this->postData;
        if ($_GET['directory'] != null) {
            $folder = $_GET['directory'].'/temp';
        } else {
            $folder = 'pesanan/temp';
        }

        if ($_FILES) {
            $_FILES['gambar'] = $_FILES['file'];
            $images_arr = array();

            $id = md5(SessionManagerWeb::getUserID(). $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }
            // echo $path;
            $name = $_FILES['gambar']['name'];

            $file_name = Image::getFileName($id, Image::IMAGE_ORIGINAL, $name);
            unlink($path . $file_name);
            
            $file_mime = Image::getMime($folder.$file_name);
            $image_mime = explode('/',$file_mime);
            if ($image_mime[0]=='image') {
                Image::upload('gambar', $id, $name, $folder);
                $link = Image::generateLink($id, $name, $folder);
                $images_arr[] = $link['thumb'];
            }
            // kalau mau responsenya list dari 5 file image yang diconvert 
            // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Success"));
        } 
    }

    function getKonsumenDealPengajuan() {
        // $data = $this->postData;
        // $this->load->model('Konsumen_model');
        // $this->data = $this->Konsumen_model->getKonsumenDealPengajuan($data['keyword']);
        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->data);
    }

    function edit($pesanan_id){
        // $data = $this->input->post();
        $update = $this->model->edit($this->postData, $pesanan_id);
        if($update){
            SessionManagerWeb::setFlashMsg(true, "Berhasil update data");
        }else{
            SessionManagerWeb::setFlashMsg(false, "Gagal update data");
        }
        redirect('web/pesanan/detail/'.$pesanan_id);

    }

}