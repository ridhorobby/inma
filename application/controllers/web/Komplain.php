<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Komplain extends WEB_Controller {
	
	
    /**
	 * Halaman login user
	 */
    public function index() {
        redirect('web/komplain/komplainbaru');
    }

    public function komplainBaru(){
        if(SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            if(SessionManagerWeb::getRole() == Role::MITRA_MARKETING) {
                $where .=" and k.user_id=".SessionManagerWeb::getUserID();
            }
            $this->data['data'] = $this->model->filter($where)->getKomplainBaru($this->page);
            $this->data['need_action'] = self::getNeedAction($this->postData);
            $this->data['in_progress'] = self::getInProgress($this->postData);
            $this->data['filter'] = $this->postData;
            // echo "<pre>";print_r($this->data);die(); 
            // SessionManagerWeb::setFlashMsg(true, 'Berhasil mendapatkan data');
            // $this->load->view('web/komplain_komplainbaru',$this->data);
            // echo "<pre>";print_r($this->data['need_action']);die();
            $this->template->viewDefault($this->view,$this->data);

        }
        else{
            SessionManagerWeb::setFlashMsg(false, 'Role anda tidak bisa mengakses fungsi ini');
            redirect('web/pesanan');
        }

    }

    public function usulkanPemasangan($komplain_id) {
        
        if(SessionManagerWeb::getRole() == Role::PRODUKSI){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            $this->load->model('User_model');
            $this->data['mitra_pemasang'] = $this->User_model->column('*')->filter("where role='".Role::PEMASANG."'")->getUser();
            $this->data['komplain_baru'] = $this->model->getKomplainBaru(0);
            
            $this->data['detail'] = $detail = $this->model->getDetailKomplain($komplain_id);

            if($detail['flow_id']==2){
                $this->data['user_mitra_pemasang'] = $this->User_model->column('*')->filter("where id='".$detail['mitra_pemasang_kusen']."'")->getUserById();
            }elseif($detail['flow_id']==5){
                $this->data['user_mitra_pemasang'] = $this->User_model->column('*')->filter("where id='".$detail['mitra_pemasang_finish']."'")->getUserById();
            }else{
                $this->data['user_mitra_pemasang'] = null;
            }

            // echo "<pre>";print_r($this->data['detail']);die();
            $this->template->viewDefault($this->view,$this->data);
        }
        else{
            $detail = $this->model->getDetailKomplain($komplain_id);
            // echo "<pre>";print_r($detail);die();
            if(($detail['flow_id']==2 && $detail['tanggal_pasang_kusen']==null && $detail['mitra_pemasang_kusen'] == SessionManagerWeb::getUserID()) or
               ($detail['flow_id']==5 && $detail['tanggal_pasang_finish']==null && $detail['mitra_pemasang_finish'] == SessionManagerWeb::getUserID())
            ){
                $this->data['detail'] = $detail;
                $back = explode('/', $_SERVER['HTTP_REFERER']);
                $this->data['back'] = $back[5].'/'.$back[6];

                $this->template->viewDefault($this->view,$this->data);
            }
        }
        // $this->load->view('web/pesanan_index',$this->data);
    }

    public function getInProgress($postData=null){
        $where = "where 1 ";
        if($postData['filterExist']!=null){
            $where .= $this->getFilter($postData);
        } 
        if(SessionManagerWeb::getRole() == Role::PRODUKSI){
            $column = "k.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan, us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and k.flow_id in(3,4,6,7)";
        }
        elseif(SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
            $where .= "and k.flow_id in (4,7)";
            if(SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
                $where .= "and k.user_id=".SessionManagerWeb::getUserID();
            }
        }
        // NOTE : AGAR DAPAT MELIHAT SEMUA STATUS PESANAN
        elseif(SessionManagerWeb::getRole() == Role::DIREKTUR){
            $where .= "and k.flow_id in (2,4,5,7)";
        }
        elseif(SessionManagerWeb::getRole() == Role::PEMASANG){
            $where .= "and k.flow_id=5";
        }
        $in_progress = $this->model->column($column)->filter($where)->join($join)->getKomplains('unl');

        return $in_progress;
    }

    public function getNeedAction($postData =null){
        $where = "where 1 ";
        if($postData['filterExist']!=null){
            $where .= $this->getFilter($postData);
        }
        if(SessionManagerWeb::getRole() == Role::PRODUKSI){
            // $column = "k.* , u.jadwal_usulan as jadwal_usulan, us.name as nama_pengusul, us.role as role_pengusul,usl.id as usulan_exist"; // ORIGINAL
            // $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
            //          left join komplain_usulan usl on k.id = usl.komplain_id
            //          left join users us on u.user_id = us.id
            // "; // ORIGINAL
            // $where .= "and k.flow_id in(2,5) group by k.id"; // ORIGINAL
            
            $column = "k.* , u.jadwal_usulan as jadwal_usulan, u.jam_usulan as jam_usulan, us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and k.flow_id in(2,5) and mitra_pemasang_kusen <> 0 ";
        }
        elseif(SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
            $column = "k.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan,  us.name as nama_pengusul, us.role as role_pengusul,
                      (select konsumen_id from pesanan where id=k.pesanan_id) as konsumen_id
            ";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and k.flow_id in (3,6)";
            if(SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
                $where .= "and k.user_id=".SessionManagerWeb::getUserID();
            }
        }
        elseif(SessionManagerWeb::getRole() == Role::PEMASANG){
            // $where .= "and k.flow_id in (4,7)"; // ORIGINAL
            // $where .= "and k.flow_id in (4,7) and (k.mitra_pemasang_kusen=".SessionManagerWeb::getUserID()." or k.mitra_pemasang_finish=".SessionManagerWeb::getUserID().")";

            $column = "k.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan , us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id";
            $where .= "and (
                                (k.flow_id in (4,7)  and (k.mitra_pemasang_kusen=".SessionManagerWeb::getUserID()." or k.mitra_pemasang_finish=".SessionManagerWeb::getUserID().")) or 
                               (k.flow_id = 2 and tanggal_pasang_kusen is null and k.mitra_pemasang_kusen=".SessionManagerWeb::getUserID().") or
                               (k.flow_id = 5 and tanggal_pasang_kusen is not null and tanggal_pasang_finish is null and k.mitra_pemasang_finish=".SessionManagerWeb::getUserID().")
                           ) 
                        ";
        }
        $need_action = $this->model->column($column)->join($join)->filter($where)->getKomplains('unl');
        return $need_action;
    }

    public function getFilter($data){
        $filter = '';
        if($data['kode_order']){
            $kode_order = strtolower($data['kode_order']);
            $filter .= " and lower(k.kode_order) like '%$kode_order%' ";
        }
        if($data['nama_konsumen']){
            $nama_konsumen = strtolower($data['nama_konsumen']);
            $filter .= " and lower(k.nama_konsumen) like '%$nama_konsumen%' ";
        }
        if($data['alamat_konsumen']){
            $alamat_konsumen = strtolower($data['alamat_konsumen']);
            $filter .= " and lower(k.alamat_konsumen) like '%$alamat_konsumen%' ";
        }
        if($data['no_hp_konsumen']){
            $no_hp_konsumen = strtolower($data['no_hp_konsumen']);
            $filter .= " and lower(k.no_hp_konsumen) like '%$no_hp_konsumen%' ";
        }
        return $filter;
    }

    public function detail($komplain_id, $notification_user_id=null){
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($notification_user_id);
        
        $data = $this->model->getDetailKomplain($komplain_id);
        $dataFotoOrder = $this->model->getPhoto($komplain_id);
        
        $id = md5($dataFotoOrder[0]['user_id'] . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = base_url().'/assets/uploads/komplain/photos/';

        foreach ($dataFotoOrder as $fotoOrder) {
            $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
            if($fotoOrder['tipe'] == 'OM'){
                $file_order[] = $path.$file_name;
            }else{
                $file_acc[] = $path.$file_name;
            }
            
        }
        $data['foto_order'] = $file_order;
        $data['foto_acc'] = $file_acc;

        $this->data['detail'] = $data;

        // echo "<pre>";print_r($data);die();
        $this->template->viewDefault($this->view,$this->data);
        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->data);
    }

    public function create() {
        if(SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::PEMASANG){
            SessionManagerWeb::setFlashMsg(false, "Anda tidak bisa mengakses fungsi ini");
        }
        
        $data = $this->postData;

        if($data['adaFoto'] <= 0){
            SessionManagerWeb::setFlashMsg(false, "Silahkan Masukkan Foto / Gambar");
            redirect("web/komplain/add");
        }
        // echo "<pre>";print_r($data);die();
        // $temp['user_id'] = $data['user_id'];

        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
            // $users = $this->User_model->getUser($data['user_id']);
        }


        /// CHECK KODE ORDER IS EXIST
        $this->load->model('Pesanan_model');
        $checkOrder = $this->Pesanan_model->get_by('kode_order', $data['kode_order']);
        // echo "<pre>";print_r($checkOrder);die();
        if(!$checkOrder){
            $this->load->model('Konsumen_model');
            $dataKonsumen = array(
                'nama'      => $data['nama'],
                'alamat'    => $data['alamat'],
                'no_hp'     => $data['no_hp']    
            );
            $data['konsumen_id'] = $this->Konsumen_model->create($dataKonsumen);
            $data['tgl_order_masuk'] = $data['tgl_komplain'];
            $data['tipe'] = 'K';
            $data['status_produksi'] = '3';
            $order_id = $this->Pesanan_model->create($data, SessionManagerWeb::getUserID());
            $updateSelesai = $this->Pesanan_model->setSelesai($order_id);
        }else{
            $order_id = $checkOrder['id'];
        }
        if($order_id){
            $data['pesanan_id'] = $order_id;
            $insert = $this->model->create($data, SessionManagerWeb::getUserID());
            
            if ($insert) {
                if (!$this->_moveFileTemp($insert, 'OM','komplain')) {
                    $hapus = dbQuery("delete from komplain where id=".$insert);
                    SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Komplain');
                }
                $this->load->model('Notification_model');
                $this->Notification_model->komplainGenerate(Notification_model::ACTION_KOMPLAIN_CREATE, $insert,SessionManagerWeb::getUserID());
                SessionManagerWeb::setFlashMsg(true, 'Berhasil Membuat Komplain');
            } 
            else {
                $validation = $this->model->getErrorValidate();
                
                // echo $validation;die();
                if (empty($validation)) {
                    SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Komplain');
                } else {
                    SessionManagerWeb::setFlashMsg(false, $validation);
                }
            }
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Komplain');
        }
        redirect("web/komplain/komplainbaru");

    }

    public function komplainDeleted(){
        if(SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::PRODUKSI){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            $this->data['komplain_deleted'] = $this->model->filter($where)->getKomplainDeleted($this->page);
            // echo "<pre>";print_r($this->data['komplain_deleted']);die();
        }
        SessionManagerWeb::setFlashMsg(false, 'Role anda tidak bisa mengakses fungsi ini');
        redirect('web/pesanan');
    }

    public function usulan($komplain_id){
        $this->load->model('Komplain_usulan_model');
        $this->load->model('Notification_model');
        $renker = dbGetRow("select flow_id,rencana_kerja_id, mitra_pemasang_kusen, mitra_pemasang_finish from komplain_view where id=".$komplain_id);
        // echo "<pre>";print_r($renker);die();
        switch ($renker['flow_id']) {
            case 2:
                if(SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getUserID() == $renker['mitra_pemasang_kusen']){
                    $usulan_exist = dbGetRow("select id,jadwal_usulan from komplain_usulan where komplain_id=".$komplain_id." and flow_id=".$renker['flow_id']." and status='P'");
                    if(!$usulan_exist && SessionManagerWeb::getRole() == Role::PRODUKSI){ // pengajuan pesanan_baru
                        if($this->postData['jadwal_usulan']){
                            $usulan_id = $this->Komplain_usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'],  $renker['rencana_kerja_id'], $renker['flow_id'], $komplain_id, SessionManagerWeb::getUserID());
                            if($usulan_id){
                                $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $komplain_id);
                                $this->model->addFlow($komplain_id, $renker['flow_id']);
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $komplain_id,SessionManagerWeb::getUserID());
                                SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan pengajuan');
                            }else{
                                SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan');
                            }
                        }else{
                            $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $komplain_id);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $komplain_id,SessionManagerWeb::getUserID());
                            SessionManagerWeb::setFlashMsg(true,'Berhasil set mitra pemasang');
                        }
                    }
                    // if(!$usulan_exist){   
                        
                    //     if($usulan_id){
                    //         $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $komplain_id);
                    //         $this->model->addFlow($komplain_id, $renker['flow_id']);
                            
                    //         SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan pengajuan');
                    //     }else{
                    //         SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan');
                    //     }
                        
                    // }
                    else if(!$usulan_exist && SessionManagerWeb::getUserID() == $renker['mitra_pemasang_kusen']){ // diisi oleh mitra pemasang'
                        $usulan_id = $this->Komplain_usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $komplain_id, SessionManagerWeb::getUserID());
                        if($usulan_id){
                            $this->model->addFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $komplain,SessionManagerWeb::getUserID());
                            SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan pengajuan');
                        }else{
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan');
                        }
                    }
                    else{ // konformasi penolakan
                        if($this->postData['confirm'] == 0 && !$this->postData['jadwal_usulan']){
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan, masukkan tanggal pengajuan');
                        }
                        $konfirmasi = $this->Komplain_usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,SessionManagerWeb::getUserID(),false, 2);
                        if($konfirmasi){
                            if($this->postData['confirm'] == 0){
                                $this->model->addFlow($komplain_id, $renker['flow_id']);
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_REJECT_REVISI_KUSEN, $komplain_id,SessionManagerWeb::getUserID());
                            }else{
                                $this->model->addFlow($komplain_id, $renker['flow_id'], 2);
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_CONFIRM_REVISI_KUSEN, $komplain_id,SessionManagerWeb::getUserID());
                            }
                            SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');
                        }else{
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan konfirmasi');
                        }
                    }
                }
            break;
            case 3:
                if(SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
                    // postData: confirm, jadwal_usulan(jika ada)
                    if($this->postData['confirm']==0 && !($this->postData['jadwal_usulan'])){
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan, masukkan tanggal pengajuan');
                    }
                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,SessionManagerWeb::getUserID());
                    if($konfirmasi){
                        if($this->postData['confirm'] == 0){
                            $this->model->minFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_REJECT_JADWAL_KUSEN, $komplain_id,SessionManagerWeb::getUserID());
                        }else{
                            $this->model->addFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_CONFIRM_JADWAL_KUSEN, $komplain_id,SessionManagerWeb::getUserID());
                        }
                        SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');
                    }else{
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan konfirmasi');
                    }
                }
            break;
            case 4:
                if((SessionManagerWeb::getRole() == Role::PEMASANG && SessionManagerWeb::getUserID() == $renker['mitra_pemasang_kusen']) || SessionManagerWeb::getRole() == Role::PRODUKSI){
                    if($this->postData['konfirmasi_selesai'] == 1){
                        $this->model->addFlow($komplain_id, $renker['flow_id'],4);
                        $this->model->setTglAcc($komplain_id);
                        $this->model->setJatuhTempo($komplain_id);
                        $this->Notification_model->komplainGenerate(Notification_model::ACTION_DONE_JADWAL_FINISH, $komplain_id,SessionManagerWeb::getUserID());
                    }else{
                        $this->model->addFlow($komplain_id, $renker['flow_id']);
                        $this->Notification_model->komplainGenerate(Notification_model::ACTION_DONE_JADWAL_KUSEN, $komplain_id,SessionManagerWeb::getUserID());
                    }
                    SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');
                }
                elseif(SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR){
                    if(!$this->postData['jadwal_usulan']){
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan reschedule, masukkan tanggal pengajuan');
                    }
                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi(0,$this->postData['jadwal_usulan'],$this->postData['jam_usulan'],$renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,SessionManagerWeb::getUserID(),true);
                    $this->model->minFlow($komplain_id, $renker['flow_id'], 2);
                    $this->Notification_model->komplainGenerate(Notification_model::ACTION_RESCHEDULE_JADWAL_KUSEN, $konfirmasi,SessionManagerWeb::getUserID());
                    SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan reschedule');
                }
            break;
            case 5:
                if(SessionManagerWeb::getRole() == Role::PRODUKSI  || SessionManagerWeb::getUserID() == $renker['mitra_pemasang_finish']){
                    $usulan_exist = dbGetOne("select id from komplain_usulan where komplain_id=".$komplain_id." and flow_id=".$renker['flow_id']." and status='P'");
                    if(!$usulan_exist  && SessionManagerWeb::getRole() == Role::PRODUKSI){ // usulan baru                    
                        if($this->postData['jadwal_usulan']){
                            $usulan_id = $this->Komplain_usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'],  $renker['rencana_kerja_id'], $renker['flow_id'], $komplain_id, SessionManagerWeb::getUserID());
                            if($usulan_id){
                                $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $komplain_id);
                                $this->model->addFlow($komplain_id, $renker['flow_id']);
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $komplain_id,SessionManagerWeb::getUserID());
                                SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan pengajuan');
                            }else{
                                SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan');
                            }
                        }else{
                            $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $komplain_id);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $komplain_id,SessionManagerWeb::getUserID());
                            SessionManagerWeb::setFlashMsg(true,'Berhasil set mitra pemasang');
                        }
                    }

                    else if(!$usulan_exist && SessionManagerWeb::getUserID() == $renker['mitra_pemasang_finish']){ // diisi oleh mitra pemasang
                        $usulan_id = $this->Komplain_usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $komplain_id, SessionManagerWeb::getUserID());
                        if($usulan_id){
                            $this->model->addFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $komplain_id,SessionManagerWeb::getUserID());
                            SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan pengajuan');
                        }else{
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan');
                        }
                    }


                    else{ // konfirmasi dari penolakan dan juga reschedule
                        if($this->postData['confirm'] == 0 && !$this->postData['jadwal_usulan']){
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan, masukkan tanggal pengajuan');
                        }
                        $konfirmasi = $this->Komplain_usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,SessionManagerWeb::getUserID(), false , 5);
                        if($konfirmasi){
                            if($this->postData['confirm'] == 0){
                                $this->model->addFlow($komplain_id, $renker['flow_id']);
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_REJECT_REVISI_FINISH, $komplain_id,SessionManagerWeb::getUserID());
                            }else{
                                $this->model->addFlow($komplain_id, $renker['flow_id'], 2);
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_CONFIRM_REVISI_FINISH, $komplain_id,SessionManagerWeb::getUserID());
                            }
                            SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');

                        }else{
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan konfirmasi');
                        }
                    }
                }
            break;
            case 6:
                if(SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
                    // postData: confirm, jadwal_usulan(jika ada)
                    if($this->postData['confirm']==0 && !($this->postData['jadwal_usulan'])){
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan, masukkan tanggal pengajuan');
                    }
                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,SessionManagerWeb::getUserID());
                    if($konfirmasi){
                        if($this->postData['confirm'] == 0){
                            $this->model->minFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_REJECT_JADWAL_FINISH, $komplain_id,SessionManagerWeb::getUserID());
                        }else{
                            $this->model->addFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_CONFIRM_JADWAL_FINISH, $komplain_id,SessionManagerWeb::getUserID());
                        }
                        SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');
                    }else{
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan konfirmasi');
                    }
                }
            break;
            case 7:
                if((SessionManagerWeb::getRole() == Role::PEMASANG && SessionManagerWeb::getUserID() == $renker['mitra_pemasang_finish']) || SessionManagerWeb::getRole() == Role::PRODUKSI){
                    if($this->postData['adaFoto']<=0){
                        SessionManagerWeb::setFlashMsg(false,'Wajib Menambahkan foto');
                        redirect('web/komplain/setselesaiproses/'.$komplain_id);
                    }else{
                        $this->model->addFlow($komplain_id, $renker['flow_id']);
                        $this->model->setTglAcc($komplain_id);
                        if (!$this->_moveFileTemp($komplain_id, 'PF')) {
                            $this->model->minFlow($komplain_id, 8);
                            dbUpdate("komplain", array("tgl_acc_konsumen" => null), "id=".$komplain_id);
                            SessionManagerWeb::setFlashMsg(false,'Gagal melakukan konfirmasi');
                        }
                        // echo "asdsadasd";die();
                        $this->Notification_model->komplainGenerate(Notification_model::ACTION_DONE_JADWAL_FINISH, $komplain_id,SessionManagerWeb::getUserID());

                        SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');
                    }
                    
                }
                elseif(SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR){
                    if(!$this->postData['jadwal_usulan']){
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan reschedule, masukkan tanggal pengajuan');
                    }
                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi(0,$this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,SessionManagerWeb::getUserID(),true);
                    $this->model->minFlow($komplain_id, $renker['flow_id'], 2);
                    $this->Notification_model->komplainGenerate(Notification_model::ACTION_RESCHEDULE_JADWAL_FINISH, $komplain_id,SessionManagerWeb::getUserID());
                    SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi reschedule');
                }
            break;
            
        }
        if($komplain_id || $usulan_id){
            if (SessionManagerWeb::getRole() != Role::PEMASANG) {
                redirect('web/komplain/komplainbaru');
            }else{
                if($this->postData['back']){
                    $exp = explode('/', $this->postData['back']);
                    if($exp[1] == 'detail'){
                        redirect('web/pesanan/'.$exp[1].'/'.$pesanan_id);
                    }else{
                        redirect('web/pesanan/daftarorder');
                    }
                    
                }else{
                    redirect('web/pesanan/daftarorder');
                }
            }
            
            
        }
        else{
            redirect('');
        }
        
    }

    public function add(){
        if(SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR ||  SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){
            $this->removeTemp('komplain',true);
            $this->load->model('Pesanan_model');
            $this->data['pesanan_baru'] = $this->Pesanan_model->getPesananBaru(0);
            // echo "<pre>";print_r($this->data['pesanan_baru']);die();
            $this->template->viewDefault($this->view,$this->data);

        }else{
            SessionManagerWeb::setFlashMsg(false, 'Anda tidak dapat mengakses fungsi ini');
            redirect($this->data['class']);
        }
    }

    public function tolakUsulkanPemasangan($komplain_id) {
        
        //if(SessionManagerWeb::getRole() != Role::PRODUKSI){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            $this->data['detail'] = $this->model->getDetailKomplain($komplain_id);
            $this->template->viewDefault($this->view,$this->data);
        //}
        // $this->load->view('web/pesanan_index',$this->data);
    }

    public function rescheduleJadwal($komplain_id) {
        
        if(SessionManagerWeb::getRole() != Role::PRODUKSI){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }

            $this->data['detail'] = $this->model->getDetailKomplain($komplain_id);
            // echo "<pre>";print_r($this->data['detail']);die();
            $this->template->viewDefault($this->view,$this->data);
        }
        // $this->load->view('web/pesanan_index',$this->data);
    }

    public function setselesaiproses($komplain_id) {
        $this->removeTemp('komplain',true);
        $this->data['komplain_id'] = $komplain_id;
        $this->template->viewDefault($this->view,$this->data);
    }

    public function edit($komplain_id){
        // $data = $this->input->post();
        $update = $this->model->edit($this->postData, $komplain_id);
        if($update){
            SessionManagerWeb::setFlashMsg(true, "Berhasil update data");
        }else{
            SessionManagerWeb::setFlashMsg(false, "Gagal update data");
        }
        redirect('web/komplain/detail/'.$komplain_id);

    }

}