<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
 
    class Site extends WEB_Controller {
		protected $ispublic = true;
		
        /**
		 * Halaman login user
		 */
        public function index() {
			// jika sudah login lempar ke post
            if(SessionManagerWeb::isAuthenticated()) {
                SessionManagerWeb::setFlashMsg(false,'Kamu sudah login');
                redirect($this->getCTL('pesanan/index'));
            }
            
            // lakukan validasi form
            $this->form_validation->set_rules('username','Username','required');
            $this->form_validation->set_rules('password','Password','required');
            
            if ($this->form_validation->run() === true) {
                list($ok,$msg,$user) = AuthManagerWeb::login($this->input->post('username'),$this->input->post('password'));
                
                if($ok) {
                    // simpan cookie
                    setcookie('username',$user['username'],time() + 2592000,'/'); // 30 hari
                    SessionManagerWeb::setUser($user);

                    redirect($this->getCTL('pesanan/index'));
                }
                else {
                    // echo $msg;die();
                    SessionManagerWeb::setFlash(array('errmsg' => array($msg)));
                    redirect($this->ctl);
                }
            }
            else {
                $errstr = validation_errors();
                if(!empty($errstr)) {
                    SessionManagerWeb::setFlash(array('errmsg' => array($errstr)));
                    redirect($this->ctl);
                }
            }

            $this->data['title'] = 'Inma Nasatech';
            // echo "<pre>";print_r($this->data);die();
            $this->load->view(static::VIEW_PATH.$this->view,$this->data);
        	
        }



        public function approve_penjadwalan_pesanan($konsumen_id=null, $message=null){
            $this->load->model('Pesanan_model');
            if($_SESSION["konsumen_auth"]){
                // echo "<pre>";print_r($_SESSION['konsumen_auth']);die();
                if($konsumen_id==null){
                    $this->data['konsumen_id'] = $konsumen_id;
                    $this->data['pesanan_id'] = null;
                    $this->data['srvok'] = false;
                    $this->data['srvmsg'] = 'Link tidak ditemukan';
                    SessionManagerWeb::setFlash(array('errmsg' => array('Link tidak ditemukan')));
                    $this->load->view(static::VIEW_PATH.$this->view,$this->data);
                    
                }
                else{
                    if($konsumen_id != $_SESSION["konsumen_auth"]['data']['konsumen_id']){
                        $this->data['konsumen_id'] = $_SESSION["konsumen_auth"]['data']['konsumen_id'];
                        $this->data['pesanan_id'] = $_SESSION["konsumen_auth"]['data']['id'];

                        $this->data['data'] = $this->Pesanan_model->getDetailPesananForKonsumen($_SESSION["konsumen_auth"]['data']['kode_order'],  $this->data['konsumen_id']);

                        $dataFotoOrder = $this->Pesanan_model->getPhoto($this->data['data']['id']);
            
                        
                        $ciConfig = $this->config->item('utils');
                        $path = base_url().'assets/uploads/pesanan/photos/';

                        foreach ($dataFotoOrder as $fotoOrder) {
                            $id = md5($fotoOrder['user_id'] . $this->config->item('encryption_key'));
                            $folder .= '/' . $id;
                            $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
                            if($fotoOrder['tipe'] == 'OM'){
                                $file_order[] = $path.$file_name;
                            }else{
                                $file_acc[] = $path.$file_name;
                            }
                            
                        }
                        $this->data['data']['foto_order'] = $file_order;
                        $this->data['data']['foto_acc'] = $file_acc;

                        
                        // $this->load->view(static::VIEW_PATH.$this->view,$this->data);
                        redirect('web/site/approve_penjadwalan_pesanan/'.$_SESSION["konsumen_auth"]['data']['konsumen_id'].'/1');
                    }else{
                        $this->data['konsumen_id'] = $konsumen_id;
                        $this->data['pesanan_id'] = $_SESSION["konsumen_auth"]['data']['id'];

                        $this->data['data'] = $_SESSION["konsumen_auth"]['data'];

                        $dataFotoOrder = $this->Pesanan_model->getPhoto($this->data['data']['id']);
            
                        
                        $ciConfig = $this->config->item('utils');
                        $path = base_url().'assets/uploads/pesanan/photos/';

                        foreach ($dataFotoOrder as $fotoOrder) {
                            $id = md5($fotoOrder['user_id'] . $this->config->item('encryption_key'));
                            $folder .= '/' . $id;
                            $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
                            if($fotoOrder['tipe'] == 'OM'){
                                $file_order[] = $path.$file_name;
                            }else{
                                $file_acc[] = $path.$file_name;
                            }
                            
                        }
                        $this->data['data']['foto_order'] = $file_order;
                        $this->data['data']['foto_acc'] = $file_acc;

                        // echo "<pre>";print_r($this->data);die();
                        // echo "asdasd";die();
                        // echo "<pre>";print_r($_SESSION["konsumen_auth"]);die();
                        
                        // $this->data['srvok'] = false;
                        // $this->data['srvmsg'] = 'Anda sudah verifikasi sebelumnya';
                        if($message==1){
                            $this->data['srvok'] = false;
                            $this->data['srvmsg'] = 'Anda masih memiliki session, silahkan logout terlebih dahulu';
                        }
                        $this->load->view(static::VIEW_PATH.$this->view,$this->data);
                    }
                    
                }
            }else{
                if($konsumen_id==null){
                SessionManagerWeb::setFlash(array('errmsg' => array('Link tidak ditemukan')));
                }
                else{
                    $this->load->model('Konsumen_model');
                    $konsumen_exist = $this->Konsumen_model->getRow($konsumen_id);
                    if($konsumen_exist == null){
                        SessionManagerWeb::setFlash(array('errmsg' => array('Link tidak ditemukan')));
                        redirect('web/site');
                    }
                    if($this->input->post('kode_order')){
                        $this->load->model('Pesanan_model');
                        $kode_order = $this->input->post('kode_order');
                        $data = $this->Pesanan_model->getDetailPesananForKonsumen($kode_order, $konsumen_id);
                        if(empty($data)){
                            SessionManagerWeb::setFlash(array('errmsg' => array('Kode Order tidak ditemukan')));
                            redirect('web/site/approve_penjadwalan_pesanan/'.$konsumen_id);
                        }else{
                            $this->data['pesanan_id'] = $data['id'];
                            $this->data['data'] = $data;

                            $dataFotoOrder = $this->Pesanan_model->getPhoto($data['id']);
                
                            
                            $ciConfig = $this->config->item('utils');
                            $path = base_url().'assets/uploads/pesanan/photos/';

                            foreach ($dataFotoOrder as $fotoOrder) {
                                $id = md5($fotoOrder['user_id'] . $this->config->item('encryption_key'));
                                $folder .= '/' . $id;
                                $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
                                if($fotoOrder['tipe'] == 'OM'){
                                    $file_order[] = $path.$file_name;
                                }else{
                                    $file_acc[] = $path.$file_name;
                                }
                                
                            }
                            $this->data['data']['foto_order'] = $file_order;
                            $this->data['data']['foto_acc'] = $file_acc;
                        
                        }
                        $this->data['konsumen_id'] = $konsumen_id;
                        $this->data['pesanan_id'] = $data['id'];

                        $_SESSION["konsumen_auth"]["exist"] = true;
                        $_SESSION["konsumen_auth"]["data"] = $data;
                        $_SESSION["konsumen_auth"]["type"] = 1;

                        redirect('web/site/approve_penjadwalan_pesanan/'.$konsumen_id);

                    }else{
                        $this->data['konsumen_id'] = $konsumen_id;
                        $this->data['pesanan_id'] = null;
                        // echo "asd";die();
                    }
                    
                }

                $this->load->view(static::VIEW_PATH.$this->view,$this->data);
            }
            
            
    
        }
    
        public function approve_penjadwalan_komplain($konsumen_id,$message=null){
            $this->load->model('Pesanan_model');
            $this->load->model('Komplain_model');
            if($_SESSION["konsumen_auth"]){
                if($konsumen_id==null){
                    $this->data['konsumen_id'] = $konsumen_id;
                    $this->data['komplain_id'] = null;
                    $this->data['srvok'] = false;
                    $this->data['srvmsg'] = 'Link tidak ditemukan';
                    SessionManagerWeb::setFlash(array('errmsg' => array('Link tidak ditemukan')));
                    $this->load->view(static::VIEW_PATH.$this->view,$this->data);
                    
                }
                else{
                    if($konsumen_id != $_SESSION["konsumen_auth"]['data']['konsumen_id']){
                        $this->data['konsumen_id'] = $_SESSION["konsumen_auth"]['data_pesanan']['konsumen_id'];


                        $data = $this->Pesanan_model->getDetailPesananForKonsumen($_SESSION["konsumen_auth"]['data_pesanan']['kode_order'], $konsumen_id);
                        if(empty($data)){
                            SessionManagerWeb::setFlash(array('errmsg' => array('Kode Order tidak ditemukan')));
                            $this->load->view(static::VIEW_PATH.$this->view,$this->data);
                        }else{
                            $this->data['pesanan_id'] = $data['id'];
                            $dataKomplain = $this->Komplain_model->getDetailKomplainByPesanan($data['id']);
                            $this->data['komplain_id'] = $dataKomplain['id'];
                            $dataFotoOrder = $this->Komplain_model->getPhoto($komplain_id);

                            $ciConfig = $this->config->item('utils');
                            $path = base_url().'/assets/uploads/komplain/photos/';

                            foreach ($dataFotoOrder as $fotoOrder) {
                                $id = md5($fotoOrder['user_id'] . $this->config->item('encryption_key'));
                                $folder .= '/' . $id;
                                $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
                                if($fotoOrder['tipe'] == 'OM'){
                                    $file_order[] = $path.$file_name;
                                }else{
                                    $file_acc[] = $path.$file_name;
                                }
                                
                            }
                            $dataKomplain['foto_order'] = $file_order;
                            $dataKomplain['foto_acc'] = $file_acc;

                            $this->data['data'] = $dataKomplain;
                            $this->data['data']['konsumen_id'] = $data['konsumen_id'];
                        }

                        // $this->data['data'] = $_SESSION["konsumen_auth"]['data'];

                        redirect('web/site/approve_penjadwalan_komplain/'.$this->data['data']['konsumen_id'].'/1');
                        // $this->load->view(static::VIEW_PATH.$this->view,$this->data);
                    }
                    else{
                        $this->data['konsumen_id'] = $konsumen_id;
                        $this->data['pesanan_id'] = $_SESSION["konsumen_auth"]['data']['id'];
                        $data = $this->Pesanan_model->getDetailPesananForKonsumen($_SESSION["konsumen_auth"]['data_pesanan']['kode_order'], $konsumen_id);
                        if(empty($data)){
                            SessionManagerWeb::setFlash(array('errmsg' => array('Kode Order tidak ditemukan')));
                            $this->load->view(static::VIEW_PATH.$this->view,$this->data);
                        }else{
                            $this->data['pesanan_id'] = $data['id'];
                            $dataKomplain = $_SESSION["konsumen_auth"]['data'];
                            $this->data['komplain_id'] = $dataKomplain['id'];
                            $dataFotoOrder = $this->Komplain_model->getPhoto($komplain_id);
                            
                            
                            $ciConfig = $this->config->item('utils');
                            $path = base_url().'/assets/uploads/komplain/photos/';

                            foreach ($dataFotoOrder as $fotoOrder) {
                                $id = md5($fotoOrder['user_id'] . $this->config->item('encryption_key'));
                                $folder .= '/' . $id;
                                $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
                                if($fotoOrder['tipe'] == 'OM'){
                                    $file_order[] = $path.$file_name;
                                }else{
                                    $file_acc[] = $path.$file_name;
                                }
                                
                            }
                            $dataKomplain['foto_order'] = $file_order;
                            $dataKomplain['foto_acc'] = $file_acc;

                            $this->data['data'] = $dataKomplain;
                            $this->data['data']['konsumen_id'] = $data['konsumen_id'];
                            
                            if($message==1){
                                $this->data['srvok'] = false;
                                $this->data['srvmsg'] = 'Anda masih memiliki session, silahkan logout terlebih dahulu';
                            }
                        
                            $this->load->view(static::VIEW_PATH.$this->view,$this->data);
                        }
                        
                    }
                }
            }
            else{
                if($konsumen_id==null){
                    SessionManagerWeb::setFlash(array('errmsg' => array('Link tidak ditemukan')));
                    redirect('web/site');
                }
                else{
                    $this->load->model('Konsumen_model');
                    $konsumen_exist = $this->Konsumen_model->getRow($konsumen_id);
                    if($konsumen_exist == null){
                        SessionManagerWeb::setFlash(array('errmsg' => array('Link tidak ditemukan')));
                        redirect('web/site');
                    }
                    if($this->input->post('kode_order')){
                        $this->load->model('Pesanan_model');
                        $this->load->model('Komplain_model');
                        $kode_order = $this->input->post('kode_order');
                        $data = $this->Pesanan_model->getDetailPesananForKonsumen($kode_order, $konsumen_id);
                        if(empty($data)){
                            // echo "soni";die();
                            SessionManagerWeb::setFlash(array('errmsg' => array('Kode Order tidak ditemukan')));
                            redirect('web/site/approve_penjadwalan_komplain/'.$konsumen_id);
                        }else{
                            $this->data['pesanan_id'] = $data['id'];
                            $dataKomplain = $this->Komplain_model->getDetailKomplainByPesanan($data['id']);
                            // echo "<Pre>";print_r($dataKomplain);die();
                            if(empty($dataKomplain)){
                                SessionManagerWeb::setFlash(array('errmsg' => array('Data Komplain tidak ditemukan')));
                                redirect('web/site/approve_penjadwalan_komplain/'.$konsumen_id);
                            }else{
                                $this->data['komplain_id'] = $dataKomplain['id'];
                                $this->data['data'] = $dataKomplain;
                            }

                            $_SESSION["konsumen_auth"]["exist"] = true;
                            $_SESSION["konsumen_auth"]["data"] = $dataKomplain;
                            $_SESSION["konsumen_auth"]["data"]["konsumen_id"] = $data['konsumen_id'];
                            $_SESSION["konsumen_auth"]["data_pesanan"] = $data;
                            $_SESSION["konsumen_auth"]["type"] = 2;
                            
                        }
                        $this->data['konsumen_id'] = $konsumen_id;
                        $komplain_id = $dataKomplain['id'];

                            // redirect('web/site/detailkomplainkonsumen/'.$komplain_id);
                        redirect('web/site/approve_penjadwalan_komplain/'.$konsumen_id);
                    }else{
                        $this->data['konsumen_id'] = $konsumen_id;
                        $this->data['komplain_id'] = null;
                        $this->data['pesanan_id'] = null;
                        // echo "asd";die();
                        $this->load->view(static::VIEW_PATH.$this->view,$this->data);
                    }
                    
                }
            }
            
            
        }

        public function detailPesananKonsumen($pesanan_id, $notification_user_id=null){
            $this->load->model('Notification_user_model');
            $this->Notification_user_model->setAsRead($notification_user_id);

            $this->load->model('Pesanan_model');
            $this->data['detail'] = $this->Pesanan_model->getDetailPesanan($pesanan_id);

            $dataFotoOrder = $this->Pesanan_model->getPhoto($pesanan_id);
            
            $id = md5($dataFotoOrder[0]['user_id'] . $this->config->item('encryption_key'));
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = base_url().'assets/uploads/pesanan/photos/';

            foreach ($dataFotoOrder as $fotoOrder) {
                $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
                if($fotoOrder['tipe'] == 'OM'){
                    $file_order[] = $path.$file_name;
                }else{
                    $file_acc[] = $path.$file_name;
                }
                
            }
            $this->data['detail']['foto_order'] = $file_order;
            $this->data['detail']['foto_acc'] = $file_acc;
            
// var_dump(static::VIEW_PATH.$this->view);die();
            $this->load->view(static::VIEW_PATH.$this->view,$this->data);
            // $this->template->viewDefault('pesanan_detail',$this->data);
            // $this->load->view('web/pesanan_detail',$this->data);
        }

        public function tolakPesananKonsumen($pesanan_id) {
        
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            $this->load->model('Pesanan_model');
            $this->data['detail'] = $this->Pesanan_model->getDetailPesanan($pesanan_id);
            // $this->template->viewDefault($this->view,$this->data);
            // echo "<pre>";print_r($this->data);die();
            $this->load->view(static::VIEW_PATH.$this->view,$this->data);
        }

        public function detailkomplainkonsumen($komplain_id, $notification_user_id=null){
            $this->load->model('Notification_user_model');
            $this->Notification_user_model->setAsRead($notification_user_id);
            
            $this->load->model('Komplain_model');
            $data = $this->Komplain_model->getDetailKomplain($komplain_id);
            $dataFotoOrder = $this->Komplain_model->getPhoto($komplain_id);
            
            $id = md5($dataFotoOrder[0]['user_id'] . $this->config->item('encryption_key'));
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = base_url().'/assets/uploads/komplain/photos/';

            foreach ($dataFotoOrder as $fotoOrder) {
                $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
                if($fotoOrder['tipe'] == 'OM'){
                    $file_order[] = $path.$file_name;
                }else{
                    $file_acc[] = $path.$file_name;
                }
                
            }
            $data['foto_order'] = $file_order;
            $data['foto_acc'] = $file_acc;

            $this->data['detail'] = $data;

            // echo "<pre>";print_r($data);die();
            $this->load->view(static::VIEW_PATH.$this->view,$this->data);
            // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->data);
        }

         public function tolakKomplainKonsumen($komplain_id) {
        
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            $this->load->model('Komplain_model');
            $this->data['detail'] = $this->Komplain_model->getDetailKomplain($komplain_id);
            // $this->template->viewDefault($this->view,$this->data);
            $this->load->view(static::VIEW_PATH.$this->view,$this->data);
        }

        public function logout(){
            $konsumen_id = $_SESSION["konsumen_auth"]["data"]['konsumen_id'];
            if($_SESSION["konsumen_auth"]["type"] == 1){
                $redirect = 'pesanan';
            }else{
                $redirect = "komplain";
            }
            unset($_SESSION["konsumen_auth"]);
            redirect('web/site/approve_penjadwalan_'.$redirect.'/'.$konsumen_id);
        }

        public function confirmKonsumen($id=1, $type=1){
            if($_SESSION["konsumen_auth"]){
                // echo "<pre>";print_r($this->input->post());die();
                $this->load->model('Notification_model');
                $this->postData = $this->input->post();
                if($type == 1){
                    $renker = dbGetRow("select flow_id,rencana_kerja_id, mitra_pemasang_kusen, mitra_pemasang_finish from pesanan_view where id=".$id);
                    $this->load->model('Usulan_model');
                    $this->load->model('Pesanan_model');
                    if($this->postData['confirm']==0 && !($this->postData['jadwal_usulan'])){
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan, masukkan tanggal pengajuan');
                    }
                    $konfirmasi = $this->Usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'],$renker['rencana_kerja_id'],$renker['flow_id'], $id,999);
                    if($konfirmasi){
                        if($this->postData['confirm'] == 0){
                            $this->Pesanan_model->minFlow($id, $renker['flow_id']);
                            if($renker['flow_id']==3){
                                $this->Notification_model->generate(Notification_model::ACTION_REJECT_JADWAL_KUSEN, $id,999);
                            }else{
                                $this->Notification_model->generate(Notification_model::ACTION_REJECT_JADWAL_FINISH, $id,999);
                            } 
                        }else{
                            $this->Pesanan_model->addFlow($id, $renker['flow_id']);
                            if($renker['flow_id']==3){
                                $this->Notification_model->generate(Notification_model::ACTION_CONFIRM_JADWAL_KUSEN, $id,999);
                            }else{
                                $this->Notification_model->generate(Notification_model::ACTION_CONFIRM_JADWAL_FINISH, $id,999);
                            }
                            
                        }
                        $kode_order = $_SESSION["konsumen_auth"]['data']['kode_order'];
                        $konsumen_id = $_SESSION["konsumen_auth"]['data']['konsumen_id'];
                        $_SESSION["konsumen_auth"]["data"] = $this->data['data'] = $this->Pesanan_model->getDetailPesananForKonsumen($kode_order,$konsumen_id,Usulan_model::STATUS_TERIMA  );
                        SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');
            
                    }else{
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan konfirmasi');
                    }
                }else{
                    $renker = dbGetRow("select flow_id,rencana_kerja_id, mitra_pemasang_kusen, mitra_pemasang_finish from komplain_view where id=".$id);
                    $this->load->model('Komplain_usulan_model');
                    $this->load->model('Komplain_model');
                    $this->load->model('Pesanan_model');    

                    if($this->postData['confirm']==0 && !($this->postData['jadwal_usulan'])){
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan pengajuan, masukkan tanggal pengajuan');
                    }

                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'],$renker['rencana_kerja_id'],$renker['flow_id'], $id,999);
                    if($konfirmasi){
                        if($this->postData['confirm'] == 0){
                            $this->Komplain_model->minFlow($id, $renker['flow_id']);
                            if($renker['flow_id']==3){
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_REJECT_JADWAL_KUSEN, $id,999);
                            }else{
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_REJECT_JADWAL_FINISH, $id,999);
                            }
                        }else{
                            $this->Komplain_model->addFlow($id, $renker['flow_id']);
                            if($renker['flow_id']==3){
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_CONFIRM_JADWAL_KUSEN, $id,999);
                            }else{
                                 $this->Notification_model->komplainGenerate(Notification_model::ACTION_CONFIRM_JADWAL_FINISH, $id,999);
                            }
                            
                        }
                        $konsumen_id = $_SESSION["konsumen_auth"]["data"]["konsumen_id"];
                        $kode_order = $_SESSION["konsumen_auth"]["data_pesanan"]["kode_order"];
                        $data = $this->Pesanan_model->getDetailPesananForKonsumen($kode_order, $konsumen_id);
                        $dataKomplain = $this->Komplain_model->getDetailKomplainByPesanan($data['id'], Komplain_usulan_model::STATUS_TERIMA);
                        $_SESSION["konsumen_auth"]["data"] = $dataKomplain;
                        $_SESSION["konsumen_auth"]["data"]["konsumen_id"] = $data['konsumen_id'];
                        $_SESSION["konsumen_auth"]["data_pesanan"] = $data;
                        SessionManagerWeb::setFlashMsg(true,'Berhasil melakukan konfirmasi');
            
                    }else{
                        SessionManagerWeb::setFlashMsg(false,'Gagal melakukan konfirmasi');
                    }
                }
                
                $direct = ($type == 1)? 'pesanan' : 'komplain';
                redirect('web/site/approve_penjadwalan_'.$direct.'/'.$_SESSION["konsumen_auth"]["data"]["konsumen_id"]);
            }else{
                $direct = ($type == 1)? 'pesanan' : 'komplain';
                redirect('web/site/approve_penjadwalan_'.$direct);
            }
        }

    }