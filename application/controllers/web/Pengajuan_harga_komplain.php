<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Dompdf\Dompdf;

class Pengajuan_harga_komplain extends WEB_Controller {

    public function index() {
        $data = $this->postData;

        if ($this->postData['tahun'] && $this->postData['bulan']) {
        
            $_SESSION['pengajuan_harga_komplain']['tahun'] = $tahun = $this->postData['tahun'];
            $_SESSION['pengajuan_harga_komplain']['bulan'] = $bulan = $this->postData['bulan'];
        }else{
            if($data['status'] || $data['keyword']){
                $tahun = $_SESSION['pengajuan_harga_komplain']['tahun'];
                $bulan = $_SESSION['pengajuan_harga_komplain']['bulan'];
            }else{
                $_SESSION['pengajuan_harga_komplain']['tahun'] = $tahun = date('Y');
                $_SESSION['pengajuan_harga_komplain']['bulan'] = $bulan = date('m');
            }
            
        }

        if($data['status'] || $data['keyword']){
            $this->data['filter_status'] = $data['status'];
            $this->data['filter_keyword'] = $data['keyword'];
        }

        if (empty($this->postData['status'])) {
           $data['status'] = "0";
        }

        $filter = "where 1 ";


        $this->data['data'] = $this->model->filter($filter)->getPengajuan($this->page, $data['status'], SessionManagerWeb::getRole(), $bulan, $tahun,$data['keyword']);
        // echo '<pre>';print_r($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function getPengajuanAdd() {
        $status = '4';
        $keyword = '';

        if(in_array(SessionManagerWeb::getRole(), array(Role::PEMASANG, Role::PRODUKSI))){ 

            $this->load->model('Konsumen_model');
            if($this->input->post('ajax')){
                $komplain_list = self::getDataKomplain($this->input->post('keyword'));
                // $konsumen_pengajuan = $this->Konsumen_model->getKonsumenPengajuan($status, $this->input->post('keyword'));
                die(json_encode($komplain_list));
            }else{
                $komplain_list = self::getDataKomplain();
                // $konsumen_pengajuan = $this->Konsumen_model->getKonsumenPengajuan($status, $keyword);
            }
            $this->data['komplain_list'] = $komplain_list;
            // echo '<pre>';print_r($this->data['komplain_list']);die();

            $this->load->model('Harga_maintenance_model');
            $data = $this->Harga_maintenance_model->filter($filter)->getHargaMaintenance($this->page);

            $this->data['list_harga'] = $data;
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Role anda tidak dapat melakukan pengajuan harga komplain');

            redirect('web/pengajuan_harga_komplain');
        }
        // echo '<pre>';var_dump($this->data);die();

        $this->template->viewDefault($this->view,$this->data);

    }

    public function getDataKomplain($postData =null){
        $this->load->model('Komplain_model');
        $where = "where 1 ";
        if($postData!=null){
            $where .= " and k.nama_konsumen LIKE '%".$postData."%' OR k.kode_order LIKE '%".$postData."%'";
        }
        if(SessionManagerWeb::getRole() == Role::PRODUKSI){
            $column = "k.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan, us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and k.flow_id in(3,4,6,7)";
        }
        elseif(SessionManagerWeb::getRole() == Role::PEMASANG){

            $column = "k.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan , us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id";
            $where .= "and (
                                (k.flow_id in (4,7)  and (k.mitra_pemasang_kusen=".SessionManagerWeb::getUserID()." or k.mitra_pemasang_finish=".SessionManagerWeb::getUserID().")) or 
                               (k.flow_id = 2 and tanggal_pasang_kusen is null and k.mitra_pemasang_kusen=".SessionManagerWeb::getUserID().") or
                               (k.flow_id = 5 and tanggal_pasang_kusen is not null and tanggal_pasang_finish is null and k.mitra_pemasang_finish=".SessionManagerWeb::getUserID().")
                           ) 
                        ";
        }
        $need_action = $this->Komplain_model->column($column)->join($join)->filter($where)->getKomplains('unl');
        return $need_action;
    }


    public function create() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        // echo '<pre>';var_dump($data);die();
        if (empty($data['keterangan'])) {
            $data['keterangan'] = '';
        }

        $insert = $this->model->create($data, SessionManagerWeb::getUserID());
        // var_dump($insert);die();
        if ($insert) {

            $countharga = count($data['id']);
            for ($i=0; $i < $countharga; $i++) {

                if ($data['jumlah'][$i] > 0) {
                    $dataInsert = array(
                        'id_pengajuan_harga_komplain'   => $insert,
                        'id_harga_maintenance'          => $data['id'][$i],
                        'harga'                         => $data['harga_maintenance'][$i],
                        'jumlah'                        => $data['jumlah'][$i],
                        'updated_by'                    => SessionManagerWeb::getUserID()
                    );

                    $this->load->model('Detail_pengajuan_komplain_model');
                    $detail = $this->Detail_pengajuan_komplain_model->create($dataInsert, TRUE, TRUE);
                    
                    if (!$detail) {
                        $hapus = dbQuery("delete from pengajuan_harga_komplain where id=".$insert);
                        $hapus = dbQuery("delete from detail_pengajuan_komplain where id_pengajuan_harga_komplain=".$insert);
                        SessionManagerWeb::setFlashMsg(false, 'Gagal membuat pengajuan');
                    }
                } 
                
            }
            
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat pengajuan');
            redirect("web/pengajuan_harga_komplain");

        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal membuat pengajuan komplain');
            redirect("web/pengajuan_harga_komplain/getpengajuanadd");

        }
    }

    public function ajaxGetHarga() {
        $data = $this->postData;

        $this->load->model('Harga_maintenance_model');
        $data = $this->Harga_maintenance_model->getHargaMaintenanceById($data['id']);
         echo json_encode($data);
    }

    public function ajaxGetListHargaTemp() {

        $this->load->model('Harga_maintenance_model');
        // $data = $this->Harga_model->getHargaById($data['id']);
        $data = $this->Harga_maintenance_model->filter($filter)->getHargaMaintenance($this->page);

        foreach ($data as $key => $val) {
            $data[$key]['jml'] = "0";
            $data[$key]['harga_maintenance'] = "0";
        }

        // $result = array();
        // foreach ($data as $element) {
        //     $result[$element['kategori']][] = $element;
        // }

        echo json_encode($data);
        // echo json_encode($result);
    }

    public function getOnePengajuan($id) {
        // $data = $this->postData;
        $data['id_pengajuan'] = $id;
        
        $filter = "where 1 ";

        // if($this->postData['filterExist']){
        //     $filter .= $this->getFilter($this->postData);
        // }

        $this->load->model('Konsumen_model');
        $this->data['konsumen_pengajuan'] = $this->Konsumen_model->getKonsumenPengajuan('4', '');

        $this->data['data'] = $pengajuan = $this->model->filter($filter)->getOnePengajuan($this->page, $data['id_pengajuan']);
        // echo "<pre>";print_r($pengajuan);die();

        // if (SessionManagerWeb::getRole() == Role::PRODUKSI && ($pengajuan['status'] == 1 || $pengajuan['status'] == 2 || $pengajuan['status'] == 3) ) {
        //     SessionManagerWeb::setFlashMsg(false, 'Role anda tidak dapat membuka halaman ini');
        //    redirect('web/pengajuan_harga');
        // }

        // if ( (SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DESAIN) && ($pengajuan['status'] == 5) ) {
        //     SessionManagerWeb::setFlashMsg(false, 'Role anda tidak dapat membuka halaman ini');
        //    redirect('web/pengajuan_harga');
        // }

        $detail = $this->model->filter($filter)->getDetailPengajuan($data['id_pengajuan']);
        // echo "<pre>";print_r($detail);die();
			
        $this->data['detail'] = $detail;
// echo "<pre>";print_r($detail);die();
        $sum = array();
        $jumlah = 0;
        $total_harga_jual = 0;
									   
        foreach ($detail as $key) {
            $jumlah += $key['jumlah'];
            $total_harga_jual += $key['harga']*$key['jumlah'];


            $sum['jumlah'] = $jumlah;
            $sum['total_harga'] = $total_harga_jual;
																			  
        }
// die();
        $this->data['sum'] = $sum;

        $this->data['detail_full'] = $this->getstrukfull($data['id_pengajuan']);
        // echo '<pre>';print_r($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }

    public function ajukanHarga() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        // var_dump($data);die();
        $insert = $this->model->ajukanHarga($data);
        // var_dump($insert);die();
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil melakukan pengajuan');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal melakukan pengajuan');
        }

        redirect('web/pengajuan_harga_komplain/getonepengajuan/'.$data['id']);

    }

    public function approveHarga() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        // echo "<pre>";print_r($data);die();
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        
        $insert = $this->model->approveHarga($data);
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil approve pengajuan');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal approve pengajuan');
        }
         redirect('web/pengajuan_harga_komplain/getonepengajuan/'.$data['id']);

    }

    public function tolakHarga() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        
        $insert = $this->model->tolakPengajuanHarga($data);
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil menolak pengajuan');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal menolak pengajuan');
        }
        
        redirect('web/pengajuan_harga_komplain/getonepengajuan/'.$data['id']);
    }

    public function ajaxgetsavetosession(){
        $data = $this->postData;

        $_SESSION['arr_data'] = $data;

        die();
    }

    public function getstrukfull($id){
        
        $this->load->model('Pengajuan_harga_komplain_model');

        $details = $this->Pengajuan_harga_komplain_model->filter($filter)->getDetailPengajuan($id);

        $this->load->model('Harga_maintenance_model');

        $data = $this->Harga_maintenance_model->filter($filter)->getHargaMaintenance($this->page);

        $detail = array();
        $count = count($details) - 1;
        foreach ($data as $k => $element) {

            // $detail[$element['kategori']][$k]['harga_pokok_granit_revisi'] = false;
            // $detail[$element['kategori']][$k]['harga_pokok_keramik_revisi'] = false; 
            foreach ($details as $key => $val) {

            // var_dump($element['id']);
            // var_dump($val['id_harga_maintenance']);
            // die();
                if ($element['id'] == $val['id_harga_maintenance']) {
                    $hasil = 1;
                    $detail[$k] = $val;
                    break;
                }else{
                    if ($count == $key) {
                        $hasil = 0;
                    }
                }
            }

            if ($hasil == 0){

                $harga['id'] = null;
                $harga['id_pengajuan_harga_komplain'] = null;
                $harga['id_harga_maintenance'] = $element['id'];
                $harga['harga'] = 0;
                $harga['jumlah'] = 0;
                $harga['nama'] = $element['nama'];

                $detail[$k] = $harga;
            }

        }
        // echo "<pre>";var_dump($detail);die();

        return $detail;

    }

    public function ajaxpreviewpdf(){

        $this->data['jenis'] = $_SESSION['arr_data']['jenis'];
        $this->data['tipeharga'] = $_SESSION['arr_data']['tipeharga'];
        $data['kode_order'] = $_SESSION['arr_data']['kode_order'];
        $data['nama'] = $_SESSION['arr_data']['nama_konsumen'];
        $data['alamat'] = $_SESSION['arr_data']['alamat_konsumen'];
        $data['no_hp'] = $_SESSION['arr_data']['no_hp_konsumen'];

        $this->data['data'] = $data;
        $detail = $_SESSION['arr_data']['arrharga'];


        foreach ($detail as $k => $v) {
            $detail[$k]['nama'] = $v['nama'];
            $detail[$k]['jumlah'] = $v['jml'];
        }
        $this->data['detail'] = $detail;

        $this->load->view(static::VIEW_PATH.'pengajuan_harga_komplain_previewstrukpdf',$this->data);


        $html = $this->output->get_output();
// echo $html;
// die();
        $this->load->library('pdfdom');

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->set_option('isRemoteEnabled', TRUE);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("html_contents.pdf", array("Attachment"=> 0));
    }

    public function ajaxpreviewcheckpdf(){

        $this->data['jenis'] = $_SESSION['arr_data']['jenis'];
        $this->data['tipeharga'] = $_SESSION['arr_data']['tipeharga'];
        $data['kode_order'] = $_SESSION['arr_data']['kode_order'];
        $data['nama'] = $_SESSION['arr_data']['nama_konsumen'];
        $data['alamat'] = $_SESSION['arr_data']['alamat_konsumen'];
        $data['no_hp'] = $_SESSION['arr_data']['no_hp_konsumen'];

        $this->data['data'] = $data;
        $detail = $_SESSION['arr_data']['arrharga'];
// echo "<pre>";
        $result = array();

        // var_dump($detail);
        // die();
        // foreach ($detail as $key => $row) {
            
        //     unset($result[$key]);
        //     $result[$row[0]['kategori']] = $row;

        //     $count_item = 0;
        //     $i = 1;
            foreach ($detail as $k => $v) {
                $jml = (int) $v['jml'];
                if ($jml > 0) {        
                    $result[$k]['nama'] = $v['nama'];
                    $result[$k]['jumlah'] = $v['jml'];
                    $result[$k]['harga_maintenance'] = $v['harga_maintenance'];

                    $count_item += $i++;
                }
            }

            // if ($count_item == 0) {
            //     unset($result[$row[0]['kategori']]);
            // }
            
        // }
        $this->data['detail'] = $result;
// var_dump($result);
        $this->load->view(static::VIEW_PATH.'pengajuan_harga_komplain_previewstrukpdf',$this->data);


        $html = $this->output->get_output();
// echo $html;
// die();
        $this->load->library('pdfdom');

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->set_option('isRemoteEnabled', TRUE);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("html_contents.pdf", array("Attachment"=> 0));
    }

    public function revisiDetail() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        
        $this->load->model('Detail_pengajuan_komplain_model');
        // Hapus data detail yang lama
        $hapus = dbQuery("delete from detail_pengajuan_komplain where id_pengajuan_harga_komplain='".$data['id_pengajuan_harga_komplain']."'");
        
        
        if ($hapus) {
            // Tambahkan data detail yang baru
            $countharga = count($data['id']);
            for ($i=0; $i < $countharga; $i++) { 
                if ($data['jumlah'][$i] > 0) {
            
                    $dataInsert = array(
                        'id_pengajuan_harga_komplain'    => $data['id_pengajuan_harga_komplain'],
                        'id_harga_maintenance'              => $data['id'][$i],
                        'harga'                             => $data['harga'][$i],
                        'jumlah'                            => (float) $data['jumlah'][$i],
                        'updated_by'                        => SessionManagerWeb::getUserID()
                    );
                
                    $detail = $this->Detail_pengajuan_komplain_model->create($dataInsert, TRUE, TRUE);
                    
                    if (!$detail) {
                        $hapus = dbQuery("delete from detail_pengajuan_komplain where id_pengajuan_harga_komplain=".$data['id_pengajuan_harga_komplain']);
                        SessionManagerWeb::setFlashMsg(false, 'Gagal membuat pengajuan');
                        redirect('web/pengajuan_harga_komplain/getonepengajuan/'.$data['id_pengajuan_harga_komplain']);
                    }
                }

            }
            
            // Mengubah data pengajuan_harga
            // $this->load->model('Pengajuan_harga_model');
            // $revisi = $this->Pengajuan_harga_model->revisiPengajuan($data, TRUE, FALSE);
            
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat revisi');
            redirect('web/pengajuan_harga_komplain/getonepengajuan/'.$data['id_pengajuan_harga_komplain']);

        } else {
            SessionManagerWeb::setFlashMsg(false, 'Gagal membuat revisi');
            redirect('web/pengajuan_harga_komplain/getonepengajuan/'.$data['id_pengajuan_harga_komplain']);
        }
    }

    public function previewstrukcheckpdf($id,$jenis,$tipeharga){
        $this->load->model('Pengajuan_harga_komplain_model');

        $this->data['jenis'] = $jenis;
        $this->data['tipeharga'] = $tipeharga;

        $data = $this->Pengajuan_harga_komplain_model->filter($filter)->getOnePengajuan($this->page, $id);
        $data['nama'] = ($data['id_konsumen'] != 0) ? $data['nama'] : $data['nama_konsumen'];
        $data['alamat'] = ($data['id_konsumen'] != 0) ? $data['alamat'] : $data['alamat_konsumen'];
        $data['no_hp'] = ($data['id_konsumen'] != 0) ? $data['no_hp'] : $data['no_hp_konsumen'];
        $this->data['data'] = $data;
        $details = $this->Pengajuan_harga_komplain_model->filter($filter)->getDetailPengajuan($id);

        // $result = array();
        foreach ($details as $k => $element) {
            // $result[$element['nama_kategori']][$k] = $element;
            // $result[$element['nama_kategori']][$k]['harga_pokok_granit_revisi'] = false;
            // $result[$element['nama_kategori']][$k]['harga_pokok_keramik_revisi'] = false;

        //     if (SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::DIREKTUR) {
        //         $sql = "select harga_pokok_granit, harga_pokok_keramik from harga where id={$element['id_harga']}";
        //         $masterharga = dbGetRow($sql);
        //         if ($element['harga_pokok_granit'] != $masterharga['harga_pokok_granit']) {
        //             $result[$element['nama_kategori']][$k]['harga_pokok_granit_revisi'] = true;
        //         }

        //         if ($element['harga_pokok_keramik'] != $masterharga['harga_pokok_keramik']) {
        //             $result[$element['nama_kategori']][$k]['harga_pokok_keramik_revisi'] = true;
        //         }
        //     }
            $details[$k]['harga_maintenance'] = $element['harga'];
        }
        $this->data['detail'] = $details;
        // echo '<pre>';var_dump($this->data);die();

        $this->load->view(static::VIEW_PATH.'pengajuan_harga_komplain_previewstrukpdf',$this->data);

        $html = $this->output->get_output();
// echo $html;
// die();
        $this->load->library('pdfdom');

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->set_option('isRemoteEnabled', TRUE);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("html_contents.pdf", array("Attachment"=> 0));
    }

    public function previewStrukPdf($id,$jenis,$tipeharga){
        $this->load->model('Pengajuan_harga_komplain_model');

        $this->data['jenis'] = $jenis;
        $this->data['tipeharga'] = $tipeharga;

        $data = $this->Pengajuan_harga_komplain_model->filter($filter)->getOnePengajuan($this->page, $id);
        $data['nama'] = ($data['id_konsumen'] != 0) ? $data['nama'] : $data['nama_konsumen'];
        $data['alamat'] = ($data['id_konsumen'] != 0) ? $data['alamat'] : $data['alamat_konsumen'];
        $data['no_hp'] = ($data['id_konsumen'] != 0) ? $data['no_hp'] : $data['no_hp_konsumen'];
        $this->data['data'] = $data;
        
        $details = $this->Pengajuan_harga_komplain_model->filter($filter)->getDetailPengajuan($id);

        $this->load->model('Harga_maintenance_model');

        $data = $this->Harga_maintenance_model->filter($filter)->getHargaMaintenance($this->page);

        $detail = array();
        $count = count($details) - 1;
        foreach ($data as $k => $element) {
            // $detail[$element['kategori']][$k]['harga_pokok_granit_revisi'] = false;
            // $detail[$element['kategori']][$k]['harga_pokok_keramik_revisi'] = false; 

            foreach ($details as $key => $val) {
                if ($element['id'] == $val['id_harga_maintenance']) {
                    // $detail[$val['nama_kategori']][$k] = $val;
                    $hasil = 1;
                    
                    $ret['id'] = $val['id'];
                    $ret['id_pengajuan_harga_komplain'] = $val['id_pengajuan_harga_komplain'];
                    $ret['id_harga_maintenance'] = $element['id'];
                    $ret['harga_maintenance'] = $val['harga'];
                    $ret['jumlah'] = $val['jumlah'];
                    $ret['nama'] = $element['nama'];
    
                    $detail[$k] = $ret;

                    // if (SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::DIREKTUR) {
                    //     if ($val['harga_pokok_granit'] != $element['harga_pokok_granit']) {
                    //         $detail[$val['nama_kategori']][$k]['harga_pokok_granit_revisi'] = true;
                    //     }

                    //     if ($val['harga_pokok_keramik'] != $element['harga_pokok_keramik']) {
                    //         $detail[$val['nama_kategori']][$k]['harga_pokok_keramik_revisi'] = true;
                    //     }
                    // }
                    break;
                }else{
                    if ($count == $key) {
                        $hasil = 0;
                    }
                }
            }

            if ($hasil == 0){

                $harga['id'] = null;
                $harga['id_pengajuan_harga_komplain'] = null;
                $harga['id_harga_maintenance'] = $element['id'];
                $harga['harga_maintenance'] = 0;
                $harga['jumlah'] = 0;
                $harga['nama'] = $element['nama'];

                $detail[$k] = $harga;
            }

        }

        $this->data['detail'] = $detail;
        // echo '<pre>';var_dump($this->data);die();

        $this->load->view(static::VIEW_PATH.'pengajuan_harga_komplain_previewstrukpdf',$this->data);


        $html = $this->output->get_output();
// echo $html;
// die();
        $this->load->library('pdfdom');

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->set_option('isRemoteEnabled', TRUE);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("html_contents.pdf", array("Attachment"=> 0));
    }

}
?>