<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Harga_maintenance extends WEB_Controller {

    public function index() {
        $filter = "where 1 ";

        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }

        $data = $this->model->filter($filter)->getHargaMaintenance($this->page);

        $this->data['data'] = $data;

        // echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }

    public function add(){
        $this->template->viewDefault('harga_maintenance_add',$this->data);   
    }

    public function edit($id){        

        $this->data['data'] = $this->model->getHargaMaintenanceById($id);

        $this->template->viewDefault('harga_maintenance_add',$this->data);   
    }

    public function create() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        $insert = $this->model->create($data, SessionManagerWeb::getUserID());
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat harga maintenance');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal membuat harga maintenance');
        }
        redirect('web/'.$this->class);
    }
    
    public function update() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        $insert = $this->model->updateHargaMaintenance($data);
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil update harga maintenance');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal update harga maintenance');
        }
        redirect('web/'.$this->class);
    }

    public function delete($id) {
        if (!in_array(SessionManagerWeb::getRole(), array(Role::DIREKTUR, Role::ADMINISTRATOR))) {
            SessionManagerWeb::setFlashMsg(false, 'Anda tidak memiliki akses');
        }
        $delete = $this->model->deleteHargaMaintenance($id);
        
        if ($delete) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil hapus harga maintenance');
        } else {
            SessionManagerWeb::setFlashMsg(false, 'Gagal hapus harga maintenance');
        }
        redirect('web/'.$this->class);
    }
    
}

?>