<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Dompdf\Dompdf;

class Pengajuan_harga extends WEB_Controller {

    public function index($page=false) {

        $data = $this->postData;

        if ($this->postData['tahun'] && $this->postData['bulan']) {
        
            $_SESSION['pengajuan_harga']['tahun'] = $tahun = $this->postData['tahun'];
            $_SESSION['pengajuan_harga']['bulan'] = $bulan = $this->postData['bulan'];
        }else{
            if($data['status'] || $data['keyword']){
                $tahun = $_SESSION['pengajuan_harga']['tahun'];
                $bulan = $_SESSION['pengajuan_harga']['bulan'];
            }else{
                $_SESSION['pengajuan_harga']['tahun'] = $tahun = date('Y');
                $_SESSION['pengajuan_harga']['bulan'] = $bulan = date('m');
            }
            
        }

        if($data['status'] || $data['keyword']){
            $this->data['filter_status'] = $data['status'];
            $this->data['filter_keyword'] = $data['keyword'];
        }

        if (empty($this->postData['status'])) {
           $data['status'] = "0";
        }

        $filter = "where 1 ";

            // echo "<pre>";var_dump($this->postData);die();
        
        // echo '<pre>';print_r($this->data);die();
        

        $this->data['data'] = $this->model->filter($filter)->getPengajuan($this->page, $data['status'], SessionManagerWeb::getRole(), SessionManagerWeb::getUserID(), $bulan, $tahun,$data['keyword']);

        $this->template->viewDefault($this->view,$this->data);
    }

    public function pageof($page=flase){

        $data = $this->postData;


        if($data['status'] || $data['keyword']){
            $this->data['filter_status'] = $data['status'];
            $this->data['filter_keyword'] = $data['keyword'];
        }

        if (empty($this->postData['status'])) {
           $data['status'] = "0";
        }

        $filter = "where 1 ";

        $this->data['filter_status'] = $_SESSION['pengajuan_harga']['status'] = (!empty($data['status']) || $data['status'] == '0') ? $data['status'] : $_SESSION['pengajuan_harga']['status'];


        $this->data['filter_keyword'] = $_SESSION['pengajuan_harga']['keyword'] = $data['keyword'] ? $data['keyword'] : '';
        $_SESSION['pengajuan_harga']['page'] = ($page!==false) ? (((int)$page==0) ? (int) $page : (int) $page-1) : (int) $this->page;


        $this->data['data'] = $this->model->filter($filter)->getPengajuan($_SESSION['pengajuan_harga']['page'], $_SESSION['pengajuan_harga']['status'], SessionManagerWeb::getRole(), SessionManagerWeb::getUserID(), '', '',$_SESSION['pengajuan_harga']['keyword']);
        
        $_SESSION['pengajuan_harga']['total_data'] = $this->model->filter($filter)->getPengajuan('count', $_SESSION['pengajuan_harga']['status'], SessionManagerWeb::getRole(), SessionManagerWeb::getUserID(), '', '',$_SESSION['pengajuan_harga']['keyword']);
        
        
        // Pagination Configuration
        $this->load->library('pagination');

        $config['base_url'] = base_url().'web/pengajuan_harga/pageof/';
        // $config['base_url'] = '';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $_SESSION['pengajuan_harga']['total_data'];
        $config['per_page'] = 10;

        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = '&raquo';
        $config['prev_link']        = '&laquo';
        $config['full_tag_open']    = '<ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';


        // Initialize
        $this->pagination->initialize($config);

        // Initialize $data Array
        $this->data['pagination'] = $this->pagination->create_links();

        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function getPengajuanAdd() {
        $status = '4';
        $keyword = '';

        if(SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DIREKTUR || SessionManagerWeb::getRole() == Role::ADMINISTRATOR || SessionManagerWeb::getRole() == Role::DESAIN || SessionManagerWeb::getRole() == Role::MITRA_MARKETING){ 

            $this->load->model('Konsumen_model');
            if($this->input->post('ajax')){
                $konsumen_pengajuan = $this->Konsumen_model->getKonsumenPengajuan($status, $this->input->post('keyword'));
                die(json_encode($konsumen_pengajuan));
            }else{
                $konsumen_pengajuan = $this->Konsumen_model->getKonsumenPengajuan($status, $keyword);
            }
            $this->data['konsumen_pengajuan'] = $konsumen_pengajuan;
            // echo '<pre>';var_dump($this->data);die();

            $this->load->model('Harga_model');
            $data = $this->Harga_model->filter($filter)->getHarga($this->page);

            $result = array();
            foreach ($data as $element) {
                $result[$element['kategori']][] = $element;
            }
            $this->data['list_harga'] = $result;
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Role anda tidak dapat melakukan pengajuan harga');

            redirect('web/pengajuan_harga');
        }
        // echo '<pre>';var_dump($this->data);die();

        $this->template->viewDefault($this->view,$this->data);

    }

    public function ajaxGetListHargaTemp() {

        $this->load->model('Harga_model');
        // $data = $this->Harga_model->getHargaById($data['id']);
        $data = $this->Harga_model->filter($filter)->getHarga($this->page);

        foreach ($data as $key => $val) {
            $data[$key]['jml'] = "0";
            $data[$key]['harga_jual_granit'] = "0";
            $data[$key]['harga_jual_keramik'] = "0";
            $data[$key]['harga_pokok_granit'] = "0";
            $data[$key]['harga_pokok_keramik'] = "0";
        }

        $result = array();
        foreach ($data as $element) {
            $result[$element['kategori']][] = $element;
        }

        // echo json_encode($data);
        echo json_encode($result);
    }

    public function ajaxGetHarga() {
        $data = $this->postData;

        $this->load->model('Harga_model');
        $data = $this->Harga_model->getHargaById($data['id']);
         echo json_encode($data);
    }

    public function getOnePengajuan($id) {
        // $data = $this->postData;
        $data['id_pengajuan'] = $id;
        
        $filter = "where 1 ";

        // if($this->postData['filterExist']){
        //     $filter .= $this->getFilter($this->postData);
        // }

        $this->load->model('Konsumen_model');
        $this->data['konsumen_pengajuan'] = $this->Konsumen_model->getKonsumenPengajuan('4', '');

        $this->data['data'] = $pengajuan = $this->model->filter($filter)->getOnePengajuan($this->page, $data['id_pengajuan']);

        if (SessionManagerWeb::getRole() == Role::PRODUKSI && ($pengajuan['status'] == 1 || $pengajuan['status'] == 2 || $pengajuan['status'] == 3) ) {
            SessionManagerWeb::setFlashMsg(false, 'Role anda tidak dapat membuka halaman ini');
           redirect('web/pengajuan_harga');
        }

        if ( (SessionManagerWeb::getRole() == Role::MARKETING || SessionManagerWeb::getRole() == Role::DESAIN) && ($pengajuan['status'] == 5) ) {
            SessionManagerWeb::setFlashMsg(false, 'Role anda tidak dapat membuka halaman ini');
           redirect('web/pengajuan_harga');
        }

        $detail = $this->model->filter($filter)->getDetailPengajuan($data['id_pengajuan']);

        $result = array();
        foreach ($detail as $k => $element) {
            
            $result[$element['nama_kategori']][$k] = $element;
            $result[$element['nama_kategori']][$k]['harga_pokok_granit_revisi'] = false;
            $result[$element['nama_kategori']][$k]['harga_pokok_keramik_revisi'] = false;

            if (SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::DIREKTUR) {
                $sql = "select harga_pokok_granit, harga_pokok_keramik from harga where id={$element['id_harga']}";
                $masterharga = dbGetRow($sql);
                if ($element['harga_pokok_granit'] != $masterharga['harga_pokok_granit']) {
                    $result[$element['nama_kategori']][$k]['harga_pokok_granit_revisi'] = true;
                }

                if ($element['harga_pokok_keramik'] != $masterharga['harga_pokok_keramik']) {
                    $result[$element['nama_kategori']][$k]['harga_pokok_keramik_revisi'] = true;
                }
            }
        }
        $this->data['detail'] = $result;
// echo "<pre>";var_dump($detail);die();
        $sum = array();
        $jumlah = 0;
        $total_harga_jual_granit = 0;
        $total_harga_jual_keramik = 0;
        $total_harga_pokok_granit = 0;
        $total_harga_pokok_keramik = 0;
        foreach ($detail as $key) {
            $jumlah += $key['jumlah'];
            $total_harga_jual_granit += $key['harga_jual_granit']*$key['jumlah'];
            $total_harga_jual_keramik += $key['harga_jual_keramik']*$key['jumlah'];
            $total_harga_pokok_granit += $key['harga_pokok_granit']*$key['jumlah'];
            $total_harga_pokok_keramik += $key['harga_pokok_keramik']*$key['jumlah'];

            $sum['jumlah'] = $jumlah;
            $sum['total_harga_jual_granit'] = $total_harga_jual_granit;
            $sum['total_harga_jual_keramik'] = $total_harga_jual_keramik;
            $sum['total_harga_pokok_granit'] = $total_harga_pokok_granit;
            $sum['total_harga_pokok_keramik'] = $total_harga_pokok_keramik;
        }
// die();
        $this->data['sum'] = $sum;

        $this->data['detail_full'] = $this->getstrukfull($data['id_pengajuan']);
        // echo '<pre>';print_r($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function getDetailPengajuan($id) {
        $this->data['data'] = $this->model->filter($filter)->getDetailPengajuan($id);
        echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }

    public function revisiDetail() {
        $data = $this->postData;
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        
        $this->load->model('Detail_pengajuan_model');
        
        // Hapus data detail yang lama
        $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga='".$data['id_pengajuan_harga']."'");
        
        
        if ($hapus) {
            // Tambahkan data detail yang baru

            $grand_total = array(
                    'harga_setuju_granit'           => 0,
                    'harga_setuju_keramik'          => 0,
                    'harga_setuju_pokok_granit'     => 0,
                    'harga_setuju_pokok_keramik'    => 0,
                    'id'                            => $data['id_pengajuan_harga']
            );

            $countharga = count($data['id']);
            for ($i=0; $i < $countharga; $i++) { 
                if ($data['jumlah'][$i] > 0) {
            
                    $dataInsert = array(
                        'id_pengajuan_harga'    => $data['id_pengajuan_harga'],
                        'id_harga'              => $data['id'][$i],
                        'harga_pokok_granit'    => $data['harga_pokok_granit'][$i],
                        'harga_pokok_keramik'   => $data['harga_pokok_keramik'][$i],
                        'harga_jual_granit'     => $data['harga_jual_granit'][$i],
                        'harga_jual_keramik'    => $data['harga_jual_keramik'][$i],
                        'jumlah'                => (float) $data['jumlah'][$i],
                        'updated_by'            => SessionManagerWeb::getUserID()
                    );


                    $grand_total['harga_setuju_granit']        += $data['harga_jual_granit'][$i] * (float) $data['jumlah'][$i];
                    $grand_total['harga_setuju_keramik']       += $data['harga_jual_keramik'][$i] * (float) $data['jumlah'][$i];
                    $grand_total['harga_setuju_pokok_granit']  += $data['harga_pokok_granit'][$i] * (float) $data['jumlah'][$i];
                    $grand_total['harga_setuju_pokok_keramik'] += $data['harga_pokok_keramik'][$i] * (float) $data['jumlah'][$i];

                    $detail = $this->Detail_pengajuan_model->create($dataInsert, TRUE, TRUE);
                    
                    if (!$detail) {
                        $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga=".$data['id_pengajuan_harga']);
                        SessionManagerWeb::setFlashMsg(false, 'Gagal membuat pengajuan');
                        redirect('web/pengajuan_harga/getonepengajuan/'.$data['id_pengajuan_harga']);
                    }
                }

            }
            // echo "<pre>";print_r($grand_total);die();
            $this->updateHargaSetuju($grand_total);
            $this->updateFeeDesainer($data['id_pengajuan_harga']);
            // Mengubah data pengajuan_harga
            // $this->load->model('Pengajuan_harga_model');
            // $revisi = $this->Pengajuan_harga_model->revisiPengajuan($data, TRUE, FALSE);
            
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat revisi');
            redirect('web/pengajuan_harga/getonepengajuan/'.$data['id_pengajuan_harga']);

        } else {
            SessionManagerWeb::setFlashMsg(false, 'Gagal membuat revisi');
            redirect('web/pengajuan_harga/getonepengajuan/'.$data['id_pengajuan_harga']);
        }
    }

    public function updateFeeDesainer($pengajuan_harga_id){
        $this->load->model('Fee_desainer_model');
        $id_konsumen = dbGetOne("select id_konsumen from pengajuan_harga where id=".$pengajuan_harga_id);
        $pesanan = dbGetRow("select id,harga_deal from pesanan_view where konsumen_id=".$id_konsumen);;
        $fee_desainer = $this->Fee_desainer_model->getFeeRow($pesanan['id']);
        if($fee_desainer){
            $fee_design = 50000;
            if ($pesanan['harga_deal']>=7500000 && $pesanan['harga_deal']<10000000) {
                $fee_design = 75000;
            }elseif($pesanan['harga_deal']>=10000000){
                $fee_design = 100000;
            }
            $updateFeeDesainer = array(
                'pesanan_id'            => $pesanan['id'],
                'fee_design'            => $fee_design
            );

            if(!in_array($fee_desainer['persentase_eksekusi'], array(0, null))){
                $updateFeeDesainer['fee_eksekusi'] = $pesanan['harga_deal'] * ($fee_desainer['persentase_eksekusi'] / 100);
            }
            $this->Fee_desainer_model->edit($updateFeeDesainer, $fee_desainer['id']);
            return true;

        }else{
            return false;
        }

        
    }

    public function simpanAjukanHarga(){
        
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        
        $this->load->model('Detail_pengajuan_model');
        // Hapus data detail yang lama
        $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga='".$data['id_pengajuan_harga']."'");
        
        
        if ($hapus) {
            // Tambahkan data detail yang baru
            $countharga = count($data['id']);
            for ($i=0; $i < $countharga; $i++) { 
                if ($data['jumlah'][$i] > 0) {
            
                    $dataInsert = array(
                        'id_pengajuan_harga'    => $data['id_pengajuan_harga'],
                        'id_harga'              => $data['id'][$i],
                        'harga_pokok_granit'    => $data['harga_pokok_granit'][$i],
                        'harga_pokok_keramik'   => $data['harga_pokok_keramik'][$i],
                        'harga_jual_granit'     => $data['harga_jual_granit'][$i],
                        'harga_jual_keramik'    => $data['harga_jual_keramik'][$i],
                        'jumlah'                => $data['jumlah'][$i],
                        'updated_by'            => SessionManagerWeb::getUserID()
                    );
                
                    $detail = $this->Detail_pengajuan_model->create($dataInsert, TRUE, TRUE);
                    
                    if (!$detail) {
                        $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga=".$data['id_pengajuan_harga']);
                        SessionManagerWeb::setFlashMsg(false, 'Gagal membuat pengajuan');
                        redirect('web/pengajuan_harga/getonepengajuan/'.$data['id_pengajuan_harga']);
                    }
                }

            }
            
            $data['id'] = $data['id_pengajuan_harga'];
            $insert = $this->model->ajukanHarga($data);

            if (!$insert) {
                SessionManagerWeb::setFlashMsg(false, 'Gagal membuat pengajuan');
                redirect('web/pengajuan_harga/getonepengajuan/'.$data['id_pengajuan_harga']);
            }

            // Mengubah data pengajuan_harga
            // $this->load->model('Pengajuan_harga_model');
            // $revisi = $this->Pengajuan_harga_model->revisiPengajuan($data, TRUE, FALSE);
            
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat revisi dan pengajuan');
            redirect('web/pengajuan_harga/getonepengajuan/'.$data['id_pengajuan_harga']);

        } else {
            SessionManagerWeb::setFlashMsg(false, 'Gagal membuat revisi dan pengajuan');
            redirect('web/pengajuan_harga/getonepengajuan/'.$data['id_pengajuan_harga']);
        }

    }

    public function simpanAjukanHargaProduksi(){
        
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        
        $this->load->model('Detail_pengajuan_model');
        // Hapus data detail yang lama
        $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga='".$data['id_pengajuan_harga']."'");
        
        
        if ($hapus) {
            // Tambahkan data detail yang baru
            $countharga = count($data['id']);
            $harga_setuju_granit = 0;
            $harga_setuju_keramik = 0;
            $harga_setuju_pokok_granit = 0;
            $harga_setuju_pokok_keramik = 0;
            for ($i=0; $i < $countharga; $i++) { 
                if ($data['jumlah'][$i] > 0) {

                    $harga_setuju_granit += $data['harga_pokok_granit'][$i];
                    $harga_setuju_keramik += $data['harga_pokok_keramik'][$i];
                    $harga_setuju_pokok_granit += $data['harga_jual_granit'][$i];
                    $harga_setuju_pokok_keramik += $data['harga_jual_keramik'][$i];
            
                    $dataInsert = array(
                        'id_pengajuan_harga'    => $data['id_pengajuan_harga'],
                        'id_harga'              => $data['id'][$i],
                        'harga_pokok_granit'    => $data['harga_pokok_granit'][$i],
                        'harga_pokok_keramik'   => $data['harga_pokok_keramik'][$i],
                        'harga_jual_granit'     => $data['harga_jual_granit'][$i],
                        'harga_jual_keramik'    => $data['harga_jual_keramik'][$i],
                        'jumlah'                => $data['jumlah'][$i],
                        'updated_by'            => SessionManagerWeb::getUserID()
                    );
                
                    $detail = $this->Detail_pengajuan_model->create($dataInsert, TRUE, TRUE);
                    
                    if (!$detail) {
                        $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga=".$data['id_pengajuan_harga']);
                        SessionManagerWeb::setFlashMsg(false, 'Gagal membuat pengajuan');
                        redirect('web/pengajuan_harga/getonepengajuan/'.$data['id_pengajuan_harga']);
                    }
                }

            }
            
            $data['id'] = $data['id_pengajuan_harga'];
            $data['harga_setuju_granit'] = $harga_setuju_granit;
            $data['harga_setuju_keramik'] = $harga_setuju_keramik;
            $data['harga_setuju_pokok_granit'] = $harga_setuju_pokok_granit;
            $data['harga_setuju_pokok_keramik'] = $harga_setuju_pokok_keramik;
            
            $insert = $this->model->updateHargaSetuju($data);

            if (!$insert) {
                SessionManagerWeb::setFlashMsg(false, 'Gagal membuat pengajuan');
                redirect('web/pengajuan_harga/getonepengajuan/'.$data['id_pengajuan_harga']);
            }

            $this->model->revisiHpp($data);

            // Mengubah data pengajuan_harga
            // $this->load->model('Pengajuan_harga_model');
            // $revisi = $this->Pengajuan_harga_model->revisiPengajuan($data, TRUE, FALSE);
            
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat revisi dan pengajuan');
            redirect('web/pengajuan_harga');

        } else {
            SessionManagerWeb::setFlashMsg(false, 'Gagal membuat revisi dan pengajuan');
            redirect('web/pengajuan_harga/getonepengajuan/'.$data['id_pengajuan_harga']);
        }

    }

    public function create() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        // echo '<pre>';var_dump($data);die();
        if (empty($data['keterangan'])) {
            $data['keterangan'] = '';
        }
        // echo "<pre>";print_r($data);die();
        $insert = $this->model->create($data, SessionManagerWeb::getUserID());
        
        if ($insert) {

            $countharga = count($data['id']);
            for ($i=0; $i < $countharga; $i++) {

                if ($data['jumlah'][$i] > 0) {
                    $dataInsert = array(
                        'id_pengajuan_harga'    => $insert,
                        'id_harga'              => $data['id'][$i],
                        'harga_pokok_granit'    => $data['harga_pokok_granit'][$i],
                        'harga_pokok_keramik'   => $data['harga_pokok_keramik'][$i],
                        'harga_jual_granit'     => $data['harga_jual_granit'][$i],
                        'harga_jual_keramik'    => $data['harga_jual_keramik'][$i],
                        'jumlah'                => $data['jumlah'][$i],
                        'updated_by'            => SessionManagerWeb::getUserID()
                    );

                    $this->load->model('Detail_pengajuan_model');
                    $detail = $this->Detail_pengajuan_model->create($dataInsert, TRUE, TRUE);
                    
                    if (!$detail) {
                        $hapus = dbQuery("delete from pengajuan_harga where id=".$insert);
                        $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga=".$insert);
                        SessionManagerWeb::setFlashMsg(false, 'Gagal membuat pengajuan');
                    }
                } 
                
            }
            
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat pengajuan');
            redirect("web/pengajuan_harga");

        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal membuat pengajuan');
            redirect("web/pengajuan_harga/getpengajuanadd");

        }
    }
    
    public function update() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] =  SessionManagerWeb::getUserID();
        }
        
        $insert = $this->model->updateHarga($data);
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil update harga');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal update harga');
        }
    }
    
    public function approveHarga() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        
        $insert = $this->model->approveHarga($data);
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil approve pengajuan');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal approve pengajuan');
        }
         redirect('web/pengajuan_harga/getonepengajuan/'.$data['id']);

    }
    
    public function updateHargaSetuju($data_contr) {
        if(empty($data_contr)){
            $data = $this->postData;
        }else{
            $data = $data_contr;
        }
        
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        
        $insert = $this->model->updateHargaSetuju($data);

        if($insert && $data_contr){
            return 1;
        }else{
            if($insert) {
                 SessionManagerWeb::setFlashMsg(true, 'Berhasil update pengajuan');
            }else{
                SessionManagerWeb::setFlashMsg(false, 'Gagal update pengajuan');
            }

            redirect('web/pengajuan_harga/getonepengajuan/'.$data['id']);
        }
        
        

    }
    
    public function tolakHarga() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        
        $insert = $this->model->tolakPengajuanHarga($data);
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil menolak pengajuan');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal menolak pengajuan');
        }
        
        redirect('web/pengajuan_harga/getonepengajuan/'.$data['id']);
    }
    
    public function ajukanHarga() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        // var_dump($data);die();
        $insert = $this->model->ajukanHarga($data);
        // var_dump($insert);die();
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil melakukan pengajuan');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal melakukan pengajuan');
        }

        redirect('web/pengajuan_harga/getonepengajuan/'.$data['id']);

    }
    
    public function revisiHpp() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        
        $insert = $this->model->revisiHpp($data);
        
        if ($insert) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil melakukan pengajuan');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal melakukan pengajuan');
        }
    }
    
    public function updateKonsumen() {
        $data = $this->postData;
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }

        $data['id'] = $data['id_pengajuan'];
        
        $update = $this->model->updateKonsumen($data);
        
        if ($update) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil update data');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal update data');
        }

        redirect('web/pengajuan_harga/getonepengajuan/'.$data['id']);
    }
    
    public function updateJenis() {
        $data = $this->postData;
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        $update = $this->model->updateJenis($data);
        
        if ($update) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil update data');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal update data');
        }
        
        redirect('web/pengajuan_harga/getonepengajuan/'.$data['id']);

    }
    
    public function ubahDiskon() {
        $data = $this->postData;
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        $update = $this->model->ubahDiskon($data);
        
        if ($update) {
             SessionManagerWeb::setFlashMsg(true, 'Berhasil update data');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal update data');
        }

        redirect('web/pengajuan_harga/getonepengajuan/'.$data['id_pengajuan_harga']);

    }
    
    public function getLabaRugiBulanan() {

        if ($this->postData['tahun']) {
            $_SESSION['labarugi']['tahun'] = $tahun = $this->postData['tahun'];
        }else{
            $_SESSION['labarugi']['tahun'] = $tahun = date('Y');
        }
        // $tahun = 2020;
        $this->data['data'] = $this->model->getLabaRugiBulanan($tahun);
        // echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function getDetailLaba() {
        $this->data['tahun'] = $tahun = $this->postData['tahun'];
        $this->data['bulan'] = $bulan = $this->postData['bulan'];

        $this->data['data'] = $this->model->getDetailLaba($bulan, $tahun);
        // echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function getDetailPengeluaran() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $this->data['data'] = $this->model->getDetailPengeluaran($bulan, $tahun);
        echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function deletePengajuan($id) {
        $hapus = dbQuery("update pengajuan_harga set is_deleted = '1' where id=".$id);

        SessionManagerWeb::setFlashMsg(true, 'Berhasil hapus data');
        redirect('web/pengajuan_harga');
        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data'), $hapus);
    }
    
    public function index_dev() {
        $data = $this->postData;
        
        $filter = "where 1 ";

        // if($this->postData['filterExist']){
        //     $filter .= $this->getFilter($this->postData);
        // }

        $this->data['data'] = $this->model->filter($filter)->getPengajuanDev($this->page,0, SessionManagerWeb::getRole(), $data['bulan'], $data['tahun']);
        
        echo '<pre>';var_dump($this->data);die();
        $this->template->viewDefault($this->view,$this->data);
    }
    
    public function revisi_detail_dev() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = SessionManagerWeb::getUserID();
        }
        
        // Hapus data detail yang lama
        $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga='".$data['id_pengajuan_harga']."'");
        
        $this->load->model('Detail_pengajuan_model');
        
        if ($hapus) {
            // Tambahkan data detail yang baru
            $hargaArray = json_decode($data['list_harga'], true);
                
            foreach ($hargaArray as $key => $value) {
                $dataInsert = array(
                    'id_pengajuan_harga'    => $data['id_pengajuan_harga'],
                    'id_harga'              => $value['id'],
                    'harga_pokok_granit'    => $value['harga_pokok_granit'],
                    'harga_pokok_keramik'   => $value['harga_pokok_keramik'],
                    'harga_jual_granit'     => $value['harga_jual_granit'],
                    'harga_jual_keramik'    => $value['harga_jual_keramik'],
                    'jumlah'                => $value['jumlah'],
                    'updated_by'            => SessionManagerWeb::getUserID()
                );
    
                $detail = $this->Detail_pengajuan_model->create($dataInsert, TRUE, TRUE);
            }
            
            
             SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat revisi');
        }else{
            SessionManagerWeb::setFlashMsg(false, 'Gagal membuat revisi');
        }
    }

    public function previewstrukcheckpdf($id,$jenis,$tipeharga){
        $this->load->model('Pengajuan_harga_model');

        $this->data['jenis'] = $jenis;
        $this->data['tipeharga'] = $tipeharga;

        $data = $this->Pengajuan_harga_model->filter($filter)->getOnePengajuan($this->page, $id);
        $data['nama'] = ($data['id_konsumen'] != 0) ? $data['nama'] : $data['nama_konsumen'];
        $data['alamat'] = ($data['id_konsumen'] != 0) ? $data['alamat'] : $data['alamat_konsumen'];
        $data['no_hp'] = ($data['id_konsumen'] != 0) ? $data['no_hp'] : $data['no_hp_konsumen'];
        $this->data['data'] = $data;
        $details = $this->Pengajuan_harga_model->filter($filter)->getDetailPengajuan($id);

        $result = array();
        foreach ($details as $k => $element) {
            $result[$element['nama_kategori']][$k] = $element;
            $result[$element['nama_kategori']][$k]['harga_pokok_granit_revisi'] = false;
            $result[$element['nama_kategori']][$k]['harga_pokok_keramik_revisi'] = false;

            if (SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::DIREKTUR) {
                $sql = "select harga_pokok_granit, harga_pokok_keramik from harga where id={$element['id_harga']}";
                $masterharga = dbGetRow($sql);
                if ($element['harga_pokok_granit'] != $masterharga['harga_pokok_granit']) {
                    $result[$element['nama_kategori']][$k]['harga_pokok_granit_revisi'] = true;
                }

                if ($element['harga_pokok_keramik'] != $masterharga['harga_pokok_keramik']) {
                    $result[$element['nama_kategori']][$k]['harga_pokok_keramik_revisi'] = true;
                }
            }
        }
        $this->data['detail'] = $result;
        // echo '<pre>';var_dump($this->data);die();

        $this->load->view(static::VIEW_PATH.'pembayaran_log_previewstrukpdf',$this->data);

        $html = $this->output->get_output();
// echo $html;
// die();
        $this->load->library('pdfdom');

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->set_option('isRemoteEnabled', TRUE);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("html_contents.pdf", array("Attachment"=> 0));
    }

    public function getstrukfull($id){
        

        $details = $this->Pengajuan_harga_model->filter($filter)->getDetailPengajuan($id);

        $this->load->model('Harga_model');

        $data = $this->Harga_model->filter($filter)->getHarga($this->page);

        $detail = array();
        $count = count($details) - 1;
        foreach ($data as $k => $element) {
            $detail[$element['kategori']][$k]['harga_pokok_granit_revisi'] = false;
            $detail[$element['kategori']][$k]['harga_pokok_keramik_revisi'] = false; 

            foreach ($details as $key => $val) {
                if ($element['id'] == $val['id_harga']) {
                    $detail[$val['nama_kategori']][$k] = $val;
                    $hasil = 1;

                    if (SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::DIREKTUR) {
                        if ($val['harga_pokok_granit'] != $element['harga_pokok_granit']) {
                            $detail[$val['nama_kategori']][$k]['harga_pokok_granit_revisi'] = true;
                        }

                        if ($val['harga_pokok_keramik'] != $element['harga_pokok_keramik']) {
                            $detail[$val['nama_kategori']][$k]['harga_pokok_keramik_revisi'] = true;
                        }
                    }
                    break;
                }else{
                    if ($count == $key) {
                        $hasil = 0;
                    }
                }
            }

            if ($hasil == 0){

                $harga['id'] = null;
                $harga['id_pengajuan_harga'] = null;
                $harga['id_harga'] = $element['id'];
                $harga['harga_pokok_granit'] = 0;
                $harga['harga_pokok_keramik'] = 0;
                $harga['harga_jual_granit'] = 0;
                $harga['harga_jual_keramik'] = 0;
                $harga['jumlah'] = 0;
                $harga['nama'] = $element['nama_harga'];
                $harga['nama_kategori'] = $element['kategori'];

                $detail[$element['kategori']][$k] = $harga;
            }

        }
        

        return $detail;

    }

    public function previewStrukPdf($id,$jenis,$tipeharga){
        $this->load->model('Pengajuan_harga_model');

        $this->data['jenis'] = $jenis;
        $this->data['tipeharga'] = $tipeharga;

        $data = $this->Pengajuan_harga_model->filter($filter)->getOnePengajuan($this->page, $id);
        $data['nama'] = ($data['id_konsumen'] != 0) ? $data['nama'] : $data['nama_konsumen'];
        $data['alamat'] = ($data['id_konsumen'] != 0) ? $data['alamat'] : $data['alamat_konsumen'];
        $data['no_hp'] = ($data['id_konsumen'] != 0) ? $data['no_hp'] : $data['no_hp_konsumen'];
        $this->data['data'] = $data;
        
        $details = $this->Pengajuan_harga_model->filter($filter)->getDetailPengajuan($id);

        $this->load->model('Harga_model');

        $data = $this->Harga_model->filter($filter)->getHarga($this->page);

        $detail = array();
        $count = count($details) - 1;
        foreach ($data as $k => $element) {
            $detail[$element['kategori']][$k]['harga_pokok_granit_revisi'] = false;
            $detail[$element['kategori']][$k]['harga_pokok_keramik_revisi'] = false; 

            foreach ($details as $key => $val) {
                if ($element['id'] == $val['id_harga']) {
                    $detail[$val['nama_kategori']][$k] = $val;
                    $hasil = 1;

                    if (SessionManagerWeb::getRole() == Role::PRODUKSI || SessionManagerWeb::getRole() == Role::DIREKTUR) {
                        if ($val['harga_pokok_granit'] != $element['harga_pokok_granit']) {
                            $detail[$val['nama_kategori']][$k]['harga_pokok_granit_revisi'] = true;
                        }

                        if ($val['harga_pokok_keramik'] != $element['harga_pokok_keramik']) {
                            $detail[$val['nama_kategori']][$k]['harga_pokok_keramik_revisi'] = true;
                        }
                    }
                    break;
                }else{
                    if ($count == $key) {
                        $hasil = 0;
                    }
                }
            }

            if ($hasil == 0){

                $harga['id'] = null;
                $harga['id_pengajuan_harga'] = null;
                $harga['id_harga'] = $element['id'];
                $harga['harga_pokok_granit'] = 0;
                $harga['harga_pokok_keramik'] = 0;
                $harga['harga_jual_granit'] = 0;
                $harga['harga_jual_keramik'] = 0;
                $harga['jumlah'] = 0;
                $harga['nama'] = $element['nama_harga'];
                $harga['nama_kategori'] = $element['kategori'];

                $detail[$element['kategori']][$k] = $harga;
            }

        }

        $this->data['detail'] = $detail;
        // echo '<pre>';var_dump($this->data);die();

        $this->load->view(static::VIEW_PATH.'pembayaran_log_previewstrukpdf',$this->data);


        $html = $this->output->get_output();
// echo $html;
// die();
        $this->load->library('pdfdom');

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->set_option('isRemoteEnabled', TRUE);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("html_contents.pdf", array("Attachment"=> 0));
    }
    
    public function ajaxgetsavetosession(){
        $data = $this->postData;

        $_SESSION['arr_data'] = $data;

        die();
    }

    public function ajaxpreviewpdf(){

        $this->data['jenis'] = $_SESSION['arr_data']['jenis'];
        $this->data['tipeharga'] = $_SESSION['arr_data']['tipeharga'];
        $data['kode_order'] = $_SESSION['arr_data']['kode_order'];
        $data['nama'] = $_SESSION['arr_data']['nama_konsumen'];
        $data['alamat'] = $_SESSION['arr_data']['alamat_konsumen'];
        $data['no_hp'] = $_SESSION['arr_data']['no_hp_konsumen'];

        $this->data['data'] = $data;
        $detail = $_SESSION['arr_data']['arrharga'];

        foreach ($detail as $key => $row) {
            unset($detail[$key]);
            $detail[$row[0]['kategori']] = $row;

            foreach ($row as $k => $v) {
                $detail[$row[0]['kategori']][$k]['nama'] = $v['nama_harga'];
                $detail[$row[0]['kategori']][$k]['jumlah'] = $v['jml'];
            }
        }
        $this->data['detail'] = $detail;

        $this->load->view(static::VIEW_PATH.'pembayaran_log_previewstrukpdf',$this->data);


        $html = $this->output->get_output();
// echo $html;
// die();
        $this->load->library('pdfdom');

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->set_option('isRemoteEnabled', TRUE);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("html_contents.pdf", array("Attachment"=> 0));
    }

    public function ajaxpreviewcheckpdf(){

        $this->data['jenis'] = $_SESSION['arr_data']['jenis'];
        $this->data['tipeharga'] = $_SESSION['arr_data']['tipeharga'];
        $data['kode_order'] = $_SESSION['arr_data']['kode_order'];
        $data['nama'] = $_SESSION['arr_data']['nama_konsumen'];
        $data['alamat'] = $_SESSION['arr_data']['alamat_konsumen'];
        $data['no_hp'] = $_SESSION['arr_data']['no_hp_konsumen'];

        $this->data['data'] = $data;
        $detail = $_SESSION['arr_data']['arrharga'];
// echo "<pre>";
        $result = array();

        // var_dump($detail);
        foreach ($detail as $key => $row) {
            
            unset($result[$key]);
            $result[$row[0]['kategori']] = $row;

            $count_item = 0;
            $i = 1;
            foreach ($row as $k => $v) {
                $jml = (int) $v['jml'];
                if ($jml > 0) {        
                    $result[$row[0]['kategori']][$k]['nama'] = $v['nama_harga'];
                    $result[$row[0]['kategori']][$k]['jumlah'] = $v['jml'];

                    $count_item += $i++;
                }else{
                    unset($result[$row[0]['kategori']][$k]);
                }
            }

            if ($count_item == 0) {
                unset($result[$row[0]['kategori']]);
            }
            
        }
        $this->data['detail'] = $result;
// var_dump($result);
        $this->load->view(static::VIEW_PATH.'pembayaran_log_previewstrukpdf',$this->data);


        $html = $this->output->get_output();
// echo $html;
// die();
        $this->load->library('pdfdom');

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->set_option('isRemoteEnabled', TRUE);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("html_contents.pdf", array("Attachment"=> 0));
    }

    public function getKpiMarketing(){
        $this->load->model('Pembayaran_log_model');
        // $satuan = 2;
        // $start = '2020-06-29';
        // $end   = '2020-08-02';

        // $start = '2020-27';
        // $end   = '2020-31';

       
        // echo $week;die();

        unset($_SESSION['kpi_marketing']['satuan']);
        unset($_SESSION['kpi_marketing']['start']);
        unset($_SESSION['kpi_marketing']['end']);

        $post = $this->postData;

        if ($post) {
            $satuan = (int) $post['satuan'];
            $start = xFormatDate($post['start']);
            $end = xFormatDate($post['end']);

            if ($satuan == 3) {
                $start = xFormatDate('01-'.$post['start']);

                $split = explode("-", $post['end']);
                $number = cal_days_in_month(CAL_GREGORIAN, $split[0], $split[1]);
                $end = xFormatDate($number.'-'.$post['end']);
            }
            // var_dump(xFormatDate($end));
            // die();    

            $_SESSION['kpi_marketing']['satuan'] = (int) $post['satuan'];
            $_SESSION['kpi_marketing']['start'] = $post['start'];
            $_SESSION['kpi_marketing']['end'] = $post['end'];


        
            if($satuan==2){
                $date_start = new DateTime($start);
                $week_start = $date_start->format("Y-W");
                // echo $week_start;die();
                $date_end = new DateTime($end);
                $week_end = $date_end->format("Y-W");
                $lead = $this->model->getLeadOrDeal($satuan, $week_start, $week_end);
                $deal = $this->model->getLeadOrDeal($satuan, $week_start, $week_end,'d');
                $pengeluaran = $this->Pembayaran_log_model->getPengeluaran($satuan, $week_start, $week_end);

                // echo "<pre>";print_r($lead);die();
            }
            else{
                $lead = $this->model->getLeadOrDeal($satuan, $start, $end);
                $deal = $this->model->getLeadOrDeal($satuan, $start, $end,'d');
                $pengeluaran = $this->Pembayaran_log_model->getPengeluaran($satuan, $start, $end);
                // echo "<pre>";print_r($lead);die();
            }
            
            
            // echo "<pre>";print_r($pengeluaran);die();
            // echo $start;die();
            // echo "<pre>";print_r($lead);echo "</pre>";
            // echo "<pre>";print_r($deal);die();
            

            $data = array();
            $index = 0;

            switch ($satuan) {
                case 1:
                    // echo $mulai;die();
                    $mulai    = (new DateTime($start));
                    $akhir    = (new DateTime($end));
                    $interval = DateInterval::createFromDateString('1 day');
                    $period   = new DatePeriod($mulai, $interval, $akhir);
                    foreach ($period as $dt) {
                        // echo $dt->format("Y-m") . "<br>\n";
                        $month = $dt->format("m");
                        $year = $dt->format("Y");
                        $tanggal = $dt->format("Y-m-d");
                        // echo $tahun_bulan."<br>";
                        $data[$index]['tanggal'] = $tanggal;
                        $data[$index]['tahun'] = $dt->format("Y");
                        $data[$index]['bulan'] = $dt->format("M");
                        $data[$index]['tanggal'] = $dt->format("d");
                        $data[$index]['label'] = $dt->format("d M Y");

                        $index_lead =  array_search($tanggal, array_column($lead, 'tgl_pengajuan'));
                        if ($index_lead === 0 || $index_lead > 0){
                            // echo "data lead=".$lead[$index]['jumlah']."<br>";
                            $data[$index]['lead'] = $lead[$index_lead]['jumlah'];
                        }
                        else{
                            $data[$index]['lead'] = 0;
                            // echo "data lead=0<br>";
                        }

                        $index_deal =  array_search($tanggal, array_column($deal, 'tgl_order_masuk'));
                        // echo $index."<br>";
                        if ($index_deal === 0 || $index_deal > 0){
                            // echo "data deal=".$deal[$index_deal]['jumlah']."<br>";
                            $data[$index]['deal'] = $deal[$index_deal]['jumlah'];
                            $data[$index]['harga_produksi'] = $deal[$index_deal]['harga_produksi'];
                            $data[$index]['omzet'] = $deal[$index_deal]['omzet'];
                        }
                        else{
                            $data[$index]['deal'] = 0;
                            $data[$index]['harga_produksi'] = 0;
                            $data[$index]['omzet'] = 0;
                            // echo "data deal=0<br>";
                        }

                        $data[$index]['persentase'] = ($data[$index]['deal']/$data[$index]['lead'])*100;
                        $data[$index]['profit'] = ($data[$index]['omzet'] - $data[$index]['harga_produksi']);

                        $index_pengeluaran =  array_search($tanggal, array_column($pengeluaran, 'tgl_input'));
                        // echo $index."<br>";
                        if ($index_pengeluaran === 0 || $index_pengeluaran > 0){
                            // echo "data deal=".$deal[$index_deal]['jumlah']."<br>";
                            $data[$index]['pengeluaran'] = $pengeluaran[$index_pengeluaran]['total_pengeluaran'];

                        }
                        else{
                            $data[$index]['pengeluaran'] = 0;
                            // echo "data deal=0<br>";
                        }

                        $data[$index]['margin_profit'] = $data[$index]['profit'] - $data[$index]['pengeluaran'];
                        $data[$index]['persentase_profit'] = ($data[$index]['margin_profit'] / $data[$index]['omzet'])*100;

                        $index +=1;
                        
                    }
                break;
                case 2:
                    $mulai    = (new DateTime($start));
                    $akhir    = (new DateTime($end));
                    $interval = DateInterval::createFromDateString('1 week');
                    $period   = new DatePeriod($mulai, $interval, $akhir);
                    foreach ($period as $dt) {
                        $tahun_pekan = $dt->format("Y-W");
                        $data[$index]['tahun_pekan'] = $tahun_pekan;
                        $data[$index]['tahun'] = $dt->format("Y");
                        $data[$index]['pekan'] = $dt->format("W");
                        $data[$index]['label'] = $dt->format("W Y");

                        $index_lead =  array_search($tahun_pekan, array_column($lead, 'tahun_pekan'));
                        if ($index_lead === 0 || $index_lead > 0){
                            $data[$index]['lead'] = $lead[$index_lead]['jumlah'];
                        }
                        else{
                            $data[$index]['lead'] = 0;
                        }

                        $index_deal =  array_search($tahun_pekan, array_column($deal, 'tahun_pekan'));
                        // echo $index."<br>";
                        if ($index_deal === 0 || $index_deal > 0){
                            // echo "data deal=".$deal[$index_deal]['jumlah']."<br>";
                            $data[$index]['deal'] = $deal[$index_deal]['jumlah'];
                            $data[$index]['harga_produksi'] = $deal[$index_deal]['harga_produksi'];
                            $data[$index]['omzet'] = $deal[$index_deal]['omzet'];
                        }
                        else{
                            $data[$index]['deal'] = 0;
                            $data[$index]['harga_produksi'] = 0;
                            $data[$index]['omzet'] = 0;
                            // echo "data deal=0<br>";
                        }

                        $data[$index]['persentase'] = ($data[$index]['deal']/$data[$index]['lead'])*100;
                        $data[$index]['profit'] = ($data[$index]['omzet'] - $data[$index]['harga_produksi']);

                        $index_pengeluaran =  array_search($tahun_pekan, array_column($pengeluaran, 'tahun_pekan'));
                        // echo $index."<br>";
                        if ($index_pengeluaran === 0 || $index_pengeluaran > 0){
                            // echo "data deal=".$deal[$index_deal]['jumlah']."<br>";
                            $data[$index]['pengeluaran'] = $pengeluaran[$index_pengeluaran]['total_pengeluaran'];

                        }
                        else{
                            $data[$index]['pengeluaran'] = 0;
                            // echo "data deal=0<br>";
                        }
                        $data[$index]['margin_profit'] = $data[$index]['profit'] - $data[$index]['pengeluaran'];
                        $data[$index]['persentase_profit'] = ($data[$index]['margin_profit'] / $data[$index]['omzet'])*100;

                        $index +=1;
                    }
                break;
                case 3:
                    $mulai    = (new DateTime($start))->modify('first day of this month');
                    $akhir    = (new DateTime($end))->modify('first day of next month');
                    $interval = DateInterval::createFromDateString('1 month');
                    $period   = new DatePeriod($mulai, $interval, $akhir);
                    foreach ($period as $dt) {
                        // echo $dt->format("Y-m") . "<br>\n";
                        $month = $dt->format("m");
                        $year = $dt->format("Y");
                        $tahun_bulan = $dt->format("Y-m");
                        // echo $tahun_bulan."<br>";
                        $data[$index]['tahun_bulan'] = $tahun_bulan;
                        $data[$index]['tahun'] = $dt->format("Y");
                        $data[$index]['bulan'] = $dt->format("M");
                        $data[$index]['label'] = $dt->format("M Y");

                        $index_lead =  array_search($tahun_bulan, array_column($lead, 'tahun_bulan'));
                        if ($index_lead === 0 || $index_lead > 0){
                            // echo "data lead=".$lead[$index]['jumlah']."<br>";
                            $data[$index]['lead'] = $lead[$index_lead]['jumlah'];
                        }
                        else{
                            $data[$index]['lead'] = 0;
                            // echo "data lead=0<br>";
                        }

                        $index_deal =  array_search($tahun_bulan, array_column($deal, 'tahun_bulan'));
                        // echo $index."<br>";
                        if ($index_deal === 0 || $index_deal > 0){
                            // echo "data deal=".$deal[$index_deal]['jumlah']."<br>";
                            $data[$index]['deal'] = $deal[$index_deal]['jumlah'];
                            $data[$index]['harga_produksi'] =($deal[$index_deal]['harga_produksi'] == null || $deal[$index_deal]['harga_produksi'] == '') ? 0 : $deal[$index_deal]['harga_produksi'];
                            $data[$index]['omzet'] = ($deal[$index_deal]['omzet'] == null || $deal[$index_deal]['omzet'] == '') ? 0 : $deal[$index_deal]['omzet'];
                        }
                        else{
                            $data[$index]['deal'] = 0;
                            $data[$index]['harga_produksi'] = 0;
                            $data[$index]['omzet'] = 0;
                            // echo "data deal=0<br>";
                        }

                        $data[$index]['persentase'] = ($data[$index]['deal']/$data[$index]['lead'])*100;
                        $data[$index]['profit'] = ($data[$index]['omzet'] - $data[$index]['harga_produksi']);

                        $index_pengeluaran =  array_search($tahun_bulan, array_column($pengeluaran, 'tahun_bulan'));
                        // echo $index."<br>";
                        if ($index_pengeluaran === 0 || $index_pengeluaran > 0){
                            // echo "data deal=".$deal[$index_deal]['jumlah']."<br>";
                            $data[$index]['pengeluaran'] = $pengeluaran[$index_pengeluaran]['total_pengeluaran'];

                        }
                        else{
                            $data[$index]['pengeluaran'] = 0;
                            // echo "data deal=0<br>";
                        }

                        $data[$index]['margin_profit'] = $data[$index]['profit'] - $data[$index]['pengeluaran'];
                        $data[$index]['persentase_profit'] = ($data[$index]['margin_profit'] / $data[$index]['omzet'])*100;

                        $index +=1;
                        
                    }
                break;
                
            }
        
            $this->data['data'] = $data;
        }

        
        // echo "<pre>";print_r($data);die();
        $this->template->viewDefault($this->view,$this->data);
        

    }
}

?>