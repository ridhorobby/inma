<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends PrivateApiController {

    public function index() {
        $tags = $this->model->order_by('name', 'asc')->get_all();

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $tags);
    }

    public function getListTag() {
    	$tags = $this->model->getListTagsAndroid();
    	$this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $tags);
    }

}

?>