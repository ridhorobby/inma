<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Fee_desainer extends PrivateApiController {

    const TAMPILAN_DESAINER = 'D';
    const TAMPILAN_ORDER = 'O';
    function __construct() {
        parent::__construct();
        $this->load->model('Pesanan_model');
    }

    public function index() {
        $data = $this->postData;
        

        // $this->data = $this->Pesanan_model->filter($filter)->getDesainerAssignment($data['bulan'], $data['tahun']);
        if($data['tampilan'] == self::TAMPILAN_DESAINER){
            $this->data = $this->User_model->column("*")->filter("where role=!'".Role::ADMINISTRATOR."' and is_active=1")->getUser();
        }else{
            $this->data = $this->Pesanan_model->filter($filter)->getFeeFromPesanan($data['bulan'], $data['tahun']);
        }
        
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function detail($desainer_id) {
        $data = $this->postData;
        
        $this->load->model('Pesanan_model');
        // echo "<pre>";print_r($_SESSION['order_desainer']['tahun']);die();
        $this->data = $this->Pesanan_model->filter($filter)->getOrderDesainer($desainer_id,$data['bulan'], $data['tahun']);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

    public function feeDesain($pesanan_id, $desainer_id){
        if(!in_array($this->user['role'], array(Role::DIREKTUR, Role::ADMINISTRATOR))){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        }
        $data = $this->postData;
        $this->data = $this->model->getFeeRow($pesanan_id, $desainer_id);
        if($this->data){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Fee Desainer ditemukan, edit untuk mengubah'), $this->data);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Fee Desainer belum ada, silahkan tambahkan fee'));
        }
    }

    public function gajiDesain($desainer_id){
        if(!in_array($this->user['role'], array(Role::DIREKTUR, Role::ADMINISTRATOR))){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        }
        $data = $this->postData;
        $this->load->model('Gaji_desainer_model');
        $filter = " and bulan='".$data['bulan']."' and tahun='".$data['tahun']."'";
        $this->data = $this->Gaji_desainer_model->column('*')->filter($filter)->getGajiRow($desainer_id);
        if($this->data){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Gaji Desainer ditemukan, edit untuk mengubah'), $this->data);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'gaji Desainer belum ada, silahkan tambahkan fee'));
        }
    }

    public function create() {
        if(!in_array($this->user['role'], array(Role::DIREKTUR, Role::ADMINISTRATOR))){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        }
        if($this->input->post('desainer_design_id')){
            $data['desainer_design_id'] = $this->input->post('desainer_design_id');
            $data['fee_design'] = $this->input->post('fee_design');
        }
        if($this->input->post('desainer_eksekusi_id')){
            $data['desainer_eksekusi_id'] = $this->input->post('desainer_eksekusi_id');
            $data['fee_eksekusi'] = $this->input->post('fee_eksekusi');
            $data['persentase_eksekusi'] = $this->input->post('persentase_eksekusi');
        
        }

        $data['pesanan_id'] = $this->input->post('pesanan_id');

        $check = $this->model->getFeeRow($data['pesanan_id']);
        if($check){
            unset($data['pesanan_id']);
            $insert = $this->model->edit($data, $check['id']);
        }else{
            $insert = $this->model->create($data);
        }
        // $insert = $this->model->create($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat fee untuk desainer'), $pengajuan_id);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat fee untuk desainer'));
        }
    }

    public function createPokok(){
        if(!in_array($this->user['role'], array(Role::DIREKTUR, Role::ADMINISTRATOR))){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        }
        $this->load->model('Gaji_desainer_model');
        $data = $this->postData;
        
        $insert = $this->Gaji_desainer_model->create($data);
        
        if ($insert) {
            
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menambahkan gaji'), $pengajuan_id);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal menambahkan gaji'));
        }
    }

    public function update($fee_desainer_id) {
        if(!in_array($this->user['role'], array(Role::DIREKTUR, Role::ADMINISTRATOR))){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        }
        $data = $this->postData;

        
        $update = $this->model->edit($data, $fee_desainer_id);
        
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update fee desainer '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update fee desainer'));
        }
    }

    public function updatePokok($gaji_desainer_id) {
        if(!in_array($this->user['role'], array(Role::DIREKTUR, Role::ADMINISTRATOR))){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        }
        $this->load->model('Gaji_desainer_model');
        $data = $this->postData;

        
        $update = $this->Gaji_desainer_model->edit($data, $gaji_desainer_id);
        
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update gaji desainer '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update gaji desainer'));
        }
    }
    
    public function getGaji($desainer_id) {
        $this->load->model('Pesanan_model');
        $this->load->model('Gaji_desainer_model');
        $postData = $this->postData;
        $fee = $this->Pesanan_model->filter($filter)->getOrderDesainer($desainer_id,$postData['bulan'], $postData['tahun']);
        $data['fee_design'] = 0;
        $data['fee_eksekusi'] = 0;

        foreach ($fee as $key => $value) {
            if($value['desainer_design'] == $desainer_id){
                $data['fee_design'] += $value['fee_design'];
            }
            if($value['desainer_eksekusi'] == $desainer_id){
                $data['fee_eksekusi'] += $value['fee_eksekusi'];
            }
        }
        $filter = " and bulan='".$postData['bulan']." and ' tahun='".$postData['tahun']."'";
        $gaji = $this->Gaji_desainer_model->column('*')->filter($filter)->getGajiRow($desainer_id);
        $data['gaji_pokok'] = $gaji['gaji_pokok'];
        $data['gaji_tunjangan'] = $gaji['gaji_tunjangan'];

        $data['total_gaji'] = $data['fee_design'] + $data['fee_eksekusi'] + $data['gaji_pokok'] + $data['gaji_tunjangan'];

        $this->data = $data;
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

    


    
    
    
    
    
    
    
    
   
    
}

?>