<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Harga_maintenance extends PrivateApiController {

    public function index() {
        $filter = "where 1 ";

        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }

        $this->data = $this->model->filter($filter)->getHargaMaintenance($this->page);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }



    public function create() {
        $data = $this->postData;
        $insert = $this->model->create($data, $this->user['id']);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat harga maintenance'), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat harga maintenance'));
        }
    }
    
    public function update() {
        $data = $this->postData;
        $update = $this->model->updateHargaMaintenance($data);
        
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update harga maintenance'), $update);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update harga maintenance'));
        }
        redirect('web/'.$this->class);
    }

    public function delete() {
        $data = $this->postData;
        $delete = $this->model->deleteHargaMaintenance($data['id']);
        
        if ($delete) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil hapus harga maintenance'), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal hapus harga maintenance'));
        }
    }
    
}

?>