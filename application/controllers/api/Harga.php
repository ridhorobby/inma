<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Harga extends PrivateApiController {

    function __construct() {
        parent::__construct();
    }


    // public function tesnotif(){
    //     $this->load->model('Notification_model');
    //     $this->Notification_model->generate(Notification_model::ACTION_PESANAN_CREATE, 67,$this->user['id']);
    //     // $receiver_id = 18;
    //     // $message = "ini adalah message";
    //     // $reference_id = 65;
    //     // $notification_user_id = 13;
    //     // $this->Notification_model->mobileNotification($receiver_id, $message, $reference_id,$notification_user_id, $type='pesanan');
    // }

    public function index() {
        $filter = "where 1 ";

        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }

        $this->data = $this->model->filter($filter)->getHarga($this->page);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

    public function create() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        $insert = $this->model->create($data, $this->user['id']);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat harga '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat harga'));
        }
    }
    
    public function update() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        $insert = $this->model->updateHarga($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update harga '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update harga'));
        }
    }

    public function delete() {
        $data = $this->postData;
        $delete = $this->model->deleteHarga($data['id']);
        
        if ($delete) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil hapus harga '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal hapus harga'));
        }
    }
    
}

?>