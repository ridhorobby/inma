<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan_harga_komplain extends PrivateApiController {

    function __construct() {
        parent::__construct();
    }


    // public function tesnotif(){
    //     $this->load->model('Notification_model');
    //     $this->Notification_model->generate(Notification_model::ACTION_PESANAN_CREATE, 67,$this->user['id']);
    //     // $receiver_id = 18;
    //     // $message = "ini adalah message";
    //     // $reference_id = 65;
    //     // $notification_user_id = 13;
    //     // $this->Notification_model->mobileNotification($receiver_id, $message, $reference_id,$notification_user_id, $type='pesanan');
    // }

    public function index() {
        $data = $this->postData;
        
        $filter = "where 1 ";

        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }

        $this->data = $this->model->filter($filter)->getPengajuan($this->page, $data['status'], $this->user['role'], $data['bulan'], $data['tahun']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getOnePengajuan() {
        $data = $this->postData;
        
        $filter = "where 1 ";

        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }

        $this->data = $this->model->filter($filter)->getOnePengajuan($this->page, $data['id_pengajuan']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getDetailPengajuan($id) {
        $this->data = $this->model->filter($filter)->getDetailPengajuan($id);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

    public function create() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        
        $insert = $this->model->create($data, $this->user['id']);
        
        if ($insert) {
            $hargaArray = json_decode($data['list_harga'], true);
                
            foreach ($hargaArray as $key => $value) {
                $dataInsert = array(
                    'id_pengajuan_harga_komplain'   => $insert,
                    'id_harga_maintenance'          => $value['id'],
                    'harga'                         => $value['harga_maintenance'],
                    'jumlah'                        => $value['jumlah'],
                    'updated_by'                    => $this->user['id']
                );
                
                // var_dump($dataInsert);
                // die();

                $this->load->model('Detail_pengajuan_model');
                $detail = $this->Detail_pengajuan_komplain_model->create($dataInsert, TRUE, TRUE);
                
                if (!$detail) {
                    $hapus = dbQuery("delete from pengajuan_harga where id=".$insert);
                    $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga=".$insert);
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat pengajuan'));
                }
            }
            
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat pengajuan '), $pengajuan_id);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat pengajuan'));
        }
    }
    
    public function update() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->updateHarga($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update harga '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update harga'));
        }
    }
    
    public function approveHarga() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->approveHarga($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil approve pengajuan '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal approve pengajuan'));
        }
    }
    
    public function updateHargaSetuju() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->updateHargaSetuju($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update pengajuan '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update pengajuan'));
        }
    }
    
    public function tolakHarga() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->tolakPengajuanHarga($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menolak pengajuan '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal menolak pengajuan'));
        }
    }
    
    public function ajukanHarga() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->ajukanHarga($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
        }
    }
    
    public function revisiHpp() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->revisiHpp($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
        }
    }
    
    public function updateKonsumen() {
        $data = $this->postData;
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        $update = $this->model->updateKonsumen($data);
        
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data'), $update);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update data'));
        }
    }
    
    // public function updateJenis() {
    //     $data = $this->postData;
    //     if ($data['user_id']=="") {
    //         $data['user_id'] = $this->user['id'];
    //     }
    //     $update = $this->model->updateJenis($data);
        
    //     if ($update) {
    //         $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data'), $update);
    //     } else {
    //         $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update data'));
    //     }
    // }
    
    // public function ubahDiskon() {
    //     $data = $this->postData;
    //     if ($data['user_id']=="") {
    //         $data['user_id'] = $this->user['id'];
    //     }
    //     $update = $this->model->ubahDiskon($data);
        
    //     if ($update) {
    //         $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data'), $update);
    //     } else {
    //         $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update data'));
    //     }
    // }
    
    public function getLabaRugiBulanan() {
        $tahun = $this->postData['tahun'];

        $this->data = $this->model->getLabaRugiBulanan($tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getDetailLaba() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $this->data = $this->model->getDetailLaba($bulan, $tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getDetailPengeluaran() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $this->data = $this->model->getDetailPengeluaran($bulan, $tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function deletePengajuan($id) {
        $hapus = dbQuery("update pengajuan_harga set is_deleted = '1' where id=".$id);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data'), $hapus);
    }
    
}

?>