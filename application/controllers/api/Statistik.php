<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik extends PrivateApiController {

    public function getPerfomance() {
        $data = $this->postData;
        $statistik['temp_all'] = $this->model->getUserStat($data['helpdesk'],$data['date_start'],$data['date_end'],false);
        $statistik['data_statistik'] = $this->model->getUserStatRatingedTicket($data['helpdesk'],$data['date_start'],$data['date_end'], false);
        $statistik['std_all'] = $this->model->getStd($data['helpdesk'],$data['date_start'],$data['date_end'], false);
        $statistik['std'] = $this->model->getStdRatingedTicket($data['helpdesk'],$data['date_start'],$data['date_end'], false);

        $counter=0;
        foreach ($statistik['temp_all'] as $key => $value) {
            $is_in = 0;
            foreach ($statistik['data_statistik'] as $k => $v) {
                if ($value['id']==$v['id']){
                    $counter++;
                    $is_in=1;
                }
            }
            if ($is_in==0) {
                $counter++;
                $statistik['data_statistik'][] = $value;
            }
        }
        foreach ($statistik['data_statistik'] as &$data){
            $data['track'] = "round((0.5*".(string)$data['s']."/".(string)$statistik['std'][0]['avg_s']."+0.4*".(string)$statistik['std'][0]['avg_process']."/".(string)$data['processing_time']."+0.1* ".(string)$statistik['std'][0]['avg_response']."/".(string)$data['response_time'].")*".(string)$data['solvability']."/10,3)";
            $data['poin'] = round((0.5*$data['s']/$statistik['std'][0]['avg_s']+0.4*$statistik['std'][0]['avg_process']/$data['processing_time']+0.1*$statistik['std'][0]['avg_response']/$data['response_time'])*$data['solvability']/10,3);

        }
        $statistik['std_all'][0]['avg_response'] = (int) $statistik['std_all'][0]['avg_response'];
        $statistik['std_all'][0]['avg_process'] = (int) $statistik['std_all'][0]['avg_process'];
        $statistik['std_all'][0]['avg_solving'] = (int) $statistik['std_all'][0]['avg_solving'];
        $statistik['std'][0]['avg_response'] = (int) $statistik['std'][0]['avg_response'];
        $statistik['std'][0]['avg_process'] = (int) $statistik['std'][0]['avg_process'];
        $statistik['std'][0]['avg_solving'] = (int) $statistik['std'][0]['avg_solving'];
        // Change null to 0
        // if ($statistik['std_all']['avg_response']==null) {
        //  $statistik['std_all']['avg_response']==0;
        // }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $statistik);
    }

    public function getPerfomanceKopertis() {
        $data = $this->postData;
        $statistik['temp_all'] = $this->model->getUserStat(NULL,$data['date_start'],$data['date_end'],TRUE);
        $statistik['data_statistik'] = $this->model->getUserStatRatingedTicket(NULL,$data['date_start'],$data['date_end'], TRUE);
        $statistik['std_all'] = $this->model->getStd(NULL,$data['date_start'],$data['date_end'], TRUE);
        $statistik['std'] = $this->model->getStdRatingedTicket(NULL,$data['date_start'],$data['date_end'], TRUE);

        $counter=0;
        foreach ($statistik['temp_all'] as $key => $value) {
            $is_in = 0;
            foreach ($statistik['data_statistik'] as $k => $v) {
                if ($value['id']==$v['id']){
                    $counter++;
                    $is_in=1;
                }
            }
            if ($is_in==0) {
                $counter++;
                $statistik['data_statistik'][] = $value;
            }
        }
        foreach ($statistik['data_statistik'] as &$data){
            $data['track'] = "round((0.5*".(string)$data['s']."/".(string)$statistik['std'][0]['avg_s']."+0.4*".(string)$statistik['std'][0]['avg_process']."/".(string)$data['processing_time']."+0.1* ".(string)$statistik['std'][0]['avg_response']."/".(string)$data['response_time'].")*".(string)$data['solvability']."/10,3)";
            $data['poin'] = round((0.5*$data['s']/$statistik['std'][0]['avg_s']+0.4*$statistik['std'][0]['avg_process']/$data['processing_time']+0.1*$statistik['std'][0]['avg_response']/$data['response_time'])*$data['solvability']/10,3);

        }
        $statistik['std_all'][0]['avg_response'] = (int) $statistik['std_all'][0]['avg_response'];
        $statistik['std_all'][0]['avg_process'] = (int) $statistik['std_all'][0]['avg_process'];
        $statistik['std_all'][0]['avg_solving'] = (int) $statistik['std_all'][0]['avg_solving'];
        $statistik['std'][0]['avg_response'] = (int) $statistik['std'][0]['avg_response'];
        $statistik['std'][0]['avg_process'] = (int) $statistik['std'][0]['avg_process'];
        $statistik['std'][0]['avg_solving'] = (int) $statistik['std'][0]['avg_solving'];

        $this->load->model('Group_member_model');
        foreach ($statistik['data_statistik'] as $key => $value) {
            $kopertis = $this->Group_member_model->getAdminKopertis($value['id']);
            $statistik['data_statistik'][$key]['name'] = $value['name'].'( '.$kopertis['name'].' )';
        }
        // Change null to 0
        // if ($statistik['std_all']['avg_response']==null) {
        //  $statistik['std_all']['avg_response']==0;
        // }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $statistik);
    }

    public function getRating() {
        $data = $this->postData;
        $statistik['data_rating'] = $this->model->getStatRating($data['helpdesk'],$data['date_start'],$data['date_end']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $statistik);
    }

    // public function getTicket() {
    //     $data = $this->postData;

    //     $statistik['data_statistik']=$this->model->getStatTicket($data['date_start'],$data['date_end']);
    //     $index=0;
    //     $statistik['month']=array();
    //     $statistik['ticket_open']=array();
    //     $statistik['ticket_solved'] = array();
    //     foreach ($statistik['data_statistik'] as $d) {
    //         $d['month'][$index]=date('M', mktime(0, 0, 0, (int)$d['month'], 10)).'-'.$d['year'];
    //         if ($d['open']==NULL){
    //             $d['ticket_open'][$index]=0;
    //         } else {
    //             $d['ticket_open'][$index]=(int)$d['open'];
    //         }
    //         if ($d['solved']==NULL){
    //             $d['ticket_solved'][$index]=0;
    //         } else {
    //             $d['ticket_solved'][$index]=(int)$d['solved'];
    //         }
    //         $index++;
    //     }

    //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $statistik);
    // }
    public function getTicket() {
        $data = $this->postData;

        $statistik = $this->model->getTicketRange($data['helpdesk'],$data['date_start'],$data['date_end']);
        $created = array();
        $solved = array();
        foreach ($statistik as $k => $d) {
            $created_arr = $this->model->getTicketCreated($data['helpdesk'],$d['month'],$d['year']);
            $created[] = $created_arr['created'];
            $solved_arr = $this->model->getTicketSolved($data['helpdesk'],$d['month'],$d['year']);
            $solved[] = $solved_arr['solved'];
            $statistik['data_statistik'][$k]['month'] = $d['month'];
            $statistik['data_statistik'][$k]['year'] = $d['year'];
            $statistik['data_statistik'][$k]['open'] = $created_arr['created'];
            $statistik['data_statistik'][$k]['solved'] = $solved_arr['solved'];
        }
        $index=0;
        $statistik['month']=array();
        $statistik['ticket_open']=array();
        $statistik['ticket_solved'] = array();
        foreach ($statistik['data_statistik'] as $d) {
            $statistik['month'][$index]=date('M', mktime(0, 0, 0, (int)$d['month'], 10)).'-'.$d['year'];
            if ($d['open']==NULL){
                $statistik['ticket_open'][$index]=0;
            } else {
                $statistik['ticket_open'][$index]=(int)$d['open'];
            }
            if ($d['solved']==NULL){
                $statistik['ticket_solved'][$index]=0;
            } else {
                $statistik['ticket_solved'][$index]=(int)$d['solved'];
            }
            $index++;
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $statistik);
    }

    public function getTime() {
        $data = $this->postData;

        $statistik['data_statistik'] = $this->model->getStatWaktu($data['helpdesk'],$data['date_start'],$data['date_end']);
        $statistik['month'] = array();
        // $statistik['year'] = array();
        $statistik['response_time']=array();
        $statistik['processing_time']=array();
        $statistik['solving_time']=array();
        $index=0;

        foreach ($statistik['data_statistik'] as $d) {
            $d['month'][$index]=date('M', mktime(0, 0, 0, (int)$d['month'], 10)).'-'.$d['year'];
            // $d['year']=$d['year'];
            $d['response_time'][$index]=floor($d['response_time']/60/24);
            $d['processing_time'][$index]=floor($d['processing_time']/60/24);
            $d['solving_time'][$index]=floor($d['solving_time']/60/24);
            $index++;
        }

        if ($statistik['data_statistik']===FALSE) {
            $statistik['data_statistik'] = array();
        }   

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $statistik);
    }

    public function getBeban() {
        $data = $this->postData;
        $statistik['data_statistik'] = $this->model->getUserBeban($data['helpdesk']);
        $statistik['avg_bobot'] = $this->model->getAvgUserBeban($data['helpdesk']);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $statistik);
    }

    public function getBebanKopertis() {
        $data = $this->postData;
        $statistik['data_statistik'] = $this->model->getUserBeban(null, true);
        $statistik['avg_bobot'] = $this->model->getAvgUserBeban(null, true);

        $this->load->model('Group_member_model');
        foreach ($statistik['data_statistik'] as $key => $value) {
            $kopertis = $this->Group_member_model->getAdminKopertis($value['id']);
            $statistik['data_statistik'][$key]['name'] = $value['name'].'( '.$kopertis['name'].' )';
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $statistik);
    }

    public function getStatKeyword() {
        $data = $this->postData;
        $statistik = $this->model->getStatKeyword();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $statistik);
    }

    public function getStatSearch() {
        $data = $this->postData;
        $statistik = $this->model->getStatSearch($data['date_start'], $data['date_end']);
        // $statistik = $this->model->getStatSearch("1-5-2018","30-6-2018");
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $statistik);
    }

}

?>