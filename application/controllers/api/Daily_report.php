<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Daily_report extends PrivateApiController {

    public function me() {
        $dailyReports = $this->model->getMe($this->user['id'], $this->page, $this->postData['param']);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $dailyReports);
    }

    public function for_me() {
        if ($this->user['role']['id'] == Role::ADMINISTRATOR) {
            $dailyReport = $this->model->getAll($this->user['id'], $this->page, $this->postData['param']);
        } else {
            $dailyReport = $this->model->getForMe($this->user['id'], $this->page, $this->postData['param']);
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $dailyReport);
    }

    public function create() {
        $data = $this->postData;
        $data['user_id'] = $this->user['id'];
        $data['image'] = isset($_FILES['image']) ? $_FILES['image']['name'] : NULL;
        $data['video'] = isset($_FILES['video']) ? $_FILES['video']['name'] : NULL;
        $data['file'] = isset($_FILES['file']) ? $_FILES['file']['name'] : NULL;

        $insert = $this->model->create($data);
        if ($insert === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat laporan harian'));
        } elseif (is_string($insert)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat laporan harian'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }
    }

    public function detail($id) {
        $dailyReport = $this->model->get_by('posts.id', $id);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $dailyReport);
    }

    public function delete($id) {
        $dailyReport = $this->model->get_by('posts.id', $id);
        if ($dailyReport['userId'] == $this->user['id']) {
            $this->load->model('Post_model');
            $delete = $this->Post_model->delete($id);
            if ($delete === TRUE) {
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menghapus laporan harian'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal menghapus laporan harian"));
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Tidak dapat menghapus laporan harian ini"));
        }
    }

    public function recapitulation() {
        if ($this->user['role']['id'] == Role::ADMINISTRATOR || $this->user['role']['id'] == Role::MANAGEMENT) {
            $recapitulations = $this->model->getRecapitulation($this->postData['month'], $this->postData['year']);
        } else {
            $recapitulations = $this->model->getRecapitulation($this->postData['month'], $this->postData['year'], $this->user['id']);
        }

        $this->load->model('Work_day_model');
        $workDays = $this->Work_day_model->getCount($this->postData['month'], $this->postData['year']);
        foreach ($recapitulations as $key => $value) {
            $recapitulations[$key]['percentage'] = round($value['countreport'] / $workDays, 4);
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $recapitulations);
    }

}
