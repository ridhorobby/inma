<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Detail_pengajuan extends PrivateApiController {

    function __construct() {
        parent::__construct();
    }


    // public function tesnotif(){
    //     $this->load->model('Notification_model');
    //     $this->Notification_model->generate(Notification_model::ACTION_PESANAN_CREATE, 67,$this->user['id']);
    //     // $receiver_id = 18;
    //     // $message = "ini adalah message";
    //     // $reference_id = 65;
    //     // $notification_user_id = 13;
    //     // $this->Notification_model->mobileNotification($receiver_id, $message, $reference_id,$notification_user_id, $type='pesanan');
    // }

    public function getDetailPengajuan($id) {
        $this->data = $this->model->filter($filter)->getDetailPengajuan($id);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function updateOneDetail() {
        $data = $this->postData;
        $update = $this->model->filter($filter)->updateHarga($data);
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update harga '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update harga'));
        }
    }
    
    public function addDetail() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        
        $dataInsert = array(
            'id_pengajuan_harga'    => $data['id_pengajuan'],
            'id_harga'              => $data['id'],
            'harga_pokok_granit'    => $data['harga_pokok_granit'],
            'harga_pokok_keramik'   => $data['harga_pokok_keramik'],
            'harga_jual_granit'     => $data['harga_jual_granit'],
            'harga_jual_keramik'    => $data['harga_jual_keramik'],
            'jumlah'                => $data['jumlah'],
            'updated_by'            => $this->user['id']
        );
        
        $detail = $this->model->create($dataInsert, TRUE, TRUE);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menambah detail '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal menambah detail'));
        }
    }
    
    public function deleteOneDetail() {
        $update = $this->model->filter($filter)->deleteDetail($data);
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil hapus detail '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal hapus detail'));
        }
    }
    
    public function revisiDetail() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        // echo "<pre>"; var_dump($hapus); die();
        // Hapus data detail yang lama
        $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga='".$data['id_pengajuan_harga']."'");
        
        
        if ($hapus) {
            // Tambahkan data detail yang baru
            $hargaArray = json_decode($data['list_harga'], true);
                
            foreach ($hargaArray as $key => $value) {
                $dataInsert = array(
                    'id_pengajuan_harga'    => $data['id_pengajuan_harga'],
                    'id_harga'              => $value['id'],
                    'harga_pokok_granit'    => $value['harga_pokok_granit'],
                    'harga_pokok_keramik'   => $value['harga_pokok_keramik'],
                    'harga_jual_granit'     => $value['harga_jual_granit'],
                    'harga_jual_keramik'    => $value['harga_jual_keramik'],
                    'jumlah'                => $value['jumlah'],
                    'updated_by'            => $this->user['id']
                );
    
                $detail = $this->model->create($dataInsert, TRUE, TRUE);
                
                if (!$detail) {
                    $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga=".$data['id_pengajuan_harga']);
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat pengajuan'));
                }
            }
            
            // Mengubah data pengajuan_harga
            // $this->load->model('Pengajuan_harga_model');
            // $revisi = $this->Pengajuan_harga_model->revisiPengajuan($data, TRUE, FALSE);
            
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat revisi '), $revisi);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat revisi'));
        }
    }
    
    public function revisi_detail_dev() {
        echo "<pre>"; var_dump($hapus); die();
        // $data = $this->postData;
        
        // // if ($data['user_id']=="") {
        // //     $data['user_id'] = $this->user['id'];
        // // }
        
        // // Hapus data detail yang lama
        // $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga='".$data['id_pengajuan_harga']."'");
        // // $hapus = true;
        
        
        // if ($hapus) {
        //     // Tambahkan data detail yang baru
        //     $hargaArray = json_decode($data['list_harga'], true);
                
        //     foreach ($hargaArray as $key => $value) {
        //         $dataInsert = array(
        //             'id_pengajuan_harga'    => $data['id_pengajuan_harga'],
        //             'id_harga'              => $value['id'],
        //             'harga_pokok_granit'    => $value['harga_pokok_granit'],
        //             'harga_pokok_keramik'   => $value['harga_pokok_keramik'],
        //             'harga_jual_granit'     => $value['harga_jual_granit'],
        //             'harga_jual_keramik'    => $value['harga_jual_keramik'],
        //             'jumlah'                => $value['jumlah'],
        //             'updated_by'            => $this->user['id']
        //         );
    
        //         $detail = $this->model->create($dataInsert, TRUE, TRUE);
        //     }
            
        //     // Mengubah data pengajuan_harga
        //     // $this->load->model('Pengajuan_harga_model');
        //     // $revisi = $this->Pengajuan_harga_model->revisiPengajuan($data, TRUE, FALSE);
            
        //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat revisi '), $revisi);
        // } else {
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat revisi'));
        // }
    }
    
}

?>