<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends PublicApiController {

    public function login() {
        // echo $this->input->post('username');die();
        $data = AuthManager::login($this->input->post('username'), $this->input->post('password'), $this->input->post('device'));

        if (is_string($data)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $data));
        } else {
            list($message, $token, $user) = $data;
            $this->load->model('User_token_model');
            $user['userToken'] = $this->User_token_model->with('Device')->get_by(array('token' => $token));
            $user['resetExist'] = dbGetOne("select id from reset_password where user_id=".$user['id']);
            $user['listRole'] = Role::getArray();
            $this->systemResponse->token = $token;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $user);
        }
    }

    public function register(){
        // echo "<pre>";print_r($this->input->post());die();
        $postData = $this->input->post();
        $this->load->model('User_model');
        $auth_key = SecurityManager::generateAuthKey();
        $dataInsert = array(
            'name'          => $postData['name'],
            'username'      => strtolower($postData['username']),
            'role'          => $postData['role'],
            'no_hp'         => $postData['no_hp'],
            'password'      => SecurityManager::hashPassword($postData['password'], $auth_key),
            'password_salt' => $auth_key,
            'created_at'    => date('Y-m-d G:i:s', time()),
            'updated_at'    => date('Y-m-d G:i:s', time())
        );
        // echo "<pre>";print_r($dataInsert);die();
        $this->data['password'] = $postData['password'];
        $this->data['auth_key'] = $auth_key;
        $this->data['has']      = SecurityManager::hashPassword($postData['password'], $auth_key);
        $insert = $this->User_model->create($dataInsert, true, true);
        if($insert){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil mendaftarkan diri"), $this->data);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mendaftarkan diri'));
        }
        
    }

    public function checkEmail(){
        $email = $this->input->post('email');
        $this->load->model('User_model');
        $exist = $this->User_model->column('id')->filter(" where email='".strtolower($email)."'")->getOne();
        if($exist){
            $this->data['exist'] = true;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Email is exist"), $this->data);
        }else{
            $this->data['exist'] = false;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Email is not exist"), $this->data);
        }
    }
    


}

?>