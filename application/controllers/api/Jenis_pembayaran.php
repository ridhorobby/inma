<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_pembayaran extends PrivateApiController {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $filter = "where 1 ";

        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }

        $this->data = $this->model->filter($filter)->getJenis();

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function create() {
        $data = $this->postData;
        $insert = $this->model->create($data, $this->user['id']);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat jenis '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat jenis'));
        }
    }
    
    public function update() {
        $data = $this->postData;
        $insert = $this->model->updateJenis($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update jenis '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update jenis'));
        }
    }
    
    public function delete($id) {
        $hapus = dbQuery("delete from jenis_pembayaran where id=".$id);
        if ($hapus) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil hapus jenis '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal hapus jenis'));
        }
    }
    
}

?>
