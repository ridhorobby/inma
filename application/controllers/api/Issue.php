<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Issue extends PrivateApiController {

    public function me() {
        $issues = $this->model->getMe($this->user['id'], $this->page, $this->postData['param']);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $issues);
    }

    public function for_me() {
        if ($this->user['role']['id'] == Role::ADMINISTRATOR) {
            $issues = $this->model->getAll($this->page, $this->postData['param']);
        } else {
            $issues = $this->model->getForMe($this->user['id'], $this->page, $this->postData['param']);
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $issues);
    }
    
    public function chart_dashboard(){
      if ($this->user['role']['id'] == Role::ADMINISTRATOR) {
            $issues = $this->model->getChartDashboard(NULL);
      } else {
	  $issues = $this->model->getChartDashboard($this->user['id']);
      }
      $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $issues);
    }

    public function dashboard($status) {
	if($status >= Issue_model::STATUS_REOPEN)
	  $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Tidak dapat mengakses status Issue ini'));
    
        $condition = array();
        if (!empty($this->postData['projectId']))
            $condition['posts.group_id'] = $this->postData['projectId'];
        if (!empty($this->postData['categoryId']))
            $condition['posts.category_id'] = $this->postData['categoryId'];
        if (!empty($this->postData['priority']))
            $condition['issues.priority'] = $this->postData['priority'];
        if (!empty($this->postData['startDate']))
            $condition['issues.deadline >='] = date('Y-m-d', strtotime($this->postData['startDate']));
        if (!empty($this->postData['endDate']))
            $condition['issues.deadline <='] = date('Y-m-d', strtotime($this->postData['endDate']));
        if (!empty($this->postData['assignmentTask']) && $this->postData['assignmentTask'] == 'M') // T->Team | M-> 
            $condition['post_users.user_id'] = $this->user['id'];
        
        
        if($status == Issue_model::STATUS_OPEN)
	  $status = array(Issue_model::STATUS_OPEN, Issue_model::STATUS_REOPEN);

        if ($this->user['role']['id'] == Role::ADMINISTRATOR) {
            $issues = $this->model->getDashboardByStatus($status, NULL, $condition);
        } else {
            $issues = $this->model->getDashboardByStatus($status, $this->user['id'], $condition);
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $issues);
    }

    public function create() {
        $data = $this->postData;
        $data['user_id'] = $this->user['id'];
        $data['image'] = isset($_FILES['image']) ? $_FILES['image']['name'] : NULL;
        $data['video'] = isset($_FILES['video']) ? $_FILES['video']['name'] : NULL;
        $data['file'] = isset($_FILES['file']) ? $_FILES['file']['name'] : NULL;

        $insert = $this->model->create($data);
        if ($insert === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat tugas'));
        } elseif (is_string($insert)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat tugas'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }
    }

    public function detail($id) {
        //SET READ NOTIFICATION
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($this->user['id'], Notification_model::TYPE_ISSUE_DETAIL, $id);
        
        $issue = $this->model->get_by('posts.id', $id);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $issue);
    }

    public function delete($id) {
        $issue = $this->model->get_by('posts.id', $id);
        if ($issue['userId'] == $this->user['id']) {
            $this->load->model('Post_model');
            $delete = $this->Post_model->delete($id);
            if ($delete === TRUE) {
		if ($delete) {
		    $this->load->model('Notification_model');
		    $delete = $this->Notification_model->delete_by(array('reference_id' => $id, 'reference_type' => Notification_model::TYPE_ISSUE_DETAIL));
		}
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menghapus issue'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal menghapus issue"));
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Tidak dapat menghapus issue ini"));
        }
    }

    public function set_status($id) {
        $data = $this->model->get_by('posts.id', $id);
        $postUsers = Util::toList($data['postUsers'], 'id');
        $this->load->model('Group_member_model');
        $member = $this->Group_member_model->get_by(array('group_id' => $data['group']['id'], 'user_id' => $this->user['id']));
        if ($data['status'] != Issue_model::STATUS_CLOSE && $data['status'] != $this->postData['status'] && ($data['userId'] == $this->user['id'] || in_array($this->user['id'], $postUsers) || !empty($member))) {

//            if ($this->postData['status'] == Issue_model::STATUS_CLOSE && $data['userId'] != $this->user['id']) {
//                $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Hanya pembuat yang dapat menutup issue ini"));
//            }
            $update = $this->model->update($id, array('status' => $this->postData['status']));
            if ($update) {
                $statuses = Issue_model::getStatus();
                $this->load->model('Comment_model');
                $this->Comment_model->create(array(
                    'post_id' => $id,
                    'text' => '[' . $statuses[$this->postData['status']] . ']' . '  ' . $this->postData['description'],
                    'user_id' => $this->user['id'],
                    'candelete' => '0'
                ), TRUE, TRUE, FALSE);
                $this->load->model('Notification_model');
                $this->Notification_model->generate(Notification_model::ACTION_ISSUE_STATUS, $id, $this->user['id']);
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mengubah status tugas ini menjadi ' . $statuses[$this->postData['status']]));
            }else{
	      $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mengubah status tugas ini menjadi ' . $statuses[$this->postData['status']]));
	    }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Tidak dapat mengubah tugas ini'));
        }
    }

}
