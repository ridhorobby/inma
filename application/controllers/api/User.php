<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends PrivateApiController {

    /**
     * Digunakan untuk mengambil list user
     */
    public function index() {
        if($this->postData['filterExist']){
            $filter = "where 1 ";
            $filter .= $this->getFilter($this->postData);
        }
        $user = $this->model->getAll($filter);
        // echo "<pre>";print_r($user);die();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $user);
    }

    public function getFilter($data){
        $filter = '';
        if($data['username']){
            $username = strtolower($data['username']);
            $filter .= " and lower(username) like '%$username%' ";
        }
        if($data['name']){
            $name = strtolower($data['name']);
            $filter .= " and lower(name) like '%$name%' ";
        }
        if($data['no_hp']){
            $no_hp = $data['no_hp'];
            $filter .= " and no_hp like '%$no_hp%' ";
        }
        if($data['role']){
            $role = strtoupper($data['role']);
            $filter .= " and upper(role) like '%$role%' ";
        }
        return $filter;
    }

    /**
     * Digunakan untuk mengambil detail user dengan ID user
     */
    public function detail($id) {
        $user = $this->model->get($id);

        //SET RESPONSE
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $user);
    }

    /**
     * Digunakan untuk mengambil profil user dan role user yang sedang login
     */
    public function me() {
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->user);
    }

    public function create(){
        if($this->user['role'] == Role::ADMINISTRATOR || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR){
            $postData = $this->input->post();
            $this->load->model('User_model');
            $auth_key = SecurityManager::generateAuthKey();
            $dataInsert = array(
                'name'          => $postData['name'],
                'username'      => strtolower($postData['username']),
                'role'          => $postData['role'],
                'no_hp'         => $postData['no_hp'],
                'password'      => SecurityManager::hashPassword($postData['password'], $auth_key),
                'password_salt' => $auth_key,
                'created_at'    => date('Y-m-d G:i:s', time()),
                'updated_at'    => date('Y-m-d G:i:s', time())
            );
            // echo "<pre>";print_r($dataInsert);die();
            $this->data['password'] = $postData['password'];
            $this->data['auth_key'] = $auth_key;
            $this->data['has']      = SecurityManager::hashPassword($postData['password'], $auth_key);
            $insert = $this->User_model->create($dataInsert, true, true);
            if($insert){
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil mendaftarkan diri"), $this->data);
            }else{
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mendaftarkan diri'));
            }
        }
        
        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Anda tidak bisa mengakses fungsi ini'));
    }

    public function getListRole(){
        $data = Role::getArray();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $data);
    }

    /**
     * Digunakan untuk update profil user
     */
    public function update() {
        $data = $this->postData;

        if(isset($data['password'])){
            $user = dbGetRow("select password, password_salt from users where id=".$this->user['id']);
            // echo "<pre>";print_r($user);die();
            $check = SecurityManager::validate($data['password_lama'], $user['password'], $user['password_salt']);
            if(!$check){
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Password lama salah'));
            }
            $auth_key = SecurityManager::generateAuthKey();
            $data['password']      = SecurityManager::hashPassword($data['password'], $auth_key);
            $data['password_salt'] = $auth_key;
        }
        $update = $this->model->save($data, $data['user_id']);
        if ($update === TRUE) {
            $newData = $this->model->get($this->user['id']);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mengubah profil'), $newData);
        } elseif (is_string($update)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $update));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mengubah profil ' . $this->user['name']));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }
    }


    /**
     * Digunakan untuk mengganti password user
     */
    public function change_password() {
        $this->form_validation->set_rules('oldPassword', 'Password Lama', 'required');
        $this->form_validation->set_rules('newPassword', 'Password Baru', 'required|min_length[6]');
        $this->form_validation->set_rules('confirmPassword', 'Konfirmasi Password', 'required|min_length[6]|matches[newPassword]');
        $user = $this->model->is_private(TRUE)->get($this->user['id']);
        if(SecurityManager::validate($this->postData['oldPassword'], $user['password'], $user['passwordSalt'])){
            if ($this->form_validation->run() == TRUE) {
                
                $update = $this->model->update($this->user['id'], SecurityManager::encode($this->postData['newPassword']), TRUE);
                $resetExist = dbGetOne("select id from reset_password where user_id=".$this->user['id']);
                if($resetExist){
                    dbQuery("delete from reset_password where id=".$resetExist);
                }
                if ($update) {
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mengganti password'));
                } else {
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mengganti password'));
                }
            } else {
                $validation = $this->model->getErrorManualValidation(array('oldPassword', 'newPassword', 'confirmPassword'));
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, array(), $validation));
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Password lama tidak sesuai'));
        }
    }

    public function resetPassword($user_id){
        if($this->user['role'] == Role::ADMINISTRATOR || $this->user['role'] == Role::DIREKTUR){
            $auth_key = SecurityManager::generateAuthKey();
            $pass = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);
            $data = array(
                'password'  => SecurityManager::hashPassword($pass, $auth_key),
                'password_salt' => $auth_key,
                'updated_at'    => date('Y-m-d G:i:s', time())
            );

            $save = $this->model->save($data, $user_id);
            if($save){
                $dataPass = array(
                    'temp_password' => $pass,
                    'user_id'       => $user_id,
                    'created_at'    => date('Y-m-d G:i:s', time()),
                    'updated_at'    => date('Y-m-d G:i:s', time())
                );
                dbInsert("reset_password", $dataPass);
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil reset password'), $pass);
            }else{
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Anda tidak bisa mengakses fungsi ini'));
            }

        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Anda tidak bisa mengakses fungsi ini'));
        }
    }


    public function getUserByRole($role= Role::PEMASANG,$listView=false) {
        $column='*';
        if($listView){
            $column = "id,name";
        }
        $users = $this->model->column($column)->filter("where role='".$role."'")->getUser();
        if($listView){
            $users = Util::toMap($users,'id','name');
            // echo "<pre>";print_r($users);die();
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $users);
    }

    public function list_me() {
        $users = $this->model->getListFor($this->user['id']);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $users);
    }

    public function nonactive($user_id){
        if($this->user['role'] != Role::DIREKTUR){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Anda tidak bisa mengakses fungsi ini'));
        }
        $users = $this->model->setNonactive($user_id);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menonaktifkan user'), $users);

    }

    public function logout() {
        $logout = AuthManager::logout($this->token);
        if ($logout === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Logout berhasil'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $logout));
        }
    }
}

?>