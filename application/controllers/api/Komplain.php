<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Komplain extends PrivateApiController {

    function __construct() {
        parent::__construct();
        $this->load->model('Pesanan_model');
    }


    public function index() {
        $filter = "where 1 ";
        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }
        if($this->user['role'] == Role::MITRA_MARKETING){
            $filter .=" and k.user_id=".$this->user['id'];
        }
        $this->data = $this->model->filter($filter)->getKomplains($this->page);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

    public function create() {
        if($this->user['role'] == Role::PRODUKSI || $this->user['role'] == Role::PEMASANG){
             $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        }
        
        $data = $this->postData;
        // echo "<pre>";print_r($data);die();
        // $temp['user_id'] = $data['user_id'];

        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
            // $users = $this->User_model->getUser($data['user_id']);
        }


        /// CHECK KODE ORDER IS EXIST
        $checkOrder = $this->Pesanan_model->get_by('kode_order', $data['kode_order']);
        if(!$checkOrder){
            // echo "atas";die();
            $this->load->model('Konsumen_model');
            $dataKonsumen = array(
                'nama'      => $data['nama'],
                'alamat'    => $data['alamat'],
                'no_hp'     => $data['no_hp']    
            );
            $data['konsumen_id'] = $this->Konsumen_model->create($dataKonsumen);
            $data['tgl_order_masuk'] = $data['tgl_komplain'];
            $data['tipe'] = 'K';
            $data['status_produksi'] = '3';
            $order_id = $this->Pesanan_model->create($data, $this->user['id']);
            $updateSelesai = $this->Pesanan_model->setSelesai($order_id);
        }else{
            // echo "bawah";die();
            $order_id = $checkOrder['id'];
        }
        
        if($order_id){
            $data['pesanan_id'] = $order_id;
            $insert = $this->model->create($data, $this->user['id']);
            
            if ($insert) {
                if (!$this->_moveFileTemp($insert)) {
                    $hapus = dbQuery("delete from komplain where id=".$insert);
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Pesanan'));
                }
                $this->load->model('Notification_model');
                $this->Notification_model->komplainGenerate(Notification_model::ACTION_KOMPLAIN_CREATE, $insert,$this->user['id']);
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil Membuat Komplain'), $insert);
            } 
            else {
                $validation = $this->model->getErrorValidate();
                
                // echo $validation;die();
                if (empty($validation)) {
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Komplain 1'));
                } else {
                    $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
                }
            }
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Komplain 2'));
        }
        

    }

    public function updateKomplain($komplain_id){
        $data = $this->postData;
        $flow_id = dbGetOne("select flow_id from komplain where id=".$komplain_id);
        if($flow_id != 2){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mengubah data komplain'));
        }
        // nama, alamat no_hp
        $data['updated_at'] = date('Y-m-d G:i:s', time());
        $update = dbUpdate('komplain', $data, "id=".$komplain_id);
        if($update){
            $this->load->model('Notification_model');
            $this->Notification_model->komplainGenerate(Notification_model::ACTION_KOMPLAIN_UPDATE, $komplain_id,$this->user['id']);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mengubah data Komplain'), $komplain_id);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mengubah data Komplain'));
        }
    }

    public function deleteKomplain($komplain_id){
        $flow_id = dbGetOne("select flow_id from komplain where id=".$komplain_id);
        if($flow_id != 2){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal menghapus data Komplain'));
        }
        $data['is_deleted'] = 1;
        $data['updated_at'] = date('Y-m-d G:i:s', time());
        $update = dbUpdate('komplain', $data, "id=".$komplain_id);
        if($update){
            $this->load->model('Komplain_log_model');
            $this->Komplain_log_model->generate($komplain_id, $this->user['id'], 'Komplain dihapus');
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menghapus data'), $pesanan_id);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal menghapus data'));
        }
    }

    public function getFilter($data){
        $filter = '';
        if($data['kode_order']){
            $kode_order = strtolower($data['kode_order']);
            $filter .= " and lower(k.kode_order) like '%$kode_order%' ";
        }
        if($data['nama_konsumen']){
            $nama_konsumen = strtolower($data['nama_konsumen']);
            $filter .= " and lower(k.nama_konsumen) like '%$nama_konsumen%' ";
        }
        if($data['alamat_konsumen']){
            $alamat_konsumen = strtolower($data['alamat_konsumen']);
            $filter .= " and lower(k.alamat_konsumen) like '%$alamat_konsumen%' ";
        }
        if($data['no_hp_konsumen']){
            $no_hp_konsumen = strtolower($data['no_hp_konsumen']);
            $filter .= " and lower(k.no_hp_konsumen) like '%$no_hp_konsumen%' ";
        }
        return $filter;
    }

    public function getFlowPesanan(){
        $this->data = $this->model->getFlow();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

    public function getNeedAction(){
        $where = "where 1 ";
        if($this->postData['filterExist']){
            $where .= $this->getFilter($this->postData);
        }
        if($this->user['role'] == Role::PRODUKSI){
            // $column = "k.* , u.jadwal_usulan as jadwal_usulan, us.name as nama_pengusul, us.role as role_pengusul,usl.id as usulan_exist"; // ORIGINAL
            // $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
            //          left join komplain_usulan usl on k.id = usl.komplain_id
            //          left join users us on u.user_id = us.id
            // "; // ORIGINAL
            // $where .= "and k.flow_id in(2,5) group by k.id"; // ORIGINAL
            
            $column = "k.* , u.jadwal_usulan as jadwal_usulan, u.jam_usulan as jam_usulan, us.name as nama_pengusul, us.role as role_pengusul,
                    (select konsumen_id from pesanan where id=k.pesanan_id) as konsumen_id
            ";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and k.flow_id in(2,5) and mitra_pemasang_kusen <> 0 ";
        }
        elseif($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR || $this->user['role'] = Role::MITRA_MARKETING){
            $column = "k.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan, us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and k.flow_id in (3,6)";
            if($this->user['role'] == Role::MITRA_MARKETING){
                $where .=" and k.user_id=".$this->user['id'];
            }
        }
        elseif($this->user['role'] == Role::PEMASANG){
            // $where .= "and k.flow_id in (4,7)"; // ORIGINAL
            // $where .= "and k.flow_id in (4,7) and (k.mitra_pemasang_kusen=".$this->user['id']." or k.mitra_pemasang_finish=".$this->user['id'].")";
            $column = "k.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan , us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id";
            $where .= "and (
                                (k.flow_id in (4,7)  and (k.mitra_pemasang_kusen=".$this->user['id']." or k.mitra_pemasang_finish=".$this->user['id'].")) or 
                               (k.flow_id = 2 and tanggal_pasang_kusen is null and k.mitra_pemasang_kusen=".$this->user['id'].") or
                               (k.flow_id = 5 and tanggal_pasang_kusen is not null and tanggal_pasang_finish is null and k.mitra_pemasang_finish=".$this->user['id'].")
                           ) 
                        ";
        }
        $this->data = $this->model->column($column)->join($join)->filter($where)->getKomplains($this->page);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);

    }

    public function getInProgress(){
        $where = "where 1 ";
        if($this->postData['filterExist']){
            $where .= $this->getFilter($this->postData);
        } 
        if($this->user['role'] == Role::PRODUKSI){
            $column = "k.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan, us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and k.flow_id in(3,4,6,7)";
        }
        elseif($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::MITRA_MARKETING){
            $where .= "and k.flow_id in (4,7)";
            if($this->user['role'] == Role::MITRA_MARKETING){
                $where .=" and k.user_id=".$this->user['id'];
            }
        }
        // NOTE : AGAR DAPAT MELIHAT SEMUA STATUS PESANAN
        elseif($this->user['role'] == Role::DIREKTUR){
            $where .= "and k.flow_id in (2,4,5,7)";
        }
        elseif($this->user['role'] == Role::PEMASANG){
            $where .= "and k.flow_id=5";
        }
        $this->data = $this->model->column($column)->filter($where)->join($join)->getKomplains($this->page);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

    public function komplainBaru(){
        if($this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR || $this->user['role'] == Role::PRODUKSI || $this->user['role'] == Role::MITRA_MARKETING){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            if($this->user['role'] == Role::MITRA_MARKETING){
                $where .=" and k.user_id=".$this->user['id'];
            }
            $this->data = $this->model->filter($where)->getKomplainBaru($this->page);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
        }
        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Role anda tidak bisa mengakses fungsi ini'));
    }

    public function usulan($komplain_id){
        $this->load->model('Komplain_usulan_model');
        $this->load->model('Notification_model');
        $renker = dbGetRow("select flow_id,rencana_kerja_id, mitra_pemasang_kusen, mitra_pemasang_finish from komplain_view where id=".$komplain_id);
        // echo "<pre>";print_r($renker);die();
        switch ($renker['flow_id']) {
            case 2:
                if($this->user['role'] == Role::PRODUKSI || $this->user['id'] == $renker['mitra_pemasang_kusen']){
                    $usulan_exist = dbGetOne("select id from komplain_usulan where komplain_id=".$komplain_id." and flow_id=".$renker['flow_id']." and status='P'");

                    if(!$usulan_exist && $this->user['role'] == Role::PRODUKSI){
                        if($this->postData['jadwal_usulan']){
                            $usulan_id = $this->Komplain_usulan_model->create($this->postData['jadwal_usulan'], $this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $komplain_id, $this->user['id']);
                            if($usulan_id){
                                $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $komplain_id);
                                $this->model->addFlow($komplain_id, $renker['flow_id']);
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $komplain_id,$this->user['id']);
                                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                            }else{
                                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
                            }
                        }else{
                            $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $komplain_id);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $komplain_id,$this->user['id']);
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil set mitra pemasang'), $usulan_id);
                        }   
                        
                    }

                    else if(!$usulan_exist && $this->user['id'] == $renker['mitra_pemasang_kusen']){ // diisi oleh mitra pemasang
                        $usulan_id = $this->Komplain_usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $komplain_id, $this->user['id']);
                        if($usulan_id){
                            $this->model->addFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $komplain_id,$this->user['id']);
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                        }else{
                            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
                        }
                    }

                    else{ // konformasi penolakan
                        if($this->postData['confirm'] == 0 && !$this->postData['jadwal_usulan']){
                            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan, masukkan tanggal pengajuan'));
                        }
                        $konfirmasi = $this->Komplain_usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'] , $renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,$this->user['id'],false, 2);
                        if($konfirmasi){
                            if($this->postData['confirm'] == 0){
                                $this->model->addFlow($komplain_id, $renker['flow_id']);
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_REJECT_REVISI_KUSEN, $komplain_id,$this->user['id']);
                            }else{
                                $this->model->addFlow($komplain_id, $renker['flow_id'], 2);
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_CONFIRM_REVISI_KUSEN, $komplain_id,$this->user['id']);
                            }
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $komplain_id);
                        }else{
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Gagal melakukan konfirmasi'));
                        }
                    }
                }
            break;
            case 3:
                if($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR || $this->user['role'] == Role::MITRA_MARKETING){
                    // postData: confirm, jadwal_usulan(jika ada)
                    if($this->postData['confirm']==0 && !($this->postData['jadwal_usulan'])){
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan, masukkan tanggal pengajuan'));
                    }
                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'] , $renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,$this->user['id']);
                    if($konfirmasi){
                        if($this->postData['confirm'] == 0){
                            $this->model->minFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_REJECT_JADWAL_KUSEN, $komplain_id,$this->user['id']);
                        }else{
                            $this->model->addFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_CONFIRM_JADWAL_KUSEN, $komplain_id,$this->user['id']);
                        }
                        
                        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $komplain_id);
                    }else{
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan konfirmasi'));
                    }
                }
            break;
            case 4:
                if(($this->user['role'] == Role::PEMASANG && $this->user['id'] == $renker['mitra_pemasang_kusen']) || $this->user['role'] == Role::PRODUKSI){
                    if($this->postData['konfirmasi_selesai'] == 1){
                        $this->model->addFlow($komplain_id, $renker['flow_id'],4);
                        $this->model->setTglAcc($komplain_id);
                        $this->model->setJatuhTempo($komplain_id);
                        $this->Notification_model->komplainGenerate(Notification_model::ACTION_DONE_JADWAL_FINISH, $komplain_id,$this->user['id']);
                    }else{
                        $this->model->addFlow($komplain_id, $renker['flow_id']);
                        $this->Notification_model->komplainGenerate(Notification_model::ACTION_DONE_JADWAL_KUSEN, $komplain_id,$this->user['id']);
                    }
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $komplain_id);
                }
                elseif($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR){
                    if(!$this->postData['jadwal_usulan']){
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan reschedule, masukkan tanggal pengajuan'));
                    }
                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi(0,$this->postData['jadwal_usulan'], $this->postData['jam_usulan'], $renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,$this->user['id'],true);
                    $this->model->minFlow($komplain_id, $renker['flow_id'], 2);
                    $this->Notification_model->komplainGenerate(Notification_model::ACTION_RESCHEDULE_JADWAL_KUSEN, $konfirmasi,$this->user['id']);
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan reschedule'), $komplain_id);
                }
            break;
            case 5:
                if($this->user['role'] == Role::PRODUKSI || $this->user['id'] == $renker['mitra_pemasang_finish']){
                    $usulan_exist = dbGetOne("select id from usulan where komplain_id=".$komplain_id." and flow_id=".$renker['flow_id']." and status='P'");
                    if(!$usulan_exist && $this->user['role'] == Role::PRODUKSI){ // usulan baru
                        if($this->postData['jadwal_usulan']){                 
                            $usulan_id = $this->Komplain_usulan_model->create($this->postData['jadwal_usulan'], $this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $komplain_id, $this->user['id']);
                            if($usulan_id){
                                $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $komplain_id);
                                $this->model->addFlow($komplain_id, $renker['flow_id']);
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $komplain_id,$this->user['id']);
                                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                            }else{
                                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
                            }
                        }else{
                            $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $komplain_id);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $komplain_id,$this->user['id']);
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil set mitra pemasang'), $usulan_id);
                        }
                    }

                    else if(!$usulan_exist && $this->user['id'] == $renker['mitra_pemasang_finish']){ // diisi oleh mitra pemasang
                        $usulan_id = $this->Komplain_usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $komplain_id, $this->user['id']);
                        if($usulan_id){
                            $this->model->addFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $komplain_id,$this->user['id']);
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                        }else{
                            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
                        }
                    }

                    else{ // konfirmasi dari penolakan dan juga reschedule
                        if($this->postData['confirm'] == 0 && !$this->postData['jadwal_usulan']){
                            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan, masukkan tanggal pengajuan'));
                        }
                        $konfirmasi = $this->Komplain_usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'] , $renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,$this->user['id'], false , 5);
                        if($konfirmasi){
                            if($this->postData['confirm'] == 0){
                                $this->model->addFlow($komplain_id, $renker['flow_id']);
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_REJECT_REVISI_FINISH, $komplain_id,$this->user['id']);
                            }else{
                                $this->model->addFlow($komplain_id, $renker['flow_id'], 2);
                                $this->Notification_model->komplainGenerate(Notification_model::ACTION_CONFIRM_REVISI_FINISH, $komplain_id,$this->user['id']);
                            }
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $komplain_id);
                        }else{
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Gagal melakukan konfirmasi'));
                        }
                    }
                }
            break;
            case 6:
                if($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR || $this->user['role'] == Role::MITRA_MARKETING){
                    // postData: confirm, jadwal_usulan(jika ada)
                    if($this->postData['confirm']==0 && !($this->postData['jadwal_usulan'])){
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan, masukkan tanggal pengajuan'));
                    }
                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'] , $renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,$this->user['id']);
                    if($konfirmasi){
                        if($this->postData['confirm'] == 0){
                            $this->model->minFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_REJECT_JADWAL_FINISH, $komplain_id,$this->user['id']);
                        }else{
                            $this->model->addFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerate(Notification_model::ACTION_CONFIRM_JADWAL_FINISH, $komplain_id,$this->user['id']);
                        }
                        
                        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $komplain_id);
                    }else{
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan konfirmasi'));
                    }
                }
            break;
            case 7:
                if(($this->user['role'] == Role::PEMASANG && $this->user['id'] == $renker['mitra_pemasang_finish']) || $this->user['role'] == Role::PRODUKSI){

                    $this->model->addFlow($komplain_id, $renker['flow_id']);
                    $this->model->setTglAcc($komplain_id);
                    if (!$this->_moveFileTemp($komplain_id, 'PF')) {
                        $this->model->minFlow($komplain_id, 8);
                        dbUpdate("komplain", array("tgl_acc_konsumen" => null), "id=".$komplain_id);
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan konfirmasi'));
                    }
                    $this->Notification_model->komplainGenerate(Notification_model::ACTION_DONE_JADWAL_FINISH, $komplain_id,$this->user['id']);
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $komplain_id);
                }
                elseif($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR || $this->user['role'] == Role::MITRA_MARKETING){
                    if(!$this->postData['jadwal_usulan']){
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan reschedule, masukkan tanggal pengajuan'));
                    }
                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi(0,$this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,$this->user['id'],true);
                    $this->model->minFlow($komplain_id, $renker['flow_id'], 2);
                    $this->Notification_model->komplainGenerate(Notification_model::ACTION_RESCHEDULE_JADWAL_FINISH, $komplain_id,$this->user['id']);
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan reschedule'), $komplain_id);
                }
            break;
            
        }
        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Role anda tidak bisa mengakses pengajuan ini'));
    }

    public function detail($komplain_id, $notification_user_id=null){
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($notification_user_id);
        
        $this->data = $this->model->getDetailKomplain($komplain_id);
        $dataFotoOrder = $this->model->getPhoto($komplain_id);
        
        
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['ip_upload_dir'].'komplain/photos/';

        foreach ($dataFotoOrder as $fotoOrder) {
            $id = md5($fotoOrder['user_id'] . $this->config->item('encryption_key'));
            $folder .= '/' . $id;
            $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
            if($fotoOrder['tipe'] == 'OM'){
                $file_order[] = $path.$file_name;
            }else{
                $file_acc[] = $path.$file_name;
            }
            
        }
        $this->data['foto_order'] = $file_order;
        $this->data['foto_acc'] = $file_acc;
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->data);
    }




    public function kalendarView(){
        $filter = "where 1 ";
        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];
        $filter .= " and
             ((YEAR(tgl_order_masuk) = $tahun AND MONTH(tgl_order_masuk) = $bulan) 
             OR ( YEAR(tgl_jatuh_tempo)=$tahun AND MONTH(tgl_jatuh_tempo)=$bulan)
             OR ( YEAR(tanggal_pasang_kusen)=$tahun AND MONTH(tanggal_pasang_kusen)=$bulan) 
             OR (YEAR(tanggal_pasang_finish) = $tahun AND MONTH(tanggal_pasang_finish) =$bulan))
        ";
        if($this->user['role'] == Role::MITRA_MARKETING){
            $where .=" and user_id=".$this->user['id'];
        }
        $this->data = $this->model->filter($filter)->getPesanans('unl');
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS),$this->data);
    }

    public function testNotif(){
        // echo $this->user['id'];die();
        $this->load->model('Notification_model');
        // echo "asdasd";die();
        $this->Notification_model->generate(Notification_model::ACTION_POST_CREATE, 17266,$this->user['id']);
    }
    

    

    public function sendImage($folder='komplain/temp') {
        $folder_ori = $folder;

        if ($_FILES) {
            $images_arr = array();

            $id = md5($this->user['id'] . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }
            
            $name = $_FILES['gambar']['name'];

            $file_name = Image::getFileName($id, Image::IMAGE_ORIGINAL, $name);
            unlink($path . $file_name);
            
            $file_mime = Image::getMime($folder.$file_name);
            $image_mime = explode('/',$file_mime);
            if ($image_mime[0]=='image') {
                Image::upload('gambar', $id, $name, $folder);
                $link = Image::generateLink($id, $name, $folder);
                $images_arr[] = $link['thumb'];
            }
            // kalau mau responsenya list dari 5 file image yang diconvert 
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Success"));
        } 
    }


    function _moveFileTemp($post_id, $tipe='OM', $folder_source='komplain/temp') {
        $id = md5($this->user['id']. $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $files = glob($path_source.'*'); 
            $arr_image = array();
            $arr_files = array();
            foreach ($files as $file) {
                list($mime,$ext) = explode('/',Image::getMime($file));
                $basename = basename($file);
                if ($mime=='image') {
                    $folder_dest = 'komplain/photos';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $image = Image::getName($basename);
                    if (!in_array($image, $arr_image)) {
                        $arr_image[] = $image;
                        if (!$this->model->insertKomplainImage($post_id, $image, $file,$tipe, $this->user['id'])) {
                            return false;
                        }
                    }
                }
                if (!rename($file, $path_dest . '/' . $basename)) {
                    return false;
                }
            }
        }
        else {
            return true;
        }
        return $this->_deleteFiles($path_source);
    }

    function _getdraftfiles($folder='komplain/temp') {
        $str = '';
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';
        if (is_dir($path)) {
           $files = glob($path.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $file_name = explode('-',$file_src);
               $arr_file_ext = explode('.',$file_name[count($file_name)-1]);
               $file_ext = $arr_file_ext[count($arr_file_ext)-1];
               if ($file_ext=="pdf" || $file_ext=="doc" || $file_ext=="docx" || $file_ext=="xls" || $file_ext=="xlsx" || $file_ext=="ppt" || $file_ext=="pptx"){
                    $index = 0;
                    $name = '';
                    foreach ($file_name as $fname){
                        if ($index>0) {
                            $name .=$fname;
                            if ($index<count($file_name)-1) {
                                $name .='-';
                            }
                        }
                        $index++;
                    }
                    $str .= $name . "<br>";
               }
           }
        }
        return $str;
    }


    function removeTemp($nama_file){
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'].'komplain/temp'. $folder . '/';
        if($nama_file){
            $file_name_org = Image::getFileName($id, Image::IMAGE_ORIGINAL, $nama_file);
            $file_name_med = Image::getFileName($id, Image::IMAGE_MEDIUM, $nama_file);
            $file_name_thm = Image::getFileName($id, Image::IMAGE_THUMB, $nama_file);
            self::_deleteFiles($path.$file_name_org);
            self::_deleteFiles($path.$file_name_med);
            self::_deleteFiles($path.$file_name_thm);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil hapus file"));
        }else{
            self::_deleteFiles($path);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil hapus data"));
        }
        
    }
    function _deleteFiles($path){
        if (is_dir($path) === true) {
            $files = array_diff(scandir($path), array('.', '..'));
            foreach ($files as $file) {
                $this->_deleteFiles(realpath($path) . '/' . $file);
            }
            return rmdir($path);
        }

        else if (is_file($path) === true) {
            return unlink($path);
        }
        return false;
    }


    /**
     * Digunakan untuk mengambil post public, dari group yang diikuti, dan dari tag friend
     */
    public function me() {
        $posts = $this->model->getMe($this->user['id'], $this->page, $this->postData['param']);

        //SET RESPONSE
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $posts);
    }



    public function detailInfo($id) {
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($this->user['id'], Notification_model::TYPE_POST_DETAIL, $id);

        // $post = $this->model->with(array('Category', 'Comment' => 'User', 'User', 'Post_user', 'Group'))->get_by('posts.id', $id);
        $post = $this->model->getInformasi($this->user['id'],$id);

        if (!empty($post)) {
            $this->load->model('Tracker_model');
            $this->Tracker_model->post($id, $this->user['id']);
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $post);
    }

    // KEPERLUAN DEV & TESTING
    public function usulan_dev($komplain_id){
        $this->load->model('Komplain_usulan_model');
        $this->load->model('Notification_model');
        $renker = dbGetRow("select flow_id,rencana_kerja_id, mitra_pemasang_kusen, mitra_pemasang_finish from komplain_view where id=".$komplain_id);
        echo "<pre>";print_r($renker);die();
        switch ($renker['flow_id']) {
            case 2:
                if($this->user['role'] == Role::PRODUKSI){
                    $usulan_exist = dbGetOne("select id from komplain_usulan where komplain_id=".$komplain_id." and flow_id=".$renker['flow_id']." and status='P'");
                    if(!$usulan_exist){   
                        $usulan_id = $this->Komplain_usulan_model->create_dev($this->postData['jadwal_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $komplain_id, $this->user['id']);
                        if($usulan_id){
                            $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $komplain_id);
                            $this->model->addFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $usulan_id,$this->user['id']);
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                        }else{
                            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
                        }
                        
                    }
                    else{ // konformasi penolakan
                        if($this->postData['confirm'] == 0 && !$this->postData['jadwal_usulan']){
                            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan, masukkan tanggal pengajuan'));
                        }
                        $konfirmasi = $this->Komplain_usulan_model->konfirmasi_dev($this->postData['confirm'],$this->postData['jadwal_usulan'],$renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,$this->user['id'],false, 2);
                        if($konfirmasi){
                            if($this->postData['confirm'] == 0){
                                $this->model->addFlow($komplain_id, $renker['flow_id']);
                                $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_REJECT_REVISI_KUSEN, $insert,$this->user['id']);
                            }else{
                                $this->model->addFlow($komplain_id, $renker['flow_id'], 2);
                                $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_CONFIRM_REVISI_KUSEN, $insert,$this->user['id']);
                            }
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $komplain_id);
                        }else{
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Gagal melakukan konfirmasi'));
                        }
                    }
                }
            break;
            case 3:
                if($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR){
                    // postData: confirm, jadwal_usulan(jika ada)
                    if($this->postData['confirm']==0 && !($this->postData['jadwal_usulan'])){
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan, masukkan tanggal pengajuan'));
                    }
                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi_dev($this->postData['confirm'],$this->postData['jadwal_usulan'],$renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,$this->user['id']);
                    if($konfirmasi){
                        if($this->postData['confirm'] == 0){
                            $this->model->minFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_REJECT_JADWAL_KUSEN, $insert,$this->user['id']);
                        }else{
                            $this->model->addFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_CONFIRM_JADWAL_KUSEN, $insert,$this->user['id']);
                        }
                        
                        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $komplain_id);
                    }else{
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan konfirmasi'));
                    }
                }
            break;
            case 4:
                if(($this->user['role'] == Role::PEMASANG && $this->user['id'] == $renker['mitra_pemasang_kusen']) || $this->user['role'] == Role::PRODUKSI){
                    if($this->postData['konfirmasi_selesai'] == 1){
                        $this->model->addFlow($komplain_id, $renker['flow_id'],4);
                        $this->model->setTglAcc($komplain_id);
                        $this->model->setJatuhTempo($komplain_id);
                        $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_DONE_JADWAL_FINISH, $komplain_id,$this->user['id']);
                    }else{
                        $this->model->addFlow($komplain_id, $renker['flow_id']);
                        $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_DONE_JADWAL_KUSEN, $komplain_id,$this->user['id']);
                    }
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $komplain_id);
                }
                elseif($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR){
                    if(!$this->postData['jadwal_usulan']){
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan reschedule, masukkan tanggal pengajuan'));
                    }
                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi_dev(0,$this->postData['jadwal_usulan'],$renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,$this->user['id'],true);
                    $this->model->minFlow($komplain_id, $renker['flow_id'], 2);
                    $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_RESCHEDULE_JADWAL_KUSEN, $insert,$this->user['id']);
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan reschedule'), $komplain_id);
                }
            break;
            case 5:
                if($this->user['role'] == Role::PRODUKSI){
                    $usulan_exist = dbGetOne("select id from usulan where komplain_id=".$komplain_id." and flow_id=".$renker['flow_id']." and status='P'");
                    if(!$usulan_exist){ // usulan baru                    
                        $usulan_id = $this->Komplain_usulan_model->create_dev($this->postData['jadwal_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $komplain_id, $this->user['id']);
                        if($usulan_id){
                            $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $komplain_id);
                            $this->model->addFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_AJUKAN_JADWAL_FINISH, $insert,$this->user['id']);
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                        }else{
                            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
                        }
                    }else{ // konfirmasi dari penolakan dan juga reschedule
                        if($this->postData['confirm'] == 0 && !$this->postData['jadwal_usulan']){
                            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan, masukkan tanggal pengajuan'));
                        }
                        $konfirmasi = $this->Komplain_usulan_model->konfirmasi_dev($this->postData['confirm'],$this->postData['jadwal_usulan'],$renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,$this->user['id'], false , 5);
                        if($konfirmasi){
                            if($this->postData['confirm'] == 0){
                                $this->model->addFlow($komplain_id, $renker['flow_id']);
                                $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_REJECT_REVISI_FINISH, $insert,$this->user['id']);
                            }else{
                                $this->model->addFlow($komplain_id, $renker['flow_id'], 2);
                                $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_CONFIRM_REVISI_FINISH, $insert,$this->user['id']);
                            }
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $komplain_id);
                        }else{
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Gagal melakukan konfirmasi'));
                        }
                    }
                }
            break;
            case 6:
                if($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR){
                    // postData: confirm, jadwal_usulan(jika ada)
                    if($this->postData['confirm']==0 && !($this->postData['jadwal_usulan'])){
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan, masukkan tanggal pengajuan'));
                    }
                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi_dev($this->postData['confirm'],$this->postData['jadwal_usulan'],$renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,$this->user['id']);
                    if($konfirmasi){
                        if($this->postData['confirm'] == 0){
                            $this->model->minFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_REJECT_JADWAL_FINISH, $insert,$this->user['id']);
                        }else{
                            $this->model->addFlow($komplain_id, $renker['flow_id']);
                            $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_CONFIRM_JADWAL_FINISH, $insert,$this->user['id']);
                        }
                        
                        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $komplain_id);
                    }else{
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan konfirmasi'));
                    }
                }
            break;
            case 7:
                if(($this->user['role'] == Role::PEMASANG && $this->user['id'] == $renker['mitra_pemasang_finish']) || $this->user['role'] == Role::PRODUKSI){

                    $this->model->addFlow($komplain_id, $renker['flow_id']);
                    $this->model->setTglAcc($komplain_id);
                    if (!$this->_moveFileTemp($komplain_id, 'PF')) {
                        $this->model->minFlow($komplain_id, 8);
                        dbUpdate("komplain", array("tgl_acc_konsumen" => null), "id=".$komplain_id);
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan konfirmasi'));
                    }
                    $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_DONE_JADWAL_FINISH, $insert,$this->user['id']);
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $komplain_id);
                }
                elseif($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR){
                    if(!$this->postData['jadwal_usulan']){
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan reschedule, masukkan tanggal pengajuan'));
                    }
                    $konfirmasi = $this->Komplain_usulan_model->konfirmasi_dev(0,$this->postData['jadwal_usulan'],$renker['rencana_kerja_id'],$renker['flow_id'], $komplain_id,$this->user['id'],true);
                    $this->model->minFlow($komplain_id, $renker['flow_id'], 2);
                    $this->Notification_model->komplainGenerateDev(Notification_model::ACTION_RESCHEDULE_JADWAL_FINISH, $insert,$this->user['id']);
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan reschedule'), $komplain_id);
                }
            break;
            
        }
        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Role anda tidak bisa mengakses pengajuan ini'));
    }
    
    public function komplainDeleted(){
        if($this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR || $this->user['role'] == Role::PRODUKSI || $this->user['role'] == Role::MITRA_MARKETING){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            if($this->user['role'] == Role::MITRA_MARKETING){
                $where .=" and  k.user_id=".$this->user['id'];
            }
            $this->data = $this->model->filter($where)->getKomplainDeleted($this->page);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
        }
        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Role anda tidak bisa mengakses fungsi ini'));
    }
    
    public function getNeedActionDev(){
        $where = "where 1 ";
        if($this->postData['filterExist']){
            $where .= $this->getFilter($this->postData);
        }
        if($this->user['role'] == Role::PRODUKSI){
            // $column = "k.* , u.jadwal_usulan as jadwal_usulan, us.name as nama_pengusul, us.role as role_pengusul,usl.id as usulan_exist"; // ORIGINAL
            // $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
            //          left join komplain_usulan usl on k.id = usl.komplain_id
            //          left join users us on u.user_id = us.id
            // "; // ORIGINAL
            // $where .= "and k.flow_id in(2,5) group by k.id"; // ORIGINAL
            
            $column = "k.* , u.jadwal_usulan as jadwal_usulan, us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and k.flow_id in(2,5) and mitra_pemasang_kusen <> 0 ";
        }
        elseif($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR){
            $column = "k.* , u.jadwal_usulan as jadwal_usulan, us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and k.flow_id in (3,6)";
        }
        elseif($this->user['role'] == Role::PEMASANG){
            // $where .= "and k.flow_id in (4,7)"; // ORIGINAL
            $where .= "and k.flow_id in (4,7) and (k.mitra_pemasang_kusen=".$this->user['id']." or k.mitra_pemasang_finish=".$this->user['id'].")";
        }
        $this->data = $this->model->column($column)->join($join)->filter($where)->getKomplainsDev($this->page);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);

    }

    public function getInProgressDev(){
        $where = "where 1 ";
        if($this->postData['filterExist']){
            $where .= $this->getFilter($this->postData);
        } 
        if($this->user['role'] == Role::PRODUKSI){
            $column = "k.* , u.jadwal_usulan as jadwal_usulan, us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join komplain_usulan u on k.id = u.komplain_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and k.flow_id in(3,4,6,7)";
        }
        elseif($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING){
            $where .= "and k.flow_id in (4,7)";
        }
        // NOTE : AGAR DAPAT MELIHAT SEMUA STATUS PESANAN
        elseif($this->user['role'] == Role::DIREKTUR){
            $where .= "and k.flow_id in (2,4,5,7)";
        }
        elseif($this->user['role'] == Role::PEMASANG){
            $where .= "and k.flow_id=5";
        }
        $this->data = $this->model->column($column)->filter($where)->join($join)->getKomplainsDev($this->page);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    // DEV
    public function createDev() {
        if($this->user['role'] == Role::PRODUKSI || $this->user['role'] == Role::PEMASANG){
             $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        }
        
        $data = $this->postData;
        // echo "<pre>";print_r($data);die();
        // $temp['user_id'] = $data['user_id'];

        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
            // $users = $this->User_model->getUser($data['user_id']);
        }


        /// CHECK KODE ORDER IS EXIST
        $checkOrder = $this->Pesanan_model->get_by('kode_order', $data['kode_order']);
        if(!$checkOrder){
            // echo "atas";die();
            $this->load->model('Konsumen_model');
            $dataKonsumen = array(
                'nama'      => $data['nama'],
                'alamat'    => $data['alamat'],
                'no_hp'     => $data['no_hp']    
            );
            $data['konsumen_id'] = $this->Konsumen_model->createDev($dataKonsumen);
            $data['tgl_order_masuk'] = $data['tgl_komplain'];
            $data['tipe'] = 'K';
            $data['status_produksi'] = '3';
            $order_id = $this->Pesanan_model->createDev($data, $this->user['id']);
            $updateSelesai = $this->Pesanan_model->setSelesai($order_id);
        }else{
            // echo "bawah";die();
            $order_id = $checkOrder['id'];
        }
        
        echo $order_id;die();
        
        if($order_id){
            $data['pesanan_id'] = $order_id;
            $data['tipe'] = 'K';
            $insert = $this->model->create($data, $this->user['id']);
            
            if ($insert) {
                if (!$this->_moveFileTemp($insert)) {
                    $hapus = dbQuery("delete from komplain where id=".$insert);
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Pesanan'));
                }
                // $this->load->model('Notification_model');
                // $this->Notification_model->komplainGenerate(Notification_model::ACTION_KOMPLAIN_CREATE, $insert,$this->user['id']);
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil Membuat Komplain'), $insert);
            } 
            else {
                $validation = $this->model->getErrorValidate();
                
                // echo $validation;die();
                if (empty($validation)) {
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Komplain 1'));
                } else {
                    $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
                }
            }
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Komplain 2'));
        }
        

    }

    public function getAllKonsumen() {
        $this->load->model('Konsumen_model');
        $this->data = $this->Konsumen_model->getAllKonsumen($this->postData['keyword']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS),$this->data);
    }

    public function edit($komplain_id){
        // $data = $this->input->post();
        $update = $this->model->edit($this->postData, $komplain_id);
        if($update){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data'), $update);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update data'));
        }

    }

}

?>