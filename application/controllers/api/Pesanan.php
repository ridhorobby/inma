<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends PrivateApiController {

    function __construct() {
        parent::__construct();
    }


    // public function tesnotif(){
    //     $this->load->model('Notification_model');
    //     $this->Notification_model->generate(Notification_model::ACTION_PESANAN_CREATE, 67,$this->user['id']);
    //     // $receiver_id = 18;
    //     // $message = "ini adalah message";
    //     // $reference_id = 65;
    //     // $notification_user_id = 13;
    //     // $this->Notification_model->mobileNotification($receiver_id, $message, $reference_id,$notification_user_id, $type='pesanan');
    // }

    public function index() {
        $filter = "where 1 ";
        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }
        if($this->user['role'] == Role::MITRA_MARKETING){
            $filter .= "p.user_id=".$this->user['id'];
        }
        $this->data = $this->model->filter($filter)->getPesanans($this->page);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

    public function getFilter($data){
        $filter = '';
        if($data['kode_order']){
            $kode_order = strtolower($data['kode_order']);
            $filter .= " and lower(p.kode_order) like '%$kode_order%' ";
        }
        if($data['nama_konsumen']){
            $nama_konsumen = strtolower($data['nama_konsumen']);
            $filter .= " and lower(p.nama_konsumen) like '%$nama_konsumen%' ";
        }
        if($data['alamat_konsumen']){
            $alamat_konsumen = strtolower($data['alamat_konsumen']);
            $filter .= " and lower(p.alamat_konsumen) like '%$alamat_konsumen%' ";
        }
        if($data['no_hp_konsumen']){
            $no_hp_konsumen = strtolower($data['no_hp_konsumen']);
            $filter .= " and lower(p.no_hp_konsumen) like '%$no_hp_konsumen%' ";
        }
        if($data['flow_id']){
            $filter .= " and p.flow_id=".$flow_id." ";
        }
        return $filter;
    }

    public function getFlowPesanan(){
        $this->data = $this->model->getFlow();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function updateStatusProduksi() {
        $data = $this->postData;
        
        $update = $this->model->updateStatusProduksi($data);
        
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data '), $update);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update data'));
        }
    }

    public function getNeedAction(){
        $where = "where 1 ";
        if($this->postData['filterExist']){
            $where .= $this->getFilter($this->postData);
        }
        if($this->user['role'] == Role::PRODUKSI){
            $column = "p.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan,  us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join usulan u on p.id = u.pesanan_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and p.flow_id in(2,5) and mitra_pemasang_kusen <> 0 ";
        }
        elseif($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR || $this->user['role'] == Role::MITRA_MARKETING){
            $column = "p.* , u.jadwal_usulan as jadwal_usulan, u.jam_usulan as jam_usulan, us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join usulan u on p.id = u.pesanan_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and p.flow_id in (3,6)";
            if($this->user['role'] == Role::MITRA_MARKETING){
                $where .= " and p.user_id=".$this->user['id'];
            }
        }
        elseif($this->user['role'] == Role::PEMASANG){
            $column = "p.* , u.jadwal_usulan as jadwal_usulan,u.jam_usulan as jam_usulan , us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join usulan u on p.id = u.pesanan_id and u.status='P'
                     left join users us on u.user_id = us.id";
            $where .= "and (
                                (p.flow_id in (4,7)  and (p.mitra_pemasang_kusen=".$this->user['id']." or p.mitra_pemasang_finish=".$this->user['id'].")) or 
                               (p.flow_id = 2 and tanggal_pasang_kusen is null and p.mitra_pemasang_kusen=".$this->user['id'].") or
                               (p.flow_id = 5 and tanggal_pasang_kusen is not null and tanggal_pasang_finish is null and p.mitra_pemasang_finish=".$this->user['id'].")
                           ) 
                        ";
        }
        $this->data = $this->model->column($column)->join($join)->filter($where)->getPesanans($this->page);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);

    }

    public function getInProgress(){
        $where = "where 1 ";
        if($this->postData['filterExist']){
            $where .= $this->getFilter($this->postData);
        }

        if($this->user['role'] == Role::PRODUKSI){
            $column = "p.* , u.jadwal_usulan as jadwal_usulan, u.jam_usulan as jam_usulan, us.name as nama_pengusul, us.role as role_pengusul";
            $join = "left join usulan u on p.id = u.pesanan_id and u.status='P'
                     left join users us on u.user_id = us.id
            ";
            $where .= "and p.flow_id in(3,4,6,7)";
        }
        elseif($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::MITRA_MARKETING){
            $where .= "and p.flow_id in (4,7)";
            if($this->user['role'] == Role::MITRA_MARKETING){
                $where .= " and p.user_id=".$this->user['id'];
            }
        }
        // NOTE : AGAR DAPAT MELIHAT SEMUA STATUS PESANAN
        elseif($this->user['role'] == Role::DIREKTUR){
            $where .= "and p.flow_id in (2,4,5,7)";
        }
        elseif($this->user['role'] == Role::PEMASANG){
            $where .= "and p.flow_id=5 and (p.mitra_pemasang_kusen=".$this->user['id']." or p.mitra_pemasang_finish=".$this->user['id'].")";
        }

        $this->data = $this->model->filter($where)->getPesanans($this->page);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

    public function PesananBaru(){
        if($this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR || $this->user['role'] == Role::ADMINISTRATOR || $this->user['role'] == Role::PRODUKSI || $this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MITRA_MARKETING ){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            if($this->user['role'] == Role::MITRA_MARKETING){
                $where .= " and p.user_id=".$this->user['id'];
            }
            $this->data = $this->model->filter($where)->getPesananBaru($this->page);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
        }
        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Role anda tidak bisa mengakses fungsi ini'));
    }

    public function usulan($pesanan_id){
        $this->load->model('Usulan_model');
        $this->load->model('Notification_model');
        $renker = dbGetRow("select flow_id,rencana_kerja_id, mitra_pemasang_kusen, mitra_pemasang_finish from pesanan_view where id=".$pesanan_id);
        switch ($renker['flow_id']) {
            case 2:
                if($this->user['role'] == Role::PRODUKSI || $this->user['id'] == $renker['mitra_pemasang_kusen']){
                    // Melakukan pengajuan
                    $usulan_exist = dbGetRow("select id,jadwal_usulan from usulan where pesanan_id=".$pesanan_id." and flow_id=".$renker['flow_id']." and status='P'");
                    if(!$usulan_exist && $this->user['role'] == Role::PRODUKSI){ // pengajuan pesanan_baru
                        if($this->postData['jadwal_usulan']){
                            $usulan_id = $this->Usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $pesanan_id, $this->user['id']);
                            if($usulan_id){
                                $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $pesanan_id);
                                $this->model->addFlow($pesanan_id, $renker['flow_id']);
                                $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $pesanan_id,$this->user['id']);
                                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                            }else{
                                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
                            }
                            
                        }else{
                            $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $pesanan_id);
                            $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $pesanan_id,$this->user['id']);
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil set mitra pemasang'), $usulan_id);
                        }        
                    }
                    // $usulan_exist = dbGetOne("select id from usulan where pesanan_id=".$pesanan_id." and flow_id=".$renker['flow_id']." and status='P'");
                    // if(!$usulan_exist){   
                    //     $usulan_id = $this->Usulan_model->create($this->postData['jadwal_usulan'], $this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $pesanan_id, $this->user['id']);
                    //     if($usulan_id){
                    //         $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $pesanan_id);
                    //         $this->model->addFlow($pesanan_id, $renker['flow_id']);
                    //         $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $pesanan_id,$this->user['id']);
                    //         $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                    //     }else{
                    //         $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
                    //     }
                        
                    // }

                    else if(!$usulan_exist && $this->user['id'] == $renker['mitra_pemasang_kusen']){ // diisi oleh mitra pemasang
                        $usulan_id = $this->Usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $pesanan_id, $this->user['id']);
                        if($usulan_id){
                            $this->model->addFlow($pesanan_id, $renker['flow_id']);
                            $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_KUSEN, $pesanan_id,$this->user['id']);
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                        }else{
                            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
                        }
                    }

                    else{ // konformasi penolakan
                        if($this->postData['confirm'] == 0 && !$this->postData['jadwal_usulan']){
                            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan, masukkan tanggal pengajuan'));
                        }
                        $konfirmasi = $this->Usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'],$renker['flow_id'], $pesanan_id,$this->user['id'],false, 2);
                        if($konfirmasi){
                            if($this->postData['confirm'] == 0){
                                $this->model->addFlow($pesanan_id, $renker['flow_id']);
                                $this->Notification_model->generate(Notification_model::ACTION_REJECT_REVISI_KUSEN, $pesanan_id,$this->user['id']);
                            }else{
                                $this->model->addFlow($pesanan_id, $renker['flow_id'], 2);
                                $this->Notification_model->generate(Notification_model::ACTION_CONFIRM_REVISI_KUSEN, $pesanan_id,$this->user['id']);
                            }
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $pesanan_id);
                        }else{
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Gagal melakukan konfirmasi'));
                        }
                    }
                }
            break;
            case 3:
                if($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR || $this->user['role'] == Role::MITRA_MARKETING){
                    // postData: confirm, jadwal_usulan(jika ada)
                    if($this->postData['confirm']==0 && !($this->postData['jadwal_usulan'])){
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan, masukkan tanggal pengajuan'));
                    }
                    $konfirmasi = $this->Usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'] ,$renker['rencana_kerja_id'],$renker['flow_id'], $pesanan_id,$this->user['id']);
                    if($konfirmasi){
                        if($this->postData['confirm'] == 0){
                            $this->model->minFlow($pesanan_id, $renker['flow_id']);
                            $this->Notification_model->generate(Notification_model::ACTION_REJECT_JADWAL_KUSEN, $pesanan_id,$this->user['id']);
                        }else{
                            $this->model->addFlow($pesanan_id, $renker['flow_id']);
                            $this->Notification_model->generate(Notification_model::ACTION_CONFIRM_JADWAL_KUSEN, $pesanan_id,$this->user['id']);
                        }
                        
                        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $pesanan_id);
                    }else{
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan konfirmasi'));
                    }
                }
            break;
            case 4:
                if(($this->user['role'] == Role::PEMASANG && $this->user['id'] == $renker['mitra_pemasang_kusen']) || $this->user['role'] == Role::PRODUKSI){
                    $this->model->addFlow($pesanan_id, $renker['flow_id']);
                    $this->model->setJatuhTempo($pesanan_id);
                    $this->Notification_model->generate(Notification_model::ACTION_DONE_JADWAL_KUSEN, $pesanan_id,$this->user['id']);
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $pesanan_id);
                }
                elseif($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR){
                    if(!$this->postData['jadwal_usulan']){
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan reschedule, masukkan tanggal pengajuan'));
                    }
                    $konfirmasi = $this->Usulan_model->konfirmasi(0,$this->postData['jadwal_usulan'],$this->postData['jam_usulan'] , $renker['rencana_kerja_id'],$renker['flow_id'], $pesanan_id,$this->user['id'],true);
                    $this->model->minFlow($pesanan_id, $renker['flow_id'], 2);
                    $this->Notification_model->generate(Notification_model::ACTION_RESCHEDULE_JADWAL_KUSEN, $pesanan_id,$this->user['id']);
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan reschedule'), $pesanan_id);
                }
            break;
            case 5:
                if($this->user['role'] == Role::PRODUKSI || $this->user['id'] == $renker['mitra_pemasang_finish']){
                    $usulan_exist = dbGetOne("select id from usulan where pesanan_id=".$pesanan_id." and flow_id=".$renker['flow_id']." and status='P'");
                    if(!$usulan_exist && $this->user['role'] == Role::PRODUKSI){ // usulan baru                    
                        if($this->postData['jadwal_usulan']){
                            $usulan_id = $this->Usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $pesanan_id, $this->user['id']);
                            if($usulan_id){
                                $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $pesanan_id);
                                $this->model->addFlow($pesanan_id, $renker['flow_id']);
                                $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $pesanan_id,$this->user['id']);
                                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                            }else{
                                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
                            }
                            
                        }else{
                            $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $pesanan_id);
                            $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $pesanan_id,$this->user['id']);
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil set mitra pemasang'), $usulan_id);
                        }
                    }

                    else if(!$usulan_exist && $this->user['id'] == $renker['mitra_pemasang_finish']){ // diisi oleh mitra pemasang
                        $usulan_id = $this->Usulan_model->create($this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $pesanan_id, $this->user['id']);
                        if($usulan_id){
                            $this->model->addFlow($pesanan_id, $renker['flow_id']);
                            $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $pesanan_id,$this->user['id']);
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                        }else{
                            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
                        }
                    }
                    // if(!$usulan_exist){ // usulan baru                    
                    //     $usulan_id = $this->Usulan_model->create($this->postData['jadwal_usulan'], $this->postData['jam_usulan'], $renker['rencana_kerja_id'], $renker['flow_id'], $pesanan_id, $this->user['id']);
                    //     if($usulan_id){
                    //         $this->model->setMitraPemasang($this->postData['id_mitra_pemasang'], $renker['rencana_kerja_id'], $pesanan_id);
                    //         $this->model->addFlow($pesanan_id, $renker['flow_id']);
                    //         $this->Notification_model->generate(Notification_model::ACTION_USULAN_JADWAL_FINISH, $pesanan_id,$this->user['id']);
                    //         $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $usulan_id);
                    //     }else{
                    //         $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
                    //     }
                    // }

                    else{ // konfirmasi dari penolakan dan juga reschedule
                        if($this->postData['confirm'] == 0 && !$this->postData['jadwal_usulan']){
                            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan, masukkan tanggal pengajuan'));
                        }
                        $konfirmasi = $this->Usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'],$this->postData['jam_usulan'], $renker['rencana_kerja_id'],$renker['flow_id'], $pesanan_id,$this->user['id'], false , 5);
                        if($konfirmasi){
                            if($this->postData['confirm'] == 0){
                                $this->model->addFlow($pesanan_id, $renker['flow_id']);
                                $this->Notification_model->generate(Notification_model::ACTION_REJECT_REVISI_FINISH, $pesanan_id,$this->user['id']);
                            }else{
                                $this->model->addFlow($pesanan_id, $renker['flow_id'], 2);
                                $this->Notification_model->generate(Notification_model::ACTION_CONFIRM_REVISI_FINISH, $pesanan_id,$this->user['id']);
                            }
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $pesanan_id);
                        }else{
                            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Gagal melakukan konfirmasi'));
                        }
                    }
                }
            break;
            case 6:
                if($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR || $this->user['role'] == Role::MITRA_MARKETING){
                    // postData: confirm, jadwal_usulan(jika ada)
                    if($this->postData['confirm']==0 && !($this->postData['jadwal_usulan'])){
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan, masukkan tanggal pengajuan'));
                    }
                    $konfirmasi = $this->Usulan_model->konfirmasi($this->postData['confirm'],$this->postData['jadwal_usulan'], $this->postData['jam_usulan'], $renker['rencana_kerja_id'],$renker['flow_id'], $pesanan_id,$this->user['id']);
                    if($konfirmasi){
                        if($this->postData['confirm'] == 0){
                            $this->model->minFlow($pesanan_id, $renker['flow_id']);
                            $this->Notification_model->generate(Notification_model::ACTION_REJECT_JADWAL_FINISH, $pesanan_id,$this->user['id']);
                        }else{
                            $this->model->addFlow($pesanan_id, $renker['flow_id']);
                            $this->Notification_model->generate(Notification_model::ACTION_CONFIRM_JADWAL_FINISH, $pesanan_id,$this->user['id']);
                        }
                        
                        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $pesanan_id);
                    }else{
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan konfirmasi'));
                    }
                }
            break;
            case 7:
                if(($this->user['role'] == Role::PEMASANG && $this->user['id'] == $renker['mitra_pemasang_finish']) || $this->user['role'] == Role::PRODUKSI){
                    $this->model->addFlow($pesanan_id, $renker['flow_id']);
                    $this->model->setTglAcc($pesanan_id);
                    if (!$this->_moveFileTemp($pesanan_id, 'PF')) {
                        $this->model->minFlow($pesanan_id, 8);
                        dbUpdate("pesanan", array("tgl_acc_konsumen" => null), "id=".$pesanan_id);
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan konfirmasi'));
                    }
                    $this->Notification_model->generate(Notification_model::ACTION_DONE_JADWAL_FINISH, $pesanan_id,$this->user['id']);
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan konfirmasi'), $pesanan_id);
                }
                elseif($this->user['role'] == Role::DESAIN || $this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR){
                    if(!$this->postData['jadwal_usulan']){
                        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan reschedule, masukkan tanggal pengajuan'));
                    }
                    $konfirmasi = $this->Usulan_model->konfirmasi(0,$this->postData['jadwal_usulan'],$this->postData['jam_usulan'] , $renker['rencana_kerja_id'],$renker['flow_id'], $pesanan_id,$this->user['id'],true);
                    $this->model->minFlow($pesanan_id, $renker['flow_id'], 2);
                    $this->Notification_model->generate(Notification_model::ACTION_RESCHEDULE_JADWAL_FINISH, $pesanan_id,$this->user['id']);
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan reschedule'), $pesanan_id);
                }
            break;
            
        }
        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Role anda tidak bisa mengakses pengajuan ini'));
    }

    public function detail($pesanan_id, $notification_user_id=null){
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($notification_user_id);


        $this->load->model('Pembayaran_log_model');
        $this->data = $this->model->getDetailPesanan($pesanan_id);
        $dataFotoOrder = $this->model->getPhoto($pesanan_id);

        foreach ($dataFotoOrder as $fotoOrder) {
            $id = md5($fotoOrder['user_id'] . $this->config->item('encryption_key'));
        
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
        
            $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
            
            switch ($fotoOrder['tipe']) {
                case 'OM':
                    $path = $ciConfig['ip_upload_dir'].'pesanan/photos/';
                    $file_order[] = $path.$file_name;
                    break;
                case 'PF':
                    $path = $ciConfig['ip_upload_dir'].'pesanan/photos/';
                    $file_acc[] = $path.$file_name;
                    break;
                case 'FA':
                    $path = $ciConfig['ip_upload_dir'].'foto_after/photos/';
                    $file_fa[] = $path.$file_name;
                    break;
                case 'EK':
                    $path = $ciConfig['ip_upload_dir'].'edit_katalog/photos/';
                    $file_ek[] = $path.$file_name;
                    break;
            }
        }
        
        $this->data['foto_order'] = $file_order;
        $this->data['foto_acc'] = $file_acc;
        $this->data['file_foto_after'] = $file_fa;
        $this->data['file_edit_katalog'] = $file_ek;


        $this->data['pembayaran'] = $this->Pembayaran_log_model->getPembayaranLogByKonsumen($this->data['konsumen_id']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->data);
    }

    public function create() {
        if($this->user['role'] == Role::PRODUKSI || $this->user['role'] == Role::PEMASANG){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        }
        $data = $this->postData;
        // $temp['user_id'] = $data['user_id'];

        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
            // $users = $this->User_model->getUser($data['user_id']);
        }
        $this->load->model('Konsumen_model');
        $dataKonsumen = array(
            'nama'      => $data['nama'],
            'alamat'    => $data['alamat'],
            'no_hp'     => $data['no_hp']    
        );


        /// CHECK KODE ORDER IS EXIST
        // NOTE : KARENA PESANAN BISA DIHAPUS SEMUA, MAKA ATURAN INI TIDAK BERLAKU AGAR BISA INPUT DENGAN KODE YANG SAMA
        // $checkOrder = $this->model->get_by('kode_order', $data['kode_order']);
        // if($checkOrder){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Pesanan, Kode Pesanan sudah ada'));
        // }


        $konsumen_id = $this->Konsumen_model->create($dataKonsumen);
        
        if($konsumen_id){
            $data['konsumen_id'] = $konsumen_id;
            $data['tipe'] = 'P';
            
            if ($data['status_produksi']=="") {
                $data['status_produksi'] = '3';
                // $users = $this->User_model->getUser($data['user_id']);
            }
        
            $insert = $this->model->create($data, $this->user['id']);
            if ($insert) {
                if(isset($data['id_pengajuan'])){
                    dbUpdate('pengajuan_harga', array('id_konsumen' => $konsumen_id),"id=".$data['id_pengajuan']);

                    $this->load->model("Fee_desainer_model");

                    $pesanan['harga_deal'] = dbGetOne("select harga_deal from pesanan_view where id=".$insert);
                    $fee_design = 50000;
                    if ($pesanan['harga_deal']>=7500000 && $pesanan['harga_deal']<10000000) {
                        $fee_design = 75000;
                    }elseif($pesanan['harga_deal']>=10000000){
                        $fee_design = 100000;
                    }
                    $insertFeeDesainer = array(
                        'desainer_design_id'    => $data['desainer_design'],
                        'desainer_eksekusi_id'  => $data['desainer_eksekusi'],
                        'pesanan_id'            => $insert,
                        'fee_design'            => $fee_design
                    );
                    $this->Fee_desainer_model->create($insertFeeDesainer);
                }
                
                if (!$this->_moveFileTemp($insert)) {
                    $hapus = dbQuery("delete from pesanan where id=".$insert);
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Pesanan'));
                }
                $this->load->model('Notification_model');
                $this->Notification_model->generate(Notification_model::ACTION_PESANAN_CREATE, $insert,$this->user['id']);
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil Membuat Pesanan'), $insert);
            } 
            else {
                $validation = $this->model->getErrorValidate();
                if (empty($validation)) {
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Pesanan'));
                } else {
                    $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
                }
            }
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Pesanan'));
        }
        

    }

    public function updateKonsumen($pesanan_id){
        $data = $this->postData;
        $flow_id = dbGetOne("select flow_id from pesanan where id=".$pesanan_id);
        if($flow_id != 2){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mengubah data Konsumen'));
        }
        $konsumen_id = dbGetOne("select konsumen_id from pesanan where id=".$pesanan_id);
        
        // nama, alamat no_hp
        $data['updated_at'] = date('Y-m-d G:i:s', time());
        
        $this->load->model('Konsumen_model');
        $data['id_konsumen'] = $konsumen_id;
        $update = $this->Konsumen_model->updateKonsumen($data);
        
        if($update){
            $updatePesanan = $this->model->updateKodeOrder($pesanan_id, $data['kode_order']);
            // $this->load->model('Notification_model');
            // $this->Notification_model->generate(Notification_model::ACTION_PESANAN_UPDATE, $pesanan_id,$this->user['id']);
            if($updatePesanan) {
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mengubah data Pesanan'), $pesanan_id);
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mengubah data Pesanan'));
            }
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mengubah data Konsumen'));
        }
    }

    public function deletePesanan($pesanan_id){
        $flow_id = dbGetOne("select flow_id from pesanan where id=".$pesanan_id);
        // NOTE : DIHAPUS KARENA MINTA SEMUA BISA DIHAPUS
        // if($flow_id != 2){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal menghapus data Konsumen'));
        // }
        $data['is_deleted'] = 1;
        $data['updated_at'] = date('Y-m-d G:i:s', time());
        $update = dbUpdate('pesanan', $data, "id=".$pesanan_id);
        if($update){
            $this->load->model('Pesanan_log_model');
            $this->Pesanan_log_model->generate($pesanan_id, $this->user['id'], 'Pesanan dihapus');
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menghapus data'), $pesanan_id);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal menghapus data'));
        }
    }


    public function kalendarView($page=false){
        $filter = "where 1 ";
        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];
        if($bulan && $tahun){
            $filter .= " and
                 ((YEAR(tgl_masuk) = $tahun AND MONTH(tgl_masuk) = $bulan) 
                 OR ( YEAR(tgl_acc_konsumen)=$tahun AND MONTH(tgl_acc_konsumen)=$bulan)
                 OR ( YEAR(tanggal_pasang_kusen)=$tahun AND MONTH(tanggal_pasang_kusen)=$bulan) 
                 OR (YEAR(tanggal_pasang_finish) = $tahun AND MONTH(tanggal_pasang_finish) =$bulan))
            ";

        }
        
        if($this->user['role'] == Role::MITRA_MARKETING){
            $filter .= " and user_id=".$this->user['id'];
        }
        if($this->user['role'] == Role::PEMASANG){
            $filter .= " and (mitra_pemasang_kusen=".$this->user['id'].") or (mitra_pemasang_finish=".$this->user['id'].")";
        }
        $this->data = $this->model->filter($filter)->getKalendar(($page!==false) ? $page : 'unl');
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS),$this->data);
    }

    // NOTE : SAMA SEPERTI 'KALENDAR VIEW' TETAPI TIDAK MENAMPILKAN ORDER YANG SUDAH SELESAI
    public function kalendarViewUndone(){
        $filter = "where 1 ";
        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];
        $filter .= " and flow_id <> 8 and 
             ((YEAR(tgl_masuk) = $tahun AND MONTH(tgl_masuk) = $bulan) 
             OR ( YEAR(tgl_jatuh_tempo)=$tahun AND MONTH(tgl_jatuh_tempo)=$bulan)
             OR ( YEAR(tanggal_pasang_kusen)=$tahun AND MONTH(tanggal_pasang_kusen)=$bulan)
             OR ( YEAR(jatuh_tempo_kusen) = $tahun AND MONTH(jatuh_tempo_kusen) =$bulan)
             OR (YEAR(tanggal_pasang_finish) = $tahun AND MONTH(tanggal_pasang_finish) =$bulan)
             OR ( YEAR(jatuh_tempo_kusen) = $tahun AND MONTH(jatuh_tempo_kusen) =$bulan))
        ";
        if($this->user['role'] == Role::MITRA_MARKETING){
            $filter .= " and user_id=".$this->user['id'];
        }
        if($this->user['role'] == Role::PEMASANG){
            $filter .= " and (mitra_pemasang_kusen=".SessionManagerWeb::getUserID().") or (mitra_pemasang_finish=".SessionManagerWeb::getUserID().")";
        }
        $this->data = $this->model->filter($filter)->getKalendar('unl');
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS),$this->data);
    }
    
    // NOTE : FITUR UNTUK MENCARI PESANAN YG BELUM 'PASANG FINISH' TETAPI SUDAH MENDEKATI JATUH TEMPO
    public function getPesananJatuhTempo() {
        if($this->postData['filterExist']){
            $where .= $this->getFilter($this->postData);
        }
        if($this->user['role'] == Role::MITRA_MARKETING){
            $where .= " and p.user_id=".$this->user['id'];
        }
        $this->data = $this->model->filter($where)->getPesananJatuhTempo();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    

    public function sendImage($folder) {
        $folder_ori = $folder;
        
        $data = $this->postData;
        if ($data['directory'] != null) {
            $folder = $data['directory'];
        } else {
            $folder = 'pesanan/temp';
        }

        if ($_FILES) {
            $images_arr = array();

            $id = md5($this->user['id'] . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }
            
            $name = $_FILES['gambar']['name'];

            $file_name = Image::getFileName($id, Image::IMAGE_ORIGINAL, $name);
            unlink($path . $file_name);
            
            $file_mime = Image::getMime($folder.$file_name);
            $image_mime = explode('/',$file_mime);
            if ($image_mime[0]=='image') {
                Image::upload('gambar', $id, $name, $folder);
                $link = Image::generateLink($id, $name, $folder);
                $images_arr[] = $link['thumb'];
            }
            // kalau mau responsenya list dari 5 file image yang diconvert 
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Success"));
        } 
    }
    
    public function sendImageTemp() {
        $data = $this->postData;

        if ($data['directory'] != null) {
            $folder = $data['directory'];
        } else {
            $folder = 'pesanan/temp';
        }

        if ($_FILES) {
            $images_arr = array();

            $id = md5($this->user['id'] . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }
            
            $name = $_FILES['gambar']['name'];

            $file_name = Image::getFileName($id, Image::IMAGE_ORIGINAL, $name);
            unlink($path . $file_name);
            
            $file_mime = Image::getMime($folder.$file_name);
            $image_mime = explode('/',$file_mime);
            if ($image_mime[0]=='image') {
                Image::upload('gambar', $id, $name, $folder);
                $link = Image::generateLink($id, $name, $folder);
                $images_arr[] = $link['thumb'];
            }
            // kalau mau responsenya list dari 5 file image yang diconvert 
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Success"));
        } 
    }

    public function sendFile($folder='posts/temp') {
        $folder_ori = $folder;

        if ($_FILES) {
            $files_arr = array();

            $id = md5($this->user['id'] . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }

            $name = $_FILES['file_upload']['name'];
            $file_name = File::getFileName($id, $name);
            unlink($path . $file_name);

            File::uploadFile('file_upload', $id, $name, $folder);
            $link = File::generateLink($id, $name, $folder);
            // $files_arr = $link;

            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ""));
        }    
    }

    function _moveFileTemp($post_id, $tipe='OM', $folder_source='pesanan/temp', $folder_dest='pesanan/photos') {
        $id = md5($this->user['id']. $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $files = glob($path_source.'*'); 
            $arr_image = array();
            $arr_files = array();
            foreach ($files as $file) {
                list($mime,$ext) = explode('/',Image::getMime($file));
                $basename = basename($file);
                if ($mime=='image') {
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $image = Image::getName($basename);
                    if (!in_array($image, $arr_image)) {
                        $arr_image[] = $image;
                        if (!$this->model->insertPesananImage($post_id, $image, $file,$tipe, $this->user['id'])) {
                            return false;
                        }
                    }
                }
                

                if (!rename($file, $path_dest . '/' . $basename)) {
                    return false;
                }
            }
        }
        else {
            return true;
        }
        return $this->_deleteFiles($path_source);
    }

    function _getdraftfiles($folder='posts/temp') {
        $str = '';
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';
        if (is_dir($path)) {
           $files = glob($path.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $file_name = explode('-',$file_src);
               $arr_file_ext = explode('.',$file_name[count($file_name)-1]);
               $file_ext = $arr_file_ext[count($arr_file_ext)-1];
               if ($file_ext=="pdf" || $file_ext=="doc" || $file_ext=="docx" || $file_ext=="xls" || $file_ext=="xlsx" || $file_ext=="ppt" || $file_ext=="pptx"){
                    $index = 0;
                    $name = '';
                    foreach ($file_name as $fname){
                        if ($index>0) {
                            $name .=$fname;
                            if ($index<count($file_name)-1) {
                                $name .='-';
                            }
                        }
                        $index++;
                    }
                    $str .= $name . "<br>";
               }
           }
        }
        return $str;
    }


    function removeTemp($nama_file){
        $data = $this->postData;
        $directory = 'pesanan/temp';
        
        if ($data['directory'] != null) {
            $directory = $data['directory'];
        } else {
            $directory = 'pesanan/temp';
        }
        
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'].$directory. $folder . '/';
        if($nama_file){
            $file_name_org = Image::getFileName($id, Image::IMAGE_ORIGINAL, $nama_file);
            $file_name_med = Image::getFileName($id, Image::IMAGE_MEDIUM, $nama_file);
            $file_name_thm = Image::getFileName($id, Image::IMAGE_THUMB, $nama_file);
            self::_deleteFiles($path.$file_name_org);
            self::_deleteFiles($path.$file_name_med);
            self::_deleteFiles($path.$file_name_thm);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil hapus file"));
        }else{
            self::_deleteFiles($path);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil hapus data"));
        }
        
    }
    function _deleteFiles($path){
        if (is_dir($path) === true) {
            $files = array_diff(scandir($path), array('.', '..'));
            foreach ($files as $file) {
                $this->_deleteFiles(realpath($path) . '/' . $file);
            }
            return rmdir($path);
        }

        else if (is_file($path) === true) {
            return unlink($path);
        }
        return false;
    }


    /**
     * Digunakan untuk mengambil post public, dari group yang diikuti, dan dari tag friend
     */
    public function me() {
        $posts = $this->model->getMe($this->user['id'], $this->page, $this->postData['param']);

        //SET RESPONSE
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $posts);
    }



    public function detailInfo($id) {
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($this->user['id'], Notification_model::TYPE_POST_DETAIL, $id);

        // $post = $this->model->with(array('Category', 'Comment' => 'User', 'User', 'Post_user', 'Group'))->get_by('posts.id', $id);
        $post = $this->model->getInformasi($this->user['id'],$id);

        if (!empty($post)) {
            $this->load->model('Tracker_model');
            $this->Tracker_model->post($id, $this->user['id']);
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $post);
    }

    // ADDITIONAL FEATURE : 30 JUNI 2020
    // TESTED
    function removeImagePesanan(){
        $data = $this->postData;
        $pesanan_id = $data['pesanan_id'];
        $namaFile = $data['nama_file'];
        $hashId = $data['id_hash'];
        $tipe = 'OM';
        
        if ($data['tipe'] == null) {
            $tipe = 'OM';
        } else {
            $tipe = $data['tipe'];
        }
        
        switch ($tipe) {
            case 'FA':
                $folder = 'foto_after/photos/';
                break;
            case 'EK':
                $folder = 'edit_katalog/photos/';
                break;
            case 'OM':
                $folder = 'pesanan/photos/';
                break;
            case 'PF':
                $folder = 'pesanan/photos/';
                break;
        }
        
        if ($folder == null) {
            $folder = 'pesanan/photos/';
        }

        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'].$folder;
        
        if($namaFile){
            if ($this->model->deletePesananImage($pesanan_id, $namaFile)) {
                $file_name_org = 'original-'.$hashId.'-'.$namaFile;
                $file_name_med = 'medium-'.$hashId.'-'.$namaFile;
                $file_name_thm = 'thumb-'.$hashId.'-'.$namaFile;
                
                self::_deleteFiles($path.$file_name_org);
                self::_deleteFiles($path.$file_name_med);
                self::_deleteFiles($path.$file_name_thm);
                
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil menghapus gambar"));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Gagal menghapus gambar "));
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Gagal menghapus gambar "));
        }
    }
    
    // ADDITIONAL FEATURE : 1 JULI 2020
    function saveEditGambar($pesanan_id){
        $hashId = $this->postData['hashId'];
        if ($this->_moveFileTempRev($pesanan_id, $hashId)) {
            $this->load->model('Notification_model');
            $this->Notification_model->generate(Notification_model::ACTION_REVISI_GAMBAR_KERJA, $pesanan_id, $this->user['id']);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil simpan file"));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal simpan file'));
        }
    }
    
    // ADDITIONAL FEATURE : 2 JULI 2020
    function _moveFileTempRev($post_id, $hashId, $tipe='OM', $folder_source='pesanan/temp') {
        $id = md5($this->user['id']. $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $files = glob($path_source.'*'); 
            $arr_image = array();
            $arr_files = array();
            foreach ($files as $file) {
                list($mime,$ext) = explode('/',Image::getMime($file));
                
                $basename = basename($file);
                
                // Rename
                $pieces = explode("-", $basename);
                $basename = $pieces[0]."-".$id."-".$pieces[2];
                
                if ($mime=='image') {
                    $folder_dest = 'pesanan/photos';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $image = Image::getName($basename);
                    if (!in_array($image, $arr_image)) {
                        $arr_image[] = $image;
                        if (!$this->model->insertPesananImage($post_id, $image, $file,$tipe, $this->user['id'])) {
                            return false;
                        }
                    }
                }
            
                if (!rename($file, $path_dest . '/' . $basename)) {
                    return false;
                }
            }
        }
        else {
            return true;
        }
        return $this->_deleteFiles($path_source);
    }
    
    // ADDITIONAL FEATURE : AKUNTANSI 10 AGUSTUS 2020
    public function getKonsumenPengajuan() {
        $data = $this->postData;
        $this->load->model('Konsumen_model');
        $this->data = $this->Konsumen_model->getKonsumenPengajuan($data['status'], $data['keyword']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->data);
    }
    
    // ADDITIONAL FEATURE : AKUNTANSI 16 NOVEMBER 2020
    public function getKonsumenUpdatePengajuan() {
        $data = $this->postData;
        $this->load->model('Konsumen_model');
        $this->data = $this->Konsumen_model->getKonsumenUpdatePengajuan($data['status'], $data['keyword']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->data);
    }
    
    public function getKPIPesanan() {
        $this->data = $this->model->getKPIPesanan();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    // Mendapatkan data KPI Bulanan
    public function getKPIBulanan() {
        $tahun = $this->postData['tahun'];
        
        $this->data = $this->model->getKPIBulanan($tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    // ADDITIONAL FEATURE 09 January 2021
    // Menambahkan bukti foto after dan foto bukti edit katalog beserta link
    function saveFotoAfter($pesanan_id){
        $data = $this->postData;
        
        $this->updateLinkPesanan($pesanan_id, $data['link']);
        
        $folder_source = 'foto_after/temp';
        $folder_dest = 'foto_after/photos';
        $tipe = 'FA';
        $this->_moveFileTemp($pesanan_id, $tipe, $folder_source, $folder_dest);
        
        $folder_source = 'edit_katalog/temp';
        $folder_dest = 'edit_katalog/photos';
        $tipe = 'EK';
        if ($this->_moveFileTemp($pesanan_id, $tipe, $folder_source, $folder_dest)) {
            // $this->load->model('Notification_model');
            // $this->Notification_model->generate(Notification_model::ACTION_FOTO_AFTER, $pesanan_id, $this->user['id']);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil simpan file"));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal simpan file'));
        }
    }
    
    // Untuk update link pada pesanan
    function updateLinkPesanan($pesanan_id, $link) {
        $this->model->updateLinkPesanan($pesanan_id, $link);
        // $update = $this->model->updateLinkPesanan($link);
        
        // if ($update) {
        //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data '), $update);
        // } else {
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update data'));
        // }
    }
    
    // Untuk get data gambar After dan gambar Edit katalog
    function getGambarAfter($pesanan_id) {
        $dataFotoOrder = $this->model->getGambarAfter($pesanan_id);
        
        $id = md5($dataFotoOrder[0]['user_id'] . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');

        foreach ($dataFotoOrder as $fotoOrder) {
            $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
            if($fotoOrder['tipe'] == 'FA'){
                $path = $ciConfig['ip_upload_dir'].'foto_after/photos/';
                $file_foto_after[] = $path.$file_name;
            }else{
                $path = $ciConfig['ip_upload_dir'].'edit_katalog/photos/';
                $file_edit_katalog[] = $path.$file_name;
            }
            
        }
        
        $this->data['file_foto_after'] = $file_foto_after;
        $this->data['file_edit_katalog'] = $file_edit_katalog;
        
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

    function getKonsumenDealPengajuan() {
        $data = $this->postData;
        $this->load->model('Konsumen_model');
        $this->data = $this->Konsumen_model->getKonsumenDealPengajuan($data['keyword']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->data);
    }
    
    // KEBUTUHAN DEV 
    // KEBUTUHAN DEV 
    // KEBUTUHAN DEV 
    // KEBUTUHAN DEV 
    // KEBUTUHAN DEV 
    // KEBUTUHAN DEV 
    // KEBUTUHAN DEV 
    
    public function kalendarViewDev(){
        $filter = "where 1 ";
        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];
        $filter .= " and
             ((YEAR(p.tgl_masuk) = $tahun AND MONTH(p.tgl_masuk) = $bulan) 
             OR ( YEAR(p.tgl_acc_konsumen)=$tahun AND MONTH(p.tgl_acc_konsumen)=$bulan)
             OR ( YEAR(p.tanggal_pasang_kusen)=$tahun AND MONTH(p.tanggal_pasang_kusen)=$bulan) 
             OR (YEAR(p.tanggal_pasang_finish) = $tahun AND MONTH(p.tanggal_pasang_finish) =$bulan))
        ";
        $this->data = $this->model->filter($filter)->getKalendarDev('unl');
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS),$this->data);
    }
    
    public function detail2($pesanan_id, $notification_user_id){

        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($notification_user_id);

        $this->data = $this->model->getDetailPesanan($pesanan_id);
        $dataFotoOrder = $this->model->getPhoto($pesanan_id);

        foreach ($dataFotoOrder as $fotoOrder) {
            $id = md5($fotoOrder['user_id'] . $this->config->item('encryption_key'));
        
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
        
            $file_name = Image::getFileName($id, Image::IMAGE_MEDIUM, $fotoOrder['nama_file']);
            
            switch ($fotoOrder['tipe']) {
                case 'OM':
                    $path = $ciConfig['ip_upload_dir'].'pesanan/photos/';
                    $file_order[] = $path.$file_name;
                    break;
                case 'PF':
                    $path = $ciConfig['ip_upload_dir'].'pesanan/photos/';
                    $file_acc[] = $path.$file_name;
                    break;
                case 'FA':
                    $path = $ciConfig['ip_upload_dir'].'foto_after/photos/';
                    $file_fa[] = $path.$file_name;
                    break;
                case 'EK':
                    $path = $ciConfig['ip_upload_dir'].'edit_katalog/photos/';
                    $file_ek[] = $path.$file_name;
                    break;
            }
        }
        
        $this->data['foto_order'] = $file_order;
        $this->data['foto_acc'] = $file_acc;
        $this->data['file_foto_after'] = $file_fa;
        $this->data['file_edit_katalog'] = $file_ek;
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->data);
    }
    
    public function PesananBaruDev(){
        if($this->user['role'] == Role::MARKETING || $this->user['role'] == Role::DIREKTUR || $this->user['role'] == Role::ADMINISTRATOR || $this->user['role'] == Role::PRODUKSI || $this->user['role'] == Role::DESAIN){
            if($this->postData['filterExist']){
                $where .= $this->getFilter($this->postData);
            }
            $this->data = $this->model->filter($where)->getPesananBaruDev($this->page);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
        }
        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Role anda tidak bisa mengakses fungsi ini'));
    }


    public function edit($pesanan_id){
        // $data = $this->input->post();
        $update = $this->model->edit($this->postData, $pesanan_id);
        if($update){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data'), $update);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update data'));
        }

    }
    
}

?>