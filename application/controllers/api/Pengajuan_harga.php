<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan_harga extends PrivateApiController {

    function __construct() {
        parent::__construct();
    }


    // public function tesnotif(){
    //     $this->load->model('Notification_model');
    //     $this->Notification_model->generate(Notification_model::ACTION_PESANAN_CREATE, 67,$this->user['id']);
    //     // $receiver_id = 18;
    //     // $message = "ini adalah message";
    //     // $reference_id = 65;
    //     // $notification_user_id = 13;
    //     // $this->Notification_model->mobileNotification($receiver_id, $message, $reference_id,$notification_user_id, $type='pesanan');
    // }

    public function index() {
        $data = $this->postData;
        
        $filter = "where 1 ";

        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }
        // if($this->user['role'] == Role::MITRA_MARKETING){
        //     $filter .=" and user_id_pengaju=".$this->user['id'];
        // }
        $this->data = $this->model->filter($filter)->getPengajuan($this->page, $data['status'], $this->user['role'], $this->user['id'], $data['bulan'], $data['tahun'], $data['keyword']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getOnePengajuan() {
        $data = $this->postData;
        
        $filter = "where 1 ";

        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }

        $this->data = $this->model->filter($filter)->getOnePengajuan($this->page, $data['id_pengajuan']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getDetailPengajuan($id) {
        $this->data = $this->model->filter($filter)->getDetailPengajuan($id);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

    public function create() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        
        $insert = $this->model->create($data, $this->user['id']);
        
        if ($insert) {
            $hargaArray = json_decode($data['list_harga'], true);
                
            foreach ($hargaArray as $key => $value) {
                $dataInsert = array(
                    'id_pengajuan_harga'    => $insert,
                    'id_harga'              => $value['id'],
                    'harga_pokok_granit'    => $value['harga_pokok_granit'],
                    'harga_pokok_keramik'   => $value['harga_pokok_keramik'],
                    'harga_jual_granit'     => $value['harga_jual_granit'],
                    'harga_jual_keramik'    => $value['harga_jual_keramik'],
                    'jumlah'                => $value['jumlah'],
                    'updated_by'            => $this->user['id']
                );
                
                // var_dump($dataInsert);
                // die();

                $this->load->model('Detail_pengajuan_model');
                $detail = $this->Detail_pengajuan_model->create($dataInsert, TRUE, TRUE);
                
                if (!$detail) {
                    $hapus = dbQuery("delete from pengajuan_harga where id=".$insert);
                    $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga=".$insert);
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat pengajuan'));
                }
            }
            
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat pengajuan '), $pengajuan_id);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat pengajuan'));
        }
    }
    
    public function update() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->updateHarga($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update harga '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update harga'));
        }
    }
    
    public function approveHarga() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->approveHarga($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil approve pengajuan '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal approve pengajuan'));
        }
    }
    
    public function updateHargaSetuju() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->updateHargaSetuju($data);
        
        if ($insert) {
            $this->updateFeeDesainer($data['id']);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update pengajuan '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update pengajuan'));
        }
    }

    public function updateFeeDesainer($pengajuan_harga_id){
        $this->load->model('Fee_desainer_model');
        $id_konsumen = dbGetOne("select id_konsumen from pengajuan_harga where id=".$pengajuan_harga_id);
        $pesanan = dbGetRow("select id,harga_deal from pesanan_view where konsumen_id=".$id_konsumen);;
        $fee_desainer = $this->Fee_desainer_model->getFeeRow($pesanan['id']);
        if($fee_desainer){
            $fee_design = 50000;
            if ($pesanan['harga_deal']>=7500000 && $pesanan['harga_deal']<10000000) {
                $fee_design = 75000;
            }elseif($pesanan['harga_deal']>=10000000){
                $fee_design = 100000;
            }
            $updateFeeDesainer = array(
                'pesanan_id'            => $pesanan['id'],
                'fee_design'            => $fee_design
            );

            if(!in_array($fee_desainer['persentase_eksekusi'], array(0, null))){
                $updateFeeDesainer['fee_eksekusi'] = $pesanan['harga_deal'] * ($fee_desainer['persentase_eksekusi'] / 100);
            }
            $this->Fee_desainer_model->edit($updateFeeDesainer, $fee_desainer['id']);
            return true;

        }else{
            return false;
        }

        
    }
    
    public function tolakHarga() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->tolakPengajuanHarga($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menolak pengajuan '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal menolak pengajuan'));
        }
    }
    
    public function ajukanHarga() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->ajukanHarga($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
        }
    }
    
    public function revisiHpp() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->revisiHpp($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil melakukan pengajuan'), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal melakukan pengajuan'));
        }
    }
    
    public function updateKonsumen() {
        $data = $this->postData;
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        $update = $this->model->updateKonsumen($data);
        
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data'), $update);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update data'));
        }
    }
    
    public function updateJenis() {
        $data = $this->postData;
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        $update = $this->model->updateJenis($data);
        
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data'), $update);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update data'));
        }
    }
    
    public function ubahDiskon() {
        $data = $this->postData;
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        $update = $this->model->ubahDiskon($data);
        
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data'), $update);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update data'));
        }
    }
    
    public function getLabaRugiBulanan() {
        $tahun = $this->postData['tahun'];

        $this->data = $this->model->getLabaRugiBulanan($tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getDetailLaba() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $this->data = $this->model->getDetailLaba($bulan, $tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getDetailPengeluaran() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $this->data = $this->model->getDetailPengeluaran($bulan, $tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function deletePengajuan($id) {
        $hapus = dbQuery("update pengajuan_harga set is_deleted = '1' where id=".$id);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data'), $hapus);
    }
    
    public function index_dev() {
        $data = $this->postData;
        
        $filter = "where 1 ";

        if($this->postData['filterExist']){
            $filter .= $this->getFilter($this->postData);
        }

        $this->data = $this->model->filter($filter)->getPengajuanDev($this->page, $data['status'], $this->user['role'], $data['bulan'], $data['tahun']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function revisi_detail_dev() {
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        // Hapus data detail yang lama
        $hapus = dbQuery("delete from detail_pengajuan where id_pengajuan_harga='".$data['id_pengajuan_harga']."'");
        
        $this->load->model('Detail_pengajuan_model');
        
        if ($hapus) {
            // Tambahkan data detail yang baru
            $hargaArray = json_decode($data['list_harga'], true);
                
            foreach ($hargaArray as $key => $value) {
                $dataInsert = array(
                    'id_pengajuan_harga'    => $data['id_pengajuan_harga'],
                    'id_harga'              => $value['id'],
                    'harga_pokok_granit'    => $value['harga_pokok_granit'],
                    'harga_pokok_keramik'   => $value['harga_pokok_keramik'],
                    'harga_jual_granit'     => $value['harga_jual_granit'],
                    'harga_jual_keramik'    => $value['harga_jual_keramik'],
                    'jumlah'                => $value['jumlah'],
                    'updated_by'            => $this->user['id']
                );
    
                $detail = $this->Detail_pengajuan_model->create($dataInsert, TRUE, TRUE);
            }
            
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat revisi '), $revisi);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat revisi'));
        }
    }

    public function getKpiMarketing(){
        $this->load->model('Pembayaran_log_model');
        $data = $this->postData;
        $satuan = $data['satuan'];
        $start = $data['start'];
        $end = $data['end'];
        // $satuan = 1;
        // $start = '2020-07-01';
        // $end   = '2020-07-30';

        $lead = $this->model->getLeadOrDeal($satuan, $start, $end);
        $deal = $this->model->getLeadOrDeal($satuan, $start, $end,'d');
        if($satuan==2){
            $date_start = new DateTime($start);
            $week_start = $date_start->format("Y-W");
            $date_end = new DateTime($end);
            $week_end = $date_end->format("Y-W");
            $lead = $this->model->getLeadOrDeal($satuan, $week_start, $week_end);
            $deal = $this->model->getLeadOrDeal($satuan, $week_start, $week_end,'d');
            $pengeluaran = $this->Pembayaran_log_model->getPengeluaran($satuan, $week_start, $week_end);
        }else{
            $lead = $this->model->getLeadOrDeal($satuan, $start, $end);
            $deal = $this->model->getLeadOrDeal($satuan, $start, $end,'d');
            $pengeluaran = $this->Pembayaran_log_model->getPengeluaran($satuan, $start, $end);
        }
        // echo "<pre>";print_r($pengeluaran);die();
        

        $data = array();
        $index = 0;

        switch ($satuan) {
            case 1:
                $mulai    = (new DateTime($start));
                $akhir    = (new DateTime($end));
                $interval = DateInterval::createFromDateString('1 day');
                $period   = new DatePeriod($mulai, $interval, $akhir);
                foreach ($period as $dt) {
                    // echo $dt->format("Y-m") . "<br>\n";
                    $month = $dt->format("m");
                    $year = $dt->format("Y");
                    $tanggal = $dt->format("Y-m-d");
                    // echo $tahun_bulan."<br>";
                    $data[$index]['tanggal'] = $tanggal;
                    $data[$index]['tahun'] = $dt->format("Y");
                    $data[$index]['bulan'] = $dt->format("M");
                    $data[$index]['tanggal'] = $dt->format("d");
                    $data[$index]['label'] = $dt->format("d M Y");

                    $index_lead =  array_search($tanggal, array_column($lead, 'tgl_pengajuan'));
                    if ($index_lead === 0 || $index_lead > 0){
                        // echo "data lead=".$lead[$index]['jumlah']."<br>";
                        $data[$index]['lead'] = $lead[$index_lead]['jumlah'];
                    }
                    else{
                        $data[$index]['lead'] = 0;
                        // echo "data lead=0<br>";
                    }

                    $index_deal =  array_search($tanggal, array_column($deal, 'tgl_order_masuk'));
                    // echo $index."<br>";
                    if ($index_deal === 0 || $index_deal > 0){
                        // echo "data deal=".$deal[$index_deal]['jumlah']."<br>";
                        $data[$index]['deal'] = $deal[$index_deal]['jumlah'];
                        $data[$index]['harga_produksi'] = $deal[$index_deal]['harga_produksi'];
                        $data[$index]['omzet'] = $deal[$index_deal]['omzet'];
                    }
                    else{
                        $data[$index]['deal'] = 0;
                        $data[$index]['harga_produksi'] = 0;
                        $data[$index]['omzet'] = 0;
                        // echo "data deal=0<br>";
                    }

                    $data[$index]['persentase'] = ($data[$index]['deal']/$data[$index]['lead'])*100;
                    $data[$index]['profit'] = ($data[$index]['omzet'] - $data[$index]['harga_produksi']);

                    $index_pengeluaran =  array_search($tanggal, array_column($pengeluaran, 'tgl_input'));
                    // echo $index."<br>";
                    if ($index_pengeluaran === 0 || $index_pengeluaran > 0){
                        // echo "data deal=".$deal[$index_deal]['jumlah']."<br>";
                        $data[$index]['pengeluaran'] = $pengeluaran[$index_pengeluaran]['total_pengeluaran'];

                    }
                    else{
                        $data[$index]['pengeluaran'] = 0;
                        // echo "data deal=0<br>";
                    }

                    $data[$index]['margin_profit'] = $data[$index]['profit'] - $data[$index]['pengeluaran'];
                    $data[$index]['persentase_profit'] = ($data[$index]['margin_profit'] / $data[$index]['omzet'])*100;

                    $index +=1;
                    
                }
            break;
            case 2:
                $mulai    = (new DateTime($start));
                $akhir    = (new DateTime($end));
                $interval = DateInterval::createFromDateString('1 week');
                $period   = new DatePeriod($mulai, $interval, $akhir);
                foreach ($period as $dt) {
                    $tahun_pekan = $dt->format("Y-W");
                    $data[$index]['tahun_pekan'] = $tahun_pekan;
                    $data[$index]['tahun'] = $dt->format("Y");
                    $data[$index]['pekan'] = $dt->format("W");
                    $data[$index]['label'] = $dt->format("W Y");

                    $index_lead =  array_search($tahun_pekan, array_column($lead, 'tahun_pekan'));
                    if ($index_lead === 0 || $index_lead > 0){
                        $data[$index]['lead'] = $lead[$index_lead]['jumlah'];
                    }
                    else{
                        $data[$index]['lead'] = 0;
                    }

                    $index_deal =  array_search($tahun_pekan, array_column($deal, 'tahun_pekan'));
                    // echo $index."<br>";
                    if ($index_deal === 0 || $index_deal > 0){
                        // echo "data deal=".$deal[$index_deal]['jumlah']."<br>";
                        $data[$index]['deal'] = $deal[$index_deal]['jumlah'];
                        $data[$index]['harga_produksi'] = $deal[$index_deal]['harga_produksi'];
                        $data[$index]['omzet'] = $deal[$index_deal]['omzet'];
                    }
                    else{
                        $data[$index]['deal'] = 0;
                        $data[$index]['harga_produksi'] = 0;
                        $data[$index]['omzet'] = 0;
                        // echo "data deal=0<br>";
                    }

                    $data[$index]['persentase'] = ($data[$index]['deal']/$data[$index]['lead'])*100;
                    $data[$index]['profit'] = ($data[$index]['omzet'] - $data[$index]['harga_produksi']);

                    $index_pengeluaran =  array_search($tahun_pekan, array_column($pengeluaran, 'tahun_pekan'));
                    // echo $index."<br>";
                    if ($index_pengeluaran === 0 || $index_pengeluaran > 0){
                        // echo "data deal=".$deal[$index_deal]['jumlah']."<br>";
                        $data[$index]['pengeluaran'] = $pengeluaran[$index_pengeluaran]['total_pengeluaran'];

                    }
                    else{
                        $data[$index]['pengeluaran'] = 0;
                        // echo "data deal=0<br>";
                    }
                    $data[$index]['margin_profit'] = $data[$index]['profit'] - $data[$index]['pengeluaran'];
                    $data[$index]['persentase_profit'] = ($data[$index]['margin_profit'] / $data[$index]['omzet'])*100;

                    $index +=1;
                }
            break;
            case 3:
                $mulai    = (new DateTime($start))->modify('first day of this month');
                $akhir    = (new DateTime($end))->modify('first day of next month');
                $interval = DateInterval::createFromDateString('1 month');
                $period   = new DatePeriod($mulai, $interval, $akhir);
                foreach ($period as $dt) {
                    // echo $dt->format("Y-m") . "<br>\n";
                    $month = $dt->format("m");
                    $year = $dt->format("Y");
                    $tahun_bulan = $dt->format("Y-m");
                    // echo $tahun_bulan."<br>";
                    $data[$index]['tahun_bulan'] = $tahun_bulan;
                    $data[$index]['tahun'] = $dt->format("Y");
                    $data[$index]['bulan'] = $dt->format("M");
                    $data[$index]['label'] = $dt->format("M Y");

                    $index_lead =  array_search($tahun_bulan, array_column($lead, 'tahun_bulan'));
                    if ($index_lead === 0 || $index_lead > 0){
                        // echo "data lead=".$lead[$index]['jumlah']."<br>";
                        $data[$index]['lead'] = $lead[$index_lead]['jumlah'];
                    }
                    else{
                        $data[$index]['lead'] = 0;
                        // echo "data lead=0<br>";
                    }

                    $index_deal =  array_search($tahun_bulan, array_column($deal, 'tahun_bulan'));
                    // echo $index."<br>";
                    if ($index_deal === 0 || $index_deal > 0){
                        // echo "data deal=".$deal[$index_deal]['jumlah']."<br>";
                        $data[$index]['deal'] = $deal[$index_deal]['jumlah'];
                        $data[$index]['harga_produksi'] = $deal[$index_deal]['harga_produksi'];
                        $data[$index]['omzet'] = $deal[$index_deal]['omzet'];
                    }
                    else{
                        $data[$index]['deal'] = 0;
                        $data[$index]['harga_produksi'] = 0;
                        $data[$index]['omzet'] = 0;
                        // echo "data deal=0<br>";
                    }

                    $data[$index]['persentase'] = ($data[$index]['deal']/$data[$index]['lead'])*100;
                    $data[$index]['profit'] = ($data[$index]['omzet'] - $data[$index]['harga_produksi']);

                    $index_pengeluaran =  array_search($tahun_bulan, array_column($pengeluaran, 'tahun_bulan'));
                    // echo $index."<br>";
                    if ($index_pengeluaran === 0 || $index_pengeluaran > 0){
                        // echo "data deal=".$deal[$index_deal]['jumlah']."<br>";
                        $data[$index]['pengeluaran'] = $pengeluaran[$index_pengeluaran]['total_pengeluaran'];

                    }
                    else{
                        $data[$index]['pengeluaran'] = 0;
                        // echo "data deal=0<br>";
                    }

                    $data[$index]['margin_profit'] = $data[$index]['profit'] - $data[$index]['pengeluaran'];
                    $data[$index]['persentase_profit'] = ($data[$index]['margin_profit'] / $data[$index]['omzet'])*100;

                    $index +=1;
                    
                }
            break;
            
        }
        $this->data = $data;

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data KPI Marketing'), $this->data);

    }
    
}

?>