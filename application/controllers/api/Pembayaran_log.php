<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_log extends PrivateApiController {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        if($this->user['role'] == Role::MITRA_MARKETING){
            $filter = " and created_by=".$this->user['user_id'];
        }
        $this->data = $this->model->filter($filter)->getLog($bulan, $tahun,'tgl_input','desc', $filter);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }

    public function getSubJenisPembayaran($id_jenis_pembayaran){

        $this->load->model('Jenis_pembayaran_model');
        $this->data = $this->Jenis_pembayaran_model->getSubJenis($id_jenis_pembayaran);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    
    }

    public function create() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        $insert = $this->model->create($data, $this->user['id']);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat data '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat data'));
        }
    }
    
    public function update() {
        // if($this->user['role'] != Role::DIREKTUR || $this->user['role'] != Role::ADMINISTRATOR){
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda tidak bisa mengakses fungsi ini"));
        // }
        $data = $this->postData;
        
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $insert = $this->model->updateLog($data);
        
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update data '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update data'));
        }
    }
    
    public function delete($id) {
        // $hapus = dbQuery("UPDATE FROM pembayaran_log SET is_deleted='1' where id=".$id);
        $hapus = dbQuery("DELETE FROM pembayaran_log WHERE id=".$id);
        if ($hapus) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil hapus data '), $insert);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal hapus data'));
        }
    }
    
    public function getBebanStatistikPerMonth() {
        $tahun = $this->postData['tahun'];

        $this->data = $this->model->filter($filter)->getBebanStatistikPerMonth($tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getLunasStatistikPerMonth() {
        $tahun = $this->postData['tahun'];

        $this->data = $this->model->filter($filter)->getLunasStatistikPerMonth($tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getDetailStatistik() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $this->data = $this->model->filter($filter)->getDetailStatistik($bulan, $tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getDetailBebanStatistik() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $this->data = $this->model->getDetailBebanPembayaran($bulan, $tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getDetailLunasStatistik() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $this->data = $this->model->getDetailLunasPembayaran($bulan, $tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getHutangProduksiPerBulan() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $this->data = $this->model->getHutangProduksiPerBulan($bulan, $tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getListHutangProduksi() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $this->data = $this->model->getListHutangProduksi($bulan, $tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getListStatusPiutangKonsumen() {
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];

        $this->data = $this->model->getListStatusPiutangKonsumen($bulan, $tahun, $this->user['role'], $this->user['id']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
    
    public function getSumPiutangKonsumenPerBulan() {
        $tahun = $this->postData['tahun'];

        $this->data = $this->model->getSumPiutangKonsumenPerBulan($tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
        
    }
    
    // 19 NOVEMBEr 2020
    public function getOmzet() {
        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
        }
        
        $tahun = $this->postData['tahun'];
        $bulan = $this->postData['bulan'];
        
        $this->data = $this->model->getOmzet($data['user_id'], $bulan, $tahun);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendapatkan data'), $this->data);
    }
}

?>