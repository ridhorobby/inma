<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Todo extends PrivateApiController {

    public function for_new() {
        $todos = $this->model->getNew($this->user['id'], $this->page);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $todos);
    }

    public function for_old() {
        $todos = $this->model->getOld($this->user['id'], $this->page);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $todos);
    }

    public function create() {
        $data = $this->postData;
        $data['user_id'] = $this->user['id'];
        $data['image'] = isset($_FILES['image']) ? $_FILES['image']['name'] : NULL;
        $data['video'] = isset($_FILES['video']) ? $_FILES['video']['name'] : NULL;
        $data['file'] = isset($_FILES['file']) ? $_FILES['file']['name'] : NULL;

        $insert = $this->model->create($data);
        if ($insert === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat todo'));
        } elseif (is_string($insert)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat todo'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }
    }

    public function edit($id) {
        $todo = $this->model->get($id);
        if ($todo['userId'] == $this->user['id']) {
            $update = $this->model->save($id, $this->postData, TRUE);
            if ($update === TRUE) {
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mengubah todo'));
            } elseif (is_string($update)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, $update));
            } else {
                $validation = $this->model->getErrorValidate();
                if (empty($validation)) {
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mengubah todo'));
                } else {
                    $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
                }
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Tidak dapat mengubah todo ini"));
        }
    }

    public function detail($id) {
        $todo = $this->model->get($id);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $todo);
    }

    public function delete($id) {
        $todo = $this->model->get($id);
        if ($todo['userId'] == $this->user['id']) {
            $this->load->model('Post_model');
            $delete = $this->Post_model->delete($id);
            if ($delete === TRUE) {
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menghapus todo'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal menghapus todo"));
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Tidak dapat menghapus todo ini"));
        }
    }

}
