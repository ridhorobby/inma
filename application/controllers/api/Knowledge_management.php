<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Knowledge_management extends PrivateApiController {

    /**
     * Digunakan untuk mengambil list post public
     */
    public function index() {
        $posts = $this->model->getAll($this->page, $this->postData['param']);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $posts);
    }

    public function tag() {
        $posts = $this->model->getByTag($this->page, $this->postData['param']);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $posts);
    }

    public function create() {
        $data = $this->postData;
        $data['user_id'] = $this->user['id'];
        $data['image'] = isset($_FILES['image']) ? $_FILES['image']['name'] : NULL;
        $data['video'] = isset($_FILES['video']) ? $_FILES['video']['name'] : NULL;
        $data['file'] = isset($_FILES['file']) ? $_FILES['file']['name'] : NULL;

        $data['categoryId'] = Category_model::KNOWLEDGE_MANAGEMENT;

        $etags = explode('#', $data['tags']);
        $tmpTags = array();
        foreach ($etags as $tag) {
            $tag = strtolower(str_replace(" ", "", $tag));
            if (!empty($tag))
                $tmpTags[] = $tag;
        }

        $data['postTags'] = implode(',', $tmpTags);
        $insert = $this->model->create($data);
        if ($insert === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat postingan'));
        } elseif (is_string($insert)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat postingan'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }
    }

    public function update($id) {
        $data = $this->postData;
        $data['userIdUpdated'] = $this->user['id'];
        $data['image'] = isset($_FILES['image']) ? $_FILES['image']['name'] : NULL;
        $data['video'] = isset($_FILES['video']) ? $_FILES['video']['name'] : NULL;
        $data['file'] = isset($_FILES['file']) ? $_FILES['file']['name'] : NULL;

        $data['categoryId'] = Category_model::KNOWLEDGE_MANAGEMENT;

        $etags = explode('#', $data['tags']);
        $tmpTags = array();
        foreach ($etags as $tag) {
            $tag = strtolower(str_replace(" ", "", $tag));
            if (!empty($tag))
                $tmpTags[] = $tag;
        }

        $data['postTags'] = implode(',', $tmpTags);

        $insert = $this->model->update($id, $data);
        if ($insert === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat postingan'));
        } elseif (is_string($insert)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat postingan'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }
    }

    public function detail($id) {
        //SET READ NOTIFICATION
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($this->user['id'], Notification_model::TYPE_POST_DETAIL, $id);

        $post = $this->model->with(array('Category', 'Comment' => 'User', 'User', 'Post_tag', 'Updated_by'))->get_by('posts.id', $id);

        if (!empty($post)) {
            $this->load->model('Tracker_model');
            $this->Tracker_model->post($id, $this->user['id']);
        }
        
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $post);
    }

    public function delete($id) {
        $post = $this->model->get_by('posts.id', $id);
        if ($post['userId'] == $this->user['id']) {
            $delete = $this->model->delete($id);
            if ($delete === TRUE) {
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menghapus postingan'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal menghapus postingan"));
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Tidak dapat menghapus postingan ini"));
        }
    }

    public function reuse($id) {
        $this->load->model('Comment_model');
        $data = $this->postData;
        $data['userId'] = $this->user['id'];
        if (!empty($data['text'])) {
            $data['text'] = '[REUSE] ' . $data['text'];
        }
        $insert = $this->Comment_model->create($data);
        if ($insert === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menggunakan KM'));
        } elseif (is_string($insert)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        } else {
            $validation = $this->Comment_model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal gagal menggunakan KM'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }
    }

}

?>