<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends PrivateApiController {

    /**
     * Digunakan untuk mengambil notifikasi user yang sedang login
     */
    public function me() {
        //SET UNREAD NOTIFICATION
        $this->load->model('Notification_user_model');
        // $this->Notification_user_model->setAsUnRead($this->user['id']);

        $notifications = $this->model->getMe($this->user['id'], $this->page, true);
    // foreach ($notifications as $key => $notification) {
    //     $post = $this->Post_model->getPost($this->user['id'],$notification['referenceId']);
    //     $notifications[$key]['post_type'] = $post['post_type'];
    // }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $notifications);
    }

    public function setAllRead() {
        $this->load->model('Notification_user_model');
        $resp = $this->Notification_user_model->setAsUnRead($this->user['id']);
        if ($resp) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Semua notifikasi terbaca'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Gagal set Notifikasi'));
        }
    }

}

?>