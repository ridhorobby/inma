ALTER TABLE `usulan` ADD `jam_usulan` TIME NULL DEFAULT NULL AFTER `jadwal_usulan`;

ALTER TABLE `komplain_usulan` ADD `jam_usulan` TIME NULL DEFAULT NULL AFTER `jadwal_usulan`;


ALTER TABLE `komplain` ADD `catatan` TEXT NULL DEFAULT NULL AFTER `is_deleted`;

ALTER TABLE `pesanan` ADD `catatan` TEXT NULL DEFAULT NULL AFTER `link`;


CREATE or replace view pesanan_view as select
    `p`.`id` as `id`,
    `p`.`kode_order` as `kode_order`,
    `p`.`tgl_order_masuk` as `tgl_order_masuk`,
    `p`.`tgl_jatuh_tempo` as `tgl_jatuh_tempo`,
    `p`.`tgl_acc_konsumen` as `tgl_acc_konsumen`,
    `p`.`flow_id` as `flow_id`,
    `f`.`nama` as `flow_name`,
    `f`.`rencana_kerja_id` as `rencana_kerja_id`,
    `r`.`nama` as `rencana_kerja_nama`,
    `p`.`user_id` as `user_id`,
    `u`.`name` as `nama_user`,
    `p`.`konsumen_id` as `konsumen_id`,
    `k`.`nama` as `nama_konsumen`,
    `k`.`alamat` as `alamat_konsumen`,
    `k`.`no_hp` as `no_hp_konsumen`,
    `p`.`desainer_design` as `desainer_design`,
    `dsn`.`name` as `nama_desainer_design`,
    `p`.`desainer_eksekusi` as `desainer_eksekusi`,
    `dsne`.`name` as `nama_desainer_eksekusi`,
    `p`.`mitra_pemasang_kusen` as `mitra_pemasang_kusen`,
    `mpk`.`name` as `nama_pemasang_kusen`,
    `p`.`mitra_pemasang_finish` as `mitra_pemasang_finish`,
    `mpf`.`name` as `nama_pemasang_finish`,
    `uk`.`jam_usulan` as `jam_pasang_kusen`,
    `uk`.`jadwal_usulan` as `tanggal_pasang_kusen`,
    date_add(`p`.`tgl_order_masuk`,INTERVAL 4 DAY) as `jatuh_tempo_kusen`,
    (CASE
        when current_date >= date_add(`p`.`tgl_order_masuk`,INTERVAL 4 DAY) and `uk`.`jadwal_usulan` is null and `p`.`flow_id`= 2 then 1
        else 0
    END) as `reminder_kusen`,
    datediff(current_date, date_add(`p`.`tgl_order_masuk`,INTERVAL 4 DAY)) as `selisih_tanggal_kusen`,
    `uf`.`jam_usulan` as `jam_pasang_finish`,
    `uf`.`jadwal_usulan` as `tanggal_pasang_finish`,
    date_add(`p`.`tgl_order_masuk`,INTERVAL 4 DAY) as `jatuh_tempo_finish`,
    (CASE
        when current_date >= date_add(`uk`.`jadwal_usulan`,INTERVAL 10 DAY) and `uf`.`jadwal_usulan` is null and `p`.`flow_id`= 5  then 1
        else 0
    END) as `reminder_finish`,
    datediff(current_date, date_add(`uk`.`jadwal_usulan`,INTERVAL 10 DAY)) as `selisih_tanggal_finish`,
    `p`.`is_deleted` as `is_deleted`,
    `p`.`tipe` as `tipe`,
    `p`.`status_produksi` as `status_produksi`,
    `p`.`link` as `link_sosmed`,
    `p`.`catatan` as `catatan`,
    (case
        `ph`.`jenis`
        when 1 then `ph`.`harga_setuju_granit`
        when 2 then `ph`.`harga_setuju_keramik`
    end) as `harga_deal`,
    (case
        `ph`.`jenis`
        when 1 then 'ranit'
        when 2 then 'keramik'
    end) as `tipe_pengajuan`,
    `fd`.`fee_design` as `fee_design`,
    `fd`.`persentase_eksekusi` as `persentase_eksekusi`,
    `fd`.`fee_eksekusi` as `fee_eksekusi`,
    (
        select count(0)
    from
        `inma`.`pesanan_files` `pf`
    where
        ((`pf`.`pesanan_id` = `p`.`id`)
        and (`pf`.`tipe` = 'FA'))) as `foto_after`,
    (
        select count(0)
    from
        `inma`.`pesanan_files` `pf`
    where
        ((`pf`.`pesanan_id` = `p`.`id`)
        and (`pf`.`tipe` = 'EK'))) as `edt_katalog`
from
    ((((((((((((`inma`.`pesanan` `p`
left join `inma`.`pesanan_flow` `f` on
    ((`p`.`flow_id` = `f`.`id`)))
left join `inma`.`rencana_kerja` `r` on
    ((`f`.`rencana_kerja_id` = `r`.`id`)))
left join `inma`.`users` `dsn` on
    ((`p`.`desainer_design` = `dsn`.`id`)))
left join `inma`.`users` `dsne` on
    ((`p`.`desainer_eksekusi` = `dsne`.`id`)))
left join `inma`.`users` `mpk` on
    ((`p`.`mitra_pemasang_kusen` = `mpk`.`id`)))
left join `inma`.`users` `mpf` on
    ((`p`.`mitra_pemasang_finish` = `mpf`.`id`)))
left join `inma`.`users` `u` on
    ((`p`.`user_id` = `u`.`id`)))
left join `inma`.`konsumen` `k` on
    ((`p`.`konsumen_id` = `k`.`id`)))
left join `inma`.`usulan` `uk` on
    (((`p`.`id` = `uk`.`pesanan_id`)
    and (`uk`.`status` = 'D')
    and (`uk`.`rencana_kerja_id` = 1))))
left join `inma`.`usulan` `uf` on
    (((`p`.`id` = `uf`.`pesanan_id`)
    and (`uf`.`status` = 'D')
    and (`uf`.`rencana_kerja_id` = 2))))
left join `inma`.`pengajuan_harga` `ph` on
    ((`p`.`konsumen_id` = `ph`.`id_konsumen`)))
left join `inma`.`fee_desainer` `fd` on
    ((`p`.`id` = `fd`.`pesanan_id`)))




create or replace
view `inma`.`komplain_view` as select
    `c`.`id` as `id`,
    `c`.`tgl_komplain` as `tgl_komplain`,
    `c`.`tgl_jatuh_tempo` as `tgl_jatuh_tempo`,
    `c`.`tgl_acc_konsumen` as `tgl_acc_konsumen`,
    `c`.`description` as `description`,
    `c`.`pesanan_id` as `pesanan_id`,
    `c`.`user_id` as `user_id`,
    `c`.`status` as `status`,
    `c`.`flow_id` as `flow_id`,
    `c`.`mitra_pemasang_kusen` as `mitra_pemasang_kusen`,
    `c`.`mitra_pemasang_finish` as `mitra_pemasang_finish`,
    `c`.`created_at` as `created_at`,
    `c`.`updated_at` as `updated_at`,
    `c`.`is_deleted` as `is_deleted`,
    `p`.`kode_order` as `kode_order`,
    `p`.`tgl_order_masuk` as `tgl_order_masuk`,
    `p`.`tanggal_pasang_kusen` as `tanggal_pasang_kusen_pesanan`,
    `p`.`tanggal_pasang_finish` as `tanggal_pasang_finish_pesanan`,
    `p`.`tgl_acc_konsumen` as `tgl_acc_konsumen_pesanan`,
    `mpk`.`name` as `nama_pemasang_kusen`,
    `mpf`.`name` as `nama_pemasang_finish`,
    `uk`.`jam_usulan` as `jam_pasang_kusen`,
    `uk`.`jadwal_usulan` as `tanggal_pasang_kusen`,
    (CASE
        when current_date >= date_add(`p`.`tgl_order_masuk`,INTERVAL 4 DAY) and `uk`.`jadwal_usulan` is null and `c`.`flow_id`= 2 then 1
        else 0
    END) as `reminder_kusen`,
    datediff(current_date, date_add(`p`.`tgl_order_masuk`,INTERVAL 4 DAY)) as `selisih_tanggal_kusen`,
    `uf`.`jam_usulan` as `jam_pasang_finish`,
    `uf`.`jadwal_usulan` as `tanggal_pasang_finish`,
    (CASE
        when current_date >= date_add(`uk`.`jadwal_usulan`,INTERVAL 10 DAY) and `uf`.`jadwal_usulan` is null and `c`.`flow_id`= 5 then 1
        else 0
    END) as `reminder_finish`,
    datediff(current_date, date_add(`uk`.`jadwal_usulan`,INTERVAL 10 DAY)) as `selisih_tanggal_finish`,
    `f`.`nama` as `flow_name`,
    `f`.`rencana_kerja_id` as `rencana_kerja_id`,
    `r`.`nama` as `rencana_kerja_nama`,
    `p`.`konsumen_id` as `konsumen_id`,
    `k`.`nama` as `nama_konsumen`,
    `k`.`alamat` as `alamat_konsumen`,
    `k`.`no_hp` as `no_hp_konsumen`,
    `c`.`catatan` as `catatan`
from
    ((((((((`inma`.`komplain` `c`
left join `inma`.`pesanan_flow` `f` on
    ((`c`.`flow_id` = `f`.`id`)))
left join `inma`.`rencana_kerja` `r` on
    ((`f`.`rencana_kerja_id` = `r`.`id`)))
left join `inma`.`pesanan_view` `p` on
    ((`c`.`pesanan_id` = `p`.`id`)))
left join `inma`.`konsumen` `k` on
    ((`p`.`konsumen_id` = `k`.`id`)))
left join `inma`.`users` `mpk` on
    ((`c`.`mitra_pemasang_kusen` = `mpk`.`id`)))
left join `inma`.`users` `mpf` on
    ((`c`.`mitra_pemasang_finish` = `mpf`.`id`)))
left join `inma`.`komplain_usulan` `uk` on
    (((`c`.`id` = `uk`.`komplain_id`)
    and (`uk`.`status` = 'D')
    and (`uk`.`rencana_kerja_id` = 1))))
left join `inma`.`komplain_usulan` `uf` on
    (((`c`.`id` = `uf`.`komplain_id`)
    and (`uf`.`status` = 'D')
    and (`uf`.`rencana_kerja_id` = 2))))


CREATE TABLE IF NOT EXISTS `harga_maintenance` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

ALTER TABLE harga_maintenance
ADD PRIMARY KEY (ID);
ALTER TABLE `harga_maintenance` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `harga_maintenance` (`id`, `nama`, `harga`, `created_at`, `updated_at`) VALUES
(1, 'Lem Plat Pintu', 100000, '2021-04-26 14:22:54', '0000-00-00 00:00:00'),
(2, 'Ganti Magnet', 50000, '2021-04-26 14:22:54', '0000-00-00 00:00:00'),
(3, 'Ganti Engsel', 50000, '2021-04-26 14:22:54', '0000-00-00 00:00:00'),
(4, 'Cat Ulang Pintu', 30000, '2021-04-26 14:22:54', '0000-00-00 00:00:00'),
(5, 'Ganti Rangka Laci', 350000, '2021-04-26 14:22:54', '0000-00-00 00:00:00'),
(6, 'Ganti Rel Laci', 150000, '2021-04-26 14:22:54', '0000-00-00 00:00:00'),
(7, 'Ganti Pintu Granit', 400000, '2021-04-26 14:22:54', '0000-00-00 00:00:00'),
(8, 'Ganti Pintu Keramik', 300000, '2021-04-26 14:22:54', '0000-00-00 00:00:00');

CREATE TABLE IF NOT EXISTS `pengajuan_harga_komplain` (
  `id` int(11) NOT NULL,
  `tgl_pengajuan` date NOT NULL,
  `tgl_disetujui` date DEFAULT '0000-00-00',
  `id_konsumen` int(11) NOT NULL DEFAULT '0',
  `nama_konsumen` varchar(50) NOT NULL,
  `alamat_konsumen` varchar(100) NOT NULL,
  `no_hp_konsumen` varchar(20) NOT NULL,
  `harga_setuju` int(11) NOT NULL DEFAULT '0',
  `diskon` int(11) NOT NULL DEFAULT '0',
  `uang_muka` int(11) DEFAULT '0',
  `tanda_jadi` int(11) NOT NULL DEFAULT '0',
  `pelunasan` int(11) NOT NULL DEFAULT '0',
  `jenis` int(11) NOT NULL DEFAULT '0' COMMENT '1 Untuk granit, 2 untuk keramik',
  `status` varchar(50) NOT NULL,
  `revisi_hpp` varchar(1) DEFAULT '0',
  `keterangan` varchar(200) NOT NULL,
  `is_deleted` varchar(1) NOT NULL DEFAULT '0',
  `user_id_pengaju` int(11) NOT NULL,
  `user_id_approve` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

ALTER TABLE pengajuan_harga_komplain
ADD PRIMARY KEY (ID);
ALTER TABLE `pengajuan_harga_komplain` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE IF NOT EXISTS `detail_pengajuan_komplain` (
  `id` int(11) NOT NULL,
  `id_pengajuan_harga_komplain` int(11) NOT NULL,
  `id_harga_maintenance` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` decimal(10,0) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `is_deleted` varchar(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

ALTER TABLE detail_pengajuan_komplain
ADD PRIMARY KEY (ID);
ALTER TABLE `detail_pengajuan_komplain` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pesanan` ADD `desainer` INT NULL DEFAULT NULL AFTER `konsumen_id`;
ALTER TABLE `pesanan` ADD `catatan` TEXT NULL AFTER `status_produksi`;



CREATE TABLE `fee_desainer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desainer_id` int(11) NOT NULL,
  `pesanan_id` int(11) NOT NULL,
  `fee_design` double,
  `persentase_eksekusi`double,
  `fee_eksekusi` double,
    PRIMARY KEY `id`(`id`)
);

ALTER TABLE `fee_desainer` ADD `created_at` TIMESTAMP NULL AFTER `fee_eksekusi`, ADD `updated_at` TIMESTAMP NULL AFTER `created_at`;

CREATE TABLE IF NOT EXISTS `gaji_desainer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desainer_id` int(11) NOT NULL,
  `gaji_pokok` double,
  `gaji_tunjangan`double,
   PRIMARY KEY `id`(`id`)
);

ALTER TABLE `gaji_desainer` ADD `created_at` TIMESTAMP NULL AFTER `gaji_tunjangan`, ADD `updated_at` TIMESTAMP NULL AFTER `created_at`;
ALTER TABLE `gaji_desainer` ADD `bulan` INT NOT NULL AFTER `gaji_tunjangan`, ADD `tahun` INT NOT NULL AFTER `bulan`;
ALTER TABLE `gaji_desainer` CHANGE `bulan` `bulan` VARCHAR(20) NOT NULL, CHANGE `tahun` `tahun` VARCHAR(20) NOT NULL;

ALTER TABLE `users` ADD `bank_rekening` VARCHAR(200) NULL AFTER `role`, ADD `no_rekening` VARCHAR(200) NULL AFTER `bank_rekening`;


ALTER TABLE `pesanan` CHANGE `desainer` `desainer_design` INT(11) NULL DEFAULT NULL;
ALTER TABLE `pesanan` ADD `desainer_eksekusi` INT NULL AFTER `desainer_design`;
ALTER TABLE `fee_desainer` CHANGE `desainer_id` `desainer_design_id` INT(11) NULL;
ALTER TABLE `fee_desainer` ADD `desainer_eksekusi_id` INT NULL AFTER `desainer_design_id`;

ALTER TABLE `pembayaran_log` CHANGE `updated_by` `updated_by` INT(11) NULL DEFAULT NULL;

create or replace
view `inma`.`user_view` as select
    `u`.`id` as `id`,
    `u`.`username` as `username`,
    `u`.`name` as `name`,
    `u`.`no_hp` as `no_hp`,
    `u`.`is_active` as `is_active`,
    `u`.`password` as `password`,
    `u`.`password_salt` as `password_salt`,
    `u`.`role` as `role`,
    `u`.`no_rekening` as `no_rekening`,
    `u`.`created_at` as `created_at`,
    `u`.`updated_at` as `updated_at`,
    `rp`.`id` as `reset_id`,
    `rp`.`temp_password` as `temp_password`
from
    (`inma`.`users` `u`
left join `inma`.`reset_password` `rp` on
    ((`u`.`id` = `rp`.`user_id`)))

-- 31-08-2021 --
----------------

CREATE TABLE jenis_pembayaran_sub (
    id int NOT NULL AUTO_INCREMENT,
    nama varchar(255) NOT NULL,
    id_jenis_pembayaran int,
    create_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

INSERT INTO `jenis_pembayaran_sub`(`id`, `nama`, `id_jenis_pembayaran`) VALUES (1,'Beban Gaji',5);
INSERT INTO `jenis_pembayaran_sub`(`id`, `nama`, `id_jenis_pembayaran`) VALUES (2,'Biaya ATK',5);
INSERT INTO `jenis_pembayaran_sub`(`id`, `nama`, `id_jenis_pembayaran`) VALUES (3,'Biaya IT',5);
INSERT INTO `jenis_pembayaran_sub`(`id`, `nama`, `id_jenis_pembayaran`) VALUES (4,'Biaya Iklan',5);
INSERT INTO `jenis_pembayaran_sub`(`id`, `nama`, `id_jenis_pembayaran`) VALUES (5,'Biaya Service',5);
INSERT INTO `jenis_pembayaran_sub`(`id`, `nama`, `id_jenis_pembayaran`) VALUES (6,'Akomodasi',5);

ALTER TABLE `pembayaran_log` ADD `subjenis` INT NOT NULL AFTER `jenis`;